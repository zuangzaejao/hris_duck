 
  <?php
  require_once '../../config.php';
  require_once '../../Model/Ducklab/duck.class.php'; 
  require_once '../../Model/Ducklab/contents.class.php'; 
  require_once '../../Model/Ducklab/org.class.php'; 
  require_once '../../Model/Ducklab/func.php'; 
  require_once '../include/header.php'; 
 
    $menu1 ="ORGSTRUC" ;
    $menu2 ="ORGSTRUCDEPT";
    $menu3 ="ORGSTRUCDEPTPERSON";
    $menu4 ="ORGANIZATIONGROUPTYPE";

    $clsorg = new OrgClass();

    $lastid = $clsorg->LoadIdenCurrent('OrgGroupType');

    $newid = $lastid['lastid']+1 ;

  ?>
  <?php include '../include/menu.php'; ?>
  <?php include_once '../include/modelOnload.php' ?>
  <div class="app-content content">
      <div class="content-wrapper">
          <div class="content-header row">
              <div class="content-header-left col-md-6 col-12 mb-2">
                  <h3 class="content-header-title">เพิ่มประเภทโครงสร้าง</h3>
                  <div class="row breadcrumbs-top">

                  </div>
              </div> 
          </div>
          <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="./index.php">ระบบงานโครงสร้างอัตรากำลังพล</a></li>
                  <li class="breadcrumb-item"><a href="./index.php">โครงสร้าง</a></li>
                  <li class="breadcrumb-item active" aria-current="page">เพิ่มประเภทโครงสร้าง</li>
              </ol>
          </nav>
          <div class="content-body">
              <!-- Basic form layout section start -->
              <section id="horizontal-form-layouts">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collpase show">
                                <div class="card-body">
                                    <!-- <div class="card-text">
                                        <p>คำโปรย</p>
                                    </div> -->
                                    <form class="form form-horizontal" >
                                        <div class="form-body">
                                            <div class="row"> 
                                                <div class="col-md-10 m-auto mt-1 ">
                                                    <label class="col-md-6 label-control" for="IDAUTO">รหัสประเภทโครงสร้าง</label>
                                                    <div class="col-md-4">
                                                        <div class="position-relative ">
                                                            <input type="text" id="IDAUTO" class="form-control border-primary" name="IDAUTO" value="<?php echo   $newid;?>" disabled>
                                                        </div>
                                                    </div>
                                                </div> 
                                            </div>
                                            <div class="row">
                                                <div class="col-md-10 m-auto  pt-2">
                                                    <label class="col-md-6 label-control" for="orgGroupTypeName">ชื่อประเภทโครงสร้าง</label>
                                                    <div class="col-md-6">
                                                        <div class="position-relative ">
                                                            <input type="text" class="form-control border-primary" id="OrgGroupTypeName" name="OrgGroupTypeName">
                                                        </div>
                                                        <span class="text-cmp"> </span>
                                                    </div>
                                                </div> 
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-10 m-auto">
                                                    <br>
                                                    <label class="col-md-1 label-control" for="OrgGroupTypeActive" style="padding-right:0px;">สถานะ</label>
                                                    <input id="OrgGroupTypeActive" name="OrgGroupTypeActive" type="checkbox" checked data-toggle="toggle" data-style="ios" data-on="ใช้งาน" data-off="ยกเลิก" data-onstyle="success" data-offstyle="danger" data-size="sm">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-10 m-auto">
                                            <div class="form-actions text-center" >
                                                
                                                <input type="hidden"  class="form-control border-primary"  name="OrgGroupTypeCreateBy" id="OrgGroupTypeCreateBy" value="1">
                                                <input type="hidden"  class="form-control border-primary"  name="OrgGroupTypeUpdateBy" id="OrgGroupTypeUpdateBy" value="1">
                                                <input type="hidden"  class="form-control border-primary"  name="OrgGroupTypeCreateDate" id="OrgGroupTypeCreateDate" value="<?php echo GetToday('');?>">
                                                <input type="hidden"  class="form-control border-primary"  name="OrgGroupTypeUpdateDate" id="OrgGroupTypeUpdateDate" value="<?php echo GetToday('');?>">

                                                <button type="button" class="btn btn-danger  round btn-min-width mr-1 mb-1" id="type-error">ยกเลิก</button>
                                                <button type="button" class="btn btn-success  round btn-min-width mr-1 mb-1"  id="btncreatecf">บันทึก</button>

                                            </div>
                                            </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
              <!-- // Basic form layout section end -->
          </div>
      </div>
  </div>

  <?php include '../include/footer.php'; ?>  
    <script type="text/javascript">
    $( document ).ready(function() {
        
       
 

        $("#btncreatecf").attr('disabled','disabled');
        $("#OrgGroupTypeName").keyup(function(){ 
            var DataSet = {
                table: 'OrgGroupType',
                field: 'OrgGroupTypeName',
                where: {
                    OrgGroupTypeName : $("#OrgGroupTypeName").val(),
                }

             };
            org.checkFieldmore(DataSet,'OrgGroupTypeName');
        });


        $("#btncreatecf").click(function(){ 
            $('#modal_createcf').modal('show');
        });
        $("#btncreate").click(function(){
            org.CreateOrgGroupType();
        });
    });
    </script>