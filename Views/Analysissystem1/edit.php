
  <!-- header -->
  <?php include '../include/header.php'; ?>

  <!-- menu -->
  <?php include '../include/menu.php'; ?>

  <style>
/* ol > li > a {color:#222233;} */
.toggle.ios,
.toggle-on.ios,
.toggle-off.ios {
    border-radius: 20rem;
}

.toggle.ios .toggle-handle {
    border-radius: 20rem;
}


</style>

  <section>

      <div class="app-content content">
          <div class="content-wrapper">
                                            <div class="content-header row">
                                                <div class="content-header-left col-12 mb-2">
                                                    <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
                                                    <h3 class="content-header-title">บันทึกข้อมูลเสนอขอบรรจุกําลังพลของ นขต.ทอ.</h3>
                                                                        <nav aria-label="breadcrumb">
                                                                            <ol class="breadcrumb">
                                                                            <li class="breadcrumb-item"><a
                                                                                        href="../home/index.php">ระบบงานวิเคราะห์ความต้องการกำลังพลประจำปี</a></li>
                                                                                <li class="breadcrumb-item active" aria-current="page">
                                                                                บรรจุกำลังพล</li>
                                                                            </ol>
                                                                        </nav>
                                                </div>
                                            </div>
<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- /////////////////////////////////////////////////บันทึก/แก้ไข เสนอขอบรรจุกําลังพลของ นขต.ทอ./////////////////////////////////////////////////////////////// -->
                                                                    
<div class="shadow p-0 bg-white rounded border mb-2">

    <div class="card collapse-icon accordion-icon-rotate active ">
            <div id="headingCollapse31" class="card-header">
            <a data-toggle="collapse" href="#collapse31" aria-expanded="true" aria-controls="collapse31" class="card-title lead white" style="color:white;" >
            <h6 style="background-color:#0f1733;color:white;line-height:40px;padding-left:10px;">บันทึก/แก้ไข เสนอขอบรรจุกําลังพลของ นขต.ทอ.</h6></a></div>
                <div id="collapse31" role="tabpanel" aria-labelledby="headingCollapse31" class="card-collapse collapse show" aria-expanded="true" >  

        <form>

                    <div class="row">
                            <div class="col-10 m-auto"> 
                                <div class="form-row px-2 m-auto">

                                    
                                            <div class="form-group col-md-6">
                                                <label for="formGroupExampleInput">เลขที่เสนอความต้องการ :</label>
                                                <fieldset disabled>
                                                 <input type="text" id="disabledTextInput" class="form-control text-center" placeholder="322330086500"> </fieldset>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label for="formGroupExampleInput">วันที่บันทึกข้อมูล :</label>
                                                <fieldset disabled>
                                                 <input type="text" id="disabledTextInput" class="form-control text-center" placeholder="16 ม.ค. 2562"> </fieldset>
                                            </div>

                            </div>
                        </div>
                    </div>

        </form>

        </div> <!-- id="headingCollapse31" class="card-header" -->
    </div> <!-- close card collapse-icon accordion-icon-rotate active -->
</div><!-- close shadow -->
      
<!-- ////////////////////////////////////////////ส่วนที่ 1 เสนอขอบรรจุกําลังพลของ นขต.ทอ.//////////////////////////////////////////////////////////////////// -->

                <div class="shadow p-0 bg-white rounded">
                    <div class="card collapse-icon accordion-icon-rotate active">

                        <div id="headingCollapse32" class="card-header">
                        <a data-toggle="collapse" href="#collapse32" aria-expanded="true" aria-controls="collapse32" class="card-title lead white">
                        <h6 style="background-color:#D8E0FA;color:#000;line-height:40px;padding-left:10px;"> ส่วนที่ 1 เสนอขอบรรจุกําลังพลของ นขต.ทอ.</h6></a></div>
                        <div id="collapse32" role="tabpanel" aria-labelledby="headingCollapse32" class="card-collapse collapse show" aria-expanded="true" >  

                        <div class="row">
                            <div class="col-10 m-auto"> 


                                    <div class="row">

                                        <div class="form-group col-md-3">
                                                        <label for="formGroupExampleInput">ปีงบประมาณ :</label>
                                                        <input type="text" class="form-control border-danger" id="formGroupExampleInput" placeholder="2562">
                                        </div>

                                        <div class="form-group col-md-3">
                                                <label for="formGroupExampleInput">สังกัดที่ขอบรรจุ:</label>
                                                <select class="form-control border-danger" id="exampleFormControlSelect1" >
                                                    <option>คปอ.</option>
                                                    <option>คปอ.</option>
                                                </select>
                                        </div>

                                        <div class="form-group col-md-6">
                                                <label for="formGroupExampleInput2">หน่วยงานที่ขอบรรจุ:</label>
                                                <select class="form-control border-danger" id="exampleFormControlSelect1">
                                                    <option>กคว.คปอ.</option>
                                                    <option>กคว.คปอ.</option>
                                                </select>
                                        </div>

                                        <div class="w-100"></div>

                                            <div class="form-group col-md-6">
                                                    <label for="formGroupExampleInput">กําลังพลที่ต้องการ:</label>
                                                    <select class="form-control border-danger" id="exampleFormControlSelect1" >
                                                        <option>ประทวน</option>
                                                        <option>สัญญาบัตร</option>
                                                    </select>
                                            </div>

                                        <div class="form-group col-md-6">
                                                <h6 class="py-1">เพศ:</h6>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="exampleRadios0" id="exampleRadios1" value="option1" checked>
                                                        <label class="form-check-label" for="exampleRadios1">
                                                            ชาย/หญิง
                                                        </label>
                                                    </div>

                                                    <div class="form-check form-check-inline">
                                                
                                                        <input class="form-check-input m-1" type="radio" name="exampleRadios0" id="exampleRadios2" value="option2">
                                                    
                                                        <label class="form-check-label m-1" for="exampleRadios2">
                                                            ชาย
                                                        </label>
                                                    </div>

                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="exampleRadios0" id="exampleRadios2" value="option3">
                                                        <label class="form-check-label" for="exampleRadios3">
                                                            หญิง
                                                        </label>
                                                    </div>

                                        </div>

                                        <div class="w-100"></div>

                                        <div class="form-group col-12">
                                                        <label for="formGroupExampleInput">ตําแหน่งที่ขอบรรจุ:</label>
                                                        <input type="text" class="form-control border-danger" id="formGroupExampleInput" placeholder="นายทหารค้นหาและช่วยชีวิต กคว.คปอ.">
                                        </div>

                                </div>

                                <div class="row">
                                    <div class="col-lg-8">
                                    <div class="row">
                                                            <div class="form-group col-md-3">
                                                                <label for="formGroupExampleInput">อัตราอนุมัติ:</label>
                                                                <input type="text" class="form-control" id="formGroupExampleInput" placeholder="4">
                                                            </div>
                                                            <div class="form-group col-md-3">
                                                                <label for="formGroupExampleInput">กรอบการบรรจุ:</label>
                                                                <input type="text" class="form-control" id="formGroupExampleInput" placeholder="4">
                                                            </div>
                                                            <div class="form-group col-md-3">
                                                                <label for="formGroupExampleInput">จํานวนที่บรรจุ:</label>
                                                                <input type="text" class="form-control" id="formGroupExampleInput" placeholder="3">
                                                            </div>

                                                            <div class="form-group col-md-3">
                                                                <label for="formGroupExampleInput">อัตรา:</label>
                                                                <select class="form-control" id="exampleFormControlSelect1">
                                                                    <option>ร.ต.</option>
                                                                    <option>ร.ต.</option>
                                                                </select>
                                                             </div>
                                </div>
                                    </div>


                                    <div class="col-lg-4">
                                            
                                                    <table class="table table-striped table-border table-hover">
                                                    <thead style="background-color:#0f1733;color:white">
                                                        <tr>
                                                        <th scope="col">ลชทอ. หน้าที่</th>
                                                        <th scope="col">เหล่า</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                        <td>-</td>
                                                        <td>นบ.</td>
                                                        </tr>
                                                        <tr>
                                                        <td>-</td>
                                                        <td>นบ.</td>
                                                        </tr>
                                                    </tbody>
                                                    </table>
                                    </div>

                            </div>

                            <div class="row">
                                    <div class="col-lg-12">
                                    <table class="table table-striped table-border table-hover">
                                    <thead class="text-center">
                                <tr class="justify-content-center"style="background-color:#0f1733; color:whitesmoke;">
                                                        <th scope="col">เลือก</th>
                                                        <th scope="col">ลำดับ</th>
                                                        <th scope="col">ยศ</th>
                                                        <th scope="col">ชื่อ-นามสกุล</th>
                                                        <th scope="col">สาเหตุในการขอบรรจุกำลังพล</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="text-center">
                                                        <tr class="justify-content-center">
                                                            <td><input type="checkbox" class="checkAll" /></td>
                                                            <td>1</td>
                                                            <td>ร.อ.</td>
                                                            <td>คมสัน  ทองสีมา</td> 
                                                            <td>
                                                            <select class="form-control" id="exampleFormControlSelect1">
                                                                    <option>สูญเสียกำลังพล</option>
                                                                    <option>เตรียมกำลังพลไว้ทดแทนผู้เกษียญอายุราชการ</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><input type="checkbox" class="checkAll" /></td>
                                                            <td>1</td>
                                                            <td>ร.อ.</td>
                                                            <td>คมสัน  ทองสีมา</td> 
                                                            <td>
                                                            <select class="form-control" id="exampleFormControlSelect1">
                                                                    <option>สูญเสียกำลังพล</option>
                                                                    <option>เตรียมกำลังพลไว้ทดแทนผู้เกษียญอายุราชการ</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        
                                                    </tbody>
                                                    </table>

                                    </div>
                            </div>
                            <hr>

                                                <div class="form-group col-md-3">
                                                                <label for="formGroupExampleInput">จำนวนที่ต้องบรรจุ:</label>
                                                                <fieldset disabled>
                                                 <input type="text" id="disabledTextInput" class="form-control text-center" placeholder="4"> </fieldset>
                                                </div>

                        
                                                
                            
                        </div>


                    </div> <!-- close card collapse-icon accordion-icon-rotate active -->
                    </div> <!-- close shadow -->

        </div>
    </div>
</section>

<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

<!-- ///////////////////////////////////////////ส่วนที่ 2 เสนอขอบรรจุกําลังพลของ นขต.ทอ.////////////////////////////////////////////////////////////////////// -->
<section>
    <div class="app-content content">
        <div class="content-wrapper">

                <div class="shadow p-0 bg-white rounded mt-1">

                    <div class="card collapse-icon accordion-icon-rotate active ">
                            <div id="headingCollapse33" class="card-header">
                            <a data-toggle="collapse" href="#collapse33" aria-expanded="true" aria-controls="collapse33" class="card-title lead white" style="color:white;" >
                            <h6 style="background-color:#D8E0FA;color:#000;line-height:40px;padding-left:10px;"> ส่วนที่ 2 เสนอขอบรรจุกําลังพลของ นขต.ทอ.</h6></a></div>
                                <div id="collapse33" role="tabpanel" aria-labelledby="headingCollapse33" class="card-collapse collapse show" aria-expanded="true" >  

                                <div class="m-1">
                                    
                                <div> ลำดับ ยศ ชื่อ-สกุล </div>
                                <!-- ///////////////////////////  -->

                                <div class="card collapse-icon accordion-icon-rotate active ">
                                 <div id="headingCollapse40" class="card-header" style="color:#fff;">
                                 <a data-toggle="collapse" href="#collapse40" aria-expanded="true" aria-controls="collapse40" class="card-title lead white" style="color:#fff;" >
                                <h6 style="background-color:#0f1733;color:#fff;line-height:40px;padding-left:10px;"> 1.  ร.อ.คมสัน  ทองสีมา</h6></a></div>
                                <div id="collapse40" role="tabpanel" aria-labelledby="headingCollapse40" class="card-collapse collapse show"  aria-expanded="true" >  

                                    <div class="row">
                                    
                                        <div class="form-group col-md-6 px-5">
                                                   <h6 class="py-1" > เลือกประเภทกําลังพล:</h6>

                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="exampleRadios0" id="exampleRadios1" value="option1" checked>
                                                        <label class="form-check-label" for="exampleRadios1">
                                                        กําลังพลตามข้อผูกพัน
                                                        </label>
                                                    </div>

                                                    <div class="form-check form-check-inline">
                                                
                                                        <input class="form-check-input m-1" type="radio" name="exampleRadios0" id="exampleRadios2" value="option2">
                                                    
                                                        <label class="form-check-label m-1" for="exampleRadios2">
                                                        กําลังพลพิเศษ
                                                        </label>
                                                    </div>

                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="exampleRadios0" id="exampleRadios2" value="option3">
                                                        <label class="form-check-label" for="exampleRadios3">
                                                        เลื่อนฐานะ
                                                        </label>
                                                    </div>

                                            </div>

                                            <div class="form-group col-md-6">
                                                    <h6 class="py-1">คุณวุฒิ:</h6>

                                                        
                                                    <select class="form-control" id="exampleFormControlSelect1" >
                                                            <option>นนอ.</option>
                                                            <option>นนอ.</option>
                                                        </select>
                                                        

                                            </div>

                                        <div class="float-left mx-2">
                                        <a href="#" class="btn btn-social btn-min-width mb-1 mr-1 round bg-hr">
                                         <span class="fa fa-plus-circle"> </span> เพิ่ม</a> </div>
                                    
                                        
                                        
                                        <div class="table-responsive mx-2">
                                        <table class="table table-striped table-border table-hover">
                                            <thead class="text-center">
                                                <tr class="justify-content-center"style="background-color:#0f1733; color:whitesmoke;">
                                                      <th></th>
                                                      <th>ลำดับที่</th>
                                                      <th>สายวิชา</th>
                                                      <th>ประเภทสายวิทยาการ</th>
                                                  </tr>
                                              </thead>

                                              <tbody class="text-center">
                                                <tr>
                                                        <td>
                                                            <a href="./delete.php"><i class="la la-trash-o"
                                                                    style="color:#0f1733;"></i></a>
                                                        </td>
                                                        <td>1</td>
                                                        <td>
                                                            <select class="form-control" id="exampleFormControlSelect1" >
                                                                <option>วิศวกรรมอากาศยานและการบิน</option>
                                                                <option>วิศวกรรมอากาศยานและการบิน</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                                <select class="form-control" id="exampleFormControlSelect1" >
                                                                    <option>ยก.ทอ.</option>
                                                                    <option>ยก.ทอ.</option>
                                                                </select>
                                                        </td>
                                                  </tr>
                                              </tbody>
                                             
                                                </table>
                                                </div>
                                    

                                    </div>

                                    <!-- //////////////////////////////////////////////// -->
                                    
                                    <div class="mx-2"> 
                                            
                                            <div class="form-group">
                                            <label for="exampleFormControlTextarea1">คุณสมบัติเฉพาะ :</label>
                                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                                            </div>
                                         
                                         
                                    </div>

                                    <!-- //////////////////////////////////////////////// -->
                                    <div style="background-color:#ccc; line-height:50px;"> 
                                    <div class="row px-1">
                                            <div class="col-4">
                                                สายวิทยาการ :
                                                    <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="exampleRadios0" id="exampleRadios2" value="option3">
                                                    <label class="form-check-label" for="exampleRadios3">อนุมัติ </label></div>

                                                    <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="exampleRadios0" id="exampleRadios2" value="option3">
                                                    <label class="form-check-label" for="exampleRadios3">ไม่อนุมัติ </label></div>

                                            </div>
                                         

                                         <div class="col-4">
                                                งานวิเคราะห์:
                                                    <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="exampleRadios0" id="exampleRadios2" value="option3">
                                                    <label class="form-check-label" for="exampleRadios3">อนุมัติ </label></div>

                                                    <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="exampleRadios0" id="exampleRadios2" value="option3">
                                                    <label class="form-check-label" for="exampleRadios3">ไม่อนุมัติ </label>
                                                    </div>
                                                    
                                        </div>

                                        

                                         <div class="col-4">
                                                งานแผน :
                                                    <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="exampleRadios0" id="exampleRadios2" value="option3">
                                                    <label class="form-check-label" for="exampleRadios3">อนุมัติ </label></div>

                                                    <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="exampleRadios0" id="exampleRadios2" value="option3">
                                                    <label class="form-check-label" for="exampleRadios3">ไม่อนุมัติ </label>
                                                    </div>
                                                    
                                        </div>

                                    </div>
                                    </div>
                                  



                            
                            </div>
                        </div> <!-- id="headingCollapse31" class="card-header" -->
                    </div> <!-- close card collapse-icon accordion-icon-rotate active -->


                        </div> <!-- id="headingCollapse31" class="card-header" -->
                    </div> <!-- close card collapse-icon accordion-icon-rotate active -->
                </div><!-- close shadow -->     

<!-- ///////////////////////////////////////////ส่วนที่ 3 เสนอขอบรรจุกําลังพลของ นขต.ทอ.////////////////////////////////////////////////////////////////////// -->


                <div class="shadow p-0 bg-white rounded mt-1">

                    <div class="card collapse-icon accordion-icon-rotate active ">
                            <div id="headingCollapse34" class="card-header">
                            <a data-toggle="collapse" href="#collapse34" aria-expanded="true" aria-controls="collapse34" class="card-title lead white" style="color:white;" >
                            <h6 style="background-color:#D8E0FA;color:#000;line-height:40px;padding-left:10px;"> ส่วนที่ 3 เสนอขอบรรจุกําลังพลของ นขต.ทอ.</h6></a></div>
                                <div id="collapse34" role="tabpanel" aria-labelledby="headingCollapse34" class="card-collapse collapse show" aria-expanded="true" >  

                                    <form>

                                                <div class="row">
                                                        <div class="col-10 m-auto"> 

                                                        <div class="row">
                                    <div class="col-lg-12">
                                    <table class="table table-striped table-border table-hover">
                                                    <thead class="text-center">
                                                        <tr style="background-color:#0f1733; color:whitesmoke;">
                                                        <th scope="col">ประเภท</th>
                                                        <th scope="col">บรรจุในหน่วย</th>
                                                        <th scope="col">มาช่วยราชการ</th>
                                                        <th scope="col">ไปช่วยราชการ</th>
                                                        <th scope="col">คงเหลือปฏิบัติงานจริง</th>
                                                        </tr>
                                                    </thead>
                                            <tbody class="text-center">
                                                        <tr>
                                                            <td>สัญญาบัตร</td>
                                                            <td><input type="text" class="form-control border-danger text-center" id="formGroupExampleInput"  placeholder="12"></td>
                                                            <td><input type="text" class="form-control border-danger text-center" id="formGroupExampleInput"  placeholder="2"></td>
                                                            <td><input type="text" class="form-control border-danger text-center" id="formGroupExampleInput"  placeholder="2"></td>
                                                            <td><fieldset disabled>
                                                                <input type="text" id="disabledTextInput" class="form-control text-center" placeholder="12"> </fieldset></td>
                                                        </tr>

                                                        <tr>
                                                            <td>ประทวน</td>
                                                            <td><input type="text" class="form-control border-danger text-center" id="formGroupExampleInput"  placeholder="12"></td>
                                                            <td><input type="text" class="form-control border-danger text-center" id="formGroupExampleInput"  placeholder="2"></td>
                                                            <td><input type="text" class="form-control border-danger text-center" id="formGroupExampleInput"  placeholder="2"></td>
                                                            <td><fieldset disabled>
                                                                <input type="text" id="disabledTextInput" class="form-control text-center" placeholder="12"> </fieldset></td>
                                                        </tr>

                                                        <tr>
                                                            <td>พนักงานราชการ</td>
                                                            <td><input type="text" class="form-control border-danger text-center" id="formGroupExampleInput"  placeholder="12"></td>
                                                            <td><input type="text" class="form-control border-danger text-center" id="formGroupExampleInput"  placeholder="2"></td>
                                                            <td><input type="text" class="form-control border-danger text-center" id="formGroupExampleInput"  placeholder="2"></td>
                                                            <td><fieldset disabled>
                                                                <input type="text" id="disabledTextInput" class="form-control text-center" placeholder="12"> </fieldset></td>
                                                        </tr>

                                                        <tr>
                                                            <td>ลูกจ้างประจำ</td>
                                                            <td><input type="text" class="form-control border-danger text-center" id="formGroupExampleInput"  placeholder="12"></td>
                                                            <td><input type="text" class="form-control border-danger text-center" id="formGroupExampleInput"  placeholder="2"></td>
                                                            <td><input type="text" class="form-control border-danger text-center" id="formGroupExampleInput"  placeholder="2"></td>
                                                            <td><fieldset disabled>
                                                                <input type="text" id="disabledTextInput" class="form-control text-center" placeholder="12"> </fieldset></td>
                                                        </tr>
                                                        
                                                        
                                                    </tbody>
                                        </table>

                                    </div>
                            </div>
                
                                                        </div>
                                                   
                                                </div>

                                    </form>

                        </div> <!-- id="headingCollapse31" class="card-header" -->
                    </div> <!-- close card collapse-icon accordion-icon-rotate active -->
                </div><!-- close shadow -->   

<!-- ///////////////////////////////////////////ส่วนที่ 4 สาเหตุในการขอบรรจุกําลังพลเพิ่มเติม////////////////////////////////////////////////////////////////////// -->


<div class="shadow p-0 bg-white rounded mt-1">

<div class="card collapse-icon accordion-icon-rotate active ">
        <div id="headingCollapse35" class="card-header">
        <a data-toggle="collapse" href="#collapse35" aria-expanded="true" aria-controls="collapse35" class="card-title lead white" style="color:white;" >
        <h6 style="background-color:#D8E0FA;color:#000;line-height:40px;padding-left:10px;">ส่วนที่ 4 สาเหตุในการขอบรรจุกําลังพลเพิ่มเติม</h6></a></div>
            <div id="collapse35" role="tabpanel" aria-labelledby="headingCollapse35" class="card-collapse collapse show" aria-expanded="true" >  

                <form>

                            <div class="row">
                                    <div class="col-10 m-auto"> 
                                        <div class="form-row px-2 m-auto">

                                        <div class="form-group col-md-8">
                                            <label for="exampleFormControlTextarea1">สาเหตุในการขอบรรจุกําลังพลเพิ่มเติม :</label>
                                            <textarea class="form-control border-danger" id="exampleFormControlTextarea1" rows="3"></textarea>
                                        </div>


                                    </div>
                                </div>
                            </div>

                </form>

    </div> <!-- id="headingCollapse31" class="card-header" -->
</div> <!-- close card collapse-icon accordion-icon-rotate active -->
</div><!-- close shadow -->   

<!-- ///////////////////////////////////////////ส่วนที่ 5 ภารกิจและหน้าที่////////////////////////////////////////////////////////////////////// -->


<div class="shadow p-0 bg-white rounded mt-1">

<div class="card collapse-icon accordion-icon-rotate active ">
        <div id="headingCollapse36" class="card-header">
        <a data-toggle="collapse" href="#collapse36" aria-expanded="true" aria-controls="collapse36" class="card-title lead white" style="color:white;" >
        <h6 style="background-color:#D8E0FA;color:#000;line-height:40px;padding-left:10px;">ส่วนที่ 5 ภารกิจและหน้าที่</h6></a></div>
            <div id="collapse36" role="tabpanel" aria-labelledby="headingCollapse36" class="card-collapse collapse show" aria-expanded="true" >  

                <form>

                            <div class="row">
                                    <div class="col-10 m-auto"> 
                                        <div class="form-row px-2 m-auto">

                                        <div class="form-group col-md-8">
                                            <label for="exampleFormControlTextarea1">ภารกิจของหน่วยงาน :</label>
                                            <textarea class="form-control border-danger" id="exampleFormControlTextarea1" rows="3"></textarea>
                                        </div>

                                        <div class="form-group col-md-8">
                                            <label for="exampleFormControlTextarea1">หน้าที่ของตําแหน่ง :</label>
                                            <textarea class="form-control border-danger" id="exampleFormControlTextarea1" rows="3"></textarea>
                                        </div>

                                        <div class="form-group col-md-8">
                                            <label for="exampleFormControlTextarea1">ปริมาณงาน 3 ปี ย้อนหลัง (ตามปีงบประมาณ) :</label>
                                            <textarea class="form-control border-danger" id="exampleFormControlTextarea1" rows="3"></textarea>
                                        </div>

                                            
                                    </div>
                                </div>
                            </div>

                </form>

    </div> <!-- id="headingCollapse31" class="card-header" -->
</div> <!-- close card collapse-icon accordion-icon-rotate active -->
</div><!-- close shadow -->   




         </div> <!-- close app-content content -->  
     </div> <!-- close content-wrapper -->  
</section> <!-- close /section -->  

<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->




    <!-- <div class="row mt-2"> 
            <div class=" col-12 m-auto">
                            <div class="col-12 d-flex justify-content-center"> 
                                <a href="#" class="btn btn-social btn-min-width mb-1 round" role="button" style="background-color:green; color:white;">
                                    <span class="la la-save"style="color:white; font-weight: bold;font-size: 18px"></span>บันทึกเลขที่ร่างส่ง</a> 
                                <a href="#" class="btn btn-social btn-min-width mb-1 round" role="button" style="background-color:green; color:white;">
                                    <span class="la la-save"style="color:white; font-weight: bold;font-size: 18px"></span>บันทึกเลขที่ร่างส่งตามเงื่อนไขค้นหา</a> 
                        
                            </div>
                        
            </div>
     </div> -->


<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
 

<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
                                         
                                
  <script src="http://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
      crossorigin="anonymous"></script>

  <script type="text/javascript">
$(document).ready(function() {
    console.log("ready");
    change_autorefreshdiv();
});

function change_autorefreshdiv() {
    // $('#prefixPage').addClass('active');
}

function toggle(source) {
    var checkboxes = document.querySelectorAll('.checkAll');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i] != source)
            checkboxes[i].checked = source.checked;
    }
}
  </script>



  <!-- footer -->
  <?php include '../include/footer.php'; ?>