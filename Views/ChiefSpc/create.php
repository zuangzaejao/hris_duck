<?php 

require_once '../../config.php';
require_once '../../Model/Ducklab/duck.class.php'; 
require_once '../../Model/Ducklab/contents.class.php'; 
require_once '../../Model/Ducklab/org.class.php'; 
require_once '../../Model/Ducklab/hrt.class.php'; 
require_once '../../Model/Ducklab/func.php'; 
require_once '../include/header.php'; 
 
  
$menu1 ="ORGSTRUC" ;
$menu2 ="ORGSTRUCDATA";
$menu3 ="CHIEFSPC"; 
   

  $clshrt = new HrtClass();

 
include_once '../include/menu.php'; 
include_once '../include/modelOnload.php' ;
?>

  <div class="app-content content">
      <div class="content-wrapper">
          <div class="content-header row">
              <div class="content-header-left col-md-6 col-12 mb-2">
                  <h3 class="content-header-title">เพิ่มสายวิทยาการ</h3>
                  <div class="row breadcrumbs-top">

                  </div>
              </div>

          </div>
          <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="./index.php">ระบบงานโครงสร้างอัตรากำลังพล</a></li>
                  <li class="breadcrumb-item"><a href="./">ข้อมูลทั่วไป</a></li>
                  <li class="breadcrumb-item active" aria-current="page">เพิ่มสายวิทยาการ</li>
              </ol>
          </nav>
          <div class="content-body">
              <!-- Basic form layout section start -->
              <section id="horizontal-form-layouts">

                  <div class="row">
                      <div class="col-md-12">
                          <div class="card">
                              <div class="card-header">
                                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                  <div class="heading-elements">
                                      <ul class="list-inline mb-0">
                                          <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                          <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                          <li><a data-action="close"><i class="ft-x"></i></a></li>
                                      </ul>
                                  </div>
                              </div>
                              <div class="card-content collpase show">
                                  <div class="card-body">
                             
                                      <form class="form form-horizontal">
                                          <div class="form-body">
                                          <div class="row">
                                                 <div class="col-md-6">
                                                      <label class="col-md-6 label-control" for="HrtChiefSpcID">รหัสสายวิทยาการ</label>
                                                      <div class="col-md-12">
                                                          <div class="position-relative ">
                                                              <input type="text" id="HrtChiefSpcID" class="form-control border-primary"  name="HrtChiefSpcID">
                                                          </div>  
                                                     </div>
                                                  </div>
                                                <div class="col-md-6">
                                                      <label class="col-md-6 label-control" for="HrtPersonType"> ประเภทกำลังพล </label>
                                                      <div class="col-md-12">
                                                          <div class="position-relative">
                                                                <select name="HrtPersonType" id="HrtPersonType" class="  form-control border-primary">
                                                                </select> 
                                                          </div>
                                                      </div>
                                                </div>
                                          </div>
                                          <br>
                                          <div class="row">  

                                          <div class="col-md-6">
                                                      <label class="col-md-6 label-control" for="HrtChiefSpcName">ชื่อสายวิทยาการ</label>
                                                      <div class="col-md-12">
                                                          <div class="position-relative ">
                                                              <input type="text" id="HrtChiefSpcName" class="form-control border-primary"  name="HrtChiefSpcName">
                                                          </div>  
                                                     </div>
                                                  </div>
                                                 <div class="col-md-6">
                                                      <label class="col-md-6 label-control" for="HrtChiefSpcNameAbbr">ชื่อย่อสายวิทยาการ</label>
                                                      <div class="col-md-12">
                                                          <div class="position-relative">
                                                              <input type="text" id="HrtChiefSpcNameAbbr" class="form-control border-primary" name="HrtChiefSpcNameAbbr">
                                                          </div>
                                                      </div>
                                                  </div>
                                              
                                                  
                                              </div>
                                        
                                                  
                                              <br>
                                              <div class="row">
                                                  <div class="col-md-12">
                                                      <br>
                                                      <label class="col-md-1 label-control" for="HrtChiefSpcActive" style="padding-right:0px;">สถานะ</label>
                                                      <input type="checkbox" id="HrtChiefSpcActive" data-toggle="toggle" data-style="ios" data-on="ใช้งาน" data-off="ยกเลิก" data-onstyle="success" data-offstyle="danger" data-size="sm">
                                                  </div>
                                              </div>
                                              <div class="form-actions text-center" > 
                                                   
                                                   <input type="hidden"  class="form-control border-primary"  name="HrtChiefSpcCreateBy" id="HrtChiefSpcCreateBy" value="0">
                                                   <input type="hidden"  class="form-control border-primary"  name="HrtChiefSpcCreateDate" id="HrtChiefSpcCreateDate" value="<?php echo GetToday('');?>">
    
                                                     <button type="button" class="btn btn-danger  round btn-min-width mr-1 mb-1"  >ยกเลิก</button>
                                                     <button type="button" class="btn btn-success  round btn-min-width mr-1 mb-1" id="btncreatecf" >บันทึก</button>
   
                                                </div>
                                          </div>
                                           
                                      </form>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                
              </section>
              <!-- // Basic form layout section end -->
          </div>
      </div>
  </div>

  <?php include '../include/footer.php'; ?> 

  <script type="text/javascript">
       
         
       $(document).ready(function() {

          hrt.LoadPersonTypeSel('','HrtPersonType');
          // $("#btncreatecf").attr('disabled','disabled');

          $("#HrtChiefSpcID").keyup(function(){ 
            var DataSet = {
                table: 'HrtChiefSpc',
                field: 'HrtChiefSpcID',
                where: {
                    HrtChiefSpcID : $("#HrtChiefSpcID").val(),
                }

             };
            hrt.checkFieldmore(DataSet,'HrtChiefSpcID');
        });
        // $("#btncreatecf").attr('disabled','disabled');

           $("#HrtChiefSpcName").keyup(function(){ 
            var DataSet = {
                table: 'HrtChiefSpc',
                field: 'HrtChiefSpcName',
                where: {
                    HrtChiefSpcName : $("#HrtChiefSpcName").val(),
                }

             };
            hrt.checkFieldmore(DataSet,'HrtChiefSpcName');
        });
        // $("#btncreate").attr('disabled','disabled');

        $("#HrtChiefSpcNameAbbr").keyup(function(){ 
            var DataSet = {
                table: 'HrtChiefSpc',
                field: 'HrtChiefSpcNameAbbr',
                where: {
                    HrtChiefSpcNameAbbr : $("#HrtChiefSpcNameAbbr").val(),
                }

             };
            hrt.checkFieldmore(DataSet,'HrtChiefSpcNameAbbr');
        });

        
        $("#btncreatecf").click(function(){   
            $('#modal_createcf').modal('show');
        });
        
        $("#btncreate").click(function(){
            hrt.CreateChiefSpc(); 
        });
            
       });
       
    </script> 
    <?php/*
    
    hrt.LoadPersonTypeSel('','HrtPersonType');
        ////
        $("#btncreatecf").click(function(){   
            $('#modal_createcf').modal('show');
        });
        $("#btncreate").click(function(){
            hrt.CreateChiefSpc(); 
        });

    */?>