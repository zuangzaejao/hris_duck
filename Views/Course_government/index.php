  <!-- header -->
  <?php include '../include/header.php'; ?>

  <!-- menu -->
  <?php include '../include/menu.php'; ?>

  <style>
      /* ol > li > a {color:#222233;} */
      .toggle.ios,
      .toggle-on.ios,
      .toggle-off.ios {
          border-radius: 20rem;
      }

      .toggle.ios .toggle-handle {
          border-radius: 20rem;
      }
  </style>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <!-- select2 -->
  <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/forms/selects/select2.min.css">
  <section>
      <div class="app-content content">
          <div class="content-wrapper">
              <div class="content-header row">
                  <div class="content-header-left col-12 mb-2">
                      <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
                      <h3 class="content-header-title">บันทึกขึ้นทะเบียนกองประจำการ</h3>
                  </div>
              </div>
              <div class="content-body">
                  <!-- Bootstrap 3 table -->
                  <section id="bootstrap3">
                      <div class="row">
                          <div class="col-12">
                              <div class="card">
                                  <div class="card-content collapse show">
                                      <div class="card-body card-dashboard">

                                          <nav aria-label="breadcrumb">
                                              <ol class="breadcrumb">
                                                  <li class="breadcrumb-item"><a href="../home/index.php">ระบบงานสัสดี</a></li>
                                                  <li class="breadcrumb-item active" aria-current="page">บันทึกขึ้นทะเบียนกองประจำการ
                                                  </li>


                                              </ol>
                                          </nav>
                                          <div id="how-to" class="card">
                                              <!-- card -->
                                              <div class="card-header" style="background-color:#0f1733;padding:1.0rem 1.0rem">
                                                  <h6 class="card-title">
                                                      <font color="White">ค้นหา
                                                  </h6>
                                                  </font>
                                                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                                  <div class="heading-elements">
                                                      <ul class="list-inline mb-0">
                                                          <li><a data-action="collapse"><i class="ft-minus"></i></a>
                                                          </li>
                                                      </ul>
                                                  </div>
                                              </div>
                                          </div> <!-- card -->

                           <div class="container">

                                              <div class="card-content">
                                                  <div class="card-body">


                                                    <div class="tab-content px-1 pt-1">
                                                          <div role="tabpanel" class="tab-pane active" id="tab11" aria-expanded="true" aria-labelledby="base-tab11">

                                                              <div class="card collapse-icon accordion-icon-rotate active">
                                                                  <div id="headingCollapse31" class="card-header bg-success">
                                                                      <a data-toggle="collapse" href="#collapse31" aria-expanded="true" aria-controls="collapse31" class="card-title lead white">
                                                                          <h6 style="margin-left:5px;">ส่วนที่1 บันทึก/แก้ไข การเปลี่ยนชื่อ/สกุล</h6>
                                                                      </a>
                                                                  </div>



                                                                  <div class="tab-content px-1 pt-1">
                                                                                  <div role="tabpanel55"
                                                                                      class="tab-pane active"
                                                                                      id="tab1155" aria-expanded="true"
                                                                                      aria-labelledby="base-tab1155">

                                                                                      <div
                                                                                          class="card collapse-icon accordion-icon-rotate active">
                                                                                          
                                                                                          <div id="collapse55"
                                                                                              role="tabpanel55"
                                                                                              aria-labelledby="headingCollapse55"
                                                                                              class="card-collapse collapse show"
                                                                                              aria-expanded="true">
                                                                                              <div class="card-content">
                                                                                                  <div
                                                                                                      class="card-body">

                                                                                                      <div class="col-md-12 col-sm-12">
                                                                                                    <div class="card-body">
                                                                                                    ประเภท :
                                                                                                        <div class="card-content">
                                                                                                            <div class="card-body">
                                                                                                                <div
                                                                                                                    class="d-inline-block custom-control custom-radio mr-1">
                                                                                                                    <input
                                                                                                                        type="radio"
                                                                                                                        class="custom-control-input"
                                                                                                                        name="colorRadio"
                                                                                                                        id="radio3">
                                                                                                                    <label
                                                                                                                        class="custom-control-label"
                                                                                                                        for="radio3">ข้าราชการ</label>
                                                                                                                </div>
                                                                                                                <div
                                                                                                                    class="d-inline-block custom-control custom-radio mr-1">
                                                                                                                    <input
                                                                                                                        type="radio"
                                                                                                                        class="custom-control-input"
                                                                                                                        name="colorRadio"
                                                                                                                        id="radio4"
                                                                                                                        checked>
                                                                                                                    <label
                                                                                                                        class="custom-control-label"
                                                                                                                        for="radio4"
                                                                                                                        checked>นักเรียนในทอ.</label>
                                                                                                                </div>   
                                                                                                          </div>     

                                                                                                          <!-- --- -->
                                                                                                      <div
                                                                                                          class="row match-height">


                                                                                                                                                                                                                  


                                                                                                          <!-- -ยศ -->
                                                                                                          <div
                                                                                                              class=" col-md-6">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      ยศ
                                                                                                                      :
                                                                                                                      <select
                                                                                                          class="select2 form-control"
                                                                                                          style="width: 100%;">
                                                                                                          <optgroup
                                                                                                              label="สัญญาบัตร">
                                                                                                              <option
                                                                                                                  value="AK">
                                                                                                                  เลือก
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  พลอากาศเอก/พล.อ.อ.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  พลอากาศโท/พล.อ.ท.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  พลอากาศตรี/พล.อ.ต.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  พลอากาศจัตวา/พล.อ.จ.(ยกเลิกแล้ว)
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  นาวาอากาศเอก/น.อ.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  นาวาอากาศโท/น.ท.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  นาวาอากาศตรี/น.ต.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  เรืออากาศเอก/ร.อ.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  เรืออากาศโท/ร.ท.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  เรืออากาศตรี/ร.ต.
                                                                                                              </option>
                                                                                                          </optgroup>
                                                                                                          <optgroup
                                                                                                              label="ชั้นประทวน">
                                                                                                              <option
                                                                                                                  value="CA">
                                                                                                                  พันจ่าอากาศเอก
                                                                                                                  พิเศษ/พ.อ.อ.(พ.)
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="NV">
                                                                                                                  พันจ่าอากาศเอก/พ.อ.อ.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="OR">
                                                                                                                  พันจ่าอากาศโท/พ.อ.ท
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="WA">
                                                                                                                  พันจ่าอากาศตรี/พ.อ.ต
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="NV">
                                                                                                                  จ่าอากาศเอก/จ.อ.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="OR">
                                                                                                                  จ่าอากาศโท/จ.ท.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="WA">
                                                                                                                  จ่าอากาศตรี/จ.ต.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="WA">
                                                                                                                  พลทหาร/พลฯ
                                                                                                              </option>
                                                                                                          </optgroup>
                                                                                                      </select>

                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>
                                                                                                          <!-- -ชื่อ - สกุล -->
                                                                                                          <div
                                                                                                              class=" col-md-6">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      ชื่อ - สกุล
                                                                                                                      :
                                                                                                                      <select
                                                                                                          class="select2 form-control"
                                                                                                          style="width: 100%;">
                                                                                                          <optgroup
                                                                                                              label="ชื่อ - สกุล">
                                                                                                              <option
                                                                                                                  value="AK">
                                                                                                                  เลือก
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  พลอากาศเอก/พล.อ.อ.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  พลอากาศโท/พล.อ.ท.
                                                                                                              </option>                                                                                                              
                                                                                                          </optgroup>
                                                                                                      </select>

                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>


                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      ประเภทกำลังประทวน
                                                                                                                      :
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">

                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>

                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">

                                                                                                                  <div
                                                                                                                      class="input-group">
                                                                                                                     วัน/เดือน/ปี เกิด 
                                                                                                                     
                                                                                                                    
                                                                                                                      
                                                                                                                      <input
                                                                                                                          type="text"
                                                                                                                          class="form-control pickadate-disable-dates"
                                                                                                                          placeholder="25 กรกฏาคม 2562"
                                                                                                                          aria-describedby="button-addon4">
                                                                                                                      <div
                                                                                                                          class="input-group-append">
                                                                                                                          <button
                                                                                                                              class="btn btn-primary"
                                                                                                                              type="button"
                                                                                                                              style=" padding-bottom: 1px; padding-top: 1px;"><i
                                                                                                                                  class="la la-calendar-o"></i></button>
                                                                                                                      </div>
                                                                                                                  </div>

                                                                                                              </div>
                                                                                                          </div>

                                                                                                          <div
                                                                                                              class="col-md-3">
                                                                                                              <div
                                                                                                                  class="card-body ">
                                                                                                                 โทรศัพท์ :
                                                                                                                  <input
                                                                                                                      class="input form-control"
                                                                                                                      style="width: 100%;"
                                                                                                                      placeholder=" ">
                                                                                                              </div>

                                                                                                          </div>

                                                                                                          <!-- -นับถือศาสนา -->
                                                                                                          <div
                                                                                                              class=" col-md-3">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      นับถือศาสนา
                                                                                                                      :
                                                                                                                      <select
                                                                                                                        class="select2 form-control"
                                                                                                                        style="width: 100%;">
                                                                                                                        <optgroup
                                                                                                                            label="นับถือศาสนา">
                                                                                                                            <option
                                                                                                                                value="AK">
                                                                                                                                เลือก
                                                                                                                            </option>
                                                                                                                            <option
                                                                                                                                value="HI">
                                                                                                                                พุทธ
                                                                                                                            </option>
                                                                                                                            <option
                                                                                                                                value="HI">
                                                                                                                                คริสเตียน
                                                                                                                            </option>    
                                                                                                                            <option
                                                                                                                                value="HI">
                                                                                                                                อิสลาม
                                                                                                                            </option>                                                                                                           
                                                                                                                        </optgroup>
                                                                                                                    </select>

                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>

                                                                                                          <!-- -อาชีพ -->
                                                                                                          <div
                                                                                                              class=" col-md-6">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      อาชีพ
                                                                                                                      :
                                                                                                                      <select
                                                                                                                        class="select2 form-control"
                                                                                                                        style="width: 100%;">
                                                                                                                        <optgroup
                                                                                                                            label="อาชีพ">
                                                                                                                            <option
                                                                                                                                value="AK">
                                                                                                                                เลือก
                                                                                                                            </option>
                                                                                                                            <option
                                                                                                                                value="HI">
                                                                                                                                รับจ้าง
                                                                                                                            </option>
                                                                                                                            <option
                                                                                                                                value="HI">
                                                                                                                                ครู
                                                                                                                            </option>    
                                                                                                                            <option
                                                                                                                                value="HI">
                                                                                                                                นักวิทยาศาสตร์
                                                                                                                            </option>                                                                                                           
                                                                                                                        </optgroup>
                                                                                                                    </select>

                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>



                                                                                                      </div>
                                                                                                  </div>
                                                                                              </div>
                                                                                          </div>

                                                                                      </div>



                                                                                  </div>
                                                                              </div>

                                                              </div>
                                                          </div>
                                                    </div>

                                                    <div class="tab-content px-1 pt-1">
                                                          <div role="tabpanel" class="tab-pane active" id="tab11" aria-expanded="true" aria-labelledby="base-tab11">

                                                              <div class="card collapse-icon accordion-icon-rotate active">
                                                                  <div id="headingCollapse31" class="card-header bg-success">
                                                                      <a data-toggle="collapse" href="#collapse31" aria-expanded="true" aria-controls="collapse31" class="card-title lead white">
                                                                          <h6 style="margin-left:5px;">ส่วนที่2 ขึ้นทะเบียน</h6>
                                                                      </a>
                                                                  </div>



                                                                  <div class="tab-content px-1 pt-1">
                                                                                  <div role="tabpanel55"
                                                                                      class="tab-pane active"
                                                                                      id="tab1155" aria-expanded="true"
                                                                                      aria-labelledby="base-tab1155">

                                                                                      <div
                                                                                          class="card collapse-icon accordion-icon-rotate active">
                                                                                          
                                                                                          <div id="collapse55"
                                                                                              role="tabpanel55"
                                                                                              aria-labelledby="headingCollapse55"
                                                                                              class="card-collapse collapse show"
                                                                                              aria-expanded="true">
                                                                                              <div class="card-content">
                                                                                                  <div
                                                                                                      class="card-body">
                                                                                                      <div
                                                                                                          class="row match-height">

                                                                                                          <div
                                                                                                              class=" col-md-6">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      เครื่องหมาย 
                                                                                                                      :
                                                                                                                      <select
                                                                                                                      class="select2 form-control"
                                                                                                                      style="width: 100%;">
                                                                                                                            <optgroup
                                                                                                                                label="พ.อ.ต">
                                                                                                                                <option
                                                                                                                                    value="AK">
                                                                                                                                    เลือก
                                                                                                                                </option>
                                                                                                                                <option value="HI">พ.อ.ต</option>
                                                                                                                                    <option value="HI">พ.อ.ต </option>
                                                                                                                                    <option value="HI">พ.อ.ต </option>
                                                                                                                                    <option value="HI">พ.อ.ต </option>
                                                                                                                                    <option value="HI">พ.อ.ต </option>                                                                                                                        
                                                                                                                            </optgroup>                                                                                                         
                                                                                                                        </select>

                                                                                                                              </div>
                                                                                                                          </div>
                                                                                                                      </div>
                                                                                                                      <!-- --- -->

                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      หมายเลขประจำตัว :
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">

                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>

                                                                                                          <!-- --------------- -->  
                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">

                                                                                                                  <div
                                                                                                                      class="input-group">
                                                                                                                      วันขึ้นทะเบียนทหาร :   
                                                                                                                      <input
                                                                                                                          type="text"
                                                                                                                          class="form-control pickadate-disable-dates"
                                                                                                                          placeholder="25 กรกฏาคม 2562"
                                                                                                                          aria-describedby="button-addon4">
                                                                                                                      <div
                                                                                                                          class="input-group-append">
                                                                                                                          <button
                                                                                                                              class="btn btn-primary"
                                                                                                                              type="button"
                                                                                                                              style=" padding-bottom: 1px; padding-top: 1px;"><i
                                                                                                                                  class="la la-calendar-o"></i></button>
                                                                                                                      </div>
                                                                                                                  </div>

                                                                                                              </div>
                                                                                                          </div>
                                                                                                              <!-- --------------- -->  
                                                                                                              <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">

                                                                                                                  <div
                                                                                                                      class="input-group">
                                                                                                                      วันล้วง :   
                                                                                                                      <input
                                                                                                                          type="text"
                                                                                                                          class="form-control pickadate-disable-dates"
                                                                                                                          placeholder="25 กรกฏาคม 2562"
                                                                                                                          aria-describedby="button-addon4">
                                                                                                                      <div
                                                                                                                          class="input-group-append">
                                                                                                                          <button
                                                                                                                              class="btn btn-primary"
                                                                                                                              type="button"
                                                                                                                              style=" padding-bottom: 1px; padding-top: 1px;"><i
                                                                                                                                  class="la la-calendar-o"></i></button>
                                                                                                                      </div>
                                                                                                                  </div>

                                                                                                              </div>
                                                                                                          </div>
                                                                                                              <!-- -------- วันเข้ารับราชการ : ------- -->  
                                                                                                              <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">

                                                                                                                  <div
                                                                                                                      class="input-group">
                                                                                                                      วันเข้ารับราชการ : 
                                                                                                                      <input
                                                                                                                          type="text"
                                                                                                                          class="form-control pickadate-disable-dates"
                                                                                                                          placeholder="25 กรกฏาคม 2562"
                                                                                                                          aria-describedby="button-addon4">
                                                                                                                      <div
                                                                                                                          class="input-group-append">
                                                                                                                          <button
                                                                                                                              class="btn btn-primary"
                                                                                                                              type="button"
                                                                                                                              style=" padding-bottom: 1px; padding-top: 1px;"><i
                                                                                                                                  class="la la-calendar-o"></i></button>
                                                                                                                      </div>
                                                                                                                  </div>

                                                                                                              </div>
                                                                                                          </div>
                                                                                                              <!-- -------ขึ้นทะเบียนเพราะ :-------- --> 
                                                                                                              <div
                                                                                                              class=" col-md-6">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      ขึ้นทะเบียนเพราะ :
                                                                                                                      <select
                                                                                                                      class="select2 form-control"
                                                                                                                      style="width: 100%;">
                                                                                                                            <optgroup
                                                                                                                                label="ขึ้นทะเบียนเพราะ">
                                                                                                                                <option
                                                                                                                                    value="AK">
                                                                                                                                    ขึ้นทะเบียนเพราะ
                                                                                                                                </option>
                                                                                                                                <option value="HI">ขึ้นทะเบียนเพราะ</option>
                                                                                                                                    <option value="HI">ขึ้นทะเบียนเพราะ </option>
                                                                                                                                    <option value="HI">ขึ้นทะเบียนเพราะ</option>
                                                                                                                                    <option value="HI">ขึ้นทะเบียนเพราะ</option>
                                                                                                                                    <option value="HI">ขึ้นทะเบียนเพราะ</option>                                                                                                                        
                                                                                                                            </optgroup>                                                                                                         
                                                                                                                        </select>

                                                                                                                              </div>
                                                                                                                          </div>
                                                                                                                      </div>
                                                                                                              <!-- --------------- -->  

                                                                                                              <!-- --------------- -->                                                                                             
                                                                                                        </div>
                                                                                                  </div>
                                                                                              </div>
                                                                                          </div>

                                                                                      </div>



                                                                                  </div>
                                                                              </div>

                                                              </div>
                                                          </div>
                                                    </div>

                                                    <div class="tab-content px-1 pt-1">
                                                          <div role="tabpanel" class="tab-pane active" id="tab11" aria-expanded="true" aria-labelledby="base-tab11">

                                                              <div class="card collapse-icon accordion-icon-rotate active">
                                                                  <div id="headingCollapse31" class="card-header bg-success">
                                                                      <a data-toggle="collapse" href="#collapse31" aria-expanded="true" aria-controls="collapse31" class="card-title lead white">
                                                                          <h6 style="margin-left:5px;">ส่วนที่3 ลักษณะร่างกายและหมู่เลือด</h6>
                                                                      </a>
                                                                  </div>
                                                                  <div class="tab-content px-1 pt-1">
                                                                                  <div role="tabpanel55"
                                                                                      class="tab-pane active"
                                                                                      id="tab1155" aria-expanded="true"
                                                                                      aria-labelledby="base-tab1155">

                                                                                      <div
                                                                                          class="card collapse-icon accordion-icon-rotate active">
                                                                                          
                                                                                          <div id="collapse55"
                                                                                              role="tabpanel55"
                                                                                              aria-labelledby="headingCollapse55"
                                                                                              class="card-collapse collapse show"
                                                                                              aria-expanded="true">
                                                                                              <div class="card-content">
                                                                                                  <div
                                                                                                      class="card-body">
                                                                                                      <div
                                                                                                          class="row match-height">
                                                                                                          <!-- ---------ส่วนสูง--------- -->  
                                                                                                          <div
                                                                                                              class="col-lg-3 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      ส่วนสูง :                                                                                                                       
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">

                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>
                                                                                                           <!-- ---------น้ำหนัก--------- -->  
                                                                                                          <div 
                                                                                                              class="col-lg-3 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">                                                                                                                      
                                                                                                                      น้ำหนัก :
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">
                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div> 
                                                                                                             <!-- ---------รอบอก--------- -->
                                                                                                             <div
                                                                                                              class="col-lg-3 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      รอบอก :                                                                                                                     
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">
                                                                                                                          
                                                                                                                  </div>                                                                                                                  
                                                                                                              </div>
                                                                                                          </div> 
                                                                   
                                                                                                          <div
                                                                                                              class="col-lg-3 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      รอบอก :                                                                                                                     
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">
                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>                                                                                                            
                                                                                                           
                                                                                                          <!-- --------- รูปร่าง/สีผิว : --------- -->     
                                                                                                          <div
                                                                                                              class=" col-md-4">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      รูปร่าง/สีผิว : 
                                                                                                                      <select
                                                                                                                      class="select2 form-control"
                                                                                                                      style="width: 100%;">
                                                                                                                            <optgroup
                                                                                                                                label="รูปร่าง/สีผิว">
                                                                                                                                <option
                                                                                                                                    value="AK">
                                                                                                                                    รูปร่าง/สีผิว
                                                                                                                                </option>
                                                                                                                                <option value="HI">ขาว</option>
                                                                                                                                    <option value="HI">ดำ </option>
                                                                                                                                    <option value="HI">เหลือง</option>
                                                                                                                                    <option value="HI">ชมพู</option>
                                                                                                                                    <option value="HI">ดำแดง</option>                                                                                                                        
                                                                                                                            </optgroup>                                                                                                         
                                                                                                                        </select>

                                                                                                                              </div>
                                                                                                                          </div>
                                                                                                                      </div>
                                                                                                          <!-- --------กลุ่มเลือด : ---------- --> 
                                                                                                          <div
                                                                                                              class=" col-md-2">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      กลุ่มเลือด : 
                                                                                                                      <select
                                                                                                                      class="select2 form-control"
                                                                                                                      style="width: 100%;">
                                                                                                                            <optgroup
                                                                                                                                label="รูปร่าง/สีผิว">
                                                                                                                                <option
                                                                                                                                    value="AK">
                                                                                                                                    กลุ่มเลือด
                                                                                                                                </option>
                                                                                                                                <option value="HI">เอ</option>
                                                                                                                                    <option value="HI">บี </option>
                                                                                                                                    <option value="HI">เอบี</option>
                                                                                                                                    <option value="HI">โอ</option>                                                                                                                      
                                                                                                                            </optgroup>                                                                                                         
                                                                                                                        </select>

                                                                                                                              </div>
                                                                                                                          </div>
                                                                                                                      </div>
                                                                                                         
                                                                                                          <!-- --------ตำหนิ / แผลเป็น :---------- --> 
                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      ตำหนิ / แผลเป็น :                                                                                                                     
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">
                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>    

                                                                                                          <!-- --สิทธิลดเวลานรับราชการทหาร :-- -->
                                                                                                   <div class="col-12">
                                                                                                    <div class="card-body">
                                                                                                    สิทธิลดเวลานรับราชการทหาร :
                                                                                                    <div class="col-12">
                                                                                                    <div class="row">
                                                                                                        <div class="card-content">
                                                                                                            <div class="card-body">
                                                                                                                <div
                                                                                                                    class="d-inline-block custom-control custom-radio mr-1">
                                                                                                                    <input
                                                                                                                        type="radio"
                                                                                                                        class="custom-control-input"
                                                                                                                        name="colorRadio"
                                                                                                                        id="radio3">
                                                                                                                    <label
                                                                                                                        class="custom-control-label"
                                                                                                                        for="radio3">ร้องขอ ใช้สิทธิลด</label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                          </div>

                                                                                                          <div class="col-12">
                                                                                                    <div class="row">
                                                                                                        <div class="card-content">
                                                                                                            <div class="card-body">
                                                                                                                <div
                                                                                                                    class="d-inline-block custom-control custom-radio mr-1">
                                                                                                                    <input
                                                                                                                        type="radio"
                                                                                                                        class="custom-control-input"
                                                                                                                        name="colorRadio"
                                                                                                                        id="radio3">
                                                                                                                    <label
                                                                                                                        class="custom-control-label"
                                                                                                                        for="radio3">ร้องขอ ไม่ใช้สิทธิลด</label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                          </div>

                                                                                                          <div class="col-12">
                                                                                                    <div class="row">
                                                                                                        <div class="card-content">
                                                                                                            <div class="card-body">
                                                                                                                <div
                                                                                                                    class="d-inline-block custom-control custom-radio mr-1">
                                                                                                                    <input
                                                                                                                        type="radio"
                                                                                                                        class="custom-control-input"
                                                                                                                        name="colorRadio"
                                                                                                                        id="radio3">
                                                                                                                    <label
                                                                                                                        class="custom-control-label"
                                                                                                                        for="radio3">ไม่ร้องขอ ใช้สิทธิลด</label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                          </div>

                                                                                                          <div class="col-12">
                                                                                                    <div class="row">
                                                                                                        <div class="card-content">
                                                                                                            <div class="card-body">
                                                                                                                <div
                                                                                                                    class="d-inline-block custom-control custom-radio mr-1">
                                                                                                                    <input
                                                                                                                        type="radio"
                                                                                                                        class="custom-control-input"
                                                                                                                        name="colorRadio"
                                                                                                                        id="radio3">
                                                                                                                    <label
                                                                                                                        class="custom-control-label"
                                                                                                                        for="radio3">ไม่ร้องขอ ไม่ใช้สิทธิลด</label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                          </div>
                                                                                                    </div>
                                                                                                    </div>                                                                                                   

                                                                                                          <!-- --- -->
                                                                                                          <!-- --------วุฒิการศึกษา : ---------- --> 
                                                                                                          <div
                                                                                                              class=" col-md-6">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      วุฒิการศึกษา : 
                                                                                                                      <select
                                                                                                                      class="select2 form-control"
                                                                                                                      style="width: 100%;">
                                                                                                                            <optgroup
                                                                                                                                label="วุฒิการศึกษา">
                                                                                                                                <option
                                                                                                                                    value="AK">
                                                                                                                                    วุฒิการศึกษา
                                                                                                                                </option>
                                                                                                                                <option value="HI">ป.ตรี</option>
                                                                                                                                    <option value="HI">ป.โท</option>
                                                                                                                                    <option value="HI">ป.เอก</option>                                                                                                                   
                                                                                                                            </optgroup>                                                                                                         
                                                                                                                        </select>

                                                                                                                              </div>
                                                                                                                          </div>
                                                                                                                      </div>
                                                                                                          <!-- ---------สาขา :--------- --> 
                                                                                                          <div
                                                                                                              class=" col-md-6">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      สาขา : 
                                                                                                                      <select
                                                                                                                      class="select2 form-control"
                                                                                                                      style="width: 100%;">
                                                                                                                            <optgroup
                                                                                                                                label="สาขา">
                                                                                                                                <option
                                                                                                                                    value="AK">
                                                                                                                                    สาขา
                                                                                                                                </option>
                                                                                                                                <option value="HI">สาขา1</option>
                                                                                                                                    <option value="HI">สาขา2 </option>
                                                                                                                                    <option value="HI">สาขา3</option>
                                                                                                                                    <option value="HI">สาขา4</option>                                                                                                                      
                                                                                                                            </optgroup>                                                                                                         
                                                                                                                        </select>

                                                                                                                              </div>
                                                                                                                          </div>
                                                                                                                      </div>
                                                                                                          <!-- --------เวลารับราชการ---------- --> 
                                                                                                          <div
                                                                                                              class=" col-md-6">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      เวลารับราชการ : 
                                                                                                                      <select
                                                                                                                      class="select2 form-control"
                                                                                                                      style="width: 100%;">
                                                                                                                            <optgroup
                                                                                                                                label="เวลารับราชการ">
                                                                                                                                <option
                                                                                                                                    value="AK">
                                                                                                                                    เวลารับราชการ
                                                                                                                                </option>
                                                                                                                                <option value="HI">1</option>
                                                                                                                                    <option value="HI">2 </option>
                                                                                                                                    <option value="HI">3</option>
                                                                                                                                    <option value="HI">4</option>                                                                                                                      
                                                                                                                            </optgroup>                                                                                                         
                                                                                                                        </select>

                                                                                                                              </div>
                                                                                                                          </div>
                                                                                                                      </div>
                                                                                                          <!-- -------เวลารับราชการ : ----------- --> 



                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                        </div>
                                                                    </div>
                                                              </div>
                                                          </div>
                                                    </div>
                                                    








                                                  </div>
                                              </div>
                                          </div>

                                          <div class="tab-content px-1 pt-1">
                                                                      <div class="form-actions center" align="center">
                                                                          <!-- <button type="button" class="btn btn-danger  round btn-min-width mr-1 mb-1" id="type-error">ยกเลิก</button>
                                <button type="button" class="btn btn-success  round btn-min-width mr-1 mb-1" id="confirm-text" onclick="insertOrganizationParts()">บันทึก</button> -->

                                                                          <a href="Record_evidence_of_changes.php"><button type="button"
                                                                              class="btn btn-success  round btn-min-width mr-1 mb-1"
                                                                              id="submit" name="submit"
                                                                              onclick="insertOrganizationGroupType()">บันทึก</button></a>
                                                                          <a href="#"><button type="button"
                                                                              class="btn btn-danger  round btn-min-width mr-1 mb-1"
                                                                              id="type-error">ยกเลิก</button></a>
                                                                      </div>
                                                                  </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                  </section>
                  <!--/ Bootstrap 3 table -->
              </div>
          </div>
      </div>
  </section>

  <script src=" http://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous">
  </script>
  <script src="../app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
  <script type="text/javascript">
      $(document).ready(function() {
          console.log("ready");
          change_autorefreshdiv();
      });

      function change_autorefreshdiv() {
          // $('#prefixPage').addClass('active');
      }

      function toggle(source) {
          var checkboxes = document.querySelectorAll('.checkAll');
          for (var i = 0; i < checkboxes.length; i++) {
              if (checkboxes[i] != source)
                  checkboxes[i].checked = source.checked;
          }
      }
  </script>

  <!-- select2 -->
  <script src="../../app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
  <script src="../../app-assets/js/scripts/forms/select/form-select2.js" type="text/javascript"></script>
  <!-- select2 -->

  <!-- footer -->
  <?php include '../include/footer.php'; ?>



  </div>