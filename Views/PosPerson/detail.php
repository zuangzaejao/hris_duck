<?php 

require_once '../../config.php';
require_once '../../Model/Ducklab/duck.class.php'; 
require_once '../../Model/Ducklab/contents.class.php'; 
require_once '../../Model/Ducklab/org.class.php'; 
require_once '../../Model/Ducklab/func.php'; 
require_once '../include/header.php'; 
  
    
$menu1 ="HRTPERSONCHECK" ;
$menu2 ="PERSONCHECK"; 
 
    $clsorg = new OrgClass();
  
    $OrgTypeID = $_GET['OrgTypeID'] ;  
    $OrgLevel2 = $_GET['OrgLevel2'] ; 
    //$PersonTypeID = $_GET['PersonTypeID'] ; 
    $OrgLevelDIDSelect = $_GET['OrgLevelDIDSelect'] ; 

    
    
    $OrgLevel2DID = $OrgLevel2  ;

   $OrgTypeData =  $clsorg->LoadOnce('OrgType',array('OrgTypeID'=>$OrgTypeID) ); 
    
 
?>

<?php 
include '../include/menu.php';  
include_once '../include/modelOnload.php' ;
?>
<style>
.box_positionsetup ul.list-group{
    margin: 10px;
}
</style>
    <input type="hidden" name="OrgLevel2DID" id="OrgLevel2DID" value="<?php echo $OrgLevel2DID ;?>" > 
    <section>
        <div class="app-content content">
            <div class="content-wrapper">
                <div class="content-header row">
                    <div class="content-header-left col-md-6 col-12 mb-2">
                        <div class="htab" ></div>
                        <h3 class="content-header-title">ทำเนียบบรรจุกำลังพล</h3>
                        <div class="row breadcrumbs-top">
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="../index.php">ระบบงานทำเนียบบรรจุกำลังพล</a></li> 
                                    <li class="breadcrumb-item"><a href="index.php">ทำเนียบบรรจุกำลังพล</a></li> 
                                    <li class="breadcrumb-item active" aria-current="page"> ทำเนียบบรรจุกำลังพล</li>  
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
 
                <div class="container-fluid">
                    
                    <div class="row hidden"> 
                        <div class="col-sm-12">
                            <div class="box_h1 card">
                                <div class="card-header card-head-inverse  ">
                                    <h4 class="card-title text-white"> ค้นหา </h4> 
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                    <div class="row">
                                        <div class=" col-md-8  m-auto "> 
                                        <form class="form">
                                            <div class="form-body">  
                                                <div class="row"> 
                                                    <div class="col-md-6 m-auto">
                                                        <div class="form-group">
                                                            <label for="ChiefSpcID">  สายวิทยาการ    </label>
                                                            <select id="ChiefSpcID" name="ChiefSpcID" class="form-control select2">
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>  
                                            </div> 
                                            <div class="text-center">
                                            <button type="button" class="btn round btn1 btncustom1 mr-1" id="btnSearch">
                                                <i class="fa fa-search"></i> ค้นหา
                                            </button> 
                                            <button type="reset" class="btn round btn1 btncustom1">
                                                <i class=" fa fa-repeat"></i> ล้างค่า
                                            </button>
                                            </div> 
                                        </form> 
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>


                    <div class="row"> 
                        <div class="col-lg-4">
                            <div class="content-body">
                                <div class="sidebar-content card d-none d-lg-block">
                                    <div class="card-header boxheader1" >
                                        โครงการส่วนราชการ
                                        <a style="float:right;"><i class="la la-sitemap"></i> </a>
                                    </div>
                                    <div class="card-body "  >
                                        
                                        <div class="card-content">
                                            <div class="card-body skin-flat"> 
                                            <?php 
                                                  require_once 'detail_setting.php'; 
                                            ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">    
                            <input type="hidden" class="form-control inp_orgName" id="inp_orgName">
                            <input type="hidden" class="form-control inp_orgNameAbbr" id="inp_orgNameAbbr">
                            <input type="hidden" class="form-control inp_orgNameAbbrSemi" id="inp_orgNameAbbrSemi">

                            <?php  require_once "detail_position.php" ; ?>  
                        </div> 
                    </div> 
                </div> 
            </div>
        </div>
    </section>
     
    <input type="hidden" id="OrgLevelDIDSelect" name="OrgLevelDIDSelect" value="<?php echo $OrgLevelDIDSelect;?>">
    <input type="hidden" id="OrgLevel2x" name="OrgLevel2x" value="<?php echo $_GET['OrgLevel2'];?>">
    <input type="hidden" id="OrgTypeID" name="OrgTypeID" value="<?php echo $_GET['OrgTypeID'];?>">
<style>

</style>

<!-- footer -->
<?php include '../include/footer.php'; ?>
 
     <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" />
    <script src="//code.jquery.com/ui/1.10.4/jquery-ui.min.js"></script>   -->

    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/ui/dragula.min.css">
    <link rel="stylesheet" type="text/css"  href="../../Asset/custom/css/jquery-ui.css"> 
    <script type="text/javascript" src="../../Asset/custom/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../Asset/js/jquery.mjs.nestedSortable.js"></script>
    <script type="text/javascript" src="../../Asset/custom/js/hirachysetting.js"></script> 
    <link rel="stylesheet" href="../../Asset/custom/css/hirachysetting.css" />
     
    <style>
    .deptnow{
        background: #d8e7ff;
    }
    </style>

<script type="text/javascript">
    $(document).ready(function() {
        $(".menuDiv").click(function(){
            $(".menuDiv").removeClass("deptnow");
            $(this).addClass("deptnow");
        });

        if( $("#OrgLevelDIDSelect").val()){
            org.LoadPositionByOrgLevelDID($("#OrgLevelDIDSelect").val());
        } 

        org.LoadChiefSpcSel('','ChiefSpcID','');
    }); 

    function gotoCreate(){ 
       // window.location.href="create.php?OrgLevelDID="+$("#OrgLevelDIDtxt").val()+"&OrgType="+ $("#OrgTypeIDtxt").val()+"&OrgLevel2="+$("#OrgLevel2DID").val();
    }
 </script>
