<?php 

require_once '../../config.php';
require_once '../../Model/Ducklab/duck.class.php'; 
require_once '../../Model/Ducklab/contents.class.php'; 
require_once '../../Model/Ducklab/org.class.php'; 
require_once '../../Model/Ducklab/func.php'; 
require_once '../include/header.php'; 
  
    
$menu1 ="HRTPERSONCHECK" ;
$menu2 ="PERSONCHECK"; 
 
    $clsorg = new OrgClass();
  
    $OrgTypeID = $_GET['OrgTypeID'] ;  
    $PositionID = $_GET['positionID'] ; 
    $PosNo = $_GET['posNo'] ; 
    $OrgLevelDID = $_GET['OrgLevelDID'] ; 
    $OrgLevel2 = $_GET['OrgLevel2'] ; 
    $OrgTypeID = $_GET['OrgTypeID'] ; 

    
    $datapos = $clsorg->LoadPositionDetail($PositionID);
    $dataorg  = $clsorg->LoadDataOrgParent($OrgLevelDID);
    $dataposperson  = $clsorg->LoadPersonByPositionNo($PositionID,$PosNo);
     
    //PosAddamt
    
 
include_once '../include/menu.php'; 
include_once '../include/modelOnload.php' ;
?>
 

  <section>
      <div class="app-content content">
          <div class="content-wrapper">
                <div class="content-header row">
                    <div class="content-header-left col-md-6 col-12 mb-2">
                        <div class="htab" ></div>
                        <h3 class="content-header-title">ทำเนียบบรรจุกำลังพล</h3>
                        <div class="row breadcrumbs-top">
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="../index.php">ระบบงานทำเนียบบรรจุกำลังพล</a></li> 
                                    <li class="breadcrumb-item"><a href="index.php">ทำเนียบบรรจุกำลังพล</a></li> 
                                    <li class="breadcrumb-item  active" aria-current="page"> รายละเอียดของอัตราตำแหน่งในโครงสร้าง   </li>  
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-body">
                    <div class="row ">
                        <div class="col-12  px-4">
                        <?php    
                      
                            echo $OrgTypeID .":".$PositionID.":". $PosNo.":". $OrgLevelDID .":". $OrgLevel2 .":".$OrgTypeID  ; 
                         /*  echo "<pre>".print_r($datapos,true)."</pre>";  
                            echo "<pre>".print_r($dataposperson,true)."</pre>"; 
                             echo "<pre>".print_r($PosAddamt,true)."</pre>"; */
                            $PosPerson = $dataposperson[0] ; 
                        ?>
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <div class="row"> 
                                            <div class="col-12">
                                                <div class="row"> 
                                                    <div class="col-sm-12">
                                                        <div class="box_h1 card">
                                                            <div class="card-header card-head-inverse text-center ">
                                                                <h4 class="card-title text-white"> รายละเอียดของอัตราตำแหน่งในโครงสร้าง </h4>  
                                                            </div>
                                                            <div class="card-content collapse show">
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class=" col-md-8  m-auto ">
                                                                        
                                                                        <form class="form">
                                                                            <div class="form-body"> 
                                        
                                                                            <div class="boxsearchplan2">
                                                                            
                                                                                <div class="row"> 
                                                                                    <div class="col-md-8 m-auto">
                                                                                        <div class="form-group row">
                                                                                            <label class="col-3 text-bold-600 text-right" > ชื่อตำแหน่ง : </label>
                                                                                            <div class="col-9"> <?php echo $datapos['PosName'];?> </div>
                                                                                        </div>
                                                                                    </div> 
                                                                                </div>  
                                                                                <div class="row"> 
                                                                                    <div class="col-md-8 m-auto">
                                                                                        <div class="form-group row">
                                                                                            <label class="col-3 text-bold-600 text-right" > ชื่อตำแหน่ง-สังกัด : </label>
                                                                                            <div class="col-9"> <?php echo $datapos['PosName'] ." ".$dataorg['listname']['orgNameAbbrSemi'] ;?> </div>
                                                                                        </div>
                                                                                    </div> 
                                                                                </div> 
                                                                                <div class="row"> 
                                                                                    <div class="col-md-8 m-auto ">
                                                                                        <div class="row">
                                                                                            <div class="col-md-6  ">
                                                                                                <div class="form-group  row  ">
                                                                                                    <label class="col-6 text-bold-600 text-right" > เงินเดือนอัตรา : </label>
                                                                                                    <div class="col-6"><?php echo $datapos['HrtRankAbbrTh'];?>   </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6  ">
                                                                                                <div class="form-group  row  ">
                                                                                                    <label class="col-6 text-bold-600 text-right" > จำนวนอัตรา : </label>
                                                                                                    <div class="col-6"> <?php echo $datapos['HrtPosNum'];?>   </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div> 
                                                                                    
                                                                                </div> 
                                                                                
                                                                            </div>  <!-- ./boxsearchplan2 -->
                                                                            
                                                                            </div> 

                                                                            

                                                                        </form>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                        <div class="row"> 
                                            <div class="col-12 ">
                                                <div class="box_h1 card">

                                                    <ul class="nav nav-tabs nav-top-border no-hover-bg mb-1"  >
                                                        <li class="nav-item">
                                                            <a class="nav-link active" id="base-tab11" data-toggle="tab" aria-controls="tab11" href="#tab11" aria-expanded="true"> ทำเนียบ </a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" id="base-tab12" data-toggle="tab" aria-controls="tab12" href="#tab12" aria-expanded="false"> เงินเพิ่มประจำตำแหน่ง </a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" id="base-tab13" data-toggle="tab" aria-controls="tab13" href="#tab13" aria-expanded="false"> ลชทอ./เลขหมายรายงาน</a>
                                                        </li>
                                                    </ul>


                                                    <div class="tab-content pt-1"> 
                                                        <div role="tabpanel" class="tab-pane active" id="tab11" aria-expanded="true" aria-labelledby="base-tab11" >
                                                            <div class="row"> 
                                                                <div class="col-sm-12 px-4">
                                                                    <div class="box_h1 card">
                                                                        <div class="card-header card-head-inverse text-center ">
                                                                            <h4 class="card-title text-white"> บันทึก/แก้ไข ทำเนียบ </h4> 
                                                                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                                                            <div class="heading-elements">
                                                                                <ul class="list-inline mb-0">
                                                                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="card-content collapse show">
                                                                            <div class="card-body">
                                                                                        
                                                                                <form class="form_custom1">
                                                                                  
                                                                                    <div class="row">
                                                                                        <div class="col-6 m-auto pb-2">   
                                                                                            <div class="row"> 
                                                                                                <div class="col-md-6 col-10">
                                                                                                    <div class="form-group">
                                                                                                        <label for=""> เลขที่อัตรา : </label>
                                                                                                        <input type="text" class="form-control" id="PosNo" name="PosNo" value="<?php echo $PosPerson['PosNo']?>" >
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-6 col-10">
                                                                                                    <div class="form-group">
                                                                                                        <label for=""> รหัสจ่ายตรง : </label>
                                                                                                        <input type="text" class="form-control" id="DirectpayCode" name="DirectpayCode"  value="<?php echo $PosPerson['DirectpayCode']?>" >
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>  
                                                                                            <div class="row"> 
                                                                                                <div class="col-md-6 col-10">
                                                                                                    <div class="form-group">
                                                                                                        <label for=""> ลำดับการจัดเรียง : </label>
                                                                                                        <input type="text" class="form-control col-md-6 " id="HrtPosPrintSeq" name="HrtPosPrintSeq" value="<?php echo $datapos['HrtPosPrintSeq']?>">
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-6 col-10">
                                                                                                    <div class="form-group">
                                                                                                        <label for=""> หมายเลขประจำตัว : </label>
                                                                                                        <input type="text" class="form-control" id="AirForceID" name="AirForceID" value="<?php echo $PosPerson['AirForceID']?>">
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>  
                                                                                            <div class="row"> 
                                                                                                <div class="col-md-12 col-10">
                                                                                                    <div class="form-group">
                                                                                                        <label for=""> ชื่อ - สกุล : </label>
                                                                                                        <select  id="PersonID"  name="PersonID" class="form-control select2 block " > 
                                                                                                        </select> 
                                                                                                    </div>
                                                                                                </div>
                                                                                                
                                                                                            </div>  
                                                                                        </div>
                                                                                    </div>
                                                                                   
                                                                                    <div class="row">
                                                                                        <div class="col-12 col-md-4 px-0 border"> 
                                                                                            <div class="boxsub1 text-bold-600 text-center  ">  
                                                                                                เลขที่คำสั่งย้ายเข้าตำแหน่ง  
                                                                                            </div> 
                                                                                            <div class="px-3 py-2">
                                                                                                <div class="form-group row">
                                                                                                    <label class="col-5 text-right"> ประเภทการครองตำแหน่ง : </label> 
                                                                                                    <select  id="PosPerStatus"  name="PosPerStatus" class="form-control col-7 select2" >
                                                                                                    </select>
                                                                                                </div> 
                                                                                                <div class="form-group row">
                                                                                                    <label class="col-5  text-right"> คำสั่ง : </label>
                                                                                                    <select  id="SourceOfCommID"  name="SourceOfCommID" class="form-control col-4 " >
                                                                                                    </select>
                                                                                                </div>  
                                                                                                <div class="form-group row">
                                                                                                    <label class="col-5  text-right"> เลขที่  : </label>
                                                                                                    <input type="text" class="form-control col-6"  id="CommDocNo" name="CommDocNo" value="<?php echo $PosPerson['CommDocNo'];?>">
                                                                                                </div>  

                                                                                                
                                                                                                <div class="form-group row">
                                                                                                    <label class="col-5  text-right"> ลงวันที่ :   </label> 
                                                                                                    <div class="input-group datep col-6">
                                                                                                        <input type="text" class="form-control form2 pickadate-translations  " style="width:78%;"  placeholder="" id="CommDocDate" name="CommDocDate" data-value="<?php echo $PosPerson['CommDocDate'];?>" />
                                                                                                        <div class="input-group-append">
                                                                                                            <span class="input-group-text">
                                                                                                                <span class="la la-calendar-o"></span>
                                                                                                            </span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>  
                                                                                                <div class="form-group row">
                                                                                                    <label class="col-5  text-right"> วันที่มีผล :   </label> 
                                                                                                    <div class="input-group datep col-6">
                                                                                                        <input type="text" class="form-control form2 pickadate-translations  " style="width:78%;"  placeholder="" id="CommEffDate" name="CommEffDate" data-value="<?php echo $PosPerson['CommEffDate'];?>"  />
                                                                                                        <div class="input-group-append">
                                                                                                            <span class="input-group-text">
                                                                                                                <span class="la la-calendar-o"></span>
                                                                                                            </span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="form-group row">
                                                                                                    <label class="col-5  text-right"> ลำดับที่ในคำสั่ง  : </label>
                                                                                                    <input type="number" class="form-control col-3"  id="CommLevelID" name="CommLevelID" value="<?php echo $PosPerson['CommLevelID'];?>">
                                                                                                </div>

                                                                                            </div>
                                                                                        </div>   

                                                                                        <div class="col-12 col-md-4 px-0 border"> 
                                                                                            <div class="boxsub1 text-bold-600 text-center  ">  
                                                                                                เลขที่คำสั่งย้ายไปตำแหน่งอื่น  
                                                                                            </div> 
                                                                                            <div class="px-3 py-2">
                                                                                                <div class="form-group row">
                                                                                                    <label class="col-5 text-right"> ประเภทการครองตำแหน่ง : </label> 
                                                                                                    <select  id="PosPerStatusPsr"  name="PosPerStatusPsr" class="form-control col-7 select2 " >
                                                                                                    </select>
                                                                                                </div> 
                                                                                                <div class="form-group row">
                                                                                                    <label class="col-5  text-right"> คำสั่ง : </label>
                                                                                                    <select  id="SourceOfCommIDPsr"  name="SourceOfCommIDPsr" class="form-control col-4 select2" >
                                                                                                    </select>
                                                                                                </div>  
                                                                                                <div class="form-group row">
                                                                                                    <label class="col-5  text-right"> เลขที่  : </label>
                                                                                                    <input type="text" class="form-control col-6"  id="CommDocNoPsr" name="CommDocNoPsr" value="<?php echo $PosPerson['CommDocNoPsr']?>">
                                                                                                </div>  

                                                                                                
                                                                                                <div class="form-group row">
                                                                                                    <label class="col-5  text-right"> ลงวันที่ :   </label> 
                                                                                                    <div class="input-group datep col-6">
                                                                                                        <input type="text" class="form-control form2 pickadate-translations  " style="width:78%;"  placeholder="" id="CommDocDatePsr" name="CommDocDatePsr" data-value="<?php echo $PosPerson['CommDocDatePsr']?>" />
                                                                                                        <div class="input-group-append">
                                                                                                            <span class="input-group-text">
                                                                                                                <span class="la la-calendar-o"></span>
                                                                                                            </span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>  
                                                                                                <div class="form-group row">
                                                                                                    <label class="col-5  text-right"> วันที่มีผล :   </label> 
                                                                                                    <div class="input-group datep col-6">
                                                                                                        <input type="text" class="form-control form2 pickadate-translations  " style="width:78%;"  placeholder="" id="CommEffDatePsr" name="CommEffDatePsr" data-value="<?php echo $PosPerson['CommEffDatePsr']?>" />
                                                                                                        <div class="input-group-append">
                                                                                                            <span class="input-group-text">
                                                                                                                <span class="la la-calendar-o"></span>
                                                                                                            </span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="form-group row">
                                                                                                    <label class="col-5  text-right"> ลำดับที่ในคำสั่ง  : </label>
                                                                                                    <input type="number" class="form-control col-3"  id="CommItemNoPsr" name="CommItemNoPsr" value="<?php echo $PosPerson['CommItemNoPsr'];?>">
                                                                                                </div>

                                                                                            </div>
                                                                                        </div>   

                                                                                        <div class="col-12 col-md-4 px-0 border"> 
                                                                                            <div class="boxsub1 text-bold-600 text-center  ">  
                                                                                                หลักฐานการอนุมัติแผนบรรจุประจำปี  
                                                                                            </div> 
                                                                                            <div class="px-3 py-2">
                                                                                                <div class="form-group row">
                                                                                                    <label class="col-5 text-right"> แผนปีงบประมาณ : </label> 
                                                                                                    <input type="text" class="form-control col-4"  id="BudgetYearPlan" name="BudgetYearPlan" value="<?php echo $PosPerson['BudgetYearPlan'];?>">
                                                                                                </div> 
                                                                                                <div class="form-group row">
                                                                                                    <label class="col-5  text-right"> คุณวุฒิจากแผนที่บรรจุ : </label>
                                                                                                    <select  id="ReqQualifyCode"  name="ReqQualifyCode" class="form-control col-4 select2" >
                                                                                                        <option value="1">  </option>
                                                                                                        <option value="2">   </option>
                                                                                                        <option value="3">  </option>
                                                                                                    </select>
                                                                                                </div>  
                                                                                                <div class="form-group row">
                                                                                                    <label class="col-5  text-right"> เลขที่หลักฐาน  : </label>
                                                                                                    <input type="text" class="form-control col-6"  id="CommDocNoPlan" name="CommDocNoPlan" value="<?php echo $PosPerson['CommDocNoPlan'];?>">
                                                                                                </div>  

                                                                                                
                                                                                                <div class="form-group row">
                                                                                                    <label class="col-5  text-right">  วันที่อนุมัติ :   </label> 
                                                                                                    <div class="input-group datep col-6">
                                                                                                        <input type="text" class="form-control form2 pickadate-translations  " style="width:78%;"  placeholder="" id="CommEffDatePlan" name="CommEffDatePlan" data-value="<?php echo $PosPerson['CommEffDatePlan'];?>" />
                                                                                                        <div class="input-group-append">
                                                                                                            <span class="input-group-text">
                                                                                                                <span class="la la-calendar-o"></span>
                                                                                                            </span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>  
 

                                                                                            </div>
                                                                                        </div>     
                                                                                    </div> 

                                                                                    <div class="row">
                                                                                        <div class="col-12 mt-2">
                                                                                            <div class="form-group row">
                                                                                                <label class="col-5  text-right"> หมายเหตุ  : </label>
                                                                                                <input type="text" class="form-control col-4"  id="Remark" name="Remark" value="">
                                                                                            </div>  
                                                                                            <div class="form-group row skin skin-flat">
                                                                                                <label class="col-5  text-right">   </label>
                                                                                                <div class="col-4">
                                                                                                    <fieldset>
                                                                                                    <input type="checkbox" id="IsPosPersonChangeHist" name="IsPosPersonChangeHist">
                                                                                                    <label for="IsPosPersonChangeHist">ลงประวัติดำรงตำแหน่ง</label>
                                                                                                    </fieldset> 
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>  
                                                                                   
                                                                                    </div> 

                                                                                    <div class="row">
                                                                                        <div  class="col-12 m-auto text-center py-2 ">
                                                                                            <button type="button" class="btn btn2 btn-success   round" id="btn_cfsavePosPerson"> <i class="fa fa-save"></i> บันทึก </button>
                                                                                            <button type="button" class="btn btn2 btn-danger   round"  data-dismiss="modal"> <i class="fa fa-times-circle-o"></i> ยกเลิก </button> 
                                                                                        </div>
                                                                                    </div>


                                                                                </form>

                                
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div> <!-- /.tab11 -->

                                                             
                                                        <div role="tabpanel" class="tab-pane" id="tab12" aria-expanded="true" aria-labelledby="base-tab12" > 
                                                            <div class="row"> 
                                                                <div class="col-sm-12 px-4">
                                                                    <div class="box_h1 card">
                                                                        <div class="card-header card-head-inverse text-center ">
                                                                            <h4 class="card-title text-white"> เงินเพิ่มประจำตำแหน่ง</h4> 
                                                                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                                                            <div class="heading-elements">
                                                                                <ul class="list-inline mb-0">
                                                                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="card-content collapse show">
                                                                            <div class="card-body">
                                                                                <p class="card-text"></p>
                                                                                
                                                                                <a href="#" onclick="CreateAddamt();" class="btn btn-social btn-min-width mb-1 btn_b1">
                                                                                    <span class="la la-plus-circle"></span> เพิ่ม
                                                                                </a>

                                                                                <table id="PosAddamtTable" class="table table-striped table-border table-hover table_custom1" style="">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th></th>
                                                                                            <th>ลำดับที่</th>
                                                                                            <th class="text-left">ชื่อเงินเพิ่ม</th>
                                                                                            <th>จำนวนเงิน</th>
                                                                                            <th>ค่าตอบแทน</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody> 
                                                                                    </tbody>
                                                                                </table> 
                                                                                
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div> 
                                                        </div> <!-- /.tab12 -->
                                                
                                                
                                                        <div role="tabpanel" class="tab-pane" id="tab13" aria-expanded="true" aria-labelledby="base-tab13" >
                                                            <div class="row"> 
                                                                <div class="col-sm-12 px-4">
                                                                    <div class="box_h1 card">
                                                                        <div class="card-header card-head-inverse text-center ">
                                                                            <h4 class="card-title text-white"> ลชทอ./เลขหมายรายงาน </h4> 
                                                                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                                                            <div class="heading-elements">
                                                                                <ul class="list-inline mb-0">
                                                                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="card-content collapse show">
                                                                            <div class="card-body">
                                                                                <p class="card-text"></p>
                                                                                
                                                                                <a href="#"  onclick="$('#modalSkill').modal();"  class="btn btn-social btn-min-width mb-1 btn_b1">
                                                                                    <span class="la la-plus-circle"></span> เพิ่ม
                                                                                </a>

                                                                                <table id="PosSkillTable" class="table table-striped table-borderless table-hover table_custom1" style=" ">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th></th>
                                                                                            <th>ลำดับที่</th>
                                                                                            <th>ลชทอ./เลขหมายรายงาน </th>
                                                                                            <th>เหล่า</th> 
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>

                                                                                    </tbody>
                                                                                </table>
                                                                           
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div> 
                                                        </div> <!-- /.tab13 -->

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </section>
    
    <input type="hidden" id="AddAmtCodeOld" name="AddAmtCodeOld" value="">
    <input type="hidden" id="PositionID" name="PositionID" value="<?php echo $PositionID;?>">
    <input type="hidden" id="PersonTypeID" name="PersonTypeID" value="<?php echo $datepos['HrtPositionTypeID'];?>">

    <input type="hidden" id="CreateUserID" name="CreateUserID" value="<?php echo '1';?>">
    <input type="hidden" id="CreateDate" name="CreateDate" value="<?php echo GetToday('')?>">
    <input type="hidden" id="UpdateUserID" name="UpdateUserID" value="<?php echo '1';?>">
    <input type="hidden" id="UpdateDate" name="UpdateDate" value="<?php echo GetToday('')?>">
    
    <!-- #######################  Modal  ####################### -->
        <!-- #######################  modalAddamt  ####################### --> 
        <div class="modal fade text-left modal_custom1" id="modalAddamt" tabindex="-1" role="dialog" aria-labelledby="modalAddamt"  aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
            <div class="modal-header bg_custom1">
                <h5 class="modal-title"> บันทึก/แก้ไข เงินเพิ่มประจำตำแหน่ง </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <form class="form_custom1">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-10 m-auto pt-2">   
                                <div>
                                    <div class="row"> 
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <label for="positionname " class="col-3 text-right"> เงินเพิ่ม <span class="text-danger">*</span> : </label>
                                                <select  id="AddAmtCode"  name="AddAmtCode" class="form-control col-8 select2" style="width:58%" >
                                                </select> 
                                            </div>
                                        </div> 
                                    </div> 
                                    <div class="row"> 
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <label class="col-3 text-right"> จำนวนเงิน  : </label>
                                                <input type="text" class="form-control col-2 text-right" id="PosStrucAddAmt" name="PosStrucAddAmt" value=""> 
                                                <label class="col-1"> บาท </label>
                                                
                                                <label class="col-2 text-right"> ตอบแทน  : </label>
                                                <input type="text" class="form-control col-2 text-right" id="PosStrucRewardAmt" name="PosStrucRewardAmt" value="" > 
                                                <label class="col-1"> บาท </label>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 text-right"> จำนวนอัตราที่ได้รับเงิน  : </label>
                                                <input type="text" class="form-control col-2 text-right" id="NumApprove" name="NumApprove" value="" > 
                                                <label class="col-1">  </label> 
                                            </div>
                                        </div> 
                                    </div> 
                                </div>  

                                <hr class="">
                                <div class="mt-2">
                                    <div class="row"> 
                                        <div class="col-12 mb-1">
                                            <span class="text-bold-600 font-medium-2 fontc1"><u>คำสั่ง</u></span>
                                        </div>
                                    </div>
                                    <div class="row"> 
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <label for="" class="col-3 text-right"> คำสั่ง : </label>
                                                <select  id="AddamtSourceOfCommID"  name="AddamtSourceOfCommID" class="form-control col-4 select2  "  style="width:58%"> 
                                                </select> 
                                            </div>
                                        </div> 
                                    </div>  
                                    <div class="row"> 
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <label for=""  class="col-3 text-right"> เลขที่คำสั่ง : </label>
                                                <input type="text" class="form-control col-4 " id="AddamtCommDocNo" name="AddamtCommDocNo" > 
                                            </div>
                                        </div> 
                                    </div> 
                                
                                    <div class="row"> 
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <label for=""  class="col-3 text-right"> ลงวันที่ : </label>
                                                <div class="input-group datep col-4">
                                                    <input type="text" class="form-control form2 pickadate-translations  " style="width:78%;"  id="AddamtCommDocDate" name="AddamtCommDocDate" data-value="" />
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <span class="la la-calendar-o"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                    </div> 
                                    <div class="row"> 
                                        <div class="col-12"> 
                                            <div class="form-group row">
                                                <label for=""  class="col-3 text-right"> วันที่มีผล : </label>
                                                <div class="input-group datep col-4">
                                                    <input type="text" class="form-control form2 pickadate-translations  " style="width:78%;" id="AddamtCommEffDate" name="AddamtCommEffDate" data-value="" />
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <span class="la la-calendar-o"></span>
                                                        </span>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div> 
                                    </div> 
                                    <div class="row"> 
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <label for=""  class="col-3 text-right"> วันที่สิ้นสุด : </label>
                                                <div class="input-group datep col-4">
                                                    <input type="text" class="form-control form2 pickadate-translations  " style="width:78%;"  id="AddamtCommExpDate" name="AddamtCommExpDate" data-value="" />
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <span class="la la-calendar-o"></span>
                                                        </span>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div> 
                                    </div> 
                                </div>  


                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div  class="m-auto ">
                            <button type="button" class="btn btn2 btn-success round" id="btneditcf_Addamt"> <i class="fa fa-save"></i>  แก้ไข </button>
                            <button type="button" class="btn btn2 btn-success round" id="btncreatecf_Addamt"> <i class="fa fa-save"></i> บันทึก </button>
                            <button type="button" class="btn btn2 btn-danger round"  data-dismiss="modal"> <i class="fa fa-times-circle-o"></i> ยกเลิก </button> 
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
        <!-- #######################  modalSkill  ####################### --> 
        <div class="modal fade text-left modal_custom1" id="modalSkill" tabindex="-1" role="dialog" aria-labelledby="modalSkill"  aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg_custom1">
                        <h5 class="modal-title"> บันทึก/แก้ไข ลชทอ./เลขหมายรายงาน </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form class="form_custom1">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-10 m-auto pt-2">   

                                    <div class="row"> 
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <label class="col-4 text-right"> ชื่ออักษรนำ  : </label>
                                                <select  id="PrefixSkillID"  name="PrefixSkillID" class="form-control  col-4 select2" style="width:55%"   >
                                                </select> 
                                            </div>
                                        </div> 
                                    </div> 
                                    <div class="row"> 
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <label class="col-4 text-right"> เลขหมายรายงาน  : </label>  
                                                <select  id="SkillNo"  name="SkillNo" class="form-control col-4 select2" style="width:55%" >
                                                </select> 
                                            </div>
                                        </div> 
                                    </div> 
                                    <div class="row"> 
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <label class="col-4 text-right"> เหล่า  : </label>  
                                                <select  id="ArmCode"  name="ArmCode" class="form-control col-4 select2 "  style="width:55%" >
                                                    <option value="1">  </option>
                                                    <option value="2">   </option>
                                                    <option value="3">  </option>
                                                </select> 
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div  class="m-auto ">
                                <button type="button" class="btn btn2 btn-success   round"> <i class="fa fa-save"></i> บันทึก </button>
                                <button type="button" class="btn btn2 btn-danger   round"  data-dismiss="modal"> <i class="fa fa-times-circle-o"></i> ยกเลิก </button> 
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        
        <!-- ####################################   modal_createcf_Addamt  #################################### -->
        <div class="modal fade text-left modal_custom1" id="modal_createcf_Addamt" tabindex="-1" role="dialog" aria-labelledby="modal_createcf_Addamt"  aria-hidden="true">
                <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <form>
                    <div class="my-2 mx-2 px-2 py-1">
                        <div class="text-center"> 
                        <span class=" fontcolor3 "> <i class="la la-file-text-o la10" ></i> </span>
                        <div class="pb-3 pt-1">
                        <h3> ต้องการบันทึกข้อมูล ?</h3> 
                        </div>
                        </div>
                        <div class="col-12 d-flex justify-content-center"> 
                        <div class="row"> 
                            <button type="button" class="btn btn-min-width mb-1 mr-1  round btn-success" id="btncreate_Addamt"   >  บันทึก </button>
                            <button class="btn btn-min-width mb-1 mr-1  round btn-danger" role="button"  data-dismiss="modal">  ยกเลิก </button> 
                        </div>
                        </div>
                    </div> 
                    </form>
                </div> 
                </div> 
        </div>
        <!-- ####################################   end modal_createcf_Addamt  #################################### -->
        <!-- ####################################   modal_editcf_Addamt   #################################### -->
        <div class="modal fade text-left modal_custom1" id="modal_editcf_Addamt" tabindex="-1" role="dialog" aria-labelledby="modal_editcf_Addamt"  aria-hidden="true">
                <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <form>
                    <div class="my-2 mx-2 px-2 py-1">
                        <div class="text-center"> 
                        <span class=" fontcolor3 "> <i class="la la-file-text-o la10" ></i> </span>
                        <div class="pb-3 pt-1">
                        <h3> ต้องการแก้ไขข้อมูล ?</h3> 
                        </div>
                        </div>
                        <div class="col-12 d-flex justify-content-center"> 
                        <div class="row"> 
                            <button type="button" class="btn btn-min-width mb-1 mr-1  round btn-success" id="btnedit_Addamt"   >  บันทึก </button>
                            <button class="btn btn-min-width mb-1 mr-1  round btn-danger" role="button"  data-dismiss="modal">  ยกเลิก </button> 
                        </div>
                        </div>
                    </div> 
                    </form>
                </div> 
                </div> 
        </div>
        <!-- ####################################   end modal_editcf_Addamt  #################################### -->

        <!-- ####################################   modal_createcf_Skill  #################################### -->
        <div class="modal fade text-left modal_custom1" id="modal_createcf_Skill" tabindex="-1" role="dialog" aria-labelledby="modal_createcf_Skill"  aria-hidden="true">
                <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <form>
                    <div class="my-2 mx-2 px-2 py-1">
                        <div class="text-center"> 
                        <span class=" fontcolor3 "> <i class="la la-file-text-o la10" ></i> </span>
                        <div class="pb-3 pt-1">
                        <h3> ต้องการบันทึกข้อมูล ?</h3> 
                        </div>
                        </div>
                        <div class="col-12 d-flex justify-content-center"> 
                        <div class="row"> 
                            <button type="button" class="btn btn-min-width mb-1 mr-1  round btn-success" id="btncreate_Skill"   >  บันทึก </button>
                            <button class="btn btn-min-width mb-1 mr-1  round btn-danger" role="button"  data-dismiss="modal">  ยกเลิก </button> 
                        </div>
                        </div>
                    </div> 
                    </form>
                </div> 
                </div> 
        </div>
        <!-- ####################################   end modal_createcf_Skill  #################################### -->
        <!-- ####################################   modal_editcf_Skill   #################################### -->
        <div class="modal fade text-left modal_custom1" id="modal_editcf_Skill" tabindex="-1" role="dialog" aria-labelledby="modal_editcf_Skill"  aria-hidden="true">
                <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <form>
                    <div class="my-2 mx-2 px-2 py-1">
                        <div class="text-center"> 
                        <span class=" fontcolor3 "> <i class="la la-file-text-o la10" ></i> </span>
                        <div class="pb-3 pt-1">
                        <h3> ต้องการแก้ไขข้อมูล ?</h3> 
                        </div>
                        </div>
                        <div class="col-12 d-flex justify-content-center"> 
                        <div class="row"> 
                            <button type="button" class="btn btn-min-width mb-1 mr-1  round btn-success" id="btnedit_Skill"   >  บันทึก </button>
                            <button class="btn btn-min-width mb-1 mr-1  round btn-danger" role="button"  data-dismiss="modal">  ยกเลิก </button> 
                        </div>
                        </div>
                    </div> 
                    </form>
                </div> 
                </div> 
        </div>
        <!-- ####################################   end modal_editcf_Skill  #################################### -->
        <!-- ####################### End Modal  ####################### -->


  <?php
     include "../include/footer.php";
   ?> 

 <script type="text/javascript">
       
         
    $(document).ready(function() {
        org.LoadPersonSel('<?php echo $PosPerson['PersonID'];?>','PersonID');
        org.LoadPosPerStatusSel('<?php echo $PosPerson['PosPerStatus'];?>','PosPerStatus'); 
        org.LoadPosPerStatusSel('<?php echo $PosPerson['PosPerStatusPsr'];?>','PosPerStatusPsr'); 
        org.LoadSourceofCommSel('<?php echo $PosPerson['SourceOfCommID'];?>','SourceOfCommID');
        org.LoadSourceofCommSel('<?php echo $PosPerson['SourceOfCommIDPsr'];?>','SourceOfCommIDPsr');
        org.LoadPosStrucAddAmt('<?php echo $PosPerson['PositionID'];?>','PosAddamtTable');

        org.LoadAddamtSel('','AddAmtCode')
        org.LoadSourceofCommSel('','AddamtSourceOfCommID');
        org.LoadPosStrucSkill('<?php echo $PosPerson['PositionID'];?>','PosSkillTable');
        
        ////////////////////////////////////////////////////////////////////////////////
        org.LoadSkillPrefixSel('','PrefixSkillID','<?php echo $datapos['PersonTypeID'];?>');
        org.LoadSkillSel('','SkillNo','<?php echo $datapos['PersonTypeID'];?>');
        org.LoadArmSel('','ArmCode');
        $("#SkillNo").change(function(){   
            org.LoadArmSel( $(this).find(':selected').data("armcode"),'ArmCode');
        });

        ////////////////////////////////////////////////////////////////////////////////

        //AddamtSourceOfCommID AddamtCommDocNo AddamtCommDocDate  AddamtCommEffDate  AddamtCommExpDate

        $("#btneditcf_Addamt").click(function(){   
            $('#modal_editcf_Addamt').modal('show');
            $('#modalAddamt').hide(); 
        });
        $("#btnedit_Addamt").click(function(){
            org.EditPosStrucAddAmt();
        });

        $("#btncreatecf_Addamt").click(function(){   
            $('#modal_createcf_Addamt').modal('show');
            $('#modalAddamt').hide(); 
        });
        $("#btncreate_Addamt").click(function(){
            org.CreatePosStrucAddAmt();
        });
        ///////////////////////////////////////////////////////

        $("#btneditcf_Skill").click(function(){   
            $('#modal_editcf_Skill').modal('show');
            $('#modalSkill').hide(); 
        });
        $("#btnedit_Skill").click(function(){
            org.EditPosStrucSkill();
        });
        //prefixSkillID

    });  

    function CreateAddamt(){

        
            $("#AddAmtCode").val('');
            $("#AddAmtCodeOld").val('');
            $("#PosStrucAddAmt").val('');
            $("#PosStrucRewardAmt").val('');
            $("#NumApprove").val('');
 
            $("#AddamtSourceOfCommID").val('');
            $("#AddamtCommDocNo").val('');

            $("#AddamtCommDocDate").val('');
            $("#AddamtCommEffDate").val('');
            $("#AddamtCommExpDate").val('');
        
           
        $("#btneditcf_Addamt").addClass('hidden');
        $("#btncreatecf_Addamt").removeClass('hidden');
        $('#modalAddamt').modal();
       
    }


 </script> 