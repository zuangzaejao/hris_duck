<div class="card">
    <div class="card-content">
        <div class="card-body">
             
            <input type="hidden" id="dataTableSet" name="dataTableSet" value="0">
            <input type="hidden" id="OrgLevelDIDtxt" name="OrgLevelDIDtxt">
            <input type="hidden" id="OrgTypeIDtxt" name="OrgTypeIDtxt" value="<?php echo $OrgTypeID?>">
            <!-- <a href="./delete.php" class="btn btn-social btn-min-width mb-1 btn_b1"  >
                <span class="la la-trash-o"  ></span> ลบ
            </a> -->
            <table id="PositionTable1" class="table table-bordered table_custom1  table_custom2" >
                <thead> 
                    <tr>
                        <th colspan="5">  ตำแหน่ง </th>
                        <th rowspan="2"> UM </th>
                        <th rowspan="2"> เงินเดือนอัตรา </th>
                        <th rowspan="2"> เหล่า </th>
                        <th colspan="2"> ลชทอ. </th>
                        <th colspan="2"> เงินเดือน </th>
                        <th colspan="2"> วันที่ดำรงตำแหน่ง </th>

                    </tr>
                    <tr>
                        <th> 

                            <!-- <a class="skin-flat"><input type="checkbox" class="checkAll" onclick="toggle(this);" /> </a> -->
                         </th>
                        <th> รหัสจ่ายตรง </th>
                        <th> ยศ </th>
                        <th> ชื่อ - สกุล</th>
                        <th> เลขประจำตัว </th>
                        <!-- <th> UM </th>
                        <th> เงินเดือนอัตรา </th>
                        <th> เหล่า </th> -->
                        <th> หลัก </th>
                        <th> รอง </th>
                        <th> น./ชั้น </th>
                        <th> เบิกลด </th>
                        <th> ตั้งแต่ </th>
                        <th> คำสั่ง </th>

                    </tr>
                </thead>
                <tbody> </tbody>
            </table>

    <!-- 
    <div class="modal fade text-left modal_custom1" id="modalEditPosPerson" tabindex="-1" role="dialog" aria-labelledby="modalPlanPublic"  aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg_custom1">
                <h5 class="modal-title">แก้ไขข้อมูล </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="row ">
                <div class="col-12  px-4">

                    <ul class="nav nav-tabs nav-top-border no-hover-bg mt-2 mb-1"  >
                        <li class="nav-item">
                            <a class="nav-link active" id="base-tab11" data-toggle="tab" aria-controls="tab11" href="#tab11" aria-expanded="true"> ทำเนียบ </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="base-tab12" data-toggle="tab" aria-controls="tab12" href="#tab12" aria-expanded="false"> เงินเพิ่มประจำตำแหน่ง </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="base-tab13" data-toggle="tab" aria-controls="tab13" href="#tab13" aria-expanded="false"> ลชทอ./เลขหมายรายงาน</a>
                        </li>
                    </ul>


                    <div class="tab-content pt-1"> 
                        <div role="tabpanel" class="tab-pane active" id="tab11" aria-expanded="true" aria-labelledby="base-tab11" >

                                <form class="form_custom1">
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-10 m-auto pt-2">   
                                                <div class="row"> 
                                                    <div class="col-md-6 col-10">
                                                        <div class="form-group">
                                                            <label for="provinceaddr"> ที่ตั้งหน่วย : </label>
                                                            <select  id="provinceaddr"  name="provinceaddr" class="form-control block " disabled >
                                                                <option value="1"> กรุงเทพฯ </option>
                                                                <option value="2"> กระบี่  </option>
                                                                <option value="3"> กาฬสินธุ์ </option>
                                                            </select> 
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-10">
                                                        <div class="form-group">
                                                            <label for="positionno"> จำนวน : </label>
                                                            <input type="number" class="form-control text-right" id="positionno" name="positionno" value="15" disabled >  
                                                        </div>
                                                    </div>
                                                </div>  
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="spcfeatures"> เพศ :   </label>
                                                            <div class="row skin icheck_minimal ">  
                                                            <div class="col-sm-12 "> 
                                                                <fieldset class="d-inline-block mr-1" >
                                                                <input type="radio" name="input-radio-3" id="input-radio-11" checked disabled>
                                                                <label for="input-radio-11">ชาย/หญิง</label>
                                                                </fieldset>
                                                                <fieldset class="d-inline-block mr-1" >
                                                                <input type="radio" name="input-radio-3" id="input-radio-12" disabled>
                                                                <label for="input-radio-12">ชาย</label>
                                                                </fieldset>
                                                                <fieldset class="d-inline-block  " >
                                                                <input type="radio" name="input-radio-3" id="input-radio-13" disabled>
                                                                <label for="input-radio-13">หญิง</label>
                                                                </fieldset>
                                                            </div>  
                                                            </div>
                                                        </div>
                                                    </div>   
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="positionname"> ตำแหน่ง : </label>
                                                            <select  id="positionname"  name="positionname" class="form-control block " disabled>
                                                                <option value="1"> นายทหารควบคุมการเบิกจ่าย </option>
                                                                <option value="2"> นายทหารตรวจสอบ  </option>
                                                                <option value="3"> นายทหารนโยบายและแผน </option>
                                                            </select> 
                                                        </div>
                                                    </div> 
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <div  class="m-auto ">
                                            <button type="button" class="btn btn2 btn-success   round"> <i class="fa fa-save"></i> บันทึก </button>
                                            <button type="button" class="btn btn2 btn-danger   round"  data-dismiss="modal"> <i class="fa fa-times-circle-o"></i> ยกเลิก </button> 
                                        </div>
                                    </div>
                                </form>
                        </div>
                    </div>

                    <div class="tab-content pt-1"> 
                        <div role="tabpanel" class="tab-pane active" id="tab12" aria-expanded="true" aria-labelledby="base-tab12" >
                        
                        </div>
                    </div>
                    <div class="tab-content pt-1"> 
                        <div role="tabpanel" class="tab-pane active" id="tab13" aria-expanded="true" aria-labelledby="base-tab13" >
                        <form class="form_custom1">
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-10 m-auto pt-2">   
                                                 
                                                <div class="row"> 
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="positionname"> ชื่ออักษรนำ  : </label>
                                                            <select  id="positionname"  name="positionname" class="form-control block " disabled>
                                                                <option value="1"> นายทหารควบคุมการเบิกจ่าย </option>
                                                                <option value="2"> นายทหารตรวจสอบ  </option>
                                                                <option value="3"> นายทหารนโยบายและแผน </option>
                                                            </select> 
                                                        </div>
                                                    </div> 
                                                </div> 
                                                <div class="row"> 
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="positionname"> เลขหมายรายงาน  : </label>
                                                            <select  id="positionname"  name="positionname" class="form-control block " disabled>
                                                                <option value="1"> นายทหารควบคุมการเบิกจ่าย </option>
                                                                <option value="2"> นายทหารตรวจสอบ  </option>
                                                                <option value="3"> นายทหารนโยบายและแผน </option>
                                                            </select> 
                                                        </div>
                                                    </div> 
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <div  class="m-auto ">
                                            <button type="button" class="btn btn2 btn-success   round"> <i class="fa fa-save"></i> บันทึก </button>
                                            <button type="button" class="btn btn2 btn-danger   round"  data-dismiss="modal"> <i class="fa fa-times-circle-o"></i> ยกเลิก </button> 
                                        </div>
                                    </div>
                                </form>
                        </div>
                    </div>

                </div>
            </div>


        </div>
      </div>
    </div>

    -->
 

        </div>
    </div> 
</div>