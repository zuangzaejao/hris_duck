  <!-- header -->
  <?php include '../include/header.php'; ?>
  <?php include '../../Model/Ducklab/func.php'; ?>
  <?php  
    //Recruitment and appointment
    $menu1 = "RECRNAPPOINT" ;
    $menu2 = "PlanManpowerReg" ;

  ?>
  <!-- menu -->
  <?php include '../include/menu.php'; ?>
  
  <section>
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
          <h3 class="content-header-title">สมัครสอบ  </h3>
        </div>
      </div>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb"> 
          <li class="breadcrumb-item"><a href="index.php">หน้าแรก </a></li>  
            <li class="breadcrumb-item active" aria-current="page">  สมัครสอบ </li>
        </ol>
      </nav>
      <div class="content-body">
         <!-- Bootstrap 3 table -->
         <section id="bootstrap3">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                  <div class="row   "> 
                      <div class=" col-sm-12">
                        <div class="box1 txtdesc">
                            ข้อมูลการสมัครสอบ
                        </div>
                      </div> 
                    </div>
                    <div class="row"> 
                      <div class="col-12">
                        <div class="box_h1 card">
                          <div class="card-header card-head-inverse  ">
                            <h4 class="card-title text-white"> ประวัติ  </h4> 
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                              </ul>
                            </div>
                          </div>
                          <div class="card-content collapse show">
                            <div class="card-body">
                              <div class="row">
                                <div class=" col-md-2 "> 
                                    <img src="/Asset/Images/user1.png" class="img_profile" >
                                </div>
 
                                <div class=" col-md-10 fontcustom2"> 
                                  <div class="row">
                                    <div class=" col-md-12 ">
                                      <p> หมายเลขประจำตัว : 3232665970</p>
                                    </div> 
                                  </div> 
                                  <div class="row">
                                    <div class=" col-md-12 ">
                                      <p> ชื่อ-สกุล : ร.อ. กิตติศักดิ์  สวนสอน </p>
                                    </div> 
                                  </div>
                                  <div class="row">
                                    <div class=" col-md-6 ">
                                      <p> เหล่า : สบ.  </p>
                                    </div> 
                                    <div class=" col-md-6 ">
                                      <p> จำพวก : กำลังพล </p>
                                    </div> 
                                  </div>
                                  <div class="row">
                                    <div class=" col-md-12 ">
                                      <p> สังกัด : กพ.ทอ.  </p>
                                    </div>  
                                  </div>
                                  <div class="row">
                                    <div class=" col-md-12 ">
                                      <p> ตำแหน่ง : ปฏิบัติการเครื่องจักรคำนวณ แผนกวิเคราะห์ </p>
                                    </div>  
                                  </div>
                                  <div class="row">
                                    <div class=" col-md-6 ">
                                      <p> วันเดือนปีที่บรรจุ :  20 มกราคม 2560  </p>
                                    </div> 
                                    <div class=" col-md-6 ">
                                      <p> วันเดือนปียศปัจจุบัน :  20 มกราคม 2560  </p>
                                    </div>   
                                  </div>
                                  <div class="row">
                                    <div class=" col-md-6 ">
                                      <p> คุณวุฒิ :  ปริญญาตรี </p>
                                    </div> 
                                    <div class=" col-md-6 ">
                                      <p> การครองยศ :  -  </p>
                                    </div>   
                                  </div>
                                </div>

                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div> 

                    <div class="row"> 
                      <div class="col-12">
                        <div class="box_h1 card">
                          <div class="card-header card-head-inverse  ">
                            <h4 class="card-title text-white"> <span class=""><u></u> </span>&nbsp;&nbsp; น.ประทวน ทำหน้าที่  </h4> 
                          </div>
                          <div class="card-content collapse show">
                            <div class="card-body">
                              <div class="row">
                                <div class=" col-md-12  m-auto ">
                                  <table id="planManpowerTable3" class="table table-bordered tb1   "  >
                                    <thead>
                                      <tr>
                                        <th>   </th> 
                                        <th> ลำดับที่ </th> 
                                        <th> เหล่าทหาร/จำพวกทหาร </th>
                                        <th > สังกัด</th>
                                        <th> ตำแหน่ง </th> 
                                        <th > ลชทอ.หน้าที่ </th> 
                                        <th >อัตรา</th>  
                                        <th >วันที่สมัคร</th> 
                                      </tr>
                                    </thead>
                                    <tbody> 
                                      <tr> 
                                        <td class="text-center">  </td>
                                        <td class="text-center"> 1 </td>
                                        <td class="text-center"> กง. </td>
                                        <td class="text-center"> กง.ทอ. </td>
                                        <td class="text-left"> นายทหารรายงานและหลักฐาน แปนกรายงานและหลักฐาน กกง.กง.ทอ. </td>
                                        <td class="text-center"> 6713 </td>
                                        <td class="text-center"> ร.อ. </td> 
                                        <td class="text-center"> 1 มี.ค. 2562 </td>
                                      </tr> 
                                      <tr> 
                                        <td class="text-center">  </td>
                                        <td class="text-center"> 2 </td>
                                        <td class="text-center"> กง. </td>
                                        <td class="text-center"> กง.ทอ. </td>
                                        <td class="text-left"> นายทหารบัญชี แผนกการเงิน ชย.ทอ. </td>
                                        <td class="text-center"> 6713 </td>
                                        <td class="text-center"> ร.อ. </td> 
                                        <td class="text-center"> 20 ก.พ. 2561</td>
                                      </tr>
                                    </tbody> 
                                  </table>  
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div> 

                    <div class="row"> 
                      <div class="col-12">
                        <div class="box_h1 card">
                          <div class="card-header card-head-inverse  ">
                            <h4 class="card-title text-white"> <span class=""><u></u> </span>&nbsp;&nbsp; น.ประทวน ป.ตรีปรับ  </h4>  
                          </div>
                          <div class="card-content collapse show">
                            <div class="card-body">
                              <div class="row">
                                <div class=" col-md-12  m-auto ">
                                  <table id="planManpowerTable3" class="table table-bordered tb1   "  >
                                    <thead>
                                      <tr>
                                    
                                        <th> ลำดับที่ </th>
                                        <th style="width:32%;"> คุณวุฒิ</th>
                                        <th> เหล่าทหาร/จาพวก </th>
                                        <th> สังกัด </th>
                                        <th  style="width:32%;"> ตำแหน่ง</th>
                                        <th >ลชทอ.หน้าที่</th> 
                                        <th >เพศ</th> 
                                        <th >อัตรา</th>  
                                        <th > วันที่สมัคร </th> 
                                      </tr>
                                    </thead>
                                    <tbody> 
                                      <tr> 
                                        <td class="text-center"> 1 </td>
                                        <td >   
                                          ปริญญาตรี ในสาขาวิชาพยาบาลศาสตร์  ได้รับใบประกอบวิชาชีพทางการพยาบาลและการผดุงครรภ์
                                        </td>
                                        <td class="text-center"> พ. </td> 
                                        <td class="text-center"> พอ. </td>
                                        <td class=""> รองผู้บังคับหมวดนักเรียนพยาบาลทหารอากาศ ฝ่ายปกครอง แผนกปกครอง วพอ.พอ. </td>
                                        <td class="text-center"> 9713 </td> 
                                        <td class="text-center"> ช </td>
                                        <td class="text-center"> ร.ท. </td>   
                                        <td class="text-center"> 16 ก.พ. 2561 </td>
                                      </tr> 
                                      <tr> 
                                        <td class="text-center"> 2</td>
                                        <td >   
                                        ปริญญาตรี ในสาขาวิชาการบัญชี หรือในสาขาวิชาใดสาขาวิชาหนึ่ง
                                        ทางการบัญชี, ทางการธนาคารและการเงิน,
                                        ทางการบัญชีการเงิน, ทางการบัญชีบริหาร,
                                        ทางระบบสารสนเทศทางการบัญชี
                                        </td>
                                        <td class="text-center"> กง. </td> 
                                        <td class="text-center"> อย. </td>
                                        <td class=""> นายทหารควบคุมการเบิกจ่าย แผนกการเงิน อย. </td>
                                        <td class="text-center"> 6713 </td> 
                                        <td class="text-center"> ช/ญ </td>
                                        <td class="text-center"> ร.ท. </td>   
                                        <td class="text-center"> 16 ก.พ. 2561 </td>
                                      </tr>  
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div> 



                    <div class="row"> 
                      <div class="col-sm-10 m-auto">
                        <div  class="text-center">
                          <button type="button" class="btn btn2 btn-info   round"> <i class="fa fa-print"></i> ปริ้น </button>
                          <button type="button" class="btn btn2 btn-success   round"> <i class="fa fa-save"></i> บันทึก </button>
                          <button type="button" class="btn btn2 btn-danger   round"  data-dismiss="modal"> <i class="fa fa-times-circle-o"></i> ยกเลิก </button> 
                        </div>
                      </div>
                    </div>



                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--/ Bootstrap 3 table -->
      </div>
    </div>
  </div>
  </section>

 
  <!-- footer -->
  <?php include '../include/footer.php'; ?> 

  <script src="../../Controllers/planManpowerController.js"></script>
  <script type="text/javascript">
    $(document).ready(function() { 

    }); 
  </script>
  