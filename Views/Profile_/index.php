  <!-- header -->
<?php
    require_once "../../config.php";
    require_once '../../Model/Ducklab/func.php';
    require_once '../include/header.php';
  
    $menu1 ="HRTOFFICIAL" ;
    $menu2 ="PROFILE"; 
?>

<?php include '../include/menu.php'; ?>

<?php  

  if( $_POST['action']== "search")
  { 
      $where ="";
      $check_ops = 0;
        //---------- OPS-1
          if(!empty($_POST['HrtAirForceID'])){
              $check_ops = 1;
              $where.= " HrtAirForceID LIKE '%".$_POST['HrtAirForceID']."%' ";
          }
        //---------- OPS-2
          if(!empty($_POST['HrtPerCardID']))
          {
              if($check_ops > 0){
                  $check_ops = 2;
                  $where.= " AND HrtPerCardID LIKE '%".$_POST['HrtPerCardID']."%' ";
              } else {
                  $check_ops = 1;
                  $where.= " HrtPerCardID LIKE '%".$_POST['HrtPerCardID']."%' ";
              }
          }
        //---------- OPS-3
          if(!empty($_POST['HrtName']))
          {
              if($_POST['HrtName'][0] == '*' && $check_ops > 0){ 
                  $check_ops = 3;
                  $where.= " AND HrtName LIKE '%".substr($_POST['HrtName'],1)."%' ";
              } elseif($_POST['HrtName'][0] == '*'){
                  $check_ops = 1;
                  $where.= " HrtName LIKE  '%".substr($_POST['HrtName'],1)."%' ";
              } elseif(substr($_POST['HrtName'],-1) == '*' && $check_ops > 0){
                  $check_ops = 3;
                  $exp_sn = explode("*", $_POST['HrtName']);
                  $where.= " AND HrtSurName LIKE '%".$exp_sn[0]."%' ";
              } elseif(substr($_POST['HrtName'],-1) == '*'){
                  $check_ops = 1;
                  $exp_sn = explode("*", $_POST['HrtName']);
                  $where.= " HrtSurName LIKE '%".$exp_sn[0]."%' ";
              }
          }
        //---------- OPS-4
          if(!empty($_POST['HrtSoundDex']))
          {
              if($check_ops > 0){
                  $check_ops = 4;
                  $where.= " AND HrtSoundDex LIKE  '%".$_POST['HrtSoundDex']."%' ";
              } else {
                  $check_ops = 1;
                  $where.= " HrtSoundDex LIKE  '%".$_POST['HrtSoundDex']."%' ";
              }
          }
        //---------- OPS-5
          if(!empty($_POST['set_HrtPersonTypeName']))
          {
              if($check_ops > 0){
                  $check_ops = 5;
                  $where.= " AND HrtPersonTypeID = '".$_POST['set_HrtPersonTypeName']."' ";
              } else {
                  $check_ops = 1;
                  $where.= " HrtPersonTypeID = '".$_POST['set_HrtPersonTypeName']."' ";
              }
          }
  
          // if( !empty($_POST['HrtRankNameTh']) ){
          //    $check_ops=1;
          //    $where.= " AND HrtRankNameTh LIKE  '%".$_POST['HrtRankNameTh']."%' ";
          // }
  
          // if( !empty($_POST['HrtRankNameTh']) ){
          //    $check_ops=1;
          //    $where.= " AND HrtRankNameTh LIKE  '%".$_POST['HrtRankNameTh']."%' ";
          // }
  

          if($check_ops > 0){
              $sql_search ="SELECT HrtPersonID,HrtAirForceID,HrtSoundDex,HrtRankID,HrtName,HrtSurName,HrtPersonTypeID
              FROM HrtPerson where ".$where." ";

              $query_search = sqlsrv_query($conn, $sql_search );
              $data_search = array();

              while($row=sqlsrv_fetch_array($query_search, SQLSRV_FETCH_ASSOC )){
                  $data_search[] = $row ; 
              }
              //echo $sql;
          }else{
              
          }
      //---------- ---------- ---------- ----------//
  }
/*
    $sql04 = "SELECT HrtPosCode,HrtPosName From HrtPos where 1=1 ";

    $query04 = sqlsrv_query($conn, $sql04 );
    $data04=array();

    while($row04=sqlsrv_fetch_array($query04, SQLSRV_FETCH_ASSOC )){
        $data04[] = $row04 ; 
    }
*/
?>

  <section>

      <div class="app-content content">
        <div class="content-wrapper">

          <div class="content-header row">
            <div class="content-header-left col-12 mb-2">
              <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
              <h3 class="content-header-title">ข้อมูลบุคคล <?php //echo $sql_search; ?></h3>
            </div>
          </div>

          <div class="content-body">            
            <section id="bootstrap3">
              <div class="row">
                <div class="col-12">
                  <div class="card">
                    <div class="card-content collapse show">
                      <div class="card-body card-dashboard">
                        <p class="card-text"></p>
                        <nav aria-label="breadcrumb">
                          <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../home/index.php">งานประวัติข้าราชการ</a></li>
                            <li class="breadcrumb-item active" aria-current="page">ข้อมูลบุคคล</li>
                          </ol>
                        </nav>
                        <div class="content-body">
                          <section class="horizontal-grid" id="horizontal-grid">
                            <div class="row">
                              <div class="col-md-12">
                                <div class="box_h1 card">
                                  <div class="card-header card-head-inverse">
                                    <h4 class="card-title text-white"> ค้นหา </h4> 
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                      <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                                      </ul>
                                    </div>
                                  </div>

                                  <div class="card-content collapse show">
                                    <div class="card-body">
          <!----------------------------------------------------------------------------------------------------------------->
          <form action="" method="post" id="frmsearch">
            
              <div class="form-body">

                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">เลขบัตรประจำตัวราชการ:
                      <input type="text" class="form-control"  placeholder=" " name="HrtAirForceID" id="HrtAirForceIDe"  value="<?php echo $_POST['HrtAirForceID'];?>" >
                    </div>
                  </div>
        
                  <div class="col-md-4">
                    <div class="form-group">เลขประจำตัวบัตรประชาชน:
                      <input type="text" class="form-control" placeholder="" name="HrtPerCardID" id="HrtPerCardID" value="<?php echo $_POST['HrtPerCardID'];?>">
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="form-group">ชื่อ - นามสกุล:
                      <input type="text" class="form-control" placeholder=" " name="HrtName" id="HrtName" value="<?php echo $_POST['HrtName'];?>">
                    </div>
                  </div> 
                </div>                    

                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">หมายเลขแฟ้มประวัติ:
                      <input type="text" class="form-control" placeholder="" name="HrtSoundDex" id="HrtSoundDex" value="<?php echo $_POST['HrtSoundDex'];?>">
                    </div>
                  </div>              

                  <div class="col-md-4">
                    <div class="form-group">ประเภทกำลังพล:
                      <select  class="select2 form-control" style="width: 100%;" name="set_HrtPersonTypeName" id="set_HrtPersonTypeName" > 
                          <option value="">  กรุณาเลือกประเภทกำลังพล  </option>
                          <?php
                              $sql_4ops_5 = "SELECT HrtPersonTypeID,HrtPersonTypeName From HrtPersonType where 1=1 ";

                                  $query_4ops_5 = sqlsrv_query($conn, $sql_4ops_5 );
                                  $data_4ops_5=array();
                              
                                  while($row_4ops_5=sqlsrv_fetch_array($query_4ops_5, SQLSRV_FETCH_ASSOC )){
                                      $data_4ops_5[] = $row_4ops_5 ; 
                                  } 
                              if($data_4ops_5){
                                  foreach($data_4ops_5 as $key => $val ){ //HrtPosCode,HrtPosName 
                                  ?>
                                <option value="<?php echo $val['HrtPersonTypeID']; ?>"
                                <?php if( $val['HrtPersonTypeID'] == $_POST['set_HrtPersonTypeName']) { echo "selected"; } ?> > <?php echo $val['HrtPersonTypeName']; ?>
                                </option>
                                  <?php
                                  }
                              } 
                          ?> 
                      </select>
                    </div>
                  </div>        

                  <!--<div class="col-md-4">
                    <div class="form-group">ตำแหน่ง:
                      <select  class="select2 form-control"style="width: 100%;" name="set_HrtPosName" id="set_HrtPosName" >
                          <option value="">  กรุณาเลือกประเภทตำแหน่ง  </option>
                          <?php /*
                              if($data04){
                                  foreach($data04 as $key4 => $val4 ){//HrtPosCode,HrtPosName
                                  ?>
                                <option value="<?php echo $val4['HrtPosCode']; ?>"
                                <?php if( $val4['HrtPosCode'] == $_POST['set_HrtPosName']) { echo "selected"; }?> > <?php   echo $val4['HrtPosName'] ;?>
                                </option>
                                  <?php
                                  }
                              }   */
                          ?> 
                      </select>
                    </div>-->
                  </div>
                </div>

                <div class="text-center">
                  <input type="hidden" name="action" value="search">
                  <button type="button" class="btn round btn1 btncustom1 mr-1 " id="btnsubmit" > 
                    <i class="fa fa-search"></i> ค้นหา
                  </button>
                  <button type="reset" class="btn round btn1 btncustom1" name="repeat" id="repeat">
                    <i class=" fa fa-repeat"></i> ล้างค่า
                  </button>
                </div>
          </form>
          <!----------------------------------------------------------------------------------------------------------------->
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </div>
                          </section>
                        </div>

      <br>
      <a href="./create.php" class="btn btn-social btn-min-width mb-1"
          style="background-color:#0f1733; color:white;">
          <span class="la la-plus-circle"
              style="color:white; font-weight: bold;font-size: 18px"></span> เพิ่ม
      </a>
      <a href="./delete.php" class="btn btn-social btn-min-width mb-1"
          style="background-color:#0f1733; color:white;">
          <span class="la la-trash-o"
              style="color:white; font-weight: bold;font-size: 18px"></span> ลบ
      </a>
<!-- ---------- ---------- ---------- ---------- ---------- Show DATA ---------- ---------- ---------- ---------- ---------- -->
      <br><br>
      <table id="PersonTable"  class="table table-striped table-borderless table-hover table_custom1">
          <thead>
            <tr>
              <th><input type="checkbox" class="checkAll" onclick="toggle(this);" /></th>
              <th></th>
              <th>ลำดับที่</th>
              <th>เลขประจำตัวข้าราชการ</th>
              <th>หมายเลขแฟ้ม</th>
              <th>ยศ</th>
              <th>ชื่อ</th>
              <th>นามสกุล</th>
              <th>ตำแหน่ง</th>
              <th>สถานะ</th>
            </tr>
          </thead>
          <tbody >
              <?php 
                if($data_search)
                {
                    $i=1; 
                    foreach( $data_search as $key => $val )
                    {
                        $sql_rank_name = " SELECT HrtRankNameTh 
                                            FROM HrtRank
                                            WHERE HrtRankID = '". $val['HrtRankID']."' ";

                        $query_rank_name = sqlsrv_query($conn, $sql_rank_name );
                        $data_rank_name = array();

                        while($row_rank_name = sqlsrv_fetch_array($query_rank_name, SQLSRV_FETCH_ASSOC )){
                          $data_rank_name[] = $row_rank_name ; 
                        }
                        
                        $sql_pos_name = " SELECT HrtPos.HrtPosName 
                                          FROM HrtPos left join HrtPosPerson on HrtPos.HrtPosCode = HrtPosPerson.HrtPosCode 
                                          WHERE HrtPosPerson.HrtPersonID = '". $val['HrtPersonID']."' ";

                        $query_pos_name = sqlsrv_query($conn, $sql_pos_name );
                            if( $query_pos_name === false ) {
                              die( print_r( sqlsrv_errors(), true));
                            }
                        $data_pos_name = array();

                        while($row_pos_name = sqlsrv_fetch_array($query_pos_name, SQLSRV_FETCH_ASSOC )){
                            $data_pos_name[] = $row_pos_name ; 
                        }

                        ?>   

                        <tr> 
                          <td><input type="checkbox" class="checkAll" onclick="toggle(this);" /></td>
                          <td>
                            <a href="./detail.php?PersonID=<?php echo $val1['HrtPersonID']  ;?>"><i class="la la-pencil-square-o"
                                style="color:#0f1733;"></i></a>
                            <a href="./delete.php"><i class="la la-trash-o"
                                style="color:#0f1733;"></i></a>
                          </td>
                          <td><?php echo $i; ?></td> 
                          <td><?php echo $val['HrtAirForceID']; ?></td> 
                          <td><?php echo $val['HrtSoundDex']; ?></td> 
                          <td>
                            <?php
                            foreach($data_rank_name as $key_rn => $val_rn) { echo  $val_rn['HrtRankNameTh']; } 
                            ?>
                          </td> 
                          <td><?php echo $val['HrtName']; ?></td> 
                          <td><?php echo $val['HrtSurName']; ?></td> 
                          <td>  
                            <?php 
                            foreach($data_pos_name as $key_pn => $val_pn ){ echo  $val_pn['HrtPosName'] ; }
                            ?>
                          </td> 
                          <td align="center">
                            <input type="checkbox" checked data-toggle="toggle"
                              data-style="ios" data-on="ปกติ" data-off="ยกเลิก"
                              data-onstyle="success" data-offstyle="danger"
                              data-size="sm">
                          </td>                     
                        </tr>

                        <?php
                        $i++;
                    } 
                } 
              ?>        
          </tbody>
      </table>
<!-- ---------- ---------- ---------- ---------- ---------- End DATA ---------- ---------- ---------- ---------- ---------- -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>

        </div>
      </div>

  </section>
  
  <!-- footer -->
<?php include '../include/footer.php'; ?>
<script src="../../Controllers/Ducklab/duck.script.js"></script> 
<script src="../../Controllers/Ducklab/contents.script.js"></script> 
<script type="text/javascript">
    $(document).ready(function() { 

      $("#btnsubmit").click(function() {
      // alert( "Handler for .click() called." );
         $("#frmsearch").submit();
      });
     
     $('#PersonTable').DataTable( {
   
      } );
    }); 
</script>
  
