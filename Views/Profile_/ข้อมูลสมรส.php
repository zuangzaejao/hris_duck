<!-- header -->
<?php include '../include/header.php'; ?>
<?php include '../../Model/Ducklab/func.php'; ?> 

<?php  
/*require_once "../../config.php";*/

$menu1 ="HRTOFFICIAL" ;
$menu2 ="ORGSTRUCDATA";
$menu3 ="PROFILE"; 
?>
<!-- menu -->
<?php include '../include/menu.php'; ?>


<section>
<div class="app-content content">
<div class="content-wrapper">

<div class="content-header row">
<div class="content-header-left col-12 mb-2">
<div class="linetitle" ></div>
<h3 class="content-header-title">ข้อมูลประวัติราชการ</h3>
<nav aria-label="breadcrumb">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="../home/index.php">งานประวัติรับราชการ</a></li>
<li class="breadcrumb-item active" aria-current="page">   ข้อมูลตำแหน่ง </li>
</ol>
</nav>
<br>
<div class="content-body">
<section id="bootstrap3">
<div class="row">
<div class="col-12">
<div class="card">
<div class="card-content collapse show">
<div class="card-body card-dashboard">

<!-- เริ่ม--กล้องข้อมูลคนรูป -->
<div class="card">

<div class="card-header" style="background-color:#0f1733;padding:1.0rem 1.0rem">
<h6 class="card-title text-white">
พล.อ.ธานินทร์ จันทร์อังคารพุธ
หมายเลขประจำตัว
332124562455
</h6>

<a class="heading-elements-toggle"><i
class="la la-ellipsis-v font-medium-3"></i></a>
<div class="heading-elements">
<ul class="list-inline mb-0">
<li><a data-action="collapse"><i
class="ft-minus"></i></a>
</li>
</ul>
</div>
</div>
</div>
<!-- จบ--กล้องข้อมูลคนรูป -->
<!-- เริ่ม--กล้องข้อมูลคนเนื้อหา -->
<div class="profile"
style="background-color:#f4f5fa; color:whitesmoke;padding:1%; height: 180px;width: 100%;">
<div class="image"
style="background-color:#BEBEBE;height: 160px;width: 12%;">

<div class="profile_data"
style="height: 160px;width: 450px;margin-left:150px;padding:1%;">
<p class="text">
<h6>หมายเลขประจำตัว : 12345678888</h6>
<h6>ยศ : ร.อ.</h6>
<h6>เหล่า : สบ.</h6>
<h6>สังกัด : กพ.ทอ.</h6>
<h6>ตำแหน่ง : ปฏิบัติการเครื่องจักรคำนวณ แผนกวิเคราะห์
</h6>
</p>
</div>
</div>
</div>
<!-- จบ--กล้องข้อมูลคนเนื้อหา -->
<div class="profile_head"
style="background-color:#f5f5f5;height: auto;width: 100%;margin-left:0px;padding:1%;">
<br>
<div class="card">
<div class="card-header">
<h5 class="card-title">ประวัติข้าราชการ</h5>
</div>
<div class="card-content">
<div class="card-body">
<?php require_once '../include/profile_table.php'; ?>

<div class="tab-pane" id="tab14"
aria-labelledby="base-tab14">
<div class="container">
<div class="card-content">
<div class="card-body">
  <!-----------------------------////////////////////////////////--------------------------------->   
    <div class="card collapse-icon accordion-icon-rotate active">
        <div id="headingCollapse35" class="card-header bg-success">
            <a data-toggle="collapse"
                href="#collapse35"
                aria-expanded="true"
                aria-controls="collapse35"
                class="card-title lead white">
                <h6><U>ส่วนที่ 1</U>
                  ข้อมูลคู่สมรส 
                </h6>
            </a>
        </div>
        <div id="collapse35"
           role="tabpanel"
           aria-labelledby="headingCollapse35"
            class="card-collapse collapse show"
            aria-expanded="true">
            <div class="card-content">
  <!-----------------------------////////////////////////////////--------------------------------->   
        <div class="row">
             <div class="col-md-6">
                <div class="card-body" id="File_number">
                    ลำดับคู่สมรส :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-body" id="File_number">
                    หมายเลขประจำตัว :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>
            </div>
        </div>
            <!-----------------------------////////////////////////////////--------------------------------->   
        <div class="row">           
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body" id="noncomm_and_comm">
                        ยศ :
                        <select
                            class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศโท/พล.อ.ท.
                                </option>                                        
                            </optgroup>                          
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body" id="noncomm_and_comm">
                        คำนำหน้าชื่อ :
                        <select
                            class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศโท/พล.อ.ท.
                                </option>                                        
                            </optgroup>                          
                        </select>
                    </div>
                </div>
            </div>
       
        </div>
        <!-----------------------------////////////////////////////////--------------------------------->   
        <div class="row">           
            <div class="col-md-6">  
                <div class="card-body" id="gender"> เพศ:
                    <div class="card-content">
                    <div class="card-body">
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio1">
                            <label
                                class="custom-control-label"
                                for="radio1">เพศชาย</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio2"
                                checked>
                            <label
                                class="custom-control-label"
                                for="radio2"
                                checked>เพศหญิง</label>
                             </div>
                        </div>
                    </div>
                </div>
                </div>
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body" id="noncomm_and_comm">
                        เลขประจำตัวประชาชน :
                        <select
                            class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศโท/พล.อ.ท.
                                </option>                                        
                            </optgroup>                          
                        </select>
                    </div>
                </div>
            </div>     
        </div>
    <!-----------------------------////////////////////////////////--------------------------------->   
       <div class="row">           
         <div class="col-md-6">
                <div class="card-body" id="File_number">
                    ชื่อ :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-body" id="File_number">
                    นามสกุลที่ใช้ปัจจุบัน :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>
            </div>
        </div>
        <!-----------------------------////////////////////////////////--------------------------------->           
     <div class="row">   
    
        <div class="col-md-6">
                <div class="card-body" id="File_number">
                    สกุลเดิม :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>
            </div>
         </div>

    <!-----------------------------////////////////////////////////--------------------------------->    
    <div class="row">           
    <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body" id="noncomm_and_comm">
                        เชื่อชาติ :
                        <select
                            class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศโท/พล.อ.ท.
                                </option>                                        
                            </optgroup>                          
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body" id="noncomm_and_comm">
                        สัญชาติ :
                        <select
                            class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศโท/พล.อ.ท.
                                </option>                                        
                            </optgroup>                          
                        </select>
                    </div>
                </div>
            </div>
       
         </div>

    <!-----------------------------////////////////////////////////--------------------------------->  
    <div class="row">           
    <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body" id="noncomm_and_comm">
                        ศาสนา :
                        <select
                            class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศโท/พล.อ.ท.
                                </option>                                        
                            </optgroup>                          
                        </select>
                    </div>
                </div>
            </div>   
            <div class="col-md-6">
                <div class="card-block">
                    <div class="input-group">
                        วัน เดือน ปี เกิดของคู่สมรส :
                        <input
                            type="text"
                            class="form-control pickadate-disable-dates"
                            placeholder="วันปลดประจำการ"
                            aria-describedby="button-addon4">
                        <div
                            class="input-group-append">
                            <button
                                class="btn btn-primary"
                                type="button"
                                style=" padding-bottom: 1px; padding-top: 1px;"><i
                                class="la la-calendar-o"></i></button>
                        </div>
                    </div>
                </div>
            </div>
   
         </div>  
     <!-----------------------------////////////////////////////////--------------------------------->  
     <div class="col-md-6">  
                <div class="card-body" id="gender"> สถานภาพ:
                <a href="../Organizations/create.php"><i class="la la-plus-circle" style="font-size:36px;color:#0f1733;"></i></a>
                <div class="card-content">
                    
                    <div class="card-body">
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio1">
                            <label
                                class="custom-control-label"
                                for="radio1">สมรส</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio2"
                                checked>
                            <label
                                class="custom-control-label"
                                for="radio2"
                                checked>หย่า</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio2"
                                checked>
                            <label
                                class="custom-control-label"
                                for="radio2"
                                checked>เสียชีวิต</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio2"
                                checked>
                            <label
                                class="custom-control-label"
                                for="radio2"
                                checked>สูญหาย</label>
                        </div>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio2"
                                checked>
                            <label
                                class="custom-control-label"
                                for="radio2"
                                checked>สมรสต่างประเทศ/ไม่สามารถระบุหน่วยที่ออกหลักฐาน</label>
                        </div>
                    </div>
                </div>
                </div>
       <!-----------------------------////////////////////////////////--------------------------------->           
     <div class="row">   
    
    <div class="col-md-6">
            <div class="card-body" id="File_number">
               หลักฐานการสมรส :
                <input
                    class="input form-control"
                    style="width: 100%;"
                    placeholder=" ">
            </div>
        </div>
        <div class="col-md-6">
                <div class="card-block">
                    <div class="input-group">
                        ลงวันที่ : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input
                            type="text"
                            class="form-control pickadate-disable-dates"
                            placeholder="วันปลดประจำการ"
                            aria-describedby="button-addon4">
                        <div
                            class="input-group-append">
                            <button
                                class="btn btn-primary"
                                type="button"
                                style=" padding-bottom: 1px; padding-top: 1px;"><i
                                class="la la-calendar-o"></i></button>
                        </div>
                    </div>
                </div>
            </div>
     </div>

<!-----------------------------////////////////////////////////--------------------------------->
<div class="row">   
  <div class="col-md-3">
                <div class="card-block">
                    <div class="card-body" id="noncomm_and_comm">
                        ออกโดย จังหวัด :
                        <select
                            class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศโท/พล.อ.ท.
                                </option>                                        
                            </optgroup>                          
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card-block">
                    <div class="card-body" id="noncomm_and_comm">
                        ออกโดย อำเภอ :
                        <select
                            class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศโท/พล.อ.ท.
                                </option>                                        
                            </optgroup>                          
                        </select>
                    </div>
                </div>
            </div>
        
        <div class="col-md-6">
                <div class="card-block">
                    <div class="input-group">
                        แนบไฟล์หลักฐานการสมรส
                        <input
                            type="text"
                            class="form-control pickadate-disable-dates"
                            placeholder="วันปลดประจำการ"
                            aria-describedby="button-addon4">
                        <div
                            class="input-group-append">
                            <button
                                class="btn btn-primary"
                                type="button"
                                style=" padding-bottom: 1px; padding-top: 1px;"><i
                                class="la la-calendar-o"></i></button>
                        </div>
                    </div>
                </div>
            </div>
     </div>

<!-----------------------------////////////////////////////////--------------------------------->
<!-----------------------------////////////////////////////////--------------------------------->
<div class="row">   
    
    <div class="col-md-6">
    <div class="card-body">
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio1">
                            <label
                                class="custom-control-label"
                                for="radio1">หย่าต่างประเทศ/ไม่สามารถระบุหน่วยที่ออกหลักฐาน</label>
                        </div>        
    </div>
    </div>
    </div> 

<!-----------------------------////////////////////////////////--------------------------------->
 
<!-----------------------------////////////////////////////////--------------------------------->   
        </div>
        </div>
    <br>
  <!-----------------------------////////////////////////////////--------------------------------->   
    <!-- ----ส่วนที่1------      -->
    
  <!-----------------------------////////////////////////////////--------------------------------->   

<div class="card collapse-icon accordion-icon-rotate active">
    <div id="headingCollapse36" class="card-header bg-success">
        <a data-toggle="collapse"
            href="#collapse36"
            aria-expanded="true"
            aria-controls="collapse36"
            class="card-title lead white">
            <h6><U>ส่วนที่ 2</U>
                ข้อมูลบิดา
            </h6>
        </a>
    </div>
        
  <!-----------------------------////////////////////////////////---------------------------------> 
    <div id="collapse36"
        role="tabpanel"
        aria-labelledby="headingCollapse36"
        class="card-collapse collapse show"
        aria-expanded="true">
        <div class="card-content">   
            
  <!-----------------------------////////////////////////////////--------------------------------->         
    <div class="row">  
        <div class="col-md-3">
            <div class="card-body" id="File_number">
               ลำดับบุตร-ธิดา :
                <input
                    class="input form-control"
                    style="width: 100%;"
                    placeholder=" ">
            </div>
            
        </div>
        
        <div class="col-md-3">
          
        </div>
        <div class="col-md-6">  
                <div class="card-body" id="gender"> เพศ:
                    <div class="card-content">
                    <div class="card-body">
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio1">
                            <label
                                class="custom-control-label"
                                for="radio1">เพศชาย</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio2"
                                checked>
                            <label
                                class="custom-control-label"
                                for="radio2"
                                checked>เพศหญิง</label>
                             </div>
                        </div>
                    </div>
                </div>
                </div>
        </div>
        <!-----------------------------////////////////////////////////---------------------------------> 
        <div class="row">  
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body ">
                        หมายเลขประจำตัวราชการ :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body ">
                        ยศ :
                        <select
                            class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>                 
                            </optgroup>                                   
                        </select>
                    </div>
                </div>
            </div>
         </div>            
  <!-----------------------------////////////////////////////////---------------------------------> 
  <div class="row">  
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body ">
                        เลขประจำตัวประชาชน :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body ">
                        คำนำหน้าชื่อ :
                        <select
                            class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>                 
                            </optgroup>                                   
                        </select>
                    </div>
                </div>
            </div>
         </div>            
  <!-----------------------------////////////////////////////////---------------------------------> 
  <div class="row">  
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body ">
                        ชื่อ :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body ">
                        นามสกุล :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
         </div>       
         
    <!-----------------------------////////////////////////////////--------------------------------->    
    <div class="row">           
    <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body" id="noncomm_and_comm">
                        เชื่อชาติ :
                        <select
                            class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศโท/พล.อ.ท.
                                </option>                                        
                            </optgroup>                          
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body" id="noncomm_and_comm">
                        สัญชาติ :
                        <select
                            class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศโท/พล.อ.ท.
                                </option>                                        
                            </optgroup>                          
                        </select>
                    </div>
                </div>
            </div>
       
         </div>

    <!-----------------------------////////////////////////////////--------------------------------->  
    <div class="row">           
    <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body" id="noncomm_and_comm">
                        ศาสนา :
                        <select
                            class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศโท/พล.อ.ท.
                                </option>                                        
                            </optgroup>                          
                        </select>
                    </div>
                </div>
            </div>   
       
   
         </div>  
     <!-----------------------------////////////////////////////////--------------------------------->  
 <!-----------------------------////////////////////////////////---------------------------------> 
      <hr>
  <!-----------------------------////////////////////////////////--------------------------------->      
  <div class="row">   
        <div class="col-md-12">  
                <div class="card-body" id="gender"> สถานภาพบุตร-ธิดา :
                    <div class="card-content">
                    <div class="card-body">
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio1">
                            <label
                                class="custom-control-label"
                                for="radio1">บุตร</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio2"
                                checked>
                            <label
                                class="custom-control-label"
                                for="radio2"
                                checked>รับรองบุตร</label>
                             </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio2"
                                checked>
                            <label
                                class="custom-control-label"
                                for="radio2"
                                checked>บุตรบุญธรรม</label>
                             </div>
                        </div>
                    </div>
                </div>
                </div>
        </div>
  <!-----------------------------////////////////////////////////---------------------------------> 
  <div class="row">  
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body ">
                        หลักฐานการเกิด :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body ">
                        เลขที่ :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
         </div>  
    <!-----------------------------////////////////////////////////---------------------------------> 
    <div class="row">
    <div class="col-md-6">
                <div class="card-block">
                    <div class="input-group">
                        ลงวันที่ :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input
                            type="text"
                            class="form-control pickadate-disable-dates"
                            placeholder="วันปลดประจำการ"
                            aria-describedby="button-addon4">
                        <div
                            class="input-group-append">
                            <button
                                class="btn btn-primary"
                                type="button"
                                style=" padding-bottom: 1px; padding-top: 1px;"><i
                                class="la la-calendar-o"></i></button>
                        </div>
                    </div>
                </div>
            </div>
                
            <div class="col-md-3">
                <div class="card-block">
                    <div class="card-body ">
                        จังหวัด :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
                
            <div class="col-md-3">
                <div class="card-block">
                    <div class="card-body ">
                        อำเภอ :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
            </div>
      <!-----------------------------////////////////////////////////---------------------------------> 
      <div class="row">
    <div class="col-md-6">
                <div class="card-block">
                    <div class="input-group">
                        วันเดือนปีเกิด :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input
                            type="text"
                            class="form-control pickadate-disable-dates"
                            placeholder="วันปลดประจำการ"
                            aria-describedby="button-addon4">
                        <div
                            class="input-group-append">
                            <button
                                class="btn btn-primary"
                                type="button"
                                style=" padding-bottom: 1px; padding-top: 1px;"><i
                                class="la la-calendar-o"></i></button>
                        </div>
                    </div>
                </div>
            </div>     
     
    <div class="col-md-6">
                <div class="card-block">
                    <div class="input-group">
                        แนบไฟล์หลักฐาน :
                        <input
                            type="text"
                            class="form-control pickadate-disable-dates"
                            placeholder="วันปลดประจำการ"
                            aria-describedby="button-addon4">
                        <div
                            class="input-group-append">
                            <button
                                class="btn btn-primary"
                                type="button"
                                style=" padding-bottom: 1px; padding-top: 1px;"><i
                                class="la la-calendar-o"></i></button>
                        </div>
                    </div>
                </div>
            </div>     
            </div> 
        <!-----------------------------////////////////////////////////--------------------------------->
        <div class="row">   
        <div class="col-md-12">  
                <div class="card-body" id="gender"> สถานภาพ :
                    <div class="card-content">
                    <div class="card-body">
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio1">
                            <label
                                class="custom-control-label"
                                for="radio1">ปกติ</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio2"
                                checked>
                            <label
                                class="custom-control-label"
                                for="radio2"
                                checked>สูญเสีย</label>
                             </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio2"
                                checked>
                            <label
                                class="custom-control-label"
                                for="radio2"
                                checked>ยกเลิก</label>
                             </div>
                        </div>
                    </div>
                </div>
                </div>
        </div>
        <!-----------------------------////////////////////////////////--------------------------------->
        <!-----------------------------////////////////////////////////--------------------------------->
        <!-----------------------------////////////////////////////////--------------------------------->


        <!-----------------------------////////////////////////////////--------------------------------->
             </div>  
        </div>
    </div>
    
    <!-- ----ส่วนที่2--- -->
  <!-----------------------------////////////////////////////////---------------------------------> 
    <div class="card collapse-icon accordion-icon-rotate active">
        <div id="headingCollapse37" class="card-header bg-success">
            <a data-toggle="collapse"
                href="#collapse37"
                aria-expanded="true"
                aria-controls="collapse37"
                class="card-title lead white">
                <h6><U>ส่วนที่ 3</U>
                    ข้อมูลมารดา 
                </h6>
            </a>
        </div>
      <!-----------------------------////////////////////////////////---------------------------------> 
        <div id="collapse37"
            role="tabpanel"
            aria-labelledby="headingCollapse37"
            class="card-collapse collapse show"
            aria-expanded="true">
            <div class="card-content">
            
       <!-----------------------------////////////////////////////////---------------------------------> 
       <div class="row">  
        <div class="col-md-3">
            <div class="card-body" id="File_number">
               ลำดับบุตร-ธิดา :
                <input
                    class="input form-control"
                    style="width: 100%;"
                    placeholder=" ">
            </div>
            
        </div>
        
        <div class="col-md-3">
          
        </div>
        <div class="col-md-6">  
                <div class="card-body" id="gender"> เพศ:
                    <div class="card-content">
                    <div class="card-body">
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio1">
                            <label
                                class="custom-control-label"
                                for="radio1">เพศชาย</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio2"
                                checked>
                            <label
                                class="custom-control-label"
                                for="radio2"
                                checked>เพศหญิง</label>
                             </div>
                        </div>
                    </div>
                </div>
                </div>
        </div>
        <!-----------------------------////////////////////////////////---------------------------------> 
        <div class="row">  
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body ">
                        หมายเลขประจำตัวราชการ :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body ">
                        ยศ :
                        <select
                            class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>                 
                            </optgroup>                                   
                        </select>
                    </div>
                </div>
            </div>
         </div>            
  <!-----------------------------////////////////////////////////---------------------------------> 
  <div class="row">  
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body ">
                        เลขประจำตัวประชาชน :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body ">
                        คำนำหน้าชื่อ :
                        <select
                            class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>                 
                            </optgroup>                                   
                        </select>
                    </div>
                </div>
            </div>
         </div>            
  <!-----------------------------////////////////////////////////---------------------------------> 
  <div class="row">  
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body ">
                        ชื่อ :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body ">
                        นามสกุล :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
         </div>       
         
    <!-----------------------------////////////////////////////////--------------------------------->    
    <div class="row">           
    <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body" id="noncomm_and_comm">
                        เชื่อชาติ :
                        <select
                            class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศโท/พล.อ.ท.
                                </option>                                        
                            </optgroup>                          
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body" id="noncomm_and_comm">
                        สัญชาติ :
                        <select
                            class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศโท/พล.อ.ท.
                                </option>                                        
                            </optgroup>                          
                        </select>
                    </div>
                </div>
            </div>
       
         </div>

    <!-----------------------------////////////////////////////////--------------------------------->  
    <div class="row">           
    <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body" id="noncomm_and_comm">
                        ศาสนา :
                        <select
                            class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศโท/พล.อ.ท.
                                </option>                                        
                            </optgroup>                          
                        </select>
                    </div>
                </div>
            </div>   
       
   
         </div>  
     <!-----------------------------////////////////////////////////--------------------------------->  
 <!-----------------------------////////////////////////////////---------------------------------> 
      <hr>
  <!-----------------------------////////////////////////////////--------------------------------->      
  <div class="row">   
        <div class="col-md-12">  
                <div class="card-body" id="gender"> สถานภาพบุตร-ธิดา :
                    <div class="card-content">
                    <div class="card-body">
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio1">
                            <label
                                class="custom-control-label"
                                for="radio1">บุตร</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio2"
                                checked>
                            <label
                                class="custom-control-label"
                                for="radio2"
                                checked>รับรองบุตร</label>
                             </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio2"
                                checked>
                            <label
                                class="custom-control-label"
                                for="radio2"
                                checked>บุตรบุญธรรม</label>
                             </div>
                        </div>
                    </div>
                </div>
                </div>
        </div>
  <!-----------------------------////////////////////////////////---------------------------------> 
  <div class="row">  
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body ">
                        หลักฐานการเกิด :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body ">
                        เลขที่ :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
         </div>  
    <!-----------------------------////////////////////////////////---------------------------------> 
    <div class="row">
    <div class="col-md-6">
                <div class="card-block">
                    <div class="input-group">
                        ลงวันที่ :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        <input
                            type="text"
                            class="form-control pickadate-disable-dates"
                            placeholder="วันปลดประจำการ"
                            aria-describedby="button-addon4">
                        <div
                            class="input-group-append">
                            <button
                                class="btn btn-primary"
                                type="button"
                                style=" padding-bottom: 1px; padding-top: 1px;"><i
                                class="la la-calendar-o"></i></button>
                        </div>
                    </div>
                </div>
            </div>
                
            <div class="col-md-3">
                <div class="card-block">
                    <div class="card-body ">
                        จังหวัด :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
                
            <div class="col-md-3">
                <div class="card-block">
                    <div class="card-body ">
                        อำเภอ :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
            </div>
      <!-----------------------------////////////////////////////////---------------------------------> 
      <div class="row">
    <div class="col-md-6">
                <div class="card-block">
                    <div class="input-group">
                        วันเดือนปีเกิด :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input
                            type="text"
                            class="form-control pickadate-disable-dates"
                            placeholder="วันปลดประจำการ"
                            aria-describedby="button-addon4">
                        <div
                            class="input-group-append">
                            <button
                                class="btn btn-primary"
                                type="button"
                                style=" padding-bottom: 1px; padding-top: 1px;"><i
                                class="la la-calendar-o"></i></button>
                        </div>
                    </div>
                </div>
            </div>     
     
    <div class="col-md-6">
                <div class="card-block">
                    <div class="input-group">
                        แนบไฟล์หลักฐาน :
                        <input
                            type="text"
                            class="form-control pickadate-disable-dates"
                            placeholder="วันปลดประจำการ"
                            aria-describedby="button-addon4">
                        <div
                            class="input-group-append">
                            <button
                                class="btn btn-primary"
                                type="button"
                                style=" padding-bottom: 1px; padding-top: 1px;"><i
                                class="la la-calendar-o"></i></button>
                        </div>
                    </div>
                </div>
            </div>     
            </div> 
        <!-----------------------------////////////////////////////////--------------------------------->
        <div class="row">   
        <div class="col-md-12">  
                <div class="card-body" id="gender"> สถานภาพ :
                    <div class="card-content">
                    <div class="card-body">
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio1">
                            <label
                                class="custom-control-label"
                                for="radio1">ปกติ</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio2"
                                checked>
                            <label
                                class="custom-control-label"
                                for="radio2"
                                checked>สูญเสีย</label>
                             </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio2"
                                checked>
                            <label
                                class="custom-control-label"
                                for="radio2"
                                checked>ยกเลิก</label>
                             </div>
                        </div>
                    </div>
                </div>
                </div>
        </div>
        <!-----------------------------////////////////////////////////--------------------------------->
        <!-----------------------------////////////////////////////////--------------------------------->
        <!-----------------------------////////////////////////////////--------------------------------->


        <!-----------------------------////////////////////////////////--------------------------------->
             </div>  
        </div>
    </div>
    
<!-----------------------------////////////////////////////////---------------------------------> 
<!-----------------------------////////////////////////////////---------------------------------> 
<!-----------------------------////////////////////////////////---------------------------------> 
    <!-- -------ส่วนที่3 -->
    <div class="tab-content px-1 pt-1">
       
        <div class="form-actions center"
            align="center">
            <button type="button"
                class="btn btn-success  round btn-min-width mr-1 mb-1"
                id="submit"
                name="submit"
                data-target="#modalConfirm"
                onclick="insertOrganizationGroupType()"><i class="fa fa-save"></i> &nbsp;บันทึก </button>
            <button type="button"
                class="btn btn-danger  round btn-min-width mr-1 mb-1"
                id="type-error"><i class="fa fa-times-circle-o"></i> &nbsp;ยกเลิก</button>
        </div>
    </div>
</div>
</div>
</div>
<br>
</div>
</div>
</div>
</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 </section>