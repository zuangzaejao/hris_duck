<!-- header -->
<?php include '../include/header.php'; ?>
<?php include '../../Model/Ducklab/func.php'; ?> 

<?php  
/*require_once "../../config.php";*/

$menu1 ="HRTOFFICIAL" ;
$menu2 ="ORGSTRUCDATA";
$menu3 ="PROFILE"; 
?>
<!-- menu -->
<?php include '../include/menu.php'; ?>


<section>
<div class="app-content content">
<div class="content-wrapper">

<div class="content-header row">
<div class="content-header-left col-12 mb-2">
<div class="linetitle" ></div>
<h3 class="content-header-title">ข้อมูลประวัติราชการ</h3>
<nav aria-label="breadcrumb">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="../home/index.php">งานประวัติรับราชการ</a></li>
<li class="breadcrumb-item active" aria-current="page">   ข้อมูลตำแหน่ง </li>
</ol>
</nav>
<br>
<div class="content-body">
<section id="bootstrap3">
<div class="row">
<div class="col-12">
<div class="card">
<div class="card-content collapse show">
<div class="card-body card-dashboard">

<!-- เริ่ม--กล้องข้อมูลคนรูป -->
<div class="card">

<div class="card-header" style="background-color:#0f1733;padding:1.0rem 1.0rem">
<h6 class="card-title text-white">
พล.อ.ธานินทร์ จันทร์อังคารพุธ
หมายเลขประจำตัว
332124562455
</h6>

<a class="heading-elements-toggle"><i
class="la la-ellipsis-v font-medium-3"></i></a>
<div class="heading-elements">
<ul class="list-inline mb-0">
<li><a data-action="collapse"><i
class="ft-minus"></i></a>
</li>
</ul>
</div>
</div>
</div>
<!-- จบ--กล้องข้อมูลคนรูป -->
<!-- เริ่ม--กล้องข้อมูลคนเนื้อหา -->
<div class="profile"
style="background-color:#f4f5fa; color:whitesmoke;padding:1%; height: 180px;width: 100%;">
<div class="image"
style="background-color:#BEBEBE;height: 160px;width: 12%;">

<div class="profile_data"
style="height: 160px;width: 450px;margin-left:150px;padding:1%;">
<p class="text">
<h6>หมายเลขประจำตัว : 12345678888</h6>
<h6>ยศ : ร.อ.</h6>
<h6>เหล่า : สบ.</h6>
<h6>สังกัด : กพ.ทอ.</h6>
<h6>ตำแหน่ง : ปฏิบัติการเครื่องจักรคำนวณ แผนกวิเคราะห์
</h6>
</p>
</div>
</div>
</div>
<!-- จบ--กล้องข้อมูลคนเนื้อหา -->
<div class="profile_head"
style="background-color:#f5f5f5;height: auto;width: 100%;margin-left:0px;padding:1%;">
<br>
<div class="card">
<div class="card-header">
<h5 class="card-title">ประวัติข้าราชการ</h5>
</div>
<div class="card-content">
<div class="card-body">
<?php require_once '../include/profile_table.php'; ?>

<div class="tab-pane" id="tab14"
aria-labelledby="base-tab14">
<div class="container">
<div class="card-content">
<div class="card-body">
  <!-----------------------------////////////////////////////////--------------------------------->
  <div role="tabpanel" class="tab-pane active" id="tab11" aria-expanded="true" aria-labelledby="base-tab11">
    <div class="container">
 
     
    <div class="card collapse-icon accordion-icon-rotate active">
        <div id="headingCollapse31"
            class="card-header bg-success">
            <a data-toggle="collapse"
                href="#collapse31"
                aria-expanded="true"
                aria-controls="collapse31"
                class="card-title lead white">
                <h6><U>ส่วนที่ 1</U>
                    ข้อมูลบุคคล(ประวัติข้าราชการ)
                </h6>
            </a>
        </div>
        <div id="collapse31"
           role="tabpanel"
           aria-labelledby="headingCollapse31"
            class="card-collapse collapse show"
            aria-expanded="true">
            <div class="card-content">
      <!-----------------------------////////////////////////////////--------------------------------->      
   <!-----------------------------////////////////////////////////--------------------------------->          
            </div>
        </div>
    </div>
    </div>
    </div>
    <br>


   <!-----------------------------////////////////////////////////--------------------------------->  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 </section>

