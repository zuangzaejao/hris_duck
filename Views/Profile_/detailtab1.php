
<?php
            $sql03 = "SELECT HrtPersonTypeID,HrtPersonTypeName From HrtPersonType where 1=1 ";
                $query03 = sqlsrv_query($conn, $sql03 );
                $data03=array();
            while($row03=sqlsrv_fetch_array($query03, SQLSRV_FETCH_ASSOC )){
                $data03[] = $row03 ; 
            }
?>
        <br><br>
        
    
       <!-----------------------------////////////////////////////////--------------------------------->  
<div role="tabpanel" class="tab-pane active" id="tab11" aria-expanded="true" aria-labelledby="base-tab11">
    <div class="container">
 
     
    <div class="card collapse-icon accordion-icon-rotate active">
        <div id="headingCollapse31"
            class="card-header bg-success">
            <a data-toggle="collapse"
                href="#collapse31"
                aria-expanded="true"
                aria-controls="collapse31"
                class="card-title lead white">
                <h6><U>ส่วนที่ 1</U>
                    ข้อมูลบุคคล(ประวัติข้าราชการ)
                </h6>
            </a>
        </div>
        <div id="collapse31"
           role="tabpanel"
           aria-labelledby="headingCollapse31"
            class="card-collapse collapse show"
            aria-expanded="true">
            <div class="card-content">

  
  <!-----------------------------////////////////////////////////--------------------------------->   
         <div class="row">
             <div class="col-md-6">
                    <div class="card-body" id="Personnel_type">
                    ประเภทกำลังพล :
                      <select class="select2 form-control" style="width: 100%;" name="HrtPersonTypeName" id="HrtPersonTypeName" >
  <!-----------------------------////////////////////////////////--------------------------------->   
                      <option value="" >   กรุณาเลือกประเภทกำลังพล </option>
                                <?php 
                                if($data03){
                                  foreach($data03 as $key3 => $val3 ){ //HrtPosCode,HrtPosName 
                                     
                                     ?>
                                      <option value="<?php echo $val3['HrtPersonTypeID']; ?>" <?php if( $val3['HrtPersonTypeID'] == $_POST['HrtPersonTypeName']) { echo "selected"; }?> > <?php   echo $val3['HrtPersonTypeName'] ;?> </option>
                                     <?php
                                  }
                                }   
                                  ?> 
  <!-----------------------------////////////////////////////////--------------------------------->   
                      </select>
                    </div> 
                </div>
                <div class="col-md-6">                         
                         <div class="card-body"id="Government_Number">
                            หมายเลขประจำตัวข้าราชการ :

                            <input 
                                value=<?php echo $data5['HrtAirForceID'] ?>
                                class="input form-control"
                                style="width: 100%;"
                                placeholder="">
                        </div>
                    </div>
                </div>
              
        <!-----------------------------////////////////////////////////--------------------------------->      
        <div class="row">  
            <div class="col-md-6">    
                    <div class="card-body"id="Rank_name">
                        ยศ :
                        <select class="select2 form-control" style="width: 100%;">
                            <optgroup
                                label="คำนำหน้าชื่อ">
                                <option value="AK">
                                <?php echo $val1['HrtRankNameTh'] ;?>
                                
                                </option>
                                <option value="HI">
                                    ***1***
                                </option>
                                <option value="WA">
                                    ***2****
                                </option>
                            </optgroup>
                        </select>
                    </div>     
             </div>   
            <div class="col-md-6">    
                    <div class="card-body" id="title_name">
                         ยศก่อนสูญเสีย :
                        <select class="select2 form-control"   style="width: 100%;">
                            <optgroup
                                label="ยศก่อนสูญเสีย">
                                <option
                                    value="AK">
                                    <?php echo $val1['HrtRankNameTh'] ;?>
                                    </option>
                                <option
                                    value="AK">
                                    จอมพลอากาศ
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>                              
                            </optgroup>
                            <optgroup
                                label="ชั้นประทวน">
                                <option
                                    value="CA">
          
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option> 
                        </select>
                    </div>
                </div>
          
         </div>
<!-----------------------------////////////////////////////////--------------------------------->      
        <div class="row">      
            <div class="col-md-6">  
                <div class="card-body" id="gender"> เพศ:
                    <div class="card-content">
                    <div class="card-body">
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio1">
                            <label
                                class="custom-control-label"
                                for="radio1">เพศชาย</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio2"
                                checked>
                            <label
                                class="custom-control-label"
                                for="radio2"
                                checked>เพศหญิง</label>
                             </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body" id="Title name"> คำนำหน้าชื่อ :
                    <select
                        class="select2 form-control" style="width: 100%;">
                        <optgroup
                            label="เลขที่ประจำตัว">
                            <option value="AK">
                            <?php echo $val1['HrtRankAbbrTh'] ;?>
                            </option>
                            <option value="HI">
                                ***1***
                            </option>
                            <option value="WA">
                                ***2***
                            </option>
                    </select>
                    </div>
                  </div>
            </div>
        </div>
  <!-----------------------------////////////////////////////////--------------------------------->       
        <div class="row">   
            <div class="col-md-6">
               <div class="card-body" id="name_thai">
                    ชื่อ(ไทย) :
                    
                    <input
                    
                    
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                        
                </div> 
            </div>  
            <div class="col-md-6">         
                <div class="card-body" id="lastnames_thai">
                    นามสกุล(ไทย) :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>
            </div>   
        </div>        
       <!-----------------------------////////////////////////////////--------------------------------->       
       <div class="row">   
            <div class="col-md-6">
               <div class="card-body" id="name_Eng">
                    ชื่อ(อังกฤษ) :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div> 
            </div>  
            <div class="col-md-6">         
                <div class="card-body" id="lastnames_Eng">
                    นามสกุล(อังกฤษ) :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>
            </div>   
        </div>        
       <!-----------------------------////////////////////////////////--------------------------------->  
       <div class="row">      
            <div class="col-md-12">  
                <div class="card-body" id="Book">ประเภทแฟ้ม:
                    <div class="card-content">
                    <div class="card-body">
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio1">
                            <label
                                class="custom-control-label"
                                for="radio1">แผ่น</label>
                        </div>
                         <!----->
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio2"
                                checked>
                            <label
                                class="custom-control-label"
                                for="radio2"
                                checked>เล่ม</label>
                             </div>
                             <!----->
                             <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio2"
                                checked>
                            <label
                                class="custom-control-label"
                                for="radio2"
                                checked>ชุดบรรจุซอง</label>
                             </div>
                              <!----->
                              <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio2"
                                checked>
                            <label
                                class="custom-control-label"
                                for="radio2"
                                checked>เล่ม(ปกสีฟ้า)</label>
                             </div>
                               <!----->
                               <div class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio2"
                                checked>
                            <label
                                class="custom-control-label"
                                for="radio2"
                                checked>ไม่ระบุ</label>
                             </div>
                                <!----->
                        </div> 
                    </div>
                </div>
            </div>
      
        </div>
       <!-----------------------------////////////////////////////////--------------------------------->   
       <div class="row">
            <div class="col-md-3">
                <div class="card-body"
                    id="NTT">
                    หมายเลขแฟ้มประวัติ :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>
                </div>
                <div class="col-md-3">
                    <div class="card-body"
                        id="NTT_repetitive">
                        <a>หมายเลขแฟ้มประวัติใหม่ :
                        </a>
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
                <div class="col-md-6">         
                  <div class="card-body" id="MyID">
                    <a>เลขที่ประจำตัว :</a>
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>
            </div>   
        </div>
       <!-----------------------------////////////////////////////////--------------------------------->  
    
  <!-----------------------------////////////////////////////////--------------------------------->  
    <div class="row">                
        <div class="col-md-6">
            <div class="card-block">
                <div class="input-group col-12 datep">
                <label class="label-control col-12 pl-0">     ตั้งเเต่วันที่  :</label>
                   
                    <input type="text" class="form-control pickadate-translations"   placeholder="" id="startdate" name="startdate" data-value="<?php echo GetToday('');?>" />
                    <div class="input-group-append">
                            <span class="input-group-text">
                            <span class="la la-calendar-o"></span>
                            </span>
                    </div>
                </div>
            </div>
        </div>
                      
        <div class="col-md-6">
        <div class="card-block">
                <div class="input-group col-12 datep">
                <label class="label-control col-12 pl-0">     ตั้งเเต่วันที่  :</label>
                   
                    <input type="text" class="form-control pickadate-translations"   placeholder="" id="startdate" name="startdate" data-value="<?php echo GetToday('');?>" />
                    <div class="input-group-append">
                            <span class="input-group-text">
                            <span class="la la-calendar-o"></span>
                            </span>
                    </div>
                </div>
            </div>
        </div>
       
    </div>
       <!-----------------------------////////////////////////////////--------------------------------->                
            </div>
        </div>
    </div>
    <br>




    <!-- ----ส่วนที่1------      -->

    <!-----------------------------////////////////////////////////--------------------------------->  
    <!-----------------------------////////////////////////////////--------------------------------->  
        <!-----------------------------////////////////////////////////--------------------------------->  
            <!-----------------------------////////////////////////////////--------------------------------->         
         
<div class="card collapse-icon accordion-icon-rotate active">
<div id="headingCollapse33"
    class="card-header bg-success">
    <a data-toggle="collapse"
        href="#collapse33"
        aria-expanded="true"
        aria-controls="collapse33"
        class="card-title lead white">
        <h6><U>ส่วนที่ 2</U>
            ข้อมูลบุคคล(ประวัติข้าราชการ)
        </h6>
    </a>
</div>
<div id="collapse33"
    role="tabpanel"
    aria-labelledby="headingCollapse33"
    class="card-collapse collapse show"
    aria-expanded="true">
    <div class="card-content">
        <div class="card-body">
            <!-- <div class="row match-height"> -->
                        <!-----------------------------////////////////////////////////--------------------------------->  
                       
        <div class="row">
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body ">
                        กำเนิดแรกบรรจุ :
                        <select
                            class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="กำเนิดแรกบรรจุ">
                                <option value="AK">
                                    เลือก
                                </option>
                                <option value="HI">
                                    ***1***
                                </option>
                                <option value="WA">
                                    ***2****
                                </option>
                        </select>                          
                    </div>
                </div>
            </div>    
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body "> กำเนิดปรับสภาพ :
                        <select class="select2 form-control" style="width: 100%;">
                            <optgroup label="กำเนิดปรับสภาพ">
                            <option value="AK">
                                    เลือก
                                </option>
                                <option value="HI">
                                    ***1***
                                </option>
                                <option value="WA">
                                    ***2****
                                </option>
                        </select>
                        
                    </div>
                </div>
            </div> 
            
        </div>
                    
        <!-----------------------------////////////////////////////////--------------------------------->  
        <div class="row"> 
        <div class="col-md-6">
            <div class="card-block">
                <div class="card-body "> คุณวุฒิแรกบรรจุ :
                    <select class="select2 form-control" style="width: 100%;">
                        <optgroup label="คุณวุฒิแรกบรรจุ">
                        <option value="AK">
                                เลือก
                            </option>
                            <option value="HI">
                                ***1***
                            </option>
                            <option value="WA">
                                ***2****
                            </option>
                    </select>
                    
                </div>
            </div>
        </div> 
        <div class="col-lg-6 col-md-6">
            <div class="card-block">
                <div class="card-body "> คุณวุฒิปรับสภาพ :
                    <select class="select2 form-control" style="width: 100%;">
                        <optgroup label="สัญญาบัตร">
                            <option value="AK">
                                เลือก
                            </option>
                            <option value="HI">
                                พลอากาศเอก/พล.อ.อ.
                            </option>
                            <option value="WA">
                                พลทหาร/พลฯ
                            </option>
                    </select>
                    
                </div>
            </div>
        </div> 
     </div>            
       <!-----------------------------////////////////////////////////--------------------------------->        
    <div class="row"> 
            <div class="col-md-6"> 
                <div class="card-block">
                    <div class="card-body "> เหล่า :
                        <select class="select2 form-control" style="width: 100%;">
                            <optgroup label="เหล่า">
                            <option value="AK">
                                    เลือก
                                </option>
                                <option value="HI">
                                    ***1***
                                </option>
                                <option value="WA">
                                    ***2****
                                </option>
                        </select>
                        
                    </div>
                </div>
            </div> 
            <div class="col-md-6 ">
                <div class="card-block">
                    <div class="card-body "> จำพวก :
                        <select class="select2 form-control" style="width: 100%;">
                            <optgroup label="จำพวก">
                                <option value="AK">
                                    เลือก
                                </option>
                                <option value="HI">
                                    ***1***
                                </option>
                                <option value="WA">
                                    ***2****
                                </option>
                        </select>                
                    </div>
                </div>
            </div> 
        </div> 
    <!-----------------------------////////////////////////////////--------------------------------->                  
       <div class="row">                    
            <div class="col-md-3">
                <div class="card-body"
                    id="NTT">
                    รุ่น นตท. :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>
            </div>
            <div class="col-md-3">
                <div class="card-body" id="NTT_repetitive">
                    <a>รุ่น นตท. ซ้ำชั้น :</a>
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>
            </div>
            <div class="col-md-3">
                <div class="card-body" id="NNO">รุ่น นนอ. :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>
            </div>
            <div class="col-md-3">
                <div class="card-body"
                    id="NNO_repetitive">
                    <a>รุ่น นนอ. ซ้ำชั้น :</a>
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>
            </div>
        </div>             
       <!-----------------------------////////////////////////////////--------------------------------->                  
       <div class="row">                                
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body"
                        id="Flying_student">
                        รุ่นศิษย์การบิน :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder="">
                    </div>
                </div>
            </div>
        </div>
        <!-----------------------------////////////////////////////////--------------------------------->   
        <hr> <!----เส้นคัน--->
       <!-----------------------------////////////////////////////////--------------------------------->                                                   
       <div class="row">        
            <div class="col-md-3">
                <div class="card-block">
                    <div class="card-body ">
                        เลขหมายเลขรายงาน :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
            <div class=" col-md-3">
                <div class="card-block">
                    <div class="card-body ">
                           <a> ลชทอ.หลัก : </a>
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card-body ">
                    <a>ลชทอ.รอง1 :</a>
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>
            </div>
            <div class="col-md-3">
                <div class="card-body ">
                    <a>ลชทอ.รอง2 :</a>
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>
            </div>
        </div>
        <!-----------------------------////////////////////////////////--------------------------------->   
        <div class="row">      
            <div class=" col-md-6">
                <div class="card-block">
                    <div
                        class="card-body ">
                        เลขที่ตำแหน่งระบบจ่ายตรงฯ
                        (ตำแหน่งหลัก)
                        :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">

                    </div>
                </div>
            </div>
        </div>
        <!-----------------------------////////////////////////////////--------------------------------->   
        <div class="row">    
            <div class="col-md-12">
                <div
                    class="card-block">
                    <div
                        class="card-body ">
                        ชื่อตำแหน่ง :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">

                    </div>
                </div>
            </div>
        </div>               
         <!-----------------------------////////////////////////////////--------------------------------->                 
         <div class="row">            
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body ">
                        สังกัด :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">

                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body ">
                        สายวิทยาการ :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
        </div>
        <!-----------------------------////////////////////////////////--------------------------------->      
        <div class="row">               
            <div class="col-md-3">
                <div class="card-body ">
                    ชั้นเงินเดือน :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>
            </div>
            <div class="col-md-3">
                <div class="card-body ">
                    <a>เงินเดือน :</a>
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>
            </div>
       </div>
        <!-----------------------------////////////////////////////////--------------------------------->      
           
       <div class="row">    
        <div class="col-md-6">
        <div class="card-block">
                <div class="input-group col-12 datep">
                <label class="label-control col-12 pl-0">     ตั้งเเต่วันที่  :</label>
                   
                    <input type="text" class="form-control pickadate-translations"   placeholder="" id="startdate" name="startdate" data-value="<?php echo GetToday('');?>" />
                    <div class="input-group-append">
                            <span class="input-group-text">
                            <span class="la la-calendar-o"></span>
                            </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
        <div class="card-block">
                <div class="input-group col-12 datep">
                <label class="label-control col-12 pl-0">     ตั้งเเต่วันที่  :</label>
                   
                    <input type="text" class="form-control pickadate-translations"   placeholder="" id="startdate" name="startdate" data-value="<?php echo GetToday('');?>" />
                    <div class="input-group-append">
                            <span class="input-group-text">
                            <span class="la la-calendar-o"></span>
                            </span>
                    </div>
                </div>
            </div>
        </div>
         </div>
      <!-----------------------------////////////////////////////////--------------------------------->  
      <div class="row">   
      <div class="col-md-6">
        <div class="card-block">
                <div class="input-group col-12 datep">
                <label class="label-control col-12 pl-0">     ตั้งเเต่วันที่  :</label>
                   
                    <input type="text" class="form-control pickadate-translations"   placeholder="" id="startdate" name="startdate" data-value="<?php echo GetToday('');?>" />
                    <div class="input-group-append">
                            <span class="input-group-text">
                            <span class="la la-calendar-o"></span>
                            </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
        <div class="card-block">
                <div class="input-group col-12 datep">
                <label class="label-control col-12 pl-0">     ตั้งเเต่วันที่  :</label>
                   
                    <input type="text" class="form-control pickadate-translations"   placeholder="" id="startdate" name="startdate" data-value="<?php echo GetToday('');?>" />
                    <div class="input-group-append">
                            <span class="input-group-text">
                            <span class="la la-calendar-o"></span>
                            </span>
                    </div>
                </div>
            </div>
        </div>
          </div>
        <!-----------------------------////////////////////////////////--------------------------------->  
        <div class="row">  
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body ">
                        เบิกลด :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
           
            <div class="col-md-3">
                <div class="card-body ">
                    คำนวณวันรับยศ :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>

            </div>
            <div class="col-md-3">
                <div class="card-body ">
                    <a>จำนวนปีครองยศ : </a>
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>
            </div>
        </div>
             
       <!-----------------------------////////////////////////////////--------------------------------->      
       <div class="row">  
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body ">
                        สถานะภาพบุคคล :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card-body ">
                    จำนวนปีรับราชการ :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>
            </div>
            <div class="col-md-3">
                <div class="card-body ">
                    <a>จำนวนวันลา :</a>
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>
            </div>
         </div>
         <!-----------------------------////////////////////////////////--------------------------------->     
         <div class="row">                
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body ">
                        วันทวีคูณ :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
         </div>
         <!-----------------------------////////////////////////////////--------------------------------->     
         <div class="row">                      
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body ">
                        ประเภทการสูญเสีย :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>

            <div class="col-md-6">
             <div class="card-block">
                <div class="input-group col-12 datep">
                <label class="label-control col-12 pl-0">     ตั้งเเต่วันที่  :</label>
                   
                    <input type="text" class="form-control pickadate-translations"   placeholder="" id="startdate" name="startdate" data-value="<?php echo GetToday('');?>" />
                    <div class="input-group-append">
                            <span class="input-group-text">
                            <span class="la la-calendar-o"></span>
                            </span>
                    </div>
                </div>
            </div>
        </div>
        </div>
   <!-----------------------------////////////////////////////////--------------------------------->                    
                </div>
            </div>
        </div>
    </div>
    <br>


    <!-- ----ส่วนที่2--- -->
    <!-----------------------------////////////////////////////////--------------------------------->  
    <!-----------------------------////////////////////////////////--------------------------------->  
    <!-----------------------------////////////////////////////////--------------------------------->  

    <div
    class="card collapse-icon accordion-icon-rotate active">
    <div id="headingCollapse34"
        class="card-header bg-success">
        <a data-toggle="collapse"
            href="#collapse34"
            aria-expanded="true"
            aria-controls="collapse34"
            class="card-title lead white">
            <h6><U>ส่วนที่ 3</U>
                ข้อมูลบุคคล(ประวัติข้าราชการ)
            </h6>
        </a>
    </div>
    <div id="collapse34"
        role="tabpanel"
        aria-labelledby="headingCollapse34"
        class="card-collapse collapse show"
        aria-expanded="true">
        <div class="card-content">
            <div class="card-body">
                    <!-- <div class="row match-height"> -->
        <!-----------------------------////////////////////////////////--------------------------------->  
        <div class="row">
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body">
                        เลขประจำตัวประชาชน :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-block">
                    <div
                        class="card-body ">
                        เลขประจำตัวผู้เสียภาษี :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
        </div>
        <!-----------------------------////////////////////////////////--------------------------------->  
        <div class="row">           
           <div class="col-md-12">
              <div class="card-body ">
                สถานภาพสมรส
                :
                <div
                    class="card-content">
                    <div
                        class="card-body">
                        <div
                            class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio8">
                            <label
                                class="custom-control-label"
                                for="radio8">ไม่ระบุ</label>
                        </div>
                        <div
                            class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio9"
                                checked>
                            <label
                                class="custom-control-label"
                                for="radio9"
                                checked>โสด</label>
                        </div>
                        <div
                            class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio10"
                                checked>
                            <label
                                class="custom-control-label"
                                for="radio10"
                                checked>สมรส</label>
                        </div>
                        <div
                            class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio11"
                                checked>
                            <label
                                class="custom-control-label"
                                for="radio11"
                                checked>หย่า</label>
                        </div>
                        <div
                            class="d-inline-block custom-control custom-radio mr-1">
                            <input
                                type="radio"
                                class="custom-control-input"
                                name="colorRadio"
                                id="radio12"
                                checked>
                            <label
                                class="custom-control-label"
                                for="radio12"
                                checked>หม้าย</label>
                        </div>
                    </div>
                  </div>
                 </div>
             </div>
         </div>
         <!-----------------------------////////////////////////////////--------------------------------->  
         <div class="row">            
            <div class="col-md-6">
                <div class="card-block">
                    <div
                        class="input-group">
                        วัน/เดือน/ปี เกิด :
                        <input
                            type="text"
                            class="form-control pickadate-disable-dates"
                            placeholder="25 กรกฏาคม 2562"
                            aria-describedby="button-addon4">
                        <div
                            class="input-group-append">
                            <button
                                class="btn btn-primary"
                                type="button"
                                style=" padding-bottom: 1px; padding-top: 1px;">
                                <i class="la la-calendar-o"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-block">
                    <div class="input-group">
                        วัน/เดือน/ปี เกษียณ :
                        <input
                            type="text"
                            class="form-control pickadate-disable-dates"
                            placeholder="25 กรกฏาคม 2562"
                            aria-describedby="button-addon4">
                        <div
                            class="input-group-append">
                            <button
                                class="btn btn-primary"
                                type="button"
                                style=" padding-bottom: 1px; padding-top: 1px;">
                                <i class="la la-calendar-o"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-----------------------------////////////////////////////////--------------------------------->  
        <div class="row">   
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body ">
                        เชื้อชาติ :
                        <select
                            class="select2 form-control"
                            style="width: 100%;">
                            <optgroup>
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    ****1***
                                </option>
                            </optgroup>
                        </select>                                 
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body ">
                        สัญชาติ :
                        <select class="select2 form-control" style="width: 100%;">
                            <optgroup>
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    ****1***
                                </option>
                                <option
                                    value="HI">
                                    ****2***
                                </option>                                           
                            </optgroup>                                      
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <!-----------------------------////////////////////////////////--------------------------------->  
        <div class="row">  
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body ">
                        ศาสนา :
                        <select class="select2 form-control" style="width: 100%;">
                        <optgroup>
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    ****1***
                                </option>
                                <option
                                    value="HI">
                                    ****2***
                                </option>                                           
                            </optgroup>                             
                        </select>                          
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="card-block">
                    <div class="card-body ">
                        อาชีพก่อนรับราชการ :
                        <select class="select2 form-control" style="width: 100%;">
                        <optgroup>
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    ****1***
                                </option>
                                <option
                                    value="HI">
                                    ****2***
                                </option>                                           
                            </optgroup>                                      
                        </select>                                  
                    </div>
                </div>
            </div>
        </div>
<!-----------------------------////////////////////////////////--------------------------------->  
        <div class="row">  
            <div class="col-md-6">
                <div class="card-body ">
                    กบข./กสจ. :
                    <div class="card-content">
                        <div class="card-body">
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input
                                    type="radio"
                                    class="custom-control-input"
                                    name="colorRadio"
                                    id="radio14">
                                <label
                                    class="custom-control-label"
                                    for="radio14">ไม่ระบุ</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input
                                    type="radio"
                                    class="custom-control-input"
                                    name="colorRadio"
                                    id="radio15"
                                    checked>
                                <label
                                    class="custom-control-label"
                                    for="radio15"
                                    checked>เป็นสมาชิก</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input
                                    type="radio"
                                    class="custom-control-input"
                                    name="colorRadio"
                                    id="radio16"
                                    checked>
                                <label
                                    class="custom-control-label"
                                    for="radio16"
                                    checked>ไม่เป็นสมาชิก</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-body ">
                    ประเภทการเป็นสมาชิก :
                    <div class="card-content">
                        <div class="card-body">
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input
                                    type="radio"
                                    class="custom-control-input"
                                    name="colorRadio"
                                    id="radio16">
                                <label
                                    class="custom-control-label"
                                    for="radio16">สมาชิกแบบสะสม</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input
                                    type="radio"
                                    class="custom-control-input"
                                    name="colorRadio"
                                    id="radio17"
                                    checked>
                                <label
                                    class="custom-control-label"
                                    for="radio17"
                                    checked>สมาชิกแบบไม่สะสม</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>         
<!-----------------------------////////////////////////////////--------------------------------->  
        <div class="row">  
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body ">
                        อีเมล :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="card-block">
                    <div class="card-body ">
                        อีเมลสำรอง :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
        </div>
        <!-----------------------------////////////////////////////////---------------------------------> 
        <div class="row"> 
            <div class="col-lg-6 col-md-12">
                <div class="card-block">
                    <div class="card-body ">
                        หมายเลขโทรศัพท์ที่ทำงาน :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
        </div>
        <!-----------------------------////////////////////////////////---------------------------------> 
        <!-----------------------------ตารางใหญ่กรอกข้อมูล---------------------------------> 
        <!-----------------------------////////////////////////////////---------------------------------> 
        <div class="row"> 
            <div class="col-lg-12 col-md-12">
                <div class="card-block">
                    <div class="card-body ">
                        ข้อมูลทะเบียนราษฎร์ :
                        <input
                            class="input form-control"
                            style="width: 100%;height: 150px;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
        </div>
    <!-----------------------------////////////////////////////////---------------------------------> 
                    <!-- </div> -->
                </div>
            </div>
        </div>
    </div>
    <br>

   
     <!-----------------------------////////////////////////////////---------------------------------> 
      <!-----------------------------////////////////////////////////---------------------------------> 
       <!-----------------------------////////////////////////////////---------------------------------> 
        <!-----------------------------////////////////////////////////---------------------------------> 
    <!-- -------ส่วนที่3 -->
      
    <div class="tab-content px-1 pt-1">
        <div class="form-actions center text-center" >
            <button type="button"
                class="btn btn-success  round btn-min-width mr-1 mb-1"
                id="submit"
                name="submit"
                data-target="#modalConfirm"
                onclick="insertOrganizationGroupType()"><i class="fa fa-save"></i> &nbsp;บันทึก </button>
            <button type="button"
                class="btn btn-danger  round btn-min-width mr-1 mb-1"
                id="type-error"><i class="fa fa-times-circle-o"></i> &nbsp;ยกเลิก</button>
        </div>
    </div>
  

</div>
</div>
</div>

