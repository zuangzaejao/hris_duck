<!-- header -->
<?php include '../include/header.php'; ?>
<?php include '../../Model/Ducklab/func.php'; ?> 

<?php  
require_once "../../config.php";

$menu1 ="HRTOFFICIAL" ;
$menu2 ="ORGSTRUCDATA";
$menu3 ="PROFILE"; 
?>
<!-- menu -->
<?php include '../include/menu.php'; ?>


<section>
<div class="app-content content">
<div class="content-wrapper">

<div class="content-header row">
<div class="content-header-left col-12 mb-2">
<div class="linetitle" ></div>
<h3 class="content-header-title">ข้อมูลประวัติราชการ</h3>
<nav aria-label="breadcrumb">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="../home/index.php">งานประวัติรับราชการ</a></li>
<li class="breadcrumb-item active" aria-current="page">   ข้อมูลตำแหน่ง </li>
</ol>
</nav>
<br>
<div class="content-body">
<section id="bootstrap3">
<div class="row">
<div class="col-12">
<div class="card">
<div class="card-content collapse show">
<div class="card-body card-dashboard">

<!-- เริ่ม--กล้องข้อมูลคนรูป -->
<?php 
                   
                   $PersonIDD=$_GET['PersonID'];

                        $sql5 = "SELECT HR.HrtRankAbbrTh,HP.HrtPersonID,HP.HrtName,HP.HrtSurName,HP.HrtAirForceID FROM HrtPerson AS HP left join HrtRank AS HR on HR.HrtRankID = HP.HrtRankID WHERE HP.HrtPersonID = '$PersonIDD' "; //echo $val1['HrtPersonID']
                        $query5 = sqlsrv_query($conn,$sql5); 
                        $data5=array();
                        ///////////////////---------------Person------------/////////////////////
                        while($row5=sqlsrv_fetch_array($query5, SQLSRV_FETCH_ASSOC )){
                        
                            $data5=$row5 ;
  
                        }     
                        //////////////////////////////////////--------------------------------//////////////////////////////////////////
                         $sql6 = "SELECT HR.HrtRankAbbrTh,HP.HrtPersonID,HP.HrtName +'  '+ HP.HrtSurName As FullName,HP.HrtAirForceID FROM HrtPerson AS HP left join HrtRank AS HR on HR.HrtRankID = HP.HrtRankID WHERE HP.HrtPersonID = '$PersonIDD' "; //
                         $query6 = sqlsrv_query($conn,$sql6); 
                         $data6=array();
                         ///////////////////---------------Person------------/////////////////////
                         while($row6=sqlsrv_fetch_array($query6, SQLSRV_FETCH_ASSOC )){
                        
                         $data6=$row6 ;
  
                         }   



 ?>
 
</div>
<div class="card">
<div class="card-header" style="background-color:#0f1733;padding:1.0rem 1.0rem">
<h6 class="card-title text-white">
                            <td>  <?php  echo $data5['HrtRankAbbrTh'];?></td>             <!--ยศ-->
                            <td>  <?php  echo $data5['HrtName'] ;?></td>             <!--ชื่อ-->
                            <td>  <?php echo $data5['HrtSurName'] ;?></td>             <!--นามสกุล-->
                            <td>  <?php echo "เลขประจำบัตรประชาชน"  ;?></td>
                                       <!---->                          
                            <td>  <?php echo $data5['HrtAirForceID'] ;?></td>            <!--รหัสราชการ-->
</h6>
<a class="heading-elements-toggle"><i
class="la la-ellipsis-v font-medium-3"></i></a>
<div class="heading-elements">
<ul class="list-inline mb-0">
<li><a data-action="collapse"><i
class="ft-minus"></i></a>
</li>
</ul>
</div>
</div>
</div>
<!-- จบ--กล้องข้อมูลคนรูป -->
<?php
// echo "<br>".$PersonID ;
// echo "<br>".$sql5;
// echo "<pre>".print_r($data5,true)."</pre>";  
//  echo "<br>".$sql6;
//  echo "<pre>".print_r($data6,true)."</pre>";  
?>
<!-- เริ่ม--กล้องข้อมูลคนเนื้อหา -->
<div class="profile"
style="background-color:#f4f5fa; color:whitesmoke;padding:1%; height: 180px;width: 100%;">
<div class="image"
style="background-color:#BEBEBE;height: 160px;width: 132px;">

<div class="profile_data"
style="height: 160px;width: 450px;margin-left:150px;padding:1%;">
<p class="text">

<!---->
<div class="row">  
    <div class="col-md-12">
            <h6>เลขประจำตัวบัตรประชาชน : <?php echo $data6['HrtAirForceID'] ;?></h6>
    </div>
</div> 
<h6></h6>  
<div class="row">  
        <div class="col-md-6">
            <h6>ยศ : <?php  echo $data6['HrtRankAbbrTh'] ;?></h6>  
        </div>    
        <div class="col-md-6">
            <h6>ชื่อ-นามสกุล : <?php echo $data6['FullName'] ?></h6>  
        </div>
</div> 
<h6></h6>   
<div class="row">   
        <div class="col-md-6">
            <h6>เหล่า : <?php ?></h6>   
        </div>   
        <div class="col-md-6">
            <h6>จำพวก : <?php ?></h6>   
        </div>   
</div>      
<h6></h6>  
<div class="row">   
        <div class="col-md-12">
            <h6>สังกัด : <?php ?></h6>   
        </div>      
</div>     
<h6></h6>  
<div class="row">   
        <div class="col-md-12">
             <h6>ตำแหน่ง : <?php ?></h6>      
        </div>      
</div>    
</p>
</div>
</div>
</div>
<!-- จบ--กล้องข้อมูลคนเนื้อหา -->
<div class="profile_head"
style="background-color:#f5f5f5;height: auto;width: 100%;margin-left:0px;padding:1%;">
<br>
<div class="card">
<div class="card-header">
<h5 class="card-title">ประวัติข้าราชการ</h5>
</div>
<div class="card-content">
<div class="card-body">
<?php require_once '../include/profile_table.php'; ?>


<div class="tab-content px-1 pt-1">

<!-- ---------------ข้อมูลบุคคล-------------- -->
<!--////*********************************************************************************************************111111111111111111111111111111111111-->
<?php include 'detailtab1.php'; ?>
<!--////*********************************************************************************************************111111111111111111111111111111111111-->

<!-- ---------------รูปภาพ-------------- -->
<!--////*********************************************************************************************************111111111111111111111111111111111111-->
<?php //include 'detailtab2.php'; ?>
<!--/// /*********************************************************************************************************111111111111111111111111111111111111-->

<!-- ---------------การเปลี่ยนชื่อ-------------- -->
<!--////*********************************************************************************************************111111111111111111111111111111111111-->
<?php //include 'detailtab3.php'; ?>
<!--////*********************************************************************************************************111111111111111111111111111111111111-->

<!-- ---------------ขึ้นทะเบียนทหาร-------------- -->
<!--////*********************************************************************************************************111111111111111111111111111111111111-->
<?php// include 'detailtab4.php'; ?>
<!--////*********************************************************************************************************111111111111111111111111111111111111-->

<!-- ---------------เหล่าทหาร-------------- -->
<!--////*********************************************************************************************************111111111111111111111111111111111111-->
<?php //include 'detailtab5.php'; ?>
<!--////*********************************************************************************************************111111111111111111111111111111111111-->

<!-- ---------------บิดา-มารดาบรรพบุรุษ-------------- -->
<!--////*********************************************************************************************************111111111111111111111111111111111111-->
<?php //include 'detailtab6.php'; ?>
<!--////*********************************************************************************************************111111111111111111111111111111111111-->

<!-- ---------------คู่สมรส-------------- -->
<!--////*********************************************************************************************************111111111111111111111111111111111111-->
<?php //include 'detailtab7.php'; ?>
<!--////*********************************************************************************************************111111111111111111111111111111111111-->


<!-- ---------------บุตร-ธิดา-------------- -->
<!--////*********************************************************************************************************111111111111111111111111111111111111-->
<?php  //include 'detailtab8.php'; ?>
<!--////*********************************************************************************************************111111111111111111111111111111111111-->


                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 </section>


<!-- footer -->
<?php include '../include/footer.php'; ?>
<!-- <script src="../../Controllers/profileController.js"></script> -->

<script type="text/javascript">
$(document).ready(function() {
console.log("ready");
change_autorefreshdiv();
});

function change_autorefreshdiv() {
// $('#prefixPage').addClass('active');
}

function toggle(source) {
var checkboxes = document.querySelectorAll('.checkAll');
for (var i = 0; i < checkboxes.length; i++) {
if (checkboxes[i] != source)
checkboxes[i].checked = source.checked;
}
}
</script>


