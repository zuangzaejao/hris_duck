<div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
         
        <!-- ระบบงานวิเคราะห์ความต้องการกำลังพลประจำปี -->
     
         <li class=" nav-item"><a href="#"><i class="la la-pie-chart"></i><span class="menu-title" data-i18n="nav.page_layouts.main">ระบบงานวิเคราะห์ความต้องการกำลังพลประจำปี</span></a>
            <ul class="menu-content">
                <li><a class="menu-item" href="../ProposeRequirements/index.php" data-i18n="nav.navbars.nav_light">งานเสนอความต้องการบรรจุกำลังพลครับ</a>
                </li>
                <li><a class="menu-item" href="../Degrees/index.php" data-i18n="nav.navbars.nav_light">ระดับการศึกษา</a>
                </li>
                <li><a class="menu-item" href="../Branches/index.php" data-i18n="nav.navbars.nav_dark">สาขาวิชา</a>
                </li>
                <li><a class="menu-item" href="../sections/index.php" data-i18n="nav.navbars.nav_semi">ทาง</a>
                </li>
                <li><a class="menu-item" href="../type_of_request/index.php" data-i18n="nav.navbars.nav_brand_center">ประเภทที่ขอบรรจุ</a>
                </li>
                <li><a class="menu-item" href="../Reason_for_request/index.php" data-i18n="nav.navbars.nav_dark">สาเหตุในการขอบรรจุ</a>
                </li>
                <li><a class="menu-item" href="../analysis_result/index.php" data-i18n="nav.navbars.nav_semi">ผลการพิจารณาวิเคราะห์</a>
                </li>
                <li><a class="menu-item" href="../Propose_manpower/index.php" data-i18n="nav.navbars.nav_brand_center">เสนอขอบรรจุกำลังพล</a>
                </li>
            </ul>
        </li>  
        
        <!-- ระบบงานวิเคราะห์ --> 
       
        <li class=" nav-item"><a href="#"><i class="la la-home"></i><span class="menu-title" data-i18n="nav.vertical_nav.main">ระบบงานวิเคราะห์ความต้องการกำลังพลประจำปี</span></a>
            <ul class="menu-content">
                <li><a class="menu-item" href="../Analysissystem/index.php" data-i18n="nav.navbars.nav_light">บันทึกข้อมูลเสนอขอบรรจุกําลังพลของ นขต.ทอ.</a>
                </li>
                <li><a class="menu-item" href="../Analysissystem-Acamic/index.php" data-i18n="nav.navbars.nav_light">บันทึกผลการพิจารณาของสายวิทยาการ</a>
                </li>
                <li><a class="menu-item" href="../Analysissystem-Acamic2/index.php" data-i18n="nav.navbars.nav_light">ข้อมูลแสดงการของบรรจุกำลังพลสายวิทยาการ</a>
                </li>
                <!-- <li><a class="menu-item" href="../Home_status_system/index.php" data-i18n="nav.navbars.nav_light">ระบบแสดงสถานะบ้าน</a>
                </li>
                <li><a class="menu-item" href="../House_allocation_system/index.php" data-i18n="nav.navbars.nav_light">ระบบจัดสรรบ้าน</a>
                </li>  -->
 
            </ul> 
        </li>  

        <!-- ระบบงานแผนบรรจุกำลังพลประจำปี -->
         <li class=" nav-item"><a href="#"><i class="la la-book"></i><span class="menu-title" data-i18n="nav.navbars.main">ระบบงานแผนบรรจุกำลังพลประจำปี</span></a>
            <ul class="menu-content">
                <li><a class="menu-item" href="../Propose_approve/index.php">เสนอขอบรรจุกำลังพล/บันทึกผลการอนุมัติ</a>
                </li>
                <li><a class="menu-item" href="../plan_type/index.php" data-i18n="nav.navbars.nav_dark">ประเภทแผน</a>
                </li>
                <li><a class="menu-item" href="../5Plan_later/index.php" data-i18n="nav.navbars.nav_semi">แผน 5 ปีย้อนหลัง</a>
                </li>
            </ul>
        </li> 
         <!-- ระบบงานสรรหาและบรรจุกำลังพล la-odnoklassniki -->
         
         <li class=" nav-item <?php if($menu1 =="RECRNAPPOINT"){ echo "open";} ?>"><a href="#"><i class="la la-street-view"></i><span class="menu-title" data-i18n="nav.navbars.main">ระบบงานสรรหากำลังพล</span></a>
            <ul class="menu-content">
                <li class=""><a class="menu-item <?php if($menu2 =="PlanManpower"){ echo "active";} ?>" href="../PlanManpower/index.php"> แผนการบรรจุกำลังพล</a> 
                </li>
                <li class=""><a class="menu-item <?php if($menu2 =="PlanManpowerReg"){ echo "active";} ?>" href="../PlanManpowerReg/index.php"> สมัครสอบ </a> 
                </li>
 
            </ul>
        </li>

        <li class=" nav-item <?php if($menu1 =="PERSONASSIGN"){ echo "open";} ?>"><a href="#"><i class="la la-street-view"></i><span class="menu-title" data-i18n="nav.navbars.main">ระบบงานบรรจุกำลังพล</span></a>
            <ul class="menu-content">
              

                <li class=""><a class="menu-item <?php if($menu2 =="PlanManpowerAssign"){ echo "active";} ?>" href="../PlanManpowerAssign/index.php"> บรรจุกำลังพล </a> 
                </li>
            </ul>
        </li>
         

        </ul>
    </div>
</div>