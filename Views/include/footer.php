<!-- ////////////////////////////////////////////////////////////////////////////-->
  <footer class="footer footer-static footer-light navbar-border navbar-shadow hidden">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
      <span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2019 
        <a class="text-bold-800 grey darken-2" href="#" > กรมกำลังพล ทหารอากาศ </a> </span>
      <span class="float-md-right d-block d-md-inline-blockd-none d-lg-block">  </span>
    </p>
  </footer>

  <script src="../../Asset/js/jquery-3.3.1.js"  type="text/javascript"></script>
  <script src="../../Asset/js/sweetalert2.min.js"></script>
  <!-- BEGIN VENDOR JS -->
  <script src="../../app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
  <!-- BEGIN VENDOR JS-->
  <!-- BEGIN PAGE VENDOR JS-->
  <script src="../../app-assets/vendors/js/extensions/toastr.min.js" type="text/javascript"></script>
  <script src="../../app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
  <script src="../../app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>

  <script src="../../app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
  <script src="../../app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
  <script src="../../app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
  <script src="../../app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
  <script src="../../app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js" type="text/javascript"></script>
  <script src="../../app-assets/vendors/js/pickers/daterange/daterangepicker.js" type="text/javascript"></script>
  <script src="../../app-assets/vendors/js/forms/icheck/icheck.min.js" type="text/javascript"></script>
  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN MODERN JS-->
  <script src="../../app-assets/js/core/app-menu.js" type="text/javascript"></script>
  <script src="../../app-assets/js/core/app.js" type="text/javascript"></script>
  <script src="../../app-assets/js/scripts/customizer.js" type="text/javascript"></script>
  <!-- END MODERN JS-->
  <!-- BEGIN PAGE LEVEL JS-->
  <script src="../../app-assets/js/scripts/extensions/toastr.js" type="text/javascript"></script>
  <script src="../../Asset/custom/js/pick-a-datetime.js"  type="text/javascript"></script>
  <script src="../../app-assets/js/scripts/forms/select/form-select2.js" type="text/javascript"></script>
  <script src="../../app-assets/js/scripts/tables/datatables/datatable-styling.js" type="text/javascript"></script>
  <script src="../../Asset/custom/js/bootstrap4-toggle.min.js"></script> 
  <script src="../../Asset/custom/js/checkbox-radio.js"></script> 
  <!-- <script src="../../app-assets/js/scripts/forms/checkbox-radio_modified.js" type="text/javascript"></script>  -->
  <!-- END PAGE LEVEL JS--> 
  
 
  <script src="../../Controllers/Ducklab/duck.script.js"></script>
  <script src="../../Controllers/Ducklab/contents.script.js"></script>
  <script src="../../Controllers/Ducklab/org.script.js"></script>
  <script src="../../Controllers/Ducklab/org2.script.js"></script>
  <script src="../../Controllers/Ducklab/hrt.script.js"></script>
  
</body> 
</html>


