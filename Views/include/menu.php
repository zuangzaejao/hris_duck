 <div class="main-menu menu-fixed   menu-light menu-collapsible menu-shadow  " data-scroll-to-active="true">
    <div class="main-menu-content"> 
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <!-- ระบบงานโครงสร้างอัตรากำลังพล --->
            <li class=" nav-item <?php if($menu1 =="ORGSTRUC"){ echo "open";} ?>"><a href="index.html"><i class="la la-sitemap"></i><span class="menu-title" data-i18n="nav.ORGSTRUC">ระบบงานโครงสร้างอัตรากำลังพล</span></a>
                <ul class="menu-content">
                    <li class="<?php if($menu2 =="ORGSTRUCDEPT"){ echo "active";} ?>"><a class="menu-item" href="#" data-i18n="nav.ORGSTRUC.STRUC">โครงสร้าง</a>
                        <ul class="menu-content">
                            <li class="<?php if($menu3 =="ORGSTRUCDEPTSETTING"){ echo "active";} ?>"><a class="menu-item" href="#" >โครงสร้างอัตรากำลังพล</a>
                                <ul class="menu-content">  
                                    <li class="<?php if($menu4 =="ORGANIZATIONGROUPTYPE"){ echo "active";} ?>"><a class="menu-item" href="../OrganizationGroupType/index.php" >ประเภทโครงสร้าง</a></li>
                                    <li class="<?php if($menu4 =="ORGANIZATIONTYPE"){ echo "active";} ?>"><a class="menu-item" href="../OrganizationTypes/index.php" >โครงสร้าง</a></li>
                                    <li class="<?php if($menu4 =="ORGANIZATIONPARTS"){ echo "active";} ?>"><a class="menu-item" href="../Organizationparts/index.php" >ส่วนราชการ</a></li> 
                                    <li class="<?php if($menu4 =="ORGANIZATIONLEVEL"){ echo "active";} ?>"><a class="menu-item" href="../OrganizationLevels/index.php" >ฐานะหน่วย</a> </li>
                                    <li class="<?php if($menu4 =="ORGANIZATIONSTRUC"){ echo "active";} ?>"><a class="menu-item" href="../OrganizationsStruc/index.php" > หน่วยงาน</a></li>  
                                    <li class="<?php if($menu4 =="ORGANIZATIONSUBUNIT"){ echo "active";} ?>"><a class="menu-item" href="../OrganizationsSubUnit/index.php" >ชื่อหน่วยงาน</a></li>
                                </ul>
                            </li>
                            <!--
                            <li class="<?php if($menu3 =="ORG"){ echo "active";} ?>"><a class="menu-item" href="../OrganizationTree/index.php" data-i18n="nav.ORGSTRUC.STRUC.ORG">โครงสร้างองค์กร</a>
                            </li> -->
                            <li class="<?php if($menu3 =="ORGPROG"){ echo "active";} ?>"><a class="menu-item" href="../OrganizationProgram/index.php" data-i18n="nav.ORGSTRUC.STRUC.ORGPROG">โปรแกรมจัดการโครงสร้าง</a>
                            </li>
                        </ul>
                    </li>
                    <li <?php if($menu2 =="ORGSTRUCPOSITION"){ echo "active";} ?>><a class="menu-item" href="#" data-i18n="nav.ORGSTRUC.POSTITION">ตำแหน่ง</a>
                        <ul class="menu-content">
                            <li class="<?php if($menu3 =="POS"){ echo "active";} ?>"><a class="menu-item" href="../Pos/index.php">ชื่อตำแหน่ง</a> </li>
                            <li class="<?php if($menu3 =="POSITIONSETUP"){ echo "active";} ?>"><a class="menu-item" href="../PositionSetup/index.php"  >บันทึกกรอบอัตราตำแหน่ง</a> </li>
                        </ul>
                    </li>
                    <!--
                    <li <?php if($menu2 =="ORGSTRUCREPORT"){ echo "active";} ?>><a class="menu-item" href="#" data-i18n="nav.ORGSTRUC.REPORT">รายงาน</a>
                        <ul class="menu-content">
                            <li><a class="menu-item" href="../Personnel_rate/index.php" >รายงานสรุปยอดอัตรากำลังพล</a>
                            </li>
                            <li><a class="menu-item" href="../Rate52/index.php" >รายงาน อัตรา ทอ.52</a>
                            </li>
                            <li><a class="menu-item" href="../Account_report/index.php" >รายงานบัญชีชื่อตำแหน่ง</a>
                            </li>
                            <li><a class="menu-item" href="../Account_report_newtoold_position/index.php" >รายงานบัญชีชื่อตำแหน่งในกรณีที่มีตำแหน่งใหม่ทดแทนตำแหน่งเก่า</a>
                            </li>
                        </ul>
                    </li> -->
                    <li  <?php if($menu2 =="ORGSTRUCDATA"){ echo "active";} ?>><a class="menu-item" href="#" data-i18n="nav.ORGSTRUC.SETTING">ข้อมูลทั่วไป</a>
                        <ul class="menu-content">
                            <li class="<?php if($menu3 =="PERSONTYPE"){ echo "active";} ?>"><a class="menu-item" href="../PersonType/index.php" data-i18n="">ประเภทกำลังพล</a>
                            </li>
                            <li class="<?php if($menu3 =="MONEYTYPE"){ echo "active";} ?>"><a class="menu-item" href="../money_types/index.php" data-i18n="">เงินเพิ่ม</a>
                            </li>

                            <li class="<?php if($menu3 =="SALARYRATES"){ echo "active";} ?>"><a class="menu-item" href="../Salary_rates/index.php" data-i18n="">อัตราเงินเดือน</a>
                            </li> 
                            <li  class="<?php if($menu3 =="ARMY"){ echo "active";} ?>" ><a class="menu-item" href="../Armys/index.php" data-i18n="">เหล่าทหาร</a>
                            </li>
                            <li  class="<?php if($menu3 =="MILITARYTYPE"){ echo "active";} ?>" ><a class="menu-item" href="../Military_type/index.php" data-i18n="">ประเภททหาร</a><!---เพิ่มดัดแปลงข้อมูลจาก Prefix----->
                            </li>
                            <li  class="<?php if($menu3 =="MILITARIES"){ echo "active";} ?>"><a class="menu-item" href="../Militaries/index.php" data-i18n="">จำพวกทหาร</a>
                            </li>

                            <li class="<?php if($menu3 =="UNITLEADER"){ echo "active";} ?>"><a class="menu-item" href="../UnitLeader/index.php" data-i18n="">หัวหน้าสายวิทยาการ</a><!---เพิ่มดัดแปลงข้อมูลจาก Prefix----->
                            </li>
                            <li class="<?php if($menu3 =="CHIEFSPC"){ echo "active";} ?>"><a class="menu-item" href="../ChiefSpc/index.php" data-i18n="">สายวิทยาการ</a>
                            </li>

                            <li class="<?php if($menu3 =="TYPEOFEXPERTISE"){ echo "active";} ?>"><a class="menu-item" href="../TypeOfExpertise/index.php" data-i18n="">ประเภทความชำนาญ</a><!---เพิ่มดัดแปลงข้อมูลจาก Prefix----->

                            <li class="<?php if($menu3 =="CHIEFSPC2"){ echo "active";} ?>"><a class="menu-item" href="../ChiefSpc/index.php" data-i18n="">ประเภทความชำนาญ*</a>
                            </li>
                            <li class="<?php if($menu3 =="CHIEFSPC3"){ echo "active";} ?>"><a class="menu-item" href="../ChiefSpc/index.php" data-i18n="">ระดับความชำนาญ*</a>
                            </li>
                            <li class="<?php if($menu3 =="CHIEFSPC4"){ echo "active";} ?>"><a class="menu-item" href="../ChiefSpc/index.php" data-i18n="">เลขหมายความชำนาญทหารอากาศ*</a>

                            <li class="<?php if($menu3 =="TYPEOFEXPERTISE"){ echo "active";} ?>"><a class="menu-item" href="../TypeOfExpertise/index.php" data-i18n="">ประเภทความชำนาญ*</a><!---เพิ่มดัดแปลงข้อมูลจาก Prefix----->

                            </li>
                            <li class="<?php if($menu3 =="SKILLLEVEL"){ echo "active";} ?>"><a class="menu-item" href="../SkillLevel/index.php" data-i18n="">ระดับความชำนาญ</a><!---เพิ่มดัดแปลงข้อมูลจาก Prefix----->
                            </li>

                            <li class="<?php if($menu3 =="AIRFORCEMILITARYNUMBER"){ echo "active";} ?>"><a class="menu-item" href="../AirForceMilitaryNumber/index.php" data-i18n="">เลขหมายความชำนาญทหารอากาศ</a><!---เพิ่มดัดแปลงข้อมูลจาก Prefix----->

                            <li class="<?php if($menu3 =="AIRFORCEMILITARYNUMBER"){ echo "active";} ?>"><a class="menu-item" href="../AirForceMilitaryNumber/index.php" data-i18n="">เลขหมายความชำนาญทหารอากาศ*</a><!---เพิ่มดัดแปลงข้อมูลจาก Prefix----->

                            </li>
 
                            <li class="<?php if($menu3 =="SKILLPREFIX"){ echo "active";} ?>"><a class="menu-item" href="../SkillPrefix/index.php" data-i18n="">อักษรนำเลข ลชทอ.</a>
                            </li>
                            <li class="<?php if($menu3 =="SKILLSUFFIX"){ echo "active";} ?>"><a class="menu-item" href="../SkillSuffix/index.php" data-i18n="">อักษรตามเลข ลชทอ.</a>
                            </li>


                             <li class="<?php if($menu3 =="PREFIXS"){ echo "active";} ?>"><a class="menu-item" href="../Prefixs/index.php" data-i18n="">คำนำหน้า</a>
                            </li>
                            <li class="<?php if($menu3 =="NATIONS"){ echo "active";} ?>"><a class="menu-item" href="../Nations/index.php" data-i18n="">สัญชาติ</a>
                            </li>
                            <li class="<?php if($menu3 =="ORIGINS"){ echo "active";} ?>"><a class="menu-item" href="../origins/index.php" data-i18n="">เชื้อชาติ</a>
                            </li>
                            <li class="<?php if($menu3 =="RELIGIONS"){ echo "active";} ?>"><a class="menu-item" href="../Religions/index.php" data-i18n="">ศาสนา</a>
                            </li>
                            <li class="<?php if($menu3 =="CTLTCNTRY"){ echo "active";} ?>"><a class="menu-item" href="../Ctltcntry/index.php" data-i18n="">ประเทศ</a>
                            </li>
                            <li class="<?php if($menu3 =="PROVINCE"){ echo "active";} ?>"><a class="menu-item" href="../Provinces/index.php" data-i18n="">จังหวัด</a>
                            </li>
                         <!--   <li class="<?php if($menu3 =="ANPLURS"){ echo "active";} ?>"><a class="menu-item" href="../Amphurs/index.php" data-i18n="">เขต/อำเภอ</a>
                            </li>
                            <li class="<?php if($menu3 =="DISTRICTS"){ echo "active";} ?>"><a class="menu-item" href="../Districts/index.php" data-i18n=""> แขวง/ตำบล</a>
                            </li> -->
                            <!-- <li class="<?php if($menu3 =="POSTCODE"){ echo "active";} ?>"><a class="menu-item" href="../postcodes/index.php" data-i18n="">รหัสไปรษณีย์</a>
                            </li> -->
                           <!-- -->
                            <li class="<?php if($menu3 =="EDUCATION"){ echo "active";} ?>"><a class="menu-item" href="../Education/index.php" data-i18n="">หลักสูตรการศึกษา</a>
                            </li>
                            <li  class="<?php if($menu3 =="RANK"){ echo "active";} ?>"><a class="menu-item" href="../Ranks/index.php" >ยศ</a>
                            </li>
                            
                           

                            <li class="<?php if($menu3 =="PASS"){ echo "active";} ?>"><a class="menu-item" href="../Pass/index.php" data-i18n="">สาเหตุการปลด/สูญเสีย</a>
                            </li>
                           
                            
                            
                            <li class="<?php if($menu3 =="POSITIONTYPEGROUP"){ echo "active";} ?>"><a class="menu-item" href="../position_type_group/index.php" data-i18n="">กลุ่มพนักงาน</a>
                            </li>
                            <li class="<?php if($menu3 =="POSITIONTYPE"){ echo "active";} ?>"><a class="menu-item" href="../Position_type/index.php" data-i18n="">ประเภทตำแหน่ง</a>
                            </li>
                            
                            <li class="<?php if($menu3 =="POSITIONLEVEL"){ echo "active";} ?>"><a class="menu-item" href="../Position_level/index.php" data-i18n="">ระดับตำแหน่งของลูกจ้างประจำ</a>
                            </li>
                            <li class="<?php if($menu3 =="POSITIONACCOUNTHGROUP"){ echo "active";} ?>"><a class="menu-item" href="../Position_account_group/index.php" data-i18n="">ตำแหน่งของลูกจ้างประจำ</a>
                            </li>
                    </li> 
                </ul>
            </li>
        </ul>
        </li>
        <!-- ระบบงานทำเนียบกำลังพล -->
        
         <li class=" nav-item  <?php if($menu1 =="HRTPERSON"){ echo "open";} ?>"><a href="#"><i class="la la-bank"></i><span class="menu-title" data-i18n="nav.templates.main">ระบบงานทำเนียบกำลังพล</span></a>
            <ul class="menu-content">
                <li><a class="menu-item" href="../PosPerson/index.php" data-i18n="nav.navbars.nav_light">ทำเนียบบรรจุกำลังพล</a>
                </li>
                <!-- <li><a class="menu-item" href="navbar-dark.html" data-i18n="nav.navbars.nav_dark">รายงานทำเนียบบรรจุกำลังพล</a>
                </li>
                <li><a class="menu-item" href="navbar-semi-dark.html" data-i18n="nav.navbars.nav_semi">รายงานทำเนียบบรรจุกำลังพลอย่างย่อ</a>
                </li>
                <li><a class="menu-item" href="navbar-brand-center.html" data-i18n="nav.navbars.nav_brand_center">รายงานอัตรากำลังพล</a>
                </li> -->
            </ul>
        </li>   
         <!-- งานประวัติรับราชการ -->
         <li class=" nav-item  <?php if($menu1 =="HRTOFFICIAL"){ echo "open";} ?>"><a href="#"><i class="ft-users"></i><span class="menu-title" data-i18n="nav.horz_nav.main">งานประวัติข้าราชการ</span></a>
            <ul class="menu-content">
                  
                <li class=" <?php if($menu2 =="PROFILE"){ echo "active";} ?>"><a class="menu-item" href="../Profile/index.php" >ประวัติข้าราชการ</a>
                </li> 
            </ul>
        </li>
		
		 <!-- ตรวจสอบประวัติรับราชการ -->
         <!-- 
         <li class=" nav-item  <?php if($menu1 =="HRTPERSONCHECK"){ echo "open";} ?>"><a href="#"><i class="la la-check-square-o"></i><span class="menu-title"  > ตรวจสอบประวัติรับราชการ </span></a>
            <ul class="menu-content">
                  
                <li class=" <?php if($menu2 =="PERSONCHECK"){ echo "active";} ?>"><a class="menu-item" href="../PersonCheck/index.php" > ตรวจสอบประวัติรับราชการ </a>
                </li> 
            </ul>
        </li>
        -->


         <!-- ระบบงานวิเคราะห์ -->
         <!-- <li class=" nav-item"><a href="#"><i class="la la-pie-chart"></i><span class="menu-title" data-i18n="nav.vertical_nav.main">ระบบงานวิเคราะห์ความต้องการกำลังพลประจำปี</span></a>
            <ul class="menu-content">
                <li><a class="menu-item" href="../Analysissystem/index.php" data-i18n="nav.navbars.nav_light">บันทึกข้อมูลเสนอขอบรรจุกําลังพลของ นขต.ทอ.</a>
                </li>
                <li><a class="menu-item" href="../Analysissystem-Acamic/index.php" data-i18n="nav.navbars.nav_light">บันทึกผลการพิจารณาของสายวิทยาการ</a>
                </li>
                <li><a class="menu-item" href="../Analysissystem-Acamic2/displaydata.php" data-i18n="nav.navbars.nav_light">ข้อมูลแสดงการของบรรจุกำลังพลสายวิทยาการ</a>
                </li>   -->

             

                <!-- ระบบงานวิเคราะห์ความต้องการกำลังพลประจำปี -->
                    <!-- <li class=" nav-item"><a href="#"><span class="menu-title" data-i18n="nav.page_layouts.main">ตั้งค่าระบบงานวิเคราะห์ความต้องการกำลังพลประจำปี</span></a>
                        <ul class="menu-content">
                            <li><a class="menu-item" href="../ProposeRequirements/index.php" data-i18n="nav.navbars.nav_light">งานเสนอความต้องการบรรจุกำลังพลครับ</a>
                            </li>
                            <li><a class="menu-item" href="../Degrees/index.php" data-i18n="nav.navbars.nav_light">ระดับการศึกษา</a>
                            </li>
                            <li><a class="menu-item" href="../Branches/index.php" data-i18n="nav.navbars.nav_dark">สาขาวิชา</a>
                            </li>
                            <li><a class="menu-item" href="../sections/index.php" data-i18n="nav.navbars.nav_semi">ทาง</a>
                            </li>
                            <li><a class="menu-item" href="../type_of_request/index.php" data-i18n="nav.navbars.nav_brand_center">ประเภทที่ขอบรรจุ</a>
                            </li>
                            <li><a class="menu-item" href="../Reason_for_request/index.php" data-i18n="nav.navbars.nav_dark">สาเหตุในการขอบรรจุ</a>
                            </li>
                            <li><a class="menu-item" href="../analysis_result/index.php" data-i18n="nav.navbars.nav_semi">ผลการพิจารณาวิเคราะห์</a>
                            </li>
                            <li><a class="menu-item" href="../Propose_manpower/index.php" data-i18n="nav.navbars.nav_brand_center">เสนอขอบรรจุกำลังพล</a>
                            </li>
                        </ul>
                    </li>
             
            </ul> 
        </li> -->

        
        <!-- ระบบงานแผนบรรจุกำลังพลประจำปี -->
        <!-- <li class=" nav-item hidden"><a href="#"><i class="la la-book"></i><span class="menu-title" data-i18n="nav.navbars.main">ระบบงานแผนบรรจุกำลังพลประจำปี</span></a>
            <ul class="menu-content">
                <li><a class="menu-item" href="../Propose_approve/index.php">เสนอขอบรรจุกำลังพล/บันทึกผลการอนุมัติ</a>
                </li>
                <li><a class="menu-item" href="../plan_type/index.php" data-i18n="nav.navbars.nav_dark">ประเภทแผน</a>
                </li>
                <li><a class="menu-item" href="../5Plan_later/index.php" data-i18n="nav.navbars.nav_semi">แผน 5 ปีย้อนหลัง</a>
                </li>
            </ul>
        </li> -->
         <!-- ระบบงานสรรหาและบรรจุกำลังพล la-odnoklassniki -->
         <!--
        <li class=" nav-item <?php if($menu1 =="RECRNAPPOINT"){ echo "open";} ?>"><a href="#"><i class="la la-street-view"></i><span class="menu-title" data-i18n="nav.navbars.main">ระบบงานสรรหากำลังพล</span></a>
            <ul class="menu-content">
                <li class=""><a class="menu-item <?php if($menu2 =="PlanManpower"){ echo "active";} ?>" href="../PlanManpower/index.php"> แผนการบรรจุกำลังพล</a> 
                </li>
                <li class=""><a class="menu-item <?php if($menu2 =="PlanManpowerReg"){ echo "active";} ?>" href="../PlanManpowerReg/index.php"> สมัครสอบ </a> 
                </li>
 
            </ul>
        </li> -->
<!-- 
        <li class=" nav-item  <?php if($menu1 =="RECRNAPPOINT"){ echo "open";} ?>"><a href="#"><i class="la la-street-view"></i><span class="menu-title" data-i18n="nav.vertical_nav.main">ระบบงานสรรหากำลังพล</span></a>
            <ul class="menu-content">
                <li><a class="menu-item" href="../PlanManpower/index.php" data-i18n="nav.navbars.nav_light">แผนการบรรจุกำลังพล จากกองแผน</a></li>
                <li><a class="menu-item" href="../PlanManpower/index.php" data-i18n="nav.navbars.nav_light">แผนการบรรจุกำลังพล</a></li>
                <li><a class="menu-item" href="../PlanManpower/regislist.php" data-i18n="nav.navbars.nav_light">รายชื่อผู้สมัครสอบ</a>
                <li><a class="menu-item" href="../PlanManpower/positionmatch.php" data-i18n="nav.navbars.nav_light">นำผู้รายชื่อผู้สอบผ่านลงตำแหน่ง</a>
                <li><a class="menu-item" href="../PlanManpowerReg/index.php" data-i18n="nav.navbars.nav_light">สมัครสอบ</a>

                </li>
            </ul>
        </li> -->

      <?php 
      /*
      ?>
        <!-- ระบบงานวิเคราะห์ -->
        <li class=" nav-item"><a href="#"><i class="la la-street-view"></i><span class="menu-title" data-i18n="nav.vertical_nav.main">ระบบงานสรรหากำลังพล</span></a>
            <ul class="menu-content">
                <li><a class="menu-item" href="../PlanManpower/index.php" data-i18n="nav.navbars.nav_light">แผนการบรรจุกำลังพล (จากกองแผน)</a></li>
                <!-- <li><a class="menu-item" href="../PlanManpower/plan_detail.php" data-i18n="nav.navbars.nav_light">แผนการบรรจุกำลังพล</a></li> -->
                <!-- <li><a class="menu-item" href="../PlanManpower/list_province.php" data-i18n="nav.navbars.nav_light">จัดกลุ่มคุณวุฒิ/จังหวัด</a></li> -->
                <li><a class="menu-item" href="#" data-i18n="nav.page_layouts.3_columns.main">แผนการบรรจุกำลังพล</a>
                    <ul class="menu-content">
                    <li><a class="menu-item" href="../PlanManpower/list_province.php" data-i18n="nav.navbars.nav_light">กำลังพลพิเศษ</a></li>
                    <li><a class="menu-item" href="../PlanManpower/list_province.php" data-i18n="nav.navbars.nav_light">น.ประทวนทำหน้า</a></li>
                    <li><a class="menu-item" href="../PlanManpower/list_province.php" data-i18n="nav.navbars.nav_light">ประทวนป.ตรี ปรับ</a></li>
                </ul>
                </li>
             
                <li><a class="menu-item" href="../PlanManpower/regislist.php" data-i18n="nav.navbars.nav_light">รายชื่อผู้สมัครสอบ</a>
                <li><a class="menu-item" href="../PlanManpower/positionmatch.php" data-i18n="nav.navbars.nav_light">นำผู้รายชื่อผู้สอบผ่านลงตำแหน่ง</a>
                <li><a class="menu-item" href="../PlanManpowerReg/index.php" data-i18n="nav.navbars.nav_light">สมัครสอบ</a>
            </ul>
        </li> 

        <!-- ระบบงานบรรจุกำลังพล la-odnoklassniki --> 
        <li class=" nav-item <?php if($menu1 =="PERSONASSIGN"){ echo "open";} ?>">
            <a href="#"><i class="la la-magic"></i><span class="menu-title" data-i18n="nav.navbars.main">ระบบงานบรรจุกำลังพล</span></a>
            <ul class="menu-content">
                <li class="<?php if($menu2 =="PSETTING"){ echo "open";} ?>"><a class="menu-item " href="#">การตั้งค่า </a> 
                    <ul class="menu-content">
                        <li <?php if($menu3 =="SETTING1"){ echo "active";} ?>><a class="menu-item" href="../PlanManpowerAssign/setting1.php" data-i18n="nav.navbars.nav_light">ตั้งค่าปริญญาและคุณวุฒิ</a></li>
                        <li <?php if($menu3 =="SETTING2"){ echo "active";} ?>><a class="menu-item" href="../PlanManpowerAssign/setting2.php" data-i18n="nav.navbars.nav_light">ตั้งค่าประเภทปัจจัย</a></li>
                        <li <?php if($menu3 =="SETTING3"){ echo "active";} ?>><a class="menu-item" href="../PlanManpowerAssign/setting3.php" data-i18n="nav.navbars.nav_light">ตั้งค่าปัจจัยตามประเภทคุณวุฒิ</a></li>
                    </ul>
                </li>
                <li class=""><a class="menu-item <?php if($menu2 =="PlanManpowerAssign"){ echo "active";} ?>" href="../PlanManpowerAssign/assign_index.php"> บรรจุกำลังพล </a> 
                <li class="<?php if($menu2 =="AssignNumber"){ echo "active";} ?>"><a class="menu-item " href="../PlanManpowerAssign/assignNumber_index.php"> บรรจุกำลังพล2 </a> 
                <li class="<?php if($menu2 =="assignCalculate_index"){ echo "active";} ?>"><a class="menu-item " href="../PlanManpowerAssign/assignCalculate_index.php"> บรรจุกำลังพล3 </a> 
                <li class="<?php if($menu2 =="searchAssign"){ echo "active";} ?>"><a class="menu-item " href="../PlanManpowerAssign/searchAssign.php"> บันทึก/แก้ไข การบรรจุกำลังพล </a> 
                <li class="<?php if($menu2 =="profile"){ echo "active";} ?>"><a class="menu-item " href="../PlanManpowerAssign/profile.php"> บันทึก/แก้ไข ประวัติการบรรจุกำลังพล </a> 
                <li class="<?php if($menu2 =="searchName"){ echo "active";} ?>"><a class="menu-item " href="../PlanManpowerAssign/searchName.php"> ค้นหารายชื่อที่บรรจุ </a> 
                <li class="<?php if($menu2 =="SearchData"){ echo "active";} ?>"><a class="menu-item" href="../PlanManpowerAssign/SearchData.php"> ค้นหาข้อมูล </a> 
                <li class="<?php if($menu2 =="searchReport"){ echo "active";} ?>"><a class="menu-item" href="../PlanManpowerAssign/searchReport.php"> ค้นหารายงานสรุปการออกคำสั่งบรรจุประจำปี </a> 
                <li class="<?php if($menu2 =="saveAndExportReport"){ echo "active";} ?>"><a class="menu-item" href="../PlanManpowerAssign/saveAndExportReport.php"> บันทึกและออกคำสั่ง น.ประทวนเลื่อนฐานะ(คอค.) </a> 
                <li class="<?php if($menu2 =="stepLevel"){ echo "active";} ?>"><a class="menu-item" href="../PlanManpowerAssign/stepLevel.php"> บันทึก/แก้ไข น.ประทวนเลื่อนฐานะ(คอค.) </a> 
            </ul>
        </li> 

        
 <!-- ระบบงานสรรหา -->
 <li class=" nav-item"><a href="#"><i class="la la-street-view"></i><span class="menu-title" data-i18n="nav.vertical_nav.main">ระบบงานสรรหากำลังพล</span></a>
            <ul class="menu-content">
                <li><a class="menu-item" href="../PlanManpower/index.php" data-i18n="nav.navbars.nav_light">แผนการบรรจุกำลังพล (จากกองแผน)</a></li>
                <!-- <li><a class="menu-item" href="../PlanManpower/plan_detail.php" data-i18n="nav.navbars.nav_light">แผนการบรรจุกำลังพล</a></li> -->
                <li><a class="menu-item" href="../PlanManpower/realName.php" data-i18n="nav.navbars.nav_light">ค้นหารายชื่อตัวจริงเเละตัวสำรอง</a></li>
                <li><a class="menu-item" href="#" data-i18n="nav.page_layouts.3_columns.main">แผนการบรรจุกำลังพล</a>
                    <ul class="menu-content">
                    <li><a class="menu-item" href="../PlanManpower/list_province.php" data-i18n="nav.navbars.nav_light">กำลังพลพิเศษ</a></li>
                    <li><a class="menu-item" href="../PlanManpower/list_province.php" data-i18n="nav.navbars.nav_light">น.ประทวนทำหน้าที่</a></li>
                    <li><a class="menu-item" href="../PlanManpower/list_province.php" data-i18n="nav.navbars.nav_light">น.ประทวนป.ตรี ปรับ</a></li>
                </ul>
                </li>
             
                <li><a class="menu-item" href="../PlanManpower/regislist.php" data-i18n="nav.navbars.nav_light">รายชื่อผู้สมัครสอบ</a>
                <li><a class="menu-item" href="../PlanManpower/positionmatch.php" data-i18n="nav.navbars.nav_light">นำผู้รายชื่อผู้สอบผ่านลงตำแหน่ง</a>
                <li><a class="menu-item" href="../PlanManpowerReg/index.php" data-i18n="nav.navbars.nav_light">สมัครสอบ</a>
</ul>
   </li>
    

   <!-- ระบบงานบรรจุกำลังพล -->

         
        <li class=" nav-item <?php if($menu1 =="PERSONASSIGN"){ echo "open";} ?>">
        <a href="#"><i class="la la-magic"></i><span class="menu-title" data-i18n="nav.navbars.main">ระบบงานบรรจุกำลังพล</span></a>
            <ul class="menu-content">
            <li class="<?php if($menu2 =="PSETTING"){ echo "open";} ?>"><a class="menu-item " href="#">การตั้งค่า </a> 
                    <ul class="menu-content">
                        <li <?php if($menu3 =="SETTING1"){ echo "active";} ?>><a class="menu-item" href="../PlanManpowerAssign/setting1.php" data-i18n="nav.navbars.nav_light">ตั้งค่าปริญญาและคุณวุฒิ</a></li>
                        <li <?php if($menu3 =="SETTING2"){ echo "active";} ?>><a class="menu-item" href="../PlanManpowerAssign/setting2.php" data-i18n="nav.navbars.nav_light">ตั้งค่าประเภทปัจจัย</a></li>
                        <li <?php if($menu3 =="SETTING3"){ echo "active";} ?>><a class="menu-item" href="../PlanManpowerAssign/setting3.php" data-i18n="nav.navbars.nav_light">ตั้งค่าปัจจัยตามประเภทคุณวุฒิ</a></li>
                    </ul>
                </li>
            <li class="<?php if($menu2 =="AssignNumber"){ echo "active";} ?>"><a class="menu-item " href="../PlanManpowerAssign/assignNumber_index.php"> บรรจุกำลังพล </a> 
            <li class="<?php if($menu2 =="searchAssign"){ echo "active";} ?>"><a class="menu-item " href="../PlanManpowerAssign/searchAssign.php"> บันทึก/แก้ไข การบรรจุกำลังพล </a> 
            <li class="<?php if($menu2 =="profile"){ echo "active";} ?>"><a class="menu-item " href="../PlanManpowerAssign/profile.php"> บันทึก/แก้ไข ประวัติการบรรจุกำลังพล </a> 
            <li class="<?php if($menu2 =="searchName"){ echo "active";} ?>"><a class="menu-item " href="../PlanManpowerAssign/searchName.php"> ค้นหารายชื่อที่บรรจุ </a> 
            <li class="<?php if($menu2 =="SearchData"){ echo "active";} ?>"><a class="menu-item" href="../PlanManpowerAssign/SearchData.php"> ค้นหาข้อมูล </a> 
            <li class="<?php if($menu2 =="searchReport"){ echo "active";} ?>"><a class="menu-item" href="../PlanManpowerAssign/searchReport.php"> ค้นหารายงานสรุปการออกคำสั่งบรรจุประจำปี </a> 
            <li class="<?php if($menu2 =="saveAndExportReport"){ echo "active";} ?>"><a class="menu-item" href="../PlanManpowerAssign/saveAndExportReport.php"> บันทึกและออกคำสั่ง น.ประทวนเลื่อนฐานะ(คอค.) </a> 
            <li class="<?php if($menu2 =="stepLevel"){ echo "active";} ?>"><a class="menu-item" href="../PlanManpowerAssign/stepLevel.php"> บันทึก/แก้ไข น.ประทวนเลื่อนฐานะ(คอค.) </a> 

            </ul>
        </li>
        <?php 
        */
        /*
        ?>
         <!-- ระบบสัสดี  -->
         <li class=" nav-item"><a href="#"><i class="la la-clipboard"></i><span class="menu-title" data-i18n="nav.vertical_nav.main">ระบบงานสัสดี</span></a>
            <ul class="menu-content">
                <li><a class="menu-item" href="../Search_for_government/Search_for_government_official.php" data-i18n="nav.navbars.nav_light">ค้นหาบัญชีรายชื่อข้าราชการ</a>
                </li>
                <li><a class="menu-item" href="../Record_of_registration/index.php" data-i18n="nav.navbars.nav_light">บันทึกการแก้ไขข้อมูล</a>
                </li>
                <li><a class="menu-item" href="../Course_government/index.php" data-i18n="nav.navbars.nav_light">บันทึกขึ้นทะเบียนกองประจำการ</a>
                </li>
                <li><a class="menu-item" href="../Course_government/index.php" data-i18n="nav.navbars.nav_light">เพิ่มหน้าใหม่ เฉพาะแก้ไขชื่อ - ชื่อสกุล</a>
                </li> 

            </ul>
        </li>

        <!-- ระบบงานโอนย้าย -->
        <li class=" nav-item"><a href="#"><i class="la la-random"></i><span class="menu-title" data-i18n="nav.vertical_nav.main">ระบบงานย้ายโอน</span></a>
            <ul class="menu-content">
                <li><a class="menu-item" href="../Course_government/index.php" data-i18n="nav.navbars.nav_light">หลักสูตรที่ใช้ในการบรรจุเข้ารับราชการ</a>
                </li>

            </ul>
        </li>
       
        <!-- งานส่วนกลางสำหรับผู้ดูแลระบบ -->
        <li class=" nav-item"><a href="#"><i class="la ft-user-check"></i><span class="menu-title" data-i18n="nav.page_headers.main">งานส่วนกลางสำหรับผู้ดูแลระบบ</span></a>
            <ul class="menu-content">
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >ข้อมูลหลัก-งานประวัติรับราชการ</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >ประเภทกำลังพล</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >ยศ</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >คำนำหน้า</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >ประเภทแฟ้ม</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >กำเนิดแรกบรรจุ</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >คุณวุฒิแรกบรรจุ</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >กำเนิดปรับสภาพ</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >คุณวุฒิปรับสภาพ</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >ระยะเวลาการศึกษา</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >เหล่า</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >จำพวกทหาร</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >สังกัด</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >สถานภาพ</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >ประเภทการสูญเสีย</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >สาเหตุการปลด/สูญเสีย</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >เพศ</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >สัญชาติ-เชื้อชาติ</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >ศาสนา</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >อาชีพ</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >ประเภทบำเน็จ</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >ชั้นบำเน็จ</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >สถานภาพยศ</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >สถานภาพตำแหน่ง</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >วาระ</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >ประเภทเงิน</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >ประเภทแฟ้ม</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >สภานภาพสมรส</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >ลงโทษวินัย</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >กลุ่มกรณีความผิด</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >กรณีความผิด</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >ทัณฑ์ที่ได้รับ</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >ประเทศ</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >ประเภทเครื่องราชอิสริยาภรณ์</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >เครื่องราชอิสริยาภรณ์</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >ประเภทความรู้พิเศษ</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >ความรู้พิเศษ</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >สถานภาพบุคคล</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >ระดับและชั้นของเงินเดือน</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >เงินเพิ่มค่าครองชีพ</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >ประเภทการลา</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >คำสั่ง</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >รายละเอียดของทัณฑ์ที่สามารถลงโทษได้</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >ชื่อหลักสูตร</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >สถานภาพคู่สมรส</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >ได้รับวันทวีคูณเนื่องจาก</a>
                </li>
                <li><a class="menu-item" href="layout-content-left-sidebar.html" >รายการวันทวีคูณ</a>
                </li>
            </ul>
        </li>
        <!-- ผู้ใช้งาน -->
        <li class=" nav-item"><a href="#"><i class="la la-share-alt-square"></i><span class="menu-title" data-i18n="nav.navbars.main">ผู้ใช้งาน</span></a>
        </li>
        <?php 
        */
        ?>
        </ul>
    </div>
</div>