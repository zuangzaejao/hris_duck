<style>
#modalLoading.modal {
    text-align: center;
    padding: 0 !important;
    background-color: rgba(0, 0, 20, 0.4);
}

#modalLoading .modal:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -4px;
}

#modalLoading .modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
}

#modalLoading .modal-body {
    width: 100%;
    height:100%;
}

#modalLoading .modal-content {
    background-color: transparent;
    border: none;
}
</style>

<div  id="modalLoading" class="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <img src="../../Asset/Images/gif-To-not-Bg-Th.gif" style="width:90%" alt="">
            </div>
        </div>
    </div>
</div>



<!-- ####################################   modal_delete  #################################### -->
<div class="modal fade text-left modal_custom1" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="modal_delete"  aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <form>
          <div class="my-2 mx-2 px-2 py-1">
            <div class="text-center"> 
              <span class=" fontcolor2 "> <i class="la  la-trash-o la10" ></i> </span>
              <div class="pb-3 pt-1">
              <h3> ต้องการลบใช่หรือไม่ ?</h3> 
              </div>
            </div>
            <div class="col-12 d-flex justify-content-center"> 
              <div class="row"> 
                  <a href="#" class="btn btn-min-width mb-1 mr-1  round btn-success" role="button"  data-dismiss="modal">
                  <span></span> ยกเลิก </a> 
                  <span id="btn_delete" ></span>  
              </div>
            </div>
          </div> 
        </form>
      </div> 
    </div> 
</div>
<!-- ####################################   end modal_delete  #################################### -->
    <!-- ####################################   modal_createcf  #################################### -->
<div class="modal fade text-left modal_custom1" id="modal_createcf" tabindex="-1" role="dialog" aria-labelledby="modal_createcf"  aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <form>
          <div class="my-2 mx-2 px-2 py-1">
            <div class="text-center"> 
              <span class=" fontcolor3 "> <i class="la la-file-text-o la10" ></i> </span>
              <div class="pb-3 pt-1">
              <h3> ต้องการบันทึกข้อมูล ?</h3> 
              </div>
            </div>
            <div class="col-12 d-flex justify-content-center"> 
              <div class="row"> 
                <button type="button" class="btn btn-min-width mb-1 mr-1  round btn-success" id="btncreate"   >  บันทึก </button>
                <button class="btn btn-min-width mb-1 mr-1  round btn-danger" role="button"  data-dismiss="modal">  ยกเลิก </button> 
              </div>
            </div>
          </div> 
        </form>
      </div> 
    </div> 
</div>
<!-- ####################################   end modal_createcf  #################################### -->
<!-- ####################################   modal_editcf   #################################### -->
<div class="modal fade text-left modal_custom1" id="modal_editcf" tabindex="-1" role="dialog" aria-labelledby="modal_editcf"  aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <form>
          <div class="my-2 mx-2 px-2 py-1">
            <div class="text-center"> 
              <span class=" fontcolor3 "> <i class="la la-file-text-o la10" ></i> </span>
              <div class="pb-3 pt-1">
              <h3> ต้องการแก้ไขข้อมูล ?</h3> 
              </div>
            </div>
            <div class="col-12 d-flex justify-content-center"> 
              <div class="row"> 
                <button type="button" class="btn btn-min-width mb-1 mr-1  round btn-success" id="btnedit"   >  บันทึก </button>
                <button class="btn btn-min-width mb-1 mr-1  round btn-danger" role="button"  data-dismiss="modal">  ยกเลิก </button> 
              </div>
            </div>
          </div> 
        </form>
      </div> 
    </div> 
</div>
<!-- ####################################   end modal_editcf  #################################### -->
 