<!-- MAIN TAB PROFILE -->
<ul class="nav nav-tabs nav-top-border nav-topline">
  <li class="nav-item">
      <a class="nav-link active" id="main-profile_1" data-toggle="tab" aria-controls="tab_mp1" href="#tab_mp1" aria-expanded="true">&nbsp;ประวัติข้าราชการ&nbsp;</a>
  </li>
  <li class="nav-item">
      <a class="nav-link" id="main-profile_2" data-toggle="tab" aria-controls="tab_mp2" href="#tab_mp2" aria-expanded="false"><del>&nbsp;ภูมิลำเนา&nbsp;</del></a>
  </li>
  <li class="nav-item">
      <a class="nav-link" id="main-profile_3" data-toggle="tab" aria-controls="tab_mp3" href="#tab_mp3" aria-expanded="false">&nbsp;การศึกษา/ความรู้/ความชำนาญ&nbsp;</a>
  </li>
  <li class="nav-item">
      <a class="nav-link" id="main-profile_4" data-toggle="tab" aria-controls="tab_mp4" href="#tab_mp4" aria-expanded="false">&nbsp;ยศ/ตำแหน่ง/เครื่องราชอิสริยาภรณ์&nbsp;</a>
  </li>
  <li class="nav-item">
      <a class="nav-link" id="main-profile_5" data-toggle="tab" aria-controls="tab_mp5" href="#tab_mp5" aria-expanded="false">&nbsp;ค่าตอบแทนเงินเดือน&nbsp;</a>
  </li>
  <li class="nav-item">
      <a class="nav-link" id="main-profile_6" data-toggle="tab" aria-controls="tab_mp6" href="#tab_mp6" aria-expanded="false">&nbsp;ความประพฤติ&nbsp;</a>
  </li>
  <li class="nav-item">
      <a class="nav-link" id="main-profile_7" data-toggle="tab" aria-controls="tab_mp7" href="#tab_mp7" aria-expanded="false">&nbsp;สูญเสีย&nbsp;</a>
  </li>
  <li class="nav-item">
      <a class="nav-link" id="main-profile_8" data-toggle="tab" aria-controls="tab_mp8" href="#tab_mp8" aria-expanded="false">&nbsp;ECL&nbsp;</a>
  </li>
  <li class="nav-item">
      <a class="nav-link" id="main-profile_9" data-toggle="tab" aria-controls="tab_mp9" href="#tab_mp9" aria-expanded="false">&nbsp;งานประเมิน&nbsp;</a>
  </li>
  <li class="nav-item">
      <a class="nav-link" id="main-profile_10" data-toggle="tab" aria-controls="tab_mp10" href="#tab_mp10" aria-expanded="false">&nbsp;ประวัติรายงาน 904&nbsp;</a>
  </li>
</ul>
