<?php 

require_once '../../config.php';
require_once '../../Model/Ducklab/duck.class.php'; 
require_once '../../Model/Ducklab/contents.class.php'; 
require_once '../../Model/Ducklab/org.class.php'; 
require_once '../../Model/Ducklab/func.php'; 
require_once '../include/header.php'; 
 
  
$menu1 ="ORGSTRUC" ;
$menu2 ="ORGSTRUCPOSITION";
$menu3 ="POSITIONSETUP"; 
 
   

 
    $clsorg = new OrgClass();
  
    $OrgTypeID = $_GET['OrgTypeID'] ;  
    $OrgLevel2 = $_GET['OrgLevel2'] ; 
    $PersonTypeID = $_GET['PersonTypeID'] ; 
    
    $OrgLevel2DID = $OrgLevel2  ;

   $OrgTypeData =  $clsorg->LoadOnce('OrgType',array('OrgTypeID'=>$OrgTypeID) ); 
    
 
?>
<link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/ui/dragula.min.css">
<?php 
include '../include/menu.php';  
include_once '../include/modelOnload.php' ;
?>
<style>
.box_positionsetup ul.list-group{
    margin: 10px;
}
</style>
    <input type="hidden" name="OrgLevel2DID" id="OrgLevel2DID" value="<?php echo $OrgLevel2DID ;?>" >
    <section>
        <div class="app-content content">
            <div class="content-wrapper">
                <div class="content-header row">
                    <div class="content-header-left col-md-6 col-12 mb-2">
                        <div class="htab" ></div>
                        <h3 class="content-header-title">บันทึกกรอบอัตราตำแหน่ง</h3>
                        <div class="row breadcrumbs-top">
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="../home/index.php">ระบบงานโครงสร้างอัตรากำลังพล </a> </li>
                                    <li class="breadcrumb-item"><a href="../home/index.php">ตำแหน่ง </a> </li>
                                    <li class="breadcrumb-item active">บันทึกกรอบอัตราตำแหน่ง <?php echo $OrgTypeID ."-".$OrgLevel2."-".$PersonTypeID ; ?></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

           

                <div class="container-fluid">
                    <div class="row">

                        <div class="col-lg-4">
                            <div class="content-body">
                                <div class="sidebar-content card d-none d-lg-block">
                                    <div class="card-header boxheader1" >
                                        โครงการส่วนราชการ
                                        <a style="float:right;"><i class="la la-sitemap"></i> </a>
                                    </div>
                                    <div class="card-body "  >
                                        <div class=" text-center hidden" >


                                            <button class="btn btn-sm btn_b2" onclick="create()">
                                                <span class="la la-plus-circle" style=""> เพิ่ม</span>
                                            </button>
                                            <a href="#" class="btn btn-sm btn_b2" >
                                                <span class="ft-trash-2"> ลบ</span>
                                            </a>
                                            <a href="#" class="btn btn-sm btn_b2" >
                                                <span class="ft-copy"> คัดลอก</span>
                                            </a>
                                            <a href="#" class="btn btn-sm btn_b2" >
                                                <span class="la la-gavel"> คำสั่ง</span>
                                            </a>
                                            <a href="#" class="btn btn-sm btn_b2">
                                                <span class="la la-print">พิมพ์</span>
                                            </a>
                                            <a href="testHi.php" class="btn btn-sm btn_b2 hidden" >
                                                <span class="la la-print" >แก้ไขโครงสร้าง</span>
                                            </a>
                                        </div> 

                                        <div class="card-content">
                                            <div class="card-body skin-flat"> 

                                            <?php 
                                                  require_once 'detail_setting.php'; 
                                            ?>
                                                


                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    

                        <div class="col-lg-8">    
                            <?php  require_once "detail_position.php" ; ?>  
                        </div> 
                    </div> 
                </div> 
            </div>
        </div>
    </section>
 

<style>

</style>

<!-- footer -->
<?php include '../include/footer.php'; ?>



<script type="text/javascript">
    $(document).ready(function() {
          
    }); 

    function gotoCreate(){ 
        window.location.href="create.php?OrgLevelDID="+$("#OrgLevelDIDtxt").val()+"&OrgType="+ $("#OrgTypeIDtxt").val()+"&OrgLevel2="+$("#OrgLevel2DID").val();
    }
 </script>
