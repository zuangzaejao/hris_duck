<?php 
   require_once '../../config.php';
   require_once '../../Model/Ducklab/duck.class.php'; 
   require_once '../../Model/Ducklab/contents.class.php'; 
   require_once '../../Model/Ducklab/org.class.php'; 
   require_once '../../Model/Ducklab/func.php'; 
   require_once '../include/header.php'; 
 
 
     
$menu1 ="ORGSTRUC" ;
$menu2 ="ORGSTRUCPOSITION";
$menu3 ="POSITIONSETUP"; 


$clsorg = new OrgClass();

$OrgLevelDID =  $_REQUEST['OrgLevelDID'];
$OrgType =  $_REQUEST['OrgType'];



$orglevelx = substr($OrgLevelDID,1,1) ;


$tbname = 'OrgLevel'.$orglevelx ; 
$orgdesc  = $clsorg->LoadOnce( $tbname,array('OrgLevDID'=>$OrgLevelDID)) ;
 
$OrgLevel2 =  $_REQUEST['OrgLevel2'];

 ?>
  <?php include '../include/menu.php'; ?>
   
  <?php include_once '../include/modelOnload.php' ?>


  <div class="app-content content">
      <div class="content-wrapper">
          <div class="content-header row">
              <div class="content-header-left col-md-6 col-12 mb-2">
                  <h3 class="content-header-title">บันทึกกรอบอัตราตำแหน่ง </h3>
                  <div class="row breadcrumbs-top">

                  </div>
              </div>

          </div>
          <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="./index.php">ระบบงานโครงสร้างอัตรากำลังพล</a></li>
                  <li class="breadcrumb-item"><a href="./index.php">ตำแหน่ง</a></li>
                  <li class="breadcrumb-item active" aria-current="page"> เพิ่มกรอบอัตราตำแหน่ง  </li>
              </ol>
          </nav>
          <div class="content-body">
              <!-- Basic form layout section start -->
              <section id="horizontal-form-layouts">

                  <div class="row">
                      <div class="col-md-12">
                          <div class="card">
                                <div class="card-header">
                                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                  <div class="heading-elements">
                                      <ul class="list-inline mb-0">
                                          <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                          <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                          <li><a data-action="close"><i class="ft-x"></i></a></li>
                                      </ul>
                                  </div>
                                </div>
                                <?php 
                                    //echo "OrgType: " .$OrgType;
                                  //echo "OrgLevelDID: " .$OrgLevelDID ."::". $orglevelx; 
                                  //echo "<pre>".print_r($orgdesc,true)."</pre>";
                                  ?>
                                <div class="card-content collpase show">
                                    <div class="card-body  my-2 mx-2">
                                            <div class="row"> 
                                                <div class="col-md-6">
                                                    <div class="form-group"> 
                                                        <div class="">
                                                            <label for="OrgTypeID">  โครงสร้าง </label>
                                                        </div> 
                                                        <select id="OrgTypeID" name="OrgTypeID"   class="form-control select2" disabled>
                                                        </select>
                                                    </div>
                                                </div>
                                               
                                            </div> 
                                            <div class="row"> 
                                                <!-- <div class="col-md-6">
                                                    <div class="form-group"> 
                                                        <div class="">
                                                            <label for="OrgTypeID">  สังกัด </label>
                                                        </div> 
                                                         <input  type="text" id="OrgLevName" name="OrgLevName" value="<?php echo $orgdesc['OrgLevName'];?>"  class="form-control" disabled> 
                                                    </div>
                                                </div> -->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="">
                                                            <label for="OrgLevName"> หน่วยงาน </label>
                                                        </div>   
                                                        <input type="text" id="OrgLevName" name="OrgLevName" value="<?php echo $orgdesc['OrgLevName'];?>"  class="form-control" disabled> 

                                                        <input type="hidden" id="OrgLevelDID" name="OrgLevelDID" value="<?php echo $OrgLevelDID;?>"  class="form-control"  > 
                                                        
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="row">   
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="">
                                                            <label for="HrtPersonTypeID"> ประเภทกำลังพล </label>
                                                        </div>  
                                                        <select id="HrtPersonTypeID" name="HrtPersonTypeID" class="form-control select2">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="">
                                                            <label for="PosID"> ชื่อตำแหน่ง </label>
                                                        </div>  
                                                        <select id="PosID" name="PosID" class="form-control select2">
                                                            <option >  กรุณาเลือกประเภทกำลังพล </option>
                                                        </select>
                                                        <input type="hidden" id="PosIDName" name="PosIDName" >
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">   
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="">
                                                            <label for="RankFixID"> เงินเดือนอัตรา </label>
                                                        </div>  
                                                        <select id="RankFixID" name="RankFixID" class="form-control select2">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <div class="">
                                                            <label for="PositionNum"> จำนวน </label>
                                                        </div>  
                                                        <input type="number" id="PositionNum" name="PositionNum" class="form-control" value="1">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">   
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="">
                                                            <label for="HrtChiefSpc1"> สายวิทยาการ 1 </label>
                                                        </div>  
                                                        <select id="HrtChiefSpc1" name="HrtChiefSpc1" class="form-control select2">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="">
                                                            <label for="HrtChiefSpc2"> สายวิทยาการ 2 </label>
                                                        </div>  
                                                        <select id="HrtChiefSpc2" name="HrtChiefSpc2" class="form-control select2">
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <!-- <hr class="" style="border: 1px solid #3f51b536;"> -->

                                            <div class="row hidden">   
                                                <div class="col-md-5  p-2 border">
                                                    <div class="row">   
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="">
                                                                    <h4> ใช้งาน </h4>
                                                                </div>   
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">   
                                                        <div class="col-md-12">
                                                            <div class="form-group ">
                                                                <div class="">
                                                                    <label for=""> คำสั่ง </label>
                                                                </div>  
                                                                <select id="" name="" class="form-control select2">
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">   
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="">
                                                                    <label for=""> เลขที่คำสั่ง  </label>
                                                                </div>  
                                                                <input type="text" class="form-control" id="" name=""  value="">
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="row">   
                                                        <div class="col-md-8">
                                                            <div class="form-group">
                                                                <div class="">
                                                                    <label for=""> ลงวันที่  </label>
                                                                </div>  
                                                                <div class="input-group datep">
                                                                    <input type="text" class="form-control form2 pickadate-translations" placeholder="" id="" name="" data-value="" />
                                                                    <div class="input-group-append">
                                                                        <span class="input-group-text"> <span class="la la-calendar-o"></span> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">   
                                                        <div class="col-md-8">
                                                            <div class="form-group">
                                                                <div class="">
                                                                    <label for=""> วันที่มีผล  </label>
                                                                </div>  
                                                                <div class="input-group datep">
                                                                    <input type="text" class="form-control form2 pickadate-translations" placeholder="" id="" name="" data-value="" />
                                                                    <div class="input-group-append">
                                                                        <span class="input-group-text"> <span class="la la-calendar-o"></span> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> 
                                                </div>


                                                <div class="col-md-5 p-2 border">
                                                    
                                                    <div class="row">   
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="">
                                                                    <h4> ยกเลิก </h4>
                                                                </div>   
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">   
                                                        <div class="col-md-12">
                                                            <div class="form-group ">
                                                                <div class="">
                                                                    <label for=""> คำสั่ง </label>
                                                                </div>  
                                                                <select id="" name="" class="form-control select2">
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">   
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="">
                                                                    <label for=""> เลขที่คำสั่ง  </label>
                                                                </div>  
                                                                <input type="text" class="form-control" id="" name=""  value="">
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="row">   
                                                        <div class="col-md-8">
                                                            <div class="form-group">
                                                                <div class="">
                                                                    <label for=""> ลงวันที่  </label>
                                                                </div>  
                                                                <div class="input-group datep">
                                                                    <input type="text" class="form-control form2 pickadate-translations" placeholder="" id="" name="" data-value="" />
                                                                    <div class="input-group-append">
                                                                        <span class="input-group-text"> <span class="la la-calendar-o"></span> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">   
                                                        <div class="col-md-8">
                                                            <div class="form-group">
                                                                <div class="">
                                                                    <label for=""> วันที่มีผล  </label>
                                                                </div>  
                                                                <div class="input-group datep">
                                                                    <input type="text" class="form-control form2 pickadate-translations" placeholder="" id="" name="" data-value="" />
                                                                    <div class="input-group-append">
                                                                        <span class="input-group-text"> <span class="la la-calendar-o"></span> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> 
                                                </div> 
                                            </div>


                                            <hr class="" style="border: 1px solid #3f51b536;">
 
                                            <br> 
                                            <div class="row">
                                                <div class="col-md-12"> 
                                                    <label class="col-md-1 label-control" for="HrtPositionActive" style="padding-right:0px;">สถานะ</label>
                                                    <input type="checkbox" id="HrtPositionActive"  name="HrtPositionActive"  checked data-toggle="toggle" data-style="ios" data-on="ใช้งาน" data-off="ยกเลิก" data-onstyle="success" data-offstyle="danger" data-size="sm">
                                                </div>
                                            </div>

                                            <div class="form-actions text-center"> 

                                            
                                                <input type="text"  class="form-control border-primary"  name="OrgLevel2" id="OrgLevel2" value="<?php echo $OrgLevel2;?>">
                                                
                                                
                                                <input type="hidden"  class="form-control border-primary"  name="HrtPositionCreateBy" id="HrtPositionCreateBy" value="1">
                                                <input type="hidden"  class="form-control border-primary"  name="HrtPositionCreateDate" id="HrtPositionCreateDate" value="<?php echo GetToday('');?>">

                                                <button type="button" class="btn btn-danger  round btn-min-width mr-1 mb-1"  >ยกเลิก</button>
                                                <button type="button" class="btn btn-success  round btn-min-width mr-1 mb-1" id="btncreatecf" >บันทึก</button> 
                                            </div>
                                           
                                          </div>

                                      </form>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                   
              </section>
              <!-- // Basic form layout section end -->
          </div>
      </div>
  </div>
  <?php include '../include/footer.php'; ?>
  
   
    <script type="text/javascript">
    $( document ).ready(function() {

        org.LoadOrgTypeSel('<?php echo $OrgType;?>','OrgTypeID');// 
        org.LoadPersonTypeSel('','HrtPersonTypeID');

        //org.LoadRankFixID('','RankFixID');
        org.LoadRankSortSel('','RankFixID');

       
        

        $("#HrtPersonTypeID").change(function(){
               // org.LoadPosCodeSel('','PosID','<?php echo $OrgType;?>',$("#HrtPersonTypeID").val());
                org.LoadPosCodeSel('','PosID','',$("#HrtPersonTypeID").val());
                org.LoadChiefSpcSel('','HrtChiefSpc1', $("#HrtPersonTypeID").val());
                org.LoadChiefSpcSel('','HrtChiefSpc2', $("#HrtPersonTypeID").val());
        });

        $("#PosID").change(function(){
           //alert( $(this).data("pname") ); attr("data-id")
          // alert( $(this).attr("pname"));  
           //alert( $(this).text());  
          //  $("#PosIDName").val(result.HrtPosName);
          console.log($(this).find(':selected').data("pname"));
          $("#PosIDName").val($(this).find(':selected').data("pname"));
        });
       

   
        //org.LoadPosTypeGroupSel('','HrtPosTypeGroup');
        //org.LoadPosTypeSel('','HrtPosType','');

        /*
        $('#HrtPosTypeGroup').change(function(){
            org.LoadPosTypeSel('','HrtPosType',$('#HrtPosTypeGroup').val() );
        }); */
        $("#btncreatecf").click(function(){   
            $('#modal_createcf').modal('show');
        });
        $("#btncreate").click(function(){
            org.CreatePosition();
        });
    });

 
    </script>