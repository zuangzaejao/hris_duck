<div class="card">
    <div class="card-content">
        <div class="card-body">
            <a href="javascript:gotoCreate();" class="btn btn-social btn-min-width mb-1 btn_b1"  >
                <span class="la la-plus-circle"  ></span> เพิ่ม
            </a>
            <input type="hidden" id="dataTableSet" name="dataTableSet" value="0">
            <input type="hidden" id="OrgLevelDIDtxt" name="OrgLevelDIDtxt">
            <input type="hidden" id="OrgTypeIDtxt" name="OrgTypeIDtxt" value="<?php echo $OrgTypeID?>">
            <!-- <a href="./delete.php" class="btn btn-social btn-min-width mb-1 btn_b1"  >
                <span class="la la-trash-o"  ></span> ลบ
            </a> -->

            <table id="PositionTable" class="table table-striped  table-borderless table-hover table_custom1 " >
                <thead> 
                    <tr>
                        <th> 
                            <!-- <a class="skin-flat"><input type="checkbox" class="checkAll" onclick="toggle(this);" /> </a> -->
                    </th>
                        <th>ลำดับที่</th>
                        <th class="text-left">ชื่อตำแหน่ง</th>
                        <th>เงินเดือนอัตรา</th>
                        <th>จำนวนอัตรา</th>
                        <th>ลำดับจัดเรียง</th>
                        <th>ใช้งาน</th>
                        <th> วันที่ยกเลิก </th>

                    </tr>
                </thead>
                <tbody> </tbody>
            </table>
        </div>
    </div> 
</div>