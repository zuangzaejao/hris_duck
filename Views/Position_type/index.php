<?php 

require_once '../../config.php';
require_once '../../Model/Ducklab/duck.class.php'; 
require_once '../../Model/Ducklab/contents.class.php'; 
require_once '../../Model/Ducklab/org.class.php'; 
require_once '../../Model/Ducklab/hrt.class.php'; 
require_once '../../Model/Ducklab/func.php'; 
require_once '../include/header.php'; 
 
  
$menu1 ="ORGSTRUC" ;
$menu2 ="ORGSTRUCDATA";
$menu3 ="POSITIONTYPE"; 
   

  $clshrt = new HrtClass();

 
include_once '../include/menu.php'; 
include_once '../include/modelOnload.php' ;
?>
 
 <section>
   <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
           <div class="content-header-left col-md-6 col-12 mb-2">
              <div class="htab" ></div>
               <h3 class="content-header-title">ประเภทตำแหน่ง</h3>
              </div>
           </div>
           <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../home/index.php">ระบบงานโครงสร้างอัตรากำลังพล</a></li>
                        <li class="breadcrumb-item"><a href="../home/index.php">ข้อมูลทั่วไป</a></li>
                        <li class="breadcrumb-item active" aria-current="page">ประเภทตำแหน่ง</li>
               </ol>
            </nav>
            <div class="content-body">
                 
                 <section id="bootstrap3">
                     <div class="row">
                         <div class="col-12">
                             <div class="card">
                                 <div class="card-content collapse show">
                                     <div class="card-body card-dashboard">
                                         <p class="card-text"></p>
                                         
                                         <a href="./create.php" class="btn btn-social btn-min-width mb-1 btn_b1" >
                                             <span class="la la-plus-circle"></span> เพิ่ม
                                         </a>
                                         <!-- <a href="./delete.php" class="btn btn-social btn-min-width mb-1 btn_b1" >
                                             <span class="la la-trash-o"></span> ลบ
                                         </a> -->
                                         <table id="PosTypeTable" class="table table-striped table-borderless table-hover table_custom1">
                                             <thead>
                                                 <tr>
                                                     <th></th>
                                                     <th>ลำดับที่</th>
                                                     <th>รหัสประเภทตำเเหน่ง</th>
                                                     <th>ชื่อประเภทตำเเหน่ง</th>
                                                     <th>ชื่อย่อประเภทตำเเหน่ง</th>
                                                     <th>สถานะ</th>
                                                 </tr>
                                             </thead>
                                             <tbody>

                                             </tbody>
                                         </table>
                                         <form id="formset" method="post" action="">
                                           <input type="hidden" id="dataid" name="dataid" value="">
                                           <input type="hidden" id="action" name="action" value="">  
                                       </form>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
             </div>
 </section>

  <!-- footer -->
  <?php include '../include/footer.php'; ?> 
  
 <script type="text/javascript">
       
         
       $(document).ready(function() {
           
           hrt.LoadPosType();
            
       });
    </script> 