<?php 

require_once '../../config.php';
require_once '../../Model/Ducklab/duck.class.php'; 
require_once '../../Model/Ducklab/contents.class.php'; 
require_once '../../Model/Ducklab/org.class.php'; 
require_once '../../Model/Ducklab/hrt.class.php'; 
require_once '../../Model/Ducklab/func.php'; 
require_once '../include/header.php'; 
 
  
$menu1 ="ORGSTRUC" ;
$menu2 ="ORGSTRUCDATA";
$menu3 ="POSITIONTYPE"; 
   

  $clshrt = new HrtClass();

 
include_once '../include/menu.php'; 
include_once '../include/modelOnload.php' ;
?>


<div class="app-content content">
      <div class="content-wrapper">
          <div class="content-header row">
              <div class="content-header-left col-md-6 col-12 mb-2">
                  <h3 class="content-header-title">เพิ่มประเภทตำแหน่ง</h3>
                  <div class="row breadcrumbs-top">

                  </div>
              </div>

          </div>
          <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="./index.php">ระบบงานโครงสร้างอัตรากำลังพล</a></li>
                  <li class="breadcrumb-item"><a href="./index.php">ตำแหน่ง</a></li>
                  <li class="breadcrumb-item active" aria-current="page">เพิ่มประเภทตำแหน่ง</li>
              </ol>
          </nav>
          <div class="content-body">
              <!-- Basic form layout section start -->
              <section id="horizontal-form-layouts">

                  <div class="row">
                      <div class="col-md-12">
                          <div class="card">
                              <div class="card-header">
                                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                  <div class="heading-elements">
                                      <ul class="list-inline mb-0">
                                          <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                          <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                          <li><a data-action="close"><i class="ft-x"></i></a></li>
                                      </ul>
                                  </div>
                              </div>
                              <div class="card-content collpase show">
                                  <div class="card-body">
                                      <!-- <div class="card-text">
                                          <p>คำโปรย</p>
                                      </div> -->
                                      <form class="form form-horizontal">
                                          <div class="form-body">
                                          <div class="row">
                                                  <div class="col-md-6">
                                                      <label class="col-md-6 label-control" for="PosTypeID">รหัสกลุ่มพนักงาน</label>
                                                      <div class="col-md-12">
                                                          <div class="position-relative ">
                                                              <input type="text" id="PosTypeID" class="form-control border-primary"  name="PosTypeID">

                                                          </div>  
                                                     </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                      <label class="col-md-6 label-control" for="PosTypeName">ชื่อประเภทตำแหน่ง</label>
                                                      <div class="col-md-12">
                                                          <div class="position-relative">
                                                              <input type="text" id="PosTypeName" class="form-control border-primary" name="PosTypeName">
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                              <br>
                                              <div class="row">
                                                  <div class="col-md-6">
                                                      <label class="col-md-6 label-control" for="PosAbbrName">ชื่อย่อประเภทตำแหน่ง</label>
                                                      <div class="col-md-12">
                                                          <div class="position-relative ">
                                                              <input type="text" id="PosAbbrName" class="form-control border-primary"  name="PosAbbrName">

                                                          </div>  
                                                     </div>
                                                  </div>                                          
                                              </div>
                                              <br>
                                              <div class="row">
                                                  <div class="col-md-12">
                                                      <br>
                                                      <label class="col-md-1 label-control" for="HrtPrefixActive" style="padding-right:0px;">สถานะ</label>
                                                      <input type="checkbox" id="HrtPrefixActive" data-toggle="toggle" data-style="ios" data-on="ใช้งาน" data-off="ยกเลิก" data-onstyle="success" data-offstyle="danger" data-size="sm">
                                                  </div>
                                              </div>
                                              <div class="form-actions text-center" > 
                                                   
                                                   <input type="hidden"  class="form-control border-primary"  name="PosTypeCreateBy" id="PosTypeCreateBy" value="0">
                                                   <input type="hidden"  class="form-control border-primary"  name="PosTypeCreateDate" id="PosTypeCreateDate" value="<?php echo GetToday('');?>">
    
                                                     <button type="button" class="btn btn-danger  round btn-min-width mr-1 mb-1"  >ยกเลิก</button>
                                                     <button type="button" class="btn btn-success  round btn-min-width mr-1 mb-1" id="btncreatecf" >บันทึก</button>
   
                                                </div>
                                          </div>
                                           
                                      </form>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                
              </section>
              <!-- // Basic form layout section end -->
          </div>
      </div>
  </div>

  <?php include '../include/footer.php'; ?> 

<script type="text/javascript">
     
       
     $(document).ready(function() {
         
      $("#btncreatecf").click(function(){   
          $('#modal_createcf').modal('show');
      });
      $("#btncreate").click(function(){
          hrt.CreatePosType(); 
      });
          
     });
  </script> 