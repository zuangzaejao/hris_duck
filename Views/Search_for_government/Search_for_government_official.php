  <!-- header -->
  <?php include '../include/header.php'; ?>

  <!-- menu -->
  <?php include '../include/menu.php'; ?>

  <style>
/* ol > li > a {color:#222233;} */
.toggle.ios,
.toggle-on.ios,
.toggle-off.ios {
    border-radius: 20rem;
}

.toggle.ios .toggle-handle {
    border-radius: 20rem;
}
.btn-red {
    border-color: #FF1C1C !important;
    background-color: #FF1C1C !important;
    color: #FFFFFF;
}
.btn-orang {
    border-color: #FF9149 !important;
    background-color: #FF9149 !important;
    color: #FFFFFF;
}
.btn-gree {
    border-color: #5CB85C !important;
    background-color: #5CB85C !important;
    color: #FFFFFF;
}
.btn-cancel {
    border-color: #FF1C1C !important;
    background-color: #FF1C1C !important;
    color: #FFFFFF;
}

--
  </style>

  <section>

      <div class="app-content content">
          <div class="content-wrapper">
              <div class="content-header row">
                  <div class="content-header-left col-12 mb-2">
                      <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
                      <h3 class="content-header-title">ค้นหาบัญชีรายชื่อข้าราชการ เปลี่ยนชื่อ-สกุล</h3>
                  </div>
              </div>
              <div class="content-body">
              
             <section id="bootstrap3">
                      <div class="row">
                          <div class="col-12">
                              <div class="card">
                                  <div class="card-content collapse show">
                                      <div class="card-body card-dashboard">
                                          <p class="card-text"></p>
                                          <nav aria-label="breadcrumb">
                                              <ol class="breadcrumb">
                                                  <li class="breadcrumb-item">
                                                  <a href="../Record_of_registration/index.php">ระบบสัสดี</a></li>
                                                  <li class="breadcrumb-item active" aria-current="page">
                                                  ค้นหาบัญชีรายชื่อข้าราชการ เปลี่ยนชื่อ-สกุล</li>
                                              </ol>
                                          </nav>

                                          <div class="content-body">
        
                        <section class="horizontal-grid" id="horizontal-grid">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="card">
                                <div class="card-header" style="background-color:#0f1733;"><br>
                                <h4 class="card-title" style="color:#fff; ">&nbsp;&nbsp;&nbsp;&nbsp; เลือกการแสดงบ้านพักอาศัย ทอ.</h4>
                                <a class="heading-elements-toggle"><i class="ft-align-justify font-medium-3"></i></a>
                                
                                </div>
                                <div class="card-content collapse show">
                                <div class="card-body">
                                    <form action="#">
                                    <div class="form-body">

                                    <div class="row">

                                    <div class="col-3">
                                        <div class="form-group">เลขประจําตัวประจำ :
                                        <input type="text" id="disabledTextInput" class="form-control" placeholder="เลขประจําตัวประจำ">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">ชื่อ - นามสกุล :
                                        <input type="text" id="disabledTextInput" class="form-control" placeholder="ชื่อ - นามสกุล ">
                                        </div>
                                    </div>                        
                                    <div class="col-md-6">
                                        <div class="form-group">สังกัด :<select  class="select2 form-control"
                                                                                                                                    style="width: 90%;">
                                                                                                                                    <optgroup
                                                                                                                                        label="สังกัด">
                                                                                                                                        <option
                                                                                                                                            value="AK">
                                                                                                                                            เลือก
                                                                                                                                        </option>
                                                                                                                                        <option
                                                                                                                                            value="HI">
                                                                                                                                            กพ.ทอ.
                                                                                                                                        </option>
                                                                                                                                        <option
                                                                                                                                            value="HI">
                                                                                                                                            กพ.ทอ.
                                                                                                                                        </option> 
                                                                                                                                </select>
                                        
                                        </div>
                                    </div>                        
                      </div>

                      <div class="form-actions">
                        <div class="text-center">
                          <button type="submit" class="btn btn-primary"> ค้นหา </button>
                          <button type="reset" class="btn btn-warning" > ล้างค่า </button>
                        </div>
                      </div>

                                
                            </div>
                        </div>
                    </div>
                </section>

               
                <table class="table table-striped table-borderless table-hover ">
                                              <thead>
                                                  <tr align="center"
                                                      style="background-color:#0f1733; color:whitesmoke;">
                                                      <th><input type="checkbox" class="checkAll"
                                                              onclick="toggle(this);" /></th>
                                                      <th></th>
                                                      <th>ลำดับที่</th>
                                                      <th>หมายเลขประจำตัว</th>
                                                      <th>ยศ</th>
                                                      <th>วันที่เปลี่ยนแปลง</th>
                                                      <th>แก้ไขหมายเลขประจำตัว</th>
                                                      <th>แก้ไขหมายชื่อ</th>
                                                      <th>แก้ไขหมายชื่อสกุล</th>
                                                      <th>แก้ไขวันเกิด</th>
                                                      <th>แก้ไจขวันขึ้นทะเบียนกองประจำการ</th>
                                                      <th>แก้ไขวันล้วง</th>
                                                      <th>แก้ไขวันปลด</th>
                                                      <th>แก้ไขขึ้นทะเบียนเพราะ</th>
                                                  </tr>
                                              </thead>
                                              <tbody align="center">
                                                  <tr>
                                                      <td><input type="checkbox" class="checkAll" /></td>
                                                      <td>
                                                          <a href="./investigate.php"><i class="la la-pencil-square-o"
                                                                  style="color:#0f1733;"></i></a>
                                                      
                                                            <a href="./delete.php"><i class="la la-trash-o"
                                                                        style="color:#0f1733;"></i></a>
                                                      </td>
                                                      <td>1</td>
                                                      <td>322330086500</td>
                                                      <td>ร.อ.</td>
                                                      <td>1 ก.ค. 2558</td>
                                                      <td></td>
                                                      <td></td>
                                                      <td></td>
                                                      <td></td>
                                                      <td></td>
                                                      <td></td>
                                                      <td></td>
                                                      <td></td>
                                                  </tr>
                                                  <tr>
                                                  <tr>
                                                      <td><input type="checkbox" class="checkAll" /></td>
                                                      <td>
                                                          <a href="./investigate.php"><i class="la la-pencil-square-o"
                                                                  style="color:#0f1733;"></i></a>
                                                      
                                                            <a href="./delete.php"><i class="la la-trash-o"
                                                                        style="color:#0f1733;"></i></a>
                                                      </td>
                                                      <td>1</td>
                                                      <td>322330086500</td>
                                                      <td>ร.อ.</td>
                                                      <td>1 ก.ค. 2558</td>
                                                      <td></td>
                                                      <td></td>
                                                      <td></td>
                                                      <td></td>
                                                      <td></td>
                                                      <td></td>
                                                      <td></td>
                                                      <td></td>
                                                  </tr>
                                                  </tr>
                                              </tbody>
                                              <!-- <tfoot>
                        <tr>
                          <th>Name</th>
                          <th>Position</th>
                          <th>Office</th>
                          <th>Age</th>
                          <th>Start date</th>
                          <th>Salary</th>
                        </tr>
                      </tfoot> -->
                                          </table>


             </section>
             </section>

  <script src="http://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
      crossorigin="anonymous"></script>

  <script type="text/javascript">
$(document).ready(function() {
    console.log("ready");
    change_autorefreshdiv();
});

function change_autorefreshdiv() {
    // $('#prefixPage').addClass('active');
}

function toggle(source) {
    var checkboxes = document.querySelectorAll('.checkAll');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i] != source)
            checkboxes[i].checked = source.checked;
    }
}
  </script>



  <!-- footer -->
  <?php include '../include/footer.php'; ?>