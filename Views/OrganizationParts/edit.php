<?php 

require_once '../../config.php';
require_once '../../Model/Ducklab/duck.class.php'; 
require_once '../../Model/Ducklab/contents.class.php'; 
require_once '../../Model/Ducklab/org.class.php'; 
require_once '../../Model/Ducklab/func.php'; 
require_once '../include/header.php'; 
 
  
  $menu1 = "ORGSTRUC" ;
  $menu2 = "ORGSTRUCDEPT" ;
  $menu3 = "ORGSTRUCDEPTSETTING" ;
  $menu4 = "ORGANIZATIONPART" ;
   

  $clsorg = new OrgClass();

 
  $dataid = $_REQUEST['dataid'] ; 


  //$datao  = $clsorg->LoadOnce('OrgPart',array('OrgPartID'=> $dataid ));

include_once '../include/menu.php'; 
include_once '../include/modelOnload.php' ;
?>
 
  

 
  <div class="app-content content">
      <div class="content-wrapper">
          <div class="content-header row">
              <div class="content-header-left col-md-6 col-12 mb-2">
                  <h3 class="content-header-title">แก้ไขส่วนราชการ</h3>
                  <div class="row breadcrumbs-top">

                  </div>
              </div>

          </div>
          <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="./index.php">ระบบงานโครงสร้างอัตรากำลังพล</a></li>
                  <li class="breadcrumb-item"><a href="./index.php">โครงสร้าง</a></li>
                  <li class="breadcrumb-item active" aria-current="page">แก้ไขส่วนราชการ</li>
              </ol>
          </nav>
          <div class="content-body">
              <!-- Basic form layout section start -->
              <section id="horizontal-form-layouts">

                  <div class="row">
                      <div class="col-md-12">
                          <div class="card">
                              <div class="card-header">
                                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                  <div class="heading-elements">
                                      <ul class="list-inline mb-0">
                                          <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                          <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                          <li><a data-action="close"><i class="ft-x"></i></a></li>
                                      </ul>
                                  </div>
                              </div>
                              <div class="card-content collpase show">
                                  <div class="card-body">
                                      <!-- <div class="card-text">
                                          <p>คำโปรย</p>
                                      </div> -->
                                      <?php
                                      
                                      //echo "<pre>".print_r($datao ,true)."</pre>";
                                      ?>
                                      <form class="form form-horizontal">
                                          <div class="form-body">

                                            <div class="row">
                                                  <div class="col-md-6">
                                                      <label class="col-md-6 label-control" for="OrgPartName">  รหัสส่วนราชการ</label>
                                                      <div class="col-md-4">
                                                          <div class="position-relative ">
                                                              <input type="text" id="OrgPartCode" class="form-control border-primary"  disabled name="OrgPartCode" value="">

                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                              <br>

                                              <div class="row">
                                                  <div class="col-md-6">
                                                      <label class="col-md-6 label-control" for="OrgPart">ชื่อส่วนราชการ</label>
                                                      <div class="col-md-12">
                                                          <div class="position-relative ">
                                                              <input type="text" id="OrgPart" class="form-control border-primary" name="OrgPart" value="">

                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                               
                                              <div class="row">
                                                    <div class="col-md-12">
                                                      <br>
                                                      <label class="col-md-1 label-control" for="OrgPartActive" style="padding-right:0px;">สถานะ</label>
                                                      <input id="OrgPartActive" name="OrgPartActive" type="checkbox" <?php if($datao['OrgPartActive']=="1" ) { echo "checked";}?>  data-toggle="toggle" data-style="ios" data-on="ใช้งาน" data-off="ยกเลิก" data-onstyle="success" data-offstyle="danger" data-size="sm">
                                                    </div>
                                                </div>
                                                <div class="form-actions text-center">
                                                    <input type="hidden" name="id" id="id" value=""> 
                                                    <input type="hidden" name="dataid" id="dataid" value="<?php echo $dataid;?>"> 
                                                    <input type="hidden"  class="form-control border-primary"  name="OrgGroupTypeUpdateBy" id="OrgGroupTypeUpdateBy" value="1">
                                                    <input type="hidden"  class="form-control border-primary"  name="OrgGroupTypeUpdateDate" id="OrgGroupTypeUpdateDate" value="<?php echo GetToday('');?>">
                                                    <button type="button" class="btn btn-danger  round btn-min-width mr-1 mb-1"  >ยกเลิก</button>
                                                    <button type="button" class="btn btn-success  round btn-min-width mr-1 mb-1" id="btneditcf" >บันทึก</button>
                                                </div>
                                          </div>
                                      </form>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </section>
              <!-- // Basic form layout section end -->
          </div>
      </div>
  </div>
  
    <?php include '../include/footer.php'; ?>
   
    <script type="text/javascript">
    $( document ).ready(function() {
        org.GetOrgPart();

        
      
        $("#OrgPart").keyup(function(){ 
            var DataSet = {
                table: 'OrgPart',
                field: 'OrgPart',
                where: {
                    OrgPart : $("#OrgPart").val(),
                },
                wherenot: {
                    OrgPartID : $("#dataid").val(),
                }

             };
            org.checkFieldmore(DataSet,'OrgPart');
        });

        $("#btneditcf").click(function(){   
            $('#modal_editcf').modal('show');
        });
        $("#btnedit").click(function(){
            org.EditOrgPart();
        });
    });

 
    </script>