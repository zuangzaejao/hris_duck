<div class="tab-pane" id="tab16"
aria-labelledby="base-tab16">
<div class="container">
<div class="card-content">
<div class="card-body">
    <div class="card collapse-icon accordion-icon-rotate active">
        <div id="headingCollapse38"
            class="card-header bg-success">
            <a data-toggle="collapse"
                href="#collapse38"
                aria-expanded="true"
                aria-controls="collapse38"
                class="card-title lead white">
                <h6><U>ส่วนที่ 1</U>
                    ข้อมูลบิดา
                </h6>
            </a>
        </div>
        <div id="collapse38"
            role="tabpanel"
            aria-labelledby="headingCollapse38"
            class="card-collapse collapse show"
            aria-expanded="true">
            <div class="card-content">
             
      <!---///////////////////////////////////////////////////////////////////--->
         <div class="row">          
            <div class="col-md-6">
                <div class="card-body"
                    id="File_number">
                    หมายเลขประจำตัวราชการ
                    :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="card-block">
                    <div class="card-body"
                        id="noncomm_and_comm">
                        ยศ :
                        <select
                            class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศโท/พล.อ.ท.
                                </option> 
                            </optgroup>                
                        </select>
                        <a href="../Organizations/create.php"><i class="la la-plus-circle" style="font-size:36px;color:#0f1733;"></i></a>
                    </div>
                 </div>
             </div>
         </div>
     <!---///////////////////////////////////////////////////////////////////--->
    <div class="row">   
         <div class="col-md-6">
                <div class="card-body" id="File_number">
                    เลขประจำตัวประชาชน
                    :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                </div>
        </div>
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body" id="noncomm_and_comm">
                        คำนำหน้าชื่อ :
                        <select class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>                                  
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>
        </div>
         <!---///////////////////////////////////////////////////////////////////--->
        <div class="row">               
            <div
                class="col-md-6">
                <div class="card-body"
                    id="File_number">
                    ชื่อ :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-body"
                    id="File_number">
                    นามสกุล
                    :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                </div>
            </div>
        </div>
    <!---///////////////////////////////////////////////////////////////////--->
            <!---///////////////////////////////////////////////////////////////////--->
            <div class="row">               
            <div
                class="col-md-6">
                <div class="card-body"
                    id="File_number">
                    ชื่อใหม่ :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-body"
                    id="File_number">
                    นามสกุลใหม่
                    :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                </div>
            </div>
        </div>
    <!---///////////////////////////////////////////////////////////////////--->
    
  <div class="row">
    <div class="col-md-6">
        <div class="card-block">
            <div class="card-body" id="noncomm_and_comm">
                เชื้อชาติ :
                <select  class="select2 form-control" style="width: 100%;">
                    <optgroup
                        label="สัญญาบัตร">
                        <option
                            value="AK">
                            เลือก
                        </option>
                        <option
                            value="HI">
                            พลอากาศเอก/พล.อ.อ.
                        </option>                             
                    </optgroup>
                </select>
            </div>
        </div>
      </div>
        <div class="col-md-6">
            <div class="card-block">
                <div class="card-body" id="noncomm_and_comm">
                    สัญชาติ :
                    <select class="select2 form-control" style="width: 100%;">
                        <optgroup
                            label="สัญญาบัตร">
                            <option
                                value="AK">
                                เลือก
                            </option>
                            <option
                                value="HI">
                                พลอากาศเอก/พล.อ.อ.
                            </option>
                            <option
                                value="HI">
                                พลอากาศโท/พล.อ.ท.
                            </option>

                        </optgroup>
                    </select>
                </div>
            </div>
        </div>
     </div>
    <!---///////////////////////////////////////////////////////////////////--->
    <div class="row">
      <div class="col-md-12">
        <div class="card-body ">
            สถานภาพ :
            <div class="card-content">
                <div class="card-body">
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input
                            type="radio"
                            class="custom-control-input"
                            name="colorRadio"
                            id="radio20">
                        <label
                            class="custom-control-label"
                            for="radio20">สมรส</label>
                    </div>
                    <div
                        class="d-inline-block custom-control custom-radio mr-1">
                        <input
                            type="radio"
                            class="custom-control-input"
                            name="colorRadio"
                            id="radio21"
                            checked>
                        <label
                            class="custom-control-label"
                            for="radio21"
                            checked>หย่า</label>
                    </div>
                    <div
                        class="d-inline-block custom-control custom-radio mr-1">
                        <input
                            type="radio"
                            class="custom-control-input"
                            name="colorRadio"
                            id="radio22"
                            checked>
                        <label
                            class="custom-control-label"
                            for="radio22"
                            checked>เสียชีวิต</label>
                    </div>
                    <div
                        class="d-inline-block custom-control custom-radio mr-1">
                        <input
                            type="radio"
                            class="custom-control-input"
                            name="colorRadio"
                            id="radio23"
                            checked>
                        <label
                            class="custom-control-label"
                            for="radio23"
                            checked>สูญหาย</label>
                    </div>
                    <div
                        class="d-inline-block custom-control custom-radio mr-1">
                        <input
                            type="radio"
                            class="custom-control-input"
                            name="colorRadio"
                            id="radio24"
                            checked>
                        <label
                            class="custom-control-label"
                            for="radio24"
                            checked>จดทะเบียนรับรองบุตร</label>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
      <!---///////////////////////////////////////////////////////////////////-->
      <div class="row">               
            <div
                class="col-md-6">
                <div class="card-body"
                    id="File_number">
                    หลักฐานการสมรส :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-body"
                    id="File_number">
                    เลขที่ :
                    
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                </div>
            </div>
        </div>
    <!---///////////////////////////////////////////////////////////////////--->
    <div class="row">                
        <div class="col-md-6">
            <div class="card-block">
                <div class="input-group datep">
                    ลงวันที่ :
                    <input type="text" class="form-control pickadate-translations"   placeholder="" id="startdate" name="startdate" data-value="<?php echo GetToday('');?>" />
                    <div class="input-group-append">
                            <span class="input-group-text">
                            <span class="la la-calendar-o"></span>
                            </span>
                    </div>
                </div>
            </div>
        </div>
       
        <div class="col-md-3">
                <div class="card-block">
                    <div class="card-body" id="noncomm_and_comm">
                        จังหวัด :
                        <select class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>                                  
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="col-md-3">
                <div class="card-block">
                    <div class="card-body" id="noncomm_and_comm">
                        อำเภอ :
                        <select class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>                                  
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>
        </div>
            <!---///////////////////////////////////////////////////////////////////--->
        <div class="row">               
        <div class="col-md-6">
                <div class="card-body"
                    id="File_number">
                    แนบไฟล์หลักฐาน
                    
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                </div>
            </div>
            
  
    </div>
           
 <!---///////////////////////////////////////////////////////////////////-->
</div>
</div>
<br>
 <!---///////////////////////////////////////////////////////////////////-->
    <!-- ----ส่วนที่1------ -->
 <!---///////////////////////////////////////////////////////////////////-->
 
    <div
        class="card collapse-icon accordion-icon-rotate active">
        <div id="headingCollapse39"
            class="card-header bg-success">
            <a data-toggle="collapse"
                href="#collapse39"
                aria-expanded="true"
                aria-controls="collapse39"
                class="card-title lead white">
                <h6><U>ส่วนที่ 2</U>
                    ข้อมูลมารดา
                </h6>
            </a>
        </div>
        <div id="collapse39"
            role="tabpanel"
            aria-labelledby="headingCollapse39"
            class="card-collapse collapse show"
            aria-expanded="true">
            <div class="card-content">
               <div class="row">          
            <div class="col-md-6">
                <div class="card-body"
                    id="File_number">
                    หมายเลขประจำตัวราชการ
                    :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="card-block">
                    <div class="card-body"
                        id="noncomm_and_comm">
                        ยศ :
                        <select
                            class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศโท/พล.อ.ท.
                                </option> 
                            </optgroup>                
                        </select>
                        <a href="../Organizations/create.php"><i class="la la-plus-circle" style="font-size:36px;color:#0f1733;"></i></a>
                    </div>
                 </div>
             </div>
         </div>
     <!---///////////////////////////////////////////////////////////////////--->
    <div class="row">   
         <div class="col-md-6">
                <div class="card-body" id="File_number">
                    เลขประจำตัวประชาชน
                    :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                </div>
        </div>
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body" id="noncomm_and_comm">
                        คำนำหน้าชื่อ :
                        <select class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>                                  
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>
        </div>
         <!---///////////////////////////////////////////////////////////////////--->
        <div class="row">               
            <div
                class="col-md-6">
                <div class="card-body"
                    id="File_number">
                    ชื่อ :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-body"
                    id="File_number">
                    นามสกุล
                    :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                </div>
            </div>
        </div>
    <!---///////////////////////////////////////////////////////////////////--->
            <!---///////////////////////////////////////////////////////////////////--->
            <div class="row">               
            <div
                class="col-md-6">
                <div class="card-body"
                    id="File_number">
                    ชื่อใหม่ :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-body"
                    id="File_number">
                    นามสกุลใหม่
                    :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                </div>
            </div>
        </div>
    <!---///////////////////////////////////////////////////////////////////--->
    
  <div class="row">
    <div class="col-md-6">
        <div class="card-block">
            <div class="card-body" id="noncomm_and_comm">
                เชื้อชาติ :
                <select  class="select2 form-control" style="width: 100%;">
                    <optgroup
                        label="สัญญาบัตร">
                        <option
                            value="AK">
                            เลือก
                        </option>
                        <option
                            value="HI">
                            พลอากาศเอก/พล.อ.อ.
                        </option>                             
                    </optgroup>
                </select>
            </div>
        </div>
      </div>
        <div class="col-md-6">
            <div class="card-block">
                <div class="card-body" id="noncomm_and_comm">
                    สัญชาติ :
                    <select class="select2 form-control" style="width: 100%;">
                        <optgroup
                            label="สัญญาบัตร">
                            <option
                                value="AK">
                                เลือก
                            </option>
                            <option
                                value="HI">
                                พลอากาศเอก/พล.อ.อ.
                            </option>
                            <option
                                value="HI">
                                พลอากาศโท/พล.อ.ท.
                            </option>

                        </optgroup>
                    </select>
                </div>
            </div>
        </div>
     </div>
    <!---///////////////////////////////////////////////////////////////////--->
    <div class="row">
      <div class="col-md-12">
        <div class="card-body ">
            สถานภาพ :
            <div class="card-content">
                <div class="card-body">
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input
                            type="radio"
                            class="custom-control-input"
                            name="colorRadio"
                            id="radio20">
                        <label
                            class="custom-control-label"
                            for="radio20">สมรส</label>
                    </div>
                    <div
                        class="d-inline-block custom-control custom-radio mr-1">
                        <input
                            type="radio"
                            class="custom-control-input"
                            name="colorRadio"
                            id="radio21"
                            checked>
                        <label
                            class="custom-control-label"
                            for="radio21"
                            checked>หย่า</label>
                    </div>
                    <div
                        class="d-inline-block custom-control custom-radio mr-1">
                        <input
                            type="radio"
                            class="custom-control-input"
                            name="colorRadio"
                            id="radio22"
                            checked>
                        <label
                            class="custom-control-label"
                            for="radio22"
                            checked>เสียชีวิต</label>
                    </div>
                    <div
                        class="d-inline-block custom-control custom-radio mr-1">
                        <input
                            type="radio"
                            class="custom-control-input"
                            name="colorRadio"
                            id="radio23"
                            checked>
                        <label
                            class="custom-control-label"
                            for="radio23"
                            checked>สูญหาย</label>
                    </div>
                    <div
                        class="d-inline-block custom-control custom-radio mr-1">
                        <input
                            type="radio"
                            class="custom-control-input"
                            name="colorRadio"
                            id="radio24"
                            checked>
                        <label
                            class="custom-control-label"
                            for="radio24"
                            checked>จดทะเบียนรับรองบุตร</label>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
      <!---///////////////////////////////////////////////////////////////////-->
      <div class="row">               
            <div
                class="col-md-6">
                <div class="card-body"
                    id="File_number">
                    หลักฐานการสมรส :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-body"
                    id="File_number">
                    เลขที่ :
                    
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                </div>
            </div>
        </div>
    <!---///////////////////////////////////////////////////////////////////--->
    <div class="row">                
        <div class="col-md-6">
            <div class="card-block">
                <div class="input-group datep">
                    ลงวันที่ :
                    <input type="text" class="form-control pickadate-translations"   placeholder="" id="startdate" name="startdate" data-value="<?php echo GetToday('');?>" />
                    <div class="input-group-append">
                            <span class="input-group-text">
                            <span class="la la-calendar-o"></span>
                            </span>
                    </div>
                </div>
            </div>
        </div>
       
        <div class="col-md-3">
                <div class="card-block">
                    <div class="card-body" id="noncomm_and_comm">
                        จังหวัด :
                        <select class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>                                  
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="col-md-3">
                <div class="card-block">
                    <div class="card-body" id="noncomm_and_comm">
                        อำเภอ :
                        <select class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>                                  
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>
        </div>
            <!---///////////////////////////////////////////////////////////////////--->
        <div class="row">               
        <div class="col-md-6">
                <div class="card-body"
                    id="File_number">
                    แนบไฟล์หลักฐาน
                    
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                </div>
            </div>
            
  
    </div>
           
 <!---///////////////////////////////////////////////////////////////////-->
</div>
</div>
<br>
 <!---/////////////////////////////--ฬ


    <!-- ----ส่วนที่2--- -->
 <!---///////////////////////////////////////////////////////////////////--->

    <div
        class="card collapse-icon accordion-icon-rotate active">
        <div id="headingCollapse40"
            class="card-header bg-success">
            <a data-toggle="collapse"
                href="#collapse40"
                aria-expanded="true"
                aria-controls="collapse40"
                class="card-title lead white">
                <h6><U>ส่วนที่ 3</U>
                    ข้อมูลปู่

                </h6>
            </a>
        </div>
        <div id="collapse40"
            role="tabpanel"
            aria-labelledby="headingCollapse40"
            class="card-collapse collapse show"
            aria-expanded="true">
            <div class="card-content">
                  <!---///////////////////////////////////////////////////////////////////--->
         <div class="row">          
            <div class="col-md-6">
                <div class="card-body"
                    id="File_number">
                    หมายเลขประจำตัวราชการ
                    :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="card-block">
                    <div class="card-body"
                        id="noncomm_and_comm">
                        ยศ :
                        <select
                            class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศโท/พล.อ.ท.
                                </option> 
                            </optgroup>                
                        </select>
                        <a href="../Organizations/create.php"><i class="la la-plus-circle" style="font-size:36px;color:#0f1733;"></i></a>
                    </div>
                 </div>
             </div>
         </div>
     <!---///////////////////////////////////////////////////////////////////--->
    <div class="row">   
   
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body" id="noncomm_and_comm">
                        คำนำหน้าชื่อ :
                        <select class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>                                  
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>
        </div>
         <!---///////////////////////////////////////////////////////////////////--->
        <div class="row">               
            <div
                class="col-md-6">
                <div class="card-body"
                    id="File_number">
                    ชื่อ :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-body"
                    id="File_number">
                    นามสกุล
                    :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                </div>
            </div>
        </div>
         <!---///////////////////////////////////////////////////////////////////--->
        </div>
    </div>
</div>
        
    
    <br>
 <!---///////////////////////////////////////////////////////////////////--->
    <!-- -------ส่วนที่3 -->
 <!---///////////////////////////////////////////////////////////////////--->
<div
class="card collapse-icon accordion-icon-rotate active">
<div id="headingCollapse41"
class="card-header bg-success">
<a data-toggle="collapse"
href="#collapse41"
aria-expanded="true"
aria-controls="collapse41"
class="card-title lead white">
<h6><U>ส่วนที่ 4</U>
ข้อมูลย่า

</h6>
</a>
</div>
<div id="collapse41"
role="tabpanel"
aria-labelledby="headingCollapse41"
class="card-collapse collapse show"
aria-expanded="true">
<div class="card-content">
    <!---///////////////////////////////////////////////////////////////////--->
       <div class="row">          
            <div class="col-md-6">
                <div class="card-body"
                    id="File_number">
                    หมายเลขประจำตัวราชการ
                    :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="card-block">
                    <div class="card-body"
                        id="noncomm_and_comm">
                        ยศ :
                        <select
                            class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศโท/พล.อ.ท.
                                </option> 
                            </optgroup>                
                        </select>
                        <a href="../Organizations/create.php"><i class="la la-plus-circle" style="font-size:36px;color:#0f1733;"></i></a>
                    </div>
                 </div>
             </div>
         </div>
     <!---///////////////////////////////////////////////////////////////////--->
    <div class="row">   
         
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body" id="noncomm_and_comm">
                        คำนำหน้าชื่อ :
                        <select class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>                                  
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>
        </div>
         <!---///////////////////////////////////////////////////////////////////--->
        <div class="row">               
            <div
                class="col-md-6">
                <div class="card-body"
                    id="File_number">
                    ชื่อ :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-body"
                    id="File_number">
                    นามสกุล
                    :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                </div>
            </div>
        </div>
        <!---///////////////////////////////////////////////////////////////////--->
        </div>
    </div>
</div>

    <br>
<!---///////////////////////////////////////////////////////////////////--->
    <!-- -------ส่วนที่4 -->
<!---///////////////////////////////////////////////////////////////////--->
    <div
        class="card collapse-icon accordion-icon-rotate active">
        <div id="headingCollapse42"
            class="card-header bg-success">
            <a data-toggle="collapse"
                href="#collapse42"
                aria-expanded="true"
                aria-controls="collapse42"
                class="card-title lead white">
                <h6><U>ส่วนที่ 4</U>
                    ข้อมูลตา

                </h6>
            </a>
        </div>
        <div id="collapse42"
            role="tabpanel"
            aria-labelledby="headingCollapse42"
            class="card-collapse collapse show"
            aria-expanded="true">
            <div class="card-content">
    <!---///////////////////////////////////////////////////////////////////--->
         <div class="row">          
            <div class="col-md-6">
                <div class="card-body"
                    id="File_number">
                    หมายเลขประจำตัวราชการ
                    :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="card-block">
                    <div class="card-body"
                        id="noncomm_and_comm">
                        ยศ :
                        <select
                            class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศโท/พล.อ.ท.
                                </option> 
                            </optgroup>                
                        </select>
                        <a href="../Organizations/create.php"><i class="la la-plus-circle" style="font-size:36px;color:#0f1733;"></i></a>
                    </div>
                 </div>
             </div>
         </div>
     <!---///////////////////////////////////////////////////////////////////--->
    <div class="row">   
       
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body" id="noncomm_and_comm">
                        คำนำหน้าชื่อ :
                        <select class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>                                  
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>
        </div>
 <!---///////////////////////////////////////////////////////////////////--->
        <div class="row">               
            <div
                class="col-md-6">
                <div class="card-body"
                    id="File_number">
                    ชื่อ :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-body"
                    id="File_number">
                    นามสกุล
                    :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                </div>
            </div>
        </div>
        <!---///////////////////////////////////////////////////////////////////--->
        </div>
    </div>
</div>
     
    <br>
<!---///////////////////////////////////////////////////////////////////--->
    <!-- -------ส่วนที่ 5 -->
<!---///////////////////////////////////////////////////////////////////--->


    <div
        class="card collapse-icon accordion-icon-rotate active">
        <div id="headingCollapse43"
            class="card-header bg-success">
            <a data-toggle="collapse"
                href="#collapse43"
                aria-expanded="true"
                aria-controls="collapse43"
                class="card-title lead white">
                <h6><U>ส่วนที่ 4</U>
                    ข้อมูลยาย

                </h6>
            </a>
        </div>
        <div id="collapse43"
            role="tabpanel"
            aria-labelledby="headingCollapse43"
            class="card-collapse collapse show"
            aria-expanded="true">
         <div class="card-content">
    <!---///////////////////////////////////////////////////////////////////--->
    <div class="row">          
            <div class="col-md-6">
                <div class="card-body"
                    id="File_number">
                    หมายเลขประจำตัวราชการ
                    :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="card-block">
                    <div class="card-body"
                        id="noncomm_and_comm">
                        ยศ :
                        <select
                            class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศโท/พล.อ.ท.
                                </option> 
                            </optgroup>                
                        </select>
                        <a href="../Organizations/create.php"><i class="la la-plus-circle" style="font-size:36px;color:#0f1733;"></i></a>
                    </div>
                 </div>
             </div>
         </div>
     <!---///////////////////////////////////////////////////////////////////--->
    <div class="row">   
       
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body" id="noncomm_and_comm">
                        คำนำหน้าชื่อ :
                        <select class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>                                  
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>
        </div>
         <!---///////////////////////////////////////////////////////////////////--->
        <div class="row">               
            <div
                class="col-md-6">
                <div class="card-body"
                    id="File_number">
                    ชื่อ :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-body"
                    id="File_number">
                    นามสกุล
                    :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder="">
                 </div>
            </div>
        </div>
    <!---///////////////////////////////////////////////////////////////////--->    
    </div>
</div>
</div>

    <br>
    <!-- -------ส่วนที่ 6 -->
    <div class="tab-content px-1 pt-1">
     <div class="form-actions center text-center" >
            <button type="button"
                class="btn btn-success  round btn-min-width mr-1 mb-1"
                id="submit"
                name="submit"
                data-target="#modalConfirm"
                onclick="insertOrganizationGroupType()"><i class="fa fa-save"></i> &nbsp;บันทึก </button>
            <button type="button"
                class="btn btn-danger  round btn-min-width mr-1 mb-1"
                id="type-error"><i class="fa fa-times-circle-o"></i> &nbsp;ยกเลิก</button>
        </div>
    </div>


</div>
</div>
</div>


</div>
</div>
</div>
