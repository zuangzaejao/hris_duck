<?php 
   require_once '../../config.php';
   require_once '../../Model/Ducklab/duck.class.php'; 
   require_once '../../Model/Ducklab/contents.class.php'; 
   require_once '../../Model/Ducklab/org.class.php'; 
   require_once '../../Model/Ducklab/func.php'; 
   require_once '../include/header.php'; 
 
 
     
$menu1 ="ORGSTRUC" ;
$menu2 ="ORGSTRUCPOSITION";
$menu3 ="POSITIONSETUP"; 


$clsorg = new OrgClass();

$OrgLevelDID =  $_REQUEST['OrgLevelDID'];
$OrgType =  $_REQUEST['OrgType'];
$OrgLevel2 =  $_REQUEST['OrgLevel2'];

 
$orglevelx = substr($OrgLevelDID,1,1) ;


$tbname = 'OrgLevel'.$orglevelx ; 
$orgdesc  = $clsorg->LoadOnce( $tbname,array('OrgLevDID'=>$OrgLevelDID)) ;
 

$Org2  = $clsorg->LoadOnce( 'OrgLevel2',array('OrgLevDID'=>$OrgLevel2)) ;

//$lastid = $clsorg->LoadFieldsMaxID('HrtPosition','HrtPositionID' , ''); 

// $lastid = $clsorg->LoadIdenCurrent('HrtPosition');
// $newid = $lastid['lastid']+1 ;


$positionID = $_REQUEST['position'] ; 
$position  = $clsorg->LoadOnce( 'HrtPosition',array('HrtPositionID'=>$positionID)) ;
$pos  = $clsorg->LoadOnce( 'HrtPos',array('HrtPosCode'=>$position['HrtPosCode'])) ;
//$OrgLevel2 

 ?>
  <?php include '../include/menu.php'; ?>
   
  <?php include_once '../include/modelOnload.php' ?>


  <div class="app-content content">
      <div class="content-wrapper">
          <div class="content-header row">
              <div class="content-header-left col-md-6 col-12 mb-2">
                  <h3 class="content-header-title">บันทึกกรอบอัตราตำแหน่ง </h3>
                  <div class="row breadcrumbs-top">

                  </div>
              </div>

          </div>
          <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="./index.php">ระบบงานโครงสร้างอัตรากำลังพล</a></li>
                  <li class="breadcrumb-item"><a href="./detail.php?OrgLevel2=<?php echo $OrgLevel2 ;?>&OrgTypeID=<?php echo $OrgType;?>&PersonTypeID={$PersonTypeID}">ตำแหน่ง</a></li>
                 <li class="breadcrumb-item active" aria-current="page"> เพิ่มกรอบอัตราตำแหน่ง  </li>
              </ol>
          </nav>
          <div class="content-body">
              <?php  //echo "<pre>".print_r($position ,true)."</pre>";?>  
              <?php  //echo "<pre>".print_r($pos ,true)."</pre>";?>  
              <!-- Basic form layout section start -->
              <section id="horizontal-form-layouts">

                  <div class="row">
                      <div class="col-md-12">

                      
                            <div class="card">
                                 <div class="card-header cardh1"  >
                                    <h5 class="card-title " >ส่วนที่ 1 รายละเอียดตำแหน่ง </h5>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <?php 
                                    //echo "OrgType: " .$OrgType;
                                 //  echo "OrgLevelDID: " .$OrgLevelDID ."::". $orglevelx; 
                                 // echo "<pre>".print_r($lastid ,true)."</pre>";
                                  ?>
                                    <input type="hidden" class="form-control inp_orgName" id="inp_orgName">
                                    <input type="hidden" class="form-control inp_orgNameAbbr" id="inp_orgNameAbbr">
                                    <input type="hidden" class="form-control inp_orgNameAbbrSemi" id="inp_orgNameAbbrSemi">

                                <div class="card-content collpase show">
                                    <div class="card-body  my-2 mx-2">
                                            <div class="row"> 
                                                <div class="col-md-6">
                                                    <div class="form-group"> 
                                                        <div class="">
                                                            <label for="PositionCode">  รหัสอัตราตำแหน่ง </label>
                                                        </div> 
                                                        <input  type="text" id="PositionCode" name="PositionCode" value="<?php echo $positionID;?>"  class="form-control col-4" disabled>  
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group"> 
                                                        <div class="">
                                                            <label for="OrgTypeID">  โครงสร้าง </label>
                                                        </div> 
                                                        <select id="OrgTypeID" name="OrgTypeID"   class="form-control select2 w-80" disabled>
                                                        </select>
                                                    </div>
                                                </div>
                                               
                                            </div> 
                                            <div class="row">  
                                                
                                                <div class="col-md-6">
                                                    <div class="form-group"> 
                                                        <div class="">
                                                            <label for="OrgTypeID">  สังกัด </label>
                                                        </div> 
                                                         <input  type="text" id="" name="" value="<?php echo $Org2['OrgLevAbbrName']?>"  class="form-control" disabled> 
                                                    </div>
                                                </div>  
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="">
                                                            <label for="OrgLevName"> หน่วยงาน </label>
                                                        </div>   
                                                        <input type="text" id="OrgLevName" name="OrgLevName" value=""  class="form-control inp_orgNameAbbr" disabled> 

                                                        <input type="hidden" id="OrgLevelDID" name="OrgLevelDID" value="<?php echo $OrgLevelDID;?>"  class="form-control"  > 
                                                        
                                                    </div>
                                                </div>

                                            </div> 
                                            <div class="row">   
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="">
                                                            <label for="HrtPersonTypeID"> ประเภทกำลังพล </label>
                                                        </div>  
                                                        <select id="HrtPersonTypeID" name="HrtPersonTypeID" class="form-control select2 w-80">
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                            </div>

                                            <div class="row">    
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="">
                                                            <label for="PosID"> ชื่อตำแหน่ง </label>
                                                        </div>  
                                                        <select id="PosID" name="PosID" class="form-control select2"> 
                                                        </select>
                                                        <input type="hidden" id="PosIDName" name="PosIDName" value="<?php echo $pos['HrtPosName'];?>">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="">
                                                            <label for="PosNameAbbr"> ชื่อย่อตำแหน่ง </label>
                                                        </div>   
                                                        <input type="text" class="form-control" id="PosNameAbbr" name="PosNameAbbr"  value="<?php echo $pos['HrtPosAbbrName']; ?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">    
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="">
                                                            <label for=""> ชื่อตำแหน่ง-สังกัด เต็ม </label>
                                                        </div>   
                                                        <input type="text" class="form-control" id="PosNameDept" name="PosNameDept" disabled>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div>
                                                            <label for=""> ชื่อตำแหน่ง-สังกัด ย่อ </label>
                                                        </div>   
                                                        <input type="text" class="form-control" id="PosNameDeptAbbr" name="PosNameDeptAbbr" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">    
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="">
                                                            <label for=""> ชื่อตำแหน่ง-สังกัด กึ่งย่อ </label>
                                                        </div>   
                                                        <input type="text" class="form-control" id="PosNameDeptAbbrSemi" name="PosNameDeptAbbrSemi" disabled>
                                                    </div>
                                                </div> 
                                            </div>

                                            <div class="row">   
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="">
                                                            <label for="HrtPosGrpID"> กลุ่มตำแหน่ง </label>
                                                        </div>  
                                                        <select id="HrtPosGrpID" name="HrtPosGrpID" class="form-control select2" disabled>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="">
                                                            <label for="HrtPosTypeID"> ประเภทตำแหน่ง </label>
                                                        </div>  
                                                        <select id="HrtPosTypeID" name="HrtPosTypeID" class="form-control select2" disabled>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">   
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="">
                                                            <label for="RankFixID"> เงินเดือนอัตรา </label>
                                                        </div>  
                                                        <select id="RankFixID" name="RankFixID" class="form-control select2">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <div class="">
                                                            <label for="PositionNum"> จำนวน </label>
                                                        </div>  
                                                        <input type="number" id="PositionNum" name="PositionNum" class="form-control" value="<?php echo $position['HrtPosNum'];?>">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <div class="">
                                                            <label for="HrtPosPrintSeq"> ลำดับการจัดเรียง </label>
                                                        </div>  
                                                        <input type="number" id="HrtPosPrintSeq" name="HrtPosPrintSeq" class="form-control text-right" value="<?php echo $position['HrtPosPrintSeq'];?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">   
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="">
                                                            <label for="HrtChiefSpc1"> สายวิทยาการ 1 </label>
                                                        </div>  
                                                        <select id="HrtChiefSpc1" name="HrtChiefSpc1" class="form-control select2">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="">
                                                            <label for="HrtChiefSpc2"> สายวิทยาการ 2 </label>
                                                        </div>  
                                                        <select id="HrtChiefSpc2" name="HrtChiefSpc2" class="form-control select2">
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            
                                            
                                            
                                           

                                            <hr class="" style="border: 1px solid #3f51b536;">
 
                                            <br> 
                                            <div class="row">
                                                <div class="col-md-12"> 
                                                    <label class="col-md-1 label-control" for="HrtPositionActive" style="padding-right:0px;">สถานะ</label>
                                                    <input type="checkbox" id="HrtPositionActive"  name="HrtPositionActive"  checked data-toggle="toggle" data-style="ios" data-on="ใช้งาน" data-off="ยกเลิก" data-onstyle="success" data-offstyle="danger" data-size="sm">
                                                </div>
                                            </div>

                                           
                                           
                                          </div>

                                      </form>
                                  </div>
                              </div>
 
                            <!-- ///////////////////////////////////////////////////////////////////////// -->
                             
                            <div class="card">
                                 <div class="card-header cardh1"  >
                                    <h5 class="card-title " >ส่วนที่ 2 รายละเอียดการใช้งาน </h5>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <div class="card-content collpase show">
                                    <div class="card-body  my-2 mx-2">
                                        <div class="row">

                                            <div class="col-12 col-xl-6 border-right-blue-grey border-right-lighten-4 pr-2  pl-2 p-0">
                                                <div class="row my-2">
                                                    <div class="col-4">
                                                        <h5 class="text-bold-500 mb-0">ใช้งาน</h5>
                                                    </div>
                                                </div>
                                                <form class="form form-horizontal">
                                                    <div class="form-body">
                                                        
                                                        <div class="form-group row">
                                                            <div class="col-md-12">
                                                                <label class="col-md-12" for="CommandActiveC">คำสั่ง</label>
                                                                <div class="col-md-12" style="float:left;">
                                                                    <select class="select2 form-control block" id="CommandActiveC" name="CommandActiveC"  style="width: 80%;"> 
                                                                    </select>
                                                                </div> 
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-md-12">
                                                                <label class="col-md-12" for="CommandActiveCodeC">เลขที่คำสั่ง</label>
                                                                <div class="col-md-12">
                                                                    <div class="position-relative">
                                                                        <input type="text" id="CommandActiveCodeC" class="form-control "  name="CommandActiveCodeC" value="<?php echo $position['CommDocNo'];?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div> 

                                                        <div class="form-group row">
                                                            <div class="col-md-12">
                                                                <label class="col-md-12 label-control text-left" for="CommandActiveDateC"> วันที่ลงคำสั่ง : </label>
                                                                <div class="input-group datep">
                                                                    <input type="text" class="form-control form2 pickadate-translations" placeholder="" id="CommandActiveDateC" name="CommandActiveDateC" data-value="<?php echo $position['CommDocDate'];?>" />
                                                                    <div class="input-group-append">
                                                                        <span class="input-group-text"> <span class="la la-calendar-o"></span> </span>
                                                                    </div>
                                                                </div>
                                                            </div> 
                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-md-12">
                                                                <label class="col-md-12 label-control text-left" for="CommandActiveEFFDateC">มีผลตั้งแต่ :</label>
                                                                <div class="input-group datep">
                                                                    <input type="text" class="form-control form2 pickadate-translations" placeholder="" id="CommandActiveEFFDateC" name="CommandActiveEFFDateC" data-value="<?php echo $position['CommEffDate'];?>" />
                                                                    <div class="input-group-append">
                                                                        <span class="input-group-text"> <span class="la la-calendar-o"></span> </span>
                                                                    </div>
                                                                </div>
                                                            </div> 
                                                        </div> 
                                                    </div>
                                                </form>
                                            </div>

                                            <!-- 2 -->
                                            <div class="col-12 col-xl-6 pl-2 p-0">
                                                <div class="row my-2">
                                                    <div class="col-4">
                                                        <h5 class="text-bold-500 mb-0">ยกเลิก</h5>
                                                    </div>
                                                </div>
                                                <form class="form form-horizontal">
                                                    <div class="form-body">
                                                        <div class="form-group row">
                                                            <div class="col-md-12">
                                                                <label class="col-md-12" for="CommandCancleC">คำสั่ง</label>
                                                                <div class="col-md-12" style="float:left;">
                                                                    <select class="select2 form-control block" id="CommandCancleC" class="CommandCancleC" style="width: 80%;">  
                                                                    </select>
                                                                </div> 
                                                            </div>
                                                        </div> 

                                                        <div class="form-group row">
                                                            <div class="col-md-12">
                                                                <label class="col-md-12" for="CommandCancleCodeC">เลขที่คำสั่ง</label>
                                                                <div class="col-md-12">
                                                                    <div class="position-relative">
                                                                        <input type="text" id="CommandCancleCodeC" class="form-control " class="CommandCancleCodeC" name="CommandCancleCodeC">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-md-12">
                                                                <label class="col-md-12 label-control text-left" for="CommandCancleDateC"> วันที่ลงคำสั่ง : </label>
                                                                <div class="input-group datep">
                                                                    <input type="text" class="form-control form2 pickadate-translations" placeholder="" id="CommandCancleDateC" name="CommandCancleDateC" data-value="<?php echo $position['CancleCommDocDate'];?>" />
                                                                    <div class="input-group-append">
                                                                        <span class="input-group-text"> <span class="la la-calendar-o"></span> </span>
                                                                    </div>
                                                                </div>
                                                            </div> 
                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-md-12">
                                                                <label class="col-md-12 label-control text-left" for="CommandCancleEFFDateC">มีผลตั้งแต่ :</label>
                                                                <div class="input-group datep">
                                                                    <input type="text" class="form-control form2 pickadate-translations" placeholder="" id="CommandCancleEFFDateC" name="CommandCancleEFFDateC" data-value="<?php echo $position['CancleCommEffDate'];?>" />
                                                                    <div class="input-group-append">
                                                                        <span class="input-group-text"> <span class="la la-calendar-o"></span> </span>
                                                                    </div>
                                                                </div>
                                                            </div> 
                                                        </div> 

                                                    </div>
                                                </form>
                                            </div> 
                                        </div> 
                                    </div> 
                                </div> 
                            </div> 
                                       
                            <!-- /////////////////////////////////////////////////////////// -->
                                <div class="form-actions text-center mt-2"> 
                                    <input type="hidden"  class="form-control border-primary"  name="PositionID" id="PositionID" value="<?php echo $positionID;?>">

                                    <input type="hidden"  class="form-control border-primary"  name="HrtPosGrpID" id="HrtPosGrpID" value="<?php echo $OrgLevel2;?>">
                                    <input type="hidden"  class="form-control border-primary"  name="HrtPosTypeID" id="HrtPosTypeID" value="<?php echo $OrgLevel2;?>">
                               
                                    <input type="hidden"  class="form-control border-primary"  name="OrgLevel2" id="OrgLevel2" value="<?php echo $OrgLevel2;?>">
                                    <input type="hidden"  class="form-control border-primary"  name="HrtPositionUpdateBy" id="HrtPositionUpdateBy" value="1">
                                    <input type="hidden"  class="form-control border-primary"  name="HrtPositionUpdateDate" id="HrtPositionUpdateDate" value="<?php echo GetToday('');?>">
                                    <button type="button" class="btn btn-danger  round btn-min-width mr-1 mb-1"  >ยกเลิก</button>
                                    <button type="button" class="btn btn-success  round btn-min-width mr-1 mb-1" id="btneditcf" >บันทึก</button> 
                                </div>
                            <!-- /////////////////////////////////////////////////////////// -->

                          </div>
                      </div>
                  </div>
                   
              </section>
              <!-- // Basic form layout section end -->
          </div>
      </div>
  </div>
  <?php include '../include/footer.php'; ?>
  
   
    <script type="text/javascript">
   
    $( document ).ready(function() {
         
        org.LoadOrgTypeSel('<?php echo $position['OrgTypeID'];?>','OrgTypeID');// 
        org.LoadPersonTypeSel('<?php echo $pos['HrtPersonTypeID'];?>','HrtPersonTypeID');

        org.LoadRankFix('<?php echo $position['HrtRankID'];?>','RankFixID','<?php echo $pos['HrtPersonTypeID'];?>');
        // org.LoadRankSortSel('','RankFixID');
        org.LoadDataOrgParent('<?php echo $OrgLevelDID;?>');
        
        // settimeout(  setOrgPos , 1500 );
        setTimeout(function(){ setOrgPos() }, 1000);



         
        org.LoadPosCodeSel('<?php echo $position['HrtPosCode'];?>','PosID','','<?php echo $pos['HrtPersonTypeID'];?>');
        org.LoadChiefSpcSel('<?php echo $position['HrtChiefSpcID1'];?>','HrtChiefSpc1','<?php echo $pos['HrtPersonTypeID'];?>');
        org.LoadChiefSpcSel('<?php echo $position['HrtChiefSpcID2'];?>','HrtChiefSpc2', '<?php echo $pos['HrtPersonTypeID'];?>');
        org.LoadPosTypeGroupSel('<?php echo $position['HrtPosGrpID'];?>','HrtPosGrpID',<?php echo $pos['HrtPersonTypeID'];?>);
        org.LoadPosTypeSel('<?php echo $position['HrtPosTypeID'];?>','HrtPosTypeID', <?php echo $pos['HrtPosGrpID'];?>);

        $("#HrtPersonTypeID").change(function(){ 
                // alert('<?php echo $position['HrtPosCode'];?>') ;
                org.LoadPosCodeSel('<?php echo $position['HrtPosCode'];?>','PosID','',$("#HrtPersonTypeID").val());
                org.LoadChiefSpcSel('<?php echo $position['HrtChiefSpcID1'];?>','HrtChiefSpc1', $("#HrtPersonTypeID").val());
                org.LoadChiefSpcSel('<?php echo $position['HrtChiefSpcID2'];?>','HrtChiefSpc2', $("#HrtPersonTypeID").val());

               // org.LoadRankSortSel('','RankFixID',$("#HrtPersonTypeID").val());
                org.LoadRankFix('<?php echo $position['HrtRankID'];?>','RankFixID',$("#HrtPersonTypeID").val());
        });

        $("#PosID").change(function(){ 
            console.log($(this).find(':selected').data("pname"));
          $("#PosIDName").val($(this).find(':selected').data("pname"));
          $("#PosNameAbbr").val($(this).find(':selected').data("abbrname"));

          org.LoadPosTypeGroupSel($(this).find(':selected').data("grpid"),'HrtPosGrpID',$("#HrtPersonTypeID").val());
          org.LoadPosTypeSel($(this).find(':selected').data("postype"),'HrtPosTypeID', $("#HrtPosGrpID").val());
          
          var PosIDName = $('#PosIDName').val();
          var PosNameAbbr = $('#PosNameAbbr').val();
          var inp_orgName = $('#inp_orgName').val();
          var inp_orgNameAbbr = $('#inp_orgNameAbbr').val();
          var inp_orgNameAbbrSemi = $('#inp_orgNameAbbrSemi').val();

          var  PosNameDept = PosIDName +" " +inp_orgName ;
          var  PosNameDeptAbbr = PosNameAbbr +" " +inp_orgNameAbbr ;
          var  PosNameDeptAbbrSemi = PosIDName +" " +inp_orgNameAbbrSemi ;


          $('#PosNameDept').val(PosNameDept);
          $('#PosNameDeptAbbr').val(PosNameDeptAbbr);
          $('#PosNameDeptAbbrSemi').val(PosNameDeptAbbrSemi);

        });
       
 
        $("#btneditcf").click(function(){   
            $('#modal_editcf').modal('show');
         //   console.log($("input[name=CommandCancleDateC_submit]").val());
        });
        $("#btnedit").click(function(){
            org.EditPosition();
        });

        org.LoadSourceofCommSel('<?php echo $position['SourceOfCommID'];?>','CommandActiveC');
        org.LoadSourceofCommSel('<?php echo $position['CancleSourceOfCommID'];?>','CommandCancleC');

       
    });

    function setOrgPos(){


        var PosIDName = '<?php echo $pos['HrtPosName'];?>';
        var PosNameAbbr = '<?php echo $pos['HrtPosAbbrName'];?>';
        var PosIDName = '<?php echo $pos['HrtPosName'];?>';
  
        var inp_orgName = $('#inp_orgName').val();
        var inp_orgNameAbbr = $('#inp_orgNameAbbr').val();
        var inp_orgNameAbbrSemi = $('#inp_orgNameAbbrSemi').val();

        var  PosNameDept = PosIDName +" " +inp_orgName ;
        var  PosNameDeptAbbr = PosNameAbbr +" " +inp_orgNameAbbr ;
        var  PosNameDeptAbbrSemi = PosIDName +" " +inp_orgNameAbbrSemi ;


        $('#PosNameDept').val(PosNameDept);
        $('#PosNameDeptAbbr').val(PosNameDeptAbbr);
        $('#PosNameDeptAbbrSemi').val(PosNameDeptAbbrSemi);
    }
 
    </script>