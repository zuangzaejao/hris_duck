 
<?php 

require_once '../../config.php';
require_once '../../Model/Ducklab/duck.class.php'; 
require_once '../../Model/Ducklab/contents.class.php'; 
require_once '../../Model/Ducklab/org.class.php'; 
require_once '../../Model/Ducklab/func.php'; 
require_once '../include/header.php'; 
 
  
$menu1 ="ORGSTRUC" ;
$menu2 ="ORGSTRUCPOSITION";
$menu3 ="POSITIONSETUP"; 
   

 $clsorg = new OrgClass();

 
include_once '../include/menu.php'; 
include_once '../include/modelOnload.php' ;
?>
<section>
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
                    <h3 class="content-header-title">บันทึกกรอบอัตราตำแหน่ง</h3>
                </div>
            </div>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="../index.php">ระบบงานโครงสร้างอัตรากำลังพล</a></li>
                  <li class="breadcrumb-item"><a href="./index.php">ตำแหน่ง</a></li>
                  <li class="breadcrumb-item active" aria-current="page">บันทึกกรอบอัตราตำแหน่ง</li>
              </ol>
          </nav>
          
            <div class="content-body">  
                <section id="bootstrap3">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <div class="col-12 d-flex align-items-center justify-content-center">
                                            <div class="col-lg-6 col-10 ">
                                                <div class="text-center">
                                                    <i class="la la-code-fork" style="font-size: 300px; color:#0f1733;"></i>
                                                </div>


                                                <fieldset class="form-group position-relative">
                                                    <div class="form-group col-md-12">
                                                        <div class="form-group">
                                                            <label for="OrgTypeID" class="fontset5 col-4 text-right">  โครงสร้าง : <span class="text-danger">*</span> </label>
                                                            <select id="OrgTypeID" name="OrgTypeID" class="form-control select2  col-7">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <div class="form-group">
                                                            <label for="OrgLevel2" class="fontset5 col-4 text-right">  สังกัด : <span class="text-danger">*</span> </label>
                                                            <select id="OrgLevel2" name="OrgLevel2" class="form-control select2  col-7">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group col-md-12">
                                                        <div class="form-group">
                                                            <label for="PersonTypeID" class="fontset5 col-4 text-right">  ประเภทกำลังพล : </label>
                                                            <select id="PersonTypeID" name="PersonTypeID" class="form-control select2 col-7">
                                                            </select>
                                                        </div>
                                                    </div>
                                                </fieldset>

                                                <div class="text-center">
                                                    <button type="button" class="btn btn_b1" id="btnSearch" ><i class="fa fa-search" ></i> ค้นหา </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Bootstrap 3 table -->
            </div>
        </div>
    </div>
</section>
 
</body>

</html>


 <!-- footer -->
 <?php include '../include/footer.php'; ?>
  
<script type="text/javascript">
    $(document).ready(function() {
        
        org.LoadOrgTypeSel('21','OrgTypeID'); 
        org.LoadOrg2SelByOrgTypeID('','21','OrgLevel2');  
        org.LoadPersonTypeSel('','PersonTypeID');

        $("#OrgTypeID").change(function(){  
             org.LoadOrg2SelByOrgTypeID('',$("#OrgTypeID").val(),'OrgLevel2'); //selfDID,OrgTypeID,element_id,tablename
        }); 
        $("#btnSearch").click(function(){
            if ( $('#OrgTypeID').val() != "" &&  $('#OrgLevel2').val() != ""){
                window.location = "detail.php?OrgLevel2="+$('#OrgLevel2').val()+"&OrgTypeID="+ $('#OrgTypeID').val()+"&PersonTypeID="+ $('#PersonTypeID').val() ;
            }else{
                return false ;
            }
        });
        
    }); 

 </script>
