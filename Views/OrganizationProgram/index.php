  <!-- header -->
  <?php require_once '../../config.php' ?>
  <?php require_once '../../Model/Ducklab/duck.class.php'; ?>
  <?php require_once '../../Model/Ducklab/contents.class.php'; ?>
  <?php require_once '../../Model/Ducklab/org.class.php'; ?>
  <?php require_once '../../Model/Ducklab/func.php'; ?>

  <?php require_once '../include/header.php'; ?>
  
  <?php
  
  $menu1 = "ORGSTRUC" ;
  $menu2 = "ORGSTRUCDEPT" ;
  $menu3 = "ORGSTRUCDEPTSETTING" ;
  $menu4 = "ORGANIZATIONSUBUNIT" ;
   

  $clsorg = new OrgClass();
  $data_orgtype =  $clsorg->Load('OrgType',array() ,'','');
 

  ?>
  <!-- menu -->
  <?php include '../include/menu.php'; ?>
  <?php include_once '../include/modelOnload.php' ?>

<section>
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="htab"></div>
                    <h3 class="content-header-title">โปรแกรมจัดการโครงสร้าง</h3>
                </div>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../index.php">ระบบงานโครงสร้างอัตรากำลังพล</a></li>
                    <li class="breadcrumb-item"><a href="./index.php"> โครงสร้าง</a></li>
                    <li class="breadcrumb-item active" aria-current="page">โปรแกรมจัดการโครงสร้าง</li>
                </ol>
            </nav>
            <div class="content-body">

            
                <!-- Bootstrap 3 table -->
                <section id="bootstrap3">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <div class="col-12 d-flex align-items-center justify-content-center">
                                            <div class="col-lg-6 col-10 ">
                                                <div class="text-center">
                                                    <i class="la la-code-fork" style="font-size: 300px; color:#0f1733;"></i>
                                                </div>
                                                <!-- 
                                                <fieldset class="form-group position-relative">
                                                    
                                                    <div class="form-group col-md-12">
                                                        <div class="form-group">
                                                            <label for="OrgTypeID" class="fontset5 col-4 text-right">  โครงสร้าง : <span class="text-danger">*</span> </label>
                                                            <select name="orgTypeList" id="orgTypeList" class="select2 form-control col-7">
                                                                <?php 
                                                                
                                                                foreach($data_orgtype as $k1 => $row){
                                                                    ?>
                                                                     <option value="<?php echo $row['OrgTypeID'];?>" <?php if($row['OrgTypeID']==21){ echo "selected"; } ?>> <?php echo $row['OrgTypeID'].":".$row['OrgTypeName'];?> </option>
                                                                
                                                                    <?php 
                                                                } 
                                                                ?> 
                                                            </select>
                                                        </div> 
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <div class="form-group">
                                                            <label for="OrgLevel1" class="fontset5 col-4 text-right">  หน่วยงาน : <span class="text-danger">*</span> </label>
                                                            <select id="OrgLevel1" name="OrgLevel1" class="select2 form-control col-7"></select>
                                                        </div>
                                                    </div> 
                                                     
                                                </fieldset>

                                                

                                                <div class="text-center">
                                                     <button type="button" class="btn btn_b1" id="btnsearchStruc" > <i class="fa fa-search" ></i> ค้นหา 
                                                    </button>
                                                </div>

                                                -->

                                                <fieldset class="form-group position-relative">
                                                    <div class="form-group col-md-12">
                                                        <div class="form-group">
                                                            <label for="OrgTypeID" class="fontset5 col-4 text-right">  โครงสร้าง : <span class="text-danger">*</span> </label>
                                                            <select id="OrgTypeID" name="OrgTypeID" class="form-control select2  col-7">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <div class="form-group">
                                                            <label for="OrgLevel2" class="fontset5 col-4 text-right">  สังกัด :  </label>
                                                            <select id="OrgLevel2" name="OrgLevel2" class="form-control select2  col-7">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    
                                                    
                                                </fieldset>

                                                <div class="text-center">
                                                    <button type="button" class="btn btn_b1" id="btnSearch" ><i class="fa fa-search" ></i> ค้นหา </button>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Bootstrap 3 table -->
            </div>
        </div>
    </div>
</section>
 
</body>

</html>


 <!-- footer -->
 <?php include '../include/footer.php'; ?>
  
<script type="text/javascript">
    $(document).ready(function() {

        

        // $(document).ajaxStart(function() {
        //     $(".modal").show();
        // });
        // $(document).ajaxComplete(function() {
        //     $(".modal").hide();
        // });
                /*
        var orgTypeId = $('#orgTypeList :selected').val();
        $("#orgTypeId").val(orgTypeId);

        $.ajax({
            type: "POST",
            url: "../../Model/OrganizationProgram/getOrgList.php",
            data: {
                orgTypeId: orgTypeId
            },
            success: function(data) {
                // console.log(data);
                $('#orgListList').html(data);
            },
            error: function(error) {
                // console.log(error);
            }
        });

        $(function() {
            $('#orgTypeList').change(function() {
                var orgTypeId = $('#orgTypeList :selected').val();
                $("#orgTypeId").val(orgTypeId);

                $.ajax({
                    type: "POST",
                    url: "../../Model/OrganizationProgram/getOrgList.php",
                    data: {
                        orgTypeId: orgTypeId
                    },
                    success: function(data) {
                        // console.log(data);
                        $('#orgListList').html(data);
                    },
                    error: function(error) {
                        // console.log(error);
                    }
                });

            });
        });*/
    });
</script>

 <script type="text/javascript">
    $(document).ready(function() { 
        //OrgLevel2

        // $("#btnsearchStruc").click(function(){
        //     if ( $('#orgTypeList').val() != "" &&  $('#OrgLevel2').val() != ""){
        //         window.location = "detail.php?OrgLevel2="+$('#OrgLevel2').val()+"&orgTypeList="+ $('#orgTypeList').val() ;
        //     }else{
        //         return false ;
        //     }
        // });
        // org.LoadOrgStrucByOrgType(21,'OrgLevel2');


        // $('#orgTypeList').change(function() {
        //     org.LoadOrgStrucByOrgType($('#orgTypeList').val(),'OrgLevel2');
        //  });
/*

        $("#btnsearchStruc").click(function(){
            if ( $('#orgTypeList').val() != "" &&  $('#OrgLevel1').val() != ""){
                window.location = "detail.php?OrgLevel1="+$('#OrgLevel1').val()+"&orgTypeList="+ $('#orgTypeList').val() ;
            }else{
                return false ;
            }
        });




        org.LoadOrgTypeSel('21','orgTypeList'); 
        org.LoadOrg1SelByOrgTypeID('','21','OrgLevel1'); 

        $("#orgTypeList").change(function(){ 

             org.LoadOrg1SelByOrgTypeID('',$("#orgTypeList").val(),'OrgLevel1'); //selfDID,OrgTypeID,element_id,tablename
        }); 



        //org.LoadOrgStrucByOrgType(21,'OrgLevel0');

*/
         
         


         
  
    }); 

    

 </script>



<script type="text/javascript">
    $(document).ready(function() {
        
        org.LoadOrgTypeSel('21','OrgTypeID'); 
        org.LoadOrg2SelByOrgTypeID('','21','OrgLevel2');  
        //org.LoadPersonTypeSel('','PersonTypeID');

        $("#OrgTypeID").change(function(){  
             org.LoadOrg2SelByOrgTypeID('',$("#OrgTypeID").val(),'OrgLevel2'); //selfDID,OrgTypeID,element_id,tablename
        }); 
        $("#btnSearch").click(function(){
            if ( $('#OrgTypeID').val() != "" ){
                window.location = "detail.php?OrgLevel2="+$('#OrgLevel2').val()+"&OrgTypeID="+ $('#OrgTypeID').val() ;
            }else{
                return false ;
            }

            // if ( $('#OrgTypeID').val() != "" &&  $('#OrgLevel2').val() != ""){
            //     window.location = "detail.php?OrgLevel2="+$('#OrgLevel2').val()+"&OrgTypeID="+ $('#OrgTypeID').val() ;
            // }else{
            //     return false ;
            // }
        });
        
    }); 

 </script>
