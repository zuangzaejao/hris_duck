 
<?php 
    
    $data_org2 = $clsorg->LoadOnce('OrgLevel2',array('OrgLevDID' =>$OrgLevel2));   
    $data_orgpart = $clsorg->LoadOnce('OrgPart',array('OrgPartID' =>$data_org2['OrgLevPartID']));      
    $data_orgType =  $clsorg->LoadOnce('OrgType',array('OrgTypeID'=>$data_org2['OrgLevType']) );  
    $data_orgLevel =  $clsorg->LoadOnce('OrgLevel',array('OrgLevelID'=>$data_org2['OrgLev']) );  
    
   //echo "<pre>".print_r( $data_org2 ,true ) ."</pre>";
?>
    <div class="card">
        <div class="card-header cardh1"  >
            <h5 class="card-title " >ส่วนที่ 1 รายละเอียดข้อมูล</h5>
            <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content">
            <div class="card-body">
                <div class="card-content collpase show">
                    <div class="card-body">
                        <form class="form form-horizontal">
                            <div class="form-body">
                                <div class="row">

                                    
                                   
                                    
                                    <hr >

                                    <!-- <div class="col-md-6">
                                            <div class="position-relative"> 
                                        <label class="col-md-12 label-control" for="OrgLev">รหัสส่วนราชการ </label>
                                        <div class="col-md-12">
                                                <input type="text" disabled id="OrgLev" class="form-control" placeholder="" name="OrgLev" >
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="col-md-6 mt-1">
                                        <label class="col-md-12 label-control" for="orgPartName">ชื่อส่วนราชการ</label>
                                        <div class="col-md-12" style="float:left;"> 
                                            <input type="text" disabled id="OrgLevName" class="form-control " name="OrgLevName" value="<?php echo $data_orgpart['OrgPart'];?>">
                                        </div> 
                                    </div> -->

                                   
                                    <div class="col-md-6 mt-1">
                                        <label class="col-md-12 label-control" for="OrgLevDID">รหัสหน่วย</label>
                                        <div class="col-md-12">
                                            <div class="position-relative">
                                                <input type="text" disabled id="OrgLevDID" class="form-control "   name="OrgLevDID" value="<?php echo $OrgLevel2;?>">
                                            </div>
                                        </div>
                                    </div> 

                                    <div class="col-md-6 mt-1">
                                        <label class="col-md-12 label-control" for="divisionID">ลำดับสังกัด/หน่วยใน Tree</label>
                                        <div class="col-md-12">
                                            <div class="position-relative">
                                                <input type="text" disabled id="divisionID" class="form-control " placeholder="" name="divisionID" value="1">
                                            </div>
                                        </div>
                                    </div>



                                    <div class="col-md-6 mt-1">
                                        <label class="col-md-12 label-control" for="orgTypeName">โครงสร้าง</label> <!-- ดึงมาจากการ search หน้า index -->
                                        <div class="col-md-12" style="float:left;"> 
                                            <div class="position-relative">
                                                <input type="text" disabled id="orgTypeName" class="form-control" placeholder="" name="orgTypeName" value="<?php echo $data_orgType['OrgTypeName'];?>">
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="col-md-6 mt-1">
                                        <label class="col-md-12 label-control" for="OrgPartCodeE"> ส่วนราชการ </label>
                                        <div class="col-md-12"  >  
                                            <select name="OrgPartCodeE" id="OrgPartCodeE"  class="form-control "> 
                                            </select>
                                        </div> 
                                    </div> 


                                    <div class="col-md-6 mt-1 hidden">
                                            <div class="position-relative">  
                                        <label class="col-md-12 label-control" for="orgStrucNum">รหัสส่วนราชการ </label>
                                        <div class="col-md-12">
                                                <input type="text" disabled id="orgStrucNum" class="form-control" placeholder="" name="orgStrucNum" value="<?php echo $data_org2['OrgLevPartID'];?>">
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="col-md-6 mt-1 hidden">
                                        <label class="col-md-12 label-control" for="orgPartName">ชื่อส่วนราชการ</label>
                                        <div class="col-md-12" style="float:left;"> 
                                            <input type="text" disabled id="OrgLevName" class="form-control " name="OrgLevName" value="<?php echo $data_orgpart['OrgPart'];?>">
                                        </div> 
                                    </div>
                                    
                                    


                                    <div class="col-md-8 mt-1 hidden">
                                        <label class="col-md-12 label-control" for="orgLevelName">ฐานะของหน่วย/สังกัด</label>
                                        <div class="col-md-12" style="float:left;">
                                            <input type="text" disabled id="orgLevelName" class="form-control " name="orgLevelName" value="<?php echo $data_orgLevel['OrgLevelName'];?>">
                                        </div>
                                    </div> 

                                    <div class="col-md-8 mt-1"> 
                                        <label class="col-md-12 label-control" for="orgSubUnitAbbr">ชื่อสังกัด</label>
                                        <div class="col-md-12" style="float:left;">
                                            <input type="text" disabled id="orgSubUnitAbbr" class="form-control " name="orgSubUnitAbbr" value="<?php echo $data_org2['OrgLevAbbrName'];?>">
                                        </div>  
                                    </div> 
                            
                                    <div class="col-md-8 mt-1">
                                        <label class="col-md-12 label-control" for="OrgLevelE"> ฐานะหน่วย</label>
                                        <div class="col-md-12"  >  
                                            <select name="OrgLevelE" id="OrgLevelE"  class="form-control "> 
                                            </select>
                                        </div> 
                                    </div> 
                                    

                                    <div class="col-md-8 mt-1"> 
                                         <label class="col-md-12 label-control" for="OrgSubUnitE"> ชื่อหน่วย</label>
                                            <div class="col-md-12"  >  
                                                <select name="OrgSubUnitE" id="OrgSubUnitE" class="form-control select2 " style="width: 80%"> 
                                                </select>
                                                <input type="hidden" class="form-control" id="OrgSubUnitCtxtE" name="OrgSubUnitCtxtE">
                                            </div> 
                                    </div>
                                    
                                    
                                    <div class="col-md-8 mt-1">
                                        <label class="col-md-12 label-control" for="orgSubUnitName">ชื่อหน่วย</label>
                                        <div class="col-md-12" style="float:left;">
                                            <input type="text" disabled id="orgSubUnitName" class="form-control " placeholder="" name="orgSubUnitName" value="<?php echo $data_org2['OrgLevName'];?>">
                                        </div> 
                                    </div>
                                    <div class="col-md-8 mt-1">
                                        <label class="col-md-12 label-control" for="orgListAbbr">ชื่อย่อ</label>
                                        <div class="col-md-12">
                                            <div class="position-relative">
                                                <input type="text " disabled id="orgListAbbr" class="form-control " placeholder="" name="orgListAbbr" value="<?php echo $data_org2['OrgLevAbbrName'];?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mt-1">
                                        <label class="col-md-12 label-control" for="orgStrucLong">ชื่อเต็มหน่วย/สังกัด</label>
                                        <div class="col-md-12">
                                            <div class="position-relative">
                                                <input type="text" disabled id="orgStrucLong" class="form-control inp_orgName" placeholder="" name="orgStrucLong" alue="<?php echo $data_orglevel2['OrgLevName'];?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mt-1">
                                        <label class="col-md-12 label-control" for="orgListAbbrLong">ชื่อย่อหน่วย/สังกัด</label>
                                        <div class="col-md-12">
                                            <div class="position-relative">
                                                <input type="text" disabled id="orgListAbbrLong" class="form-control inp_orgNameAbbr" placeholder="" name="orgListAbbrLong">
                                            </div>
                                        </div>
                                    </div>

                                  

                                    <div class="col-md-8 mt-1"> 
                                            <label class="col-md-12" for="ProvinceE">จังหวัด</label>
                                            <div class="col-md-12"  >
                                                <select class="select2 form-control block" id="ProvinceE" name="ProvinceE" style="width:80%;">  
                                                </select>
                                            </div>  
                                    </div>

                                 

                                    <div class="col-md-8 mt-1">  
                                        <label class="col-md-12" for="OrgAFE">หน่วยขึ้นตรง</label>
                                        <div class="col-md-12"  >
                                            <input id="OrgAFE" name="OrgAFE" type="checkbox" checked data-toggle="toggle" data-style="ios" data-on="หน่วยขึ้นตรง" data-off="ไม่เป็นหน่วยขึ้นตรง"  data-onstyle="success" data-offstyle="danger" data-size="sm">
                                        </div> 
                                    </div>  
                                    

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header" style="background-color:#e1e8fc;">
            <h5 class="card-title" style="color:#0f1733;margin-bottom: 0.5rem; margin-top: 0.3rem; margin-left: 0.5rem;"> ส่วนที่ 2 การใช้งาน</h5>
            <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content">
            <div class="card-body">
                <div class="tab-content px-1 pt-1">
                    <div role="tabpanel" class="tab-pane active" id="limit" aria-expanded="true" aria-labelledby="base-limit">
                        <div class="row">
                            <div class="col-12 col-xl-6 border-right-blue-grey border-right-lighten-4 pr-2  pl-2 p-0">
                                <div class="row my-2">
                                    <div class="col-4">
                                        <h5 class="text-bold-500 mb-0">ใช้งาน</h5>
                                    </div>
                                </div>
                                <form class="form form-horizontal">
                                    <div class="form-body">
                                        
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <label class="col-md-12" for="CommandActive">คำสั่ง</label>
                                                <div class="col-md-12" style="float:left;">
                                                    <select class="select2 form-control block" id="CommandActive" name="CommandActive"  style="width: 80%;"> 
                                                    </select>
                                                </div> 
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <label class="col-md-12" for="CommandActiveCode">เลขที่คำสั่ง</label>
                                                <div class="col-md-12">
                                                    <div class="position-relative">
                                                        <input type="text" id="CommandActiveCode" class="form-control "  name="CommandActiveCode">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <label class="col-md-12 label-control text-left" for="CommandActiveDate"> วันที่ลงคำสั่ง : </label>
                                                <div class="input-group datep">
                                                    <input type="text" class="form-control form2 pickadate-translations" placeholder="" id="CommandActiveDate" name="CommandActiveDate" data-value="" />
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"> <span class="la la-calendar-o"></span> </span>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <label class="col-md-12 label-control text-left" for="CommandActiveEFFDate">มีผลตั้งแต่ :</label>
                                                <div class="input-group datep">
                                                    <input type="text" class="form-control form2 pickadate-translations" placeholder="" id="CommandActiveEFFDate" name="CommandActiveEFFDate" />
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"> <span class="la la-calendar-o"></span> </span>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div> 
                                    </div>
                                </form>
                            </div>

                            <!-- 2 -->
                            <div class="col-12 col-xl-6 pl-2 p-0">
                                <div class="row my-2">
                                    <div class="col-4">
                                        <h5 class="text-bold-500 mb-0">ยกเลิก</h5>
                                    </div>
                                </div>
                                <form class="form form-horizontal">
                                    <div class="form-body">
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <label class="col-md-12" for="CommandCancle">คำสั่ง</label>
                                                <div class="col-md-12" >
                                                    <select class="select2 form-control block" id="CommandCancle" class="CommandCancle" style="width: 80%;">  
                                                    </select>
                                                </div> 
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <label class="col-md-12" for="CommandCancleCode">เลขที่คำสั่ง</label>
                                                <div class="col-md-12">
                                                    <div class="position-relative">
                                                        <input type="text" id="CommandCancleCode" class="form-control " class="CommandCancleCode" name="CommandCancleCode">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <label class="col-md-12 label-control text-left" for="CommandCancleDate"> วันที่ลงคำสั่ง : </label>
                                                <div class="input-group datep">
                                                    <input type="text" class="form-control form2 pickadate-translations" placeholder="" id="CommandCancleDate" name="CommandCancleDate" data-value="" />
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"> <span class="la la-calendar-o"></span> </span>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <label class="col-md-12 label-control text-left" for="CommandCancleEFFDate">มีผลตั้งแต่ :</label>
                                                <div class="input-group datep">
                                                    <input type="text" class="form-control form2 pickadate-translations" placeholder="" id="CommandCancleEFFDate" name="CommandCancleEFFDate" data-value="" />
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"> <span class="la la-calendar-o"></span> </span>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div> 

                                    </div>
                                </form>
                            </div> 
                        </div>
                        <hr>
                        <div class="form-group row">
                            <div class="col-md-12"> 
                                <input id="OrgLevActive" type="checkbox" checked data-toggle="toggle" data-style="ios" data-on="ใช้งาน" data-off="ยกเลิก" data-onstyle="success" data-offstyle="danger" data-size="sm">
        
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row text-center">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-danger  round btn-min-width mr-1 mb-1"  >ยกเลิก</button>
                                <button type="button" class="btn btn-success  round btn-min-width mr-1 mb-1" id="btneditcf" >บันทึก</button>
                            </div>
                        </div>
                             
                    </div>
                </div>
            </div>
        </div>
    </div>