 
<?php 
    
    $data_org2 = $clsorg->LoadOnce('OrgLevel2',array('OrgLevDID' =>$OrgLevel2));   
    $data_orgpart = $clsorg->LoadOnce('OrgPart',array('OrgPartID' =>$data_org2['OrgLevPartID']));      
    $data_orgType =  $clsorg->LoadOnce('OrgType',array('OrgTypeID'=>$data_org2['OrgLevType']) );  
    $data_orgLevel =  $clsorg->LoadOnce('OrgLevel',array('OrgLevelID'=>$data_org2['OrgLev']) );  
    
   //echo "<pre>".print_r( $data_org2 ,true ) ."</pre>";
?>
    <div class="card">
        <div class="card-header cardh1"  >
            <h5 class="card-title " >ส่วนที่ 1 รายละเอียดข้อมูล</h5>
            <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content">
            <div class="card-body">
                <div class="card-content collpase show">
                    <div class="card-body">
                        <form class="form form-horizontal">
                            <div class="form-body">
                                <div class="row">

                                    
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="col-md-12 label-control" for="">orgLevDIDView</label>
                                                <div class="col-md-12"> 
                                                    <input type="text" class="form-control" id="orgLevDIDView"  name="orgLevDIDView" value="<?php echo $OrgLevel2;?>"  > 
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <hr >


                                    <div class="col-md-6">
                                        <label class="col-md-12 label-control" for="orgStrucNum">รหัสส่วนราชการ</label>
                                        <div class="col-md-12">
                                            <div class="position-relative">
                                                <input type="text" disabled id="orgStrucNum" class="form-control" placeholder="" name="orgStrucNum" value="<?php echo $data_org2['OrgLevPartID'];?>">
                                            </div>
                                        </div> 
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <label class="col-md-12 label-control" for="orgTypeName">โครงสร้าง</label> <!-- ดึงมาจากการ search หน้า index -->
                                        <div class="col-md-12" style="float:left;"> 
                                            <div class="position-relative">
                                                <input type="text" disabled id="orgTypeName" class="form-control" placeholder="" name="orgTypeName" value="<?php echo $data_orgType['OrgTypeName'];?>">
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="col-md-6 mt-1">
                                        <label class="col-md-12 label-control" for="orgPartName">ชื่อส่วนราชการ</label>
                                        <div class="col-md-12" style="float:left;"> 
                                            <input type="text" disabled id="OrgLevName" class="form-control " name="OrgLevName" value="<?php echo $data_orgpart['OrgPart'];?>">
                                        </div> 
                                    </div>
                                    
                                    <div class="col-md-6 mt-1">
                                        <label class="col-md-12 label-control" for="divisionID">ลำดับสังกัด/หน่วยใน Tree</label>
                                        <div class="col-md-12">
                                            <div class="position-relative">
                                                <input type="text" disabled id="divisionID" class="form-control " placeholder="" name="divisionID" value="1">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-1">
                                        <label class="col-md-12 label-control" for="orgSubUnitId">รหัสหน่วย</label>
                                        <div class="col-md-12">
                                            <div class="position-relative">
                                                <input type="text" disabled id="orgSubUnitId" class="form-control "   name="orgSubUnitId" value="<?php echo $OrgLevel2;?>">
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="col-md-6 mt-1"> 
                                        <label class="col-md-12 label-control" for="orgSubUnitAbbr">ชื่อสังกัด</label>
                                        <div class="col-md-12" style="float:left;">
                                            <input type="text" disabled id="orgSubUnitAbbr" class="form-control " name="orgSubUnitAbbr" value="<?php echo $data_org2['OrgLevAbbrName'];?>">
                                        </div>  
                                    </div>
                                    <div class="col-md-6 mt-1">
                                        <label class="col-md-12 label-control" for="orgLevelName">ฐานะของหน่วย/สังกัด</label>
                                        <div class="col-md-12" style="float:left;">
                                            <input type="text" disabled id="orgLevelName" class="form-control " name="orgLevelName" value="<?php echo $data_orgLevel['OrgLevelName'];?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-1">
                                        <label class="col-md-12 label-control" for="orgSubUnitName">ชื่อหน่วย</label>
                                        <div class="col-md-12" style="float:left;">
                                            <input type="text" disabled id="orgSubUnitName" class="form-control " placeholder="" name="orgSubUnitName" value="<?php echo $data_org2['OrgLevName'];?>">
                                        </div> 
                                    </div>
                                    <div class="col-md-6 mt-1">
                                        <label class="col-md-12 label-control" for="orgListAbbr">ชื่อย่อ</label>
                                        <div class="col-md-12">
                                            <div class="position-relative">
                                                <input type="text " disabled id="orgListAbbr" class="form-control " placeholder="" name="orgListAbbr" value="<?php echo $data_org2['OrgLevAbbrName'];?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-1">
                                        <label class="col-md-12 label-control" for="orgStrucLong">ชื่อเต็มหน่วย/สังกัด</label>
                                        <div class="col-md-12">
                                            <div class="position-relative">
                                                <input type="text" disabled id="orgStrucLong" class="form-control " placeholder="" name="orgStrucLong" alue="<?php echo $data_orglevel2['OrgLevName'];?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-1">
                                        <label class="col-md-12 label-control" for="orgListAbbrLong">ชื่อย่อหน่วย/สังกัด</label>
                                        <div class="col-md-12">
                                            <div class="position-relative">
                                                <input type="text" disabled id="orgListAbbrLong" class="form-control " placeholder="" name="orgListAbbrLong">
                                            </div>
                                        </div>
                                    </div>

                                    <!-- <div class="col-md-6 mt-1">
                                        <label class="col-md-12 label-control" for="orgListAbbrLongAbbr">ชื่อกึ่งย่อหน่วย/สังกัด</label>
                                        <div class="col-md-12">
                                            <div class="position-relative">
                                                <input type="text" disabled id="orgListAbbrLongAbbr" class="form-control " placeholder="" name="orgListAbbrLongAbbr">
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="col-md-6 mt-1 hidden">
                                        <label class="col-md-12 label-control" for="provincesName">จังหวัด</label>
                                        <div class="col-md-12" style="float:left;">
                                        <input type="text" disabled id="provincesName" class="form-control " placeholder="" name="provincesName" value="-">
                                            <!-- <select class="select2 form-control block" id="provincesName" style="width: 100%;"> 
                                                <option value="">จังหวัด</option> 
                                            </select> -->
                                        </div>
                                    </div>
                                    <!--                                         
                                    <div class="col-md-6 mt-1">
                                        <label class="col-md-12 label-control" for="districtsName">อำเภอ</label>
                                        <div class="col-md-12" style="float:left;">
                                        <input type="text" disabled id="districtsName" class="form-control " placeholder="" name="districtsName" value="-">
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    