<div class="card">
    <div class="card-header" style="background-color:#e1e8fc;">
        <h5 class="card-title" style="color:#0f1733;margin-bottom: 0.5rem; margin-top: 0.3rem; margin-left: 0.5rem;"> ส่วนที่ 2 การใช้งาน</h5>
        <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
        <div class="heading-elements">
            <ul class="list-inline mb-0">
                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
            </ul>
        </div>
    </div>
    <div class="card-content">
        <div class="card-body">
            <div class="tab-content px-1 pt-1">
                <div role="tabpanel" class="tab-pane active" id="limit" aria-expanded="true" aria-labelledby="base-limit">
                    <div class="row">
                        <div class="col-12 col-xl-6 border-right-blue-grey border-right-lighten-4 pr-2  pl-2 p-0">
                            <div class="row my-2">
                                <div class="col-4">
                                    <h5 class="text-bold-500 mb-0">ใช้งาน</h5>
                                </div>
                            </div>
                            <form class="form form-horizontal">
                                <div class="form-body">
                                    
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label class="col-md-12" for="orgName">คำสั่ง</label>
                                            <div class="col-md-12" style="float:left;">
                                                <select class="select2 form-control block" id="orgName" style="width: 100%;">
                                                    <!-- border-color: #8c93ee !important; -->
                                                    <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                                                    <option value="">เลือกคำสั่ง</option>
                                                    <option value="">คำสั่ง2</option>
                                                    <!-- <option value="HI">Hawaii</option> -->
                                                    <!-- </optgroup> -->
                                                </select>
                                            </div>
                                            <!-- <a href="../Organizations/create.php"><i class="la la-plus-circle" style="font-size:36px;"></i></a> -->
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label class="col-md-12" for="orgName">เลขที่คำสั่ง</label>
                                            <div class="col-md-12">
                                                <div class="position-relative">
                                                    <input type="text" id="code" class="form-control " placeholder="เลขที่คำสั่ง" name="code">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-md-12" for="orgName">ลง :</label>
                                                <div class="input-group col-md-12">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <span class="la la-calendar-o"></span>
                                                        </span>
                                                    </div>
                                                    <!-- edit เงื่อนไขวัน Date limits ที่ ../../app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js บรรทัด 40 -->
                                                    <input type='text' class="form-control pickadate-limits" placeholder="_/_/_" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-md-12" for="startDate">มีผลตั่งแต่ :</label>
                                                <div class="input-group col-md-12">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <span class="la la-calendar-o"></span>
                                                        </span>
                                                    </div>
                                                    <!-- edit เงื่อนไขวัน Date limits ที่ ../../app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js บรรทัด 40 -->
                                                    <input type='text' id="startDate" disabled name="startDate" class="form-control pickadate-limits" placeholder="_/_/_" />
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </form>
                        </div>

                        <!-- 2 -->
                        <div class="col-12 col-xl-6 pl-2 p-0">
                            <div class="row my-2">
                                <div class="col-4">
                                    <h5 class="text-bold-500 mb-0">ยกเลิก</h5>
                                </div>
                            </div>
                            <form class="form form-horizontal">
                                <div class="form-body">
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label class="col-md-12" for="orgName">คำสั่ง</label>
                                            <div class="col-md-12" style="float:left;">
                                                <select class="select2 form-control block" id="orgName" style="width: 100%;">
                                                    <!-- border-color: #8c93ee !important; -->
                                                    <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                                                    <option value="">เลือกคำสั่ง</option>
                                                    <option value="">คำสั่ง2</option>
                                                    <!-- <option value="HI">Hawaii</option> -->
                                                    <!-- </optgroup> -->
                                                </select>
                                            </div>
                                            <!-- <a href="../Organizations/create.php"><i class="la la-plus-circle" style="font-size:36px;"></i></a> -->
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label class="col-md-12" for="orgName">เลขที่คำสั่ง</label>
                                            <div class="col-md-12">
                                                <div class="position-relative">
                                                    <input type="text" id="code" class="form-control " placeholder="เลขที่คำสั่ง" name="code">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-md-12" for="orgName">ลง :</label>
                                                <div class="input-group col-md-12">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <span class="la la-calendar-o"></span>
                                                        </span>
                                                    </div>
                                                    <!-- edit เงื่อนไขวัน Date limits ที่ ../../app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js บรรทัด 40 -->
                                                    <input type='text' class="form-control pickadate-limits" placeholder="_/_/_" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-md-12" for="orgName">มีผลตั่งแต่ :</label>
                                                <div class="input-group col-md-12">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <span class="la la-calendar-o"></span>
                                                        </span>
                                                    </div>
                                                    <!-- edit เงื่อนไขวัน Date limits ที่ ../../app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js บรรทัด 40 -->
                                                    <input type='text' class="form-control pickadate-limits" placeholder="_/_/_" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <hr>
                    <input id="isActive" type="checkbox" checked data-toggle="toggle" data-style="ios" data-on="ใช้งาน" data-off="ยกเลิก" data-onstyle="success" data-offstyle="danger" data-size="sm">
                </div>
            </div>
        </div>
    </div>
</div>