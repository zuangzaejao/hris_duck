<?php 

require_once '../../config.php';
require_once '../../Model/Ducklab/duck.class.php'; 
require_once '../../Model/Ducklab/contents.class.php'; 
require_once '../../Model/Ducklab/org.class.php'; 
require_once '../../Model/Ducklab/func.php'; 
require_once '../include/header.php'; 
 
  
  $menu1 = "ORGSTRUC" ;
  $menu2 = "ORGSTRUCDEPT" ;
  $menu3 = "ORGSTRUCDEPTPROGRAM" ;
  //$menu4 = "ORGANIZATIONGROUPTYPE" ;
   

 
    $clsorg = new OrgClass();
  
     
    $OrgTypeID = $_GET['OrgTypeID'] ; 
    //$OrgLevel2 = $_GET['OrgLevel2'] ; 


    $OrgLevel1 = $_GET['OrgLevel1'] ; 
    
 
    
   $orgTypeList =  $clsorg->LoadOnce('OrgType',array('OrgTypeID'=>$OrgTypeID) ); 
   $OrgLevel2 = $_GET['OrgLevel2'] ;  
    
    $a = "มี2";
   if(!$OrgLevel2!="" && $OrgTypeID){
    $a = " ไม่มี2  เพิ่ม นขต.";
   } 
   $a ="";
?>
 
<?php 
include '../include/menu.php';  
include_once '../include/modelOnload.php' ;
?>
 

  <section>
      <div class="app-content content">
          <div class="content-wrapper"> 
                <div class="content-header row">
                  <div class="content-header-left col-md-6 col-12 mb-2">
                      <div class="htab" ></div>
                      <h3 class="content-header-title">โปรแกรมจัดการโครงสร้าง<?php echo  $a ;?> </h3>
                  </div>
                </div>
                <div class="  row"> 
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../home/index.php" >ระบบงานโครงสร้างอัตรากำลังพล</a></li>
                            <li class="breadcrumb-item"><a href="./index.php">เลือกโครงสร้าง</a></li>
                            <li class="breadcrumb-item active" aria-current="page">โปรแกรมจัดการโครงสร้าง</li>
                        </ol>
                    </nav>
                </div>

              <?php  
  
              $dataorg = array();

                $i = 0 ;
              
                            
                $dataorg[$i] = $clsorg->LoadOnce('OrgLevel0',array('OrgLevType'=> $OrgTypeID ) );
                $level2 = $clsorg->LoadLevel2ByDID($OrgLevel2);

               
 
        if($level2){
         //   foreach( $level2 as $k2 => $v2 ){
            $v2 = $level2;
            $k2 =0;
            $dataorg[$i]['level2'][$k2] = $v2 ; 
            $level3 = $clsorg->LoadLevel3ByParent($v2['OrgLevDID'] ,'') ;
                $dataorg[$i]['level2'][$k2] = $v2 ; 
                $level3 = $clsorg->LoadLevel3ByParent($v2['OrgLevDID'] ,'') ;
                if($level3){
                    foreach( $level3 as $k3 => $v3 ){
                        $dataorg[$i]['level2'][$k2]['level3'][$k3] = $v3 ;
                        //$level4 = $clsorg->LoadLevel4ByParent($v3['OrgLevDID'] ,$orgTypeList) ;
                        $level4 = $clsorg->LoadLevel4ByParent($v3['OrgLevDID'] ,'') ;
                        if($level4){
                            foreach( $level4 as $k4 => $v4 ){
                                $dataorg[$i]['level2'][$k2]['level3'][$k3]['level4'][$k4] =$v4 ;
                                $level5 = $clsorg->LoadLevel5ByParent($v4['OrgLevDID'] ,'') ;  
                               
                                if($level5){
                                    foreach( $level5 as $k5 => $v5 ){
                                        $dataorg[$i]['level2'][$k2]['level3'][$k3]['level4'][$k4]['level5'][$k5] =$v5 ;
                                        $level6 = $clsorg->LoadLevel6ByParent($v5['OrgLevDID'] ,'') ;  
                                        
                                        if($level6){
                                            foreach( $level6 as $k6 => $v6 ){
                                                $dataorg[$i]['level2'][$k2]['level3'][$k3]['level4'][$k4]['level5'][$k5]['level6'][$k6] =$v6 ;
                                                $level7 = $clsorg->LoadLevel7ByParent($v6['OrgLevDID'] ,'') ;  

                                                if($level7){
                                                    foreach( $level7 as $k7 => $v7 ){
                                                        $dataorg[$i]['level2'][$k2]['level3'][$k3]['level4'][$k4]['level5'][$k5]['level6'][$k6]['level7'][$k7] =$v7 ;
                                                        $level8 = $clsorg->LoadLevel8ByParent($v7['OrgLevDID'] ,'') ;  

                                                        if($level8){
                                                            foreach( $level8 as $k8 => $v8 ){
                                                                $dataorg[$i]['level2'][$k2]['level3'][$k3]['level4'][$k4]['level5'][$k5]['level6'][$k6]['level7'][$k7]['level8'][$k8] =$v8 ;
                                                                
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }   
                                    }
                                }  
                            }
                        }
                    }
                }
          //  }
        }
                
          //     echo "<pre>".print_r($dataorg ,true)."</pre>"; 
                
        ?>


              <div class="container-fluid">
                  <div class="row">
                      <div class="col-lg-4">
                          <div class="content-body"> 
                              <!-- Description -->
                              <div class="sidebar-content card d-none d-lg-block">
                                  <div class="card-header boxheader1" >
                                        โครงการส่วนราชการ
                                      <a style="float:right;"><i class="la la-sitemap"></i> </a>
                                  </div>
                                  <div class="card-body "  >
                                        <div class=" text-center" >
                                            <button class="btn btn-sm btn_b2" id="btn_create1" disabled>
                                                <span class="la la-plus-circle" style=""> เพิ่ม</span>
                                            </button>
                                            <!--
                                            <a href="#" class="btn btn-sm btn_b2" >
                                                <span class="ft-trash-2"> ลบ</span>
                                            </a>
                                            <a href="#" class="btn btn-sm btn_b2" >
                                                <span class="ft-copy"> คัดลอก</span>
                                            </a>
                                            <a href="#" class="btn btn-sm btn_b2" >
                                                <span class="la la-gavel"> คำสั่ง</span>
                                            </a>
                                            <a href="#" class="btn btn-sm btn_b2">
                                                <span class="la la-print">พิมพ์</span>
                                            </a> -->
                                            <a href="testHi.php" class="btn btn-sm btn_b2 hidden" >
                                                <span class="la la-print" >แก้ไขโครงสร้าง</span>
                                            </a>
                                        </div>
 
                                        <div class="card-content">
                                            <div class="card-body skin-flat">
                                            <?php 
                                                require_once 'detail_setting.php';
                                            ?>
                                            <?php //require_once './Hirage.php'; ?>
                                            </div>
                                        </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <!-- ---------------------- -->
                      <!-- /Ratings sample -->



                      <!-- 1 -->
                      <div class="col-lg-8 mb-5">
                          <div class="card-header boxheader1 mb-1" style="">
                               รายละเอียดโครงการส่วนราชการ
                          </div>
                            <div id="box_edit"  >
                                <?php
                                include "detail_edit.php";
                                ?>
                            </div>
                            <div id="box_create" class="hidden"> 
                                <?php 
                                include "detail_create.php";
                                ?>
                            </div>
                      </div>



                      <div class="col-md-12 hidden">
                            <div class="row">
                            <div class="col-md-6">
                                    <label class="col-md-12 label-control" for="">OrgTypeID</label>
                                    <div class="col-md-12"> 
                                        <input type="text" class="form-control" id="OrgTypeID"  name="OrgTypeID"  value="<?php echo  $OrgTypeID;?>"   > 
                                    </div> 
                                </div>
                                <div class="col-md-6">
                                    <label class="col-md-12 label-control" for="">orgLevDIDView</label>
                                    <div class="col-md-12"> 
                                        <input type="text" class="form-control" id="orgLevDIDView"  name="orgLevDIDView"   > 
                                    </div> 
                                </div>



                                <input type="text" id="levelnow" class="form-control "   name="levelnow">
                            </div>
                        </div>
                       


                  </div>
              </div>
          </div>
      </div>
  </section>

 
 

    <!-- footer -->
    <?php include '../include/footer.php'; ?>

  
  </body>

  </html>


  <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" /> -->
  <!-- <script src="//code.jquery.com/ui/1.10.4/jquery-ui.min.js"></script> -->
  <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/ui/dragula.min.css">
    <link rel="stylesheet" type="text/css"  href="../../Asset/custom/css/jquery-ui.css"> 
  <script type="text/javascript" src="../../Asset/custom/js/jquery-ui.min.js"></script>
  <script type="text/javascript" src="../../Asset/js/jquery.mjs.nestedSortable.js"></script>
  <script type="text/javascript" src="../../Asset/custom/js/hirachysetting.js"></script> 
  <link rel="stylesheet" href="../../Asset/custom/css/hirachysetting.css" />
  <!-- END PAGE LEVEL JS-->
    <style>
    .deptnow{
        background: #d8e7ff;
    }
    </style>

    <?php 
 
    $thisdept = $_GET['thisdept'];
    if( !empty($thisdept) ){
        ?>
         <script>  
         org.LoadDataOrgLev('<?php echo $thisdept;?>');
         </script>
        <?php
    } 
    ?>
  <script>
  
   

    $(document).ready(function() {
        //  org.LoadDataOrgLev('<?php echo $OrgLevel2;?>');

        checkOrgLevel2();
	    

        $("#btn_create1").click(function(){ 
           // org.CreateOrgLevel2();
            org.CreateOrgFromStruc(); 
        }); 

        $(".menuDiv").click(function(){
            $(".menuDiv").removeClass("deptnow");
            $(this).addClass("deptnow");
        });

        $("#btncreatecf").click(function(){   
            $('#modal_createcf').modal('show');
        });
        $("#btncreate").click(function(){
            if($("#levelnow").val()<=2){ 
                org.CreateOrgLevel2(); 
            }else{
                org.CreateOrgProg(); 
            }
        });

        $("#btneditcf").click(function(){   
            $('#modal_editcf').modal('show');
        });
        $("#btnedit").click(function(){
            if( $("#levelnow").val() == 2 ){
                org.EditOrgLevel2(); 
            } else{
                org.EditOrgLevel(); 
            }
            
        });



         //
        
        /////////////////////////////////////////////
        //////  For EDIT 
       
        // org.LoadOrgPartSel('','OrgPartCode'); 
        // org.LoadOrgLevelSel('','OrgLevel'); 
        // org.LoadProvinceSel('','ProvinceE');
       // org.LoadSourceofCommSel('','CommandActive');
        //org.LoadSourceofCommSel('','CommandCancle');

        /////////////////////////////////////////////
        //////  For Create 
        // org.LoadOrgPartSel('','OrgPartCodeC'); 
        // org.LoadOrgLevelSel('','OrgLevelC'); 
        // org.LoadProvinceSel('','Province');
         org.LoadSourceofCommSel('','CommandActiveC');
        org.LoadSourceofCommSel('','CommandCancleC');
         
        $("#OrgLevelC").change(function(){
            org.LoadOrgSubUnitSel('','OrgSubUnitC', $(this).val()   );
        });
        $("#OrgSubUnitC").change(function(){ 

            org.LoadDataOrgParent('<?php echo $_GET['thisdept']?>');
            
             $("#OrgSubUnitCtxt").val($(this).find(':selected').data("pname"));
             $("#OrgSubUnitAbbrC").val($(this).find(':selected').data("abbrname"));
             $("#OrgSubUnitSemiAbbrC").val($(this).find(':selected').data("semiabbrname")); 
             
              
             $("#orgStrucLong").val("");
			$("#orgListAbbrLong").val("");
            
			var OrgSubUnitCtxt = $("#OrgSubUnitCtxt").val(); 
            var OrgSubUnitAbbrC = $("#OrgSubUnitAbbrC").val(); 
            

			var orgSubUnitAbbr = $("#OrgSubUnitDept").val(); 
            var orgSubUnitName = $("#OrgSubUnitDeptAbbr").val(); 
            
			var orgListAbbr = $("#OrgSubUnitCtxt").val(); 
            
            var orgSubUnitNametxt =   OrgSubUnitCtxt+" " +  $("#inp_orgName").val() ; 
            var orgSubUnitAbbrtxt =   OrgSubUnitAbbrC+" " + $("#inp_orgNameAbbr").val() ; 
            
            // console.log(orgSubUnitNametxt);
            // console.log(orgSubUnitAbbrtxt);
            // console.log( $("#inp_orgName").val());
            // console.log( $("#inp_orgNameAbbr").val());
            
			$("#OrgSubUnitDept").val(orgSubUnitNametxt); 
            $("#OrgSubUnitDeptAbbr").val(orgSubUnitAbbrtxt); 
         
            //OrgSubUnitDeptAbbr
            //OrgSubUnitDeptSemiAbbr
            //OrgSubUnitDept
        }); 


        $("#OrgLevelE").change(function(){
            org.LoadOrgSubUnitSel('','OrgSubUnitE', $(this).val()   );
        });
        $("#OrgSubUnitE").change(function(){ 
             $("#OrgSubUnitCtxtE").val($(this).find(':selected').data("pname"));
             $("#OrgSubUnitAbbrE").val($(this).find(':selected').data("abbrname"));
             $("#OrgSubUnitSemiAbbrE").val($(this).find(':selected').data("semiabbrname"));  
        });  
        
    });

    //orgLevDIDView
    org.CreateOrgFromStruc=function(){

        $("#modalLoading").show();
        setTimeout( function(){ $("#modalLoading").hide(); }   , 1500);

        $("#box_edit").addClass("hidden");
        $("#box_create").removeClass("hidden");

        var orgLevDID = $('#orgLevDIDView').val();
        $('#orgLevDIDParent').val(orgLevDID);
        org.LoadOrgforCreate();
        
        // window.location.href= 'create.php?orgLevDID='+orgLevDID;

    };

    org.LoadOrgforCreate=function(){

        var orgLevDIDParent = $('#orgLevDIDParent').val();
       // alert(orgLevDIDParent);

        // orgPartCode orgPartName
        // 
        var DataSet = {
            orgLevDIDParent :  orgLevDIDParent   
            
        };
        console.log(DataSet);
        
        $.ajax({ 
                url: org.url+'?mode=LoadOrgforCreate',
                type:'POST',
                dataType:'json',
                data: DataSet,
            beforeSend:function(){
                $("#modalLoading").show();
            },
            success:function(result,status,xhr){
                console.log(result);
    
                $("#OrgDID").val(result.OrgLevDID);
                $("#divisionIDC").val(result.nextLevel);
                
                org.LoadOrgLevelSel('','OrgLevelC' );  

                var lev= parseInt(result.nextLevel) ;
                $("#levelnow").val(lev); 
                

                if(lev==2){
                    $("#OrgPartCodeC").removeAttr('disabled');
                }else{
                    $("#OrgPartCodeC").attr('disabled','disabled');
                }
                



            },
            complete:function(){ 
                $("#modalLoading").hide();
            },
            error:function(xhr,status,error){
                console.log('Please check scripts LoadOrgforCreate');
                console.log(xhr);
                duck.NotiDanger();
            }
	    }) ;
    };
    
    org.CreateOrgLevel2=function(){
       
        $('#modal_createcf').modal('hide');
        
        var levelnow =  $("#levelnow").val() ;
        var lastparent = $("#lastparent").val();



        if(levelnow ==0){
            org.CreateOrg0();
            return false;	
        }

        var OrgAF = 0 ;
        var OrgLevActive = 0;
        
        lastparent = 2;
    
         var checkBox = document.getElementById("OrgAF");
        if (checkBox.checked == true) {
              OrgAF = "1";
        } 
        
        var checkBox = document.getElementById("OrgLevActiveC");
        if (checkBox.checked == true) {
        	OrgLevActive = "1";
        } 
        
        var orgLevDIDParent = $("#orgLevDIDView").val();

        var prov = $("#Province").val();
        var  LocCode ='000000'; 
        if(prov){
            LocCode = prov ; 
        } 
       

        var table = "OrgLevel2" ; 
        var exDat = {
            table :  table,
            data  : { 
                OrgLevPartID	:  $("#OrgPartCodeC").val(),
                OrgLev	:  $("#OrgLevelC").val(),
                OrgLevName :  $("#OrgSubUnitCtxt").val(),
                OrgLevAbbrName  :  $("#OrgSubUnitAbbrC").val(), 
                OrgLevSubUnit  :  $("#OrgSubUnitC").val(), 
                LocCode :  LocCode, 
                OrgLevType :  $("#OrgTypeID").val(),  
                OrgLevel	:  2,
                OrgLevState : 'N',
                OrgLevDelete : '0',
                OrgLevActive : OrgLevActive ,
                OrgAF : OrgAF  , 
                CommandActive :  $("#CommandActiveC").val(), 
                CommandActiveCode :  $("#CommandActiveCodeC").val(), 
                CommandActiveDate :   $("input[name=CommandActiveDateC_submit]").val(),   
                CommandActiveEFFDate :   $("input[name=CommandActiveEFFDateC_submit]").val(),  

                CommandActive :  $("#CommandActiveC").val(), 
                CommandCancleCode :  $("#CommandCancleCodeC").val(), 
                CommandCancleDate :   $("input[name=CommandCancleDateC_submit]").val(), 
                CommandCancleEFFDate :   $("input[name=CommandCancleEFFDateC_submit]").val(), 
 
            } ,
            orgParentCode :  orgLevDIDParent,
            levelnow :  '2'
        }; 

        console.log(exDat)

        $.ajax({
            url: org.url+'?mode=CreateOrgData',
            type:'POST',
            dataType:'json',
            data: exDat ,
            beforeSend:function(){
                $('#modal_createcf').hide();
                $("#modalLoading").show();
            },
            success:function(data){
                
                console.log(data);
                $("#modalLoading").hide();
                
                if( data.data.success == "COMPLETE"){
                   // var link = "edit.php?dataid="+data.code ;
                    var link = "detail.php?OrgLevel2="+$("#OrgDID").val()+"&OrgTypeID="+$("#OrgTypeID").val()+"&thisdept="+$("#OrgDID").val() ;
                    duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ'); 
                    setTimeout( duck.OpenPage   , 2000 , link ,'_self' );  
                   // setTimeout(duck.ReloadPage   , 2000  ); 
                }else{
                    //duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ'); 
                    duck.NotiWarning();
                } 
    
            },complete:function(){
                $("#modalLoading").hide();
            },error:function(data){
                console.log(data);
                duck.NotiDanger();
            }
	    }); 

    };

    org.EditOrgLevel2=function(){
       
       $('#modal_editcf').modal('hide');
       
       var levelnow =  $("#levelnow").val() ;
       var lastparent = $("#lastparent").val();


        /*
       if(levelnow ==0){
           org.CreateOrg0();
           return false;	
       }*/

       var OrgAF = 0 ;
       var OrgLevActive = 0;
       
       lastparent = 2;
   
        var checkBox = document.getElementById("OrgAFE");
       if (checkBox.checked == true) {
             OrgAF = "1";
       } 
       
       var checkBox = document.getElementById("OrgLevActive");
       if (checkBox.checked == true) {
           OrgLevActive = "1";
       } 
       
       var orgLevDIDParent = $("#orgLevDIDView").val();

       var prov = $("#ProvinceE").val();
       var  LocCode ='000000'; 
       if(prov){
           LocCode = prov ; 
       } 
      

       var table = "OrgLevel2" ; 
       var exDat = {
           table :  table,
           data  : { 
               OrgLevPartID	:  $("#OrgPartCodeE").val(),
               OrgLev	:  $("#OrgLevel").val(),
               OrgLevName :  $("#OrgSubUnitCtxtE").val(),
               OrgLevAbbrName  :  $("#OrgSubUnitAbbr").val(), 
               OrgLevSubUnit  :  $("#OrgSubUnit").val(), 
               LocCode :  LocCode, 
               OrgLevType :  $("#OrgTypeID").val(),  
               OrgLevel	:  2,
               OrgLevState : 'N',
               OrgLevDelete : '0',
               OrgLevActive : OrgLevActive ,
               OrgAF : OrgAF  , 
               CommandActive :  $("#CommandActive").val(), 
               CommandActiveCode :  $("#CommandActiveCode").val(), 
               CommandActiveDate :   $("input[name=CommandActiveDate_submit]").val(),   
               CommandActiveEFFDate :   $("input[name=CommandActiveEFFDate_submit]").val(),  

               CommandActive :  $("#CommandActive").val(), 
               CommandCancleCode :  $("#CommandCancleCode").val(), 
               CommandCancleDate :   $("input[name=CommandCancleDate_submit]").val(), 
               CommandCancleEFFDate :   $("input[name=CommandCancleEFFDate_submit]").val(), 

           } ,
           OrgLevDID : $("#OrgLevDID").val()  
       }; 

       console.log(exDat)

       $.ajax({
           url: org.url+'?mode=EditOrgData',
           type:'POST',
           dataType:'json',
           data: exDat ,
           beforeSend:function(){
               $('#modal_editcf').hide();
               $("#modalLoading").show();
           },
           success:function(data){
               
               console.log(data);
               $("#modalLoading").hide();
               
               if( data.data.success == "COMPLETE"){ 
                     var link = "detail.php?OrgTypeID="+$("#OrgTypeID").val()+"&OrgLevel2="+$("#orgLev2").val()+"&thisdept="+$("#OrgLevDID").val() ;
                   duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ'); 
                    setTimeout( duck.OpenPage   , 2000 , link ,'_self' );  
                  // setTimeout(duck.ReloadPage   , 2000  ); 
               }else{
                   duck.NotiWarning();
               } 
   
           },complete:function(){
               $("#modalLoading").hide();
           },error:function(data){
               console.log(data);
               duck.NotiDanger();
           }
       }); 

    };

    
    org.CreateOrgProg=function(){

        $('#modal_createcf').modal('hide');

        var  levelnow =  $("#levelnow").val() ;
        var lastparent = $("#lastparent").val();

        if(levelnow ==0){
            org.CreateOrg0();
            return false;	
        }
        var orgLevDIDParent = $("#orgLevDIDView").val();
        var OrgAF = 0 ;
        var OrgActive = 0;

        var checkBox = document.getElementById("OrgAF");
        if (checkBox.checked == true) {
            OrgAF = "1";
        } 

        var checkBox = document.getElementById("OrgLevActiveC");
        if (checkBox.checked == true) {
            OrgActive = "1";
        } 

        
        var prov = $("#Province").val();
            var  LocCode ='000000'; 
            if(prov){
                LocCode = prov ; 
            } 

        if($("input[name=CommandActiveDateC_submit]").val()){
            var CommandActiveDateC = $("input[name=CommandActiveDateC_submit]").val() ;
        }else{
            var CommandActiveDateC =  'NULL' ;
        }
        if($("input[name=CommandActiveEFFDateC_submit]").val()){
            var CommandActiveEFFDateC = $("input[name=CommandActiveEFFDateC_submit]").val() ;
        }else{
            var CommandActiveEFFDateC =   'NULL'  ;
        }
        if($("input[name=CommandCancleDateC_submit]").val()){
            var CommandCancleDateC = $("input[name=CommandCancleDateC_submit]").val() ;
        }else{
            var CommandCancleDateC =  'NULL' ;
        }
        if($("input[name=CommandCancleEFFDateC_submit]").val()){
            var CommandCancleEFFDateC = $("input[name=CommandCancleEFFDateC_submit]").val() ;
        }else{
            var CommandCancleEFFDateC =   'NULL'  ;
        }
       

        var table = "OrgLevel"+levelnow ;   
            var exDat = {
                table :  table,
                data  : { 
                    OrgLevPartID	:  $("#OrgPartCodeC").val(),
                    OrgLev	:  $("#OrgLevelC").val(),
                    OrgLevName :  $("#OrgSubUnitCtxt").val(),
                    OrgLevAbbrName  :  $("#OrgSubUnitAbbrC").val(), 
                    OrgLevSubUnit  :  $("#OrgSubUnitC").val(), 
                    LocCode :  LocCode  , 
                    OrgLevType :  $("#OrgTypeID").val(),  
                    OrgLevel	:  2,
                    OrgLevState : 'N',
                    OrgLevDelete : '0',
                    OrgLevActive : OrgActive ,
                    OrgAF : OrgAF  , 
                    CommandActive :  $("#CommandActiveC").val(), 
                    CommandActiveCode :  $("#CommandActiveCodeC").val(), 
                    CommandActiveDate :   CommandActiveDateC ,   
                    CommandActiveEFFDate :  CommandActiveEFFDateC  ,  

                    CommandActive :  $("#CommandActiveC").val(), 
                    CommandCancleCode :  $("#CommandCancleCodeC").val(), 
                    CommandCancleDate :   CommandCancleDateC , 
                    CommandCancleEFFDate :  CommandCancleEFFDateC , 
    
                } ,
                orgParentCode :  orgLevDIDParent,
                levelnow :  levelnow
            }; 

        console.log(exDat)
        
        $.ajax({
            url: org.url+'?mode=CreateOrgData',
            type:'POST',
            dataType:'json',
            data: exDat ,
            beforeSend:function(){
                $('#modal_createcf').hide();
                $("#modalLoading").show();
            },
            success:function(data){
                
                console.log(data);
                $("#modalLoading").hide();
                
                if( data.data.success == "COMPLETE" && data.dataParent.success == "COMPLETE"){
                    var link = "detail.php?OrgLevel2="+$("#orgLev2").val()+"&OrgTypeID="+$("#OrgTypeID").val()+"&thisdept="+$("#OrgDID").val() ;
                    duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ'); 
                    setTimeout( duck.OpenPage   , 2000 , link ,'_self' ); 
                }else{
                    duck.NotiWarning();
                } 

            },complete:function(){
                $("#modalLoading").hide();
            },error:function(data){
                console.log(data);
                duck.NotiDanger();
            }
        }); 
 
    };

    org.EditOrgLevel=function(){
       
       $('#modal_editcf').modal('hide');
       
       var levelnow =  $("#levelnow").val() ; 


       var OrgAF = 0 ;
       var OrgLevActive = 0;
        
       
        var checkBox = document.getElementById("OrgAFE");
       if (checkBox.checked == true) {
             OrgAF = "1";
       } 
       
       var checkBox = document.getElementById("OrgLevActive");
       if (checkBox.checked == true) {
           OrgLevActive = "1";
       } 
       
       var orgLevDIDParent = $("#orgLevDIDView").val();

       var prov = $("#ProvinceE").val();
       var  LocCode ='000000'; 
       if(prov){
           LocCode = prov ; 
       } 
      
       if($("input[name=CommandActiveDate_submit]").val()){
            var CommandActiveDate = $("input[name=CommandActiveDate_submit]").val() ;
        }else{
            var CommandActiveDate =  'NULL' ;
        }
        if($("input[name=CommandActiveEFFDate_submit]").val()){
            var CommandActiveEFFDate = $("input[name=CommandActiveEFFDate_submit]").val() ;
        }else{
            var CommandActiveEFFDate =   'NULL'  ;
        }
        if($("input[name=CommandCancleDate_submit]").val()){
            var CommandCancleDate = $("input[name=CommandCancleDate_submit]").val() ;
        }else{
            var CommandCancleDate =  'NULL' ;
        }
        if($("input[name=CommandCancleEFFDate_submit]").val()){
            var CommandCancleEFFDate = $("input[name=CommandCancleEFFDate_submit]").val() ;
        }else{
            var CommandCancleEFFDate =   'NULL'  ;
        }

       var table = "OrgLevel"+levelnow ; 
       var exDat = {
           table :  table,
           data  : { 
               OrgLevPartID	:  $("#OrgPartCodeE").val(),
               OrgLev	:  $("#OrgLevel").val(),
               OrgLevName :  $("#OrgSubUnitCtxtE").val(),
               OrgLevAbbrName  :  $("#OrgSubUnitAbbr").val(), 
               OrgLevSubUnit  :  $("#OrgSubUnitE").val(), 
               LocCode :  LocCode, 
               OrgLevType :  $("#OrgTypeID").val(),  
               OrgLevel	:  levelnow,
               OrgLevState : 'N',
               OrgLevDelete : '0',
               OrgLevActive : OrgLevActive ,
               OrgAF : OrgAF  , 
               CommandActive :  $("#CommandActive").val(), 
               CommandActiveCode :  $("#CommandActiveCode").val(), 
               CommandActiveDate :   CommandActiveDate,   
               CommandActiveEFFDate :   CommandActiveEFFDate,  

               CommandActive :  $("#CommandActive").val(), 
               CommandCancleCode :  $("#CommandCancleCode").val(), 
               CommandCancleDate :   CommandCancleDate, 
               CommandCancleEFFDate :   CommandCancleEFFDate, 

           } ,
           OrgLevDID : $("#OrgLevDID").val()  
       }; 

       console.log(exDat)

       $.ajax({
           url: org.url+'?mode=EditOrgData',
           type:'POST',
           dataType:'json',
           data: exDat ,
           beforeSend:function(){
               $('#modal_editcf').hide();
               $("#modalLoading").show();
           },
           success:function(data){
               
               console.log(data);
               $("#modalLoading").hide();
               
               if( data.data.success == "COMPLETE"){ 
                     var link = "detail.php?OrgTypeID="+$("#OrgTypeID").val()+"&OrgLevel2="+$("#orgLev2").val()+"&thisdept="+$("#OrgLevDID").val() ;
                   duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ'); 
                    setTimeout( duck.OpenPage   , 2000 , link ,'_self' ); 
               }else{
                   duck.NotiWarning();
               } 
   
           },complete:function(){
               $("#modalLoading").hide();
           },error:function(data){
               console.log(data);
               duck.NotiDanger();
           }
       }); 

    };


    

    
    //+"&thisdept="+$("#OrgLevelC").val() ;

    function checkOrgLevel2(){
        if(!$('#orgLev2').val()){
		    return false;
	    }
        org.LoadOrgAFDetail($('#orgLev2').val());
    }
    
  </script>
 