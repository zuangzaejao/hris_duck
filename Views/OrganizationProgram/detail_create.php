<?php 
 

 $OrgLevelx = $_REQUEST['orgLevDID'];

    $orgTable = substr($OrgLevelx ,0,2);
    $orgTablenum = substr($OrgLevelx ,1,1);
    $orgPK = substr($OrgLevelx ,2,8);
    


    switch($orgTable){
        case '02' : 
            $level2 =$clsorg->LoadOnce('OrgLevel'.$orgTablenum , array('OrgLevDID'=>  $OrgLevelx) ) ;  break ;
        default : 
            $levelX =$clsorg->LoadOnce('OrgLevel'.$orgTablenum , array('OrgLevDID'=>  $OrgLevelx) ) ; 
    }


    $Lev2OrgLevAbbrName =$level2['OrgLevAbbrName'] ;  


    
?>
    <div class="card">
        <div class="card-header cardh1"  >
            <h5 class="card-title " >ส่วนที่ 1 รายละเอียดข้อมูล</h5>
            <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content">
            <div class="card-body">
                <div class="card-content collpase show">
                    <div class="card-body">
                        <form class="form form-horizontal">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12  hidden">
                                        <div class="row">  
                                            <div class="col-md-6">
                                                <label class="col-md-12 label-control" for="">orgLevDIDParent</label>
                                                <div class="col-md-12"> 
                                                    <input type="text" class="form-control" id="orgLevDIDParent"  name="orgLevDIDParent" value="<?php echo $OrgLevel2;?>"  > 
                                                </div> 
                                            </div>
                                            <div class="col-md-6">
                                                <label class="col-md-12 label-control" for="">orgLev2</label>
                                                <div class="col-md-12"> 
                                                    <input type="text" class="form-control" id="orgLev2"  name="orgLev2" value="<?php echo $OrgLevel2;?>"  > 
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <hr >

                                    <div class="col-md-12">

                                        <div class="row ">
                                            <div class="col-md-6 mt-1">
                                                <label class="col-md-12 label-control" for="OrgDID">รหัส </label>
                                                <div class="col-md-12">
                                                    <input type="text"  id="OrgDID" class="form-control "  name="OrgDID"  disabled >
                                                </div>
                                            </div>
                                            <div class="col-md-4 mt-1">
                                                <label class="col-md-12 label-control" for="divisionIDC">ลำดับสังกัด/หน่วยใน Tree</label>
                                                <div class="col-md-12">
                                                    <input type="text"  class="form-control "  name="divisionIDC"  id="divisionIDC"  disabled>
                                                </div>
                                            </div>
                                            
                                        </div>

                                        <div class="row ">
                                            <div class="col-md-6 mt-1">
                                                <label class="col-md-12 label-control" for="orgTypeName">โครงสร้าง</label>
                                                <div class="col-md-12" style="float:left;"> 
                                                    <div class="position-relative">
                                                        <input type="text"  id="orgTypeName" class="form-control" placeholder="" name="orgTypeName"  value="<?php echo $orgTypeList['OrgTypeName'];?>" disabled>
                                                    </div>
                                                </div> 
                                            </div>
                                            <div class="col-md-6 mt-1">
                                                <label class="col-md-12 label-control" for="OrgPartCodeC">รหัสส่วนราชการ</label>
                                                <div class="col-md-12"  >  
                                                    <select name="OrgPartCodeC" id="OrgPartCodeC" class="form-control"> 
                                                    </select>
                                                </div> 
                                            </div>
                                        </div>
                                        
                                        <div class="row ">
                                            <div class="col-md-8 mt-1">
                                                <label class="col-md-12 label-control" for="OrgLevelC"> ฐานะหน่วย</label>
                                                <div class="col-md-12"  >  
                                                    <select name="OrgLevelC" id="OrgLevelC"  class="form-control "> 
                                                    </select>
                                                </div> 
                                            </div> 
                                        </div>
                                        <div class="row ">
                                            <div class="col-md-8 mt-1">
                                                <label class="col-md-12 label-control" for="OrgSubUnitC"> ชื่อหน่วย</label>
                                                <div class="col-md-12"  >  
                                                    <select name="OrgSubUnitC" id="OrgSubUnitC" class="form-control select2 " style="width: 80%"> 
                                                    </select>
                                                    <input type="hidden" class="form-control" id="OrgSubUnitCtxt" name="OrgSubUnitCtxt">
                                                </div> 
                                            </div> 
                                        </div>
                                        <div class="row ">
                                            <div class="col-md-8 mt-1">
                                                <label class="col-md-12 label-control" for="OrgSubUnitAbbrC"> ชื่อย่อ </label>
                                                <div class="col-md-12"  >  
                                                    <input type="text" class="form-control" id="OrgSubUnitAbbrC" name="OrgSubUnitAbbrC" disabled>
                                                </div>
                                            </div> 
                                        </div>
                                       
                                        <div class="row ">
                                            <div class="col-md-12 mt-1">
                                                <label class="col-md-12 label-control" for="OrgSubUnitDept"> ชื่อ หน่วย/สังกัด </label>
                                                <div class="col-md-12"  >  
                                                    <input type="text" class="form-control" id="OrgSubUnitDept" name="OrgSubUnitDept" disabled>
                                                </div> 
                                            </div> 
                                        </div>
                                        <div class="row ">
                                            <div class="col-md-12 mt-1">
                                                <label class="col-md-12 label-control" for="OrgSubUnitDeptAbbr">  ชื่อย่อ หน่วย/สังกัด </label>
                                                <div class="col-md-12"  >  
                                                    <input type="text" class="form-control " id="OrgSubUnitDeptAbbr" name="OrgSubUnitDeptAbbr" disabled>
 
                                                    <input type="hidden" class="form-control inp_orgName" id="inp_orgName" name="" >
                                                    <input type="hidden" class="form-control inp_orgNameAbbr" id="inp_orgNameAbbr" name="" >
                                                </div> 
                                            </div> 
                                        </div>
                                         

                                        <div class="form-group row">
                                            <div class="col-md-8 mt-1">
                                                <label class="col-md-12" for="Province">จังหวัด</label>
                                                <div class="col-md-12"  >
                                                    <select class="select2 form-control block" id="Province" name="Province" style="width: 80%;">  
                                                    </select>
                                                </div> 
                                            </div>
                                        </div>

                                         
                                        <div class="col-md-8 mt-1">  
                                        <label class="col-md-12" for="OrgAF">หน่วยขึ้นตรง</label>
                                        <div class="col-md-12"  >
                                            <input id="OrgAF" name="OrgAF" type="checkbox" checked data-toggle="toggle" data-style="ios" data-on="หน่วยขึ้นตรง" data-off="ไม่เป็นหน่วยขึ้นตรง"  data-onstyle="success" data-offstyle="danger" data-size="sm" style="width: 130px !important;">
                                        </div> 
                                    </div>  
                                        
                                        
                                    </div>


                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header" style="background-color:#e1e8fc;">
            <h5 class="card-title" style="color:#0f1733;margin-bottom: 0.5rem; margin-top: 0.3rem; margin-left: 0.5rem;"> ส่วนที่ 2 การใช้งาน</h5>
            <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content">
            <div class="card-body">
                <div class="tab-content px-1 pt-1">
                    <div role="tabpanel" class="tab-pane active" id="limit" aria-expanded="true" aria-labelledby="base-limit">
                        <div class="row">
                            <div class="col-12 col-xl-6 border-right-blue-grey border-right-lighten-4 pr-2  pl-2 p-0">
                                <div class="row my-2">
                                    <div class="col-4">
                                        <h5 class="text-bold-500 mb-0">ใช้งาน</h5>
                                    </div>
                                </div>
                                <form class="form form-horizontal">
                                    <div class="form-body">
                                        
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <label class="col-md-12" for="CommandActiveC">คำสั่ง</label>
                                                <div class="col-md-12" style="float:left;">
                                                    <select class="select2 form-control block" id="CommandActiveC" name="CommandActiveC"  style="width: 80%;"> 
                                                    </select>
                                                </div> 
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <label class="col-md-12" for="CommandActiveCodeC">เลขที่คำสั่ง</label>
                                                <div class="col-md-12">
                                                    <div class="position-relative">
                                                        <input type="text" id="CommandActiveCodeC" class="form-control "  name="CommandActiveCodeC">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <label class="col-md-12 label-control text-left" for="CommandActiveDateC"> วันที่ลงคำสั่ง : </label>
                                                <div class="input-group datep">
                                                    <input type="text" class="form-control form2 pickadate-translations" placeholder="" id="CommandActiveDateC" name="CommandActiveDateC" data-value="" />
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"> <span class="la la-calendar-o"></span> </span>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <label class="col-md-12 label-control text-left" for="CommandActiveEFFDateC">มีผลตั้งแต่ :</label>
                                                <div class="input-group datep">
                                                    <input type="text" class="form-control form2 pickadate-translations" placeholder="" id="CommandActiveEFFDateC" name="CommandActiveEFFDateC" data-value="" />
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"> <span class="la la-calendar-o"></span> </span>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div> 
                                    </div>
                                </form>
                            </div>

                            <!-- 2 -->
                            <div class="col-12 col-xl-6 pl-2 p-0">
                                <div class="row my-2">
                                    <div class="col-4">
                                        <h5 class="text-bold-500 mb-0">ยกเลิก</h5>
                                    </div>
                                </div>
                                <form class="form form-horizontal">
                                    <div class="form-body">
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <label class="col-md-12" for="CommandCancleC">คำสั่ง</label>
                                                <div class="col-md-12" style="float:left;">
                                                    <select class="select2 form-control block" id="CommandCancleC" class="CommandCancleC" style="width: 80%;">  
                                                    </select>
                                                </div> 
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <label class="col-md-12" for="CommandCancleCodeC">เลขที่คำสั่ง</label>
                                                <div class="col-md-12">
                                                    <div class="position-relative">
                                                        <input type="text" id="CommandCancleCodeC" class="form-control " class="CommandCancleCodeC" name="CommandCancleCodeC">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <label class="col-md-12 label-control text-left" for="CommandCancleDateC"> วันที่ลงคำสั่ง : </label>
                                                <div class="input-group datep">
                                                    <input type="text" class="form-control form2 pickadate-translations" placeholder="" id="CommandCancleDateC" name="CommandCancleDateC" data-value="" />
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"> <span class="la la-calendar-o"></span> </span>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <label class="col-md-12 label-control text-left" for="CommandCancleEFFDateC">มีผลตั้งแต่ :</label>
                                                <div class="input-group datep">
                                                    <input type="text" class="form-control form2 pickadate-translations" placeholder="" id="CommandCancleEFFDateC" name="CommandCancleEFFDateC" data-value="" />
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"> <span class="la la-calendar-o"></span> </span>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div> 

                                    </div>
                                </form>
                            </div> 
                        </div>
                        <hr>
                        <div class="form-group row">
                            <div class="col-md-12"> 
                                <input id="OrgLevActiveC" type="checkbox" checked data-toggle="toggle" data-style="ios" data-on="ใช้งาน" data-off="ยกเลิก" data-onstyle="success" data-offstyle="danger" data-size="sm">
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row text-center">
                            <div class="col-md-12">     
                                <button type="button" class="btn btn-danger  round btn-min-width mr-1 mb-1"  >ยกเลิก</button>
                                <button type="button" class="btn btn-success  round btn-min-width mr-1 mb-1" id="btncreatecf" >บันทึก</button>
                            </div>
                        </div>
                             
                    </div>
                </div>
            </div>
        </div>
    </div>
    