<?php
if($vlv3['level4']){
    foreach( $vlv3['level4']  as $klv4 => $vlv4){
    ?>
        <ol>
        <li class="mjs-nestedSortable-leaf" id="menuItem_<?php echo $vlv4['OrgLevDID'];?>">
            <div class="menuDiv">
                <span title="กดเพื่อแสดงหน่วยงานในสังกัด" class="disclose ui-icon ui-icon-minusthick"></span>
                <span  onclick="org.LoadDataOrgLev('<?php echo $vlv4['OrgLevDID'];?>');">
                    <span data-id="<?php echo $vlv4['OrgLevDID'];?>" class="itemTitle"><?php echo $vlv4['OrgLevDID'].": ".$vlv4['OrgLevName'];?>
                    </span>
                </span>
            </div> 
            <?php 
                if($vlv4['level5']){
                    foreach( $vlv4['level5']  as $klv5 => $vlv5){
                    ?>
                    <ol>
                        <li class="mjs-nestedSortable-leaf" id="menuItem_<?php echo $vlv5['OrgLevDID'];?>">
                            <div class="menuDiv">
                                <span title="กดเพื่อแสดงหน่วยงานในสังกัด" class="disclose ui-icon ui-icon-minusthick"></span>
                                <span>
                                    <span data-id="<?php echo $vlv5['OrgLevDID'];?>" class="itemTitle"><?php echo $vlv5['OrgLevDID'].": ".$vlv5['OrgLevName'];?>
                                    </span>
                                </span>
                            </div> 
                            <?php 
                                if($vlv5['level6']){
                                    foreach( $vlv5['level6']  as $klv6 => $vlv6){
                                    ?>
                                    <ol>
                                        <li class="mjs-nestedSortable-leaf" id="menuItem_<?php echo $vlv6['OrgLevDID'];?>">
                                            <div class="menuDiv">
                                                <span title="กดเพื่อแสดงหน่วยงานในสังกัด" class="disclose ui-icon ui-icon-minusthick"></span>
                                                <span>
                                                    <span data-id="<?php echo $vlv6['OrgLevDID'];?>" class="itemTitle"><?php echo $vlv6['OrgLevDID'].": ".$vlv6['OrgLevName'];?>
                                                    </span>
                                                </span>
                                            </div>
                                            <?php 
                                            if($vlv6['level7']){
                                                foreach( $vlv6['level7']  as $klv7 => $vlv7){
                                                ?>
                                                <ol>
                                                    <li class="mjs-nestedSortable-leaf" id="menuItem_<?php echo $vlv7['OrgLevDID'];?>">
                                                        <div class="menuDiv">
                                                            <span title="กดเพื่อแสดงหน่วยงานในสังกัด" class="disclose ui-icon ui-icon-minusthick"></span>
                                                            <span>
                                                                <span data-id="<?php echo $vlv7['OrgLevDID'];?>" class="itemTitle"><?php echo $vlv7['OrgLevDID'].": ".$vlv7['OrgLevName'];?>
                                                                </span>
                                                            </span>
                                                        </div> 
                                                        <?php 
                                                            if($vlv7['level8']){
                                                                foreach( $vlv7['level8']  as $klv8 => $vlv8){
                                                                ?>
                                                                <ol>
                                                                    <li class="mjs-nestedSortable-leaf" id="menuItem_<?php echo $vlv8['OrgLevDID'];?>">
                                                                        <div class="menuDiv">
                                                                            <span title="กดเพื่อแสดงหน่วยงานในสังกัด" class="disclose ui-icon ui-icon-minusthick"></span>
                                                                            <span>
                                                                                <span data-id="<?php echo $vlv8['OrgLevDID'];?>" class="itemTitle"><?php echo $vlv8['OrgLevDID'].": ".$vlv8['OrgLevName'];?>
                                                                                </span>
                                                                            </span>
                                                                        </div> 
                                                                    </li>
                                                                </ol>
                                                        <?php
                                                                }
                                                            }
                                                        ?>
                                                    </li>
                                                </ol>
                                        <?php
                                                }
                                            }
                                        ?>
                                        </li>
                                    </ol>
                            <?php
                                    }
                                }
                            ?>
                        </li>
                    </ol>
                    <?php
                    }
                }
            ?>
        </li>
    </ol>
    <?php
    }
}
?>