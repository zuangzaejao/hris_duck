
<section id="demo">
    <ol class="sortable ui-sortable mjs-nestedSortable-branch mjs-nestedSortable-expanded">
        
    <?php 
    foreach( $dataorg  as $kall => $vall){
        ?>
        <li style="display: list-item;" class="mjs-nestedSortable-branch mjs-nestedSortable-expanded" id="menuItem_<?php echo $vall['OrgLevDID'];?>" data-af="<?php echo $vall['OrgLevDID'];?>">
            <div class="menuDiv box_<?php echo  $vall['OrgLevDID'];?>">
                <span title="กดเพื่อแสดงหน่วยงานในสังกัด" class="disclose ui-icon ui-icon-minusthick"> </span>
                <span  onclick="org.LoadDataOrgLev('<?php echo  $vall['OrgLevDID'];?>');" >
                    <span data-id="<?php echo $vall['OrgLevDID'];?>" class="itemTitle">  <?php echo  $vall['OrgLevDID'].": ".$vall['OrgLevName'] . " " ; ?> </span>
                </span>
            </div>
            <?php 
            if($vall['level2']){
                foreach( $vall['level2']  as $klv2 => $vlv2){
            ?>
                <ol>
                    <li class="mjs-nestedSortable-leaf" id="menuItem_<?php echo $vlv2['OrgLevDID'];?>">
                        <div class="menuDiv box_<?php echo  $vlv2['OrgLevDID'];?>" >
                            <span title="กดเพื่อแสดงหน่วยงานในสังกัด" class="disclose ui-icon ui-icon-minusthick"></span>
                            <span  onclick="org.LoadDataOrgLev('<?php echo $vlv2['OrgLevDID'];?>');" >
                                <span data-id="<?php echo $vlv2['OrgLevDID'];?>" class="itemTitle"><?php echo $vlv2['OrgLevDID'].": ".$vlv2['OrgLevName'];?></span>
                            </span>
                        </div>
                        <?php 
                        if($vlv2['level3']){
                            foreach( $vlv2['level3']  as $klv3 => $vlv3){
                        ?>
                            <ol>
                                <li class="mjs-nestedSortable-leaf" id="menuItem_<?php echo $vlv3['OrgLevDID'];?>">
                                    <div class="menuDiv box_<?php echo  $vlv3['OrgLevDID'];?>" >
                                        <span title="กดเพื่อแสดงหน่วยงานในสังกัด" class="disclose ui-icon ui-icon-minusthick"></span>
                                        <span  onclick="org.LoadDataOrgLev('<?php echo $vlv3['OrgLevDID'];?>');" >
                                            <span data-id="<?php echo $vlv3['OrgLevDID'];?>" class="itemTitle"><?php echo $vlv3['OrgLevDID'].": ".$vlv3['OrgLevName'];?> </span>
                                        </span>
                                    </div>  

                                    <?php
                                        if($vlv3['level4']){
                                            foreach( $vlv3['level4']  as $klv4 => $vlv4){
                                            ?>
                                                <ol>
                                                <li class="mjs-nestedSortable-leaf" id="menuItem_<?php echo $vlv4['OrgLevDID'];?>">
                                                    <div class="menuDiv box_<?php echo  $vlv4['OrgLevDID'];?>"  >
                                                        <span title="กดเพื่อแสดงหน่วยงานในสังกัด" class="disclose ui-icon ui-icon-minusthick"></span>
                                                        <span onclick="org.LoadDataOrgLev('<?php echo $vlv4['OrgLevDID'];?>');">
                                                            <span data-id="<?php echo $vlv4['OrgLevDID'];?>" class="itemTitle"><?php echo $vlv4['OrgLevDID'].": ".$vlv4['OrgLevName'];?></span>
                                                        </span>
                                                    </div> 
                                                    <?php 
                                                        if($vlv4['level5']){
                                                            foreach( $vlv4['level5']  as $klv5 => $vlv5){
                                                            ?>
                                                            <ol>
                                                                <li class="mjs-nestedSortable-leaf" id="menuItem_<?php echo $vlv5['OrgLevDID'];?>">
                                                                    <div class="menuDiv box_<?php echo  $vlv5['OrgLevDID'];?>" >
                                                                        <span title="กดเพื่อแสดงหน่วยงานในสังกัด" class="disclose ui-icon ui-icon-minusthick"></span>
                                                                        <span onclick="org.LoadDataOrgLev('<?php echo $vlv5['OrgLevDID'];?>');">
                                                                            <span data-id="<?php echo $vlv5['OrgLevDID'];?>" class="itemTitle"><?php echo $vlv5['OrgLevDID'].": ".$vlv5['OrgLevName'];?></span>
                                                                        </span>
                                                                    </div> 
                                                                    <?php 
                                                                        if($vlv5['level6']){
                                                                            foreach( $vlv5['level6']  as $klv6 => $vlv6){
                                                                            ?>
                                                                            <ol>
                                                                                <li class="mjs-nestedSortable-leaf" id="menuItem_<?php echo $vlv6['OrgLevDID'];?>">
                                                                                    <div class="menuDiv box_<?php echo  $vlv6['OrgLevDID'];?>" >
                                                                                        <span title="กดเพื่อแสดงหน่วยงานในสังกัด" class="disclose ui-icon ui-icon-minusthick"></span>
                                                                                        <span onclick="org.LoadDataOrgLev('<?php echo $vlv6['OrgLevDID'];?>');">
                                                                                            <span data-id="<?php echo $vlv6['OrgLevDID'];?>" class="itemTitle"><?php echo $vlv6['OrgLevDID'].": ".$vlv6['OrgLevName'];?>
                                                                                            </span>
                                                                                        </span>
                                                                                    </div>
                                                                                    <?php 
                                                                                    if($vlv6['level7']){
                                                                                        foreach( $vlv6['level7']  as $klv7 => $vlv7){
                                                                                        ?>
                                                                                        <ol>
                                                                                            <li class="mjs-nestedSortable-leaf" id="menuItem_<?php echo $vlv7['OrgLevDID'];?>">
                                                                                                <div class="menuDiv box_<?php echo  $vlv7['OrgLevDID'];?>">
                                                                                                    <span title="กดเพื่อแสดงหน่วยงานในสังกัด" class="disclose ui-icon ui-icon-minusthick"></span>
                                                                                                    <span  onclick="org.LoadDataOrgLev('<?php echo $vlv7['OrgLevDID'];?>');">
                                                                                                        <span data-id="<?php echo $vlv7['OrgLevDID'];?>" class="itemTitle"><?php echo $vlv7['OrgLevDID'].": ".$vlv7['OrgLevName'];?>
                                                                                                        </span>
                                                                                                    </span>
                                                                                                </div> 
                                                                                                <?php 
                                                                                                    if($vlv7['level8']){
                                                                                                        foreach( $vlv7['level8']  as $klv8 => $vlv8){
                                                                                                        ?>
                                                                                                        <ol>
                                                                                                            <li class="mjs-nestedSortable-leaf" id="menuItem_<?php echo $vlv8['OrgLevDID'];?>">
                                                                                                                <div class="menuDiv box_<?php echo  $vlv8['OrgLevDID'];?>"  >
                                                                                                                    <span title="กดเพื่อแสดงหน่วยงานในสังกัด" class="disclose ui-icon ui-icon-minusthick"></span>
                                                                                                                    <span onclick="org.LoadDataOrgLev('<?php echo $vlv8['OrgLevDID'];?>');">
                                                                                                                        <span data-id="<?php echo $vlv8['OrgLevDID'];?>" class="itemTitle"><?php echo $vlv8['OrgLevDID'].": ".$vlv8['OrgLevName'];?>
                                                                                                                        </span>
                                                                                                                    </span>
                                                                                                                </div> 
                                                                                                            </li>
                                                                                                        </ol>
                                                                                                <?php
                                                                                                        }
                                                                                                    }
                                                                                                ?>
                                                                                            </li>
                                                                                        </ol>
                                                                                <?php
                                                                                        }
                                                                                    }
                                                                                ?>
                                                                                </li>
                                                                            </ol>
                                                                    <?php
                                                                            }
                                                                        }
                                                                    ?>
                                                                </li>
                                                            </ol>
                                                            <?php
                                                            }
                                                        }
                                                    ?>
                                                </li>
                                            </ol>
                                            <?php
                                            }
                                        }
                                        ?>
                                    
                                </li>
                            </ol>
                        <?php 
                            }
                        }
                        ?>
                    </li>
                </ol>
        <?php 
                }
            }
            ?>
        </li>
    <?php
        } 
    ?>


    </ol>
        
          


            <li style="display: list-item;" class="mjs-nestedSortable-branch mjs-nestedSortable-expanded" id="menuItem_<?php echo $level21['OrgLevDID0'];?>" data-af="<?php echo $level21['OrgLevDID0'];?>">
            <div class="menuDiv">
                <span title="กดเพื่อแสดงหน่วยงานในสังกัด" class="disclose ui-icon ui-icon-minusthick"> </span>
                <span onclick="org.LoadDataOrgLev('<?php echo $level21['OrgLevDID0'];?>');">
                    <span data-id="<?php echo $level21['OrgLevDID0'];?>" class="itemTitle">   <?php echo   $level21['OrgLevName0'] . " " ; ?> </span>
                </span>
            </div>
            <ol>
            </ol>
        </li>

  <!--

                <h3>Try the custom methods:</h3>
                <p><br>
                <input id="serialize" name="serialize" type="submit" value=
                "Serialize"></p>
                <pre id="serializeOutput">
                </pre>

                <p><input id="toArray" name="toArray" type="submit" value=
                "To array"></p>
                <pre id="toArrayOutput">
                </pre>

                <p><input id="toHierarchy" name="toHierarchy" type="submit" value=
                "To hierarchy"></p>
                <pre id="toHierarchyOutput">
                </pre>

    -->



</section><!-- END #demo -->
