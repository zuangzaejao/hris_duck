<?php 
   require_once '../../config.php';
   require_once '../../Model/Ducklab/duck.class.php'; 
   require_once '../../Model/Ducklab/contents.class.php'; 
   require_once '../../Model/Ducklab/org.class.php'; 
   require_once '../../Model/Ducklab/func.php'; 
   require_once '../include/header.php'; 
 
 
    $menu1 ="ORGSTRUC" ;
    $menu2 ="ORGSTRUCPOSITION";
    $menu3 ="POS"; 

    $clsorg = new OrgClass();
    //$lastid = $clsorg->LoadFieldsMaxID('HrtPos','HrtPosCode' , ''); 
     

    $lastid = $clsorg->LoadIdenCurrent('HrtPos');
    $newid = $lastid['lastid']+1 ;
 
  ?>
  <?php include '../include/menu.php'; ?>
   
  <?php include_once '../include/modelOnload.php' ?>


  <div class="app-content content">
      <div class="content-wrapper">
          <div class="content-header row">
              <div class="content-header-left col-md-6 col-12 mb-2">
                  <h3 class="content-header-title">เพิ่มชื่อตำแหน่ง</h3>
                  <div class="row breadcrumbs-top">

                  </div>
              </div>

          </div>
          <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="./index.php">ระบบงานโครงสร้างอัตรากำลังพล</a></li>
                  <li class="breadcrumb-item"><a href="./index.php">ตำแหน่ง</a></li>
                  <li class="breadcrumb-item active" aria-current="page">เพิ่มชื่อตำแหน่ง</li>
              </ol>
          </nav>
          <div class="content-body">
              <!-- Basic form layout section start -->
              <section id="horizontal-form-layouts">

                  <div class="row">
                      <div class="col-md-12">
                          <div class="card">
                              <div class="card-header">
                                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                  <div class="heading-elements">
                                      <ul class="list-inline mb-0">
                                          <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                          <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                          <li><a data-action="close"><i class="ft-x"></i></a></li>
                                      </ul>
                                  </div>
                              </div>
                              <div class="card-content collpase show">
                                    <div class="card-body p-2">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="HrtPosName">  รหัสชื่อตำแหน่ง</label>
                                                            <input type="text"class="form-control" name="HrtPosCode"  id="HrtPosCode" value="<?php echo  $newid;?>" disabled >
                                                        </div>
                                                    </div> 
                                                </div>
                                                 <div class="row">
                                                    <div class="col-md-6 hidden">
                                                        <div class="form-group"> 
                                                            <div class="">
                                                                <label for="OrgTypeID">  โครงสร้าง </label>
                                                            </div> 
                                                            <select id="OrgTypeID" name="OrgTypeID" class="form-control select2">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="">
                                                                 <label for="HrtPersonTypeID"> ประเภทกำลังพล </label>
                                                            </div>  
                                                            <select id="HrtPersonTypeID" name="HrtPersonTypeID" class="form-control select2">
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div> 
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="HrtPosName">  ชื่อเต็มตำแหน่ง</label>
                                                            <input type="text"class="form-control" name="HrtPosName"  id="HrtPosName" >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="HrtPosAbbrName"> ชื่อย่อตำแหน่ง </label> 
                                                            <input type="text" class="form-control" name="HrtPosAbbrName" id="HrtPosAbbrName" >
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="">
                                                                <label for="HrtPosHeadStatus">  ระดับชั้นตำแหน่ง</label>
                                                            </div> 
                                                            <select id="HrtPosHeadStatus" name="HrtPosHeadStatus" class="form-control select2">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    
                                                </div>  
                                                
                                                <div class="col-sm-12 mb-2">
                                                    <fieldset class="checkboxs">
                                                        <label>
                                                            <input id="HrtTypeOfPos"  type="checkbox"   >&nbsp; ตำแหน่งประจำหน่วย
                                                        </label>
                                                    </fieldset>
                                                    <fieldset class="checkbox">
                                                        <label>
                                                            <input id="HrtFlagNotDisp" type="checkbox">&nbsp;แสดงแฉพาะตำแหน่ง
                                                        </label>
                                                    </fieldset> 
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="">
                                                                <label for="HrtPosTypeGroup">  กลุ่มประเภทตำแหน่ง</label>
                                                            </div> 
                                                            <select id="HrtPosTypeGroup" name="HrtPosTypeGroup" class="form-control select2">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="">
                                                                <label for="HrtPosType"> ประเภทตำแหน่ง </label>
                                                            </div>
                                                            <select id="HrtPosType" name="HrtPosType" class="form-control select2">
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>   
                                                <br>

                                                <div class="row">
                                                    <div class="col-md-12"> 
                                                        <label class="col-md-1 label-control" for="HrtPosCodeActive" style="padding-right:0px;">สถานะ</label>
                                                        <input type="checkbox" id="HrtPosCodeActive"  name="HrtPosCodeActive"  checked data-toggle="toggle" data-style="ios" data-on="ใช้งาน" data-off="ยกเลิก" data-onstyle="success" data-offstyle="danger" data-size="sm">
                                                    </div>
                                                </div>

                                                <div class="form-actions text-center">
                                                   
                                                    <input type="hidden"  class="form-control border-primary"  name="HrtPosCodeCreateBy" id="HrtPosCodeCreateBy" value="1">
                                                    <input type="hidden"  class="form-control border-primary"  name="HrtPosCodeCreateDate" id="HrtPosCodeCreateDate" value="<?php echo GetToday('');?>">
 
                                                  <button type="button" class="btn btn-danger  round btn-min-width mr-1 mb-1"  >ยกเลิก</button>
                                                  <button type="button" class="btn btn-success  round btn-min-width mr-1 mb-1" id="btncreatecf" >บันทึก</button>

                                                </div>
                                           
                                          </div>

                                      </form>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                   
              </section>
              <!-- // Basic form layout section end -->
          </div>
      </div>
  </div>
  <?php include '../include/footer.php'; ?>
  
   
    <script type="text/javascript">
    $( document ).ready(function() {

        org.LoadOrgTypeSel('','OrgTypeID');
		org.LoadPersonTypeSel('','HrtPersonTypeID');
        org.LoadPosTypeGroupSel('','HrtPosTypeGroup');
        org.LoadPosTypeSel('','HrtPosType','');
        org.LoadPosHeadStatusSel('','HrtPosHeadStatus');
        
        
        $('#HrtPersonTypeID').change(function(){
            org.LoadPosTypeGroupSel('','HrtPosTypeGroup',$('#HrtPersonTypeID').val() );
        }); 


        $('#HrtPosTypeGroup').change(function(){
            org.LoadPosTypeSel('','HrtPosType',$('#HrtPosTypeGroup').val() );
        }); 

        $("#btncreatecf").click(function(){   
            $('#modal_createcf').modal('show');
        });
        $("#btncreate").click(function(){
            org.CreatePos();
        });
    });

 
    </script>