<?php 

require_once '../../config.php';
require_once '../../Model/Ducklab/duck.class.php'; 
require_once '../../Model/Ducklab/contents.class.php'; 
require_once '../../Model/Ducklab/org.class.php'; 
require_once '../../Model/Ducklab/func.php'; 
require_once '../include/header.php'; 
 
  
$menu1 ="ORGSTRUC" ;
$menu2 ="ORGSTRUCPOSITION";
$menu3 ="POS"; 
   

  $clsorg = new OrgClass();

 
include_once '../include/menu.php'; 
include_once '../include/modelOnload.php' ;
?>
 
  <section>
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="htab" ></div>
            <h3 class="content-header-title">ชื่อตำแหน่ง</h3>
          </div>
        </div>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb"> 
            <li class="breadcrumb-item"><a href="../home/index.php">ระบบงานโครงสร้างอัตรากำลังพล</a></li>
          <li class="breadcrumb-item"><a href="../home/index.php">ตำแหน่ง</a></li>
          <li class="breadcrumb-item active" aria-current="page">ชื่อตำแหน่ง</li>
          </ol>
        </nav>
        <div class="content-body">
          <section id="bootstrap3">

          <div class="row"> 
              <div class="col-sm-12">
                <div class="box_h1 card">
                  <div class="card-header card-head-inverse  ">
                    <h4 class="card-title text-white"> ค้นหา </h4> 
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                      <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                      </ul>
                    </div>
                  </div>
                  <div class="card-content collapse show">
                    <div class="card-body">
                      <div class="row">
                        <div class=" col-md-8  m-auto "> 
                          <form class="form">
                            <div class="form-body">  
                                <div class="row">
                                    <!-- <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="OrgTypeID">  โครงสร้าง <span class="text-danger">*</span> </label>
                                        <select id="OrgTypeID" name="OrgTypeID" class="form-control">
                                        </select>
                                      </div>
                                    </div> -->
                                    <div class="col-md-6 m-auto">
                                      <div class="form-group">
                                        <label for="PersonTypeID">  ประเภทกำลังพล <span class="text-danger">*</span> </label>
                                        <select id="PersonTypeID" name="PersonTypeID" class="form-control">
                                        </select>
                                      </div>
                                    </div>
                                </div>  
                            </div> 
                            <div class="text-center">
                              <button type="button" class="btn round btn1 btncustom1 mr-1" id="btnSearch">
                                <i class="fa fa-search"></i> ค้นหา
                              </button> 
                              <button type="reset" class="btn round btn1 btncustom1">
                                <i class=" fa fa-repeat"></i> ล้างค่า
                              </button>
                            </div> 
                          </form> 
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div> 
  
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-content collapse show">
                    <div class="card-body card-dashboard">
                      <p class="card-text"></p>
                      
                      <a href="./create.php" class="btn btn-social btn-min-width mb-1 btn_b1" >
                        <span class="la la-plus-circle" ></span> เพิ่ม
                      </a>
                      
                      <table id="PosTable" class="table table-striped table-borderless table-hover table_custom1 "  >
                        <thead>
                          <tr>
                            <th></th>
                            <th>ลำดับที่</th>
                            <th>รหัส</th>
                            <th class="text-left">ชื่อ</th>
                            <th class="text-left">ชื่อย่อ</th> 
                            <th>สถานะ</th>
                          </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                      </table>
                      <form id="formset" method="post" action="">
                        <input type="hidden" id="dataid" name="dataid" value="">
                        <input type="hidden" id="action" name="action" value="">  
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </section>


  <input type="hidden" id="dataTableSet" name="dataTableSet" value="0" >

  <?php
     include "../include/footer.php";
   ?> 

 <script type="text/javascript">
       
         
    $(document).ready(function() {
       // org.LoadOrgTypeSel('','OrgTypeID');
        org.LoadPersonTypeSel('','PersonTypeID');
        $("#btnSearch").click(function(){
          if(  $("#PersonTypeID").val() !=""  ){
            org.LoadPos();
          }
        });
    });
 </script> 