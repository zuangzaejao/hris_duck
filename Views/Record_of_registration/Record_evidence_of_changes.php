  <!-- header -->
  <?php include '../include/header.php'; ?>

  <!-- menu -->
  <?php include '../include/menu.php'; ?>

  <style>
      /* ol > li > a {color:#222233;} */
      .toggle.ios,
      .toggle-on.ios,
      .toggle-off.ios {
          border-radius: 20rem;
      }.
      .btn-danger{
        
      }
      
  </style>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <!-- select2 -->
  <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/forms/selects/select2.min.css">
  
  <section>
      <div class="app-content content">
          <div class="content-wrapper">
              <div class="content-header row">
                  <div class="content-header-left col-12 mb-2">
                      <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
                      <h3 class="content-header-title">บันทึกข้อมูลหลักฐานการเปลี่ยนแปลง</h3>
                  </div>
              </div>
              <div class="content-body">
                  <!-- Bootstrap 3 table -->
                  <section id="bootstrap3">
                      <div class="row">
                          <div class="col-12">
                              <div class="card">
                                  <div class="card-content collapse show">
                                      <div class="card-body card-dashboard">

                                          <nav aria-label="breadcrumb">
                                              <ol class="breadcrumb">
                                                  <li class="breadcrumb-item"><a href="../home/index.php">ระบบงานสัสดี</a></li>
                                                  <li class="breadcrumb-item"><a href="../home/index.php">บันทึกข้อมูลหลักฐานการเปลี่ยนแปลง</a></li>
                                                  <li class="breadcrumb-item " aria-current="page">บันทึกการแก้ไขข้อมูล</li>
                                              </ol>
                                          </nav>
                                          <div></div>



                                          <div id="how-to" class="card">
                                              <!-- card -->
                                              <div class="card-header" style="background-color:#0f1733;padding:1.0rem 1.0rem">
                                                  <h6 class="card-title">
                                                      <font color="White">บันทึกข้อมูลหลักฐานการเปลี่ยนแปลง
                                                  </h6>
                                                  </font>
                                                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                                  <div class="heading-elements">
                                                      <ul class="list-inline mb-0">
                                                          <li><a data-action="collapse"><i class="ft-minus"></i></a>
                                                          </li>
                                                      </ul>
                                                  </div>
                                              </div> 
                                          </div> <!-- card -->

                           <div class="container">

                                              <div class="card-content">
                                                  <div class="card-body">



                                                  <div class="row">    
                                                    <div class="col-12"> 
                                                    แนบไฟล์หลักฐานการสมรส :
                                                        <!-- image-preview-filename input [CUT FROM HERE]-->
                                                        <div class="input-group image-preview">
                                                            <input type="text" class="form-control image-preview-filename" disabled="disabled">
                                                             <!-- don't give a name === doesn't send on POST/GET -->
                                                            <span class="input-group-btn">                                                                
                                                                <div class="btn btn-default image-preview-input">
                                                                    <span class="glyphicon glyphicon-folder-open"></span>
                                                                    <span class="image-preview-input-title">Browse</span>
                                                                    <input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/> <!-- rename it -->
                                                                </div>
                                                            </span>
                                                        </div><!-- /input-group image-preview [TO HERE]--> 
                                                    </div>
                                                </div>




                                                    <div class="tab-content px-1 pt-1">
                                                          <div role="tabpanel" class="tab-pane active" id="tab11" aria-expanded="true" aria-labelledby="base-tab11">

                                                              <div class="card collapse-icon accordion-icon-rotate active">
                                                                  <div id="headingCollapse31" class="card-header bg-success">
                                                                      <a data-toggle="collapse" href="#collapse31" aria-expanded="true" aria-controls="collapse31" class="card-title lead white">
                                                                          <h6 style="margin-left:5px;">หลักฐานทะเบียนราษฎร์</h6>
                                                                      </a>
                                                                  </div>



                                                                  <div class="tab-content px-1 pt-1">
                                                                                  <div role="tabpanel55"
                                                                                      class="tab-pane active"
                                                                                      id="tab1155" aria-expanded="true"
                                                                                      aria-labelledby="base-tab1155">

                                                                                      <div
                                                                                          class="card collapse-icon accordion-icon-rotate active">
                                                                                          
                                                                                          <div id="collapse55"
                                                                                              role="tabpanel55"
                                                                                              aria-labelledby="headingCollapse55"
                                                                                              class="card-collapse collapse show"
                                                                                              aria-expanded="true">
                                                                                              <div class="card-content">
                                                                                                  <div
                                                                                                      class="card-body">
                                                                                                      <div
                                                                                                          class="row match-height">


                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      หลักฐาน 
                                                                                                                      :
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">

                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>

                                                                                                          
                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">

                                                                                                                  <div
                                                                                                                      class="input-group">
                                                                                                                      วัน/เดือน/ปี ที่ลง :  
                                                                                                                      <input
                                                                                                                          type="text"
                                                                                                                          class="form-control pickadate-disable-dates"
                                                                                                                          placeholder="25 กรกฏาคม 2562"
                                                                                                                          aria-describedby="button-addon4">
                                                                                                                      <div
                                                                                                                          class="input-group-append">
                                                                                                                          <button
                                                                                                                              class="btn btn-primary"
                                                                                                                              type="button"
                                                                                                                              style=" padding-bottom: 1px; padding-top: 1px;"><i
                                                                                                                                  class="la la-calendar-o"></i></button>
                                                                                                                      </div>
                                                                                                                  </div>

                                                                                                              </div>
                                                                                                          </div>

                                                                                                          <div
                                                                                                              class=" col-md-6">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      เขตที่ออกหลักฐาน : 
                                                                                                                      <select
                                                                                                          class="select2 form-control"
                                                                                                          style="width: 100%;">
                                                                                                                <optgroup
                                                                                                                    label="อําเภอ/เขต">
                                                                                                                    <option
                                                                                                                        value="AK">
                                                                                                                        เลือก
                                                                                                                    </option>
                                                                                                                    <option value="HI">เขตที่ออกหลักฐาน</option>
                                                                                                                        <option value="HI">เขตที่ 1 </option>
                                                                                                                        <option value="HI">เขตที่ 2 </option>
                                                                                                                        <option value="HI">เขตที่ 3 </option>
                                                                                                                        <option value="HI">เขตที่ 4 </option>
                                                                                                                        <option value="HI">เขตที่ 5</option>
                                                                                                                        <option value="HI">เขตที่ 6</option>
                                                                                                                        <option value="HI">เขตที่ 7 </option>                                                                                                                     
                                                                                                                </optgroup>                                                                                                         
                                                                                                            </select>

                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>


                                                                                                      </div>
                                                                                                  </div>
                                                                                              </div>
                                                                                          </div>

                                                                                      </div>



                                                                                  </div>
                                                                              </div>

                                                              </div>
                                                          </div>
                                                    </div>

                                                    <div class="tab-content px-1 pt-1">
                                                          <div role="tabpanel" class="tab-pane active" id="tab11" aria-expanded="true" aria-labelledby="base-tab11">

                                                              <div class="card collapse-icon accordion-icon-rotate active">
                                                                  <div id="headingCollapse31" class="card-header bg-success">
                                                                      <a data-toggle="collapse" href="#collapse31" aria-expanded="true" aria-controls="collapse31" class="card-title lead white">
                                                                          <h6 style="margin-left:5px;">ส่วนที่2 หลักฐานคำสั่ง</h6>
                                                                      </a>
                                                                  </div>



                                                                  <div class="tab-content px-1 pt-1">
                                                                                  <div role="tabpanel55"
                                                                                      class="tab-pane active"
                                                                                      id="tab1155" aria-expanded="true"
                                                                                      aria-labelledby="base-tab1155">

                                                                                      <div
                                                                                          class="card collapse-icon accordion-icon-rotate active">
                                                                                          
                                                                                          <div id="collapse55"
                                                                                              role="tabpanel55"
                                                                                              aria-labelledby="headingCollapse55"
                                                                                              class="card-collapse collapse show"
                                                                                              aria-expanded="true">
                                                                                              <div class="card-content">
                                                                                                  <div
                                                                                                      class="card-body">
                                                                                                      <div
                                                                                                          class="row match-height">


                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      คำสั่ง :
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">

                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>

                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      เลขที่คำสั่ง :
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">

                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>
                                                                                                          <!-- ---------วัน/เดือน/ปีที่ลง :--------- -->  
                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="input-group">
                                                                                                                      วัน/เดือน/ปี ที่ลง :
                                                                                                                      <input
                                                                                                                          type="text"
                                                                                                                          class="form-control pickadate-disable-dates"
                                                                                                                          placeholder="25 กรกฏาคม 2562"
                                                                                                                          aria-describedby="button-addon4">
                                                                                                                      <div
                                                                                                                          class="input-group-append">
                                                                                                                          <button
                                                                                                                              class="btn btn-primary"
                                                                                                                              type="button"
                                                                                                                              style=" padding-bottom: 1px; padding-top: 1px;"><i
                                                                                                                                  class="la la-calendar-o"></i></button>
                                                                                                                      </div>
                                                                                                                  </div>
                                                                                                                </div>
                                                                                                           </div>
                                                                                                           <!-- --------วันที่มีผล :---------- -->     
                                                                                                           <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="input-group">
                                                                                                                      วันที่มีผล :
                                                                                                                      <input
                                                                                                                          type="text"
                                                                                                                          class="form-control pickadate-disable-dates"
                                                                                                                          placeholder="25 กรกฏาคม 2562"
                                                                                                                          aria-describedby="button-addon4">
                                                                                                                      <div
                                                                                                                          class="input-group-append">
                                                                                                                          <button
                                                                                                                              class="btn btn-primary"
                                                                                                                              type="button"
                                                                                                                              style=" padding-bottom: 1px; padding-top: 1px;"><i
                                                                                                                                  class="la la-calendar-o"></i></button>
                                                                                                                      </div>
                                                                                                                  </div>
                                                                                                                </div>
                                                                                                           </div>
                                                                                                          <!-- -------ลำดับที่ในคำสั่ง :-------- --> 
                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      ลำดับที่ในคำสั่ง :
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">

                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>
                                                                                                                 <!-- --------------- -->                                                                                         
                                                                                                        </div>
                                                                                                  </div>
                                                                                              </div>
                                                                                          </div>

                                                                                      </div>



                                                                                  </div>
                                                                              </div>

                                                              </div>
                                                          </div>
                                                    </div>

                                                    <div class="tab-content px-1 pt-1">
                                                          <div role="tabpanel" class="tab-pane active" id="tab11" aria-expanded="true" aria-labelledby="base-tab11">

                                                              <div class="card collapse-icon accordion-icon-rotate active">
                                                                  <div id="headingCollapse31" class="card-header bg-success">
                                                                      <a data-toggle="collapse" href="#collapse31" aria-expanded="true" aria-controls="collapse31" class="card-title lead white">
                                                                          <h6 style="margin-left:5px;">ส่วนที่3 หลักฐานอื่นๆ </h6>
                                                                      </a>
                                                                  </div>
                                                                  <div class="tab-content px-1 pt-1">
                                                                                  <div role="tabpanel55"
                                                                                      class="tab-pane active"
                                                                                      id="tab1155" aria-expanded="true"
                                                                                      aria-labelledby="base-tab1155">

                                                                                      <div
                                                                                          class="card collapse-icon accordion-icon-rotate active">
                                                                                          
                                                                                          <div id="collapse55"
                                                                                              role="tabpanel55"
                                                                                              aria-labelledby="headingCollapse55"
                                                                                              class="card-collapse collapse show"
                                                                                              aria-expanded="true">
                                                                                              <div class="card-content">
                                                                                                  <div
                                                                                                      class="card-body">
                                                                                                      <div
                                                                                                          class="row match-height">
                                                                                                          <!-- ---------หลักฐาน :--------- -->  
                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      หลักฐาน :                                                                                                                   
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">

                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>
                                                                                                           <!-- ---------หมายเหตุ :--------- -->  
                                                                                                          <div 
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">                                                                                                                      
                                                                                                                      หมายเหตุ :
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">
                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>                                                                                                            
                                                                                              



                                                                                                      </div>
                                                                                                  </div>
                                                                                              </div>
                                                                                          </div>

                                                                                      </div>



                                                                                  </div>
                                                                              </div>

                                                              </div>
                                                          </div>
                                                    </div>








                                                  </div>
                                              </div>
                                          </div>

                                          <div class="tab-content px-1 pt-1">
                                                                      <div class="form-actions center" align="center">
                                                                          <!-- <button type="button" class="btn btn-danger  round btn-min-width mr-1 mb-1" id="type-error">ยกเลิก</button>
                                <button type="button" class="btn btn-success  round btn-min-width mr-1 mb-1" id="confirm-text" onclick="insertOrganizationParts()">บันทึก</button> -->

                                                                          <a href="Record_evidence_of_changes.php"><button type="button"
                                                                              class="btn btn-success  round btn-min-width mr-1 mb-1"
                                                                              id="submit" name="submit"
                                                                              onclick="insertOrganizationGroupType()">บันทึก</button></a>
                                                                          <a href="#"><button type="button"
                                                                              class="btn btn-danger  round btn-min-width mr-1 mb-1"
                                                                              id="type-error">ล้างคำสั่ง</button></a>
                                                                      </div>
                                                                  </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                  </section>
                  <!--/ Bootstrap 3 table -->
              </div>
          </div>
      </div>
  </section>

  <script src=" http://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous">
  </script>
  <script src="../app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
  <script type="text/javascript">
      $(document).ready(function() {
          console.log("ready");
          change_autorefreshdiv();
      });

      function change_autorefreshdiv() {
          // $('#prefixPage').addClass('active');
      }

      function toggle(source) {
          var checkboxes = document.querySelectorAll('.checkAll');
          for (var i = 0; i < checkboxes.length; i++) {
              if (checkboxes[i] != source)
                  checkboxes[i].checked = source.checked;
          }
      }
  </script>

  <!-- select2 -->
  <script src="../../app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
  <script src="../../app-assets/js/scripts/forms/select/form-select2.js" type="text/javascript"></script>
  <!-- select2 -->

  <!-- footer -->
  <?php include '../include/footer.php'; ?>



  </div>