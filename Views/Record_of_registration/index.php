  <!-- header -->
  <?php include '../include/header.php'; ?>

  <!-- menu -->
  <?php include '../include/menu.php'; ?>

  <style>
      /* ol > li > a {color:#222233;} */
      .toggle.ios,
      .toggle-on.ios,
      .toggle-off.ios {
          border-radius: 20rem;
      }

      .toggle.ios .toggle-handle {
          border-radius: 20rem;
      }
  </style>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <!-- select2 -->
  <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/forms/selects/select2.min.css">
  <section>
      <div class="app-content content">
          <div class="content-wrapper">
              <div class="content-header row">
                  <div class="content-header-left col-12 mb-2">
                      <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
                      <h3 class="content-header-title">บันทึกการแก้ไขข้อมูล</h3>
                  </div>
              </div>
              <div class="content-body">
                  <!-- Bootstrap 3 table -->
                  <section id="bootstrap3">
                      <div class="row">
                          <div class="col-12">
                              <div class="card">
                                  <div class="card-content collapse show">
                                      <div class="card-body card-dashboard">

                                          <nav aria-label="breadcrumb">
                                              <ol class="breadcrumb">
                                                  <li class="breadcrumb-item"><a href="../home/index.php">ระบบงานสัสดี</a></li>
                                                  <li class="breadcrumb-item active" aria-current="page">บันทึกการแก้ไขข้อมูล
                                                  </li>


                                              </ol>
                                          </nav>
                                          <div id="how-to" class="card">
                                              <!-- card -->
                                              <div class="card-header" style="background-color:#0f1733;padding:1.0rem 1.0rem">
                                                  <h6 class="card-title">
                                                      <font color="White">บันทึก / แก้ไข ข้อมูล
                                                  </h6>
                                                  </font>
                                                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                                  <div class="heading-elements">
                                                      <ul class="list-inline mb-0">
                                                          <li><a data-action="collapse"><i class="ft-minus"></i></a>
                                                          </li>
                                                      </ul>
                                                  </div>
                                              </div>
                                          </div> <!-- card -->

                           <div class="container">

                                              <div class="card-content">
                                                  <div class="card-body">


                                                    <div class="tab-content px-1 pt-1">
                                                          <div role="tabpanel" class="tab-pane active" id="tab11" aria-expanded="true" aria-labelledby="base-tab11">

                                                              <div class="card collapse-icon accordion-icon-rotate active">
                                                                  <div id="headingCollapse31" class="card-header bg-success">
                                                                      <a data-toggle="collapse" href="#collapse31" aria-expanded="true" aria-controls="collapse31" class="card-title lead white">
                                                                          <h6 style="margin-left:5px;">ส่วนที่1 บันทึก/แก้ไข การเปลี่ยนชื่อ/สกุล</h6>
                                                                      </a>
                                                                  </div>



                                                                  <div class="tab-content px-1 pt-1">
                                                                                  <div role="tabpanel55"
                                                                                      class="tab-pane active"
                                                                                      id="tab1155" aria-expanded="true"
                                                                                      aria-labelledby="base-tab1155">

                                                                                      <div
                                                                                          class="card collapse-icon accordion-icon-rotate active">
                                                                                          
                                                                                          <div id="collapse55"
                                                                                              role="tabpanel55"
                                                                                              aria-labelledby="headingCollapse55"
                                                                                              class="card-collapse collapse show"
                                                                                              aria-expanded="true">
                                                                                              <div class="card-content">
                                                                                                  <div
                                                                                                      class="card-body">
                                                                                                      <div
                                                                                                          class="row match-height">


                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      ชื่อ - สกุล
                                                                                                                      :
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">

                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>

                                                                                                          <div
                                                                                                              class=" col-md-6">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      ประเภท
                                                                                                                      :
                                                                                                                      <select
                                                                                                          class="select2 form-control"
                                                                                                          style="width: 100%;">
                                                                                                          <optgroup
                                                                                                              label="สัญญาบัตร">
                                                                                                              <option
                                                                                                                  value="AK">
                                                                                                                  เลือก
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  พลอากาศเอก/พล.อ.อ.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  พลอากาศโท/พล.อ.ท.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  พลอากาศตรี/พล.อ.ต.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  พลอากาศจัตวา/พล.อ.จ.(ยกเลิกแล้ว)
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  นาวาอากาศเอก/น.อ.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  นาวาอากาศโท/น.ท.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  นาวาอากาศตรี/น.ต.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  เรืออากาศเอก/ร.อ.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  เรืออากาศโท/ร.ท.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  เรืออากาศตรี/ร.ต.
                                                                                                              </option>
                                                                                                          </optgroup>
                                                                                                          <optgroup
                                                                                                              label="ชั้นประทวน">
                                                                                                              <option
                                                                                                                  value="CA">
                                                                                                                  พันจ่าอากาศเอก
                                                                                                                  พิเศษ/พ.อ.อ.(พ.)
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="NV">
                                                                                                                  พันจ่าอากาศเอก/พ.อ.อ.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="OR">
                                                                                                                  พันจ่าอากาศโท/พ.อ.ท
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="WA">
                                                                                                                  พันจ่าอากาศตรี/พ.อ.ต
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="NV">
                                                                                                                  จ่าอากาศเอก/จ.อ.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="OR">
                                                                                                                  จ่าอากาศโท/จ.ท.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="WA">
                                                                                                                  จ่าอากาศตรี/จ.ต.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="WA">
                                                                                                                  พลทหาร/พลฯ
                                                                                                              </option>
                                                                                                          </optgroup>
                                                                                                      </select>

                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>


                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      ประเภทกำลังประทวน
                                                                                                                      :
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">

                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>

                                                                                                          <div
                                                                                                              class=" col-md-6">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      ยศ
                                                                                                                      :
                                                                                                                      <select
                                                                                                          class="select2 form-control"
                                                                                                          style="width: 100%;">
                                                                                                          <optgroup
                                                                                                              label="สัญญาบัตร">
                                                                                                              <option
                                                                                                                  value="AK">
                                                                                                                  เลือก
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  พลอากาศเอก/พล.อ.อ.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  พลอากาศโท/พล.อ.ท.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  พลอากาศตรี/พล.อ.ต.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  พลอากาศจัตวา/พล.อ.จ.(ยกเลิกแล้ว)
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  นาวาอากาศเอก/น.อ.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  นาวาอากาศโท/น.ท.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  นาวาอากาศตรี/น.ต.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  เรืออากาศเอก/ร.อ.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  เรืออากาศโท/ร.ท.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="HI">
                                                                                                                  เรืออากาศตรี/ร.ต.
                                                                                                              </option>
                                                                                                          </optgroup>
                                                                                                          <optgroup
                                                                                                              label="ชั้นประทวน">
                                                                                                              <option
                                                                                                                  value="CA">
                                                                                                                  พันจ่าอากาศเอก
                                                                                                                  พิเศษ/พ.อ.อ.(พ.)
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="NV">
                                                                                                                  พันจ่าอากาศเอก/พ.อ.อ.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="OR">
                                                                                                                  พันจ่าอากาศโท/พ.อ.ท
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="WA">
                                                                                                                  พันจ่าอากาศตรี/พ.อ.ต
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="NV">
                                                                                                                  จ่าอากาศเอก/จ.อ.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="OR">
                                                                                                                  จ่าอากาศโท/จ.ท.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="WA">
                                                                                                                  จ่าอากาศตรี/จ.ต.
                                                                                                              </option>
                                                                                                              <option
                                                                                                                  value="WA">
                                                                                                                  พลทหาร/พลฯ
                                                                                                              </option>
                                                                                                          </optgroup>
                                                                                                      </select>

                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>

                                                                                                          
                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      เลขประจำตัวประชานชน
                                                                                                                      :
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">

                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>

                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">

                                                                                                                  <div
                                                                                                                      class="input-group">
                                                                                                                     วัน/เดือน/ปี เกิด 
                                                                                                                     
                                                                                                                    
                                                                                                                      
                                                                                                                      <input
                                                                                                                          type="text"
                                                                                                                          class="form-control pickadate-disable-dates"
                                                                                                                          placeholder="25 กรกฏาคม 2562"
                                                                                                                          aria-describedby="button-addon4">
                                                                                                                      <div
                                                                                                                          class="input-group-append">
                                                                                                                          <button
                                                                                                                              class="btn btn-primary"
                                                                                                                              type="button"
                                                                                                                              style=" padding-bottom: 1px; padding-top: 1px;"><i
                                                                                                                                  class="la la-calendar-o"></i></button>
                                                                                                                      </div>
                                                                                                                  </div>

                                                                                                              </div>
                                                                                                          </div>

                                                                                                          <div
                                                                                                              class="col-md-3">
                                                                                                              <div
                                                                                                                  class="card-body ">
                                                                                                                 อายุ :
                                                                                                                  <input
                                                                                                                      class="input form-control"
                                                                                                                      style="width: 100%;"
                                                                                                                      placeholder=" ">
                                                                                                              </div>

                                                                                                          </div>
                                                                                                          <div
                                                                                                              class="col-md-3">
                                                                                                              <div
                                                                                                                  class="card-body ">
                                                                                                                  นับถือศาสนา :
                                                                                                                  <input
                                                                                                                      class="input form-control"
                                                                                                                      style="width: 100%;"
                                                                                                                      placeholder=" ">
                                                                                                              </div>

                                                                                                          </div>



                                                                                                      </div>
                                                                                                  </div>
                                                                                              </div>
                                                                                          </div>

                                                                                      </div>



                                                                                  </div>
                                                                              </div>

                                                              </div>
                                                          </div>
                                                    </div>

                                                    <div class="tab-content px-1 pt-1">
                                                          <div role="tabpanel" class="tab-pane active" id="tab11" aria-expanded="true" aria-labelledby="base-tab11">

                                                              <div class="card collapse-icon accordion-icon-rotate active">
                                                                  <div id="headingCollapse31" class="card-header bg-success">
                                                                      <a data-toggle="collapse" href="#collapse31" aria-expanded="true" aria-controls="collapse31" class="card-title lead white">
                                                                          <h6 style="margin-left:5px;">ส่วนที่2 ที่อยู่ตามทะเบียนบ้าน</h6>
                                                                      </a>
                                                                  </div>



                                                                  <div class="tab-content px-1 pt-1">
                                                                                  <div role="tabpanel55"
                                                                                      class="tab-pane active"
                                                                                      id="tab1155" aria-expanded="true"
                                                                                      aria-labelledby="base-tab1155">

                                                                                      <div
                                                                                          class="card collapse-icon accordion-icon-rotate active">
                                                                                          
                                                                                          <div id="collapse55"
                                                                                              role="tabpanel55"
                                                                                              aria-labelledby="headingCollapse55"
                                                                                              class="card-collapse collapse show"
                                                                                              aria-expanded="true">
                                                                                              <div class="card-content">
                                                                                                  <div
                                                                                                      class="card-body">
                                                                                                      <div
                                                                                                          class="row match-height">


                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      ที่อยู่เลขที่ :
                                                                                                                      :
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">

                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>

                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      ชื่อหมู่บ้าน / อาคาร :
                                                                                                                      :
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">

                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>

                                                                                                          <div
                                                                                                              class="col-md-3">
                                                                                                              <div
                                                                                                                  class="card-body ">
                                                                                                                  หมู่ :
                                                                                                                  <input
                                                                                                                      class="input form-control"
                                                                                                                      style="width: 100%;"
                                                                                                                      placeholder=" ">
                                                                                                              </div>

                                                                                                          </div>
                                                                                                          <div
                                                                                                              class="col-md-3">
                                                                                                              <div
                                                                                                                  class="card-body ">
                                                                                                                  ซอย :
                                                                                                                  <input
                                                                                                                      class="input form-control"
                                                                                                                      style="width: 100%;"
                                                                                                                      placeholder=" ">
                                                                                                              </div>
                                                                                                              </div>
                                                                                                         
                                                                                                         <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      ถนน :                                                                                                                      
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">                                                                                                                        
                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>
                                                                                                          <div
                                                                                                              class=" col-md-3">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      จังหวัด
                                                                                                                      :
                                                                                                                      <select
                                                                                                          class="select2 form-control"
                                                                                                          style="width: 100%;">
                                                                                                                <optgroup
                                                                                                                    label="จังหวัด">
                                                                                                                    <option
                                                                                                                        value="AK">
                                                                                                                        เลือก
                                                                                                                    </option>
                                                                                                                    <option value="HI">กรุงเทพมหานคร</option>
                                                                                                                        <option value="HI">กระบี่ </option>
                                                                                                                        <option value="HI">กาญจนบุรี </option>
                                                                                                                        <option value="HI">กาฬสินธุ์ </option>
                                                                                                                        <option value="HI">กำแพงเพชร </option>
                                                                                                                        <option value="HI">ขอนแก่น</option>
                                                                                                                        <option value="HI">จันทบุรี</option>
                                                                                                                        <option value="HI">ฉะเชิงเทรา </option>
                                                                                                                        <option value="HI">ชัยนาท </option>
                                                                                                                        <option value="HI">ชัยภูมิ </option>
                                                                                                                        <option value="HI">ชุมพร </option>
                                                                                                                        <option value="HI">ชลบุรี </option>
                                                                                                                        <option value="HI">เชียงใหม่ </option>
                                                                                                                        <option value="HI">เชียงราย </option>
                                                                                                                        <option value="HI">ตรัง </option>
                                                                                                                        <option value="HI">ตราด </option>
                                                                                                                        <option value="HI">ตาก </option>
                                                                                                                        <option value="HI">นครนายก </option>
                                                                                                                        <option value="HI">นครปฐม </option>
                                                                                                                        <option value="HI">นครพนม </option>
                                                                                                                        <option value="HI">นครราชสีมา </option>
                                                                                                                        <option value="HI">นครศรีธรรมราช </option>
                                                                                                                        <option value="HI">นครสวรรค์ </option>
                                                                                                                        <option value="HI">นราธิวาส </option>
                                                                                                                        <option value="HI">น่าน </option>
                                                                                                                        <option value="HI">นนทบุรี </option>
                                                                                                                        <option value="HI">บึงกาฬ</option>
                                                                                                                        <option value="HI">บุรีรัมย์</option>
                                                                                                                        <option value="HI">ประจวบคีรีขันธ์ </option>
                                                                                                                        <option value="HI">ปทุมธานี </option>
                                                                                                                        <option value="HI">ปราจีนบุรี </option>
                                                                                                                        <option value="HI">ปัตตานี </option>
                                                                                                                        <option value="HI">พะเยา </option>
                                                                                                                        <option value="HI">พระนครศรีอยุธยา </option>
                                                                                                                        <option value="HI">พังงา </option>
                                                                                                                        <option value="HI">พิจิตร </option>
                                                                                                                        <option value="HI">พิษณุโลก </option>
                                                                                                                        <option value="HI">เพชรบุรี </option>
                                                                                                                        <option value="HI">เพชรบูรณ์ </option>
                                                                                                                        <option value="HI">แพร่ </option>
                                                                                                                        <option value="HI">พัทลุง </option>
                                                                                                                        <option value="HI">ภูเก็ต </option>
                                                                                                                        <option value="HI">มหาสารคาม </option>
                                                                                                                        <option value="HI">มุกดาหาร </option>
                                                                                                                        <option value="HI">แม่ฮ่องสอน </option>
                                                                                                                        <option value="HI">ยโสธร </option>
                                                                                                                        <option value="HI">ยะลา </option>
                                                                                                                        <option value="HI">ร้อยเอ็ด </option>
                                                                                                                        <option value="HI">ระนอง </option>
                                                                                                                        <option value="HI">ระยอง </option>
                                                                                                                        <option value="HI">ราชบุรี</option>
                                                                                                                        <option value="HI">ลพบุรี </option>
                                                                                                                        <option value="HI">ลำปาง </option>
                                                                                                                        <option value="HI">ลำพูน </option>
                                                                                                                        <option value="HI">เลย </option>
                                                                                                                        <option value="HI">ศรีสะเกษ</option>
                                                                                                                        <option value="HI">สกลนคร</option>
                                                                                                                        <option value="HI">สงขลา </option>
                                                                                                                        <option value="HI">สมุทรสาคร </option>
                                                                                                                        <option value="HI">สมุทรปราการ </option>
                                                                                                                        <option value="HI">สมุทรสงคราม </option>
                                                                                                                        <option value="HI">สระแก้ว </option>
                                                                                                                        <option value="HI">สระบุรี </option>
                                                                                                                        <option value="HI">สิงห์บุรี </option>
                                                                                                                        <option value="HI">สุโขทัย </option>
                                                                                                                        <option value="HI">สุพรรณบุรี </option>
                                                                                                                        <option value="HI">สุราษฎร์ธานี </option>
                                                                                                                        <option value="HI">สุรินทร์ </option>
                                                                                                                        <option value="HI">สตูล </option>
                                                                                                                        <option value="HI">หนองคาย </option>
                                                                                                                        <option value="HI">หนองบัวลำภู </option>
                                                                                                                        <option value="HI">อำนาจเจริญ </option>
                                                                                                                        <option value="HI">อุดรธานี </option>
                                                                                                                        <option value="HI">อุตรดิตถ์ </option>
                                                                                                                        <option value="HI">อุทัยธานี </option>
                                                                                                                        <option value="HI">อุบลราชธานี</option>
                                                                                                                        <option value="HI">อ่างทอง </option>
                                                                                                                        <option value="HI">อื่นๆ</option>
                                                                                                                </optgroup>                                                                                                         
                                                                                                            </select>

                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>



                                                                                                          <div
                                                                                                              class=" col-md-3">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      อําเภอ/เขต :
                                                                                                                      <select
                                                                                                          class="select2 form-control"
                                                                                                          style="width: 100%;">
                                                                                                                <optgroup
                                                                                                                    label="อําเภอ/เขต">
                                                                                                                    <option
                                                                                                                        value="AK">
                                                                                                                        เลือก
                                                                                                                    </option>
                                                                                                                    <option value="HI">กรุงเทพมหานคร</option>
                                                                                                                        <option value="HI">กระบี่ </option>
                                                                                                                        <option value="HI">กาญจนบุรี </option>
                                                                                                                        <option value="HI">กาฬสินธุ์ </option>
                                                                                                                        <option value="HI">กำแพงเพชร </option>
                                                                                                                        <option value="HI">ขอนแก่น</option>
                                                                                                                        <option value="HI">จันทบุรี</option>
                                                                                                                        <option value="HI">ฉะเชิงเทรา </option>
                                                                                                                        <option value="HI">ชัยนาท </option>
                                                                                                                        <option value="HI">ชัยภูมิ </option>                                                                                                                        
                                                                                                                </optgroup>                                                                                                         
                                                                                                            </select>

                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>

                                                                                                          <div
                                                                                                              class=" col-md-3">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      รหัสไปรษณีย์ :
                                                                                                                      <select
                                                                                                          class="select2 form-control"
                                                                                                          style="width: 100%;">
                                                                                                                <optgroup
                                                                                                                    label="รหัสไปรษณีย์">
                                                                                                                    <option
                                                                                                                        value="AK">
                                                                                                                        เลือก
                                                                                                                    </option>
                                                                                                                    <option value="HI">รหัสไปรษณีย์</option>
                                                                                                                        <option value="HI">รหัสไปรษณีย์ </option>
                                                                                                                        <option value="HI">รหัสไปรษณีย์ </option>
                                                                                                                        <option value="HI">รหัสไปรษณีย์ </option>
                                                                                                                        <option value="HI">รหัสไปรษณีย์ </option>
                                                                                                                        <option value="HI">รหัสไปรษณีย์</option>                                                                                                                   
                                                                                                                </optgroup>                                                                                                         
                                                                                                            </select>

                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>

                                                                                                          <div
                                                                                                              class=" col-md-3">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      โทรศัพท์ :
                                                                                                                      <select
                                                                                                          class="select2 form-control"
                                                                                                          style="width: 100%;">
                                                                                                                <optgroup
                                                                                                                    label="โทรศัพท์ ">
                                                                                                                    <option
                                                                                                                        value="AK">
                                                                                                                        เลือก
                                                                                                                    </option>
                                                                                                                    <option value="HI">0589635551</option>
                                                                                                                        <option value="HI">0524789663 </option>
                                                                                                                        <option value="HI">0863785224 </option>                                                                                                                                                                                                                                              
                                                                                                                </optgroup>                                                                                                         
                                                                                                            </select>

                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>
                                                                                                           <!-- --------------- --> 
                                                                                                           <div
                                                                                                              class="col-md-3">
                                                                                                              <div
                                                                                                                  class="card-body ">
                                                                                                                  โทรศัพท์ :
                                                                                                                  <input
                                                                                                                      class="input form-control"
                                                                                                                      style="width: 100%;"
                                                                                                                      placeholder=" ">
                                                                                                              </div>

                                                                                                          </div>
                                                                                                           <div
                                                                                                              class=" col-md-3">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      ประเทศ :
                                                                                                                      <select
                                                                                                          class="select2 form-control"
                                                                                                          style="width: 100%;">
                                                                                                                <optgroup
                                                                                                                    label="ประเทศ ">
                                                                                                                    <option
                                                                                                                        value="AK">
                                                                                                                        เลือก
                                                                                                                    </option>
                                                                                                                    <option value="HI">ไทย</option>
                                                                                                                        <option value="HI">ลาว </option>
                                                                                                                        <option value="HI">พม่า </option>                                                                                                                                                                                                                                              
                                                                                                                </optgroup>                                                                                                         
                                                                                                            </select>

                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>

                                                                                                          <!-- --------------- -->                                                                                                 
                                                                                                        </div>
                                                                                                  </div>
                                                                                              </div>
                                                                                          </div>

                                                                                      </div>



                                                                                  </div>
                                                                              </div>

                                                              </div>
                                                          </div>
                                                    </div>

                                                    <div class="tab-content px-1 pt-1">
                                                          <div role="tabpanel" class="tab-pane active" id="tab11" aria-expanded="true" aria-labelledby="base-tab11">

                                                              <div class="card collapse-icon accordion-icon-rotate active">
                                                                  <div id="headingCollapse31" class="card-header bg-success">
                                                                      <a data-toggle="collapse" href="#collapse31" aria-expanded="true" aria-controls="collapse31" class="card-title lead white">
                                                                          <h6 style="margin-left:5px;">ส่วนที่3 ขึ้นทะเบียน</h6>
                                                                      </a>
                                                                  </div>
                                                                  <div class="tab-content px-1 pt-1">
                                                                                  <div role="tabpanel55"
                                                                                      class="tab-pane active"
                                                                                      id="tab1155" aria-expanded="true"
                                                                                      aria-labelledby="base-tab1155">

                                                                                      <div
                                                                                          class="card collapse-icon accordion-icon-rotate active">
                                                                                          
                                                                                          <div id="collapse55"
                                                                                              role="tabpanel55"
                                                                                              aria-labelledby="headingCollapse55"
                                                                                              class="card-collapse collapse show"
                                                                                              aria-expanded="true">
                                                                                              <div class="card-content">
                                                                                                  <div
                                                                                                      class="card-body">
                                                                                                      <div
                                                                                                          class="row match-height">
                                                                                                          <!-- ---------เครื่องหมาย--------- -->  
                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      เครื่องหมาย :                                                                                                                      
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">

                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>
                                                                                                           <!-- ---------หมายเลขประจำตัว--------- -->  
                                                                                                          <div 
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">                                                                                                                      
                                                                                                                      หมายเลขประจำตัว :
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">
                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div> 
                                                                                                             <!-- ---------วันขึ้นทะเบียนประจำการ--------- -->  
                                                                                                             <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="input-group">
                                                                                                                      วันขึ้นทะเบียนประจำการ :
                                                                                                                      <input
                                                                                                                          type="text"
                                                                                                                          class="form-control pickadate-disable-dates"
                                                                                                                          placeholder="25 กรกฏาคม 2562"
                                                                                                                          aria-describedby="button-addon4">
                                                                                                                      <div
                                                                                                                          class="input-group-append">
                                                                                                                          <button
                                                                                                                              class="btn btn-primary"
                                                                                                                              type="button"
                                                                                                                              style=" padding-bottom: 1px; padding-top: 1px;"><i
                                                                                                                                  class="la la-calendar-o"></i></button>
                                                                                                                      </div>
                                                                                                                  </div>
                                                                                                                </div>
                                                                                                           </div>
                                                                                                           <!-- --------วันล้วง---------- -->     
                                                                                                           <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="input-group">
                                                                                                                      วัน/เดือน/ปี ล้วง : 
                                                                                                                      <input
                                                                                                                          type="text"
                                                                                                                          class="form-control pickadate-disable-dates"
                                                                                                                          placeholder="25 กรกฏาคม 2562"
                                                                                                                          aria-describedby="button-addon4">
                                                                                                                      <div
                                                                                                                          class="input-group-append">
                                                                                                                          <button
                                                                                                                              class="btn btn-primary"
                                                                                                                              type="button"
                                                                                                                              style=" padding-bottom: 1px; padding-top: 1px;"><i
                                                                                                                                  class="la la-calendar-o"></i></button>
                                                                                                                      </div>
                                                                                                                  </div>
                                                                                                                </div>
                                                                                                           </div>
                                                                                                           <!-- --------ส่วนสูง---------- -->
                                                                                                           <div
                                                                                                              class="col-lg-3 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      ส่วนสูง :                                                                                                                      
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">

                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>   
                                                                                                          <!-- ----------รอบอก-------- -->
                                                                                                          <div
                                                                                                              class="col-lg-3 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      รอบอก :                                                                                                                   
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">
                                                                                                                          
                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>                                                                                                           

                                                                                                          <!-- ---------วุฒิการศึกษา--------- -->
                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      วุฒิการศึกษา :                                                                                                                
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">
                                                                                                                          
                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>       

                                                                                                          <!-- ------------------ -->                                                                                                

                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                        </div>
                                                                    </div>
                                                              </div>
                                                          </div>
                                                    </div>


                                                    <div class="tab-content px-1 pt-1">
                                                          <div role="tabpanel" class="tab-pane active" id="tab11" aria-expanded="true" aria-labelledby="base-tab11">

                                                              <div class="card collapse-icon accordion-icon-rotate active">
                                                                  <div id="headingCollapse31" class="card-header bg-success">
                                                                      <a data-toggle="collapse" href="#collapse31" aria-expanded="true" aria-controls="collapse31" class="card-title lead white">
                                                                          <h6 style="margin-left:5px;">ส่วนที่4 นำปลด</h6>
                                                                      </a>
                                                                  </div>



                                                                  <div class="tab-content px-1 pt-1">
                                                                                  <div role="tabpanel55"
                                                                                      class="tab-pane active"
                                                                                      id="tab1155" aria-expanded="true"
                                                                                      aria-labelledby="base-tab1155">

                                                                                      <div
                                                                                          class="card collapse-icon accordion-icon-rotate active">
                                                                                          
                                                                                          <div id="collapse55"
                                                                                              role="tabpanel55"
                                                                                              aria-labelledby="headingCollapse55"
                                                                                              class="card-collapse collapse show"
                                                                                              aria-expanded="true">
                                                                                              <div class="card-content">
                                                                                                  <div
                                                                                                      class="card-body">
                                                                                                      <div
                                                                                                          class="row match-height">
                                                                                                          <!-- ---------วันที่ปลด--------- -->
                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="input-group">
                                                                                                                      วันที่ปลด :
                                                                                                                      <input
                                                                                                                          type="text"
                                                                                                                          class="form-control pickadate-disable-dates"
                                                                                                                          placeholder="25 กรกฏาคม 2562"
                                                                                                                          aria-describedby="button-addon4">
                                                                                                                      <div
                                                                                                                          class="input-group-append">
                                                                                                                          <button
                                                                                                                              class="btn btn-primary"
                                                                                                                              type="button"
                                                                                                                              style=" padding-bottom: 1px; padding-top: 1px;"><i
                                                                                                                                  class="la la-calendar-o"></i></button>
                                                                                                                      </div>
                                                                                                                  </div>
                                                                                                                </div>
                                                                                                           </div>
                                                                                                          <!-- ---------หมายเลขประจำตัว--------- -->
                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      หมายเลขประจำตัว :                                                                                                              
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">
                                                                                                                          
                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>
                                                                                                          <!-- ---------ตามหางว่าวได้ที่ :--------- -->
                                                                                                          <div
                                                                                                              class="col-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">                                                                                                                      
                                                                                                                      ตามหางว่าวได้ที่ :
                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>      
                                                                                                          <!-- ---------ตามหางว่าวได้ที่--------- --> 
                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">                                                                                                                      
                                                                                                                      คำสั่ง :                                                                                                          
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">
                                                                                                                          
                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>         
                                                                                                          
                                                                                                          <!-- --------เลขที่คำสั่ง :---------- -->  
                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">                                                                                                                      
                                                                                                                      เลขที่คำสั่ง :                                                                                                        
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">
                                                                                                                          
                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>       
                                                                                                          <!-- --------ลง :---------- --> 
                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="input-group">
                                                                                                                      วันที่ ลง  :
                                                                                                                      <input
                                                                                                                          type="text"
                                                                                                                          class="form-control pickadate-disable-dates"
                                                                                                                          placeholder="25 กรกฏาคม 2562"
                                                                                                                          aria-describedby="button-addon4">
                                                                                                                      <div
                                                                                                                          class="input-group-append">
                                                                                                                          <button
                                                                                                                              class="btn btn-primary"
                                                                                                                              type="button"
                                                                                                                              style=" padding-bottom: 1px; padding-top: 1px;"><i
                                                                                                                                  class="la la-calendar-o"></i></button>
                                                                                                                      </div>
                                                                                                                  </div>
                                                                                                                </div>
                                                                                                           </div>
                                                                                                          <!-- --------วันที่มีผล :---------- -->
                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="input-group">
                                                                                                                      วันที่มีผล :
                                                                                                                      <input
                                                                                                                          type="text"
                                                                                                                          class="form-control pickadate-disable-dates"
                                                                                                                          placeholder="25 กรกฏาคม 2562"
                                                                                                                          aria-describedby="button-addon4">
                                                                                                                      <div
                                                                                                                          class="input-group-append">
                                                                                                                          <button
                                                                                                                              class="btn btn-primary"
                                                                                                                              type="button"
                                                                                                                              style=" padding-bottom: 1px; padding-top: 1px;"><i
                                                                                                                                  class="la la-calendar-o"></i></button>
                                                                                                                      </div>
                                                                                                                  </div>
                                                                                                                </div>
                                                                                                           </div>
                                                                                                          <!-- ---------ลำดับที่ในคำสั่ง :--------- -->
                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">                                                                                                                      
                                                                                                                      ลำดับที่ในคำสั่ง :                                                                                                        
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">
                                                                                                                          
                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>        
                                                                                                          <!-- ----------สาเหตุ :-------- --> 
                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">                                                                                                                      
                                                                                                                      สาเหตุ :                                                                                                       
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">
                                                                                                                          
                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>   
                                                                                                          <!-- --------ประเภทกองหนุน :---------- --> 
                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">                                                                                                                      
                                                                                                                      ประเภทกองหนุน :                                                                                                       
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">
                                                                                                                          
                                                                                                                  </div>
                                                                                                              </div> 
                                                                                                              </div>    
                                                                                                          <!-- ---------ชั้นกองหนุน :--------- -->
                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">                                                                                                                      
                                                                                                                      ชั้นกองหนุน :                                                                                                       
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">
                                                                                                                          
                                                                                                                  </div>
                                                                                                              </div>
                                                                                                              </div>    
                                                                                                          <!-- ---------ชั้นกองหนุน :--------- -->      
                                                                                                          
                                                                                                      </div>
                                                                                                  </div>
                                                                                              </div>
                                                                                          </div>

                                                                                      </div>



                                                                                  </div>
                                                                              </div>

                                                              </div>
                                                          </div>
                                                    </div>

                                                    <div class="tab-content px-1 pt-1">
                                                          <div role="tabpanel" class="tab-pane active" id="tab11" aria-expanded="true" aria-labelledby="base-tab11">

                                                              <div class="card collapse-icon accordion-icon-rotate active">
                                                                  <div id="headingCollapse31" class="card-header bg-success">
                                                                      <a data-toggle="collapse" href="#collapse31" aria-expanded="true" aria-controls="collapse31" class="card-title lead white">
                                                                          <h6 style="margin-left:5px;">ส่วนที่5 บันทึก/แก้ไข การเปลี่ยนชื่อ/สกุล</h6>
                                                                      </a>
                                                                  </div>



                                                                  <div class="tab-content px-1 pt-1">
                                                                                  <div role="tabpanel55"
                                                                                      class="tab-pane active"
                                                                                      id="tab1155" aria-expanded="true"
                                                                                      aria-labelledby="base-tab1155">

                                                                                      <div
                                                                                          class="card collapse-icon accordion-icon-rotate active">
                                                                                          
                                                                                          <div id="collapse55"
                                                                                              role="tabpanel55"
                                                                                              aria-labelledby="headingCollapse55"
                                                                                              class="card-collapse collapse show"
                                                                                              aria-expanded="true">
                                                                                              <div class="card-content">
                                                                                                  <div
                                                                                                      class="card-body">
                                                                                                      <div
                                                                                                          class="row match-height">


                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">
                                                                                                                  <div
                                                                                                                      class="card-body ">
                                                                                                                      ครั้งที่
                                                                                                                      :
                                                                                                                      <input
                                                                                                                          class="input form-control"
                                                                                                                          style="width: 100%;"
                                                                                                                          placeholder=" ">

                                                                                                                  </div>
                                                                                                              </div>
                                                                                                          </div>


                                                                                                          <div
                                                                                                              class="col-lg-6 col-md-12">
                                                                                                              <div
                                                                                                                  class="card-block">

                                                                                                                  <div
                                                                                                                      class="input-group">
                                                                                                                      วันที่เปลี่ยนแปลงข้อมูล
                                                                                                                      <input
                                                                                                                          type="text"
                                                                                                                          class="form-control pickadate-disable-dates"
                                                                                                                          placeholder="25 กรกฏาคม 2562"
                                                                                                                          aria-describedby="button-addon4">
                                                                                                                      <div
                                                                                                                          class="input-group-append">
                                                                                                                          <button
                                                                                                                              class="btn btn-primary"
                                                                                                                              type="button"
                                                                                                                              style=" padding-bottom: 1px; padding-top: 1px;"><i
                                                                                                                                  class="la la-calendar-o"></i></button>
                                                                                                                      </div>
                                                                                                                  </div>

                                                                                                                </div>
                                                                                                           </div>
                                                                                                           
                                                                                                           <div
                                                                                                    class="col-md-12">
                                                                                                    <div
                                                                                                        class="card-body ">
                                                                                                        <!-- Basic Checkbox start -->
                                                                                                            <label>
                                                                                                                 <input type="checkbox" value=""> แก้ไขหมายเลขประจำตัว
                                                                                                            </label>
                                                                                                        <!-- Basic Checkbox end -->
                                                                                                        <div class="row">
                                                                                                          <div class="col-md-6">
                                                                                                              <div
                                                                                                                  class="card-body ">
                                                                                                                  หมายเลขประจำตัวทหาร (เดิม) :
                                                                                                                  <input
                                                                                                                      class="input form-control"
                                                                                                                      style="width: 100%;"
                                                                                                                      placeholder=" ">
                                                                                                              </div>
                                                                                                          </div>

                                                                                                          <div class="col-md-6">
                                                                                                              <div
                                                                                                                  class="card-body ">
                                                                                                                  หมายเลขประจำตัวทหาร (ใหม่) :
                                                                                                                  <input
                                                                                                                      class="input form-control"
                                                                                                                      style="width: 100%;"
                                                                                                                      placeholder=" ">
                                                                                                              </div>

                                                                                                          </div>
                                                                                                        </div>
                                                                                                    </div>                                                                                                    
                                                                                                </div>


                                                                                                <div class="col-md-12 col-sm-12">
                                                                                                    <div class="card-body"
                                                                                                        id="File_type">
                                                                                                        <!-- Basic Checkbox start -->
                                                                                                                <label>
                                                                                                                <input type="checkbox" value=""> แก้ไขชื่อ-สกุล
                                                                                                                </label>
                                                                                                            <!-- Basic Checkbox end -->
                                                                                                        <div class="card-content">
                                                                                                            <div class="card-body">
                                                                                                                <div
                                                                                                                    class="d-inline-block custom-control custom-radio mr-1">
                                                                                                                    <input
                                                                                                                        type="radio"
                                                                                                                        class="custom-control-input"
                                                                                                                        name="colorRadio"
                                                                                                                        id="radio3">
                                                                                                                    <label
                                                                                                                        class="custom-control-label"
                                                                                                                        for="radio3">บุตร</label>
                                                                                                                </div>
                                                                                                                <div
                                                                                                                    class="d-inline-block custom-control custom-radio mr-1">
                                                                                                                    <input
                                                                                                                        type="radio"
                                                                                                                        class="custom-control-input"
                                                                                                                        name="colorRadio"
                                                                                                                        id="radio4"
                                                                                                                        checked>
                                                                                                                    <label
                                                                                                                        class="custom-control-label"
                                                                                                                        for="radio4"
                                                                                                                        checked>รับรองบุตร</label>
                                                                                                                </div>
                                                                                                                <div
                                                                                                                    class="d-inline-block custom-control custom-radio mr-1">
                                                                                                                    <input
                                                                                                                        type="radio"
                                                                                                                        class="custom-control-input"
                                                                                                                        name="colorRadio"
                                                                                                                        id="radio5"
                                                                                                                        checked>
                                                                                                                    <label
                                                                                                                        class="custom-control-label"
                                                                                                                        for="radio5"
                                                                                                                        checked>บุตรบุญธรรม</label>
                                                                                                                </div>

                                                                                                                <div class="row">
                                                                                                          <div class="col-md-6">
                                                                                                              <div
                                                                                                                  class="card-body ">
                                                                                                                  ชื่อ (เดิม) :
                                                                                                                  <input
                                                                                                                      class="input form-control"
                                                                                                                      style="width: 100%;"
                                                                                                                      placeholder=" ">
                                                                                                              </div>
                                                                                                          </div>

                                                                                                          <div class="col-md-6">
                                                                                                              <div
                                                                                                                  class="card-body ">
                                                                                                                  สกุล (เดิม) :
                                                                                                                  <input
                                                                                                                      class="input form-control"
                                                                                                                      style="width: 100%;"
                                                                                                                      placeholder=" ">
                                                                                                              </div>
                                                                                                          </div>

                                                                                                          <div class="col-md-6">
                                                                                                              <div
                                                                                                                  class="card-body ">
                                                                                                                  ชื่อ (ใหม่) :
                                                                                                                  <input
                                                                                                                      class="input form-control"
                                                                                                                      style="width: 100%;"
                                                                                                                      placeholder=" ">
                                                                                                              </div>
                                                                                                          </div>

                                                                                                          <div class="col-md-6">
                                                                                                              <div
                                                                                                                  class="card-body ">
                                                                                                                  สกุล (ใหม่) :
                                                                                                                  <input
                                                                                                                      class="input form-control"
                                                                                                                      style="width: 100%;"
                                                                                                                      placeholder=" ">
                                                                                                              </div>

                                                                                                          </div>

                                                                                                        </div>                                                                                                                                                                                                                              
                                                                                                                
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>


                                                                                                <div
                                                                                                    class="col-md-12">
                                                                                                    <div
                                                                                                        class="card-body ">
                                                                                                        <!-- Basic Checkbox start -->
                                                                                                            <label>
                                                                                                                 <input type="checkbox" value=""> แก้ไขวันเดือนปีเกิด
                                                                                                            </label>
                                                                                                        <!-- Basic Checkbox end -->
                                                                                                        <div class="row">
                                                                                                        <div class="col-lg-6 col-md-12">
                                                                                                              <div class="card-block">
                                                                                                                  <div class="input-group">
                                                                                                                      วันที่เปลี่ยนแปลงข้อมูล
                                                                                                                      <input type="text" class="form-control pickadate-disable-dates" placeholder="25 กรกฏาคม 2562" aria-describedby="button-addon4">
                                                                                                                      <div class="input-group-append">
                                                                                                                          <button class="btn btn-primary" type="button" style=" padding-bottom: 1px; padding-top: 1px;"><i class="la la-calendar-o"></i></button>
                                                                                                                      </div>
                                                                                                                  </div>
                                                                                                                </div>
                                                                                                           </div>

                                                                                                           <div class="col-lg-6 col-md-12">
                                                                                                              <div class="card-block">
                                                                                                                  <div class="input-group">
                                                                                                                      วันที่เปลี่ยนแปลงข้อมูล
                                                                                                                      <input type="text" class="form-control pickadate-disable-dates" placeholder="25 กรกฏาคม 2562" aria-describedby="button-addon4">
                                                                                                                      <div class="input-group-append">
                                                                                                                          <button class="btn btn-primary" type="button" style=" padding-bottom: 1px; padding-top: 1px;"><i class="la la-calendar-o"></i></button>
                                                                                                                      </div>
                                                                                                                  </div>
                                                                                                                </div>
                                                                                                           </div>
                                                                                                        </div>

                                                                                                    </div>
                                                                                                </div>
                                                                                              



                                                                                                      </div>
                                                                                                  </div>
                                                                                              </div>
                                                                                          </div>

                                                                                      </div>



                                                                                  </div>
                                                                              </div>

                                                              </div>
                                                          </div>
                                                    </div>








                                                  </div>
                                              </div>
                                          </div>

                                          <div class="tab-content px-1 pt-1">
                                                                      <div class="form-actions center" align="center">
                                                                          <!-- <button type="button" class="btn btn-danger  round btn-min-width mr-1 mb-1" id="type-error">ยกเลิก</button>
                                <button type="button" class="btn btn-success  round btn-min-width mr-1 mb-1" id="confirm-text" onclick="insertOrganizationParts()">บันทึก</button> -->

                                                                          <a href="Record_evidence_of_changes.php"><button type="button"
                                                                              class="btn btn-success  round btn-min-width mr-1 mb-1"
                                                                              id="submit" name="submit"
                                                                              onclick="insertOrganizationGroupType()">บันทึก</button></a>
                                                                          <a href="#"><button type="button"
                                                                              class="btn btn-danger  round btn-min-width mr-1 mb-1"
                                                                              id="type-error">ยกเลิก</button></a>
                                                                      </div>
                                                                  </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                  </section>
                  <!--/ Bootstrap 3 table -->
              </div>
          </div>
      </div>
  </section>

  <script src=" http://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous">
  </script>
  <script src="../app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
  <script type="text/javascript">
      $(document).ready(function() {
          console.log("ready");
          change_autorefreshdiv();
      });

      function change_autorefreshdiv() {
          // $('#prefixPage').addClass('active');
      }

      function toggle(source) {
          var checkboxes = document.querySelectorAll('.checkAll');
          for (var i = 0; i < checkboxes.length; i++) {
              if (checkboxes[i] != source)
                  checkboxes[i].checked = source.checked;
          }
      }
  </script>

  <!-- select2 -->
  <script src="../../app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
  <script src="../../app-assets/js/scripts/forms/select/form-select2.js" type="text/javascript"></script>
  <!-- select2 -->

  <!-- footer -->
  <?php include '../include/footer.php'; ?>



  </div>