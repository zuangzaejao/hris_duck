  <!-- header -->
  <?php include '../include/header.php'; ?>
  <?php include '../../Model/Ducklab/func.php'; ?>
  <?php  
    //Recruitment and appointment
    $menu1 = "RECRNAPPOINT" ;
    $menu2 = "PlanManpowerReg" ;

  ?>
  <!-- menu -->
  <?php include '../include/menu.php'; ?>
  
  <section>
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-2">
            <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
            <h3 class="content-header-title">สมัครสอบ  </h3>
          </div>
        </div>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb"> 
            <li class="breadcrumb-item"><a href="index.php">หน้าแรก </a></li>  
            <li class="breadcrumb-item active" aria-current="page">รายการเปิดรับสมัครสอบ </li>
          </ol>
        </nav>
        <div class="content-body">
          <!-- Bootstrap 3 table -->
          <section id="bootstrap3">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-content collapse show">
                    <div class="card-body card-dashboard">
                      <p class="card-text"></p> 
                      <div class="row"> 
                           
                          <div class="col-md-3 col-sm-12 ">
                            <div class="form-group">  
                              <select  id="planManTypeId2" class="  form-control block  " >
                                <option value="21"> น.ประทวนทำหน้าที่ </option>
                                <option value="22">  น.ประทวน ป.ตรีปรับ </option>  
                              </select>
                            </div>
                            
                          </div>
                      </div>
                      <div class="row"> 
                        <div class="col-sm-12">
                          <div class="box_h1 card">
                            <div class="card-header card-head-inverse  ">
                              <h4 class="card-title text-white"> ค้นหา </h4> 
                              <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                              <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                  <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                                </ul>
                              </div>
                            </div>
                            <div class="card-content collapse show">
                              <div class="card-body">
                                <div class="row">
                                  <div class=" col-md-8  m-auto ">
                                  
                                    <form class="form">
                                      <div class="form-body"> 

                                        <div class="boxsearchplan1 hidden">
                                        <div class="row">
                                            <div class="col-md-3">
                                              <div class="form-group">
                                                <select id="" name="" class="form-control ">
                                                <option value="">เหล่า/จำพวก</option>
                                                  <option value=""> นักบิน </option>
                                                  <option value=""> ต้นหน </option>
                                                  <option value=""> อุตุ </option>
                                                </select>
                                              </div>
                                            </div>
                                            <div class="col-md-3">
                                              <div class="form-group">  
                                                  <select id="projectinput5" name="interested" class="form-control ">
                                                  <option value="">สังกัด</option>
                                                  <option value="">สลก.ทอ.</option>
                                                  <option value="">สบ.ทอ.</option> 
                                                  <option value="">กพ.ทอ.</option> 
                                                  <option value="">ขว.ทอ.</option> 
                                                  <option value="">ยก.ทอ.</option> 
                                                  <option value="">กบ.ทอ.</option> 
                                                  <option value="">กร.ทอ.</option>
                                                  </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                              <div class="form-group">  
                                                  <select id="projectinput5" name="interested" class="form-control ">
                                                  <option value="development">ตำแหน่ง</option>
                                                    <option value=""> จเรฝ่ายยุทธการและนิรภัยการบิน </option>
                                                    <option value=""> ผู้ช่วยจเรฝ่ายยุทธการและนิรภัยการบิน </option>
                                                    <option value="">จเรฝ่ายข่าว  </option>
                                                  </select>
                                                </div>
                                            </div>
                                          </div> 
                                          <div class="row">
                                            <div class="col-md-3">
                                              <div class="form-group">   
                                                <input type="text" id="" class="form-control " placeholder="เลขลชทอ." name="">
                                              </div>
                                            </div> 
                                            <div class="col-md-3">
                                              <div class="form-group"> 
                                                <select id="" name="" class="form-control ">
                                                  <option value="0" selected="" >อัตรา</option>
                                                  <option value="">ร.อ.</option>
                                                  <option value="">ร.ท.</option>
                                                  <option value="">ร.ต.</option>
                                                </select>
                                              </div>
                                            </div>
                                          </div>
                                        </div> <!-- ./boxsearchplan1 -->

                                        <div class="boxsearchplan2">
                                        <div class="row">
                                            <div class="col-md-6">
                                              <div class="form-group">
                                                <select id="" name="" class="form-control ">
                                                  <option value="">คุณวุฒิ</option>
                                                  <option value="">มัธยมศึกษาตอนต้น</option>
                                                  <option value="">มัธยมศึกษาตอนปลาย</option>
                                                  <option value="">ปวช.</option>
                                                  <option value="">ปวส.</option>
                                                  <option value="">ปริญญาตรี</option>
                                                  <option value="">ปริญญาโท</option>
                                                  <option value="">ปริญญาเอก</option>
                                                </select>
                                              </div>
                                            </div> 
                                            <div class="col-md-6">
                                              <div class="form-group">  
                                                  <select id="projectinput5" name="interested" class="form-control ">
                                                    <option value="development">ตำแหน่ง</option>
                                                    <option value=""> จเรฝ่ายยุทธการและนิรภัยการบิน </option>
                                                    <option value=""> ผู้ช่วยจเรฝ่ายยุทธการและนิรภัยการบิน </option>
                                                    <option value="">จเรฝ่ายข่าว  </option>
                                                  </select>
                                                </div>
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-3">
                                              <div class="form-group">
                                                <select id="" name="" class="form-control ">
                                                  <option value="">เหล่า/จำพวก</option>
                                                  <option value=""> นักบิน </option>
                                                  <option value=""> ต้นหน </option>
                                                  <option value=""> อุตุ </option>
                                                </select>
                                              </div>
                                            </div> 
                                            <div class="col-md-3">
                                              <div class="form-group">  
                                                <select id="" name="" class="form-control ">
                                                  <option value="">เพศ</option>
                                                  <option value="">ชาย</option>
                                                  <option value="">หญิง</option>
                                                </select>
                                              </div>
                                            </div>
                                            <div class="col-md-3">
                                              <div class="form-group">  
                                                <select id="" name="" class="form-control ">
                                                  <option value="">อัตรา</option>
                                                  <option value=""> นอ. </option>
                                                  <option value=""> นท. </option>
                                                  <option value=""> นต. </option>
                                                </select>
                                              </div>
                                            </div>
                                            <div class="col-md-3">
                                              <div class="form-group">  
                                                <select id="" name="" class="form-control ">
                                                  <option value="">สังกัด</option>
                                                  <option value="">สลก.ทอ.</option>
                                                  <option value="">สบ.ทอ.</option> 
                                                  <option value="">กพ.ทอ.</option> 
                                                  <option value="">ขว.ทอ.</option> 
                                                  <option value="">ยก.ทอ.</option> 
                                                  <option value="">กบ.ทอ.</option> 
                                                  <option value="">กร.ทอ.</option>
                                                </select>
                                              </div>
                                            </div>
                                            <div class="col-md-3">
                                              <div class="form-group">  
                                                <input type="text" id="" class="form-control " placeholder="เลขลชทอ.หน้าที่" name="">  
                                              </div>
                                            </div>
                                          </div>
                                        </div>  <!-- ./boxsearchplan2 -->
                                      
                                      </div> 

                                      <div class="text-center">
                                        <button type="button" class="btn round btn1 btncustom1 mr-1">
                                          <i class="fa fa-search"></i> ค้นหา
                                        </button>
                                        <button type="button" class="btn round btn1 btncustom1">
                                          <i class=" fa fa-repeat"></i> ล้างค่า
                                        </button>
                                      </div>

                                    </form>

                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div> 

                      <div class="row"> 
                        <div class="col-md-3 col-sm-12">
                          <div class="box1 txtdesc">
                              ข้อมูลรับสมัครจำนวน <span> 500 </span> อัตรา
                          </div>
                        </div> 
                      </div> 
                      <div class="box_tb1">
  
                        <table id="planManpowerTable2" class="table  tb1 "  >
                          <thead>
                            <tr>
                              <th>   </th>
                              <th> ลำดับที่ </th> 
                              <th> เหล่าทหาร/จำพวกทหาร </th>
                              <th > สังกัด</th>
                              <th> ตำแหน่ง </th> 
                              <th > ลชทอ.หน้าที่ </th> 
                              <th >อัตรา</th>  
                              <th >จำนวน</th> 
                            </tr>
                          </thead>
                          <tbody> 
                            <tr> 
                              <td class="text-center">  
                                <button type="button" class="btn round btn1 btncustom3 mr-1" data-toggle="modal" data-target="#modalConfirm" >
                                  สมัครสอบ
                                </button>
                              </td>
                              <td class="text-center"> 1 </td>
                              <td class="text-center"> กง. </td>
                              <td class="text-center"> กง.ทอ. </td>
                              <td class="text-left"> นายทหารรายงานและหลักฐาน แปนกรายงานและหลักฐาน กกง.กง.ทอ. </td>
                              <td class="text-center"> 6713 </td>
                              <td class="text-center"> ร.อ. </td> 
                              <td class="text-center"> 1 </td>
                            </tr> 
                            <tr> 
                              <td class="text-center">  
                                <button type="button" class="btn round btn-success btncustom3 mr-1" data-toggle="modal" data-target="#modalNotRegis" >
                                  สมัครแล้ว
                                </button>
                              </td>
                              <td class="text-center"> 2 </td>
                              <td class="text-center"> กง. </td>
                              <td class="text-center"> กง.ทอ. </td>
                              <td class="text-left"> นายทหารบัญชี แผนกการเงิน ชย.ทอ. </td>
                              <td class="text-center"> 6713 </td>
                              <td class="text-center"> ร.อ. </td> 
                              <td class="text-center"> 1 </td>
                            </tr>
                            <tr> 
                              <td class="text-center">  
                                <button type="button" class="btn round btn1 btncustom3 mr-1" data-toggle="modal" data-target="#modalConfirm" >
                                  สมัครสอบ
                                </button>
                              </td>
                              <td class="text-center"> 3 </td>
                              <td class="text-center"> กง. </td>
                              <td class="text-center"> บน.1. </td>
                              <td class="text-left"> ผู้ช่วยนายทหารการเงิน กองพันทหารอากาศโยธิน บน.1  </td>
                              <td class="text-center"> 6713 </td>
                              <td class="text-center"> ร.ท. </td> 
                              <td class="text-center"> 1 </td>
                            </tr>
                            <tr> 
                              <td class="text-center">  
                                <button type="button" class="btn round  btn-success btncustom3 mr-1" data-toggle="modal" data-target="#modalConfirm" >
                                  สมัครสอบ
                                </button>
                              </td>
                              <td class="text-center"> 4 </td>
                              <td class="text-center"> กง. </td>
                              <td class="text-center"> บน.21 </td>
                              <td class="text-left"> นายทหารบัญชี แผนกการเงิน บน.21   </td>
                              <td class="text-center"> 6713 </td>
                              <td class="text-center"> ร.อ. </td> 
                              <td class="text-center"> 1 </td>
                            </tr>
                            <tr> 
                              <td class="text-center">  
                                <button type="button" class="btn round btn1 btncustom3 mr-1" data-toggle="modal" data-target="#modalConfirm" >
                                  สมัครสอบ
                                </button>
                              </td>
                              <td class="text-center"> 5 </td>
                              <td class="text-center"> กง. </td>
                              <td class="text-center"> บน.21 </td>
                              <td class="text-left"> ผู้ช่วยนายทหารการเงิน กองพันทหารอากาศโยธิน บน.21   </td>
                              <td class="text-center"> 6713 </td>
                              <td class="text-center"> ร.ท. </td> 
                              <td class="text-center"> 1 </td>
                            </tr>
                            
                          </tbody>
                        </table>

                        <table id="planManpowerTable3" class="table table-bordered tb1 hidden  "  >
                          <thead>
                            <tr>
                              <th>   </th>
                              <th> ลำดับที่ </th>
                              <th style="width:32%;"> คุณวุฒิ</th>
                              <th> เหล่าทหาร/จาพวก </th>
                              <th> สังกัด </th>
                              <th  style="width:32%;"> ตำแหน่ง</th>
                              <th >ลชทอ.หน้าที่</th> 
                              <th >เพศ</th> 
                              <th >อัตรา</th>  
                              <th >จำนวน</th> 
                            </tr>
                          </thead>
                          <tbody> 
                            <tr> 
                             <td class="text-center">  
                                <button type="button" class="btn round btn1 btncustom3 mr-1" data-toggle="modal" data-target="#modalConfirm" >
                                  สมัครสอบ
                                </button>
                              </td>
                              <td  rowspan="2"  class="text-center"> 1 </td>
                              <td  rowspan="2" >   
                                ปริญญาตรี ในสาขาวิชาพยาบาลศาสตร์  ได้รับใบประกอบวิชาชีพทางการพยาบาลและการผดุงครรภ์
                              </td>
                              <td class="text-center"> พ. </td> 
                              <td class="text-center"> พอ. </td>
                              <td class="text-center"> รองผู้บังคับหมวดนักเรียนพยาบาลทหารอากาศ ฝ่ายปกครอง แผนกปกครอง วพอ.พอ. </td>
                              <td class="text-center"> 9713 </td> 
                              <td class="text-center"> ช </td>
                              <td class="text-center"> ร.ท. </td>   
                              <td class="text-center"> 1 </td>
                            </tr> 
                            <tr >   
                              <td class="text-center">  
                                <button type="button" class="btn round btn1 btncustom3 mr-1" data-toggle="modal" data-target="#modalNotRegis" >
                                  สมัครสอบ
                                </button>
                              </td>
                              <td class="text-center"> พ. </td> 
                              <td class="text-center"> พอ. </td>
                              <td class="text-center"> นายทหารสันทัดงาน กวห.รพ.ภูมิพลอดุลยเดชพอ. </td>
                              <td class="text-center"> 9713,9023 </td> 
                              <td class="text-center"> ช/ญ </td>
                              <td class="text-center"> ร.ท. </td>   
                              <td class="text-center"> 1 </td>
                            </tr>
                            
                            <tr> 
                              <td class="text-center">  
                                <button type="button" class="btn round  btn-success btncustom3 mr-1" data-toggle="modal" data-target="#modalConfirm" >
                                  สมัครแล้ว
                                </button>
                              </td>
                              <td  rowspan="5"  class="text-center"> 2</td>
                              <td  rowspan="5" >   
                              ปริญญาตรี ในสาขาวิชาการบัญชี หรือในสาขาวิชาใดสาขาวิชาหนึ่ง
                              ทางการบัญชี, ทางการธนาคารและการเงิน,
                              ทางการบัญชีการเงิน, ทางการบัญชีบริหาร,
                              ทางระบบสารสนเทศทางการบัญชี
                              </td>
                              <td class="text-center"> กง. </td> 
                              <td class="text-center"> อย. </td>
                              <td class="text-center"> นายทหารควบคุมการเบิกจ่าย แผนกการเงิน อย. </td>
                              <td class="text-center"> 6713 </td> 
                              <td class="text-center"> ช/ญ </td>
                              <td class="text-center"> ร.ท. </td>   
                              <td class="text-center"> 1 </td>
                            </tr> 
                            <tr >   
                              <td class="text-center">  
                                <button type="button" class="btn round btn1 btncustom3 mr-1" data-toggle="modal" data-target="#modalConfirm" >
                                  สมัครสอบ
                                </button>
                              </td>
                              <td class="text-center"> กง. </td> 
                              <td class="text-center"> สอ.ทอ. </td>
                              <td class="text-center"> นายทหารควบคุมการเบิกจ่าย แผนกการเงิน สอ.ทอ. </td>
                              <td class="text-center"> 6713 </td> 
                              <td class="text-center"> ช/ญ </td>
                              <td class="text-center"> ร.ท. </td>   
                              <td class="text-center"> 1 </td>
                            </tr>
                            <tr >   
                              <td class="text-center">  
                                <button type="button" class="btn round btn1 btncustom3 mr-1" data-toggle="modal" data-target="#modalConfirm" >
                                  สมัครสอบ
                                </button>
                              </td>
                              <td class="text-center"> ตน. </td> 
                              <td class="text-center"> สตน.ทอ. </td>
                              <td class="text-center"> นายทหารตรวจสอบ ชุดตรวจที่ ๒ กตจ.๑ กตส.๑ สตน.ทอ. </td>
                              <td class="text-center"> 6713 </td> 
                              <td class="text-center"> ช/ญ </td>
                              <td class="text-center"> ร.ท. </td>   
                              <td class="text-center"> 1 </td>
                            </tr>
                            <tr >   
                              <td class="text-center">  
                                <button type="button" class="btn round btn1 btncustom3 mr-1" data-toggle="modal" data-target="#modalConfirm" >
                                  สมัครสอบ
                                </button>
                              </td>
                              <td class="text-center"> ตน. </td> 
                              <td class="text-center"> สตน.ทอ. </td>
                              <td class="text-center"> นายทหารตรวจสอบ ชุดตรวจที่ ๒ กตจ.๒ กตส.๑ สตน.ทอ. </td>
                              <td class="text-center"> 6713 </td> 
                              <td class="text-center"> ช/ญ </td>
                              <td class="text-center"> ร.ท. </td>   
                              <td class="text-center"> 1 </td>
                            </tr>
                            <tr >   
                              <td class="text-center">  
                                <button type="button" class="btn round btn1 btncustom3 mr-1" data-toggle="modal" data-target="#modalConfirm" >
                                  สมัครสอบ
                                </button>
                              </td>
                              <td class="text-center"> ตน. </td> 
                              <td class="text-center"> สตน.ทอ. </td>
                              <td class="text-center"> นายทหารตรวจสอบ ชุดตรวจที่ ๒ กตจ.๒ กตส.๓ สตน.ทอ. </td>
                              <td class="text-center"> 6713 </td> 
                              <td class="text-center"> ช/ญ </td>
                              <td class="text-center"> ร.ท. </td>   
                              <td class="text-center"> 1 </td>
                            </tr> 

                            <tr> 
                              <td class="text-center">  
                                <button type="button" class="btn round btn1 btncustom3 mr-1" data-toggle="modal" data-target="#modalConfirm" >
                                  สมัครสอบ
                                </button>
                              </td>
                              <td  rowspan="2"  class="text-center"> 3 </td>
                              <td  rowspan="2" >   
                              ปริญญาตรี ในสาขาวิชาคอมพิวเตอร์ หรือในสาขาวิชาใดสาขาวิชาหนึ่ง ทางคอมพิวเตอร์, ทางวิทยาการคอมพิวเตอร์, ทางเทคโนโลยีสารสนเทศ
                              </td>
                              <td class="text-center"> ทสส. </td> 
                              <td class="text-center"> ชว.ทอ. </td>
                              <td class="text-center">  นายทหารเทคโนโลยีสารสนเทศและการสื่อสาร ขว.ทอ. </td>
                              <td class="text-center"> 2713 </td> 
                              <td class="text-center"> ช </td>
                              <td class="text-center"> ร.อ. </td>   
                              <td class="text-center"> 1 </td>
                            </tr> 
                            <tr > 
                              <td class="text-center">  
                                <button type="button" class="btn round btn1 btncustom3 mr-1" data-toggle="modal" data-target="#modalConfirm" >
                                  สมัครสอบ
                                </button>
                              </td>  
                              <td class="text-center"> ทสส. </td> 
                              <td class="text-center">  บน.4 </td>
                              <td class="text-center">  นายทหารเทคโนโลยีสารสนเทศและการสื่อสาร ฝ่ายเทคโนโลยีสารสนเทศและการสื่อสาร บก.บน.๔ </td>
                              <td class="text-center"> 2713 </td> 
                              <td class="text-center"> ช </td>
                              <td class="text-center"> ร.อ. </td>   
                              <td class="text-center"> 1 </td> 
                            </tr>


                            <tr> 
                              <td class="text-center">  
                                <button type="button" class="btn round btn1 btncustom3 mr-1" data-toggle="modal" data-target="#modalConfirm" >
                                  สมัครสอบ
                                </button>
                              </td>
                              <td  rowspan="5"  class="text-center"> 4</td>
                              <td  rowspan="5" >   
                              ปริญญาตรี ในสาขาวิชาการบัญชี หรือในสาขาวิชาใดสาขาวิชาหนึ่ง
                              ทางการบัญชี, ทางการธนาคารและการเงิน,
                              ทางการบัญชีการเงิน, ทางการบัญชีบริหาร,
                              ทางระบบสารสนเทศทางการบัญชี
                              </td>
                              <td class="text-center"> กง. </td> 
                              <td class="text-center"> อย. </td>
                              <td class="text-center"> นายทหารควบคุมการเบิกจ่าย แผนกการเงิน อย. </td>
                              <td class="text-center"> 6713 </td> 
                              <td class="text-center"> ช/ญ </td>
                              <td class="text-center"> ร.ท. </td>   
                              <td class="text-center"> 1 </td>
                            </tr> 
                            <tr >   
                              <td class="text-center">  
                                <button type="button" class="btn round btn1 btncustom3 mr-1" data-toggle="modal" data-target="#modalConfirm" >
                                  สมัครสอบ
                                </button>
                              </td>
                              <td class="text-center"> กง. </td> 
                              <td class="text-center"> สอ.ทอ. </td>
                              <td class="text-center"> นายทหารควบคุมการเบิกจ่าย แผนกการเงิน สอ.ทอ. </td>
                              <td class="text-center"> 6713 </td> 
                              <td class="text-center"> ช/ญ </td>
                              <td class="text-center"> ร.ท. </td>   
                              <td class="text-center"> 1 </td>
                            </tr>
                            <tr > 
                              <td class="text-center">  
                                <button type="button" class="btn round btn1 btncustom3 mr-1" data-toggle="modal" data-target="#modalConfirm" >
                                  สมัครสอบ
                                </button>
                              </td>  
                              <td class="text-center"> ตน. </td> 
                              <td class="text-center"> สตน.ทอ. </td>
                              <td class="text-center"> นายทหารตรวจสอบ ชุดตรวจที่ ๒ กตจ.๑ กตส.๑ สตน.ทอ. </td>
                              <td class="text-center"> 6713 </td> 
                              <td class="text-center"> ช/ญ </td>
                              <td class="text-center"> ร.ท. </td>   
                              <td class="text-center"> 1 </td>
                            </tr>
                            <tr >   
                              <td class="text-center">  
                                <button type="button" class="btn round btn1 btncustom3 mr-1">
                                  สมัครสอบ
                                </button>
                              </td>
                              <td class="text-center"> ตน. </td> 
                              <td class="text-center"> สตน.ทอ. </td>
                              <td class="text-center"> นายทหารตรวจสอบ ชุดตรวจที่ ๒ กตจ.๒ กตส.๑ สตน.ทอ. </td>
                              <td class="text-center"> 6713 </td> 
                              <td class="text-center"> ช/ญ </td>
                              <td class="text-center"> ร.ท. </td>   
                              <td class="text-center"> 1 </td>
                            </tr>
                            <tr >   
                              <td class="text-center">  
                                <button type="button" class="btn round btn1 btncustom3 mr-1">
                                  สมัครสอบ
                                </button>
                              </td>
                              <td class="text-center"> ตน. </td> 
                              <td class="text-center"> สตน.ทอ. </td>
                              <td class="text-center"> นายทหารตรวจสอบ ชุดตรวจที่ ๒ กตจ.๒ กตส.๓ สตน.ทอ. </td>
                              <td class="text-center"> 6713 </td> 
                              <td class="text-center"> ช/ญ </td>
                              <td class="text-center"> ร.ท. </td>   
                              <td class="text-center"> 1 </td>
                            </tr> 

                            <tr> 
                              <td class="text-center">  
                                <button type="button" class="btn round btn1 btncustom3 mr-1" data-toggle="modal" data-target="#modalNotRegis" >
                                  สมัครสอบ
                                </button>
                              </td>
                              <td   class="text-center"> 5 </td>
                              <td   >   
                              ปริญญาตรี ในสาขาวิชานิติศาสตร์และเป็นสมาชิกเนติบัณฑิตยสภา
                              </td>
                              <td class="text-center"> ธน. </td> 
                              <td class="text-center"> สธน.ทอ. </td>
                              <td class="text-center">  นายทหารบริหารกาลังพล กฎก.สธน.ทอ.  </td>
                              <td class="text-center"> 8813 </td> 
                              <td class="text-center"> ช/ญ </td>
                              <td class="text-center"> ร.อ. </td>   
                              <td class="text-center"> 1 </td>
                            </tr> 
                          </tbody>
                        </table>
 
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <!--/ Bootstrap 3 table -->
        </div>
      </div>
    </div>
  </section>

  
  <!-- #############   Modal   #############  --> 
<!-- 
  <div class="modal fade text-left modal_custom1" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="modalConfirm"  aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <form>
          <div class="my-2 mx-2 px-2 py-2">
            <div class="text-center"> 
              <img src="../../Asset/Images/send.png">
              <div class="py-3">
              <h1> ยืนการส่งข้อมูลหรือไม่ ?</h1>
              <h6>กรุณาเลือกปุ่มใช่หรือไม่ใช่่</h6>
              </div>
            </div>
            <div class="col-12 d-flex justify-content-center"> 
              <div class="row"> 
                  <a href="#" class="btn btn-min-width mb-1 mr-1 round btn-success" role="button" >
                  <span></span>ใช่</a> 
                  <a href="#" class="btn btn-min-width mb-1 round btn-danger" role="button">
                  <span></span> ไม่ใช่ </a> 
              </div>
            </div>
          </div> 
        </form>
      </div> 
    </div> 
  </div> -->

  <div class="modal fade text-left modal_custom1" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="modalConfirm"  aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <form>
          <div class="my-2 mx-2 px-2 py-1">
            <div class="text-center"> 
              <span class=" fontcolor3  "> <i class="la la-file-text-o la10" ></i> </span>
              <div class="pb-3 pt-1">
              <h1> ต้องการสมัครใช่หรือไม่ ?</h1>
              <h6>กรุณาเลือกปุ่มใช่หรือไม่ใช่่</h6>
              </div>
            </div>
            <div class="col-12 d-flex justify-content-center"> 
              <div class="row"> 
                  <a href="#" class="btn btn-min-width mb-1 mr-1 round btn-success" role="button" data-toggle="modal" data-target="#modalConfirm"  >
                  <span></span>ใช่</a> 
                  <a href="#" class="btn btn-min-width mb-1 round btn-danger" role="button"  data-dismiss="modal">
                  <span></span> ไม่ใช่ </a> 
              </div>
            </div>
          </div> 
        </form>
      </div> 
    </div> 
  </div>

  <div class="modal fade text-left modal_custom1" id="modalNotRegis" tabindex="-1" role="dialog" aria-labelledby="modalNotRegis"  aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <form>
          <div class="my-2 mx-2 px-2 py-1">
            <div class="text-center"> 
              <span class=" fontcolor2 "> <i class="la la-times-circle-o la10" ></i> </span>
              <div class="pb-1 pt-1">
                <h4> ขออภัยไม่สามารถสมัครสอบได้</h4>
                <h4> เนื่องจากคุณสมบัติไม่ตรงตามกำหนด</h4>
              </div>
            </div> 
            <div class="col-12 d-flex justify-content-center">
              <div class="row"> 
                  <a href="#" class="btn btn-min-width mb-1 mr-1 round btn-success" role="button"   data-dismiss="modal" >
                  <span></span> ยอมรับ</a>  
              </div>
            </div>
          </div> 
        </form>
      </div> 
    </div> 
  </div>
  
   <!-- #############   End Modal   #############  --> 


  <!-- footer -->
  <?php include '../include/footer.php'; ?> 
  <script src="../../Controllers/planManpowerController.js"></script>
  
  <script type="text/javascript">
    $(document).ready(function() { 

     
      $("#planManTypeId2").change(function(){
        $('.tb1').addClass('hidden');
         if( $(this).val() ==22 ){
            $('#planManpowerTable3').removeClass('hidden'); 
          }else{
            $('#planManpowerTable2').removeClass('hidden'); 
          }
      });


    }); 
  </script>
  