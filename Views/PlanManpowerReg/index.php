  <!-- header -->
  <?php include '../include/header.php'; ?>
  <?php include '../../Model/Ducklab/func.php'; ?>
  <?php  
  //Recruitment and appointment
    $menu1 = "RECRNAPPOINT" ;
    $menu2 = "PlanManpowerReg" ;


  ?>
  <!-- menu -->
  <?php include '../include/menu.php'; ?>

  <section>
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
          <h3 class="content-header-title">หน้าแรก</h3>
        </div> 
      </div>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <!-- <li class="breadcrumb-item"><a href="../home/index.php">หน้าแรก </a></li>  -->
          <li class="breadcrumb-item  active" aria-current="page">  หน้าแรก </li>   
        </ol>
      </nav>

      <div class="content-body">
         <!-- Bootstrap 3 table -->
         <section id="">
         
          <div class="row">
            <div class="col-12">
              <div class="card card1">
                <div class="card-header">
                  <h4 class="card-title"> สมัครสอบประจำปีงบประมาณ 2562 </h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements"> 
                    <a href="recruitllist.php" class="btn btn-sm round btn2 btn-success btn-glow"> สมัครสอบ </a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-12 mt-2">
              <div class="card card1">
                <div class="card-header">
                  <h4 class="card-title"> ข้อมูลการสมัครสอบ </h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements"> 
                    <a href="recruitlregis.php" class="btn btn-sm round btn2 btn-info btn-glow"> ข้อมูลการสมัคร  </a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-12 mt-2">
              <div class="card card1">
                <div class="card-header">
                  <h4 class="card-title"> ประวัติการสมัครสอบ </h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements"> 
                    <a href="recruitlhistory.php" class="btn btn-sm round btn2 btn-info btn-glow"> ประวัติ  </a>
                  </div>
                </div>
              </div>
            </div>
          </div>

 
        </section>
        <!--/ Bootstrap 3 table -->
      </div>
    </div>
  </div>
  </section>

  
 
  
  

    

  <!-- footer -->
  <?php include '../include/footer.php'; ?>
  <script src="../../Controllers/Ducklab/duck.script.js"></script>
  <script src="../../Controllers/planManpowerController.js"></script>
  <script type="text/javascript">
    $(document).ready(function() { 
     // planManpower.GetArmy();
     // planManpower.GetPlanHeader('');
     planManpower.GetPlanHeader('');

     $('#planManpowerTable').DataTable( {
        "paging":   true,
        "lengthChange": false,
        "ordering": false,
        "info":     false,
        "searching": false
    } );
    }); 
  </script>
  