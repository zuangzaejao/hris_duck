  <!-- header -->
  <?php require_once '../../config.php' ?>
  <?php require_once '../../Model/Ducklab/duck.class.php'; ?>
  <?php require_once '../../Model/Ducklab/contents.class.php'; ?>
  <?php require_once '../../Model/Ducklab/org.class.php'; ?>
  <?php require_once '../../Model/Ducklab/func.php'; ?>
  <?php require_once '../include/header.php'; ?>
  
  <?php
  
  $menu1 = "ORGSTRUC" ;
  $menu2 = "ORGSTRUCDEPT" ;
  $menu3 = "ORGSTRUCDEPTSETTING" ;
  $menu4 = "ORGANIZATIONSTRUC" ;
   

  $clsorg = new OrgClass();


  //$OrgType=$clsorg->Load('OrgType',array('OrgTypeActive'=>'1','OrgTypeDelete'=>0),'','');
 
  ?>
  <!-- menu -->
  <?php include '../include/menu.php'; ?>
  
  <section>
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-2">
          <div class="htab" ></div>
            <h3 class="content-header-title">ชื่อหน่วยงาน</h3>
          </div>
        </div>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="../home/index.php">ระบบงานโครงสร้างอัตรากำลังพล</a></li>
            <li class="breadcrumb-item"><a href="../home/index.php">โครงสร้าง</a></li>
            <li class="breadcrumb-item active" aria-current="page">ชื่อหน่วยงาน</li>
          </ol>
        </nav>
        <div class="content-body">
        <?php include_once '../include/modelOnload.php' ?>
          <section id="bootstrap3">
            
            <div class="row hidden"> 
              <div class="col-sm-12">
                <div class="box_h1 card">
                  <div class="card-header card-head-inverse  ">
                    <h4 class="card-title text-white"> ค้นหา </h4> 
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                      <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                      </ul>
                    </div>
                  </div>
                  <div class="card-content collapse show">
                    <div class="card-body">
                      <div class="row">
                        <div class=" col-md-8  m-auto "> 
                          <form class="form">
                            <div class="form-body">  
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="OrgType">  โครงสร้างอัตรา </label>
                                            <select id="OrgType" name="OrgType" class="form-control">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="OrgLevel1"> สังกัด </label>
                                            <select id="OrgLevel1" name="OrgLevel1" class="form-control">
                                            </select>
                                        </div>
                                    </div>
                                </div>  
                            </div> 

                            <div class="text-center">
                              <button type="button" class="btn round btn1 btncustom1 mr-1">
                                <i class="fa fa-search"></i> ค้นหา
                              </button>
                              <button type="button" class="btn round btn1 btncustom1">
                                <i class=" fa fa-repeat"></i> ล้างค่า
                              </button>
                            </div> 
                          </form> 
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div> 

            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-content collapse show">
                    <div class="card-body card-dashboard">
                      <p class="card-text"></p> 
                      <a href="./create.php" class="btn btn-social btn-min-width mb-1 btn_b1" >
                        <span class="la la-plus-circle" style=""></span> เพิ่ม
                      </a>
                      <a href="./delete.php" class="btn btn-social btn-min-width mb-1 btn_b1" >
                        <span class="la la-trash-o"></span> ลบ
                      </a>

                      <?php 
                       //$dataorg[] = $clsorg->Load('OrgLevel1',array(),'','');
                      // $dataorg1 = $clsorg->Load('OrgLevel1',array(),'','');
                     //  $d1=count($dataorg1);
                     //  $dataorg[$d1] = $dataorg1;


                    //  $clsorg = new OrgClass(); 
                    //  $dataorg2 = $clsorg->Load('OrgLevel2',array(),'','');
                     // $d2=count($dataorg2)+$d1;
                     // $dataorg[$d2] = $dataorg2;

                      $clsorg = new OrgClass(); 
                      $dataorg2 = $clsorg->Load('OrgLevel2',array(),'','');
                      $clsorg = new OrgClass(); 
                      $dataorg3 = $clsorg->Load('OrgLevel3',array(),'','');
                      $clsorg = new OrgClass(); 
                      $dataorg4 = $clsorg->Load('OrgLevel4',array(),'','');
                      $clsorg = new OrgClass(); 
                      $dataorg5 = $clsorg->Load('OrgLevel5',array(),'','');
                      $clsorg = new OrgClass(); 
                      $dataorg6 = $clsorg->Load('OrgLevel6',array(),'','');
                      $clsorg = new OrgClass(); 
                      $dataorg7 = $clsorg->Load('OrgLevel7',array(),'','');
                      $clsorg = new OrgClass(); 
                      $dataorg8 = $clsorg->Load('OrgLevel8',array(),'','');
                     // $d3=count($dataorg3)+$d2;
                     // $dataorg[$d3] = $dataorg3;


                     $dataorg = array();

                    //  foreach( $dataorg1 as $key1 => $val1){
                    //     $dataorg[] = $val1;
                    //  }
                     foreach( $dataorg2 as $key2 => $val2){
                      $dataorg[] = $val2;
                     }
                     foreach( $dataorg3 as $key3 => $val3){
                      $dataorg[] = $val3;
                     }
                     foreach( $dataorg4 as $key4 => $val4){
                      $dataorg[] = $val4;
                     }
                     /*
                     foreach( $dataorg5 as $key5 => $val5){
                      $dataorg[] = $val5;
                     }
                     foreach( $dataorg6 as $key6 => $val6){
                      $dataorg[] = $val6;
                     }
                     foreach( $dataorg7 as $key7 => $val7){
                      $dataorg[] = $val7;
                     }
                     foreach( $dataorg8 as $key8 => $val8){
                      $dataorg[] = $val8;
                     }*/

                     // echo "<pre>".print_r($dataorg ,true)."</pre>";
                      ?>
                      <table id="OrgLevelXTable" class="table table-striped table-borderless table-hover table_custom1" >
                        <thead >
                          <tr> 
                            <th></th>
                            <th>ลำดับที่</th>
                            <th>รหัสหน่วยงาน</th>
                            <th class="text-left">ชื่อเต็มหน่วยงาน</th>
                            <th class="text-left">ชื่อย่อหน่วยงาน</th>
                            
                            <th>สถานะ</th>
                            
                          </tr>
                        </thead>
                        <tbody >
                          <?php
                          if($dataorg){
                            foreach( $dataorg as $key1 => $val1){
                              ?>
                            <tr>
                              <td class="text-center"> <?php echo ($key1+1) ; ?> </td>
                              <td class="text-center"> <?php echo $val1['OrgLevID'];?> </td>
                              <td class="text-center"> <?php echo $val1['OrgLevDID'];?> </td>
                              <td> <?php echo $val1['OrgLevName'] ; ?> </td>
                              <td> <?php echo $val1['OrgLevAbbrName']; ?>  </td>
                            
                              <td class="text-center"> <?php echo $val1['OrgLevActive']; ?> </td>
                            </tr>
                              <?php
                            }
                          }
                          ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </section>

  
 

 
 

  <!-- footer -->
  <?php include '../include/footer.php'; ?>
  
  <script type="text/javascript">
    $(document).ready(function() { 

      org.LoadOrgTypeSel('','OrgType');
      //();

      //getOrganizationSubUnit();

      $('#OrgLevelXTable').DataTable( {
          "paging":   true,
          "lengthChange": true,
          "ordering": true,
          "info":     true,
          "searching": true
      } );
    }); 

  </script>

  
</body>
</html>
  