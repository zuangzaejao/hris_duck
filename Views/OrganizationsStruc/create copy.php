 
  <?php
  require_once '../../config.php';
  require_once '../../Model/Ducklab/duck.class.php'; 
  require_once '../../Model/Ducklab/contents.class.php'; 
  require_once '../../Model/Ducklab/org.class.php'; 
  require_once '../../Model/Ducklab/func.php'; 
  require_once '../include/header.php'; 
 
    $menu1 ="ORGSTRUC" ;
    $menu2 ="ORGSTRUCDEPT";
    $menu3 ="ORGSTRUCDEPTPERSON";
    $menu4 ="ORGANIZATIONSUBUNIT";

  ?>
  <?php include '../include/menu.php'; ?>
  <?php include_once '../include/modelOnload.php' ?>


<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title">เพิ่มชื่อหน่วยงาน</h3>
                <div class="row breadcrumbs-top"></div>
            </div>
        </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="./index.php">ระบบงานโครงสร้างอัตรากำลังพล</a></li>
                  <li class="breadcrumb-item"><a href="./index.php">โครงสร้าง</a></li>
                  <li class="breadcrumb-item active" aria-current="page">เพิ่มชื่อหน่วยงาน</li>
            </ol>
        </nav>
        <div class="content-body">
              <!-- Basic form layout section start -->
            <section id="horizontal-form-layouts">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collpase show">
                            <div class="card-body"> 
                                <div class=" col-md-8  m-auto pt-4 pb-4"> 

                                      <form class="form form-horizontal">
                                          <div class="form-body"> 
                                              
                                            <!-- 
                                                <div class="form-group col-md-8">
                                                    <label for="OrgLevelID">ฐานะหน่วย :</label>
                                                    <select class="form-control select2 " id="OrgLevelID" name="OrgLevelID" > 
                                                    </select>
                                                </div>
                                            
                                                <div class="row">
                                                    <div class="col-md-12 ">
                                                        <div class="form-group">   
                                                        <label class="col-md-6 label-control" for="OrgLevelID">ฐานะหน่วย</label>
                                                        <div class="col-md-12">
                                                            <div class="position-relative ">
                                                                <select name="OrgLevelID" id="OrgLevelID" class="select2 form-control " style="width:100%;"> </select>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div> -->

                                                <div class="col-md-6">
                                                    <div class="form-group">ฐานะหน่วย :
                                                        <select  class="select2 form-control" style="width: 100%;" name="OrgLevelID" id="OrgLevelID" >   
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group"> โครงสร้าง :
                                                        <select  class="select2 form-control" style="width: 100%;" name="OrgTypeID" id="OrgTypeID" >   
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group"> หน่วยงาน :
                                                        <select  class="select2 form-control" style="width: 100%;" name="OrgLevel0" id="OrgLevel0" >   
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 dvOrgLevel1">
                                                    <div class="form-group"> ส่วน/หน่วยงาน ระดับที่ 1 :
                                                        <select  class="select2 form-control" style="width: 100%;" name="OrgLevel1" id="OrgLevel1" >   
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 dvOrgLevel2 hidden">
                                                    <div class="form-group"> หน่วยงาน ระดับที่ 2 :
                                                        <select  class="select2 form-control " style="width: 100%;" name="OrgLevel2" id="OrgLevel2" >   
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 dvOrgLevel3 hidden">
                                                    <div class="form-group"> หน่วยงาน ระดับที่ 3 :
                                                        <select  class="select2 form-control " style="width: 100%;" name="OrgLevel3" id="OrgLevel3" >   
                                                        </select>
                                                    </div>
                                                </div> 
                                                <div class="col-md-6 dvOrgLevel4 hidden">
                                                    <div class="form-group"> หน่วยงาน ระดับที่ 4 :
                                                        <select  class="select2 form-control " style="width: 100%;" name="OrgLevel4" id="OrgLevel4" >   
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 dvOrgLevel5 hidden">
                                                    <div class="form-group"> หน่วยงาน ระดับที่ 5 :
                                                        <select  class="select2 form-control " style="width: 100%;" name="OrgLevel5" id="OrgLevel5" >   
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 dvOrgLevel6 hidden ">
                                                    <div class="form-group"> หน่วยงาน ระดับที่ 6 :
                                                        <select  class="select2 form-control " style="width: 100%;" name="OrgLevel6" id="OrgLevel6" >   
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 dvOrgLevel7 hidden">
                                                    <div class="form-group"> หน่วยงาน ระดับที่ 7 :
                                                        <select  class="select2 form-control " style="width: 100%;" name="OrgLevel7" id="OrgLevel7" >   
                                                        </select>
                                                    </div>
                                                </div>



                                                
                                                <div class="col-md-6">
                                                    <div class="form-group">ชื่อ หน่วยงาน :
                                                    <input type="text" class="form-control border-primary w-100"   name="OrgName" id="OrgName"  value="" >
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">ชื่อย่อ หน่วยงาน :
                                                    <input type="text" class="form-control border-primary  w-100"   name="OrgNameAbbr" id="OrgNameAbbr"  value="" >
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">ชื่อหน่วย/สังกัด :
                                                    <input type="text" class="form-control border-primary  w-100"  disabled  name="OrgNameDept" id="OrgNameDept"  value="" >
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">ชื่อย่อหน่วย/สังกัด :
                                                    <input type="text" class="form-control border-primary  w-100"  disabled  name="OrgNameAbbrDept" id="OrgNameAbbrDept"  value="" >
                                                    </div>
                                                </div>


                                                <div class="col-md-6 hidden">
                                                    <div class="form-group">ชื่อกึ่งย่อหน่วยงาน/สังกัด :
                                                    <input type="text" class="form-control border-primary  w-100"  disabled  name="OrgNameSemiAbbrDept" id="OrgNameSemiAbbrDept"  value="" >
                                                    </div>
                                                </div> 

                                                <div class="col-md-6">
                                                    <div class="form-group"> จังหวัด :
                                                        <select  class="select2 form-control" style="width: 100%;" name="ProvinceCode" id="ProvinceCode" >   
                                                        </select>
                                                    </div>
                                                </div>

                                        

                                                <div class="row">
                                                    <div class="col-md-12">
                                                      <br>
                                                      <label class="col-md-3 label-control" for="upToAirforce" style="padding-right:0px;">หน่วยขึ้นตรงกองทัพอากาศ</label>
                                                      <input id="OrgAF" name="OrgAF" type="checkbox" checked data-toggle="toggle" data-style="ios" data-on="ขึ้นตรง" data-off="ไม่ขึ้นตรง" data-onstyle="success" data-offstyle="danger" data-size="sm">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                      <br>
                                                      <label class="col-md-1 label-control" for="" style="padding-right:0px;">สถานะ</label>
                                                      <input id="OrgActive" name="OrgActive" type="checkbox" checked data-toggle="toggle" data-style="ios" data-on="ใช้งาน" data-off="ยกเลิก" data-onstyle="success" data-offstyle="danger" data-size="sm">
                                                    </div>
                                                </div>

                                                <div class="form-actions text-center " >
                                                    <input type="hidden"  class="form-control border-primary"  name="lastparent" id="lastparent" value="">
                                                    <input type="hidden"  class="form-control border-primary"  name="levelnow" id="levelnow" value="">
                                                    <input type="hidden"  class="form-control border-primary"  name="OrgLevCreateBy" id="OrgLevCreateBy" value="1">
                                                    <input type="hidden"  class="form-control border-primary"  name="OrgLevUpdateBy" id="OrgLevTypeUpdateBy" value="1">
                                                    <input type="hidden"  class="form-control border-primary"  name="OrgLevCreateDate" id="OrgLevCreateDate" value="<?php echo GetToday('');?>">
                                                    <input type="hidden"  class="form-control border-primary"  name="OrgLevUpdateDate" id="OrgLevUpdateDate" value="<?php echo GetToday('');?>">

                                                    <button type="button" class="btn btn-danger  round btn-min-width mr-1 mb-1"  >ยกเลิก</button>
                                                    <button type="button" class="btn btn-success  round btn-min-width mr-1 mb-1"  id="btncreatecf" >บันทึก</button>
 
                                                </div>
                                                                                        

                                          </div>
                                      </form>
                                  </div>
                                  </div>
                            </div>
                          </div>
                      </div>
                </div>
               
            </section>
              <!-- // Basic form layout section end -->
          </div>
      </div>
  </div>




<?php  include "../include/footer.php";  ?>  
<script type="text/javascript">
    $(document).ready(function() {
        //org.LoadOrgGroupType();
        org.LoadOrgLevelSel('','OrgLevelID');
        org.LoadOrgTypeSel('','OrgTypeID'); 
 
     //   org.LoadOrgTypeSel('','OrgTypeID');  
        $("#OrgTypeID").change(function(){ 
            org.LoadOrg0Sel('',$("#OrgTypeID").val(),'OrgLevel0','OrgLevel0');//selfDID,OrgTypeID,element_id,tablename
        }); 
        $("#levelnow").val(0);

       

        var datanameab = [];
        var datanameabset ="";
        var datanameabfull = [];
        var datanameabsetfull ="";
        $("#OrgLevel0").change(function(){
            $(".dvOrgLevel1").removeClass("hidden");
            org.LoadOrgSel('',$("#OrgLevel0").val(),'OrgLevel1','OrgLevel1');
            $("#levelnow").val(1);
            $("#lastparent").val($("#OrgLevel0").val());
            
            
           datanameab[0]  = $("#OrgLevel0 option:selected").attr("data-nameab");
           datanameab[0] = "";
           $("#OrgNameAbbrDept").val( datanameab[0]); 

           datanameabfull[0]  = $("#OrgLevel0 option:selected").attr("data-name");
           datanameabfull[0] = "";
           $("#OrgNameDept").val( datanameabfull[0]); 
           
        });

        // if OrgLevel == 1 || OrgLevel == 32 คือส่วน ไม่แสดง
        $("#OrgLevel1").change(function(){
            $(".dvOrgLevel2").removeClass("hidden");
            org.LoadOrgSel('',$("#OrgLevel1").val(),'OrgLevel2','OrgLevel2');
            $("#levelnow").val(2);
            $("#lastparent").val($("#OrgLevel1").val());
            
            datanameab[1]  = $("#OrgLevel1 option:selected").attr("data-nameab"); 
            datanameabset = datanameab[1]+" "+datanameab[0];
            $("#OrgNameAbbrDept").val(datanameabset);


            datanameabfull[1]  = $("#OrgLevel1 option:selected").attr("data-name"); 
            datanameabsetfull = datanameabfull[1]+" "+datanameabfull[0];
            $("#OrgNameDept").val(datanameabsetfull);
           
        });
        
        $("#OrgLevel2").change(function(){
            $(".dvOrgLevel3").removeClass("hidden");
            org.LoadOrgSel('',$("#OrgLevel2").val(),'OrgLevel3','OrgLevel3');
            $("#levelnow").val(3);
            $("#lastparent").val($("#OrgLevel2").val());

            datanameab[2] = $("#OrgLevel2 option:selected").attr("data-nameab");
            datanameabset = datanameab[2]+ " "+datanameab[1]+" "+datanameab[0];
            $("#OrgNameAbbrDept").val(datanameabset);

            datanameabfull[2]  = $("#OrgLevel2 option:selected").attr("data-name"); 
            datanameabsetfull = datanameabfull[2]+" "+datanameabfull[1]+" "+datanameabfull[0];
            $("#OrgNameDept").val(datanameabsetfull);
        });

        $("#OrgLevel3").change(function(){
            $(".dvOrgLevel4").removeClass("hidden");
            org.LoadOrgSel('',$("#OrgLevel3").val(),'OrgLevel4','OrgLevel4');
            $("#levelnow").val(4);
            $("#lastparent").val($("#OrgLevel3").val());

           
            datanameab[3] = $("#OrgLevel3 option:selected").attr("data-nameab");
            datanameabset =  datanameab[3]+ " "+datanameab[2]+ " "+datanameab[1]+" "+datanameab[0];
            $("#OrgNameAbbrDept").val(datanameabset);

            datanameabfull[3]  = $("#OrgLevel3 option:selected").attr("data-name"); 
            datanameabsetfull = datanameabfull[3]+ " "+datanameabfull[2] +" "+datanameabfull[1]+" "+datanameabfull[0];
            $("#OrgNameDept").val(datanameabsetfull);
        });

        $("#OrgLevel4").change(function(){
            $(".dvOrgLevel5").removeClass("hidden");
            org.LoadOrgSel('',$("#OrgLevel4").val(),'OrgLevel5','OrgLevel5');
            $("#levelnow").val(5);
            $("#lastparent").val($("#OrgLevel4").val());

            datanameab[4] = $("#OrgLevel4 option:selected").attr("data-nameab");
            datanameabset =  datanameab[4]+ " "+datanameab[3]+ " "+datanameab[2]+ " "+datanameab[1]+" "+datanameab[0];
            $("#OrgNameAbbrDept").val(datanameabset);

            datanameabfull[4]  = $("#OrgLevel4 option:selected").attr("data-name"); 
            datanameabsetfull =  datanameabfull[4]+ " "+datanameabfull[3]+ " "+datanameabfull[2] +" "+datanameabfull[1]+" "+datanameabfull[0];
            $("#OrgNameDept").val(datanameabsetfull);
        });
        
        $("#OrgLevel5").change(function(){
            $(".dvOrgLevel6").removeClass("hidden");
            org.LoadOrgSel('',$("#OrgLevel5").val(),'OrgLevel6','OrgLevel6');
            $("#levelnow").val(6);
            $("#lastparent").val($("#OrgLevel5").val());

            datanameab[5] = $("#OrgLevel5 option:selected").attr("data-nameab");
            datanameabset =  datanameab[5]+ " "+datanameab[4]+ " "+datanameab[3]+ " "+datanameab[2]+ " "+datanameab[1]+" "+datanameab[0];
            $("#OrgNameAbbrDept").val(datanameabset);

            datanameabfull[5]  = $("#OrgLevel5 option:selected").attr("data-name"); 
            datanameabsetfull =  datanameabfull[5]+ " "+datanameabfull[4]+ " "+datanameabfull[3]+ " "+datanameabfull[2] +" "+datanameabfull[1]+" "+datanameabfull[0];
            $("#OrgNameDept").val(datanameabsetfull);
        });

        $("#OrgLevel6").change(function(){
            $(".dvOrgLevel7").removeClass("hidden");
            org.LoadOrgSel('',$("#OrgLevel6").val(),'OrgLevel7','OrgLevel7');
            $("#levelnow").val(7);
            $("#lastparent").val($("#OrgLevel6").val());

            datanameab[6] = $("#OrgLevel6 option:selected").attr("data-nameab");
            datanameabset =  datanameab[6]+" "+datanameab[5]+" "+datanameab[4]+" "+datanameab[3]+" "+datanameab[2]+" "+datanameab[1]+" "+datanameab[0];
            $("#OrgNameAbbrDept").val(datanameabset);

            datanameabfull[6]  = $("#OrgLevel6 option:selected").attr("data-name"); 
            datanameabsetfull =  datanameabfull[6]+ " "+datanameabfull[5]+ " "+datanameabfull[4]+ " "+datanameabfull[3]+ " "+datanameabfull[2] +" "+datanameabfull[1]+" "+datanameabfull[0];
            $("#OrgNameDept").val(datanameabsetfull);
        });
        $("#OrgLevel7").change(function(){
            $(".dvOrgLevel7").removeClass("hidden");
            $("#levelnow").val(8);
            $("#lastparent").val($("#OrgLevel7").val());
           
            datanameab[7] = $("#OrgLevel7 option:selected").attr("data-nameab");
            datanameabset =  datanameab[7]+" "+datanameab[6]+" "+datanameab[5]+" "+datanameab[3]+" "+datanameab[3]+" "+datanameab[2]+" "+datanameab[1]+" "+datanameab[0];
            $("#OrgNameAbbrDept").val(datanameabset);

            datanameabfull[7]  = $("#OrgLevel7 option:selected").attr("data-name"); 
            datanameabsetfull =  datanameabfull[7]+ " "+datanameabfull[6]+ " "+datanameabfull[5]+ " "+datanameabfull[4]+ " "+datanameabfull[3]+ " "+datanameabfull[2] +" "+datanameabfull[1]+" "+datanameabfull[0];
            $("#OrgNameDept").val(datanameabsetfull);
        });
    


        org.LoadProvinceSel('','ProvinceCode');


        $("#btncreatecf").click(function(){ 
            $('#modal_createcf').modal('show');
        });
        $("#btncreate").click(function(){
            org.CreateOrg();
        });



        $("#OrgName").keyup(function(){
            var datanameabfull_txt = $("#OrgName").val(); 

         
            datanameabfull_txt = datanameabfull_txt+" "+datanameabfull;
            datanameabfull_txt =  datanameabfull_txt.replace( /,/g, ' ' ) ;
           // datanameabfull_txt = datanameabfull_txt.replace(',',' ');
           $("#OrgNameDept").val( datanameabfull_txt);  
        }); 

       

        $("#OrgNameAbbr").keyup(function(){
            var datanameab_txt = $("#OrgNameAbbr").val(); 
           
            datanameab_txt = datanameab_txt+" "+datanameab ;
            datanameab_txt = datanameab_txt.replace(',',' ');
           $("#OrgNameAbbrDept").val( datanameab_txt);  
        }); 


        // $("#OrgNameSemiDept").val( datanameab[0]); 

        

        
    });

    function escapeRegExp(str) {
  return str.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"); // $& means the whole matched string
}
</script> 
 

