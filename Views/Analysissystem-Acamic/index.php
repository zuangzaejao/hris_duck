  <!-- header -->
  <?php include '../include/header.php'; ?>

  <!-- menu -->
  <?php include '../include/menu.php'; ?>

  <style>
/* ol > li > a {color:#222233;} */
.toggle.ios,
.toggle-on.ios,
.toggle-off.ios {
    border-radius: 20rem;
}

.toggle.ios .toggle-handle {
    border-radius: 20rem;
}


  </style>

  <section>

      <div class="app-content content">
          <div class="content-wrapper">
                                            <div class="content-header row">
                                                <div class="content-header-left col-12 mb-2">
                                                    <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
                                                    <h3 class="content-header-title">บันทึกผลการพิจารณาของสายวิทยาการ</h3>
                                                                        <nav aria-label="breadcrumb">
                                                                            <ol class="breadcrumb">
                                                                                <li class="breadcrumb-item"><a
                                                                                        href="../home/index.php">ระบบงานวิเคราะห์ความต้องการกำลังพลประจำปี</a></li>
                                                                                <li class="breadcrumb-item active" aria-current="page">
                                                                                บันทึกผลการพิจารณาของสายวิทยาการ</li>
                                                                            </ol>
                                                                        </nav>
                                                </div>
                                            </div>
<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
                                                                    
<div class="shadow p-0 bg-white rounded">
    <div class="card collapse-icon accordion-icon-rotate active">
        <div id="headingCollapse31" class="card-header">
        <a data-toggle="collapse" href="#collapse31" aria-expanded="true" aria-controls="collapse31" class="card-title lead white" style="color:white;" >
        <h6 style="background-color:#0f1733;color:white;line-height:40px;padding-left:10px;">ค้นหา</h6></a></div>
        <div id="collapse31" role="tabpanel" aria-labelledby="headingCollapse31" class="card-collapse collapse show" aria-expanded="true" >  


<form>

<div class="row">
        <div class="col-10 m-auto"> 
            <div class="form-row px-2 m-auto">

            <div class="form-group col-md-3">
                    <label for="formGroupExampleInput">ปีงบประมาณ:</label>
                    <input type="text" class="form-control" id="formGroupExampleInput" style="width:100px;border-color:red" placeholder="2562">
                    </div>
            <div class="form-group col-md-3">
                    <label for="formGroupExampleInput2">กําลังพลที่ต้องการ :</label>
                    <select class="form-control" id="exampleFormControlSelect1" style="width: 100px;">
                        <option>สัญญาบัตร</option>
                        <option>ประทวน</option>
                    </select>
            </div>

            <div class="form-group col-md-6">
                    <label for="formGroupExampleInput">หน่วยขึ้นตรงกองทัพอากาศ :</label>
                    <select class="form-control" id="exampleFormControlSelect1">
                    <option>กองบิน 1</option>
                    <option>กองบิน 1</option>
                    <option>กองบิน 1</option>
                    </select>
                    </div>

            <div class="w-100"></div>  


            <div class="form-group col-md-3">
                    <label for="formGroupExampleInput2">เพศ:</label>
                    <select class="form-control" id="exampleFormControlSelect1">
                        <option>หญิง/ชาย</option>
                        <option>ชาย</option>
                        <option>หญิง</option>
                        
                    </select>
            </div>

            <div class="form-group col-md-3">
                    <label for="formGroupExampleInput2">คุณวุฒิ:</label>
                    <select class="form-control" id="exampleFormControlSelect1">
                    <option>รัฐศาสตรดุษฎีบัณฑิต</option>
                    <option>รัฐศาสตรบัณฑิต</option>
                    <option>รัฐศาสตรมหาบัณฑิต</option>
                    </select>
            </div>

            
</div>


<div class="row mt-2"> 
     <div class="col-12 d-flex justify-content-center"> 
        <a href="#" class="btn btn-social btn-min-width mb-1 mr-1 round bg-hr" role="button">
            <span class="fa fa-search"></span>ค้นหา</a> 
        <a href="#" class="btn btn-social btn-min-width mb-1 round bg-hr" role="button" >
            <span class="fa fa-repeat" aria-hidden="true"></span>ล้างค่า</a> 
    </div>
</div>

</div>
</div>
</form>
       

        </div> <!-- close id="collapse31" -->
    </div> <!-- close card collapse-icon accordion-icon-rotate active -->
</div> <!-- close shadow -->


<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->


<div class="row">
<div class="col-lg-6">
<div class="row">
                    <div class="col-4"> 
                                <div class="dropdown my-1">
                                <button class="btn btn-outline-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                ทั้งหมด 500  </button>
                        
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div>
                    </div>

<div class="col-4"> 
        <div class="dropdown my-1">
                <button class="btn btn-outline-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                เลือกแสดงตาราง
                </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <div class="mx-1 my-1 px-1 py-1" >
            <form> 
                <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                <label class="form-check-label" for="inlineCheckbox1">ทั้งหมด</label>
                </div>

                <hr>

                <div class="form-check form-check-inline mr-2">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                <label class="form-check-label" for="inlineCheckbox2">ลำดับ</label>
                </div>

                <div class="form-check form-check-inline mr-2">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3">
                <label class="form-check-label" for="inlineCheckbox3">ปีงบประมาณ</label>
                </div>

                <div class="form-check form-check-inline mr-2">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="option3">
                <label class="form-check-label" for="inlineCheckbox3">ประเภท</label>
                </div>

                <div class="form-check form-check-inline mr-2">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="option3">
                <label class="form-check-label" for="inlineCheckbox3">คุณวุฒิ</label>
                </div>

                <div class="form-check form-check-inline mr-2">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="option3">
                <label class="form-check-label" for="inlineCheckbox3">หน่วย</label>
                </div>

                <div class="form-check form-check-inline mr-2">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="option3">
                <label class="form-check-label" for="inlineCheckbox3">ตำแหน่ง</label>
                </div>

                <!-- <div class="form-check form-check-inline mr-2">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="option3">
                <label class="form-check-label" for="inlineCheckbox3">สายวิทยาการ</label>
                </div> -->

                <div class="form-check form-check-inline mr-2">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="option3">
                <label class="form-check-label" for="inlineCheckbox3">อัตรา</label>
                </div>

                <div class="form-check form-check-inline mr-2">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="option3">
                <label class="form-check-label" for="inlineCheckbox3">เหล่า</label>
                </div>

                <div class="form-check form-check-inline mr-2">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="option3">
                <label class="form-check-label" for="inlineCheckbox3">ลชทอ.</label>
                </div>

                <div class="form-check form-check-inline mr-2">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="option3">
                <label class="form-check-label" for="inlineCheckbox3">เพศ</label>
                </div>

                <div class="form-check form-check-inline mr-2">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="option3">
                <label class="form-check-label" for="inlineCheckbox3">จำนวนที่ขอ</label>
                </div>

                <div class="form-check form-check-inline mr-2">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="option3">
                <label class="form-check-label" for="inlineCheckbox3">วันที่</label>
                </div>
        
    
            </form>
            </div>
        </div>
    </div>
</div>



</div> <!-- row -->
</div> <!-- col6 -->

    <!-- <div class="col-lg-6">
        <div class="col-3"> ทั้งหมด 500 </div>
        <div class="col-3"> ทั้งหมด 500 </div>
        </div>  -->
</div><!-- row -->


<div class="row">
<div class="col-12"> 
        <!-- <h6 class="mb-2 mt-2">เลือกแสดงตาราง</h6> -->

            <div class="float-left">
                <a href="#" class="btn btn-social btn-min-width mb-1 mr-1 round bg-hr">
                <span class="fa fa-plus-circle"> </span> เพิ่ม</a> 

                <a href="#" class="btn btn-social btn-min-width mb-1 mr-1 round bg-hr">
                <span class="fa fa-trash-o"></span> ลบ</a> 

                <a href="#" class="btn btn-social btn-min-width mb-1 round bg-hr">
                <span class="fa fa-print"></span>ปริ้น</a>
            </div> 

            <div class="float-right">
                <a href="displaydata.php" class="btn btn-social btn-min-width mb-1 round bg-hr">
                <span class="fa fa-eye"></span>แสดงจำนวนการขอ</a>
            </div>
    </div>  
</div>


<div class="row"> 

    <div class="col-12">  
        <div class="table-responsive">
                <table class="table table-striped table-border table-hover">
                    <thead class="text-center">
                        <tr class="justify-content-center"style="background-color:#0f1733; color:whitesmoke;">
                                                                
                                                                <th><input type="checkbox" class="checkAll"
                                                                        onclick="toggle(this);" /></th>
                                                                <th></th>
                                                                <th>ลำดับที่</th>
                                                                <th>เลขลำดับสาย</th>
                                                                <th>ปีงบประมาณ</th>
                                                                <th>ประเภท</th>
                                                                <th>คุณวุฒิ</th>
                                                                <th>หน่วย</th>
                                                                <th>ตำแหน่ง</th>
                                                                <th>สายวิทยาการ</th>
                                                                <th>อัตรา</th>
                                                                <th>เหล่า</th>
                                                                <th>ลชทอ.</th>
                                                                <th>เพศ</th>
                                                                <th>จำนวนที่ขอ</th>
                                                                
                                                                <th>สถานะ</th>

                                                            </tr>
                    </thead>
                                                        <tbody class="text-center">
                                                            <tr>
                                                                <td><input type="checkbox" class="checkAll" /></td>
                                                                <td>
                                                                    <a href="edit.php" ><i class="la la-pencil-square-o"
                                                                            style="color:#0f1733;"></i></a>
                                                                    
                                                                </td>
                                                                <td>1</td>
                                                                <td><input type="text" class="form-control" id="formGroupExampleInput" style="width:50px;" placeholder="2"></td>
                                                                <td>2562</td>
                                                                <td>สัญญาบัตร </td>
                                                                <td>รัฐศาสาตร์บัณฑิต</td>
                                                                <td>กคว.คปอ.</td>
                                                                <td>นคช.กคว.คปอ.</td>
                                                                <td>ช่างอากาศ</td>
                                                                <td>ร.อ.</td>
                                                                <td>นบ.</td>
                                                                <td>1413 , 1713</td>
                                                                <td>หญิง</td>
                                                                <td>1</td>
                                                                <td ><div class="badge badge-orange12"> นขต. ทําการส่งสายวิทยาการ</div></td>
                                                                
                                                            </tr>
                                                            <tr>
                                                                <td><input type="checkbox" class="checkAll" /></td>
                                                                <td>
                                                                    <a href="#" ><i class="la la-pencil-square-o"
                                                                            style="color:#0f1733;"></i></a>
                                                                    <!-- <a href="./delete.php"><i class="la la-trash-o"
                                                                            style="color:#0f1733;"></i></a> -->
                                                                </td>
                                                                <td>1</td>
                                                                <td><input type="text" class="form-control" id="formGroupExampleInput" style="width:50px;" placeholder="2"></td>
                                                                <td>2562</td>
                                                                <td>สัญญาบัตร </td>
                                                                <td>รัฐศาสาตร์บัณฑิต</td>
                                                                <td>กคว.คปอ.</td>
                                                                <td>นคช.กคว.คปอ.</td>
                                                                <td>ช่างอากาศ</td>
                                                                <td>ร.อ.</td>
                                                                <td>นบ.</td>
                                                                <td>1413 , 1713</td>
                                                                <td>หญิง</td>
                                                                <td>1</td>
                                                                <td ><div class="badge badge-orange12"> นขต. ทําการส่งสายวิทยาการ</div></td>
                                                                
                                                            </tr>
                                                            <tr>
                                                                <td><input type="checkbox" class="checkAll" /></td>
                                                                <td>
                                                                    <a href="#" ><i class="la la-pencil-square-o"
                                                                            style="color:#0f1733;"></i></a>
                                                                    <!-- <a href="./delete.php"><i class="la la-trash-o"
                                                                            style="color:#0f1733;"></i></a> -->
                                                                </td>
                                                                <td>1</td>
                                                                <td><input type="text" class="form-control" id="formGroupExampleInput" style="width:50px;" placeholder="2"></td>
                                                                <td>2562</td>
                                                                <td>สัญญาบัตร </td>
                                                                <td>รัฐศาสาตร์บัณฑิต</td>
                                                                <td>กคว.คปอ.</td>
                                                                <td>นคช.กคว.คปอ.</td>
                                                                <td>ช่างอากาศ</td>
                                                                <td>ร.อ.</td>
                                                                <td>นบ.</td>
                                                                <td>1413 , 1713</td>
                                                                <td>หญิง</td>
                                                                <td>1</td>
                                                                <td ><div class="badge badge-orange12"> นขต. ทําการส่งสายวิทยาการ</div></td>
                                                                
                                                            </tr>
                                                            
                                                            
                                                        </tbody>
                                                        
                </table>
        </div> 
    </div>  <!-- col-12 -->
</div> <!-- row -->

         <div class="row"> 
                    <div class="col-12 d-flex justify-content-center"> 
                    <a href="#" class="btn btn-social btn-min-width mb-1 mr-1 round btn-success" role="button" >
                        <span class="fa fa-save"></span> บันทึกข้อมูล</a> 
                    <a href="#" data-toggle="modal" data-target="#bootstrap" class="btn btn-social btn-min-width mb-1 round bg-hr" role="button">
                        <span class="la la-send"> </span> ส่งงานวิเคราะห์ </a> 
                    </div>
        </div>




    </div>  
</div> 
    
<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
            </div>
        </div>
</section>

<!-- //////////////////////////////////////////// Modal////////////////////////////////////////////////////// -->
<div class="modal fade text-left modal_custom1" id="bootstrap" tabindex="-1" role="dialog" aria-labelledby="modalSettingRegis"  aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          



          <form>
                             <div class="my-2 mx-2 px-2 py-2">
                           
                                            <div class="text-center"> 
                                                <img src="../../Asset/Images/send.png">
                                                        <div class="py-3">
                                                        <h1> ยืนการส่งข้อมูลหรือไม่ ?</h1>
                                                        <h6>กรุณาเลือกปุ่มใช่หรือไม่ใช่่</h6>
                                                        </div>
                                                </div>

                                         <div class="col-12 d-flex justify-content-center"> 
                                            
                                                <div class="row"> 
                                                    <a href="#" class="btn btn-min-width mb-1 mr-1 round btn-success" role="button" >
                                                    <span></span>ใช่</a> 
                                                    <a href="#" class="btn btn-min-width mb-1 round btn-danger" role="button">
                                                    <span></span> ไม่ใช่ </a> 
                                                </div>
                                        </div>
                          </div>
           
        </form>


        </div> 
    </div> 
</div>


<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
                                         
                                
  <script src="http://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
      crossorigin="anonymous"></script>

  <script type="text/javascript">
$(document).ready(function() {
    console.log("ready");
    change_autorefreshdiv();
});

function change_autorefreshdiv() {
    // $('#prefixPage').addClass('active');
}

function toggle(source) {
    var checkboxes = document.querySelectorAll('.checkAll');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i] != source)
            checkboxes[i].checked = source.checked;
    }
}
  </script>



  <!-- footer -->
  <?php include '../include/footer.php'; ?>