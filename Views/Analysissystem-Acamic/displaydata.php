  <!-- header -->
  <?php include '../include/header.php'; ?>

  <!-- menu -->
  <?php include '../include/menu.php'; ?>

  <style>
/* ol > li > a {color:#222233;} */
.toggle.ios,
.toggle-on.ios,
.toggle-off.ios {
    border-radius: 20rem;
}

.toggle.ios .toggle-handle {
    border-radius: 20rem;
}


  </style>

  <section>

      <div class="app-content content">
          <div class="content-wrapper">
                                            <div class="content-header row">
                                                <div class="content-header-left col-12 mb-2">
                                                    <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
                                                    <h3 class="content-header-title">บันทึกผลการพิจารณาของสายวิทยาการ</h3>
                                                                        <nav aria-label="breadcrumb">
                                                                            <ol class="breadcrumb">
                                                                                <li class="breadcrumb-item"><a
                                                                                        href="../home/index.php">ระบบงานวิเคราะห์ความต้องการกำลังพลประจำปี</a></li>
                                                                                <li class="breadcrumb-item active" aria-current="page">
                                                                                บันทึกผลการพิจารณาของสายวิทยาการ</li>
                                                                            </ol>
                                                                        </nav>
                                                </div>
                                            </div>
<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
                                                                    
<div class="shadow p-0 bg-white rounded">
    <div class="card collapse-icon accordion-icon-rotate active">
        <div id="headingCollapse31" class="card-header">
        <a data-toggle="collapse" href="#collapse31" aria-expanded="true" aria-controls="collapse31" class="card-title lead white" style="color:white;" >
        <h6 style="background-color:#0f1733;color:white;line-height:40px;padding-left:10px;">ค้นหา</h6></a></div>
        <div id="collapse31" role="tabpanel" aria-labelledby="headingCollapse31" class="card-collapse collapse show" aria-expanded="true" >  

        <div class="row">
        <div class="col-lg-6 m-auto">
            <div class="row">
                    <div class="col-4"> 
                                <div class="dropdown my-1">
                                <button class="btn btn-outline-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                เลือกปี  </button>
                        
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div>
                    </div>

                    <div class="col-4"> 
                                <div class="dropdown my-1">
                                <button class="btn btn-outline-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                เลือกหน่วย  </button>
                        
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div>
                    </div>

<div class="col-4"> 
        <div class="dropdown my-1">
                <button class="btn btn-outline-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                เลือกสายวิชาการ
                </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <div class="mx-1 my-1 px-1 py-1" >
            <form> 
                <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                <label class="form-check-label" for="inlineCheckbox1">ทั้งหมด</label>
                </div>

                <hr>

                <div class="form-check form-check-inline mr-2">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                <label class="form-check-label" for="inlineCheckbox2">ลำดับ</label>
                </div>

                <div class="form-check form-check-inline mr-2">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3">
                <label class="form-check-label" for="inlineCheckbox3">ปีงบประมาณ</label>
                </div>

                <div class="form-check form-check-inline mr-2">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="option3">
                <label class="form-check-label" for="inlineCheckbox3">ประเภท</label>
                </div>

                <div class="form-check form-check-inline mr-2">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="option3">
                <label class="form-check-label" for="inlineCheckbox3">คุณวุฒิ</label>
                </div>

                <div class="form-check form-check-inline mr-2">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="option3">
                <label class="form-check-label" for="inlineCheckbox3">หน่วย</label>
                </div>

                <div class="form-check form-check-inline mr-2">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="option3">
                <label class="form-check-label" for="inlineCheckbox3">ตำแหน่ง</label>
                </div>

                <!-- <div class="form-check form-check-inline mr-2">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="option3">
                <label class="form-check-label" for="inlineCheckbox3">สายวิทยาการ</label>
                </div> -->

                <div class="form-check form-check-inline mr-2">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="option3">
                <label class="form-check-label" for="inlineCheckbox3">อัตรา</label>
                </div>

                <div class="form-check form-check-inline mr-2">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="option3">
                <label class="form-check-label" for="inlineCheckbox3">เหล่า</label>
                </div>

                <div class="form-check form-check-inline mr-2">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="option3">
                <label class="form-check-label" for="inlineCheckbox3">ลชทอ.</label>
                </div>

                <div class="form-check form-check-inline mr-2">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="option3">
                <label class="form-check-label" for="inlineCheckbox3">เพศ</label>
                </div>

                <div class="form-check form-check-inline mr-2">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="option3">
                <label class="form-check-label" for="inlineCheckbox3">จำนวนที่ขอ</label>
                </div>

                <div class="form-check form-check-inline mr-2">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="option3">
                <label class="form-check-label" for="inlineCheckbox3">วันที่</label>
                </div>
        
    
            </form>
            </div>
        </div>
    </div>
</div>



</div> <!-- row -->
</div> <!-- col6 --> 
</div><!-- row -->

       

        </div> <!-- close id="collapse31" -->
    </div> <!-- close card collapse-icon accordion-icon-rotate active -->
</div> <!-- close shadow -->


<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

<div class="row">
    <div class="col-12">
        <h6 class="bg-hr p-1 my-1">ผลการค้นหา </h6>
        <h5>ปี 2562  /  กองบิน 1, กองบิน 2  / ชอ. ส.</h5>
        <hr>
        <h5>จํานวน 3 รายการ </h5>
    </div>
</div>


<div class="row"> 

    <div class="col-12">  
        <div class="table-responsive">
                <table class="table table-striped table-border table-hover">
                    <thead class="text-center">
                        <tr class="justify-content-center"style="background-color:#0f1733; color:whitesmoke;">
                                                                
                                                                <th><input type="checkbox" class="checkAll"
                                                                        onclick="toggle(this);" /></th>
                                                                <th></th>
                                                                <th>ลำดับที่</th>
                                                                <th>เลขลำดับสาย</th>
                                                                <th>ปีงบประมาณ</th>
                                                                <th>ประเภท</th>
                                                                <th>คุณวุฒิ</th>
                                                                <th>หน่วย</th>
                                                                <th>ตำแหน่ง</th>
                                                                <th>สายวิทยาการ</th>
                                                                <th>อัตรา</th>
                                                                <th>เหล่า</th>
                                                                <th>ลชทอ.</th>
                                                                <th>เพศ</th>
                                                                <th>จำนวนที่ขอ</th>
                                                                
                                                                <th>สถานะ</th>

                                                            </tr>
                    </thead>
                                                        <tbody class="text-center">
                                                            <tr>
                                                                <td><input type="checkbox" class="checkAll" /></td>
                                                                <td>
                                                                    <a href="edit.php" ><i class="la la-pencil-square-o"
                                                                            style="color:#0f1733;"></i></a>
                                                                    
                                                                </td>
                                                                <td>1</td>
                                                                <td><input type="text" class="form-control" id="formGroupExampleInput" style="width:50px;" placeholder="2"></td>
                                                                <td>2562</td>
                                                                <td>สัญญาบัตร </td>
                                                                <td>รัฐศาสาตร์บัณฑิต</td>
                                                                <td>กคว.คปอ.</td>
                                                                <td>นคช.กคว.คปอ.</td>
                                                                <td>ช่างอากาศ</td>
                                                                <td>ร.อ.</td>
                                                                <td>นบ.</td>
                                                                <td>1413 , 1713</td>
                                                                <td>หญิง</td>
                                                                <td>1</td>
                                                                <td ><div class="badge badge-orange12"> นขต. ทําการส่งสายวิทยาการ</div></td>
                                                                
                                                            </tr>
                                                            <tr>
                                                                <td><input type="checkbox" class="checkAll" /></td>
                                                                <td>
                                                                    <a href="#" ><i class="la la-pencil-square-o"
                                                                            style="color:#0f1733;"></i></a>
                                                                    <!-- <a href="./delete.php"><i class="la la-trash-o"
                                                                            style="color:#0f1733;"></i></a> -->
                                                                </td>
                                                                <td>1</td>
                                                                <td><input type="text" class="form-control" id="formGroupExampleInput" style="width:50px;" placeholder="2"></td>
                                                                <td>2562</td>
                                                                <td>สัญญาบัตร </td>
                                                                <td>รัฐศาสาตร์บัณฑิต</td>
                                                                <td>กคว.คปอ.</td>
                                                                <td>นคช.กคว.คปอ.</td>
                                                                <td>ช่างอากาศ</td>
                                                                <td>ร.อ.</td>
                                                                <td>นบ.</td>
                                                                <td>1413 , 1713</td>
                                                                <td>หญิง</td>
                                                                <td>1</td>
                                                                <td ><div class="badge badge-orange12"> นขต. ทําการส่งสายวิทยาการ</div></td>
                                                                
                                                            </tr>
                                                            <tr>
                                                                <td><input type="checkbox" class="checkAll" /></td>
                                                                <td>
                                                                    <a href="#" ><i class="la la-pencil-square-o"
                                                                            style="color:#0f1733;"></i></a>
                                                                    <!-- <a href="./delete.php"><i class="la la-trash-o"
                                                                            style="color:#0f1733;"></i></a> -->
                                                                </td>
                                                                <td>1</td>
                                                                <td><input type="text" class="form-control" id="formGroupExampleInput" style="width:50px;" placeholder="2"></td>
                                                                <td>2562</td>
                                                                <td>สัญญาบัตร </td>
                                                                <td>รัฐศาสาตร์บัณฑิต</td>
                                                                <td>กคว.คปอ.</td>
                                                                <td>นคช.กคว.คปอ.</td>
                                                                <td>ช่างอากาศ</td>
                                                                <td>ร.อ.</td>
                                                                <td>นบ.</td>
                                                                <td>1413 , 1713</td>
                                                                <td>หญิง</td>
                                                                <td>1</td>
                                                                <td ><div class="badge badge-orange12"> นขต. ทําการส่งสายวิทยาการ</div></td>
                                                                
                                                            </tr>
                                                            
                                                            
                                                        </tbody>
                                                        
                </table>
        </div> 
    </div>  <!-- col-12 -->
</div> <!-- row -->

         <div class="row"> 
                    <div class="col-12 d-flex justify-content-center"> 
                    <a href="#" class="btn btn-social btn-min-width mb-1 mr-1 round btn-success" role="button" >
                        <span class="fa fa-save"></span> บันทึกข้อมูล</a> 
                    <a href="#" data-toggle="modal" data-target="#bootstrap" class="btn btn-social btn-min-width mb-1 round bg-hr" role="button">
                        <span class="la la-send"> </span> ส่งสายวิทยาการ </a> 
                    </div>
        </div>




    </div>  
</div> 
    
<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
            </div>
        </div>
</section>


<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
                                         
                                
  <script src="http://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
      crossorigin="anonymous"></script>

  <script type="text/javascript">
$(document).ready(function() {
    console.log("ready");
    change_autorefreshdiv();
});

function change_autorefreshdiv() {
    // $('#prefixPage').addClass('active');
}

function toggle(source) {
    var checkboxes = document.querySelectorAll('.checkAll');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i] != source)
            checkboxes[i].checked = source.checked;
    }
}
  </script>



  <!-- footer -->
  <?php include '../include/footer.php'; ?>