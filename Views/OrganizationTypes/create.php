<?php
 require_once '../../config.php';
 require_once '../../Model/Ducklab/duck.class.php'; 
 require_once '../../Model/Ducklab/contents.class.php'; 
 require_once '../../Model/Ducklab/org.class.php'; 
 require_once '../../Model/Ducklab/func.php'; 
 require_once '../include/header.php'; 
 
    $menu1 ="ORGSTRUC" ;
    $menu2 ="ORGSTRUCDEPT";
    $menu3 ="ORGSTRUCDEPTPERSON";
    $menu4 ="ORGANIZATIONTYPE";
 
  
  $clsorg = new OrgClass(); 
   $dataorgGType = $clsorg->Load('OrgGroupType',array('OrgGroupTypeDelete'=>0 , 'OrgGroupTypeActive'=>1),'','');
   
  
include_once '../include/menu.php'; 
include_once '../include/modelOnload.php' ;
?>
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
                <h3 class="content-header-title"> เพิ่มโครงสร้าง</h3>
                
            </div>

        </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="./index.php">ระบบงานโครงสร้างอัตรากำลังพล</a></li>
                <li class="breadcrumb-item"><a href="./index.php">โครงสร้าง</a></li>
                <li class="breadcrumb-item active" aria-current="page">เพิ่มโครงสร้าง</li>
            </ol>
        </nav>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="horizontal-form-layouts">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collpase show">
                                <div class="card-body">
                                    <div class="card-text">
                                        <p> 
                                            <?php   // echo "<pre>".print_r($dataorgType,true)."</pre>" ;?>
                                        </p>
                                    </div>  
                                    <form >
                                        <div class="form-body">
 
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="col-md-6 label-control" for="OrgGroupTypeID">ประเภทโครงสร้าง</label>
                                                    <div class="col-md-12">
                                                        <select name="OrgGroupTypeID" id="OrgGroupTypeID" class="select2 form-control">
                                                            <option value="">กรุณาเลือกประเภทโครงสร้าง</option>
                                                            <?php 
                                                            if($dataorgGType){
                                                                foreach( $dataorgGType as $k1 => $v1){
                                                                ?>
                                                                <option value="<?php echo $v1['OrgGroupTypeID'];?>" > <?php echo $v1['OrgGroupTypeName'];?> </option>
                                                                <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <label class="col-md-4 label-control" for="OrgTypeName">ชื่อโครงสร้าง</label>

                                                    <div class="col-md-12">
                                                        <input type="text" id="OrgTypeName" name="OrgTypeName" class="form-control border-primary" value=""> 
                                                    </div>
                                                </div>
                                            </div>
 
                                            <div class="row mt-1">
                                                
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="col-md-6 label-control" for="OrgTypeStartDate">วันที่เริ่มต้น</label>
                                                        <div class="input-group datep">
                                                            <input type="text" class="form-control form2 pickadate-translations" placeholder="" id="OrgTypeStartDate" name="OrgTypeStartDate" data-value="" />
                                                            <div class="input-group-append">
                                                                <span class="input-group-text"> <span class="la la-calendar-o"></span> </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
 

                                                <div class="col-md-6">
                                                    <label class="col-md-6 label-control" for="endDate">วันที่สิ้นสุด</label>
                                                    <div class="input-group datep">
                                                        <input type="text" class="form-control form2 pickadate-translations" placeholder="" id="OrgTypeEndDate" name="OrgTypeEndDate" data-value="" />
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"> <span class="la la-calendar-o"></span> </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">  
                                            <div class="col-md-6 mb-4">
                                                <label class="col-md-4 label-control" for="OrgName0">ชื่อหน่วยสูงสุด </label> 
                                                <div class="col-md-12">
                                                    <input type="text" id="OrgName0" name="OrgName0" class="form-control" value="กองทัพอากาศ"> 
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 mt-1">
                                            <fieldset class="checkboxsas">
                                                <label>
                                                    <input id="OrgTypeAirForce"  type="checkbox" value="1" checked>&nbsp;โครงสร้างของ ทอ.
                                                </label>
                                            </fieldset>
                                            <fieldset class="checkbox">
                                                <label>
                                                    <input id="OrgTypeExtraMoney" type="checkbox" value="1"   >&nbsp;ได้รับวันทวีคูณ
                                                </label>
                                            </fieldset>
                                            <fieldset class="checkbox">
                                                <label>
                                                    <input id="OrgTypeCurrent" type="checkbox" value="1"  checked>&nbsp;โครงสร้างปัจจุบัน
                                                </label>
                                            </fieldset>
                                            <div>
                                                <label class="label-control" for="userinput3">สถานะ</label>
                                                <input id="OrgTypeActive" type="checkbox"   data-toggle="toggle" data-style="ios" data-on="ใช้งาน" data-off="ยกเลิก" data-onstyle="success" data-offstyle="danger" data-size="sm" checked>
                                            </div>
                                        </div>
                                        <div class="form-actions text-center"  >
                                            <input type="hidden"  class="form-control border-primary"  name="OrgTypeCreateBy" id="OrgTypeCreateBy" value="1">
                                            <input type="hidden"  class="form-control border-primary"  name="OrgTypeCreateDate" id="OrgTypeCreateDate" value="<?php echo GetTodayTime();?>">
                                            <input type="hidden"  class="form-control border-primary"  name="OrgTypeUpdateBy" id="OrgTypeUpdateBy" value="1">
                                            <input type="hidden"  class="form-control border-primary"  name="OrgTypeUpdateDate" id="OrgTypeUpdateDate" value="<?php echo GetTodayTime();?>">
                                            <button type="button" class="btn btn-danger  round btn-min-width mr-1 mb-1"  >ยกเลิก</button>
                                            <button type="button" class="btn btn-success  round btn-min-width mr-1 mb-1" id="btncreatecf" >บันทึก</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- // Basic form layout section end -->
        </div>
    </div>
</div>

 
  

<?php include '../include/footer.php'; ?>
  
<script type="text/javascript">
    $( document ).ready(function() {
        
        $("#btncreatecf").attr('disabled','disabled');
        $("#OrgTypeName").keyup(function(){ 
            var DataSet = {
                table: 'OrgType',
                field: 'OrgTypeName',
                where: {
                    OrgTypeName : $("#OrgTypeName").val(),
                }

             };
            org.checkFieldmore(DataSet,'OrgTypeName');
        });


        $("#btncreatecf").click(function(){   
            $('#modal_createcf').modal('show');
        });
        $("#btncreate").click(function(){
            org.CreateOrgType(); 
        });
    });
</script>