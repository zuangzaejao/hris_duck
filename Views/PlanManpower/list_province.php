  <!-- header -->
  <?php include '../include/header.php'; ?>
  <?php include '../../Model/Ducklab/func.php'; ?>
  <?php  
    //Recruitment and appointment
    $menu1 = "RECRNAPPOINT" ;
    $menu2 = "PlanManpower" ;

  ?>
  <!-- menu -->
  <?php include '../include/menu.php'; ?>
  
  <section>
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-2">
            <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
            <h3 class="content-header-title">แผนกำลังพล </h3>
          </div>
        </div>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb"> 
            <li class="breadcrumb-item"><a href="../home/index.php">หน้าแรก </a></li> 
            <li class="breadcrumb-item"><a href="index.php">ระบบงานสรรหาและบรรจุกำลังพล</a></li> 
            <li class="breadcrumb-item active" aria-current="page">จัดกลุ่มคุณวุฒิ/จังหวัด</li>
            
          </ol>
        </nav>
        <div class="content-body">
          <!-- Bootstrap 3 table -->
          <section id="bootstrap3">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-content collapse show">
                    <div class="card-body card-dashboard">
                     
                      <div class="row"> 
                        <div class="col-sm-12">
                          <div class="box_h1 card">
                            <div class="card-header card-head-inverse  ">
                              <h4 class="card-title text-white"> ค้นหา </h4> 
                              <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                              <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                  <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                                </ul>
                              </div>
                            </div>
                            <div class="card-content collapse show">
                              <div class="card-body">
                                <div class="row">
                                  <div class=" col-md-8  m-auto ">
                                  
                                    <form class="form">
                                      <div class="form-body"> 

                                        <div class="boxsearchplan1 hidden ">
                                          <div class="row">
                                            <div class="col-md-3">
                                              <div class="form-group">
                                                <select id="" name="" class="form-control ">
                                                <option value="">เหล่า/จำพวก</option>
                                                  <option value=""> นักบิน </option>
                                                  <option value=""> ต้นหน </option>
                                                  <option value=""> อุตุ </option>
                                                </select>
                                              </div>
                                            </div>
                                            <div class="col-md-3">
                                              <div class="form-group">  
                                                  <select id="projectinput5" name="interested" class="form-control ">
                                                  <option value="">สังกัด</option>
                                                  <option value="">สลก.ทอ.</option>
                                                  <option value="">สบ.ทอ.</option> 
                                                  <option value="">กพ.ทอ.</option> 
                                                  <option value="">ขว.ทอ.</option> 
                                                  <option value="">ยก.ทอ.</option> 
                                                  <option value="">กบ.ทอ.</option> 
                                                  <option value="">กร.ทอ.</option>
                                                  </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                              <div class="form-group">  
                                                  <select id="projectinput5" name="interested" class="form-control ">
                                                  <option value="development">ตำแหน่ง</option>
                                                    <option value=""> จเรฝ่ายยุทธการและนิรภัยการบิน </option>
                                                    <option value=""> ผู้ช่วยจเรฝ่ายยุทธการและนิรภัยการบิน </option>
                                                    <option value="">จเรฝ่ายข่าว  </option>
                                                  </select>
                                                </div>
                                            </div>
                                          </div> 
                                          <div class="row">
                                            <div class="col-md-3">
                                              <div class="form-group">   
                                                <input type="text" id="" class="form-control " placeholder="เลขลชทอ." name="">
                                              </div>
                                            </div> 
                                            <div class="col-md-3">
                                              <div class="form-group"> 
                                                <select id="" name="" class="form-control ">
                                                  <option value="0" selected="" >อัตรา</option>
                                                  <option value="">ร.อ.</option>
                                                  <option value="">ร.ท.</option>
                                                  <option value="">ร.ต.</option>
                                                </select>
                                              </div>
                                            </div>
                                          </div>
                                        </div> <!-- ./boxsearchplan1 -->

                                        <div class="boxsearchplan2">
                                          <div class="row">
                                            <div class="col-md-6">
                                              <div class="form-group">
                                                <select id="" name="" class="form-control ">
                                                  <option value="">คุณวุฒิ</option>
                                                  <option value="">มัธยมศึกษาตอนต้น</option>
                                                  <option value="">มัธยมศึกษาตอนปลาย</option>
                                                  <option value="">ปวช.</option>
                                                  <option value="">ปวส.</option>
                                                  <option value="">ปริญญาตรี</option>
                                                  <option value="">ปริญญาโท</option>
                                                  <option value="">ปริญญาเอก</option>
                                                </select>
                                              </div>
                                            </div> 
                                            <div class="col-md-6">
                                              <div class="form-group">  
                                                  <select id="projectinput5" name="interested" class="form-control ">
                                                    <option value="development">ตำแหน่ง</option>
                                                    <option value=""> จเรฝ่ายยุทธการและนิรภัยการบิน </option>
                                                    <option value=""> ผู้ช่วยจเรฝ่ายยุทธการและนิรภัยการบิน </option>
                                                    <option value="">จเรฝ่ายข่าว  </option>
                                                  </select>
                                                </div>
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-3">
                                              <div class="form-group">
                                                <select id="" name="" class="form-control ">
                                                  <option value="">เหล่า/จำพวก</option>
                                                  <option value=""> นักบิน </option>
                                                  <option value=""> ต้นหน </option>
                                                  <option value=""> อุตุ </option>
                                                </select>
                                              </div>
                                            </div> 
                                            <div class="col-md-3">
                                              <div class="form-group">  
                                                <select id="" name="" class="form-control ">
                                                  <option value="">เพศ</option>
                                                  <option value="">ชาย</option>
                                                  <option value="">หญิง</option>
                                                </select>
                                              </div>
                                            </div>
                                            <div class="col-md-3">
                                              <div class="form-group">  
                                                <select id="" name="" class="form-control ">
                                                  <option value="">อัตรา</option>
                                                  <option value=""> นอ. </option>
                                                  <option value=""> นท. </option>
                                                  <option value=""> นต. </option>
                                                </select>
                                              </div>
                                            </div>
                                            <div class="col-md-3">
                                              <div class="form-group">  
                                                <select id="" name="" class="form-control ">
                                                  <option value="">สังกัด</option>
                                                  <option value="">สลก.ทอ.</option>
                                                  <option value="">สบ.ทอ.</option> 
                                                  <option value="">กพ.ทอ.</option> 
                                                  <option value="">ขว.ทอ.</option> 
                                                  <option value="">ยก.ทอ.</option> 
                                                  <option value="">กบ.ทอ.</option> 
                                                  <option value="">กร.ทอ.</option>
                                                </select>
                                              </div>
                                            </div>
                                            <div class="col-md-3">
                                              <div class="form-group">  
                                                <input type="text" id="" class="form-control " placeholder="เลขลชทอ.หน้าที่" name=""> 

                                              </div>
                                            </div>
                                          </div>
                                        </div>  <!-- ./boxsearchplan2 -->
                                      
                                      </div> 

                                      <div class="text-center">
                                        <button type="button" class="btn round btn1 btncustom1 mr-1">
                                          <i class="fa fa-search"></i> ค้นหา
                                        </button>
                                        <button type="button" class="btn round btn1 btncustom1">
                                          <i class=" fa fa-repeat"></i> ล้างค่า
                                        </button>

                                      </div>

                                    </form>

                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div> 

                      <div class="row"> 
                        <div class="col-md-3 col-sm-12">
                          <div class="box1 txtdesc">
                              ข้อมูลจำนวน <span> 500 </span> อัตรา
                          </div>
                        </div> 
                      </div>
                      <div class="row">
                        <div class="col-4 ">
                         
                            <button class="btn round btn1 btncustom1">  <i class="la la-print"></i> <span > ปริ้น </span> </button>
                            <button class="btn round btn1 btncustom1">  <i class="la la-file-excel-o"></i> <span > Export Excel</span> </button>
                            
                             
                            <button class="btn round btn1 btncustom1 px-1" data-toggle="modal" data-target="#modalGroupNumber">  <i class="la la-list-ol" ></i>   เลขกลุ่ม  </button>
                             
                             

                        </div>
                      


                      </div>
                      
                      <div class="box_tb1">
                        <table id="planManpowerTable4" class="table table-bordered tb1"  >
                          <thead>
                            <tr>
                              <th colspan="3" style="width:10%;"> </th>
                              <th style="width:4%;"> กลุ่ม </th>
                              <th style="width:4%;"> รหัสตำแหน่ง </th>
                              <th style="width:20%;"> คุณวุฒิ</th>
                              <th style="width:10%;"> คุณสมบัติเพิ่มเติม </th>
                              <th style="width:4%;">เพศ</th> 
                              <th style="width:4%;">ที่ตั้งหน่วย</th>
                              <th style="width:4%;">เหล่า</th>
                              <th style="width:20%;"> ตำแหน่ง</th> 
                              <th style="width:4%;">จำนวน</th> 
                            </tr>
                          </thead>
                          <tbody>  
                            <tr> 
                             <td class="text-center" colspan="3"> 
                              <label for="colFormLabellg" class="col-sm-2 col-form-label-lg">
                              <input class="fontcolor1"  type="checkbox" value="" id="defaultCheck1"> </label>
                                <a href="" class="fontcolor1" data-toggle="modal" data-target="#modalPlanPublic"  > <i class="la la-pencil-square-o"></i>  </a>
                                <a href="" class="fontcolor1" data-toggle="modal" data-target="#modalDuplicate "  > <i class="la la-plus"></i>  </a>
                              </td>
                            
                          
                              <td class="text-center">
                                DOF-01
                              </td>
                              <td class="text-center"> 101310  </td>
                              <td >   
                              ปริญญาโท ในสาขาวิชาใดวิชาหนึ่ง ทางความสัมพันธ์ระหว่างประเทศ,
                                ทางการระหว่างประเทศและการทูต, ทางการเมืองและการระหว่างประเทศ,
                                ทางเอเชียศึกษา, ทางเอเชียตะวันออกเฉียงใต้ศึกษา, ทางอาเซียนศึกษา
                              </td>
                              <td class="text-left"> มีคุณวุฒิปริญญาตรี สาขาวิชารัฐศาสตร์  </td> 
                              <td class="text-center"> ชายหรือหญิง  </td>
                              <td class="text-center">  กรุงเทพฯ  </td>
                              <th> นบ.</th>
                              <td class="text-left"> เจ้าหน้าที่เวชสถิติ เจ้าหน้าที่คลัง เจ้าหน้าที่คลังยา </td>  
                              <td class="text-center"> 1 </td>
                            </tr> 
                            

                            <tr> 
                            <td class="text-center" colspan="3"> 
                            <label for="colFormLabellg" class="col-sm-2 col-form-label-lg">
                              <input class="fontcolor1"  type="checkbox" value="" id="defaultCheck1"> </label>
                                <a href="" class="fontcolor1" data-toggle="modal" data-target="#modalPlanPublic"  > <i class="la la-pencil-square-o"></i>  </a>
                                <a href="" class="fontcolor1" data-toggle="modal" data-target="#modalDuplicate "  > <i class="la la-plus"></i>  </a>
                              </td>

                              <td class="text-center">
                                DOF-01
                              </td>
                              <td class="text-center"> 101311 </td>
                              <td >   
                              ปริญญาโท ในสาขาวิชาใดวิชาหนึ่ง ทางความสัมพันธ์ระหว่างประเทศ,
                                ทางการระหว่างประเทศและการทูต, ทางการเมืองและการระหว่างประเทศ,
                                ทางเอเชียศึกษา, ทางเอเชียตะวันออกเฉียงใต้ศึกษา, ทางอาเซียนศึกษา
                              </td>
                              <td class="text-left"> มีคุณวุฒิปริญญาตรี สาขาวิชารัฐศาสตร์  </td> 
                              <td class="text-center"> ชายหรือหญิง  </td>
                              <td class="text-center">  กรุงเทพฯ  </td>
                              <th> นบ.</th>
                              <td class="text-left"> เจ้าหน้าที่สัดทัดงาน เจ้าหน้าที่ห้องปฎิบ้ติการ </td>  
                              <td class="text-center"> 1 </td>
                            </tr> 

                            <tr> 
                            <td class="text-center" colspan="3"> 
                            <label for="colFormLabellg" class="col-sm-2 col-form-label-lg">
                              <input class="fontcolor1"  type="checkbox" value="" id="defaultCheck1"> </label>
                                <a href="" class="fontcolor1" data-toggle="modal" data-target="#modalPlanPublic"  > <i class="la la-pencil-square-o"></i>  </a>
                                <a href="" class="fontcolor1" data-toggle="modal" data-target="#modalDuplicate "  > <i class="la la-plus"></i>  </a>
                              </td>
                              <td class="text-center"> </td>
                              <td class="text-center"> 101312  </td>
                              <td >   
                              ปริญญาโท ในสาขาวิชาใดวิชาหนึ่ง ทางความสัมพันธ์ระหว่างประเทศ,
                                ทางการระหว่างประเทศและการทูต, ทางการเมืองและการระหว่างประเทศ,
                                ทางเอเชียศึกษา, ทางเอเชียตะวันออกเฉียงใต้ศึกษา, ทางอาเซียนศึกษา
                              </td>
                              <td class="text-left"> มีคุณวุฒิปริญญาตรี สาขาวิชารัฐศาสตร์  </td> 
                              <td class="text-center"> ชายหรือหญิง  </td>
                              <td class="text-center">  กรุงเทพฯ  </td>
                              <th> นบ.</th>
                              <td class="text-left"> เจ้าหน้าที่ผังแม่บท </td>  
                              <td class="text-center"> 1 </td>
                            </tr> 

                            <tr> 
                            <td class="text-center" colspan="3"> 
                              <label for="colFormLabellg" class="col-sm-2 col-form-label-lg">
                              <input class="fontcolor1"  type="checkbox" value="" id="defaultCheck1"> </label>
                                <a href="" class="fontcolor1" data-toggle="modal" data-target="#modalPlanPublic"  > <i class="la la-pencil-square-o"></i>  </a>
                                <a href="" class="fontcolor1" data-toggle="modal" data-target="#modalDuplicate "  > <i class="la la-plus"></i>  </a>
                              </td>
                              <td class="text-center"> DOF-02</td>
                              <td class="text-center"> 101313  </td>
                              <td >   
                              ปริญญาโท ในสาขาวิชาใดวิชาหนึ่ง ทางความสัมพันธ์ระหว่างประเทศ,
                                ทางการระหว่างประเทศและการทูต, ทางการเมืองและการระหว่างประเทศ,
                                ทางเอเชียศึกษา, ทางเอเชียตะวันออกเฉียงใต้ศึกษา, ทางอาเซียนศึกษา
                              </td>
                              <td class="text-left"> มีคุณวุฒิปริญญาตรี สาขาวิชารัฐศาสตร์  </td> 
                              <td class="text-center"> ชายหรือหญิง  </td>
                              <td class="text-center">  กรุงเทพฯ  </td>
                              <th> นบ.</th>
                              <td class="text-left"> เจ้าหน้าที่เข้ารหัส เจ้าหน้าที่เทคโนโลยีสารสนเทศและการสื่อสาร 
                                เจ้าหน้าที่แผนที่ดิจิตอล 
                              
                                เจ้าหน้าที่แผนที่ทางอากาศ 
                                เจ้าหน้าที่โปรแกรมคอมพิวเตอร์ เจ้าหน้าที่กรรมวิธีข้อมูล

                                เจ้าหน้าที่ข้อมูลและภาพสามมิติ  เจ้าหน้าที่ปฎิบติการ เจ้าหน้าที่ปฎิบัติการเครื่องจักรคำนวณ เจ้าหน้าที่ปฎิบัติการคอมพิวเตอร์ </td>  
                              <td class="text-center"> 21 </td>
                            </tr> 

                            <tr> 
                            <td class="text-center" colspan="3"> 
                                <label for="colFormLabellg" class="col-sm-2 col-form-label-lg">
                              <input class="fontcolor1"  type="checkbox" value="" id="defaultCheck1"> </label>
                                <a href="" class="fontcolor1" data-toggle="modal" data-target="#modalPlanPublic"  > <i class="la la-pencil-square-o"></i>  </a>
                                <a href="" class="fontcolor1" data-toggle="modal" data-target="#modalDuplicate "  > <i class="la la-plus"></i>  </a>
                              </td>
                              <td class="text-center"> DOF-04</td>
                              <td class="text-center"> 101315  </td>
                              <td >   
                              ปริญญาโท ในสาขาวิชาใดวิชาหนึ่ง ทางความสัมพันธ์ระหว่างประเทศ,
                                ทางการระหว่างประเทศและการทูต, ทางการเมืองและการระหว่างประเทศ,
                                ทางเอเชียศึกษา, ทางเอเชียตะวันออกเฉียงใต้ศึกษา, ทางอาเซียนศึกษา
                              </td>
                              <td class="text-left"> มีคุณวุฒิปริญญาตรี สาขาวิชารัฐศาสตร์  </td> 
                              <td class="text-center"> ชายหรือหญิง  </td>
                              <td class="text-center">  กรุงเทพฯ  </td>
                              <th> นบ.</th>
                              <td class="text-left"> เจ้าหน้าที่ผังแม่บท </td>  
                              <td class="text-center"> 1 </td>
                            </tr> 
                            <tr>

                          </tbody>
                        </table>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <!--/ Bootstrap 3 table -->
        </div>
      </div>
    </div>
  </section>

  
  
 

    <!-- Modal -->
   <div class="modal fade text-left modal_custom1" id="modalPlanPublic" tabindex="-1" role="dialog" aria-labelledby="modalPlanPublic"  aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header bg_custom1">
            <h5 class="modal-title">แก้ไขข้อมูล </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form class="form_custom1">
            <div class="modal-body">
              <div class="row">
                <div class="col-10 m-auto pt-2">  

                  <div class="row">
                    <div class="col-md-3 col-10">
                      <div class="form-group">
                        <label for="groupname"> กลุ่ม : </label>
                        <input type="text" class="form-control" id="groupname" name="groupname" value="DOF-01" >  
                      </div>
                    </div>
                    <div class="col-md-3 col-10">
                      <div class="form-group">
                        <label for="positioncode"> รหัสตำแหน่ง : </label>
                        <input type="text" class="form-control" id="positioncode" name="positioncode" value="101310" >  
                      </div>
                    </div>
                    <div class="col-md-3 col-10">
                      <div class="form-group">
                        <label for="provinceaddr"> ที่ตั้งหน่วย : </label>
                          <select  id="provinceaddr"  name="provinceaddr" class="form-control block " disabled >
                            <option value="1"> กรุงเทพฯ </option>
                            <option value="2"> กระบี่  </option>
                            <option value="3"> กาฬสินธุ์ </option>
                          </select> 
                      </div>
                    </div>
                    <div class="col-md-3 col-10">
                      <div class="form-group">
                        <label for="positionno"> จำนวน : </label>
                        <input type="number" class="form-control text-right" id="positionno" name="positionno" value="15" disabled >  
                      </div>
                    </div>
                  </div> 
          
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="edu_degree"> คุณวุฒิ :   </label>  
                        <textarea class="form-control" name="edu_degree" id="edu_degree" cols="12" rows="5" disabled>ปริญญาโท ในสาขาวิชาใดวิชาหนึ่ง ทางความสัมพันธ์ระหว่างประเทศ, ทางการระหว่างประเทศและการทูต, ทางการเมืองและการระหว่างประเทศ, ทางเอเชียศึกษา, ทางเอเชียตะวันออกเฉียงใต้ศึกษา, ทางอาเซียนศึกษา </textarea>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="spcfeatures"> เหล่า :   </label>
                        <input type="textx" class="form-control" id="positionno" name="positionno" value="นบ." disabled >  

                      </div>
                    </div>  

                  

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="spcfeatures"> เพศ :   </label>
                        <div class="row skin icheck_minimal ">  
                          <div class="col-sm-12 "> 
                            <fieldset class="d-inline-block mr-1" >
                              <input type="radio" name="input-radio-3" id="input-radio-11" checked disabled>
                              <label for="input-radio-11">ชาย/หญิง</label>
                            </fieldset>
                            <fieldset class="d-inline-block mr-1" >
                              <input type="radio" name="input-radio-3" id="input-radio-12" disabled >
                              <label for="input-radio-12">ชาย</label>
                            </fieldset>
                            <fieldset class="d-inline-block  " >
                              <input type="radio" name="input-radio-3" id="input-radio-13" disabled>
                              <label for="input-radio-13">หญิง</label>
                            </fieldset>
                          </div>  
                        </div>
                      </div>
                    </div> 
                  </div>

                  <div class="row">
                    <div class="col-md-10">
                      <div class="form-group">
                        <label for="spcfeatures"> คุณสมบัติเฉพาะเพิ่มเติม :   </label>
                        <select  id="provinceaddr"  name="provinceaddr" class="form-control block " >
                            <option value="1"> คุณสมบัติเฉพาะเพิ่มเติม 1  </option>
                            <option value="2"> คุณสมบัติเฉพาะเพิ่มเติม 2 </option>
                            <option value="3"> คุณสมบัติเฉพาะเพิ่มเติม 3 </option>
                          </select> 
                      </div>
                    </div> 
                    <div class="col-md-2 text-center mt-2 pt-1">
                    <button class="btn round btn1 btncustom1">  <i class="fa fa-plus"></i> <span >เพิ่ม </span> </button>

                 
                    </div>

                  </div>

 

                  <div class="row">
                    <div class="col-md-12">
                    <div class="table-responsive mx-2">

                   <table class="table table-striped table-border table-hover">
                      <thead class="text-center">
                          <tr class="justify-content-center" style="background-color:#0f1733; color:whitesmoke;">
                              
                                <th>ลำดับที่</th>
                                <th>ตำแหน่ง</th>
                                <th>จำนวน</th>
                            </tr>
                        </thead>

                        <tbody class="text-center">
                        <tr>
                                  
                                  <td>1</td>
                                  <td> เจ้าหน้าที่เวชสถิติ </td>
                                  <td class="text-center"> 2 </td>
                            </tr>
                            <tr>
                                  
                                  <td>2</td>
                                  <td> เจ้าหน้าที่คลัง</td>
                                  <td class="text-center"> 1</td>
                            </tr>
                            <tr>
                                  
                                  <td>3</td>
                                  <td> เจ้าหน้าที่คลังยา</td>
                                  <td class="text-center"> 4 </td>
                            </tr>
                            <tr>
                                  <td colspan="2"> รวมทั้งหมด  </td>  
                                  <td class="text-center"> 7 </td>
                            </tr>
                        </tbody>
                        
                          </table>
                          </div>
                    </div>

            

                  </div>

                </div>
              </div>
            </div>
            <div class="modal-footer">
              <div  class="m-auto ">
                <button type="button" class="btn btn2 btn-success   round"> <i class="fa fa-save"></i> บันทึก </button>
                <button type="button" class="btn btn2 btn-danger   round"  data-dismiss="modal"> <i class="fa fa-times-circle-o"></i> ยกเลิก </button> 
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>


     <!-- Modal -->
   <div class="modal fade text-left modal_custom1" id="modalDuplicate" tabindex="-1" role="dialog" aria-labelledby="modalDuplicate"  aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header bg_custom1">
            <h5 class="modal-title">ทำซ้ำข้อมูล </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form class="form_custom1">
            <div class="modal-body">
              <div class="row">
                <div class="col-10 m-auto pt-2">  

                  <div class="row">
                    <div class="col-md-3 col-10">
                      <div class="form-group">
                        <label for="groupname"> กลุ่ม : </label>
                        <input type="text" class="form-control" id="groupname" name="groupname" value="DOF-01" >  
                      </div>
                    </div>
                    <div class="col-md-3 col-10">
                      <div class="form-group">
                        <label for="positioncode"> รหัสตำแหน่ง : </label>
                        <input type="text" class="form-control" id="positioncode" name="positioncode" value="101310" >  
                      </div>
                    </div>
                    <div class="col-md-3 col-10">
                      <div class="form-group">
                        <label for="provinceaddr"> ที่ตั้งหน่วย : </label>
                          <select  id="provinceaddr"  name="provinceaddr" class="form-control block " >
                            <option value="1"> กรุงเทพฯ </option>
                            <option value="2"> กระบี่  </option>
                            <option value="3"> กาฬสินธุ์ </option>
                          </select> 
                      </div>
                    </div>
                    <div class="col-md-3 col-10">
                      <div class="form-group">
                        <label for="positionno"> จำนวน : </label>
                        <input type="number" class="form-control text-right" id="positionno" name="positionno" value="15"  >  
                      </div>
                    </div>
                  </div> 
          
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="edu_degree"> คุณวุฒิ :   </label>  
                        <textarea class="form-control" name="edu_degree" id="edu_degree" cols="12" rows="5" >ปริญญาโท ในสาขาวิชาใดวิชาหนึ่ง ทางความสัมพันธ์ระหว่างประเทศ, ทางการระหว่างประเทศและการทูต, ทางการเมืองและการระหว่างประเทศ, ทางเอเชียศึกษา, ทางเอเชียตะวันออกเฉียงใต้ศึกษา, ทางอาเซียนศึกษา </textarea>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="spcfeatures"> เหล่า :   </label>
                        <input type="text" class="form-control" id="positionno" name="positionno" value=" นบ."  >  

                      </div>
                    </div>  

                  

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="spcfeatures"> เพศ :   </label>
                        <div class="row skin icheck_minimal ">  
                          <div class="col-sm-12 "> 
                            <fieldset class="d-inline-block mr-1" >
                              <input type="radio" name="input-radio-3" id="input-radio-11" checked >
                              <label for="input-radio-11">ชาย/หญิง</label>
                            </fieldset>
                            <fieldset class="d-inline-block mr-1" >
                              <input type="radio" name="input-radio-3" id="input-radio-12" >
                              <label for="input-radio-12">ชาย</label>
                            </fieldset>
                            <fieldset class="d-inline-block  " >
                              <input type="radio" name="input-radio-3" id="input-radio-13" >
                              <label for="input-radio-13">หญิง</label>
                            </fieldset>
                          </div>  
                        </div>
                      </div>
                    </div> 

                  </div>

                  <div class="row">
                    <div class="col-md-10">
                      <div class="form-group">
                        <label for="spcfeatures"> คุณสมบัติเฉพาะเพิ่มเติม :   </label>
                        <select  id="provinceaddr"  name="provinceaddr" class="form-control block " >
                            <option value="1"> คุณสมบัติเฉพาะเพิ่มเติม 1  </option>
                            <option value="2"> คุณสมบัติเฉพาะเพิ่มเติม 2 </option>
                            <option value="3"> คุณสมบัติเฉพาะเพิ่มเติม 3 </option>
                          </select> 
                      </div>
                    </div> 
                    <div class="col-md-2 text-center mt-2 pt-1">
                    <button class="btn round btn1 btncustom1">  <i class="fa fa-plus"></i> <span >เพิ่ม </span> </button>

                 
                    </div>

                  </div>


                  <div class="row">
                      <div class="col-md-10 m-1">
                      <button class="btn round btn1 btncustom1">  <i class="fa fa-plus"></i> <span >เพิ่ม </span> </button>
                      </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                    <div class="table-responsive mx-2">

                   <table class="table table-striped table-border table-hover">
                      <thead class="text-center">
                          <tr class="justify-content-center" style="background-color:#0f1733; color:whitesmoke;">
                                <th></th>
                                <th>ลำดับที่</th>
                                <th>ตำแหน่ง</th>
                                <th> จำนวน </th>
                            </tr>
                        </thead>

                        <tbody class="text-center">
                            <tr>
                                  <td>
                                      <a href="./delete.php"><i class="la la-trash-o" style="color:#0f1733;"></i></a>
                                  </td>
                                  <td>1</td>
                                  <td> เจ้าหน้าที่เวชสถิติ </td>
                                  <td class="text-center"> 2 </td>
                            </tr>
                            <tr>
                                  <td>
                                      <a href="./delete.php"><i class="la la-trash-o" style="color:#0f1733;"></i></a>
                                  </td>
                                  <td>2</td>
                                  <td> เจ้าหน้าที่คลัง</td>
                                  <td class="text-center"> 1</td>
                            </tr>
                            <tr>
                                  <td>
                                      <a href="./delete.php"><i class="la la-trash-o" style="color:#0f1733;"></i></a>
                                  </td>
                                  <td>3</td>
                                  <td> เจ้าหน้าที่คลังยา</td>
                                  <td class="text-center"> 4 </td>
                            </tr>
                            <tr>
                                  <td colspan="3"> รวมทั้งหมด  </td>  
                                  <td class="text-center"> 7 </td>
                            </tr>
                        </tbody>
                        
                          </table>
                          </div>
                    </div>

                  </div>

                </div>
              </div>
            </div>
            <div class="modal-footer">
              <div  class="m-auto ">
                <button type="button" class="btn btn2 btn-success round"> <i class="fa fa-save"></i> ทำซ้ำข้อมูล </button>
                <button type="button" class="btn btn2 btn-danger round"  data-dismiss="modal"> <i class="fa fa-times-circle-o"></i> ยกเลิก </button> 
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>


   <!-- Modal -->
   <div class="modal fade text-left modal_custom1" id="modalGroupNumber" tabindex="-1" role="dialog" aria-labelledby="modalGroupNumber"  aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header bg_custom1">
            <h5 class="modal-title">สร้างเลขกลุ่ม</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form class="form_custom1">
            <div class="modal-body">
              <div class="row">
                <div class="col-md-6 m-auto pt-2">  

                  <div class="row m-auto">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="groupname"> กลุ่ม : </label>
                              <input type="text" class="form-control text-center" id="groupname" name="groupname" value="DOF-01" >  
                            </div>
                          </div>
                    </div>
                   
            <div class="modal-footer">
              <div  class="m-auto ">
                <button type="button" class="btn btn2 btn-success round"> <i class="fa fa-save"></i> บันทึกข้อมูล </button>
                <button type="button" class="btn btn2 btn-danger round"  data-dismiss="modal"> <i class="fa fa-times-circle-o"></i> ยกเลิก </button> 
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
 
 
    <!-- footer -->
  
  
    <?php include '../include/footer.php'; ?> 

  <script src="../../Controllers/planManpowerController.js"></script>
  <script type="text/javascript">
    $(document).ready(function() { 

      $("#planManTypeId").change(function(){
        // alert( $(this).val() );
         $('.tb1').addClass('hidden');

         $('#planManTypeId2').addClass('hidden'); 
         if( $(this).val() == 1 || $(this).val() == 0){
            $('#planManpowerTable1').removeClass('hidden');
            
            $('.boxsearchplan2').removeClass('hidden');
            $('.boxsearchplan1').addClass('hidden');


         }else if( $(this).val() ==2 ){ 
          
            $('.boxsearchplan2').addClass('hidden');
            $('.boxsearchplan1').removeClass('hidden');

            $('#planManTypeId2').removeClass('hidden'); 
            $('#planManpowerTable2').removeClass('hidden'); 
         }else if( $(this).val() == 3 ) {
            $('#planManpowerTable4').removeClass('hidden');

            $('.boxsearchplan2').addClass('hidden');
            $('.boxsearchplan1').removeClass('hidden');

         }

      });

      $("#planManTypeId2").change(function(){
        $('.tb1').addClass('hidden');
         if( $(this).val() ==22 ){
            $('#planManpowerTable3').removeClass('hidden'); 
          }else{
            $('#planManpowerTable2').removeClass('hidden'); 
          }
      });


    }); 
  </script>
  