  <!-- header -->
  <?php include '../include/header.php'; ?>
  <?php include '../../Model/Ducklab/func.php'; ?>
  <?php  
    //Recruitment and appointment
    $menu1 = "RECRNAPPOINT" ;
    $menu2 = "PlanManpower" ;

  ?>
  <!-- menu -->
  <?php include '../include/menu.php'; ?>
  
  <section>
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
          <h3 class="content-header-title">แผนการบรรจุกำลังพล </h3>
        </div>
      </div>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb"> 
          <li class="breadcrumb-item"><a href="../home/index.php">หน้าแรก </a></li> 
          <li class="breadcrumb-item"><a href="index.php">ระบบงานสรรหาและบรรจุกำลังพล</a></li> 
          <li class="breadcrumb-item" > <a href="positionmatch.php">นำผู้รายชื่อผู้สอบผ่านลงตำแหน่ง</a> </li>
        </ol>
      </nav>
      <div class="content-body">
         <!-- Bootstrap 3 table -->
         <section id="bootstrap3">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                   
                    <div class="row"> 
                      <div class="col-sm-12">
                        <div class="box_h1 card">
                          <div class="card-header card-head-inverse  ">
                            <h4 class="card-title text-white"> เวลาเปิด/ปิด แก้ไขข้อมูล </h4> 
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                              </ul>
                            </div>
                          </div>
                          <div class="card-content collapse show">
                            <div class="card-body">
                              <div class="row">
                                <div class=" col-md-8  m-auto ">
                                 
                                  <form class="form">
                                    <div class="form-body"> 
 

                                      <div class="boxsearchplan2">
                                        <div class="row">
                                          <div class="col-md-3">
                                            <div class="form-group">  
                                              <label for="positionno"> ปีงบประมาณ : </label>
                                              <input type="text" class="form-control" id="" name="" value="2562" disabled >
                                            </div>
                                          </div>  
                                          <div class="col-md-6">
                                            <div class="form-group">  
                                              <label for="positionno"> ชื่อแผน : </label>
                                              <input type="text" class="form-control" id="" name="" value="แผนการบรรจุประจำปี 2562" disabled>
                                            </div>
                                          </div> 
                                          <div class="col-md-3 col-10">
                                            <div class="form-group">
                                              <label for="positionno"> จำนวน (ตำแหน่ง) : </label>
                                              <input type="number" class="form-control " id="positionno" name="positionno" value="452" disabled >  
                                            </div>
                                          </div>
                                        </div>
                                      </div>  <!-- ./boxsearchplan2 -->
                                    
                                    </div> 
<!-- 
                                    <div class="text-center">
                                      <button type="button" class="btn round btn1 btncustom1 mr-1">
                                        <i class="fa fa-search"></i> ค้นหา
                                      </button>
                                      <button type="button" class="btn round btn1 btncustom1">
                                        <i class=" fa fa-repeat"></i> ล้างค่า
                                      </button>
                                    </div> -->

                                  </form>

                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div> 

                    <div class="row hidden"> 
                      <div class="col-md-3 col-sm-12">
                        <div class="box1 txtdesc">
                            ข้อมูลจำนวน <span> 500 </span> อัตรา
                        </div>
                      </div> 
                    </div>
                    <div class="row hidden">
                      <div class="col-6 ">
                         <button class="btn round btn1 btncustom1">  <i class="la la-print"></i> <span > ปริ้น </span> </button>
                         <button class="btn round btn1 btncustom1">  <i class="la la-file-excel-o"></i> <span > Export Excel</span> </button>
                      </div>
                      <div class="col-6 text-right">
                        <a href="" class="fontcolor1" data-toggle="modal" data-target="#modalPlanPublic"  > <i class="la la-pencil-square-o"></i>  </a>

                        <button class="btn round btn1 btncustom1"  data-toggle="modal" data-target="#modalSettingRegis">  <i class="la la-calendar"></i> <span >  เปิด/ปิด รับสมัคร </span> </button>
                      </div>
                    </div>

                    <div class="row"> 
                      <div class="col-md-12">
                         
                      </div> 
                    </div>





                    <div class="row"> 
                      <div class="col-md-12 col-lg-10 m-auto">
                        <div class="box_h2 card">
                          <div class="card-header">
                            <h4 class="card-title"><span class="fontcolor1"><u> ส่วนที่ 1</u> </span>&nbsp;&nbsp; เปิด/ปิด ข้อมูล</h4> 
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                              </ul>
                            </div>
                          </div>
                          <div class="card-content collapse show">
                            <div class="card-body">
                              <div class="row">
                                <div class=" col-md-9  m-auto ">
                                 
                                  <form class="form">
                                    <div class="form-body">

                                  
                                  


                                    <div class="row">
                                      
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="startdate"> ตั้งเเต่วันที่ :   </label> 
                                          <div class="input-group datep">
                                            <input type="text"  style="width:56%" class="form-control form2 pickadate-translations"   placeholder="" id="startdate" name="startdate" data-value="<?php echo GetToday('');?>" />
                                            <div class="input-group-append">
                                              <span class="input-group-text">
                                                <span class="la la-calendar-o"></span>
                                              </span>
                                            </div>
                                          </div>
                                        </div>
                                      </div>

              
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="starttime">  เวลา : </label> 
                                          <div class="input-group datep"> 
                                            <input type="text"   style="width:46%"class="form-control form2 pickatime"  id="starttime" name="starttime"  value="00.00"/>
                                            <div class="input-group-append">
                                              <span class="input-group-text">
                                                <span class="la la-clock-o"></span>
                                              </span>
                                            </div>
                                          </div> 
                                        </div>
                                      </div>
                                    </div> 

                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="enddate"> ถึงวันที่ :   </label> 
                                          <div class="input-group datep">
                                            <input type="text"  style="width:56%" class="form-control form2 pickadate-translations" placeholder="" id="enddate" name="enddate" data-value="2019-05-27" />
                                            <div class="input-group-append">
                                              <span class="input-group-text">
                                                <span class="la la-calendar-o"></span>
                                              </span>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="endtime">  เวลา : </label> 
                                          <div class="input-group datep"> 
                                            <input type="text"  style="width:46%" class="form-control form2 pickatime" id="endtime" name="endtime" value="16.00" />
                                            <div class="input-group-append">
                                              <span class="input-group-text">
                                                <span class="la la-clock-o"></span>
                                              </span>
                                            </div>
                                          </div> 
                                        </div>
                                      </div>
                                    </div>


                                    <div class="row">
                                      <div class="col-md-3 col-10">
                                        <div class="form-group">
                                          <label for="startdate"> จำนวนวัน : 
                                          </label>
                                          <input type="number" class="form-control" id="numdate" value="10" disabled  >  
                                        </div>
                                      </div>
                                      <div class="col-md-6 col-2">
                                        <div class="form-group form_txt">
                                          <label for="">  &nbsp; </label>
                                          <br>  วัน
                                        </div>
                                      </div>
                                      
                                    </div> 


                                    </div>  
                                  </form>

                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>  <!-- /.row  -->

                    <div class="row"> 
                      <div class="col-sm-10 m-auto">
                        <div class="box_h2 card">
                          <div class="card-header">
                            <h4 class="card-title"><span class="fontcolor1"><u> ส่วนที่ 2</u> </span>&nbsp;&nbsp; นำเข้าข้อมูล </h4> 
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                              </ul>
                            </div>
                          </div>
                          <div class="card-content collapse show">
                            <div class="card-body"> 
                              <form class="form_custom1">
                                <div class="row">
                                  <div class=" col-md-8  m-auto ">   
                                  <div class="row">
                                  <div class="col-md-12">
                                  <div class="col-md-6">
                                    <label for="exampleInputEmail1">ประเภทกำลังพล:</label>

                                    <select id="projectinput5" name="interested" class="form-control ">
                                                    <option value=""></option>
                                                    <option value=""></option>
                                                    <option value=""></option>
                                    </select>

                                  </div>
                                  </div>
                                  </div>

                                  <div class="row mt-1">
                                        <div class="col-md-12">
                                          <div class="pb-2">
                                            <span class="fontcustom1"> *กรุณากดปุ่ม Import เพื่อนำเข้าข้อมูล เพื่อสามารถกรอกข้อมูลในลำดับถัดไปได้ </span>
                                          </div>
                                          <button type="button" class="btn round btn1 btncustom1 mr-1">
                                            <i class="fa fa-upload"></i>&nbsp; Import 
                                          </button> 
                                          <span class="boxcustom3">  ข้อมูลผู้มีรายชื่อสอบผ่านจริงและสำรอง2562.xlsx
                                            <a href="#" class="btn round fontcolor2"> 
                                              <i class="fa fa-times-circle-o"></i>
                                             </a>
                                          </span>
                                        </div>   
                                      </div> 
                            
                                  </div>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>  <!-- /.row  -->


                    <div class="row"> 
                      <div class="col-md-12 col-lg-10 m-auto">
                        <div class="box_h2 card">
                          <div class="card-header">
                            <h4 class="card-title"><span class="fontcolor1"><u> ส่วนที่ 3</u> </span>&nbsp;&nbsp; กรอกข้อมูลตำแหน่ง </h4> 
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                              </ul>
                            </div>
                          </div>
                          <div class="card-content collapse show">
                            <div class="card-body">
                              <div class="row">
                                <div class=" col-md-12 ">  
                                  <div class="box1 txtdesc">
                                      ข้อมูลจำนวนตำแหน่ง <span> 5 </span> ตำแหน่ง  จำนวน 10 คน
                                  </div>
                                </div>
                              </div>
                              
                              <div class="row">
                                <div class=" col-md-12 ">   
                                  <table id="planManpowerTable2" class="table  tb1   "  >
                                    <thead>
                                      <tr>
                                        <th> ลำดับที่ </th> 
                                       
                                        <th> ตำแหน่ง</th> 	
                                        <th> คุณวุฒิ	</th> 
                                        <th> เพศ	</th> 
                                        <th> อัตรา	</th> 
                                      
                                        <th>เหล่า	</th> 
                                        <th>ลชทอ.	</th> 
                                        <th> จำพวก	</th> 
                                        <th> จำนวน </th>
                                        <th> สถานะ</th> 
                                        <th> ชื่อ-สกุล </th> 
                                        



                                      </tr>
                                    </thead>
                                    <tbody> 
                                    
                                      <tr>
                                        <td class="text-center"> 1 </td>
                                        <td class=""> อาจารย์ ภาควิชาการพยาบาลผู้ใหญ่ กองการศึกษา วพอ.พอ.  </td>
                                        <td class="text-center"> </td>
                                        <td class="text-center"> </td>
                                        <td class="text-center"> </td>
                                        <td class="text-center"> </td>
                                        <td class="text-center"> </td>
                                        <td class="text-center"> </td>




                                        <td class="text-center">  1 </td> 
                                        <td class="text-center">
                                          <div class="badge badge_c2 badge-info  ">
                                            <span> ตัวจริง </span>  
                                          </div>
                                          <div class="badge badge_c2 badgebg1">
                                            <span> สำรอง </span>  
                                          </div>
                                        </td>  
                                        <td class="formset1">   
                                          <div class="form-group">
                                            <select class="form-control select2  "  style="width:100%" >
                                              <option value="">  นางสาวจริงใจ  เสริมดี </option>
                                            </select> 
                                          </div>
                                          <div class="form-group">
                                            <select class="form-control select2"  style="width:100%">
                                              <option value="">  นางสาวจริงใจ  เสริมดี </option>
                                            </select> 
                                          </div>
                                        </td>
                                      </tr> 

                                      <tr>
                                        <td class="text-center"> 2 </td>
                                        <td class=""> อาจารย์ ภาควิชาการพยาบาลผู้ใหญ่ กองการศึกษา วพอ.พอ.  </td>
                                        <td class="text-center"> </td>
                                        <td class="text-center"> </td>
                                        <td class="text-center"> </td>
                                        <td class="text-center"> </td>
                                        <td class="text-center"> </td>
                                        <td class="text-center"> </td>
                                        <td class="text-center">  1 </td> 
                                        <td class="text-center">
                                          <div class="badge badge_c2 badge-info  ">
                                            <span> ตัวจริง </span>  
                                          </div>
                                          <div class="badge badge_c2 badgebg1">
                                            <span> สำรอง </span>  
                                          </div>
                                        </td>  
                                        <td class="formset1">   
                                          <div class="form-group">
                                            <select class="form-control select2  "  style="width:100%" >
                                              <option value="">  นางสาวจริงใจ  เสริมดี </option>
                                            </select> 
                                          </div>
                                          <div class="form-group">
                                            <select class="form-control select2"  style="width:100%">
                                              <option value="">  นางสาวจริงใจ  เสริมดี </option>
                                            </select> 
                                          </div>
                                        </td>
                                      </tr> 

                                
                                      <tr>
                                        <td class="text-center"> 3 </td>
                                        <td class=""> อาจารย์ ภาควิชาการพยาบาลผู้ใหญ่ กองการศึกษา วพอ.พอ.  </td>
                                        <td class="text-center"> </td>
                                        <td class="text-center"> </td>
                                        <td class="text-center"> </td>
                                        <td class="text-center"> </td>
                                        <td class="text-center"> </td>
                                        <td class="text-center"> </td>
                                        <td class="text-center">  1 </td> 
                                        <td class="text-center">
                                          <div class="badge badge_c2 badge-info  ">
                                            <span> ตัวจริง </span>  
                                          </div>
                                          <div class="badge badge_c2 badgebg1">
                                            <span> สำรอง </span>  
                                          </div>
                                        </td>  
                                        <td class="formset1">   
                                          <div class="form-group">
                                            <select class="form-control select2  "  style="width:100%" >
                                              <option value="">  นางสาวจริงใจ  เสริมดี </option>
                                            </select> 
                                          </div>
                                          <div class="form-group">
                                            <select class="form-control select2"  style="width:100%">
                                              <option value="">  นางสาวจริงใจ  เสริมดี </option>
                                            </select> 
                                          </div>
                                        </td>
                                      </tr> 

                                     




                                      

                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>  <!-- /.row  -->



                   

                    <div class="row"> 
                      <div class="col-sm-10 m-auto">
                        <div  class="text-center">
                          <button type="button" class="btn btn2 btn-success   round" data-toggle="modal" data-target="#modalConfirm" > <i class="fa fa-save"></i> บันทึก </button>
                          <button type="button" class="btn btn2 btn-danger   round"  data-dismiss="modal"> <i class="fa fa-times-circle-o"></i> ยกเลิก </button> 
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--/ Bootstrap 3 table -->
      </div>
    </div>
  </div>
  </section>


  <div class="modal fade text-left modal_custom1" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="modalConfirm"  aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <form>
          <div class="my-2 mx-2 px-2 py-2">
            <div class="text-center"> 
              <img src="../../Asset/Images/send.png">
              <div class="py-3">
                <h1> ยืนการส่งข้อมูลหรือไม่ ?</h1>
                <h6>กรุณาเลือกปุ่มใช่หรือไม่ใช่่</h6>
              </div>
            </div> 
            <div class="col-12 d-flex justify-content-center">
              <div class="row"> 
                  <a href="#" class="btn btn-min-width mb-1 mr-1 round btn-success" role="button" >
                  <span></span>ใช่</a> 
                  <a href="#" class="btn btn-min-width mb-1 round btn-danger"   data-dismiss="modal" role="button">
                  <span></span> ไม่ใช่ </a> 
              </div>
            </div>
          </div>
        </form>
      </div> 
    </div> 
  </div>


 
  <!-- footer -->
  <?php include '../include/footer.php'; ?> 

  <script src="../../Controllers/planManpowerController.js"></script>
  <script type="text/javascript">
    $(document).ready(function() { 

      $("#planManTypeId").change(function(){
        // alert( $(this).val() );
         $('.tb1').addClass('hidden');

         $('#planManTypeId2').addClass('hidden'); 
         if( $(this).val() == 1 || $(this).val() == 0){
            $('#planManpowerTable1').removeClass('hidden');
         }else if( $(this).val() ==2 ){ 
            $('#planManTypeId2').removeClass('hidden'); 
            $('#planManpowerTable2').removeClass('hidden'); 
         }else if( $(this).val() == 3 ) {
            $('#planManpowerTable4').removeClass('hidden');
         }

      });

      $("#planManTypeId2").change(function(){
        $('.tb1').addClass('hidden');
         if( $(this).val() ==22 ){
            $('#planManpowerTable3').removeClass('hidden'); 
          }else{
            $('#planManpowerTable2').removeClass('hidden'); 
          }
      });


    }); 
  </script>
  