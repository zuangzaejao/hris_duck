  <!-- header -->
  <?php include '../include/header.php'; ?>
  <?php include '../../Model/Ducklab/func.php'; ?>
  <?php  
    //Recruitment and appointment
    $menu1 = "RECRNAPPOINT" ;
    $menu2 = "PlanManpower" ;

  ?>
  <!-- menu -->
  <?php include '../include/menu.php'; ?>
  
  <section>
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
          <h3 class="content-header-title">รายชื่อผู้สมัครสอบ น.ประทวนเลื่อนฐานะ </h3>
        </div>
      </div>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb"> 
          <li class="breadcrumb-item"><a href="../home/index.php">หน้าแรก </a></li> 
          <li class="breadcrumb-item"><a href="index.php">ระบบงานสรรหาและบรรจุกำลังพล</a></li> 
          <li class="breadcrumb-item" > <a href="regislist.php"> รายชื่อผู้สมัครสอบ </a> </li>
          
        </ol>
      </nav>
      <div class="content-body">
         <!-- Bootstrap 3 table -->
         <section id="bootstrap3">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
 
                    <div class="row"> 
                      <div class="col-sm-12">
                        <div class="box_h1 card">
                          <div class="card-header card-head-inverse  ">
                            <h4 class="card-title text-white"> ค้นหา </h4> 
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                              </ul>
                            </div>
                          </div>
                          <div class="card-content collapse show">
                            <div class="card-body">
                              <div class="row">
                                <div class=" col-md-8  m-auto ">
                                
                                  <form class="form">
                                    <div class="form-body"> 
 
                                      <div class="boxsearchplan2">
                                       <div class="row">
                                          <div class="col-md-6">
                                            <div class="form-group">  
                                              <input type="text" id="" class="form-control " placeholder="ชื่อ-สกุล" name="">  
                                            </div>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <select id="" name="" class="form-control ">
                                                <option value="">คุณวุฒิ</option>
                                                <option value="">มัธยมศึกษาตอนต้น</option>
                                                <option value="">มัธยมศึกษาตอนปลาย</option>
                                                <option value="">ปวช.</option>
                                                <option value="">ปวส.</option>
                                                <option value="">ปริญญาตรี</option>
                                                <option value="">ปริญญาโท</option>
                                                <option value="">ปริญญาเอก</option>
                                              </select>
                                            </div>
                                          </div> 
                                          <div class="col-md-6">
                                            <div class="form-group">  
                                                <select id="projectinput5" name="interested" class="form-control ">
                                                <option value="development">ตำแหน่ง</option>
                                                    <option value=""> จเรฝ่ายยุทธการและนิรภัยการบิน </option>
                                                    <option value=""> ผู้ช่วยจเรฝ่ายยุทธการและนิรภัยการบิน </option>
                                                    <option value="">จเรฝ่ายข่าว  </option>
                                                </select>
                                              </div>
                                          </div>
                                        </div>
                                          
                                      </div>  <!-- ./boxsearchplan2 -->
                                    
                                    </div> 

                                    <div class="text-center">
                                      <button type="button" class="btn round btn1 btncustom1 mr-1">
                                        <i class="fa fa-search"></i> ค้นหา
                                      </button>
                                      <button type="button" class="btn round btn1 btncustom1">
                                        <i class=" fa fa-repeat"></i> ล้างค่า
                                      </button>
                                    </div>

                                  </form>

                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div> 
 
                    <div class="row hidden"> 
                      <div class="col-md-3 col-sm-12">
                        <div class="box1 txtdesc">
                            ข้อมูลจำนวน <span> 500 </span> อัตรา
                        </div>
                      </div> 
                    </div>
                    <div class="row ">
                      <div class="col-12 ">
                         <button class="btn round btn1 btncustom1">  <i class="la la-print"></i> <span > ปริ้น </span> </button>
                         <button class="btn round btn1 btncustom1">  <i class="la la-file-excel-o"></i> <span > Export Excel</span> </button>
                      </div>
                    </div>

                    <ul class="nav nav-tabs nav-top-border no-hover-bg mt-2 mb-1"  >
                      <li class="nav-item">
                        <a class="nav-link active" id="base-tab11" data-toggle="tab" aria-controls="tab11" href="#tab11" aria-expanded="true">ดูตามตำแหน่ง </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="base-tab12" data-toggle="tab" aria-controls="tab12" href="#tab12" aria-expanded="false"> ดูตามรายชื่อ</a>
                      </li>
                    </ul>


                    <div class="tab-content pt-1"> 
                        <div role="tabpanel" class="tab-pane active" id="tab11" aria-expanded="true" aria-labelledby="base-tab11" >
                          <div class="box_tb1">
                            <table id="planManpowerTable3" class="table table-bordered tb1    "  >
                                <thead>
                                  <tr>
                                    <th> ลำดับที่ </th>
                                    <th style="width:32%;"> ตำแหน่ง</th>
                                    <th> จำนวน </th>
                                    <th> เพศ </th> 
                                    
                                    <th  style="width:40%;"> รายชื่อ-สกุล </th>
                                    <th > วันที่สมัคร </th>  
                                  </tr>
                                </thead>
                                <tbody> 
                                  <tr> 
                                    <td  rowspan="2"  class="text-center"> 1 </td>
                                    <td  rowspan="2" >   
                                      รองผู้บังคับหมวดนักเรียนพยาบาลทหารอากาศ ฝ่ายปกครอง แผนกปกครอง วพอ.พอ. 
                                    </td>
                                    <td class="text-center" rowspan="2">4 </td>

                                    <td class="text-center"> ชาย </td>
                                    <td class="text-center"> นายศรี ส่งเสริม </td> 
                                    <td class="text-center"> 15 ก.พ. 2562 </td> 
                                  </tr> 
                                  <tr >    
                                    <td class="text-center"> ชาย </td>
                                    <td class="text-center"> นายศรี ส่งเสริม </td> 
                                    <td class="text-center"> 15 ก.พ. 2562 </td> 
                                  </tr> 
                                
                                  <tr> 
                                    <td  rowspan="4"  class="text-center"> 2 </td>
                                    <td  rowspan="4" >   
                                    นายทหารสันทัดงาน กวห.รพ.ภูมิพลอดุลยเดชพอ.
                                    </td>
                                    <td class="text-center" rowspan="4">4 </td>
                                  
                                    <td class="text-center"> ชาย </td>
                                    <td class="text-center"> นายศรี ส่งเสริม </td> 
                                    <td class="text-center"> 15 ก.พ. 2562 </td> 
                                  </tr> 
                                  
                                  <tr >    
                                    <td class="text-center"> ชาย </td>
                                    <td class="text-center"> นายศรี ส่งเสริม </td> 
                                    <td class="text-center"> 15 ก.พ. 2562 </td> 
                                  </tr> 
                                  <tr >    
                                    <td class="text-center"> ชาย </td>
                                    <td class="text-center"> นายศรี ส่งเสริม </td> 
                                    <td class="text-center"> 15 ก.พ. 2562 </td> 
                                  </tr> 
                                  <tr >    
                                    <td class="text-center"> ชาย </td>
                                    <td class="text-center"> นายศรี ส่งเสริม </td> 
                                    <td class="text-center"> 15 ก.พ. 2562 </td> 
                                  </tr>  

                                  <tr> 
                                    <td  rowspan="5"  class="text-center"> 3 </td>
                                    <td  rowspan="5" >   
                                    นายทหารตรวจสอบ ชุดตรวจที่ ๒ กตจ.๑ กตส.๑ สตน.ทอ.
                                    </td>
                                    <td class="text-center" rowspan="5">4 </td>
                                  
                                    <td class="text-center"> ชาย </td>
                                    <td class="text-center"> นายศรี ส่งเสริม </td> 
                                    <td class="text-center"> 15 ก.พ. 2562 </td> 
                                  </tr> 
                                  <tr >    
                                    <td class="text-center"> ชาย </td>
                                    <td class="text-center"> นายศรี ส่งเสริม </td> 
                                    <td class="text-center"> 15 ก.พ. 2562 </td> 
                                  </tr> 
                                  <tr >    
                                    <td class="text-center"> ชาย </td>
                                    <td class="text-center"> นายศรี ส่งเสริม </td> 
                                    <td class="text-center"> 15 ก.พ. 2562 </td> 
                                  </tr>
                                  <tr >    
                                    <td class="text-center"> ชาย </td>
                                    <td class="text-center"> นายศรี ส่งเสริม </td> 
                                    <td class="text-center"> 15 ก.พ. 2562 </td> 
                                  </tr> 
                                  <tr >    
                                    <td class="text-center"> ชาย </td>
                                    <td class="text-center"> นายศรี ส่งเสริม </td> 
                                    <td class="text-center"> 15 ก.พ. 2562 </td> 
                                  </tr>


                                  <tr> 
                                    <td  rowspan="3"  class="text-center"> 4 </td>
                                    <td  rowspan="3" >   
                                    นายทหารเทคโนโลยีสารสนเทศและการสื่อสาร ขว.ทอ.
                                    </td>
                                    <td class="text-center" rowspan="3">3 </td>
                                  
                                    <td class="text-center"> ชาย </td>
                                    <td class="text-center"> นายศรี ส่งเสริม </td> 
                                    <td class="text-center"> 15 ก.พ. 2562 </td> 
                                  </tr> 
                                  <tr >    
                                    <td class="text-center"> ชาย </td>
                                    <td class="text-center"> นายศรี ส่งเสริม </td> 
                                    <td class="text-center"> 15 ก.พ. 2562 </td> 
                                  </tr> 
                                  <tr >    
                                    <td class="text-center"> ชาย </td>
                                    <td class="text-center"> นายศรี ส่งเสริม </td> 
                                    <td class="text-center"> 15 ก.พ. 2562 </td> 
                                  </tr> 


                                  <tr> 
                                    <td  rowspan="3"  class="text-center"> 5 </td>
                                    <td  rowspan="3" >   
                                      นายทหารเทคโนโลยีสารสนเทศและการสื่อสาร ฝ่ายเทคโนโลยีสารสนเทศและการสื่อสาร บก.บน.๔
                                    </td>
                                    <td class="text-center" rowspan="3"> 3 </td>
                                  
                                    <td class="text-center"> ชาย </td>
                                    <td class="text-center"> นายศรี ส่งเสริม </td> 
                                    <td class="text-center"> 15 ก.พ. 2562 </td> 
                                  </tr> 
                                  <tr >    
                                    <td class="text-center"> ชาย </td>
                                    <td class="text-center"> นายศรี ส่งเสริม </td> 
                                    <td class="text-center"> 15 ก.พ. 2562 </td> 
                                  </tr> 
                                  <tr >    
                                    <td class="text-center"> ชาย </td>
                                    <td class="text-center"> นายศรี ส่งเสริม </td> 
                                    <td class="text-center"> 15 ก.พ. 2562 </td> 
                                  </tr>
      

      

                                </tbody>
                              </table>
                          </div>
                        </div>
         
                        <div class="tab-pane" id="tab12" aria-labelledby="base-tab12">
                          <div class="box_tb1">
                          <table id="planManpowerTable2" class="table  tb1   "  >
                          <thead>
                            <tr>
                              <th> ลำดับที่ </th> 
                              <th> รายชื่อผู้สมัคร   </th>
                              <th > เพศ </th>
                              <th> ตำแหน่ง </th> 
                              <th > วันที่สมัคร </th>  
                            </tr>
                          </thead>
                          <tbody> 
                            <tr> 
                              <td class="text-center"> 1 </td> 
                              <td class="text-center"> นายศรี ส่งเสริม </td>
                              <td class="text-center"> ชาย </td>
                              <td class="text-left"> นายทหารรายงานและหลักฐาน แปนกรายงานและหลักฐาน กกง.กง.ทอ. </td>
                              <td class="text-center"> 15 ก.พ. 2562 </td> 
                            </tr> 
                            <tr> 
                              <td class="text-center"> 2 </td> 
                              <td class="text-center"> นายศรี ส่งเสริม </td>
                              <td class="text-center"> ชาย </td>
                              <td class="text-left"> นายทหารบัญชี แผนกการเงิน ชย.ทอ. </td>
                              <td class="text-center"> 15 ก.พ. 2562 </td> 
                            </tr> 
                            <tr> 
                              <td class="text-center"> 3 </td> 
                              <td class="text-center"> นายศรี ส่งเสริม </td>
                              <td class="text-center"> ชาย </td>
                              <td class="text-left"> ผู้ช่วยนายทหารการเงิน กองพันทหารอากาศโยธิน บน.1 </td>
                              <td class="text-center"> 15 ก.พ. 2562 </td> 
                            </tr>
                            <tr> 
                              <td class="text-center"> 4 </td> 
                              <td class="text-center"> นายศรี ส่งเสริม </td>
                              <td class="text-center"> ชาย </td>
                              <td class="text-left"> นายทหารบัญชี แผนกการเงิน บน.21</td>
                              <td class="text-center"> 15 ก.พ. 2562 </td> 
                            </tr>
                            <tr> 
                              <td class="text-center">5 </td> 
                              <td class="text-center"> นายศรี ส่งเสริม </td>
                              <td class="text-center"> ชาย </td>
                              <td class="text-left"> ผู้ช่วยนายทหารการเงิน กองพันทหารอากาศโยธิน บน.21 . </td>
                              <td class="text-center"> 15 ก.พ. 2562 </td> 
                            </tr> 
                          </tbody>
                        </table>
                          </div>
                        </div>
                      </div>


                    





                      

                    </div> 
                  
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--/ Bootstrap 3 table -->
      </div>
    </div>
  </div>
  </section>

 
 
  <!-- footer -->
  <?php include '../include/footer.php'; ?> 

  <script src="../../Controllers/planManpowerController.js"></script>
  <script type="text/javascript">
    $(document).ready(function() { 
      /*
      $("#planManTypeId").change(function(){
        // alert( $(this).val() );
         $('.tb1').addClass('hidden');

         $('#planManTypeId2').addClass('hidden'); 
         if( $(this).val() == 1 || $(this).val() == 0){
            $('#planManpowerTable1').removeClass('hidden');
         }else if( $(this).val() ==2 ){ 
            $('#planManTypeId2').removeClass('hidden'); 
            $('#planManpowerTable2').removeClass('hidden'); 
         } 

      });

      $("#planManTypeId2").change(function(){
        $('.tb1').addClass('hidden');
         if( $(this).val() ==22 ){
            $('#planManpowerTable3').removeClass('hidden'); 
          }else{
            $('#planManpowerTable2').removeClass('hidden'); 
          }
      }); */


    }); 
  </script>
  