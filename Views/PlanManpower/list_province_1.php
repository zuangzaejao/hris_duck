  <!-- header -->
  <?php include '../include/header.php'; ?>
  <?php include '../../Model/Ducklab/func.php'; ?>
  <?php  
    //Recruitment and appointment
    $menu1 = "RECRNAPPOINT" ;
    $menu2 = "PlanManpower" ;

  ?>
  <!-- menu -->
  <?php include '../include/menu.php'; ?>
  
  <section>
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-2">
            <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
            <h3 class="content-header-title">แผนการบรรจุกำลังพล </h3>
          </div>
        </div>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb"> 
            <li class="breadcrumb-item"><a href="../home/index.php">หน้าแรก </a></li> 
            <li class="breadcrumb-item"><a href="index.php">ระบบงานสรรหาและบรรจุกำลังพล</a></li> 
            <li class="breadcrumb-item active" aria-current="page">แผนบรรจุกำลังพล ประจำปี 2562 </li>
          </ol>
        </nav>
        <div class="content-body">
          <!-- Bootstrap 3 table -->
          <section id="bootstrap3">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-content collapse show">
                    <div class="card-body card-dashboard">
                      <p class="card-text"></p> 
                      <div class="row"> 
                          <div class="col-md-3 col-sm-12">
                            <div class="form-group">  
                              <select  id="planManTypeId" class="  form-control block " >
                                <option value="1"> กำลังพลพิเศษ </option>
                                <option value="2"> น.ประทวนเลื่อนฐานะ  </option>
                                <option value="3">  จัดกลุ่มคุณวุฒิ/จังหวัด </option>
                              </select>
                            </div> 
                          </div> 
                          <div class="col-md-3 col-sm-12 ">
                            <div class="form-group">  
                              <select  id="planManTypeId2" class="  form-control block hidden" >
                                <option value="21"> น.ประทวนทำหน้าที่ </option>
                                <option value="22">  น.ประทวน ป.ตรีปรับ </option>  
                              </select>
                            </div>
                            
                          </div>
                      </div>
                      <div class="row"> 
                        <div class="col-sm-12">
                          <div class="box_h1 card">
                            <div class="card-header card-head-inverse  ">
                              <h4 class="card-title text-white"> ค้นหา </h4> 
                              <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                              <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                  <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                                </ul>
                              </div>
                            </div>
                            <div class="card-content collapse show">
                              <div class="card-body">
                                <div class="row">
                                  <div class=" col-md-8  m-auto ">
                                  
                                    <form class="form">
                                      <div class="form-body"> 

                                        <div class="boxsearchplan1 hidden ">
                                          <div class="row">
                                            <div class="col-md-3">
                                              <div class="form-group">
                                                <select id="" name="" class="form-control ">
                                                <option value="">เหล่า/จำพวก</option>
                                                  <option value=""> นักบิน </option>
                                                  <option value=""> ต้นหน </option>
                                                  <option value=""> อุตุ </option>
                                                </select>
                                              </div>
                                            </div>
                                            <div class="col-md-3">
                                              <div class="form-group">  
                                                  <select id="projectinput5" name="interested" class="form-control ">
                                                  <option value="">สังกัด</option>
                                                  <option value="">สลก.ทอ.</option>
                                                  <option value="">สบ.ทอ.</option> 
                                                  <option value="">กพ.ทอ.</option> 
                                                  <option value="">ขว.ทอ.</option> 
                                                  <option value="">ยก.ทอ.</option> 
                                                  <option value="">กบ.ทอ.</option> 
                                                  <option value="">กร.ทอ.</option>
                                                  </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                              <div class="form-group">  
                                                  <select id="projectinput5" name="interested" class="form-control ">
                                                  <option value="development">ตำแหน่ง</option>
                                                    <option value=""> จเรฝ่ายยุทธการและนิรภัยการบิน </option>
                                                    <option value=""> ผู้ช่วยจเรฝ่ายยุทธการและนิรภัยการบิน </option>
                                                    <option value="">จเรฝ่ายข่าว  </option>
                                                  </select>
                                                </div>
                                            </div>
                                          </div> 
                                          <div class="row">
                                            <div class="col-md-3">
                                              <div class="form-group">   
                                                <input type="text" id="" class="form-control " placeholder="เลขลชทอ." name="">
                                              </div>
                                            </div> 
                                            <div class="col-md-3">
                                              <div class="form-group"> 
                                                <select id="" name="" class="form-control ">
                                                  <option value="0" selected="" >อัตรา</option>
                                                  <option value="">ร.อ.</option>
                                                  <option value="">ร.ท.</option>
                                                  <option value="">ร.ต.</option>
                                                </select>
                                              </div>
                                            </div>
                                          </div>
                                        </div> <!-- ./boxsearchplan1 -->

                                        <div class="boxsearchplan2">
                                          <div class="row">
                                            <div class="col-md-6">
                                              <div class="form-group">
                                                <select id="" name="" class="form-control ">
                                                  <option value="">คุณวุฒิ</option>
                                                  <option value="">มัธยมศึกษาตอนต้น</option>
                                                  <option value="">มัธยมศึกษาตอนปลาย</option>
                                                  <option value="">ปวช.</option>
                                                  <option value="">ปวส.</option>
                                                  <option value="">ปริญญาตรี</option>
                                                  <option value="">ปริญญาโท</option>
                                                  <option value="">ปริญญาเอก</option>
                                                </select>
                                              </div>
                                            </div> 
                                            <div class="col-md-6">
                                              <div class="form-group">  
                                                  <select id="projectinput5" name="interested" class="form-control ">
                                                    <option value="development">ตำแหน่ง</option>
                                                    <option value=""> จเรฝ่ายยุทธการและนิรภัยการบิน </option>
                                                    <option value=""> ผู้ช่วยจเรฝ่ายยุทธการและนิรภัยการบิน </option>
                                                    <option value="">จเรฝ่ายข่าว  </option>
                                                  </select>
                                                </div>
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-3">
                                              <div class="form-group">
                                                <select id="" name="" class="form-control ">
                                                  <option value="">เหล่า/จำพวก</option>
                                                  <option value=""> นักบิน </option>
                                                  <option value=""> ต้นหน </option>
                                                  <option value=""> อุตุ </option>
                                                </select>
                                              </div>
                                            </div> 
                                            <div class="col-md-3">
                                              <div class="form-group">  
                                                <select id="" name="" class="form-control ">
                                                  <option value="">เพศ</option>
                                                  <option value="">ชาย</option>
                                                  <option value="">หญิง</option>
                                                </select>
                                              </div>
                                            </div>
                                            <div class="col-md-3">
                                              <div class="form-group">  
                                                <select id="" name="" class="form-control ">
                                                  <option value="">อัตรา</option>
                                                  <option value=""> นอ. </option>
                                                  <option value=""> นท. </option>
                                                  <option value=""> นต. </option>
                                                </select>
                                              </div>
                                            </div>
                                            <div class="col-md-3">
                                              <div class="form-group">  
                                                <select id="" name="" class="form-control ">
                                                  <option value="">สังกัด</option>
                                                  <option value="">สลก.ทอ.</option>
                                                  <option value="">สบ.ทอ.</option> 
                                                  <option value="">กพ.ทอ.</option> 
                                                  <option value="">ขว.ทอ.</option> 
                                                  <option value="">ยก.ทอ.</option> 
                                                  <option value="">กบ.ทอ.</option> 
                                                  <option value="">กร.ทอ.</option>
                                                </select>
                                              </div>
                                            </div>
                                            <div class="col-md-3">
                                              <div class="form-group">  
                                                <input type="text" id="" class="form-control " placeholder="เลขลชทอ.หน้าที่" name="">  
                                              </div>
                                            </div>
                                          </div>
                                        </div>  <!-- ./boxsearchplan2 -->
                                      
                                      </div> 

                                      <div class="text-center">
                                        <button type="button" class="btn round btn1 btncustom1 mr-1">
                                          <i class="fa fa-search"></i> ค้นหา
                                        </button>
                                        <button type="button" class="btn round btn1 btncustom1">
                                          <i class=" fa fa-repeat"></i> ล้างค่า
                                        </button>
                                      </div>

                                    </form>

                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div> 

                      <div class="row"> 
                        <div class="col-md-3 col-sm-12">
                          <div class="box1 txtdesc">
                              ข้อมูลจำนวน <span> 500 </span> อัตรา
                          </div>
                        </div> 
                      </div>
                      <div class="row">
                        <div class="col-6 ">
                          <button class="btn round btn1 btncustom1">  <i class="la la-print"></i> <span > ปริ้น </span> </button>
                          <button class="btn round btn1 btncustom1">  <i class="la la-file-excel-o"></i> <span > Export Excel</span> </button>
                        </div>
                        <div class="col-6 text-right">
                          <a href="" class="fontcolor1 hidden" data-toggle="modal" data-target="#modalPlanPublic"  > <i class="la la-pencil-square-o"></i>  </a>

                          <button class="btn round btn1 btncustom1"  data-toggle="modal" data-target="#modalSettingRegis">  <i class="la la-calendar"></i> <span >  เปิด/ปิด รับสมัคร </span> </button>
                        </div>
                      </div>
                      
                      <div class="box_tb1">

                        <table id="planManpowerTable1" class="table  tb1 "  >
                          <thead>
                            <tr>
                              <th> ลำดับที่ </th>
                              <th style="width:32%;"> คุณวุฒิ</th>
                              <th  style="width:32%;"> ตำแหน่ง</th>
                              <th> เหล่าทหาร/จำพวก </th>
                              <th >เพศ</th> 
                              <th >อัตรา</th> 
                              <th >สังกัด</th> 
                              <th >จำนวน</th> 
                            </tr>
                          </thead>
                          <tbody> 
                            <tr> 
                              <td class="text-center"> 1 </td>
                              <td>   
                                ปริญญาพยาบาลศาสตร์มหาบัณฑิต ทางการพยาบาลผู้ใหญ่, ทางการพยาบาลผู้งอายุ และได้รับใบอนุญาตประกอบวิชาชีพการพยาบาลและการผดุงครรภ์ และไม่มีข้อผูกพันในการใช้ทุน
                              </td>
                              <td  class="border1"> อาจารย์ ภาควิชาการพยาบาลผู้ใหญ่ กองการศึกษา วพอ.พอ. </td>
                              <td class="text-center"> พ. </td>
                              <td class="text-center"> ญ </td>
                              <td class="text-center"> ร.อ. </td>
                              <td class="text-center"> พอ. </td>
                              <td class="text-center"> 1 </td>
                            </tr> 
                            <tr class="even"> 
                              <td   rowspan="2"  class="text-center"> 2  </td>
                              <td   rowspan="2" >   
                                ปริญญาโท ในสาขาวิชาใดวิชาหนึ่ง ทางการจัดการทรัพยากรมนุษย์ ทางการพัฒนาทรัพยากรมนุษย์และองค์การ
                              </td>
                              <td  class="border1"> 
                                นายทหารพัฒนากาลัง แผนกพัฒนากาลังพล กคพ.สปพ.กพ.ทอ.  
                              
                              </td>
                              <td class="text-center"> กพ. </td>
                              <td class="text-center"> ช/ญ  </td>
                              <td class="text-center"> ร.อ. </td>
                              <td class="text-center"> กพ.ทอ. </td>
                              <td class="text-center"> 1 </td>
                            </tr>
                            <tr class="even"> 
                              <td class="border1">   นายทหารนโยบายและแผน ผนผ.กนผ.สนพ.กพ.ทอ.  </td>
                              <td class="text-center"> กพ. </td>
                              <td class="text-center"> ช/ญ  </td>
                              <td class="text-center"> ร.อ. </td>
                              <td class="text-center"> กพ.ทอ. </td>
                              <td class="text-center"> 1 </td>
                            </tr> 


                            <tr> 
                              <td class="text-center">3  </td> 
                              <td>   
                                ปริญญาโท ในสาขาวิชาคอมพิวเตอร์ หรือในสาขาวิชาใดวิชาหนึ่งทางคอมพิวเตอร์, ทางวิทยาการคอมพิวเอตร์, ทางเทคโนโลยีสารสนเทศ (ทดสอบปฏิบัติการสอน)
                              </td>
                              <td  class="border1"> 
                                อาจารย์ กกศ.รร.นนก.
                              </td>
                              <td class="text-center" > กพ. </td>
                              <td class="text-center" > ช/ญ  </td>
                              <td class="text-center"> ร.อ. </td>
                              <td class="text-center"> รร.นนก. </td>
                              <td class="text-center"> 1 </td>
                            </tr>

                            <tr  class="even"> 
                              <td class="text-center"> 4 </td>
                              <td>   
                                ปริญญาพยาบาลศาสตร์มหาบัณฑิต ทางการพยาบาลผู้ใหญ่, ทางการพยาบาลผู้งอายุ และได้รับใบอนุญาตประกอบวิชาชีพการพยาบาลและการผดุงครรภ์ และไม่มีข้อผูกพันในการใช้ทุน
                              </td>
                              <td  class="border1"> อาจารย์ ภาควิชาการพยาบาลผู้ใหญ่ กองการศึกษา วพอ.พอ. </td>
                              <td class="text-center"> พ. </td>
                              <td class="text-center"> ญ </td>
                              <td class="text-center"> ร.อ. </td>
                              <td class="text-center"> พอ. </td>
                              <td class="text-center"> 1 </td>
                            </tr>  
                            <tr> 
                              <td class="text-center"> 5   </td> 
                              <td>   
                                ปริญญาโท ในสาขาวิชาคอมพิวเตอร์ หรือในสาขาวิชาใดวิชาหนึ่งทางคอมพิวเตอร์, ทางวิทยาการคอมพิวเอตร์, ทางเทคโนโลยีสารสนเทศ (ทดสอบปฏิบัติการสอน)
                              </td>
                              <td  class="border1"> 
                                อาจารย์ กกศ.รร.นนก.
                              </td>
                              <td class="text-center" > กพ. </td>
                              <td class="text-center" > ช/ญ  </td>
                              <td class="text-center"> ร.อ. </td>
                              <td class="text-center"> รร.นนก. </td>
                              <td class="text-center"> 1 </td>
                            </tr>
                          </tbody>
                        </table>
                        
                        <table id="planManpowerTable2" class="table  tb1 hidden "  >
                          <thead>
                            <tr>
                              <th> ลำดับที่ </th> 
                              <th> เหล่าทหาร/จำพวกทหาร </th>
                              <th > สังกัด</th>
                              <th> ตำแหน่ง </th> 
                              <th > ลชทอ.หน้าที่ </th> 
                              <th >อัตรา</th>  
                              <th >จำนวน</th> 
                            </tr>
                          </thead>
                          <tbody> 
                            <tr> 
                              <td class="text-center"> 1 </td>
                              <td class="text-center"> กง. </td>
                              <td class="text-center"> กง.ทอ. </td>
                              <td class="text-left"> นายทหารรายงานและหลักฐาน แปนกรายงานและหลักฐาน กกง.กง.ทอ. </td>
                              <td class="text-center"> 6713 </td>
                              <td class="text-center"> ร.อ. </td> 
                              <td class="text-center"> 1 </td>
                            </tr> 
                            <tr> 
                              <td class="text-center"> 2 </td>
                              <td class="text-center"> กง. </td>
                              <td class="text-center"> กง.ทอ. </td>
                              <td class="text-left"> นายทหารบัญชี แผนกการเงิน ชย.ทอ. </td>
                              <td class="text-center"> 6713 </td>
                              <td class="text-center"> ร.อ. </td> 
                              <td class="text-center"> 1 </td>
                            </tr>
                            <tr> 
                              <td class="text-center"> 3 </td>
                              <td class="text-center"> กง. </td>
                              <td class="text-center"> บน.1. </td>
                              <td class="text-left"> ผู้ช่วยนายทหารการเงิน กองพันทหารอากาศโยธิน บน.1  </td>
                              <td class="text-center"> 6713 </td>
                              <td class="text-center"> ร.ท. </td> 
                              <td class="text-center"> 1 </td>
                            </tr>
                            <tr> 
                              <td class="text-center"> 4 </td>
                              <td class="text-center"> กง. </td>
                              <td class="text-center"> บน.21 </td>
                              <td class="text-left"> นายทหารบัญชี แผนกการเงิน บน.21   </td>
                              <td class="text-center"> 6713 </td>
                              <td class="text-center"> ร.อ. </td> 
                              <td class="text-center"> 1 </td>
                            </tr>
                            <tr> 
                              <td class="text-center"> 5 </td>
                              <td class="text-center"> กง. </td>
                              <td class="text-center"> บน.21 </td>
                              <td class="text-left"> ผู้ช่วยนายทหารการเงิน กองพันทหารอากาศโยธิน บน.21   </td>
                              <td class="text-center"> 6713 </td>
                              <td class="text-center"> ร.ท. </td> 
                              <td class="text-center"> 1 </td>
                            </tr>
                            
                          </tbody>
                        </table>

                        <table id="planManpowerTable3" class="table table-bordered tb1 hidden  "  >
                          <thead>
                            <tr>
                              <th> ลำดับที่ </th>
                              <th style="width:32%;"> คุณวุฒิ</th>
                              <th> เหล่าทหาร/จาพวก </th>
                              <th> สังกัด </th>
                              <th  style="width:32%;"> ตำแหน่ง</th>
                              <th >ลชทอ.หน้าที่</th> 
                              <th >เพศ</th> 
                              <th >อัตรา</th>  
                              <th >จำนวน</th> 
                            </tr>
                          </thead>
                          <tbody> 
                            <tr> 
                              <td  rowspan="2"  class="text-center"> 1 </td>
                              <td  rowspan="2" >   
                                ปริญญาตรี ในสาขาวิชาพยาบาลศาสตร์  ได้รับใบประกอบวิชาชีพทางการพยาบาลและการผดุงครรภ์
                              </td>
                              <td class="text-center"> พ. </td> 
                              <td class="text-center"> พอ. </td>
                              <td class="text-center"> รองผู้บังคับหมวดนักเรียนพยาบาลทหารอากาศ ฝ่ายปกครอง แผนกปกครอง วพอ.พอ. </td>
                              <td class="text-center"> 9713 </td> 
                              <td class="text-center"> ช </td>
                              <td class="text-center"> ร.ท. </td>   
                              <td class="text-center"> 1 </td>
                            </tr> 
                            <tr >   
                            <td class="text-center"> พ. </td> 
                              <td class="text-center"> พอ. </td>
                              <td class="text-center"> นายทหารสันทัดงาน กวห.รพ.ภูมิพลอดุลยเดชพอ. </td>
                              <td class="text-center"> 9713,9023 </td> 
                              <td class="text-center"> ช/ญ </td>
                              <td class="text-center"> ร.ท. </td>   
                              <td class="text-center"> 1 </td>
                            </tr>
                            
                            <tr> 
                              <td  rowspan="5"  class="text-center"> 2</td>
                              <td  rowspan="5" >   
                              ปริญญาตรี ในสาขาวิชาการบัญชี หรือในสาขาวิชาใดสาขาวิชาหนึ่ง
                              ทางการบัญชี, ทางการธนาคารและการเงิน,
                              ทางการบัญชีการเงิน, ทางการบัญชีบริหาร,
                              ทางระบบสารสนเทศทางการบัญชี
                              </td>
                              <td class="text-center"> กง. </td> 
                              <td class="text-center"> อย. </td>
                              <td class="text-center"> นายทหารควบคุมการเบิกจ่าย แผนกการเงิน อย. </td>
                              <td class="text-center"> 6713 </td> 
                              <td class="text-center"> ช/ญ </td>
                              <td class="text-center"> ร.ท. </td>   
                              <td class="text-center"> 1 </td>
                            </tr> 
                            <tr >   
                              <td class="text-center"> กง. </td> 
                              <td class="text-center"> สอ.ทอ. </td>
                              <td class="text-center"> นายทหารควบคุมการเบิกจ่าย แผนกการเงิน สอ.ทอ. </td>
                              <td class="text-center"> 6713 </td> 
                              <td class="text-center"> ช/ญ </td>
                              <td class="text-center"> ร.ท. </td>   
                              <td class="text-center"> 1 </td>
                            </tr>
                            <tr >   
                              <td class="text-center"> ตน. </td> 
                              <td class="text-center"> สตน.ทอ. </td>
                              <td class="text-center"> นายทหารตรวจสอบ ชุดตรวจที่ ๒ กตจ.๑ กตส.๑ สตน.ทอ. </td>
                              <td class="text-center"> 6713 </td> 
                              <td class="text-center"> ช/ญ </td>
                              <td class="text-center"> ร.ท. </td>   
                              <td class="text-center"> 1 </td>
                            </tr>
                            <tr >   
                              <td class="text-center"> ตน. </td> 
                              <td class="text-center"> สตน.ทอ. </td>
                              <td class="text-center"> นายทหารตรวจสอบ ชุดตรวจที่ ๒ กตจ.๒ กตส.๑ สตน.ทอ. </td>
                              <td class="text-center"> 6713 </td> 
                              <td class="text-center"> ช/ญ </td>
                              <td class="text-center"> ร.ท. </td>   
                              <td class="text-center"> 1 </td>
                            </tr>
                            <tr >   
                              <td class="text-center"> ตน. </td> 
                              <td class="text-center"> สตน.ทอ. </td>
                              <td class="text-center"> นายทหารตรวจสอบ ชุดตรวจที่ ๒ กตจ.๒ กตส.๓ สตน.ทอ. </td>
                              <td class="text-center"> 6713 </td> 
                              <td class="text-center"> ช/ญ </td>
                              <td class="text-center"> ร.ท. </td>   
                              <td class="text-center"> 1 </td>
                            </tr> 

                            <tr> 
                              <td  rowspan="2"  class="text-center"> 3 </td>
                              <td  rowspan="2" >   
                              ปริญญาตรี ในสาขาวิชาคอมพิวเตอร์ หรือในสาขาวิชาใดสาขาวิชาหนึ่ง ทางคอมพิวเตอร์, ทางวิทยาการคอมพิวเตอร์, ทางเทคโนโลยีสารสนเทศ
                              </td>
                              <td class="text-center"> ทสส. </td> 
                              <td class="text-center"> ชว.ทอ. </td>
                              <td class="text-center">  นายทหารเทคโนโลยีสารสนเทศและการสื่อสาร ขว.ทอ. </td>
                              <td class="text-center"> 2713 </td> 
                              <td class="text-center"> ช </td>
                              <td class="text-center"> ร.อ. </td>   
                              <td class="text-center"> 1 </td>
                            </tr> 
                            <tr >   
                              <td class="text-center"> ทสส. </td> 
                              <td class="text-center">  บน.4 </td>
                              <td class="text-center">  นายทหารเทคโนโลยีสารสนเทศและการสื่อสาร ฝ่ายเทคโนโลยีสารสนเทศและการสื่อสาร บก.บน.๔ </td>
                              <td class="text-center"> 2713 </td> 
                              <td class="text-center"> ช </td>
                              <td class="text-center"> ร.อ. </td>   
                              <td class="text-center"> 1 </td> 
                            </tr>


                            <tr> 
                              <td  rowspan="5"  class="text-center"> 4</td>
                              <td  rowspan="5" >   
                              ปริญญาตรี ในสาขาวิชาการบัญชี หรือในสาขาวิชาใดสาขาวิชาหนึ่ง
                              ทางการบัญชี, ทางการธนาคารและการเงิน,
                              ทางการบัญชีการเงิน, ทางการบัญชีบริหาร,
                              ทางระบบสารสนเทศทางการบัญชี
                              </td>
                              <td class="text-center"> กง. </td> 
                              <td class="text-center"> อย. </td>
                              <td class="text-center"> นายทหารควบคุมการเบิกจ่าย แผนกการเงิน อย. </td>
                              <td class="text-center"> 6713 </td> 
                              <td class="text-center"> ช/ญ </td>
                              <td class="text-center"> ร.ท. </td>   
                              <td class="text-center"> 1 </td>
                            </tr> 
                            <tr >   
                              <td class="text-center"> กง. </td> 
                              <td class="text-center"> สอ.ทอ. </td>
                              <td class="text-center"> นายทหารควบคุมการเบิกจ่าย แผนกการเงิน สอ.ทอ. </td>
                              <td class="text-center"> 6713 </td> 
                              <td class="text-center"> ช/ญ </td>
                              <td class="text-center"> ร.ท. </td>   
                              <td class="text-center"> 1 </td>
                            </tr>
                            <tr >   
                              <td class="text-center"> ตน. </td> 
                              <td class="text-center"> สตน.ทอ. </td>
                              <td class="text-center"> นายทหารตรวจสอบ ชุดตรวจที่ ๒ กตจ.๑ กตส.๑ สตน.ทอ. </td>
                              <td class="text-center"> 6713 </td> 
                              <td class="text-center"> ช/ญ </td>
                              <td class="text-center"> ร.ท. </td>   
                              <td class="text-center"> 1 </td>
                            </tr>
                            <tr >   
                              <td class="text-center"> ตน. </td> 
                              <td class="text-center"> สตน.ทอ. </td>
                              <td class="text-center"> นายทหารตรวจสอบ ชุดตรวจที่ ๒ กตจ.๒ กตส.๑ สตน.ทอ. </td>
                              <td class="text-center"> 6713 </td> 
                              <td class="text-center"> ช/ญ </td>
                              <td class="text-center"> ร.ท. </td>   
                              <td class="text-center"> 1 </td>
                            </tr>
                            <tr >   
                              <td class="text-center"> ตน. </td> 
                              <td class="text-center"> สตน.ทอ. </td>
                              <td class="text-center"> นายทหารตรวจสอบ ชุดตรวจที่ ๒ กตจ.๒ กตส.๓ สตน.ทอ. </td>
                              <td class="text-center"> 6713 </td> 
                              <td class="text-center"> ช/ญ </td>
                              <td class="text-center"> ร.ท. </td>   
                              <td class="text-center"> 1 </td>
                            </tr> 

                            <tr> 
                              <td   class="text-center"> 5 </td>
                              <td   >   
                              ปริญญาตรี ในสาขาวิชานิติศาสตร์และเป็นสมาชิกเนติบัณฑิตยสภา
                              </td>
                              <td class="text-center"> ธน. </td> 
                              <td class="text-center"> สธน.ทอ. </td>
                              <td class="text-center">  นายทหารบริหารกาลังพล กฎก.สธน.ทอ.  </td>
                              <td class="text-center"> 8813 </td> 
                              <td class="text-center"> ช/ญ </td>
                              <td class="text-center"> ร.อ. </td>   
                              <td class="text-center"> 1 </td>
                            </tr> 

                          </tbody>
                        </table>

                        <table id="planManpowerTable4" class="table table-bordered tb1 hidden "  >
                          <thead>
                            <tr>
                              <th> </th>
                              <th style="width: 90px;"> กลุ่ม </th>
                              <th style="width: 90px;"> รหัสตำแหน่ง </th>
                              <th style="width:30%;"> คุณวุฒิ</th>
                              <th style="width:20%;"> คุณสมบัติเพิ่มเติม </th>
                              <th>เพศ</th> 
                              <th>ที่ตั้งหน่วย</th>
                              <th style="width:20%;"> ตาแหน่ง</th> 
                              <th>จำนวน</th> 
                            </tr>
                          </thead>
                          <tbody>  
                            <tr> 
                              <td class="text-center"> 
                                <a href="" class="fontcolor1" data-toggle="modal" data-target="#modalPlanPublic"  > <i class="la la-pencil-square-o"></i>  </a>
                              </td>
                              <td class="text-center">
                                DOF-01
                              </td>
                              <td class="text-center"> 101310  </td>
                              <td >   
                              ปริญญาโท ในสาขาวิชาใดวิชาหนึ่ง ทางความสัมพันธ์ระหว่างประเทศ,
                                ทางการระหว่างประเทศและการทูต, ทางการเมืองและการระหว่างประเทศ,
                                ทางเอเชียศึกษา, ทางเอเชียตะวันออกเฉียงใต้ศึกษา, ทางอาเซียนศึกษา
                              </td>
                              <td class="text-left"> มีคุณวุฒิปริญญาตรี สาขาวิชารัฐศาสตร์  </td> 
                              <td class="text-center"> ชายหรือหญิง  </td>
                              <td class="text-center">  กรุงเทพฯ  </td>
                              <td class="text-left"> อาจารย์ </td>  
                              <td class="text-center"> 1 </td>
                            </tr> 
                            <tr> 
                              <td class="text-center"> 
                                <a href="" class="fontcolor1" data-toggle="modal" data-target="#modalPlanPublic"  > <i class="la la-pencil-square-o"></i>  </a>
                              </td>
                              <td class="text-center">
                                DOF-01
                              </td>
                              <td class="text-center"> 101310  </td>
                              <td >   
                              ปริญญาโท ในสาขาวิชาใดวิชาหนึ่ง ทางความสัมพันธ์ระหว่างประเทศ,
                                ทางการระหว่างประเทศและการทูต, ทางการเมืองและการระหว่างประเทศ,
                                ทางเอเชียศึกษา, ทางเอเชียตะวันออกเฉียงใต้ศึกษา, ทางอาเซียนศึกษา
                              </td>
                              <td class="text-left"> มีคุณวุฒิปริญญาตรี สาขาวิชารัฐศาสตร์  </td> 
                              <td class="text-center"> ชายหรือหญิง  </td>
                              <td class="text-center">  กรุงเทพฯ  </td>
                              <td class="text-left"> อาจารย์ </td>  
                              <td class="text-center"> 1 </td>
                            </tr> 
                            <tr> 
                              <td class="text-center"> 
                                <a href="" class="fontcolor1" data-toggle="modal" data-target="#modalPlanPublic"  > <i class="la la-pencil-square-o"></i>  </a>
                              </td>
                              <td class="text-center"> DOF-01</td>
                              <td class="text-center"> 101310  </td>
                              <td >   
                              ปริญญาโท ในสาขาวิชาใดวิชาหนึ่ง ทางความสัมพันธ์ระหว่างประเทศ,
                                ทางการระหว่างประเทศและการทูต, ทางการเมืองและการระหว่างประเทศ,
                                ทางเอเชียศึกษา, ทางเอเชียตะวันออกเฉียงใต้ศึกษา, ทางอาเซียนศึกษา
                              </td>
                              <td class="text-left"> มีคุณวุฒิปริญญาตรี สาขาวิชารัฐศาสตร์  </td> 
                              <td class="text-center"> ชายหรือหญิง  </td>
                              <td class="text-center">  กรุงเทพฯ  </td>
                              <td class="text-left"> อาจารย์ </td>  
                              <td class="text-center"> 1 </td>
                            </tr> 
                            <tr> 
                              <td class="text-center"> 
                                <a href="" class="fontcolor1" data-toggle="modal" data-target="#modalPlanPublic"  > <i class="la la-pencil-square-o"></i>  </a>
                              </td>
                              <td class="text-center"> DOF-01</td>
                              <td class="text-center"> 101310  </td>
                              <td >   
                              ปริญญาโท ในสาขาวิชาใดวิชาหนึ่ง ทางความสัมพันธ์ระหว่างประเทศ,
                                ทางการระหว่างประเทศและการทูต, ทางการเมืองและการระหว่างประเทศ,
                                ทางเอเชียศึกษา, ทางเอเชียตะวันออกเฉียงใต้ศึกษา, ทางอาเซียนศึกษา
                              </td>
                              <td class="text-left"> มีคุณวุฒิปริญญาตรี สาขาวิชารัฐศาสตร์  </td> 
                              <td class="text-center"> ชายหรือหญิง  </td>
                              <td class="text-center">  กรุงเทพฯ  </td>
                              <td class="text-left"> อาจารย์ </td>  
                              <td class="text-center"> 1 </td>
                            </tr> 

                          </tbody>
                        </table>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <!--/ Bootstrap 3 table -->
        </div>
      </div>
    </div>
  </section>

  
  <!-- Modal -->
  <div class="modal fade text-left modal_custom1" id="modalSettingRegis" tabindex="-1" role="dialog" aria-labelledby="modalSettingRegis"  aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header bg_custom1">
            <h5 class="modal-title"> บันทึก/แก้ไข วันเวลาเปิด/ปิด รับสมัคร</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form class="form_custom1">
            <div class="modal-body">
              <div class="row">
                <div class="col-8 m-auto">  

                  <div class="row">
                    <div class="col-md-3 col-10">
                      <div class="form-group">
                        <label for="startdate"> จำนวนวัน : 
                        </label>
                        <input type="number" class="form-control" id="numdate" value="10" >  
                      </div>
                    </div>
                    <div class="col-md-6 col-2">
                      <div class="form-group form_txt">
                        <label for="">  &nbsp; </label>
                        <br>  วัน
                      </div>
                    </div>
                    
                  </div> 
                
          
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="startdate"> ตั้งเเต่วันที่ :   </label> 
                        <div class="input-group datep">
                          <input type="text" class="form-control form2 pickadate-translations" disabled placeholder="" id="startdate" name="startdate" data-value="<?php echo GetToday('');?>" />
                          <div class="input-group-append">
                            <span class="input-group-text">
                              <span class="la la-calendar-o"></span>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="starttime">  เวลา : </label> 
                        <div class="input-group datep"> 
                          <input type="text" class="form-control form2 pickatime" disabled id="starttime" name="starttime"  value="00.00"/>
                          <div class="input-group-append">
                            <span class="input-group-text">
                              <span class="la la-clock-o"></span>
                            </span>
                          </div>
                        </div> 
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="enddate"> ถึงวันที่ :   </label> 
                        <div class="input-group datep">
                          <input type="text" class="form-control form2 pickadate-translations" disabled placeholder="" id="enddate" name="enddate" data-value="2019-05-27" />
                          <div class="input-group-append">
                            <span class="input-group-text">
                              <span class="la la-calendar-o"></span>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="endtime">  เวลา : </label> 
                        <div class="input-group datep"> 
                          <input type="text" class="form-control form2 pickatime" id="endtime" name="endtime" disabled value="16.00" />
                          <div class="input-group-append">
                            <span class="input-group-text">
                              <span class="la la-clock-o"></span>
                            </span>
                          </div>
                        </div> 
                      </div>
                    </div>
                  </div>
 
                  
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <div  class="m-auto ">
                <button type="button" class="btn btn2 btn-success   round"> <i class="fa fa-save"></i> บันทึก </button>
                <button type="button" class="btn btn2 btn-danger   round"  data-dismiss="modal"> <i class="fa fa-times-circle-o"></i> ยกเลิก </button> 
              </div>
            </div>
          </form>
        </div>
      </div>
  </div>
 

    <!-- Modal -->
   <div class="modal fade text-left modal_custom1" id="modalPlanPublic" tabindex="-1" role="dialog" aria-labelledby="modalPlanPublic"  aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header bg_custom1">
            <h5 class="modal-title">แก้ไขข้อมูล </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form class="form_custom1">
            <div class="modal-body">
              <div class="row">
                <div class="col-10 m-auto pt-2">  

                  <div class="row">
                    <div class="col-md-3 col-10">
                      <div class="form-group">
                        <label for="groupname"> กลุ่ม : </label>
                        <input type="text" class="form-control" id="groupname" name="groupname" value="DOF-01" >  
                      </div>
                    </div>
                    <div class="col-md-3 col-10">
                      <div class="form-group">
                        <label for="positioncode"> รหัสตำแหน่ง : </label>
                        <input type="text" class="form-control" id="positioncode" name="positioncode" value="101310" >  
                      </div>
                    </div>
                    <div class="col-md-3 col-10">
                      <div class="form-group">
                        <label for="provinceaddr"> ที่ตั้งหน่วย : </label>
                          <select  id="provinceaddr"  name="provinceaddr" class="form-control block " disabled >
                            <option value="1"> กรุงเทพฯ </option>
                            <option value="2"> กระบี่  </option>
                            <option value="3"> กาฬสินธุ์ </option>
                          </select> 
                      </div>
                    </div>
                    <div class="col-md-3 col-10">
                      <div class="form-group">
                        <label for="positionno"> จำนวน : </label>
                        <input type="number" class="form-control text-right" id="positionno" name="positionno" value="15" disabled >  
                      </div>
                    </div>
                  </div> 
          
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="edu_degree"> คุณวุฒิ :   </label>  
                        <textarea class="form-control" name="edu_degree" id="edu_degree" cols="12" rows="5" disabled>ปริญญาโท ในสาขาวิชาใดวิชาหนึ่ง ทางความสัมพันธ์ระหว่างประเทศ, ทางการระหว่างประเทศและการทูต, ทางการเมืองและการระหว่างประเทศ, ทางเอเชียศึกษา, ทางเอเชียตะวันออกเฉียงใต้ศึกษา, ทางอาเซียนศึกษา </textarea>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="spcfeatures"> คุณสมบัติเฉพาะเพิ่มเติม :   </label>
                          <textarea class="form-control" name="spcfeatures" id="spcfeatures" cols="12" rows="5">มีคุณวุฒิปริญญาตรี สาขาวิชารัฐศาสตร์</textarea>
                      </div>
                    </div>  
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="spcfeatures"> เพศ :   </label>
                        <div class="row skin icheck_minimal ">  
                          <div class="col-sm-12 "> 
                            <fieldset class="d-inline-block mr-1" >
                              <input type="radio" name="input-radio-3" id="input-radio-11" checked disabled>
                              <label for="input-radio-11">ชาย/หญิง</label>
                            </fieldset>
                            <fieldset class="d-inline-block mr-1" >
                              <input type="radio" name="input-radio-3" id="input-radio-12" disabled>
                              <label for="input-radio-12">ชาย</label>
                            </fieldset>
                            <fieldset class="d-inline-block  " >
                              <input type="radio" name="input-radio-3" id="input-radio-13" disabled>
                              <label for="input-radio-13">หญิง</label>
                            </fieldset>
                          </div>  
                        </div>
                      </div>
                    </div>   

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="positionname"> ตำแหน่ง : </label>
                          <select  id="positionname"  name="positionname" class="form-control block " disabled>
                            <option value="1"> นายทหารควบคุมการเบิกจ่าย </option>
                            <option value="2"> นายทหารตรวจสอบ  </option>
                            <option value="3"> นายทหารนโยบายและแผน </option>
                          </select> 
                      </div>
                    </div>

                  </div>

                </div>
              </div>
            </div>
            <div class="modal-footer">
              <div  class="m-auto ">
                <button type="button" class="btn btn2 btn-success   round"> <i class="fa fa-save"></i> บันทึก </button>
                <button type="button" class="btn btn2 btn-danger   round"  data-dismiss="modal"> <i class="fa fa-times-circle-o"></i> ยกเลิก </button> 
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>



 
 
    <!-- footer -->
  
  
    <?php include '../include/footer.php'; ?> 

  <script src="../../Controllers/planManpowerController.js"></script>
  <script type="text/javascript">
    $(document).ready(function() { 

      $("#planManTypeId").change(function(){
        // alert( $(this).val() );
         $('.tb1').addClass('hidden');

         $('#planManTypeId2').addClass('hidden'); 
         if( $(this).val() == 1 || $(this).val() == 0){
            $('#planManpowerTable1').removeClass('hidden');
            
            $('.boxsearchplan2').removeClass('hidden');
            $('.boxsearchplan1').addClass('hidden');


         }else if( $(this).val() ==2 ){ 
          
            $('.boxsearchplan2').addClass('hidden');
            $('.boxsearchplan1').removeClass('hidden');

            $('#planManTypeId2').removeClass('hidden'); 
            $('#planManpowerTable2').removeClass('hidden'); 
         }else if( $(this).val() == 3 ) {
            $('#planManpowerTable4').removeClass('hidden');

            $('.boxsearchplan2').addClass('hidden');
            $('.boxsearchplan1').removeClass('hidden');

         }

      });

      $("#planManTypeId2").change(function(){
        $('.tb1').addClass('hidden');
         if( $(this).val() ==22 ){
            $('#planManpowerTable3').removeClass('hidden'); 
          }else{
            $('#planManpowerTable2').removeClass('hidden'); 
          }
      });


    }); 
  </script>
  