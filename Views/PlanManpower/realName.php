  <!-- header -->
  <?php include '../include/header.php'; ?>
  <?php include '../../Model/Ducklab/func.php'; ?>
  <?php  
    //Recruitment and appointment
    $menu1 = "RECRNAPPOINT" ;
    $menu2 = "PlanManpower" ;

  ?>
  <!-- menu -->
  <?php include '../include/menu.php'; ?>
  
  <section>
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-2">
            <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
            <h3 class="content-header-title">แผนกำลังพล </h3>
          </div>
        </div>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb"> 
            <li class="breadcrumb-item"><a href="../home/index.php">หน้าแรก </a></li> 
            <li class="breadcrumb-item"><a href="index.php">ระบบงานสรรหาและบรรจุกำลังพล</a></li> 
            <li class="breadcrumb-item active" aria-current="page">ค้นหารายชื่อตัวจริงเเละตัวสำรอง</li>
            
          </ol>
        </nav>
        <div class="content-body">
          <!-- Bootstrap 3 table -->
          <section id="bootstrap3">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-content collapse show">
                    <div class="card-body card-dashboard">
                     
                      <div class="row"> 
                        <div class="col-sm-12">
                          <div class="box_h1 card">
                            <div class="card-header card-head-inverse  ">
                              <h4 class="card-title text-white"> ค้นหา </h4> 
                              <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                              <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                  <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                                </ul>
                              </div>
                            </div>
                            <div class="card-content collapse show">
                              <div class="card-body">
                                <div class="row">
                                  <div class=" col-md-10  m-auto ">
                                  
                                    <form class="form">
                                      <div class="form-body"> 

                                        <div class="boxsearchplan1">

                                          <div class="row">
                                            <div class="col-md-4">
                                              <div class="form-group">
                                              <label for="exampleInputEmail1">ปีงบประมาณ:</label>
                                                <select id="" name="" class="form-control ">
                                                  <option value=""> 2561</option>
                                                  <option value=""> 2562 </option>
                                                  <option value=""> 2563 </option>
                                                </select>
                                              </div>
                                            </div>

                                          


                                            <div class="col-md-4">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">ชื่อแผนบรรจุกำลังพลประจำปี:</label>
                                                  <select id="projectinput5" name="interested" class="form-control ">
                                                  <option value=""> แผนบรรจุกำลังพลประจำปี 2563</option>
                                                  <option value=""> แผนบรรจุกำลังพลประจำปี 2562</option>
                                                  <option value=""> แผนบรรจุกำลังพลประจำปี 2561</option>
                                               
                                                  </select>
                                                </div>
                                            </div>


                                            <div class="col-md-4">
                                            <label for="exampleInputEmail1">ประเภทกำลังพล:</label>
                                                  <select id="projectinput5" name="interested" class="form-control ">
                                                    <option value=""></option>
                                                    <option value=""></option>
                                                    <option value=""></option>
                                                  </select>
                                             </div>

                                      </div> 

                                      <div class="text-center">
                                        <button type="button" class="btn round btn1 btncustom1 mr-1">
                                          <i class="fa fa-search"></i> ค้นหา
                                        </button>

                                        

                                        <button type="button" class="btn round btn1 btncustom1">
                                          <i class=" fa fa-repeat"></i> ล้างค่า
                                        </button>
                                      </div>

                                    </form>

                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div> 

                      <div class="row"> 
                        <div class="col-md-3 col-sm-12">
                          <div class="box1 txtdesc">
                              ข้อมูลจำนวน <span> 500 </span> อัตรา
                          </div>
                        </div> 
                      </div>
                      <div class="row">
                        <div class="col-6 ">
                          <button class="btn round btn1 btncustom1">  <i class="la la-print"></i> <span > ปริ้น </span> </button>
                          <button class="btn round btn1 btncustom1">  <i class="la la-file-excel-o"></i> <span > Export Excel</span> </button>
                        </div>
                        
                      </div>
                      <br>
                      <span > สถานะผู้สอบผ่านแก้ไขแล้ว<i class="fa fa-circle text-success"></i> &nbsp;&nbsp; ผู้สอบผ่านยังไม่ได้แก้ไขข้อมูล <i class="fa fa-circle text-danger"></i> </span>
                      <div class="box_tb1">
                        <table id="planManpowerTable4" class="table table-bordered tb1"  >
                        <thead>
                                      <tr>
                                      <th>แก้ไข</th> 
                                         <th>  </th> 
                                        <th> ลำดับที่ </th> 
                                        <th> สถานะ</th> 
                                        <th> ชื่อ-สกุล </th> 
                                       
                                       
                                        <th> ตำแหน่ง</th> 	
                                        <th> คุณวุฒิ	</th> 
                                        <th> เพศ	</th> 
                                        <th> อัตรา	</th> 
                                        <th>เหล่า	</th> 
                                        <th>ลชทอ.	</th> 
                                        <th> จำพวก	</th> 
                                      
                                        
                                        



                                      </tr>
                                    </thead>
                          <tbody>  
                            <tr> 
                             <td class="text-center"> <i class="fa fa-circle text-success"></i> </td>
                              <td class="text-center"> 
                                <a href="" class="fontcolor1" > <i class="la la-trash"></i>  </a>
                                <a href="" class="fontcolor1" > <i class="la la-file-text-o"></i>  </a>
                              </td>
                              <td   class="text-center">1</td>
                              <td class="text-center"> 
                              <div class="badge badge_c2 badge-info  ">
                                            <span> ตัวจริง </span>  
                                          </div>
                                         
                               </td>
                               
                              <td class="text-center">  นายศรี ส่งเสริม</td>
                              
                              <td  rowspan="2" class="text-left"> รองผู้บังคับหมวดนักเรียนพยาบาลทหารอากาศ ฝ่ายปกครอง แผนกปกครอง</td>
                              <td  rowspan="2" class="text-center">  ชาย </td>
                              <td  rowspan="2" >     รอ.  </td>
                              <td  rowspan="2" class="text-left"> กง </td> 
                              <td  rowspan="2" class="text-center"> 7713 </td>
                              <td  rowspan="2" class="text-center"> กง </td>
                              <th  rowspan="2" > กง </th>
                               
                              
                            </tr> 
                            <tr> 
                              <td class="text-center"> <i class="fa fa-circle text-danger"></i>  </td>
                              <td class="text-center"> 
                                  <a href="" class="fontcolor1" > <i class="la la-trash"></i>  </a>
                                  <a href="" class="fontcolor1" > <i class="la la-file-text-o"></i>  </a>
                                </td>
                                <td   class="text-center">2</td>
                              <td class="text-center">  
                                <div class="badge badge_c2 badgebg1">
                                  <span> สำรอง </span>  
                                </div></td>
                              <td class="text-center">  นายธนา แน่วแน่</td>
                            </tr> 


                            <tr> 
                             <td class="text-center"> <i class="fa fa-circle text-success"></i> </td>
                              <td class="text-center"> 
                                <a href="" class="fontcolor1" > <i class="la la-trash"></i>  </a>
                                <a href="" class="fontcolor1" > <i class="la la-file-text-o"></i>  </a>
                              </td>
                              <td   class="text-center">3</td>
                              <td class="text-center"> 
                              <div class="badge badge_c2 badge-info  ">
                                            <span> ตัวจริง </span>  
                                          </div>
                                         
                               </td>
                               
                              <td class="text-center">  นายศิลา ใจดี</td>
                              
                              <td  rowspan="3" class="text-left">  นายทหารบัญชี</td>
                              <td  rowspan="3" class="text-center">  ชาย </td>
                              <td  rowspan="3" >     รอ.  </td>
                              <td  rowspan="3" class="text-left"> กง </td> 
                              <td  rowspan="3" class="text-center"> 7713 </td>
                              <td  rowspan="3" class="text-center"> กง </td>
                              <th  rowspan="3" > กง </th>
                               
                              
                            </tr> 
                            <tr> 
                              <td class="text-center"> <i class="fa fa-circle text-danger"></i>  </td>
                              <td class="text-center"> 
                                  <a href="" class="fontcolor1" > <i class="la la-trash"></i>  </a>
                                  <a href="" class="fontcolor1" > <i class="la la-file-text-o"></i>  </a>
                                </td>
                                <td   class="text-center">4</td>
                              <td class="text-center">  
                                <div class="badge badge_c2 badgebg1">
                                  <span> สำรอง </span>  
                                </div></td>
                              <td class="text-center">  นายภูผา ผ่องใส</td>
                            </tr> 
                            <tr> 
                              <td class="text-center"> <i class="fa fa-circle text-success"></i>  </td>
                              <td class="text-center"> 
                                  <a href="" class="fontcolor1" > <i class="la la-trash"></i>  </a>
                                  <a href="" class="fontcolor1" > <i class="la la-file-text-o"></i>  </a>
                                </td>
                                <td   class="text-center">5</td>
                              <td class="text-center">  
                                <div class="badge badge_c2 badgebg1">
                                  <span> สำรอง </span>  
                                </div></td>
                              <td class="text-center">  นางสาวสดใจ ใจสะอาด</td>
                            </tr> 
                            
                           

                           

                          

                          </tbody>
                        </table>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <!--/ Bootstrap 3 table -->
        </div>
      </div>
    </div>
  </section>

  
  
 

    <!-- Modal -->
   <div class="modal fade text-left modal_custom1" id="modalPlanPublic" tabindex="-1" role="dialog" aria-labelledby="modalPlanPublic"  aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header bg_custom1">
            <h5 class="modal-title">แก้ไขข้อมูล </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form class="form_custom1">
            <div class="modal-body">
              <div class="row">
                <div class="col-10 m-auto pt-2">  

                  <div class="row">
                    <div class="col-md-3 col-10">
                      <div class="form-group">
                        <label for="groupname"> กลุ่ม : </label>
                        <input type="text" class="form-control" id="groupname" name="groupname" value="DOF-01" >  
                      </div>
                    </div>
                    <div class="col-md-3 col-10">
                      <div class="form-group">
                        <label for="positioncode"> รหัสตำแหน่ง : </label>
                        <input type="text" class="form-control" id="positioncode" name="positioncode" value="101310" >  
                      </div>
                    </div>
                    <div class="col-md-3 col-10">
                      <div class="form-group">
                        <label for="provinceaddr"> ที่ตั้งหน่วย : </label>
                          <select  id="provinceaddr"  name="provinceaddr" class="form-control block " disabled >
                            <option value="1"> กรุงเทพฯ </option>
                            <option value="2"> กระบี่  </option>
                            <option value="3"> กาฬสินธุ์ </option>
                          </select> 
                      </div>
                    </div>
                    <div class="col-md-3 col-10">
                      <div class="form-group">
                        <label for="positionno"> จำนวน : </label>
                        <input type="number" class="form-control text-right" id="positionno" name="positionno" value="15" disabled >  
                      </div>
                    </div>
                  </div> 
          
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="edu_degree"> คุณวุฒิ :   </label>  
                        <textarea class="form-control" name="edu_degree" id="edu_degree" cols="12" rows="5" disabled>ปริญญาโท ในสาขาวิชาใดวิชาหนึ่ง ทางความสัมพันธ์ระหว่างประเทศ, ทางการระหว่างประเทศและการทูต, ทางการเมืองและการระหว่างประเทศ, ทางเอเชียศึกษา, ทางเอเชียตะวันออกเฉียงใต้ศึกษา, ทางอาเซียนศึกษา </textarea>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="spcfeatures"> เหล่า :   </label>
                        <input type="number" class="form-control text-right" id="positionno" name="positionno" value=" นบ." disabled >  

                      </div>
                    </div>  

                    <div class="col-md-9">
                      <div class="form-group">
                        <label for="spcfeatures"> คุณสมบัติเฉพาะเพิ่มเติม :   </label>
                        <select  id="provinceaddr"  name="provinceaddr" class="form-control block " >
                            <option value="1"> คุณสมบัติเฉพาะเพิ่มเติม 1  </option>
                            <option value="2"> คุณสมบัติเฉพาะเพิ่มเติม 2 </option>
                            <option value="3"> คุณสมบัติเฉพาะเพิ่มเติม 3 </option>
                          </select> 

                      </div>
                    </div> 
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="spcfeatures"> เพศ :   </label>
                        <div class="row skin icheck_minimal ">  
                          <div class="col-sm-12 "> 
                            <fieldset class="d-inline-block mr-1" >
                              <input type="radio" name="input-radio-3" id="input-radio-11" checked disabled>
                              <label for="input-radio-11">ชาย/หญิง</label>
                            </fieldset>
                            <fieldset class="d-inline-block mr-1" >
                              <input type="radio" name="input-radio-3" id="input-radio-12" disabled>
                              <label for="input-radio-12">ชาย</label>
                            </fieldset>
                            <fieldset class="d-inline-block  " >
                              <input type="radio" name="input-radio-3" id="input-radio-13" disabled>
                              <label for="input-radio-13">หญิง</label>
                            </fieldset>
                          </div>  
                        </div>
                      </div>
                    </div>   

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="positionname"> ตำแหน่ง : </label>
                          <select  id="positionname"  name="positionname" class="form-control block " disabled>
                            <option value="1"> นายทหารควบคุมการเบิกจ่าย </option>
                            <option value="2"> นายทหารตรวจสอบ  </option>
                            <option value="3"> นายทหารนโยบายและแผน </option>
                          </select> 
                      </div>
                    </div>

                  </div>

                </div>
              </div>
            </div>
            <div class="modal-footer">
              <div  class="m-auto ">
                <button type="button" class="btn btn2 btn-success   round"> <i class="fa fa-save"></i> บันทึก </button>
                <button type="button" class="btn btn2 btn-danger   round"  data-dismiss="modal"> <i class="fa fa-times-circle-o"></i> ยกเลิก </button> 
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>


     <!-- Modal -->
   <div class="modal fade text-left modal_custom1" id="modalDuplicate" tabindex="-1" role="dialog" aria-labelledby="modalDuplicate"  aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header bg_custom1">
            <h5 class="modal-title">ทำซ้ำข้อมูล </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form class="form_custom1">
            <div class="modal-body">
              <div class="row">
                <div class="col-10 m-auto pt-2">  

                  <div class="row">
                    <div class="col-md-3 col-10">
                      <div class="form-group">
                        <label for="groupname"> กลุ่ม : </label>
                        <input type="text" class="form-control" id="groupname" name="groupname" value="DOF-01" >  
                      </div>
                    </div>
                    <div class="col-md-3 col-10">
                      <div class="form-group">
                        <label for="positioncode"> รหัสตำแหน่ง : </label>
                        <input type="text" class="form-control" id="positioncode" name="positioncode" value="101310" >  
                      </div>
                    </div>
                    <div class="col-md-3 col-10">
                      <div class="form-group">
                        <label for="provinceaddr"> ที่ตั้งหน่วย : </label>
                          <select  id="provinceaddr"  name="provinceaddr" class="form-control block " >
                            <option value="1"> กรุงเทพฯ </option>
                            <option value="2"> กระบี่  </option>
                            <option value="3"> กาฬสินธุ์ </option>
                          </select> 
                      </div>
                    </div>
                    <div class="col-md-3 col-10">
                      <div class="form-group">
                        <label for="positionno"> จำนวน : </label>
                        <input type="number" class="form-control text-right" id="positionno" name="positionno" value="15"  >  
                      </div>
                    </div>
                  </div> 
          
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="edu_degree"> คุณวุฒิ :   </label>  
                        <textarea class="form-control" name="edu_degree" id="edu_degree" cols="12" rows="5" >ปริญญาโท ในสาขาวิชาใดวิชาหนึ่ง ทางความสัมพันธ์ระหว่างประเทศ, ทางการระหว่างประเทศและการทูต, ทางการเมืองและการระหว่างประเทศ, ทางเอเชียศึกษา, ทางเอเชียตะวันออกเฉียงใต้ศึกษา, ทางอาเซียนศึกษา </textarea>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="spcfeatures"> เหล่า :   </label>
                        <input type="number" class="form-control text-right" id="positionno" name="positionno" value=" นบ."  >  

                      </div>
                    </div>  

                  

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="spcfeatures"> เพศ :   </label>
                        <div class="row skin icheck_minimal ">  
                          <div class="col-sm-12 "> 
                            <fieldset class="d-inline-block mr-1" >
                              <input type="radio" name="input-radio-3" id="input-radio-11" checked >
                              <label for="input-radio-11">ชาย/หญิง</label>
                            </fieldset>
                            <fieldset class="d-inline-block mr-1" >
                              <input type="radio" name="input-radio-3" id="input-radio-12" >
                              <label for="input-radio-12">ชาย</label>
                            </fieldset>
                            <fieldset class="d-inline-block  " >
                              <input type="radio" name="input-radio-3" id="input-radio-13" >
                              <label for="input-radio-13">หญิง</label>
                            </fieldset>
                          </div>  
                        </div>
                      </div>
                    </div> 

                  </div>

                  <div class="row">
                    <div class="col-md-10">
                      <div class="form-group">
                        <label for="spcfeatures"> คุณสมบัติเฉพาะเพิ่มเติม :   </label>
                        <select  id="provinceaddr"  name="provinceaddr" class="form-control block " >
                            <option value="1"> คุณสมบัติเฉพาะเพิ่มเติม 1  </option>
                            <option value="2"> คุณสมบัติเฉพาะเพิ่มเติม 2 </option>
                            <option value="3"> คุณสมบัติเฉพาะเพิ่มเติม 3 </option>
                          </select> 
                      </div>
                    </div> 
                    <div class="col-md-2 text-center mt-2 pt-1">
                    <button class="btn round btn1 btncustom1">  <i class="fa fa-plus"></i> <span >เพิ่ม </span> </button>

                 
                    </div>

                  </div>


                  <div class="row">

                    <div class="col-md-10">
                      <div class="form-group">
                        <label for="positionname"> ตำแหน่ง : </label>
                          <select  id="positionname"  name="positionname" class="form-control block " >
                            <option value="1"> นายทหารควบคุมการเบิกจ่าย </option>
                            <option value="2"> นายทหารตรวจสอบ  </option>
                            <option value="3"> นายทหารนโยบายและแผน </option>
                          </select> 
                      </div>
                    </div>

                    <div class="col-md-2 text-center mt-2 pt-1">
                       
                      <button class="btn round btn1 btncustom1">  <i class="fa fa-plus"></i> <span >เพิ่ม </span> </button>
                    </div>

                  </div>

                </div>
              </div>
            </div>
            <div class="modal-footer">
              <div  class="m-auto ">
                <button type="button" class="btn btn2 btn-success round"> <i class="fa fa-save"></i> ทำซ้ำข้อมูล </button>
                <button type="button" class="btn btn2 btn-danger round"  data-dismiss="modal"> <i class="fa fa-times-circle-o"></i> ยกเลิก </button> 
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>


 
 
    <!-- footer -->
  
  
    <?php include '../include/footer.php'; ?> 

  <script src="../../Controllers/planManpowerController.js"></script>
  <script type="text/javascript">
    $(document).ready(function() { 

      $("#planManTypeId").change(function(){
        // alert( $(this).val() );
         $('.tb1').addClass('hidden');

         $('#planManTypeId2').addClass('hidden'); 
         if( $(this).val() == 1 || $(this).val() == 0){
            $('#planManpowerTable1').removeClass('hidden');
            
            $('.boxsearchplan2').removeClass('hidden');
            $('.boxsearchplan1').addClass('hidden');


         }else if( $(this).val() ==2 ){ 
          
            $('.boxsearchplan2').addClass('hidden');
            $('.boxsearchplan1').removeClass('hidden');

            $('#planManTypeId2').removeClass('hidden'); 
            $('#planManpowerTable2').removeClass('hidden'); 
         }else if( $(this).val() == 3 ) {
            $('#planManpowerTable4').removeClass('hidden');

            $('.boxsearchplan2').addClass('hidden');
            $('.boxsearchplan1').removeClass('hidden');

         }

      });

      $("#planManTypeId2").change(function(){
        $('.tb1').addClass('hidden');
         if( $(this).val() ==22 ){
            $('#planManpowerTable3').removeClass('hidden'); 
          }else{
            $('#planManpowerTable2').removeClass('hidden'); 
          }
      });


    }); 
  </script>
  