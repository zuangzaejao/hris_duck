<?php


$sql_Position_t15 = "SELECT posi.HrtPosSemiOrgName,posi.HrtPosOrgName,posi.HrtPosOrgAbbrName,
                            orgt.OrgTypeID,orgt.OrgTypeName,
                            arm.ArmAbbrName,arm.ArmName,
                            pss.SkillNo,
                            pch.CommPositionID,pch.PosPerStatus,pch.RankID,pch.PersonChgID,
                            pch.CommandTitle,pch.CommandDocDate,pch.EffDate,  
                            pch.CommandItemNo,pch.CommDocNo,pch.SourceOfCommID,pch.CommDocDate,pch.CommEffDate 
          FROM HrtPersonChangeHist pch 
          LEFT JOIN HrtPosition posi ON posi.HrtPositionID = pch.CommPositionID AND posi.OrgTypeID = pch.OrgTypeID
          LEFT JOIN OrgType orgt ON pch.OrgTypeID = orgt.OrgTypeID
          LEFT JOIN HrtArm arm ON pch.ArmCode = arm.ArmCode
          LEFT JOIN HrtPosStrucSkill pss ON pch.CommPositionID = pss.PositionID
          WHERE pch.PersonID = '$PersonID'
          ORDER BY pch.PersonChgID DESC";
  $query_Position_t15 = sqlsrv_query($conn,$sql_Position_t15);
  $data_Position_t15 = array();
  while($row_Position_t15 = sqlsrv_fetch_array($query_Position_t15, SQLSRV_FETCH_ASSOC ))
  { $data_Position_t15[] = $row_Position_t15; }
  // echo $sql_Position_t15;

    //---------- ----- โครงสร้าง
    $sql_OrgType = "SELECT OrgTypeID,OrgTypeName FROM OrgType WHERE 1=1 ";
    $query_OrgType = sqlsrv_query($conn,$sql_OrgType);
    $data_OrgType = array();
    while($row_OrgType = sqlsrv_fetch_array($query_OrgType, SQLSRV_FETCH_ASSOC ))
    { $data_OrgType[$row_OrgType['OrgTypeID']] = $row_OrgType['OrgTypeName']; }

    //---------- ----- สภาพตำแหน่ง
    $sql_PosPerStatus = "SELECT PosPerStatus,PosPerStatusDesc FROM HrtPosPerStatus WHERE 1=1 ";
    $query_PosPerStatus = sqlsrv_query($conn,$sql_PosPerStatus);
    $data_PosPerStatus = array();
    while($row_PosPerStatus = sqlsrv_fetch_array($query_PosPerStatus, SQLSRV_FETCH_ASSOC ))
    { $data_PosPerStatus[$row_PosPerStatus['PosPerStatus']] = $row_PosPerStatus['PosPerStatusDesc']; }

    //--------- ----- เงินเดือนอัตรา
    $sql_RankFix = "SELECT RankCode,RankFixAbbrTh FROM HrtRankFix WHERE 1=1 ORDER BY RankFixID ASC ";
    $query_RankFix = sqlsrv_query($conn,$sql_RankFix);
    $data_RankFix = array();
    while($row_RankFix = sqlsrv_fetch_array($query_RankFix, SQLSRV_FETCH_ASSOC ))
    { $data_RankFix[$row_RankFix['RankCode']] = $row_RankFix['RankFixAbbrTh']; }

?>
<?php //include_once '../include/modelOnload.php' ?>
<div class="tab-pane" id="posi_tab12" aria-labelledby="position-tab12">
  <div class="row">
    <div class="col-12">
      <div class="float-left">
        <a href="#"  data-toggle="modal" data-target="#from_posi2" class="btn btn1 mb-1 round" id="create_posiTab12"><span class="fa fa-plus-circle"></span> เพิ่ม</a>
        <a href="#" class="btn btn1 mb-1 round"><span class="fa fa-trash-o"></span> ลบ</a>
      </div>
    </div>
  </div> 
  <?php
  // echo '<pre>'.print_r($data_Position_t15,true).'<pre>';
  ?>
  <table class="table table-striped table-borderless table-hover bootstrap-3 ">
    <thead>
      <tr align="center" style="background-color:#0f1733; color:whitesmoke;">
        <th width="2%"><input type="checkbox" class="checkAll" onclick="toggle(this);" /></th>
        <th width="10%"></th>
        <th width="10%">ลำดับที่</th>
        <th width="10%">โครงสร้าง</th>
        <th width="35%">ตำแหน่ง</th>
        <th width="23%">สภาพตำแหน่ง</th>
        <th width="10%">วันที่มีผล</th>
      </tr>
    </thead>
    <tbody align="center">
      <?php
        if($data_Position_t15){
        foreach($data_Position_t15 as $key_pt15 => $val_pt15 ){
          $set_box_id = $val_pt15['PersonChgID'];
     ?>
          <input type="text" id="ipt15_1_<?=$set_box_id?>" style="display:none" value="<?=$val_pt15['OrgTypeID']?>">
          <input type="text" id="ipt15_2_<?=$set_box_id?>" style="display:none" value="<?=$val_pt15['HrtPosOrgName']?>">
          <input type="text" id="ipt15_3_<?=$set_box_id?>" style="display:none" value="<?=$val_pt15['HrtPosOrgAbbrName']?>">
          <input type="text" id="ipt15_4_<?=$set_box_id?>" style="display:none" value="<?=$val_pt15['PosPerStatus']?>">
          <input type="text" id="ipt15_5_<?=$set_box_id?>" style="display:none" value="<?=$val_pt15['RankID']?>">
          <input type="text" id="ipt15_6_<?=$set_box_id?>" style="display:none" value="<?=$val_pt15['ArmAbbrName']?>">
          <input type="text" id="ipt15_7_<?=$set_box_id?>" style="display:none" value="<?=$val_pt15['SkillNo']?>">
          <input type="text" id="ipt15_8_<?=$set_box_id?>" style="display:none" value="<?=$val_pt15['CommandTitle']?>">
          <input type="text" id="ipt15_9_<?=$set_box_id?>" style="display:none" value="<?=$val_pt15['CommandDocDate']?>">
          <input type="text" id="ipt15_10_<?=$set_box_id?>" style="display:none" value="<?=$val_pt15['EffDate']?>">
          <input type="text" id="ipt15_11_<?=$set_box_id?>" style="display:none" value="<?=$val_pt15['CommandItemNo']?>">
          <input type="text" id="ipt15_12_<?=$set_box_id?>" style="display:none" value="<?=$val_pt15['CommDocNo']?>">
          <input type="text" id="ipt15_13_<?=$set_box_id?>" style="display:none" value="<?=$val_pt15['SourceOfCommID']?>">
          <input type="text" id="ipt15_14_<?=$set_box_id?>" style="display:none" value="<?=$val_pt15['CommDocDate']?>">
          <input type="text" id="ipt15_15_<?=$set_box_id?>" style="display:none" value="<?=$val_pt15['CommEffDate']?>">
          <input type="text" id="ipt15_16_<?=$set_box_id?>" style="display:none" value="<?=$val_pt15['CommPositionID']?>">
        <tr>
          <td><input type="checkbox" class="checkAll"/></td>
          <td>
            <a href="#" data-toggle="modal" data-target="#from_posi2" onClick="editPosition('<?=$set_box_id?>');"><i class="la la-pencil-square-o" style="color:#0f1733;"></i></a>
            <a href="#"><i class="la la-trash-o" style="color:#0f1733;"></i></a>
          </td>
          <td><?php echo $val_pt15['PersonChgID']; ?></td>
          <td><?php echo $val_pt15['OrgTypeName']; ?></td>
          <td align="left">
            
            <?php 
            // echo $val_pt15['HrtPosSemiOrgName'];
            //echo $val_pt15['CommPositionID'];  
            
            $dataPosDet =$clsorg->LoadPositionDetail($val_pt15['CommPositionID']); 
            echo $dataPosDet['HrtPosName'];
            if($dataPosDet['OrgLevelDID']){
              $dataorg =$clsorg->LoadDataOrgParent($dataPosDet['OrgLevelDID']);
              echo " ".$dataorg['listname']['orgNameAbbr'] ;
            }
            
            ?>
          </td>
          <td align="left"><?php echo $data_PosPerStatus[$val_pt15['PosPerStatus']]; ?></td>
          <td align="left"><?php echo DateOnlyDisplay($val_pt15['CommEffDate'],8); ?></td>
        </tr>
      <?php
          }
        }
      ?>
  </tbody>
  </table>

  <div class="modal animated slideInUp text-left modal_custom1" id="from_posi2" role="dialog" aria-labelledby="modalSettingRegis"  aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <!---------- ---------- Start Content ---------- ---------- -->
          <div class="card-body">
              <div class="model-header" style="background-color:#0f1733;">
              <!-- -------------------- -->
                <div class="row">
                  <div class="col-md-11">
                    <h6 class="model-title text-white px-2 pt-2 py-1">บันทึก/แก้ไข การดำรงตำแหน่ง 
                      <?php echo $data_HrtRank['HrtRankAbbrTh']." ".$data_Person['PersonName']." ".$data_Person['SurName']." หมายเลขประจำตัว : ".$data_Person['AirForceID']; ?></h6>
                  </div>
                  <div class="col-md-1">
                    <h4 class="model-title text-white pt-2"><a data-dismiss="modal" onClick="ClearForm_t15();"><i class="fa fa-times-circle-o"></i></a></h4>
                  </div>
                </div>
              <!-- -------------------- -->
              </div>
              <div class="model-body">
              <!-- -------------------- -->
          <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">

              <div class="row"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">โครงสร้าง :
                        <select class="select2 form-control" style="width: 100%;" name="OrgTypeID_t15" id="OrgTypeID_t15">
                          <option value="">โปรดเลือก โครงสร้าง</option>
                            <?php
                                foreach($data_OrgType as $key_OrgType => $val_OrgType){
                                ?>
                                  <option value="<?php echo $key_OrgType;?>"><?php echo $val_OrgType; ?></option>
                                <?php
                                }
                            ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">สังกัด :
                        <select class="select2 form-control" style="width: 100%;" name="OrgLevel2_t15" id="OrgLevel2_t15" >
                          <option value="">โปรดเลือก สังกัด</option>
                        </select>
                      </div>
                    </div>
                  </div>
              </div>  

              <div class="row"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">หน่วย/สังกัด :
                        <select class="form-control select2" style="width: 100%;" name="OrgLevelAll_t15" id="OrgLevelAll_t15" >
                        <option value="">โปรดเลือก หน่วย/สังกัด</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6"> 
                    
                  </div>
              </div>

               <div class="row"> 
                  <div class="col-md-12"> 
                    <div class="card-block">
                      <div class="card-body ">ตำแหน่ง :
                        <select class="form-control select2" style="width: 100%;"  name="OrgPosition_t15" id="OrgPosition_t15">
                          <option value="">โปรดเลือก ตำแหน่ง</option>
                        </select>
                        <!-- <input class="input form-control" style="width: 100%;" placeholder="" name="CommPositionID_t15" id="CommPositionID_t15"> -->
                      </div>
                    </div>
                  </div>
                </div> 

              <div class="row"> 
                <div class="col-md-12 ">
                  <div class="card-body">
                  <textarea class="form-control" style="width: 100%;" rows="4" name="HrtPosOrgName_t15" id="HrtPosOrgName_t15" disabled></textarea>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6 ">
                  <div class="card-body">ตำแหน่งย่อ :
                    <input class="input form-control" style="width: 100%;" name="HrtPosOrgAbbrName_t15" id="HrtPosOrgAbbrName_t15" disabled>
                  </div>
                </div>
                <div class="col-md-6"> 
                  <div class="card-block">
                    <div class="card-body ">สภาพตำแหน่ง :
                      <select class="select2 form-control" style="width: 100%;" name="PosPerStatus_t15" id="PosPerStatus_t15">
                        <option value="">โปรดเลือก สภาพตำแหน่ง</option>
                          <?php
                              foreach($data_PosPerStatus as $key_pps => $val_pps){
                              ?>
                                <option value="<?php echo $key_pps;?>"><?php echo $val_pps; ?></option>
                              <?php
                              }
                          ?>
                      </select>
                    </div>
                  </div>
                </div>
              </div> 

              <div class="row">
                <div class="col-md-6"> 
                  <div class="card-block">
                    <div class="card-body ">เงินเดือนอัตรา :
                      <select class="select2 form-control" style="width: 100%;" name="RankID_t15" id="RankID_t15">
                      <option value="">โปรดเลือก เงินเดือนอัตรา</option>
                          <?php
                              foreach($data_RankFix as $key_RankFix => $val_RankFix){
                              ?>
                                <option value="<?php echo $key_RankFix;?>"><?php echo $val_RankFix; ?></option>
                              <?php
                              }
                          ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 ">
                </div>
              </div>

              <div class="row"> 
                  <div class="col-md-6 ">
                    <div class="card-body">เหล่า :
                      <input class="input form-control" style="width: 100%;" name="HrtArmAbbrName_t15" id="HrtArmAbbrName_t15" disabled>
                    </div>
                  </div> 
                  <div class="col-md-6 ">
                    <div class="card-body">ลชทอ. :
                      <input class="input form-control" style="width: 100%;" name="SkillNo_t15" id="SkillNo_t15" disabled>
                    </div> 
                  </div> 
              </div>

              <div class="row"> 
                  <div class="col-md-12 ">
                    <div class="card-body">รายการ :
                    <input class="input form-control" style="width: 100%;" name="CommandTitle_t15" id="CommandTitle_t15">
                    </div>
                  </div> 
              </div>

              <div class="row"> 
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">ตั้งแต่วันที่ :</label>
                        <input type="text" class="form-control pickadate-translations" name="CommandDocDate_t15" id="CommandDocDate_t15" style="width: 80%;" data-value="" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">ถึงวันที่ :</label>
                        <input type="text" class="form-control pickadate-translations" name="EffDate_t15" id="EffDate_t15" style="width: 80%;" data-value="" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>

              <div class="row">
                  <div class="col-md-12 ">
                    <div class="card-block">
                      <div class="card-body "><h6>ตามคำสั่ง</h6></div>
                    </div>
                  </div>
              </div>

              <div class="row"> 
                  <div class="col-md-3 ">
                    <div class="card-body">ลำดับที่ในคำสั่ง :
                      <input class="input form-control" style="width: 100%;" name="CommandItemNo_t15" id="CommandItemNo_t15">
                    </div>
                  </div> 
                  <div class="col-md-3 ">
                    <div class="card-body">เลขที่คำสั่ง :
                      <input class="input form-control" style="width: 100%;" name="CommDocNo_t15" id="CommDocNo_t15">
                    </div>
                  </div>
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">คำสั่ง :
                        <select class="select2 form-control" style="width: 100%;" name="SourceOfCommID_t15" id="SourceOfCommID_t15">
                        <option value="">โปรดเลือก คำสั่ง</option>
                          <?php
                              $sql_SOC = "SELECT SourceOfCommID,SourceOfCommName From HrtSourceOfComm where 1=1 ";
                              $query_SOC = sqlsrv_query($conn, $sql_SOC );
                              $data_SOC = array();
                              while($row_SOC = sqlsrv_fetch_array($query_SOC, SQLSRV_FETCH_ASSOC ))
                              { $data_SOC[] = $row_SOC ; }

                              if($data_SOC){
                                foreach($data_SOC as $key_SOC => $val_SOC ){
                                ?>
                                  <option value="<?php echo $val_SOC['SourceOfCommID'];?>">
                                    <?php echo $val_SOC['SourceOfCommName']; ?> </option>
                                <?php
                                }
                              }   
                          ?>
                        </select>
                      </div>
                    </div>
                  </div> 
            </div>

            <div class="row"> 
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">ลง :</label>
                        <input type="text" class="form-control pickadate-translations" name="CommDocDate_t15" id="CommDocDate_t15" style="width: 80%;" data-value="" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">วันที่มีผล :</label>
                        <input type="text" class="form-control pickadate-translations" name="CommEffDate_t15" id="CommEffDate_t15" style="width: 80%;" data-value="" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
 
              <input type="text" name="PersonID_t15" id="PersonID_t15" style="display:none" value="<?=$PersonID?>">
              <input type="text" name="CommPositionID_t15" id="CommPositionID_t15" style="display:none" value="">
          </form>
              <!-- -------------------- -->
              </div>
            <br><!-- ---------- เว้นระยะห่าง ---------- -->
              <div class="tab-content px-1 pt-1">
                <div class="form-actions center" align="center">
                  <button type="button" class="btn btn-success round btn-min-width mr-1 mb-1" id="submit" name="submit" data-target="#modalConfirm" onclick="insertOrganizationGroupType()">
                    <i class="fa fa-save"></i>&nbsp;บันทึก</button>
                  <button type="button" class="btn btn-danger round btn-min-width mr-1 mb-1" id="type-error" data-dismiss="modal" onClick="ClearForm_t15();">
                    <i class="fa fa-times-circle-o"></i>&nbsp;ยกเลิก</button>
                </div>
              </div>
          </div>
        <!---------- ---------- End ---------- ---------- -->
      </div> 
    </div> 
  </div>

</div>


<script src="../../Asset/js/jquery-3.3.1.js"  type="text/javascript"></script>
<script src="../../app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
<script src="../../Controllers/Ducklab/org.script.js"></script>
<script src="../../Controllers/Ducklab/org2.script.js"></script>
<script type="text/javascript">
  function editPosition(condition_value)
    {
      $('#OrgTypeID_t15').val($('#ipt15_1_'+condition_value).val()).trigger("change");
      $('#HrtPosOrgName_t15').val($('#ipt15_2_'+condition_value).val());
      $('#HrtPosOrgAbbrName_t15').val($('#ipt15_3_'+condition_value).val());
      $('#PosPerStatus_t15').val($('#ipt15_4_'+condition_value).val()).trigger("change");
      $('#RankID_t15').val($('#ipt15_5_'+condition_value).val()).trigger("change");
      $('#HrtArmAbbrName_t15').val($('#ipt15_6_'+condition_value).val());
      $('#SkillNo_t15').val($('#ipt15_7_'+condition_value).val());
      $('#CommandTitle_t15').val($('#ipt15_8_'+condition_value).val());
      $('#CommandDocDate_t15').val($('#ipt15_9_'+condition_value).val());
      $('#EffDate_t15').val($('#ipt15_10_'+condition_value).val());
      $('#CommandItemNo_t15').val($('#ipt15_11_'+condition_value).val());
      $('#CommDocNo_t15').val($('#ipt15_12_'+condition_value).val());
      $('#SourceOfCommID_t15').val($('#ipt15_13_'+condition_value).val()).trigger("change");
      $('#CommDocDate_t15').val($('#ipt15_14_'+condition_value).val());
      $('#CommEffDate_t15').val($('#ipt15_15_'+condition_value).val()); 
      $('#CommPositionID_t15').val($('#ipt15_16_'+condition_value).val());



      org.LoadDataPositionById($('#ipt15_16_'+condition_value).val());
      //ipt15_16_
    }

  function ClearForm_t15()
    {
        var list_object_M = new Array('OrgTypeID_t15','PosPerStatus_t15','RankID_t15','SourceOfCommID_t15',
        'HrtPosOrgName_t15','HrtPosOrgAbbrName_t15','HrtArmAbbrName_t15','SkillNo_t15','CommandTitle_t15','CommandDocDate_t15',
        'EffDate_t15','CommandItemNo_t15','CommDocNo_t15','CommDocDate_t15','CommEffDate_t15','OrgLevelAll_t15');
        for(clear_value = 0; clear_value <= 14; clear_value++){
          $('#'+list_object_M[clear_value]).val('');
          if(clear_value < 4){ $('#'+list_object_M[clear_value]).trigger("change"); }
        }
    }

    function ClearForm_t15_step2()
    {
        var list_object_M = new Array( 'HrtPosOrgName_t15','HrtPosOrgAbbrName_t15','PosPerStatus_t15','RankID_t15','HrtArmAbbrName_t15','SkillNo_t15');
        for(clear_value = 0; clear_value <= 14; clear_value++){
          $('#'+list_object_M[clear_value]).val('');
          if(clear_value < 4){ $('#'+list_object_M[clear_value]).trigger("change"); }
        }
    }


 

  
</script>

<script  >
  $(document).ready(function() {
    
    //ClearForm_t15
    $("a#create_posiTab12").click(function(){
      ClearForm_t15();
    });

    $("#OrgTypeID_t15").change(function(){  
      org.LoadOrg2SelByOrgTypeID('',$("#OrgTypeID_t15").val(),'OrgLevel2_t15');
      org.LoadDataOrgSel('','','','OrgLevelAll_t15');
      org.LoadPosPersonByOrgLevelDIDSel( '','','OrgPosition_t15',''); 
    }); 

    $("#OrgLevel2_t15").change(function(){   
      org.LoadDataOrgSel('',$("#OrgTypeID_t15").val(),$("#OrgLevel2_t15").val(),'OrgLevelAll_t15'); 
      ClearForm_t15_step2();
    }); 

    $("#OrgLevelAll_t15").change(function(){   
      ClearForm_t15_step2();
      org.LoadPosPersonByOrgLevelDIDSel( '','','OrgPosition_t15', $(this).val()  ); 
    }); 

    $("#OrgPosition_t15").change(function(){   
      org.LoadPosPersonDetail($(this).val() ,$(this).find(':selected').data("posno"), $('#OrgLevelAll_t15').val()); 
    }); 
   
});


  // org.script.js Using function
  // org.LoadOrg2SelByOrgTypeID
  // org.LoadDataOrgSel
  // org.LoadPosPersonByOrgLevelDIDSel
  // org.LoadPosPersonDetail

  ////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////

</script>