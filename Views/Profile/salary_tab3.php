<div class="tab-pane" id="sal_tab13" aria-labelledby="salary-tab13">
  <div class="row">
    <div class="col-12">
      <div class="float-left">
        <a href="#" class="btn btn1 mb-1 round"><span class="fa fa-plus-circle"></span> เพิ่ม</a>
        <a href="#" class="btn btn1 mb-1 round"><span class="fa fa-trash-o"></span> ลบ</a>
      </div>
    </div>
  </div> 
  <table class="table table-striped table-borderless table-hover bootstrap-3 ">
    <thead>
      <tr align="center" style="background-color:#0f1733; color:whitesmoke;">
        <th><input type="checkbox" class="checkAll" onclick="toggle(this);" /></th>
        <th></th>
        <th>ลำดับที่</th>
        <th>ประเภทการลา</th>
        <th>ตั้งแต่</th>
        <th>ถึง</th>
        <th>สถานที่</th>
        <th>วันที่ลาสิกขา</th>
        <th>วันที่กลับเข้ารับราชการ</th>
      </tr>
    </thead>
    <tbody align="center">
      <tr>
        <td><input type="checkbox" class="checkAll" /></td>
        <td>
          <a href="#" data-toggle="modal" data-target="#from_sal3"><i class="la la-pencil-square-o" style="color:#0f1733;"></i></a>
          <a href="#"><i class="la la-trash-o" style="color:#0f1733;"></i></a>
        </td>
        <td>1</td>
        <td>ลาอุปสมบท</td>
        <td>1 ก.ค.2558</td>
        <td>1 ก.ค.2558</td>
        <td>ลาดสนุ่น ต.บาง อ.ทอง จ.ปทุมธานี</td>
        <td>1 ก.ค.2558</td>
        <td>1 ก.ค.2558</td>
      </tr>
    </tbody>
  </table>
  <div class="modal animated slideInUp text-left modal_custom1" id="from_sal3" tabindex="-1" role="dialog" aria-labelledby="modalSettingRegis"  aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <!---------- ---------- Start Content ---------- ---------- -->
          <div class="card-body">
              <div class="model-header" style="background-color:#0f1733;">
              <!-- -------------------- -->
                <div class="row">
                  <div class="col-md-11">
                    <h6 class="model-title text-white px-2 pt-2 py-1">บันทึก/แก้ไข ลาอุปสมบท/ประกอบพิธีฮัจย์ 
                      <?php echo $data_HrtRank['HrtRankAbbrTh']." ".$data_Person['PersonName']."   ".$data_Person['SurName']."  หมายเลขประจำตัว : ".$data_Person['AirForceID']; ?></h6>
                  </div>
                  <div class="col-md-1">
                    <h4 class="model-title text-white pt-2"><a data-dismiss="modal"><i class="fa fa-times-circle-o"></i></a></h4>
                  </div>
                </div>
              <!-- -------------------- -->
              </div>
              <div class="model-body">
              <!-- -------------------- -->
   <!------------------- ส่วนที่ 1 -4  ----------------------------->
   <br>

        <div class="card collapse-icon accordion-icon-rotate active">
          <div class="card-header bg-success">
            <a data-toggle="collapse" href="#motab5_s3_h1" aria-expanded="true" aria-controls="motab5_s3_h1" class="card-title lead white">
              <h6><U>ส่วนที่ 1</U>  รายละเอียดลาอุปสมบท/ประกอบพิธีฮัจย์ </h6></a>
          </div>
          <div id="motab5_s3_h1" role="tabpanel" class="card-collapse collapse show" aria-expanded="true">
            <div class="card-content">
              <div class="row"> 
                    <div class="col-md-6"> 
                        <div class="card-block">
                        <div class="card-body ">ประเภทการลา :
                            <select class="select2 form-control" style="width: 100%;">
                            <option></option>
                            </select>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-6 ">
                        <div class="card-body" id="NTT">ศาสนา :
                        <input class="input form-control" style="width: 100%;" placeholder=" ">
                        </div>
                    </div> 
              </div>

              <div class="row"> 
                <div class="col-md-6">
                <div class="card-block">
                    <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">วันที่ขอลาอุปสมบทตั้งแต่ :</label>
                            <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                    <div class="input-group-append">
                        <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                    </div>
                    </div>
                </div>
                </div>
                <div class="col-md-6">
                <div class="card-block">
                    <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">ถึงวันที่  :</label>
                            <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                    <div class="input-group-append">
                        <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                    </div>
                    </div>
                </div>
                </div>
            </div>

            <div class="row"> 
                <div class="col-md-6">
                <div class="card-block">
                    <div class="input-group col-12 datep">
                    <label class="label-control col-12 pl-0">วันที่อุปสมบท  :</label>
                    <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                    <div class="input-group-append">
                        <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                    </div>
                    </div>
                </div>
                </div>
                <div class="col-md-6"></div>
            </div>

            <br>
            <div class="row"> 
                <div class="col-md-12"> 
                <div class="card-block">
                    <div class="card-body ">สถานที่อุปสมบท ณ วัด :
                    <select class="select2 form-control" style="width: 100%;">
                        <option></option>
                    </select>
                    </div>
                </div>
                </div>
            </div>

            <div class="row"> 
                <div class="col-md-6"> 
                    <div class="card-block">
                        <div class="card-body ">ตำบล :
                        <select class="select2 form-control" style="width: 100%;">
                            <option></option>
                        </select>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6"> 
                    <div class="card-block">
                        <div class="card-body ">อำเภอ :
                        <select class="select2 form-control" style="width: 100%;">
                            <option></option>
                        </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row"> 
                <div class="col-md-6"> 
                    <div class="card-block">
                        <div class="card-body ">จังหวัด :
                        <select class="select2 form-control" style="width: 100%;">
                            <option></option>
                        </select>
                        </div>
                    </div>
                </div>
                    <div class="col-md-6"></div>
            </div>

            <div class="row"> 
                  <div class="col-md-12 ">
                    <div class="card-body" id="NTT">พระอุปัชฌาย์ :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div>  
            </div> 

            <div class="row"> 
                  <div class="col-md-12 ">
                    <div class="card-body" id="NTT">สถานที่ ณ วัด :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div>  
            </div>

            <div class="row"> 
                <div class="col-md-6"> 
                    <div class="card-block">
                        <div class="card-body ">ตำบล :
                        <select class="select2 form-control" style="width: 100%;">
                            <option></option>
                        </select>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6"> 
                    <div class="card-block">
                        <div class="card-body ">อำเภอ :
                        <select class="select2 form-control" style="width: 100%;">
                            <option></option>
                        </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row"> 
                <div class="col-md-6"> 
                    <div class="card-block">
                        <div class="card-body ">จังหวัด :
                        <select class="select2 form-control" style="width: 100%;">
                            <option></option>
                        </select>
                        </div>
                    </div>
                </div>
                    <div class="col-md-6"></div>
            </div>

            <div class="row"> 
                  <div class="col-md-12 ">
                    <div class="card-body" id="NTT">หมายเหตุ :
                     <textarea class="input form-control"></textarea>
                    </div>
                  </div> 
            </div>

            <div class="row"> 
                <div class="col-md-6">
                <div class="card-block">
                    <div class="input-group col-12 datep">
                    <label class="label-control col-12 pl-0">วันที่ลาสิกขา  :</label>
                    <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                    <div class="input-group-append">
                        <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                    </div>
                    </div>
                </div>
                </div>
                <div class="col-md-6">
                <div class="card-block">
                    <div class="input-group col-12 datep">
                    <label class="label-control col-12 pl-0">วันที่รายงานตัวกลับเข้ารับราชการ  :</label>
                    <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                    <div class="input-group-append">
                        <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                    </div>
                    </div>
                </div>
                </div>
            </div>  
          </div>
      </div>
            <!-- --------------------------ส่วนที่ 2 ---------------------------->
      <br> 
          <div class="card collapse-icon accordion-icon-rotate active">
            <div class="card-header bg-success">
              <a data-toggle="collapse" href="#motab5_s3_h2" aria-expanded="true" aria-controls="motab5_s3_h2"class="card-title lead white">
                <h6><U>ส่วนที่ 2</U> หลักฐานที่ได้รับอนุญาตให้ลาจากหน่วยต้นสังกัด</h6></a>
            </div>
            <div id="motab5_s3_h2" role="tabpanel" class="card-collapse collapse show" aria-expanded="true">
              <div class="card-content">

               <div class="row"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">คำสั่ง :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 ">
                      <div class="card-body" id="NTT">ลำดับที่ในคำสั่ง :
                        <input class="input form-control" style="width: 100%;" placeholder=" ">
                      </div>
                    </div>
                    <div class="col-md-3 ">
                      <div class="card-body" id="NTT">เลขที่หนังสือ :
                        <input class="input form-control" style="width: 100%;" placeholder=" ">
                      </div>
                    </div>
                </div>

                <div class="row"> 
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">ลงวันที่  :</label>
                        <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">วันที่มีผล  :</label>
                        <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row"> 
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">วันที่อนุญาต/วันที่หน่วยเสนอรายงาน  :</label>
                        <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6"></div>
                </div>

                <div class="row"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">ยศ :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">ชื่อ - สกุล :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>   
              </div>
            </div>
          </div>

            <!-- <------------------------------- ส่วนที่ 3--------------> 
      <br>
          <div class="card collapse-icon accordion-icon-rotate active">
            <div class="card-header bg-success">
              <a data-toggle="collapse" href="#motab5_s3_h3" aria-expanded="true" aria-controls="motab5_s3_h3"class="card-title lead white">
                <h6><U>ส่วนที่ 3</U> หลักฐานที่ได้รับอนุญาตให้ลาจาก ผบ.ทอ.</h6></a>
            </div>
            <div id="motab5_s3_h3" role="tabpanel" class="card-collapse collapse show" aria-expanded="true">
              <div class="card-content">
              <div class="row"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">คำสั่ง :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 ">
                      <div class="card-body" id="NTT">ลำดับที่ในคำสั่ง :
                        <input class="input form-control" style="width: 100%;" placeholder=" ">
                      </div>
                    </div>

                    <div class="col-md-3 ">
                      <div class="card-body" id="NTT">เลขที่หนังสือ :
                        <input class="input form-control" style="width: 100%;" placeholder=" ">
                      </div>
                    </div>
                </div>
                <div class="row"> 
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">ลงวันที่  :</label>
                        <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">วันที่มีผล  :</label>
                        <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row"> 
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">วันที่อนุญาต :</label>
                        <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6"></div>
                </div>
          </div>
        </div>
      </div>
      <br>

      <br>
      </div>
      <!-- -----------------------------จบส่วนที่1-3--------------------------- -->

              <!-- -------------------- -->
              </div>
            <br><!-- ---------- เว้นระยะห่าง ---------- -->
              <div class="tab-content px-1 pt-1">
                <div class="form-actions center" align="center">
                  <button type="button" class="btn btn-success round btn-min-width mr-1 mb-1" id="submit" name="submit" data-target="#modalConfirm" onclick="insertOrganizationGroupType()">
                    <i class="fa fa-save"></i>&nbsp;บันทึก</button>
                  <button type="button" class="btn btn-danger round btn-min-width mr-1 mb-1" id="type-error" data-dismiss="modal">
                    <i class="fa fa-times-circle-o"></i>&nbsp;ยกเลิก</button>
                </div>
              </div>
          </div>
        <!---------- ---------- End ---------- ---------- -->
      </div> 
    </div> 
  </div>








</div>