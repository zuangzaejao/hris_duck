<div class="tab-pane" id="sal_tab12" aria-labelledby="salary-tab12">
  <div class="row">
    <div class="col-12">
      <div class="float-left">
        <a href="#" class="btn btn1 mb-1 round"><span class="fa fa-plus-circle"></span> เพิ่ม</a>
        <a href="#" class="btn btn1 mb-1 round"><span class="fa fa-trash-o"></span> ลบ</a>
      </div>
    </div>
  </div> 
  <table class="table table-striped table-borderless table-hover bootstrap-3 ">
    <thead>
      <tr align="center" style="background-color:#0f1733; color:whitesmoke;">
        <th><input type="checkbox" class="checkAll" onclick="toggle(this);" /></th>
        <th></th>
        <th>ลำดับที่</th>
        <th>รายการ</th>
        <th>ตั้งแต่</th>
        <th>ถึง</th>
        <th>ได้รับวันทวีคูณเนื่องจาก</th>
      </tr>
    </thead>
    <tbody align="center">
      <tr>
        <td><input type="checkbox" class="checkAll" /></td>
        <td>
          <a href="#" data-toggle="modal" data-target="#from_sal2"><i class="la la-pencil-square-o" style="color:#0f1733;"></i></a>
          <a href="#"><i class="la la-trash-o" style="color:#0f1733;"></i></a>
        </td>
        <td>1</td>
        <td>มีสิทธิ์ได้นับเวลาราชการเป็นทวีคูณตามแผนเพื่อความมั่นคงของประเทศ</td>
        <td>1 ก.ค. 2558</td>
        <td>1 ก.ค. 2558</td>
        <td>จบภารกิจ</td>
      </tr>
    </tbody>
  </table>
  <div class="modal animated slideInUp text-left modal_custom1" id="from_sal2" tabindex="-1" role="dialog" aria-labelledby="modalSettingRegis"  aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <!---------- ---------- Start Content ---------- ---------- -->
          <div class="card-body">
              <div class="model-header" style="background-color:#0f1733;">
              <!-- -------------------- -->
                <div class="row">
                  <div class="col-md-11">
                    <h6 class="model-title text-white px-2 pt-2 py-1">บันทึก/แก้ไข การศึกษาระหว่างรับราชการ 
                      <?php echo $data_HrtRank['HrtRankAbbrTh']." ".$data_Person['PersonName']."   ".$data_Person['SurName']."  หมายเลขประจำตัว : ".$data_Person['AirForceID']; ?></h6>
                  </div>
                  <div class="col-md-1">
                    <h4 class="model-title text-white pt-2"><a data-dismiss="modal"><i class="fa fa-times-circle-o"></i></a></h4>
                  </div>
                </div>
              <!-- -------------------- -->
              </div>
      <div class="model-body">
      <!-- ---------------------------------------------ส่วนที่1-2------------ -->
      <div class="card collapse-icon accordion-icon-rotate active">
        <br>
          <div class="card-header bg-success">
            <a data-toggle="collapse" href="#motab5_s2_h1" aria-expanded="true" aria-controls="motab5_s2_h1" class="card-title lead white">
              <h6><U>ส่วนที่ 1</U>  ประวัติการได้รับวันทวีคูณ(ตามคำสั่ง) </h6></a>
          </div>
          <div id="motab5_s2_h1" role="tabpanel" class="card-collapse collapse show" aria-expanded="true">
            <div class="card-content">

              <div class="row"> 
                <div class="col-md-6"> 
                  <div class="card-block">
                    <div class="card-body ">คำสั่ง :
                      <select class="select2 form-control" style="width: 100%;">
                        <option></option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="card-body" id="NTT">ลำดับที่ในคำสั่ง :
                    <input class="input form-control" style="width: 100%;" placeholder=" ">
                  </div>
                </div> 
                <div class="col-md-3 ">
                  <div class="card-body" id="NTT">เลขที่คำสั่ง :
                    <input class="input form-control" style="width: 100%;" placeholder=" ">
                  </div>
                </div> 
              </div>

              <div class="row"> 
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="input-group col-12 datep">
                      <label class="label-control col-12 pl-0">ลง  :</label>
                      <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                      <div class="input-group-append">
                        <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="input-group col-12 datep">
                      <label class="label-control col-12 pl-0">วันที่มีผล  :</label>
                      <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                      <div class="input-group-append">
                        <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <br>
              <div class="row">  
                <div class="col-md-12">
                  <div class="card-body ">ได้รับวันทวีคูณเนื่องจาก :
                    <div class="card-content">
                      <div class="card-body">
                        <div class="d-inline-block custom-control custom-radio mr-1">
                          <input type="radio" class="custom-control-input" name="Edu_hit_st1" id="sal_hst_1">
                          <label class="custom-control-label">จบภารกิจ</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                          <input type="radio" class="custom-control-input" name="Edu_hit_st1" id="sal_hst_2">
                          <label class="custom-control-label">เสียชีวิต</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                          <input type="radio" class="custom-control-input" name="Edu_hit_st1" id="sal_hst_2">
                          <label class="custom-control-label">ทุพพลภาพ</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                          <input type="radio" class="custom-control-input" name="Edu_hit_st1" id="sal_hst_2">
                          <label class="custom-control-label">ประกาศของทางราชการ</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row"> 
                  <div class="col-md-12 ">
                    <div class="card-body" id="NTT">รายการ :
                    <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div> 
              </div>

              <div class="row"> 
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">วันที่มีผล  :</label>
                        <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6"></div>
              </div>

              <div class="row"> 
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="input-group col-12 datep">
                      <label class="label-control col-12 pl-0">เริ่มวันทวีคูณ  :</label>
                      <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                      <div class="input-group-append">
                        <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="input-group col-12 datep">
                      <label class="label-control col-12 pl-0">สิ้นสุดวันทวีคูณ  :</label>
                      <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                      <div class="input-group-append">
                        <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row"> 
                  <div class="col-md-2 ">
                    <div class="card-body" id="NTT">จำนวน :
                      <input class="input form-control" style="text-align:right" value="ปี">
                    </div>
                  </div> 
                  <div class="col-md-2 ">
                    <div class="card-body" id="NTT">&nbsp; 
                      <input class="input form-control" style="text-align:right" value="เดือน">
                    </div>
                  </div>
                  <div class="col-md-2 ">
                    <div class="card-body" id="NTT">&nbsp; 
                      <input class="input form-control" style="text-align:right" value="วัน" >
                    </div>
                  </div> 
                  <div class="col-md-2 ">
                    <div class="card-body" id="NTT">ลา :
                      <input class="input form-control" style="text-align:right" value="วัน">
                    </div>
                  </div> 
                  <div class="col-md-2 ">
                    <div class="card-body" id="NTT">&nbsp; 
                      <input class="input form-control" style="text-align:right" value="ชั่วโมง" >
                    </div>
                  </div> 
                  <div class="col-md-2 ">
                    <div class="card-body" id="NTT">&nbsp; 
                      <input class="input form-control" style="text-align:right" value="นาที">
                    </div>
                  </div>  
            </div>

            <div class="row"> 
                <div class="col-md-2 ">
                  <div class="card-body" id="NTT">ป่วย :
                    <input class="input form-control" style="text-align:right" value="วัน">
                  </div>
                </div> 

                <div class="col-md-2 ">
                  <div class="card-body" id="NTT">&nbsp; 
                    <input class="input form-control" style="text-align:right" value="ชั่วโมง">
                  </div>
                </div>

                <div class="col-md-2 ">
                  <div class="card-body" id="NTT">&nbsp; 
                    <input class="input form-control" style="text-align:right" value="นาที">
                  </div>
                </div> 

                <div class="col-md-2 ">
                  <div class="card-body" id="NTT">ไปต่างประเทศ :
                    <input class="input form-control" style="text-align:right" value="วัน">
                  </div>
                </div> 

                <div class="col-md-2 ">
                  <div class="card-body" id="NTT">&nbsp; 
                    <input class="input form-control" style="text-align:right" value="ชั่วโมง">
                  </div>
                </div> 

                <div class="col-md-2 ">
                  <div class="card-body" id="NTT">&nbsp; 
                    <input class="input form-control" style="text-align:right" value="นาที ">
                  </div>
                </div>  
            </div>

            <div class="row"> 
                  <div class="col-md-8 ">
                    <div class="card-body" id="NTT">เลขที่เอกสารเริ่มต้น :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div>
                  <div class="col-md-1 ">
                    <div class="card-body" id="NTT"> &nbsp;
                    
                    </div>
                    <h3>/</h3>
                  </div> 
                  <div class="col-md-3 ">
                    <div class="card-body" id="NTT"> &nbsp;
                      <input class="input form-control" style="width: 100%;" >
                    </div>
                  </div> 
            </div>

            <div class="row"> 
              <div class="col-md-6"> 
                <div class="card-block">
                  <div class="card-body ">โครงสร้าง :
                    <select class="select2 form-control" style="width: 100%;">
                      <option></option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-6"> 
                <div class="card-block">
                  <div class="card-body ">สังกัด :
                    <select class="select2 form-control" style="width: 100%;">
                      <option></option>
                    </select>
                  </div>
                </div>
              </div>
            </div>

            <div class="row"> 
              <div class="col-md-6"> 
                <div class="card-block">
                  <div class="card-body ">หน่วย/สังกัด :
                    <select class="select2 form-control" style="width: 100%;">
                      <option></option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-6"></div>
            </div>

          <div class="row"> 
            <div class="col-md-12"> 
              <div class="card-block">
                <div class="card-body ">ตำแหน่ง :
                  <select class="select2 form-control" style="width: 100%;">
                    <option></option>
                  </select>
                </div>
              </div>
            </div>
           </div> 

            <div class="row"> 
              <div class="col-md-6"> 
                <div class="card-block">
                  <div class="card-body ">เงินเดือนอัตรา :
                    <select class="select2 form-control" style="width: 100%;">
                      <option></option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-6"> 
                <div class="card-block">
                  <div class="card-body ">เหล่า :
                    <select class="select2 form-control" style="width: 100%;">
                      <option></option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="row"> 
              <div class="col-md-6"> 
                <div class="card-block">
                  <div class="card-body ">ลชทอ. :
                    <select class="select2 form-control" style="width: 100%;">
                      <option></option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-6"></div>
            </div>
          </div>
      </div>

      <br>
      <!-- -------------------------  ส่วนที่ 2 --------------------------------------- -->
          <div class="card collapse-icon accordion-icon-rotate active">
            <div class="card-header bg-success">
              <a data-toggle="collapse" href="#motab5_s2_h2" aria-expanded="true" aria-controls="motab5_s2_h2"class="card-title lead white">
                <h6><U>ส่วนที่ 2</U> ประวัติการได้รับวันทวีคูณ(ตามคำสั่งบรรจุ)</h6></a>
            </div>
            <div id="motab5_s2_h2" role="tabpanel" class="card-collapse collapse show" aria-expanded="true">
              <div class="card-content">

                <div class="row">
                  <div class="col-md-12 ">
                    <div class="card-block">
                      <div class="card-body "><h6>ตามคำสั่งบรรจุ</h6></div>
                    </div>
                  </div>
                  </div>

                <div class="row"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">คำสั่ง :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 ">
                    <div class="card-body" id="NTT">ลำดับที่ในคำสั่ง :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div>
                  <div class="col-md-3 ">
                    <div class="card-body" id="NTT">เลขที่คำสั่ง :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div>  
                </div>

                <div class="row"> 
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">ลง  :</label>
                        <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">วันที่บรรจุ  :</label>
                        <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> 

                <div class="row"> 
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">ถึงวันที่  :</label>
                        <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6"></div>
                </div> 

                <div class="row">
                  <div class="col-md-12 ">
                    <div class="card-block">
                      <div class="card-body "><h6>ตามคำสั่งพ้น</h6></div>
                    </div>
                  </div>
                </div>

                <div class="row"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">คำสั่ง :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 ">
                    <div class="card-body" id="NTT">ลำดับที่ในคำสั่ง :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div>
                  <div class="col-md-3 ">
                    <div class="card-body" id="NTT">เลขที่คำสั่ง :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div>  
                </div>

                <div class="row"> 
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">ลง  :</label>
                        <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">วันที่มีผล  :</label>
                        <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> 
              </div>
            </div>
          </div>
           <!-- ------------------------- END ส่วนที่ 2 --------------------------------------- -->
      <br>
      </div>
              <!-- ------------------------------------------------------ -->
              </div>
            <br><!-- ---------- เว้นระยะห่าง ---------- -->
              <div class="tab-content px-1 pt-1">
                <div class="form-actions center" align="center">
                  <button type="button" class="btn btn-success round btn-min-width mr-1 mb-1" id="submit" name="submit" data-target="#modalConfirm" onclick="insertOrganizationGroupType()">
                    <i class="fa fa-save"></i>&nbsp;บันทึก</button>
                  <button type="button" class="btn btn-danger round btn-min-width mr-1 mb-1" id="type-error" data-dismiss="modal">
                    <i class="fa fa-times-circle-o"></i>&nbsp;ยกเลิก</button>
                </div>
              </div>
          </div>
        <!---------- ---------- End ---------- ---------- -->
      </div> 
    </div> 
  </div>








</div>