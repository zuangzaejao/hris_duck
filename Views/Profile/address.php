<div class="tab-pane" id="ad_tab11" aria-labelledby="address-tab11">
  <div class="container">
    <div class="card-content">
      <div class="card-body">
        <div class="card collapse-icon accordion-icon-rotate active">
          <div id="headingCollapse31" class="card-header bg-success">
            <a data-toggle="collapse" href="#collapse44" aria-expanded="true" aria-controls="collapse44" class="card-title lead white">
              <h6><U>ส่วนที่ 1</U> ที่อยู่ที่เกิด </h6></a>
          </div>
          <div id="collapse44" role="tabpanel" aria-labelledby="headingCollapse44" class="card-collapse collapse show" aria-expanded="true">
            <div class="card-content">
                <div class="row">
                  <div class="col-md-3">
                    <div class="card-block">
                      <div class="input-group">ที่อยู่เลขที่ :
                        <input type="text" class="form-control" style="width: 90%;" id="formGroupExampleInput" placeholder="">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3"></div>
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group">ชื่อหมู่บ้าน / อาคาร :
                        <input type="text" class="form-control" style="width: 100%;" id="formGroupExampleInput" placeholder="">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <div class="card-block">
                      <div class="input-group">หมู่ :
                        <input type="text" class="form-control" style="width: 100%;" id="formGroupExampleInput" placeholder="">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="card-block">
                      <div class="input-group">ซอย :
                        <input type="text" class="form-control" style="width: 100%;" id="formGroupExampleInput" placeholder="">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group">ถนน :
                        <input type="text" class="form-control" style="width: 100%;" id="formGroupExampleInput" placeholder="">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <div class="card-block">
                      <div class="input-group" id="province_hr">จังหวัด :
                        <select class="form-control" id="exampleFormControlSelect1">
                          <option value="">กรุณาเลือก จังหวัด</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="card-block">
                      <div class="input-group" id="amphur_hr">อำเภอ/เขต :
                        <select class="form-control" id="exampleFormControlSelect1">
                          <option value="">กรุณาเลือก อำเภอ/เขต</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="card-block">
                      <div class="input-group" id="district_hr">ตำบล/แขวง :
                        <select class="form-control" id="exampleFormControlSelect1">
                          <option value="">กรุณาเลือก ตำบล/แขวง</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="card-block">
                      <div class="input-group" id="zip_hr">รหัสไปรษณีย์ :
                        <select class="form-control" id="exampleFormControlSelect1">
                          <option value="">กรุณาเลือก รหัสไปรษณีย์</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <div class="card-block">
                      <div class="input-group">โทรศัพท์ :
                        <input type="text" class="form-control" style="width: 100%;" id="formGroupExampleInput" placeholder="">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="card-block">
                      <div class="input-group" id="nation_hr">ประเทศ :
                        <select class="form-control" id="exampleFormControlSelect1">
                          <option value="">กรุณาเลือก ประเทศ</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6"></div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="card-block">
                      <div class="card-body">ที่อยู่รวม :
                        <textarea class="form-control" id="tareaColor1" style="width: 100%;" rows="5"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
          </div>
      <br>
          <div class="card collapse-icon accordion-icon-rotate active">
            <div id="headingCollapse45"class="card-header bg-success">
              <a data-toggle="collapse" href="#collapse45" aria-expanded="true" aria-controls="collapse45"class="card-title lead white">
                <h6><U>ส่วนที่ 2</U> ปัจจุบัน</h6></a>
            </div>
            <div id="collapse45" role="tabpanel" aria-labelledby="headingCollapse45" class="card-collapse collapse show" aria-expanded="true">
              <div class="card-content">
                <div class="row">  
                  <div class="col-md-12 mx-0 my-2 px-4 pt-1">
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="checkbox" value="option1">
                      <label class="form-check-label">ที่อยู่ปัจจุบันที่เดียวกับที่อยู่ทะเบียนบ้าน</label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      <br>
          <div class="card collapse-icon accordion-icon-rotate active">
            <div id="headingCollapse46"class="card-header bg-success">
              <a data-toggle="collapse" href="#collapse46" aria-expanded="true" aria-controls="collapse46"class="card-title lead white">
                <h6><U>ส่วนที่ 3</U> ตามทะเบียนบ้าน</h6></a>
            </div>
            <div id="collapse46" role="tabpanel" aria-labelledby="headingCollapse46" class="card-collapse collapse show" aria-expanded="true">
              <div class="card-content">
                <div class="row">
                  <div class="col-md-3">
                    <div class="card-block">
                      <div class="input-group">ที่อยู่เลขที่ :
                        <input type="text" class="form-control" style="width: 100%;" id="formGroupExampleInput" placeholder="">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3"></div>
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group">ชื่อหมู่บ้าน / อาคาร :
                        <input type="text" class="form-control" style="width: 100%;" id="formGroupExampleInput" placeholder="">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <div class="card-block">
                      <div class="input-group">หมู่ :
                        <input type="text" class="form-control" style="width: 100%;" id="formGroupExampleInput" placeholder="">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="card-block">
                      <div class="input-group">ซอย :
                        <input type="text" class="form-control" style="width: 100%;" id="formGroupExampleInput" placeholder="">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group">ถนน :
                        <input type="text" class="form-control" style="width: 100%;" id="formGroupExampleInput" placeholder="">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <div class="card-block">
                      <div class="input-group" id="province_hr">จังหวัด :
                        <select class="form-control" id="exampleFormControlSelect1">
                          <option value="">กรุณาเลือก จังหวัด</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="card-block">
                      <div class="input-group" id="amphur_hr">อำเภอ/เขต :
                        <select class="form-control" id="exampleFormControlSelect1">
                          <option value="">กรุณาเลือก อำเภอ/เขต</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="card-block">
                      <div class="input-group" id="district_hr">ตำบล/แขวง :
                        <select class="form-control" id="exampleFormControlSelect1">
                          <option value="">กรุณาเลือก ตำบล/แขวง</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="card-block">
                      <div class="input-group" id="zip_hr">รหัสไปรษณีย์ :
                        <select class="form-control" id="exampleFormControlSelect1">
                          <option value="">กรุณาเลือก รหัสไปรษณีย์</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <div class="card-block">
                      <div class="input-group">โทรศัพท์ :
                        <input type="text" class="form-control" style="width: 100%;" id="formGroupExampleInput" placeholder="">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="card-block">
                      <div class="input-group" id="nation_hr">ประเทศ :
                        <select class="form-control" id="exampleFormControlSelect1">
                          <option value="">กรุณาเลือก ประเทศ</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="card-block">
                      <div class="input-group">วันที่เข้าพัก :
                        <input type="text" class="form-control pickadate-disable-dates" placeholder="" aria-describedby="button-addon4">
                        <div class="input-group-append">
                          <button class="btn btn-primary" type="button" style=" padding-bottom: 1px; padding-top: 1px;">
                          <i class="la la-calendar-o"></i></button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="card-block">
                      <div class="card-body">ที่อยู่รวม :
                        <textarea class="form-control" id="tareaColor1" style="width: 100%;" rows="5"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      <br>
          <div class="card collapse-icon accordion-icon-rotate active">
            <div id="headingCollapse47"class="card-header bg-success">
              <a data-toggle="collapse" href="#collapse47" aria-expanded="true" aria-controls="collapse47"class="card-title lead white">
                <h6><U>ส่วนที่ 4</U> ปัจจุบัน</h6></a>
            </div>
            <div id="collapse47" role="tabpanel" aria-labelledby="headingCollapse47" class="card-collapse collapse show" aria-expanded="true">
              <div class="card-content">
                <div class="row">  
                  <div class="col-md-12 mx-0 my-2 px-4 pt-1">
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="checkbox" value="option1">
                      <label class="form-check-label">ที่อยู่ปัจจุบันที่เดียวกับที่อยู่ทะเบียนบ้าน</label>
                    </div>
                  </div>
                </div>
                <div class="row mt-2"> 
                  <div class="col-12 d-flex justify-content-center"> 
                    <button type="button" class="btn btn-success  round btn-min-width mr-1 mb-1" id="submit" name="submit">
                      <i class="fa fa-save"></i> &nbsp;บันทึก </button>
                    <button type="button" class="btn btn-danger  round btn-min-width mr-1 mb-1" id="type-error">
                      <i class="fa fa-times-circle-o"></i> &nbsp;ยกเลิก</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
      <br>
        </div>
      </div>
    </div>
  </div>
</div>
