<div class="tab-pane" id="tab12" aria-labelledby="base-tab12">
  <a href="#" data-toggle="modal" data-target="#from_img" class="btn btn-social btn-min-width mb-1" style="background-color:#0f1733; color:white;">
    <span class="la la-plus-circle" style="color:white; font-weight: bold;font-size: 18px"></span>เพิ่ม</a>

  <section>
    <div class="row px-2">
      <?php 
        $sql_rankhist = "SELECT rh.RankID,rh.RankDate,rh.Remark,r.HrtRankAbbrTh,rh.RankSeq 
        FROM HrtRankHist rh LEFT JOIN HrtRank r ON (rh.RankID = r.HrtRankID)
        WHERE rh.PersonID = '$PersonID' ORDER BY rh.RankSeq DESC";
        $query_rankhist = sqlsrv_query($conn, $sql_rankhist );
        $data_rankhist = array();
        $runBack = 0;
        while($row=sqlsrv_fetch_array($query_rankhist, SQLSRV_FETCH_ASSOC ))
        { $data_rankhist[] = $row; $runBack++; }

        if($data_rankhist){
          foreach($data_rankhist as $key_rk => $val_rk ){
            $set_box_id = $val_rk['RankID'];

            switch($set_box_id)
            {
              case 30101 : $set_img_rank = "rank-sunyabut-01.png"; break;
              case 30102 : $set_img_rank = "rank-sunyabut-01.png"; break;
              case 30211 : $set_img_rank = "rank-sunyabut-02.png"; break;
              case 30212 : $set_img_rank = "rank-sunyabut-02.png"; break;
              case 30221 : $set_img_rank = "rank-sunyabut-03.png"; break;
              case 30222 : $set_img_rank = "rank-sunyabut-03.png"; break;
              case 30231 : $set_img_rank = "rank-sunyabut-04.png"; break;
              case 30232 : $set_img_rank = "rank-sunyabut-04.png"; break;
              case 30241 : $set_img_rank = "rank-sunyabut-05.png"; break;
              case 30242 : $set_img_rank = "rank-sunyabut-05.png"; break;
              case 30301 : $set_img_rank = "rank-sunyabut-06.png"; break;
              case 30302 : $set_img_rank = "rank-sunyabut-06.png"; break;
              case 30411 : $set_img_rank = "rank-sunyabut-06.png"; break;
              case 30412 : $set_img_rank = "rank-sunyabut-06.png"; break;
              case 31411 : $set_img_rank = "rank-sunyabut-07.png"; break;
              case 31412 : $set_img_rank = "rank-sunyabut-07.png"; break;
              case 30421 : $set_img_rank = "rank-sunyabut-07.png"; break;
              case 30422 : $set_img_rank = "rank-sunyabut-07.png"; break;
              case 31421 : $set_img_rank = "rank-sunyabut-08.png"; break;
              case 31422 : $set_img_rank = "rank-sunyabut-08.png"; break;
              case 30431 : $set_img_rank = "rank-sunyabut-08.png"; break;
              case 30432 : $set_img_rank = "rank-sunyabut-08.png"; break;
              case 31431 : $set_img_rank = "rank-sunyabut-09.png"; break;
              case 31432 : $set_img_rank = "rank-sunyabut-09.png"; break;
              case 30511 : $set_img_rank = "rank-sunyabut-09.png"; break;
              case 30512 : $set_img_rank = "rank-sunyabut-09.png"; break;
              case 31511 : $set_img_rank = "rank-sunyabut-10.png"; break;
              case 31512 : $set_img_rank = "rank-sunyabut-10.png"; break;
              case 30521 : $set_img_rank = "rank-sunyabut-10.png"; break;
              case 30522 : $set_img_rank = "rank-sunyabut-10.png"; break;
              case 31521 : $set_img_rank = "rank-sunyabut-11.png"; break;
              case 31522 : $set_img_rank = "rank-sunyabut-11.png"; break;
              case 30531 : $set_img_rank = "rank-sunyabut-11.png"; break;
              case 30532 : $set_img_rank = "rank-sunyabut-11.png"; break;
              case 31531 : $set_img_rank = "rank-pratuan-01.png"; break;
              case 31532 : $set_img_rank = "rank-pratuan-01.png"; break;
              case 30611 : $set_img_rank = "rank-pratuan-01.png"; break;
              case 30612 : $set_img_rank = "rank-pratuan-01.png"; break;
              case 30711 : $set_img_rank = "rank-pratuan-01.png"; break;
              case 30712 : $set_img_rank = "rank-pratuan-01.png"; break;
              case 30721 : $set_img_rank = "rank-pratuan-02.png"; break;
              case 30722 : $set_img_rank = "rank-pratuan-02.png"; break;
              case 30731 : $set_img_rank = "rank-pratuan-03.png"; break;
              case 30732 : $set_img_rank = "rank-pratuan-03.png"; break;
              case 30811 : $set_img_rank = "rank-pratuan-04.png"; break;
              case 30812 : $set_img_rank = "rank-pratuan-04.png"; break;
              case 30821 : $set_img_rank = "rank-pratuan-05.png"; break;
              case 30822 : $set_img_rank = "rank-pratuan-05.png"; break;
              case 30831 : $set_img_rank = "rank-pratuan-06.png"; break;
              case 30832 : $set_img_rank = "rank-pratuan-06.png"; break;
              case 30911 : $set_img_rank = "rank-pratuan-06.png"; break;
              default : $set_img_rank = "rank-default.png"; break;
            }
      ?>
              <input type="text" id="ipt2_1_<?=$set_box_id?>" style="display:none" value="<?=$val_rk['RankID']?>">
              <input type="text" id="ipt2_2_<?=$set_box_id?>" style="display:none" value="<?=$val_rk['HrtRankAbbrTh']?>">
              <input type="text" id="ipt2_3_<?=$set_box_id?>" style="display:none" value="<?=$val_rk['RankDate']?>">
              <input type="text" id="ipt2_4_<?=$set_box_id?>" style="display:none" value="<?=$val_rk['Remark']?>">

      <div class="col-sm-4" style="position:relative; min-height:1px; padding-right:15px;padding-left:15px;">
        <div class="well"  style="min-height:20px;padding:19px;margin-bottom:20px;background-color:#f5f5f5;border:1px solid #e3e3e3;border-radius:4px;">
          <table>
            <tr>
              <td rowspan="3" width="30%"><img src="../../Asset/images/user1.png" class="img-responsive" style="width:80%" alt="Image"></td>
              <td width="50%" align="left"><?php echo $runBack--; ?></td>
              <td width="20%" align="center"><img src="../../Asset/images/rank_icon/<?=$set_img_rank?>" class="img-responsive" style="width:50%" alt="Image"></td>
            </tr>
            <tr>
              <td colspan="2"><?php echo $val_rk['HrtRankAbbrTh']." ".$data_Person['PersonName']."   ".$data_Person['SurName']; ?></td>
            </tr>
            <tr>
              <td></td>
              <td align="center"><a href="#" data-toggle="modal" data-target="#from_img" onClick="editHrImage('<?=$set_box_id?>');"><i class="la la-pencil-square-o" style="color:#0f1733;"></i></a></td>
            </tr>
          </table>
        </div>
      </div>

      <?php 
          }
        }
      ?>

    </div>
  </section>

  <div class="modal animated slideInUp text-left modal_custom1" id="from_img" tabindex="-1" role="dialog" aria-labelledby="modalSettingRegis"  aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <!---------- ---------- Start Content ---------- ---------- -->
          <div class="card-body">
              <div class="model-header" style="background-color:#0f1733;">
              <!-- -------------------- -->
                <div class="row">
                  <div class="col-md-11">
                    <h6 class="model-title text-white px-2 pt-2 py-1">บันทึก/แก้ไข รูปภาพ :  
                      <?php echo $data_HrtRank['HrtRankAbbrTh']." ".$data_Person['PersonName']."   ".$data_Person['SurName']."  หมายเลขประจำตัว : ".$data_Person['AirForceID']; ?></h6>
                  </div>
                  <div class="col-md-1">
                    <h4 class="model-title text-white pt-2"><a data-dismiss="modal"  onClick="ClearForm_t2();"><i class="fa fa-times-circle-o"></i></a></h4>
                  </div>
                </div>
              <!-- -------------------- -->
              </div>
              <div class="model-body">
          <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">
              <!-- -------------------- -->
                <div class="row">  
                  <div class="col-md-12 pt-2" align="center">
                    <img src="../../Asset/images/user1.png" class="img-responsive" style="width:25%" alt="Image">
                    <p></p><b>(ขนาดรูปไม่เกิน 2MB)</b>
                  </div>
                </div>

                <div class="row pt-2 px-2"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">ยศ :
                        <select class="select2 form-control" style="width: 100%;" name="RankID_t2" id="RankID_t2">
                          <option value="" >   กรุณาเลือกยศ </option>
                          <?php
                              $sql_RankName = "SELECT HrtRankID,HrtRankAbbrTh,HrtRankNameTh From HrtRank where 1=1 ";
                              $query_RankName = sqlsrv_query($conn, $sql_RankName );
                              $data_RankName[] = array();
                              while($row_RankName = sqlsrv_fetch_array($query_RankName, SQLSRV_FETCH_ASSOC ))
                              { $data_RankName[] = $row_RankName ; }

                              if($data_RankName){
                                foreach($data_RankName as $key_RankName => $val_RankName ){
                                ?>
                                  <option value="<?php echo $val_RankName['HrtRankID']; ?>"
                                    <?php if($val_RankName['HrtRankID'] == "{$data_Person['RankID']}"){ echo "selected"; }?> >
                                    <?php echo $val_RankName['HrtRankNameTh']; ?> </option>
                                <?php
                                }
                              }   
                          ?>
                        </select>
                      </div>
                    </div>
                  </div> 
                  <div class="col-md-6 ">
                    <div class="card-body">ชื่อรูป :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div> 
                </div> 

                <div class="row px-2"> 
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">วัน/เดือน/ปี บรรจุ :</label>
                        <input type="text" class="form-control pickadate-translations" placeholder="" name="RankDate_t2" id="RankDate_t2" style="width: 80%;"
                          data-value="<?php echo GetToday('');?>" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="card-body">เปลี่ยนรูปใหม่ :
                      <input type="file" name="filUpload_t2[]" id="filUpload_t2" multiple="multiple" >
                    </div>
                  </div> 
                </div> 

                <div class="row  px-2"> 
                  <div class="col-md-12 ">
                    <div class="card-body">หมายเหตุ :
                      <textarea class="form-control" name="Remark_t2" id="Remark_t2" rows="3"></textarea>
                    </div>
                  </div> 
                </div> 
              <!-- -------------------- -->
              <input type="text" name="PersonID_t2" id="PersonID_t2" style="display:none" value="<?=$PersonID?>">
          </form>
              </div>
            <!-- <br>---------- เว้นระยะห่าง ---------- -->
              <div class="tab-content px-1 pt-1">
                <div class="form-actions center" align="center">
                  <button type="button" class="btn btn-success round btn-min-width mr-1 mb-1" id="submit" name="submit" data-target="#modalConfirm" onclick="insertOrganizationGroupType()">
                    <i class="fa fa-save"></i>&nbsp;บันทึก</button>
                  <button type="button" class="btn btn-danger round btn-min-width mr-1 mb-1" id="type-error" data-dismiss="modal" onClick="ClearForm_t2();">
                    <i class="fa fa-times-circle-o"></i>&nbsp;ยกเลิก</button>
                </div>
              </div>
          </div>
        <!---------- ---------- End ---------- ---------- -->
      </div> 
    </div> 
  </div>

</div>

<script type="text/javascript">
  function editHrImage(condition_value)
  {
    $('#RankID_t2').val($('#ipt2_1_'+condition_value).val()).trigger("change");
    $('#RankDate_t2').val($('#ipt2_3_'+condition_value).val());
    $('#Remark_t2').val($('#ipt2_4_'+condition_value).val());
  }

  function ClearForm_t2()
  {
      var list_object_M = new Array('RankID_t2', 'RankDate_t2', 'Remark_t2');
      for(clear_value = 0; clear_value <= 2; clear_value++){
        $('#'+list_object_M[clear_value]).val('');
        if(clear_value == 0){ $('#'+list_object_M[clear_value]).trigger("change"); }
      }
  }
</script>