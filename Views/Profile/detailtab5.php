
<?php 
    $sql_HrtArmHist ="SELECT a.HrtArmCode,a.HrtArmAbbrName,ah.Seq,ah.SourceOfCommID,ah.CommDocNo,ah.CommDocDate,ah.CommEffDate,ah.Remark,soc.SourceOfCommID,soc.SourceOfCommName 
    FROM HrtArmHist ah LEFT JOIN HrtArm a ON ah.ArmCode = a.HrtArmCode
    LEFT JOIN HrtSourceOfComm soc ON ah.SourceOfCommID = soc.SourceOfCommID
    WHERE ah.PersonID ='{$data_Person['PersonID']}' ";
    $query_HrtArmHist = sqlsrv_query($conn,$sql_HrtArmHist); 
    $data_HrtArmHist = array();
    while($row_HrtArmHist = sqlsrv_fetch_array($query_HrtArmHist, SQLSRV_FETCH_ASSOC ))
    { $data_HrtArmHist[] = $row_HrtArmHist; }

    //echo $sql_HrtArmHist;
?>
 
<div class="tab-pane" id="tab15" aria-labelledby="base-tab15">
  <a href="#" data-toggle="modal" data-target="#from_arm" class="btn btn-social btn-min-width mb-1" style="background-color:#0f1733; color:white;">
    <span class="la la-plus-circle" style="color:white; font-weight: bold;font-size: 18px"></span>เพิ่ม</a>
  <a href="#" class="btn btn-social btn-min-width mb-1" style="background-color:#0f1733; color:white;">
    <span class="la la-trash-o" style="color:white; font-weight: bold;font-size: 18px"></span>ลบ</a>

  <section>
    <table class="table table-striped table-borderless table-hover bootstrap-3 ">
      <thead>
        <tr align="center" style="background-color:#0f1733; color:whitesmoke;">
          <th><input type="checkbox" class="checkAll" onclick="toggle(this);"/></th>
          <th></th>
          <th>ลำดับที่</th>
          <th>เหล่า</th>
          <th>วันที่มีผล</th>
          <th>คำสั่ง</th>
          <th>เลขที่คำสั่ง</th>
          <th>ลง</th>
        </tr>
      </thead>
      <tbody align="center">
        <?php
          if($data_HrtArmHist){
            foreach($data_HrtArmHist as $key_AH => $val_AH ){
                $set_box_id = $val_AH['Seq'].$val_AH['HrtArmCode'];
        ?>
              <input type="text" id="ipt5_1_<?=$set_box_id?>" style="display:none" value="<?=$val_AH['HrtArmCode']?>">
              <input type="text" id="ipt5_2_<?=$set_box_id?>" style="display:none" value="<?=$val_AH['SourceOfCommID']?>">
              <input type="text" id="ipt5_3_<?=$set_box_id?>" style="display:none" value="<?=$val_AH['Seq']?>">
              <input type="text" id="ipt5_4_<?=$set_box_id?>" style="display:none" value="<?=$val_AH['CommDocNo']?>">
              <input type="text" id="ipt5_5_<?=$set_box_id?>" style="display:none" value="<?=$val_AH['CommDocDate']?>">
              <input type="text" id="ipt5_6_<?=$set_box_id?>" style="display:none" value="<?=$val_AH['CommEffDate']?>">
              <input type="text" id="ipt5_7_<?=$set_box_id?>" style="display:none" value="<?=$val_AH['Remark']?>">
        <tr>
          <td><input type="checkbox" class="checkAll" /></td>
          <td>
            <a href="#" data-toggle="modal" data-target="#from_arm" onClick="editHrtArm('<?=$set_box_id?>');"><i class="la la-pencil-square-o" style="color:#0f1733;"></i></a>
            <a href="#"><i class="la la-trash-o" style="color:#0f1733;"></i></a>
          </td>
          <td><?php echo $val_AH['Seq']; ?></td>
          <td><?php echo $val_AH['HrtArmAbbrName']; ?></td>
          <td><?php echo DateOnlyDisplay($val_AH['CommEffDate'],8); ?></td>
          <td><?php echo $val_AH['SourceOfCommName']; ?></td>
          <td><?php echo $val_AH['CommDocNo']; ?></td>
          <td><?php echo DateOnlyDisplay($val_AH['CommDocDate'],8); ?></td>
        </tr>
        <?php
            }
          }
        ?>
      </tbody>
    </table>
  </section>

  <div class="modal animated slideInUp text-left modal_custom1" id="from_arm" tabindex="-1" role="dialog" aria-labelledby="modalSettingRegis"  aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <!---------- ---------- Start Content ---------- ---------- -->
          <div class="card-body">
              <div class="model-header" style="background-color:#0f1733;">
              <!-- -------------------- -->
                <div class="row">
                  <div class="col-md-11">
                    <h6 class="model-title text-white px-2 pt-2 py-1">บันทึก/แก้ไข เหล่าทหาร :  
                      <?php echo $data_HrtRank['HrtRankAbbrTh']." ".$data_Person['PersonName']."   ".$data_Person['SurName']."  หมายเลขประจำตัว : ".$data_Person['AirForceID']; ?></h6>
                  </div>
                  <div class="col-md-1">
                    <h4 class="model-title text-white pt-2"><a data-dismiss="modal" onClick="ClearForm_t5();"><i class="fa fa-times-circle-o"></i></a></h4>
                  </div>
                </div>
              <!-- -------------------- -->
              </div>
              <div class="model-body">
          <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">
              <!-- -------------------- -->
                <div class="row pt-2"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">เหล่า :
                        <select class="select2 form-control" style="width: 100%;" name="HrtArmCode_t5" id="HrtArmCode_t5">
                          <option value="">โปรดเลือก เหล่า</option>
                          <?php
                            $sql_HA = "SELECT HrtArmCode,HrtArmName From HrtArm where 1=1 ";
                            $query_HA = sqlsrv_query($conn, $sql_HA );
                            $data_HA = array();
                            while($row_HA = sqlsrv_fetch_array($query_HA, SQLSRV_FETCH_ASSOC ))
                            { $data_HA[] = $row_HA ; }

                        if($data_HA){
                          foreach($data_HA as $key_HA => $val_HA ){ //HrtPosCode,HrtPosName 
                          ?>
                            <option value="<?php echo $val_HA['HrtArmCode'];?>">
                              <?php echo $val_HA['HrtArmCode']." ".$val_HA['HrtArmName']; ?> </option>
                          <?php
                          }
                        }   
                          ?>
                        </select>
                      </div>
                    </div>
                  </div> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">คำสั่ง :
                        <select class="select2 form-control" style="width: 100%;" name="SourceOfCommID_t5" id="SourceOfCommID_t5">
                          <option value="">โปรดเลือก คำสั่ง</option>
                          <?php
                            $sql_SOC = "SELECT SourceOfCommID,SourceOfCommName From HrtSourceOfComm where 1=1 ";
                            $query_SOC = sqlsrv_query($conn, $sql_SOC );
                            $data_SOC = array();
                            while($row_SOC = sqlsrv_fetch_array($query_SOC, SQLSRV_FETCH_ASSOC ))
                            { $data_SOC[] = $row_SOC ; }

                        if($data_SOC){
                          foreach($data_SOC as $key_SOC => $val_SOC ){ //HrtPosCode,HrtPosName 
                          ?>
                            <option value="<?php echo $val_SOC['SourceOfCommID'];?>">
                              <?php echo $val_SOC['SourceOfCommName']; ?> </option>
                          <?php
                          }
                        }   
                          ?>
                        </select>
                      </div>
                    </div>
                  </div> 
                </div> 

                <div class="row"> 
                  <div class="col-md-3 pt-1">
                    <div class="card-body" id="NTT">ลำดับที่ในคำสั่ง :
                      <input class="input form-control" style="width: 100%;" placeholder=" " name="Seq_t5" id="Seq_t5">
                    </div>
                  </div> 
                  <div class="col-md-3 pt-1">
                    <div class="card-body">เลขที่คำสั่ง :
                      <input class="input form-control" style="width: 100%;" placeholder=" " name="CommDocNo_t5" id="CommDocNo_t5">
                    </div>
                  </div> 
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">ลง :</label>
                        <input type="text" class="form-control pickadate-translations" placeholder="" id="CommDocDate_t5" name="CommDocDate_t5" style="width: 80%;"
                          data-value="<?php echo GetToday('');?>" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> 

                <div class="row"> 
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">วันที่มีผล : </label>
                        <input type="text" class="form-control pickadate-translations" placeholder="" id="CommEffDate_t5" name="CommEffDate_t5" style="width: 80%;"
                          data-value="<?php echo date('Y-m-d');?>" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6"></div> 
                </div> 

                <div class="row"> 
                  <div class="col-md-12 ">
                    <div class="card-body">หมายเหตุ :
                      <textarea class="form-control" name="Remark_t5" id="Remark_t5" rows="4"></textarea>
                    </div>
                  </div> 
                </div> 
              <!-- -------------------- -->
              <input type="text" name="PersonID_t5" id="PersonID_t5" style="display:none" value="<?=$PersonID?>">
          </form>
              </div>
            <!-- <br>---------- เว้นระยะห่าง ---------- -->
              <div class="tab-content px-1 pt-1">
                <div class="form-actions center" align="center">
                  <button type="button" class="btn btn-success round btn-min-width mr-1 mb-1" id="submit" name="submit" data-target="#modalConfirm" onclick="insertOrganizationGroupType()">
                    <i class="fa fa-save"></i>&nbsp;บันทึก</button>
                  <button type="button" class="btn btn-danger round btn-min-width mr-1 mb-1" id="type-error" data-dismiss="modal" onClick="ClearForm_t5();">
                    <i class="fa fa-times-circle-o"></i>&nbsp;ยกเลิก</button>
                </div>
              </div>
          </div>
        <!---------- ---------- End ---------- ---------- -->
      </div> 
    </div> 
  </div>

</div>

<script type="text/javascript">
  function editHrtArm(condition_value)
  {
    $('#HrtArmCode_t5').val($('#ipt5_1_'+condition_value).val()).trigger("change");
    $('#SourceOfCommID_t5').val($('#ipt5_2_'+condition_value).val()).trigger("change");
    $('#Seq_t5').val($('#ipt5_3_'+condition_value).val());
    $('#CommDocNo_t5').val($('#ipt5_4_'+condition_value).val());
    $('#CommDocDate_t5').val($('#ipt5_5_'+condition_value).val());
    $('#CommEffDate_t5').val($('#ipt5_6_'+condition_value).val());
    $('#Remark_t5').val($('#ipt5_7_'+condition_value).val());
  }

  function ClearForm_t5()
  {
      var list_object_M = new Array('HrtArmCode_t5', 'SourceOfCommID_t5', 'Seq_t5', 'CommDocNo_t5', 'CommDocDate_t5', 'CommEffDate_t5', 'Remark_t5');
      for(clear_value = 0; clear_value <= 6; clear_value++){
        $('#'+list_object_M[clear_value]).val('');
        if(clear_value < 2){ $('#'+list_object_M[clear_value]).trigger("change"); }
      }
  }
</script>