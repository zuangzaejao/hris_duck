<div role="tabpanel" class="tab-pane active" id="tab11" aria-expanded="true" aria-labelledby="base-tab11">
  <div class="container">
    <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post" id="form_Tab1">
      <div class="card collapse-icon accordion-icon-rotate active">
        <div id="headingCollapse31" class="card-header bg-success">
          <a data-toggle="collapse" href="#collapse31" aria-expanded="true" aria-controls="collapse31" class="card-title lead white">
            <h6><U>ส่วนที่ 1</U>ข้อมูลบุคคล(ประวัติข้าราชการ)</h6></a>
        </div>
        <div id="collapse31" role="tabpanel" aria-labelledby="headingCollapse31" class="card-collapse collapse show" aria-expanded="true">
          <div class="card-content">
            <div class="row">
              <div class="col-md-6">
                <div class="card-body" id="Personnel_type">
                  ประเภทกำลังพล :
                  <select class="select2 form-control" style="width: 100%;" name="PersonTypeID_t1" id="PersonTypeID_t1" >
                    <option value="" >   กรุณาเลือกประเภทกำลังพล </option>
                    <?php
                        $sql_PST = "SELECT HrtPersonTypeID,HrtPersonTypeName From HrtPersonType where 1=1 ";
                        $query_PST = sqlsrv_query($conn, $sql_PST );
                        $data_PST = array();
                        while($row_PST = sqlsrv_fetch_array($query_PST, SQLSRV_FETCH_ASSOC ))
                        { $data_PST[] = $row_PST ; }

                        if($data_PST){
                          foreach($data_PST as $key_PST => $val_PST ){ //HrtPosCode,HrtPosName 
                          ?>
                            <option value="<?php echo $val_PST['HrtPersonTypeID']; ?>"
                              <?php if($val_PST['HrtPersonTypeID'] == "{$data_Person['PersonTypeID']}"){ echo "selected"; }?> >
                              <?php echo $val_PST['HrtPersonTypeName']; ?> </option>
                          <?php
                          }
                        }   
                     ?>
                  </select>
                </div> 
              </div>
              <div class="col-md-6">                         
                <div class="card-body"id="Government_Number">หมายเลขประจำตัวข้าราชการ :
                  <input class="input form-control" style="width: 100%;"
                    placeholder="" name="AirForceID_t1" id="AirForceID_t1" value="<?php echo $data_Person['AirForceID']; ?>">
                </div>
              </div>
            </div>

            <div class="row">  
              <div class="col-md-6">    
                <div class="card-body"id="Rank_name">ยศ :
                  <select class="select2 form-control" style="width: 100%;" name="RankID_t1" id="RankID_t1" disabled >
                    <option value="" >   กรุณาเลือกยศ </option>
                    <?php
                        $sql_RankName = "SELECT HrtRankID,HrtRankAbbrTh,HrtRankNameTh From HrtRank where HrtRankID like '3%' ";
                        $query_RankName = sqlsrv_query($conn, $sql_RankName );
                        $data_RankName[] = array();
                        while($row_RankName = sqlsrv_fetch_array($query_RankName, SQLSRV_FETCH_ASSOC ))
                        { $data_RankName[] = $row_RankName ; }

                        if($data_RankName){
                          foreach($data_RankName as $key_RankName => $val_RankName ){
                          ?>
                            <option value="<?php echo $val_RankName['HrtRankID']; ?>"
                              <?php if($val_RankName['HrtRankID'] == "{$data_Person['RankID']}"){ echo "selected"; }?> >
                              <?php echo $val_RankName['HrtRankNameTh']; ?> </option>
                          <?php
                          }
                        }   
                     ?>
                  </select>
                </div>     
              </div>   
              <div class="col-md-6">    
                <div class="card-body" id="title_name">ยศก่อนสูญเสีย :
                  <select class="select2 form-control" style="width: 100%;" name="LostRankID_t1" id="LostRankID_t1">
                    <option value="" >   กรุณาเลือกยศ </option>
                    <?php
                        $sql_RankName = "SELECT HrtRankID,HrtRankAbbrTh,HrtRankNameTh From HrtRank where HrtRankID like '3%' ";
                        $query_RankName = sqlsrv_query($conn, $sql_RankName );
                        $data_RankName[] = array();
                        while($row_RankName = sqlsrv_fetch_array($query_RankName, SQLSRV_FETCH_ASSOC ))
                        { $data_RankName[] = $row_RankName ; }

                        if($data_RankName){
                          foreach($data_RankName as $key_RankName => $val_RankName ){
                          ?>
                            <option value="<?php echo $val_RankName['HrtRankID']; ?>"
                              <?php if($val_RankName['HrtRankID'] == "{$data_Person['RankID']}"){ echo "selected"; }?> >
                              <?php echo $val_RankName['HrtRankNameTh']; ?> </option>
                          <?php
                          }
                        }   
                     ?>
                  </select>
                </div>
              </div>
            </div>

            <div class="row">      
              <div class="col-md-6">
                <div class="card-block">
                  <div class="card-body" id="Title name">คำนำหน้าชื่อ :
                    <select class="select2 form-control" style="width: 100%;" name="PrefixID_t1" id="PrefixID_t1">
                      <option value="" >กรุณาเลือกคำนำหน้าชื่อ</option>
                      <?php
                          $sql_HrtPrefix = "SELECT HrtPrefixID,HrtPrefixAbbrNameTH,HrtPrefixNameTH From HrtPrefix where 1=1 ";
                          $query_HrtPrefix = sqlsrv_query($conn, $sql_HrtPrefix );
                          $data_HrtPrefix[] = array();
                          while($row_HrtPrefix = sqlsrv_fetch_array($query_HrtPrefix, SQLSRV_FETCH_ASSOC ))
                          { $data_HrtPrefix[] = $row_HrtPrefix ; }

                          if($data_HrtPrefix){
                            foreach($data_HrtPrefix as $key_HrtPrefix => $val_HrtPrefix ){
                            ?>
                              <option value="<?php echo $val_HrtPrefix['HrtPrefixID']; ?>"
                              <?php if($val_HrtPrefix['HrtPrefixID'] == "{$data_Person['PrefixID']}"){ echo "selected"; }?>>
                              <?php echo $val_HrtPrefix['HrtPrefixNameTH'];?></option>
                            <?php
                            }
                          }   
                      ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-6">  
                <div class="card-body" id="gender">เพศ:
                  <div class="card-content">
                    <div class="card-body">
                      <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input"
                               name="Gender_Sts_t1" id="subG_Sts1" value="M" <?php if($data_Person['GenderSts'] == 'M'){ echo "checked"; } ?>>
                        <label class="custom-control-label">เพศชาย</label>
                      </div>
                      <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input"
                               name="Gender_Sts_t1" id="subG_Sts2" value="F" <?php if($data_Person['GenderSts'] == 'F'){ echo "checked"; } ?>>
                        <label class="custom-control-label">เพศหญิง</label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">   
              <div class="col-md-6">
                <div class="card-body" id="name_thai">ชื่อ(ไทย) :
                  <input class="input form-control" style="width: 100%;" placeholder=" "
                   value="<?php echo $data_Person['PersonName']; ?>" name="PersonName_t1" id="PersonName_t1" disabled>
                </div> 
              </div>  
              <div class="col-md-6">         
                <div class="card-body" id="lastnames_thai">นามสกุล(ไทย) :
                  <input class="input form-control" style="width: 100%;" placeholder=" " 
                  value="<?php echo $data_Person['SurName']; ?>" name="SurName_t1" id="SurName_t1" disabled>
                </div>
              </div>   
            </div>        

            <div class="row">   
              <div class="col-md-6">
                <div class="card-body" id="name_Eng">ชื่อ(อังกฤษ) :
                  <input class="input form-control" style="width: 100%;" 
                  placeholder=" " value="<?php echo ucwords(strtolower($data_Person['EName'])); ?>" name="EName_t1" id="EName_t1">
                </div> 
              </div>  
              <div class="col-md-6">         
                <div class="card-body" id="lastnames_Eng">นามสกุล(อังกฤษ) :
                  <input class="input form-control" style="width: 100%;"
                  placeholder=" " value="<?php echo ucwords(strtolower($data_Person['ESurName'])); ?>" name="ESurName_t1" id="ESurName_t1">
                </div>
              </div>   
            </div>        

            <div class="row">      
              <div class="col-md-12">  
                <div class="card-body" id="Book">ประเภทแฟ้ม:
                  <div class="card-content">
                    <div class="card-body">
                      <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input" 
                              name="Book_Ps_t1" id="subBook_Ps1" value="1" <?php if($data_Person['FileType'] == '1'){ echo "checked"; } ?>>
                        <label class="custom-control-label">แผ่น</label>
                      </div>
                      <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input" 
                              name="Book_Ps_t1" id="subBook_Ps2" value="2" <?php if($data_Person['FileType'] == '2'){ echo "checked"; } ?>>
                        <label class="custom-control-label">เล่ม</label>
                      </div>
                      <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input" 
                              name="Book_Ps_t1" id="subBook_Ps3" value="3" <?php if($data_Person['FileType'] == '3'){ echo "checked"; } ?>>
                        <label class="custom-control-label">ชุดบรรจุซอง</label>
                      </div>
                      <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input" 
                              name="Book_Ps_t1" id="subBook_Ps4" value="4" <?php if($data_Person['FileType'] == '4'){ echo "checked"; } ?>>
                        <label class="custom-control-label">เล่ม(ปกสีฟ้า)</label>
                      </div>
                      <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input" 
                              name="Book_Ps_t1" id="subBook_Ps5" value="NULL" <?php if($data_Person['FileType'] == 'NULL'){ echo "checked"; } ?>>
                        <label class="custom-control-label">ไม่ระบุ</label>
                      </div>
                    </div> 
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-3">
                <div class="card-body">หมายเลขแฟ้มประวัติ :
                  <input class="input form-control" style="width: 100%;" placeholder=" " 
                  value="<?php echo $data_Person['SoundDex']; ?>" name="SoundDex_t1" id="SoundDex_t1">
                </div>
              </div>
              <div class="col-md-3">
                <div class="card-body">หมายเลขแฟ้มประวัติใหม่ :
                  <input class="input form-control" style="width: 100%;" name="NewSoundDex_t1" id="NewSoundDex_t1" disabled>
                </div>
              </div>
              <div class="col-md-6">         
                <div class="card-body">เลขที่ประจำตัว :
                  <input class="input form-control" style="width: 100%;" name="AirForceID_t1_s2" id="AirForceID_t1_s2" disabled
                    value="<?php echo $data_Person['AirForceID']; ?>">
                </div>
              </div>   
            </div>

            <div class="row">                
              <div class="col-md-6">
                <div class="card-block">
                  <div class="input-group col-12 datep">
                    <label class="label-control col-12 pl-0">วันที่ออกบัตร :</label>
                    <input type="text" class="form-control pickadate-disable-dates" disabled 
                      id="StartDateCard_t1" name="StartDateCard_t1" data-value="<?php //echo GetToday('');?>" />
                    <div class="input-group-append">
                      <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="card-block">
                  <div class="input-group col-12 datep">
                    <label class="label-control col-12 pl-0">วันที่บัตรหมดอายุ :</label><!--pickadate-translations-->
                    <input type="text" class="form-control pickadate-disable-dates"   placeholder="" 
                      id="EndDateCard_t1" name="EndDateCard_t1" data-value="<?php //echo GetToday('');?>" />
                    <div class="input-group-append">
                      <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <br><!-- ---- End ส่วนที่ 1------ -->

      <div class="card collapse-icon accordion-icon-rotate active">
        <div id="headingCollapse33" class="card-header bg-success">
          <a data-toggle="collapse" href="#collapse33" aria-expanded="true" aria-controls="collapse33" class="card-title lead white">
            <h6><U>ส่วนที่ 2</U>ข้อมูลบุคคล(ประวัติข้าราชการ)</h6></a>
        </div>
        <div id="collapse33" role="tabpanel" aria-labelledby="headingCollapse33" class="card-collapse collapse show" aria-expanded="true">
          <div class="card-content">
            <div class="card-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="card-body ">กำเนิดแรกบรรจุ :
                      <select class="select2 form-control" style="width: 100%;" name="BornCode_t1" id="BornCode_t1">
                        <option value="" >กรุณาเลือก กำเนิดแรกบรรจุ</option>
                        <?php
                            $sql_BornCode = "SELECT BornCode,BornName From HrtBornCode where 1=1 ";
                            $query_BornCode = sqlsrv_query($conn, $sql_BornCode );
                            $data_BornCode[] = array();
                            while($row_BornCode = sqlsrv_fetch_array($query_BornCode, SQLSRV_FETCH_ASSOC ))
                            { $data_BornCode[] = $row_BornCode ; }

                            if($data_BornCode){
                              foreach($data_BornCode as $key_BornCode => $val_BornCode){
                              ?>
                                <option value="<?php echo $val_HrtBornCode['BornCode']; ?>"
                                <?php if($val_BornCode['BornCode'] == "{$data_Person['BornCode']}"){ echo "selected"; }?>>
                                <?php echo $val_BornCode['BornName'];?></option>
                              <?php
                              }
                            }   
                        ?>
                      </select>
                    </div>
                  </div>
                </div>    
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="card-body "> กำเนิดปรับสภาพ :
                      <select class="select2 form-control" style="width: 100%;" name="ReBornCode_t1" id="ReBornCode_t1">
                      <option value="" >กรุณาเลือก กำเนิดปรับสภาพ</option>
                        <?php
                            $sql_RankBegin = "SELECT TOP(1) RankID From HrtRankHist 
                                              WHERE PersonID ='{$data_Person['PersonID']}' 
                                              ORDER BY RankYear ASC, RankID DESC";
                            $query_RankBegin = sqlsrv_query($conn, $sql_RankBegin );
                            $data_RankBegin = array();
                            while($row_RankBegin = sqlsrv_fetch_array($query_RankBegin, SQLSRV_FETCH_ASSOC ))
                            { $data_RankBegin = $row_RankBegin; }

                            $sql_RankName = "SELECT HrtRankID,HrtRankAbbrTh,HrtRankNameTh From HrtRank where 1=1 ";
                            $query_RankName = sqlsrv_query($conn, $sql_RankName );
                            $data_RankName[] = array();
                            while($row_RankName = sqlsrv_fetch_array($query_RankName, SQLSRV_FETCH_ASSOC ))
                            { $data_RankName[] = $row_RankName ; }

                            if($data_RankName){
                              foreach($data_RankName as $key_RankName => $val_RankName ){
                              ?>
                          <option value="<?php echo $val_RankName['HrtRankID']; ?>"
                            <?php if($val_RankName['HrtRankID'] == "{$data_RankBegin['RankID']}"){ echo "selected"; }?> >
                            <?php echo $val_RankName['HrtRankNameTh']; ?> </option>
                              <?php
                              }
                            }   
                        ?>
                      </select>
                    </div>
                  </div>
                </div> 
              </div>
              
              <div class="row"> 
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="card-body ">คุณวุฒิแรกบรรจุ : 
                      <select class="select2 form-control" style="width: 100%;" name="BornEduID_t1" id="BornEduID_t1" disabled>
                      <option value="" >กรุณาเลือก คุณวุฒิแรกบรรจุ</option>
                          <option value="AK">เลือก</option>
                          <option value="HI">***1***</option>
                          <option value="WA">***2****</option>
                      </select>
                    </div>
                  </div>
                </div> 
                <div class="col-lg-6 col-md-6">
                  <div class="card-block">
                    <div class="card-body "> คุณวุฒิปรับสภาพ :
                      <select class="select2 form-control" style="width: 100%;" name="ReBornEduID_t1" id="ReBornEduID_t1" disabled>
                      <option value="" >กรุณาเลือก คุณวุฒิปรับสภาพ</option>
                          <option value="AK">เลือก</option>
                          <option value="HI">พลอากาศเอก/พล.อ.อ.</option>
                          <option value="WA">พลทหาร/พลฯ</option>
                      </select>
                    </div>
                  </div>
                </div> 
              </div>            

              <div class="row"> 
                <div class="col-md-6"> 
                  <div class="card-block">
                    <div class="card-body "> เหล่า :
                      <select class="select2 form-control" style="width: 100%;" name="ArmCode_t1" id="ArmCode_t1" disabled>
                        <option value="" >กรุณาเลือก เหล่า</option>
                        <?php
                            $sql_ArmName = "SELECT HrtArmCode,HrtArmName From HrtArm where 1=1 ";
                            $query_ArmName = sqlsrv_query($conn, $sql_ArmName );
                            $data_ArmName[] = array();
                            while($row_ArmName = sqlsrv_fetch_array($query_ArmName, SQLSRV_FETCH_ASSOC ))
                            { $data_ArmName[] = $row_ArmName ; }

                            if($data_ArmName){
                              foreach($data_ArmName as $key_ArmName => $val_ArmName){
                              ?>
                                <option value="<?php echo $val_ArmName['HrtArmCode']; ?>"
                                <?php if($val_ArmName['HrtArmCode'] == "{$data_Person['ArmCode']}"){ echo "selected"; }?>>
                                <?php echo $val_ArmName['HrtArmName'];?></option>
                              <?php
                              }
                            }   
                        ?>
                      </select>
                    </div>
                  </div>
                </div> 
                <div class="col-md-6 ">
                  <div class="card-block">
                    <div class="card-body "> จำพวก :
                      <select class="select2 form-control" style="width: 100%;" name="SpcID_t1" id="SpcID_t1">
                        <option value="" >กรุณาเลือก จำพวก</option>
                        <?php
                            $sql_SpcName = "SELECT HrtSpcID,HrtSpcName From HrtSpc where 1=1 ";
                            $query_SpcName = sqlsrv_query($conn, $sql_SpcName );
                            $data_SpcName[] = array();
                            while($row_SpcName = sqlsrv_fetch_array($query_SpcName, SQLSRV_FETCH_ASSOC ))
                            { $data_SpcName[] = $row_SpcName ; }

                            if($data_SpcName){
                              foreach($data_SpcName as $key_SpcName => $val_SpcName){
                              ?>
                                <option value="<?php echo $val_SpcName['HrtSpcID']; ?>"
                                <?php if($val_SpcName['HrtSpcID'] == "{$data_Person['SpcID']}"){ echo "selected"; }?>>
                                <?php echo $val_SpcName['HrtSpcName'];?></option>
                              <?php
                              }
                            }   
                        ?>
                      </select>                
                    </div>
                  </div>
                </div> 
              </div> 

              <div class="row">                    
                <div class="col-md-3">
                  <div class="card-body">รุ่น นตท. :
                    <input class="input form-control" style="width: 100%;" disabled>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="card-body">รุ่น นตท. ซ้ำชั้น :
                    <input class="input form-control" style="width: 100%;" placeholder=" ">
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="card-body">รุ่น นนอ. :
                    <input class="input form-control" style="width: 100%;" disabled>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="card-body"><a>รุ่น นนอ. ซ้ำชั้น :</a>
                    <input class="input form-control" style="width: 100%;" placeholder=" ">
                  </div>
                </div>
              </div>             

              <div class="row">                                
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="card-body" id="Flying_student">รุ่นศิษย์การบิน :
                      <input class="input form-control" style="width: 100%;" placeholder="">
                    </div>
                  </div>
                </div>
              </div>

              <hr> <!----เส้นคัน--->

              <div class="row">        
                <div class="col-md-3">
                  <div class="card-block">
                    <div class="card-body ">เลขหมายเลขรายงาน :
                      <input class="input form-control" style="width: 100%;" disabled>
                    </div>
                  </div>
                </div>
                <div class=" col-md-3">
                  <div class="card-block">
                    <div class="card-body ">ลชทอ.หลัก :
                      <input class="input form-control" style="width: 100%;" name="SkillNo01_t1" id="SkillNo01_t1"
                        value="<?php echo $data_Person['SkillNo01']; ?>" disabled>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="card-body ">ลชทอ.รอง1 :
                    <input class="input form-control" style="width: 100%;" name="SkillNo02_t1" id="SkillNo02_t1"
                      value="<?php echo $data_Person['SkillNo02']; ?>" disabled>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="card-body ">ลชทอ.รอง2 :
                    <input class="input form-control" style="width: 100%;" name="SkillNo03_t1" id="SkillNo03_t1"
                      value="<?php echo $data_Person['SkillNo03']; ?>" disabled>
                  </div>
                </div>
              </div>

              <div class="row">      
                <div class=" col-md-6">
                  <div class="card-block">
                    <div class="card-body ">เลขที่ตำแหน่งระบบจ่ายตรงฯ (ตำแหน่งหลัก) :
                      <input class="input form-control" style="width: 100%;" name="payByForceID_t1" id="payByForceID_t1"
                        value="<?php echo $data_Person['AirForceID']; ?>" disabled>
                    </div>
                  </div>
                </div>
              </div>
 
              <div class="row">    
                <div class="col-md-12">
                  <div class="card-block">
                    <div class="card-body ">ชื่อตำแหน่ง :
                      <input class="input form-control" style="width: 100%;" name="PosOrgName_t1" id="PosOrgName_t1"
                        value="<?php echo $data_HrtPosition['HrtPosOrgName']; ?>" disabled>
                    </div>
                  </div>
                </div>
              </div>               

              <div class="row">            
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="card-body ">สังกัด :
                      <input class="input form-control" style="width: 100%;" name="OrgAbbrName_t1" id="OrgAbbrName_t1"
                        placeholder=" " value="<?php echo $data_Org['OrgAbbrName']; ?>" disabled>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="card-body ">สายวิทยาการ :
                      <input class="input form-control" style="width: 100%;" 
                        placeholder=" " value="<?php echo $data_HrtSpc['HrtSpcName']; ?>" >
                      <input type="text" name="SpcID_t1" id="SpcID_t1" style="display:none" value="<?=$data_Person['SpcID']?>">
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">               
                <div class="col-md-3">
                  <div class="card-body ">ชั้นเงินเดือน :
                    <input class="input form-control" style="width: 100%;" name="SalID_t1" id="SalID_t1"
                      value="<?php echo $data_SalDetail['SalDetail']; ?>" disabled>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="card-body ">เงินเดือน :
                    <input class="input form-control" style="width: 100%;" name="Salary_t1" id="Salary_t1"
                      value="<?php echo $data_Person['Salary']; ?>" disabled>
                  </div>
                </div>
              </div>
            
              <div class="row">    
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="input-group col-12 datep">
                      <label class="label-control col-12 pl-0">วัน/เดือน/ปี รับตำแหน่ง :</label>
                      <input type="text" class="form-control pickadate-translations"
                      id="OrgDate_t1" name="OrgDate_t1" value="<?php echo $data_Person['OrgDate'];?>"
                        data-value="<?php //echo GetToday('');?>" disabled />
                      <div class="input-group-append">
                        <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="input-group col-12 datep">
                      <label class="label-control col-12 pl-0">วัน/เดือน/ปี รับบรรจุ :</label>
                      <input type="text" class="form-control pickadate-translations"
                        id="startdate_t1" name="startdate_t1" value="<?php echo $data_Person['StartDate'];?>"
                        data-value="<?php //echo GetToday('');?>" />
                      <div class="input-group-append">
                        <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">   
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="input-group col-12 datep">
                      <label class="label-control col-12 pl-0">วัน/เดือน/ปี เป็น น.สัญญาบัตร/ประทวน :</label>
                      <input type="text" class="form-control pickadate-translations"
                        id="OfficialDate_t1" name="OfficialDate_t1" value="<?php echo $data_Person['OfficialDate'];?>"
                        data-value="<?php //echo GetToday('');?>" disabled />
                      <div class="input-group-append">
                        <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="input-group col-12 datep">
                      <label class="label-control col-12 pl-0">วัน/เดือน/ปี ปรรจุรับยศปัจจุบัน :</label>
                      <input type="text" class="form-control pickadate-translations"
                        id="RankDate_t1" name="RankDate_t1" value="<?php echo $data_Person['RankDate'];?>"
                        data-value="<?php //echo GetToday('');?>" disabled />
                      <div class="input-group-append">
                        <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">  
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="card-body ">เบิกลด :
                      <input class="input form-control" style="width: 100%;" name="SubtMPay_t1" id="SubtMPay_t1" value="<?=$data_Person['SubtMPay']?>" disabled>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="card-body ">คำนวณวันรับยศ :
                    <input class="input form-control" style="width: 100%;" placeholder=" ">
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="card-body ">จำนวนปีครองยศ :
                    <input class="input form-control" style="width: 100%;" placeholder=" ">
                  </div>
                </div>
              </div>

              <div class="row">  
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="card-body ">สถานะภาพบุคคล :
                      <?php $arr_PersonSts = array("Y" => "ใน ทอ.", "N" => "นอก ทอ."); ?>
                      <input class="input form-control" style="width: 100%;" value="<?php echo $arr_PersonSts[$data_Person['PersonSts']];?>" disabled>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="card-body ">จำนวนปีรับราชการ :
                    <input class="input form-control" style="width: 100%;" placeholder=" ">
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="card-body ">จำนวนวันลา :
                    <input class="input form-control" style="width: 100%;" placeholder=" ">
                  </div>
                </div>
              </div>

              <div class="row">  
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="card-body ">วันทวีคูณ :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">                      
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="card-body pt-1">ประเภทการสูญเสีย :
                      <?php $arr_OutType = array("01" => "เกษียณ", "02" => "ลาออก", "03" => "ปลด", "04" => "ถึงแก่กรรม", "05" => "ปรับย้ายออกนอก ทอ.", "06" => "ไล่ออก", "07" => "สูญหาย"); ?>
                      <input class="input form-control" style="width: 100%;" value="<?php echo $arr_OutType[$data_Person['OutType']];?>" disabled>
                      <input type="text" name="OutType_t1" id="OutType_t1" style="display:none" value="<?=$data_Person['OutType']?>">
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="input-group col-12 datep">
                      <label class="label-control col-12 pl-0">วัน/เดือน/ปี สูญเสีย  :</label>
                      <input type="text" class="form-control pickadate-translations"  id="OutLastDate_t1" name="OutLastDate_t1" value="<?php echo $data_Person['OutLastDate'];?>"
                        data-value="<?php //echo GetToday('');?>" disabled/>
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <span class="la la-calendar-o"></span>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <br><!-- ---- End ส่วนที่ 2------ -->

      <div class="card collapse-icon accordion-icon-rotate active">
        <div id="headingCollapse34" class="card-header bg-success">
          <a data-toggle="collapse" href="#collapse34" aria-expanded="true" aria-controls="collapse34" class="card-title lead white">
            <h6><U>ส่วนที่ 3</U>ข้อมูลบุคคล(ประวัติข้าราชการ)</h6></a>
        </div>
        <div id="collapse34" role="tabpanel" aria-labelledby="headingCollapse34" class="card-collapse collapse show" aria-expanded="true">
          <div class="card-content">
            <div class="card-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="card-body">เลขประจำตัวประชาชน :
                      <input class="input form-control" style="width: 100%;" placeholder=" " value="<?php echo $data_Person['PerCardID']; ?>"
                      name="PerCardID_t1" id="PerCardID_t1">
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="card-body ">เลขประจำตัวผู้เสียภาษี :
                      <input class="input form-control" style="width: 100%;" placeholder=" " value="<?php echo $data_Person['TaxID']; ?>"
                        name="TaxID_t1" id="TaxID_t1">
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">           
                <div class="col-md-12">
                  <div class="card-body ">สถานภาพสมรส :
                    <div class="card-content">
                      <div class="card-body">
                        <div class="d-inline-block custom-control custom-radio mr-1">
                          <input type="radio" class="custom-control-input" name="Married_Sts"
                                 id="Married_Sts_1" <?php if($data_Person['MarriedSts'] == '0'){ echo "checked"; } ?> disabled>
                         <label class="custom-control-label" for="Married_Sts_1">ไม่ระบุ</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                          <input type="radio" class="custom-control-input" name="Married_Sts"
                                 id="Married_Sts_2" <?php if($data_Person['MarriedSts'] == '1'){ echo "checked"; } ?> disabled>
                          <label class="custom-control-label" for="Married_Sts_2">โสด</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                          <input type="radio" class="custom-control-input" name="Married_Sts"
                                 id="Married_Sts_3" <?php if($data_Person['MarriedSts'] == '2'){ echo "checked"; } ?> disabled>
                          <label class="custom-control-label" for="Married_Sts_3">สมรส</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                          <input type="radio" class="custom-control-input" name="Married_Sts"
                                 id="Married_Sts_4" <?php if($data_Person['MarriedSts'] == '3'){ echo "checked"; } ?> disabled>
                          <label class="custom-control-label" for="Married_Sts_4">หย่า</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                          <input type="radio" class="custom-control-input" name="Married_Sts"
                                 id="Married_Sts_5" <?php if($data_Person['MarriedSts'] == '4'){ echo "checked"; } ?> disabled>
                          <label class="custom-control-label" for="Married_Sts_5">หม้าย</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">            
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="input-group">วัน/เดือน/ปี เกิด :
                      <input type="text" class="form-control pickadate-disable-dates" value="<?php echo $data_Person['BirthDate']; ?>"
                      aria-describedby="button-addon4" name= "BirthDate_t1" id="BirthDate_t1"> 
                      <div class="input-group-append">
                        <button class="btn btn-primary" type="button" style=" padding-bottom: 1px; padding-top: 1px;">
                        <i class="la la-calendar-o"></i></button>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="input-group">วัน/เดือน/ปี เกษียณ :
                      <input type="text" class="form-control pickadate-disable-dates" placeholder="" value="<?php echo $data_Person['RetireDate']; ?>"
                        aria-describedby="button-addon4" name="RetireDate_t1" id="RetireDate_t1">
                      <div class="input-group-append">
                        <button class="btn btn-primary" type="button" style=" padding-bottom: 1px; padding-top: 1px;">
                        <i class="la la-calendar-o"></i></button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">   
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="card-body ">เชื้อชาติ :
                      <select class="select2 form-control" style="width: 100%;" name="NationCode_s1_t1" id="NationCode_s1_t1">
                        <option value="" >   กรุณาเลือกเชื้อชาติ </option>
                        <?php
                          $sql_HrtNation = "SELECT HrtNationID,HrtNationRaceNameTh,HrtNationNameTh From HrtNation where 1=1 ";
                          $query_HrtNation = sqlsrv_query($conn, $sql_HrtNation );
                          $data_HrtNation[] = array();
                          while($row_HrtNation = sqlsrv_fetch_array($query_HrtNation, SQLSRV_FETCH_ASSOC ))
                          { $data_HrtNation[] = $row_HrtNation ; }

                          if($data_HrtNation){
                            foreach($data_HrtNation as $key_HrtNation => $val_HrtNation ){
                            ?>
                              <option value="<?php echo $val_HrtNation['HrtRankID']; ?>"
                                <?php if($val_HrtNation['HrtNationID'] == (int)$data_Person['NationCode']){ echo "selected"; }?> >
                                <?php echo $val_HrtNation['HrtNationNameTh']; ?> </option>
                            <?php
                            }
                          }   
                        ?>
                      </select>                                 
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="card-body ">สัญชาติ :
                      <select class="select2 form-control" style="width: 100%;" name="NationCode_s2_t1" id="NationCode_s2_t1">
                        <option value="" >   กรุณาเลือกสัญชาติ </option>
                        <?php
                          $sql_HrtNation = "SELECT HrtNationID,HrtNationRaceNameTh,HrtNationNameTh From HrtNation where 1=1 ";
                          $query_HrtNation = sqlsrv_query($conn, $sql_HrtNation );
                          $data_HrtNation[] = array();
                          while($row_HrtNation = sqlsrv_fetch_array($query_HrtNation, SQLSRV_FETCH_ASSOC ))
                          { $data_HrtNation[] = $row_HrtNation ; }

                          if($data_HrtNation){
                            foreach($data_HrtNation as $key_HrtNation => $val_HrtNation ){
                            ?>
                              <option value="<?php echo $val_HrtNation['HrtNationID']; ?>"
                                <?php if($val_HrtNation['HrtNationID'] == (int)$data_Person['NationCode']){ echo "selected"; }?> >
                                <?php echo $val_HrtNation['HrtNationNameTh']; ?> </option>
                            <?php
                            }
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">  
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="card-body ">ศาสนา :
                      <select class="select2 form-control" style="width: 100%;" name="ReligionID_t1" id="ReligionID_t1">
                      <option value="" >   กรุณาเลือกศาสนา </option>
                        <?php
                          $sql_HrtReligion = "SELECT HrtReligionId,HrtReligionName From HrtReligion where 1=1 ";
                          $query_HrtReligion = sqlsrv_query($conn, $sql_HrtReligion );
                          $data_HrtReligion[] = array();
                          while($row_HrtReligion = sqlsrv_fetch_array($query_HrtReligion, SQLSRV_FETCH_ASSOC ))
                          { $data_HrtReligion[] = $row_HrtReligion ; }

                          if($data_HrtReligion){
                            foreach($data_HrtReligion as $key_HrtReligion => $val_HrtReligion ){
                            ?>
                              <option value="<?php echo $val_HrtReligion['HrtReligionId']; ?>"
                                <?php if($val_HrtReligion['HrtReligionId'] == (int)$data_Person['ReligionID']){ echo "selected"; }?> >
                                <?php echo $val_HrtReligion['HrtReligionName']; ?> </option>
                            <?php
                            }
                          }
                        ?>
                      </select>                          
                    </div>
                  </div>
                </div>
                <div class="col-lg-6 col-md-12">
                  <div class="card-block">
                    <div class="card-body ">อาชีพก่อนรับราชการ :
                      <select class="select2 form-control" style="width: 100%;" name="OcctBefore_t1" id="OcctBefore_t1">
                      <option value="" >กรุณาเลือก อาชีพก่อนรับราชการ</option>K">เลือก</option>
                        <?php
                          $sql_OcctBefore = "SELECT OcctBeforeCode,OcctBeforeName From HrtOcctBefore where 1=1 ";
                          $query_OcctBefore = sqlsrv_query($conn, $sql_OcctBefore );
                          $data_OcctBefore[] = array();
                          while($row_OcctBefore = sqlsrv_fetch_array($query_OcctBefore, SQLSRV_FETCH_ASSOC ))
                          { $data_OcctBefore[] = $row_OcctBefore ; }

                          if($data_OcctBefore){
                            foreach($data_OcctBefore as $key_OcctBefore => $val_OcctBefore ){
                            ?>
                              <option value="<?php echo $val_OcctBefore['OcctBeforeCode']; ?>"
                                <?php if($val_OcctBefore['OcctBeforeCode'] == (int)$data_Person['OcctBefore']){ echo "selected"; }?> >
                                <?php echo $val_OcctBefore['OcctBeforeName']; ?> </option>
                            <?php
                            }
                          }
                        ?>
                      </select>                                  
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">  
                <div class="col-md-6">
                  <div class="card-body ">กบข./กสจ. :
                    <div class="card-content">
                      <div class="card-body">
                        <div class="d-inline-block custom-control custom-radio mr-1">
                          <input type="radio" class="custom-control-input" name="Fund_Sts"
                                 id="Fund_Sts_1" value="NULL" <?php if($data_Person['FundSts'] == "NULL"){ echo "checked"; } ?>>
                          <label class="custom-control-label">ไม่ระบุ</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                          <input type="radio" class="custom-control-input" name="Fund_Sts"
                                 id="Fund_Sts_2" value="Y" <?php if($data_Person['FundSts'] == "Y"){ echo "checked"; } ?>>
                          <label class="custom-control-label">เป็นสมาชิก</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                          <input type="radio" class="custom-control-input" name="Fund_Sts"
                                 id="Fund_Sts_3" value="N" <?php if($data_Person['FundSts'] == "N"){ echo "checked"; } ?>>
                          <label class="custom-control-label">ไม่เป็นสมาชิก</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="card-body ">ประเภทการเป็นสมาชิก :
                    <div class="card-content">
                      <div class="card-body">
                        <div class="d-inline-block custom-control custom-radio mr-1">
                          <input type="radio" class="custom-control-input" name="Fund_StsDet"
                                 id="Fund_SD_1" value="1" <?php if($data_Person['FundStsDet'] == "1"){ echo "checked"; } ?>>
                          <label class="custom-control-label">สมาชิกแบบสะสม</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                          <input type="radio" class="custom-control-input" name="Fund_StsDet"
                                 id="Fund_SD_2" value="2" <?php if($data_Person['FundStsDet'] == "2"){ echo "checked"; } ?>>
                          <label class="custom-control-label">สมาชิกแบบไม่สะสม</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>         

              <div class="row">  
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="card-body ">อีเมล :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div>
                </div>
                <div class="col-lg-6 col-md-12">
                  <div class="card-block">
                    <div class="card-body ">อีเมลสำรอง :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div>
                </div>
              </div>

              <div class="row"> 
                <div class="col-lg-6 col-md-12">
                  <div class="card-block">
                    <div class="card-body ">หมายเลขโทรศัพท์ที่ทำงาน :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div>
                 </div>
              </div>

              <!-----------------------------ตารางใหญ่กรอกข้อมูล---------------------------------> 
              <div class="row"> 
                <div class="col-lg-12 col-md-12">
                  <div class="card-block">
                    <div class="card-body ">ข้อมูลทะเบียนราษฎร์ :
                      <textarea class="form-control" id="tareaColor1" style="width: 100%;" rows="5"></textarea>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <input type="text" name="PersonID_t1" id="PersonID_t1" style="display:none" value="<?=$PersonID?>">
    </form>
  </div>

  <br><!-- ---- End ส่วนที่ 3------ -->
      
  <!-- <div class="tab-content px-1 pt-1">
    <div class="form-actions center text-center" >
      <button type="button" class="btn btn-success round btn-min-width mr-1 mb-1" id="submit_t1" name="submit_t1">
        <i class="fa fa-save"></i> &nbsp;บันทึก </button>
      <button type="button" class="btn btn-danger round btn-min-width mr-1 mb-1" id="type-error">
        <i class="fa fa-times-circle-o"></i> &nbsp;ยกเลิก</button>
    </div>
  </div> -->
  
</div>