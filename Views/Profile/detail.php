<?php //---------- header
//error_reporting(~E_NOTICE); 
    require_once "../../config.php";
    require_once '../../Model/Ducklab/func.php';

    require_once '../../Model/Ducklab/duck.class.php'; 
    require_once '../../Model/Ducklab/contents.class.php'; 
    require_once '../../Model/Ducklab/org.class.php'; 
    require_once '../../Model/Ducklab/func.php'; 
    $clsorg = new OrgClass();

    require_once '../include/header.php';
  
    $menu1 ="HRTOFFICIAL";
    $menu2 ="PROFILE"; 

    $PersonID=$_REQUEST['PersonID'];

    function DateDiff($strDate1,$strDate2)
    {
      return (strtotime($strDate2) - strtotime($strDate1)) / ( 60 * 60 * 24 );  // 1 day = 60*60*24
    }
?>
<!-- menu -->
<?php include '../include/menu.php'; ?>

  <section>

    <div class="app-content content">
      <div class="content-wrapper">

          <div class="content-header row">
            <div class="content-header-left col-12 mb-2">
              <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
              <h3 class="content-header-title">ข้อมูลบุคคล</h3>
            </div>
          </div>

<?php  //---------- ---------- ---------- ---------- Master Query ---------- ---------- ---------- ----------//

  //---------- ----- บุคคล
    $sql_HrtPerson = "SELECT * FROM HrtPerson WHERE PersonID = '$PersonID' ";
    $query_HrtPerson = sqlsrv_query($conn,$sql_HrtPerson);
    $data_Person = array();
    while($row_Person = sqlsrv_fetch_array($query_HrtPerson, SQLSRV_FETCH_ASSOC ))
    { $data_Person = $row_Person; }
    //  if( $query_HrtPerson === false ) { die( print_r( sqlsrv_errors(), true)); }
  //---------- ----- ยศ
    $sql_HrtRank = "SELECT HrtRankAbbrTh FROM HrtRank WHERE HrtRankID = '{$data_Person['RankID']}' ";
    $query_HrtRank = sqlsrv_query($conn,$sql_HrtRank); 
    $data_HrtRank = array();
    while($row_HrtRank = sqlsrv_fetch_array($query_HrtRank, SQLSRV_FETCH_ASSOC ))
    { $data_HrtRank = $row_HrtRank; }
  //---------- ----- เหล่า
    $sql_HrtArm = "SELECT HrtArmName,HrtArmAbbrName FROM HrtArm WHERE HrtArmCode = '{$data_Person['ArmCode']}' ";
    $query_HrtArm = sqlsrv_query($conn,$sql_HrtArm); 
    $data_HrtArm = array();
    while($row_HrtArm = sqlsrv_fetch_array($query_HrtArm, SQLSRV_FETCH_ASSOC ))
    { $data_HrtArm = $row_HrtArm; }
  //---------- ----- จำพวก
    if($data_Person['SpcID'] != "xxx") //---------- xxx จะปรากฎในลูกจ้างประจำ
    {
      $sql_HrtSpc = "SELECT HrtSpcName,HrtSpcAbbrName FROM HrtSpc WHERE HrtSpcID = '{$data_Person['SpcID']}' ";
      $query_HrtSpc = sqlsrv_query($conn,$sql_HrtSpc); 
      $data_HrtSpc = array();
      while($row_HrtSpc = sqlsrv_fetch_array($query_HrtSpc, SQLSRV_FETCH_ASSOC ))
      { $data_HrtSpc = $row_HrtSpc; }
    }
  //---------- ----- สังกัด
    $sql_Org = "SELECT OrgAbbrName,OrgTName FROM Org WHERE OrgID = '{$data_Person['OrgID']}' ";
    $query_Org = sqlsrv_query($conn,$sql_Org); 
    $data_Org = array();
    while($row_Org = sqlsrv_fetch_array($query_Org, SQLSRV_FETCH_ASSOC ))
    { $data_Org = $row_Org; }
  //---------- ----- ตำแหน่ง
    $sql_HrtPosition = "SELECT HrtPosOrgAbbrName,HrtPosOrgName FROM HrtPosition WHERE HrtPositionID = '{$data_Person['PosPositionID']}' ";
    $query_HrtPosition = sqlsrv_query($conn,$sql_HrtPosition); 
    $data_HrtPosition = array();
    while($row_HrtPosition = sqlsrv_fetch_array($query_HrtPosition, SQLSRV_FETCH_ASSOC ))
    { $data_HrtPosition = $row_HrtPosition; }
  //---------- ----- ขั้นเงินเดือน
    $sql_SalDetail = "SELECT SalDetail FROM HrtSal WHERE SalID = '{$data_Person['SalID']}' ";
    $query_SalDetail = sqlsrv_query($conn,$sql_SalDetail);
    $data_SalDetail = array();
    while($row_SalDetail = sqlsrv_fetch_array($query_SalDetail, SQLSRV_FETCH_ASSOC ))
    { $data_SalDetail = $row_SalDetail; }
  //---------- ----- ประเภทการสูญเสีย
    $sql_HrtPass = "SELECT HrtPassName FROM HrtPass WHERE HrtPassID = '{$data_Person['PassSts']}' ";
    $query_HrtPass = sqlsrv_query($conn,$sql_HrtPass);
    $data_HrtPass = array();
    while($row_HrtPass = sqlsrv_fetch_array($query_HrtPass, SQLSRV_FETCH_ASSOC ))
    { $data_HrtPass = $row_HrtPass; }
  //---------- ----- Array คำนำหน้าชื่อ
  $sql_HrtPrefix = "SELECT HrtPrefixID,HrtPrefixAbbrNameTH FROM HrtPrefix 
                    WHERE HrtPrefixID IN ('8','9','10','11','12','14','17','19','20','21','22','23','24','26','27','28','29','30','33','34','35','36') 
                    ORDER BY HrtPrefixID ASC";
    $query_HrtPrefix = sqlsrv_query($conn,$sql_HrtPrefix);
    $data_HrtPrefix = array();
    while($row_HrtPrefix = sqlsrv_fetch_array($query_HrtPrefix, SQLSRV_FETCH_ASSOC ))
    { $data_HrtPrefix[$row_HrtPrefix['HrtPrefixID']] = $row_HrtPrefix['HrtPrefixAbbrNameTH']; }
  //---------- ----- Array คุณวุฒิ
    $sql_Degree = "SELECT HrtDegreeCode,HrtDegreeName FROM HrtDegree WHERE 1=1 ";
    $query_Degree = sqlsrv_query($conn,$sql_Degree);
    $data_Degree = array();
    while($row_Degree = sqlsrv_fetch_array($query_Degree, SQLSRV_FETCH_ASSOC ))
    { $data_Degree[$row_Degree['HrtDegreeCode']] = $row_Degree['HrtDegreeName']; }
  //---------- ----- Array การค้นหา คุณวุฒิแรกบรรจุ และ คุณวุฒิปรับสภาพ
    $sql_EduTypeID = "SELECT EduTypeCodeID,DegreeCode FROM HrtEduHist WHERE PersonID = '{$data_Person['PersonID']}' ";
    $query_EduTypeID = sqlsrv_query($conn,$sql_EduTypeID);
    $data_EduTypeID = array();
    while($row_EduTypeID = sqlsrv_fetch_array($query_EduTypeID, SQLSRV_FETCH_ASSOC ))
    { $data_EduTypeID[$row_EduTypeID['EduTypeCodeID']] = (int)$row_EduTypeID['DegreeCode']; }

    //---------- ---------- ---------- ---------- End Query ---------- ---------- ---------- ----------//
?>

          <div class="content-body">            
            <section id="bootstrap3">
              <div class="row">
                <div class="col-12">
                  <div class="card">
                    <div class="card-content collapse show">
                      <div class="card-body card-dashboard">
                        <p class="card-text"></p>
                        <nav aria-label="breadcrumb">
                          <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../home/index.php">งานประวัติข้าราชการ</a></li>
                            <li class="breadcrumb-item active" aria-current="page">ข้อมูลประวัติข้าราชการ</li>
                          </ol>
                        </nav>
                        <div class="content-body">
                          <section class="horizontal-grid" id="horizontal-grid">
                            <div class="row">
                              <div class="col-md-12">
                                <div class="box_h1 card">
                                  <div class="card-header card-head-inverse">
                                    <h4 class="card-title text-white"><?php
                                      echo $data_HrtRank['HrtRankAbbrTh']." ".$data_Person['Name']."   ".$data_Person['SurName']."  หมายเลขประจำตัว : ".$data_Person['AirForceID'];
                                    ?></h4> 
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                      <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                                      </ul>
                                    </div>
                                  </div>

                                  <div class="card-content collapse show">
                                    <div class="card-body">
          <!---------- ---------- ---------- ---------- เริ่ม กล่องข้อมูลและรูปด้านบน ---------- ---------- ---------- ---------- -->
            <div class="row">
              <div class="col-sm-12" style="position:relative; min-height:1px; padding-right:15px;padding-left:15px;">
                <div class="well"  style="min-height:20px;padding:19px;margin-bottom:20px;background-color:#f5f5f5;border:1px solid #e3e3e3;border-radius:4px;">
                  <table>
                    <tr>
                      <td rowspan="5" width="20%"><img src="../../Asset/images/user1.png" class="img-responsive" style="width:90%" alt="Image"></td>
                      <td colspan="2" align="left"><h6>หมายเลขประจำตัว : <?php echo $data_Person['AirForceID']; ?></h6></td>
                    </tr>
                    <tr>
                      <td width="30%"><h6>ยศ : <?php echo $data_HrtRank['HrtRankAbbrTh']; ?></h6> </td>
                      <td width="50%"><h6>ชื่อ-นามสกุล : <?php echo $data_HrtPrefix[$data_Person['PrefixID']].$data_Person['PersonName']."   ".$data_Person['SurName']; ?></h6></td>
                    </tr>
                    <tr>
                      <td><h6>เหล่า : <?php echo $data_HrtArm['HrtArmAbbrName']; ?></h6></td>
                      <td><h6>จำพวก : <?php echo $data_HrtSpc['HrtSpcAbbrName']; ?></td>
                    </tr>
                    <tr>
                      <td colspan="2"><h6>สังกัด : <?php echo $data_Org['OrgAbbrName']; ?></h6></td>
                    </tr>
                    <tr>
                      <td colspan="2"><h6>ตำแหน่ง : <?php echo $data_HrtPosition['HrtPosOrgAbbrName']; ?></h6></td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          <!---------- ---------- ---------- ---------- จบ กล่องข้อมูลและรูปด้านบน ---------- ---------- ---------- ---------- -->
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </div>
                          </section>
                        </div>

  <!---------- ---------- ---------- ---------- START TAB ---------- ---------- ---------- ---------- -->

                        <div class="profile_head" style="background-color:#f5f5f5;height: auto;width: 100%;margin-left:0px;padding:1%;">
                          <!--<br>-->
                          <div class="card">
                            <div class="card-header">
                              <br>
                              <h5 class="card-title">
                                ประวัติข้าราชการ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="./print.php?PersonID=<?php echo $PersonID;?>" target="_blank">
                                  <i class="la la-print" style="color:#FF00FF;"></i> พิมพ์ประวัติย่อ
                                </a>
                             </h5>
                            </div>
                          <div class="card-content">
                            <div class="card-body">

    <?php require_once '../include/profile_table.php'; ?>

    <div class="tab-content px-0 pt-1">
      <!-- ---------- ---------- ประวัติข้าราชการ ---------- ---------- -->
        <div role="tabpanel" class="tab-pane active" id="tab_mp1" aria-expanded="true" aria-labelledby="main-profile_1">
            <!-- section SUB TAB ---------- ---------- -->
            <ul class="nav nav-tabs nav-top-border no-hover-bg">
              <li class="nav-item">
                <a class="nav-link active" id="base-tab11" data-toggle="tab" aria-controls="tab11" href="#tab11" 
                  aria-expanded="true">ข้อมูลบุคคล</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="base-tab12" data-toggle="tab" aria-controls="tab12" href="#tab12"
                  aria-expanded="false">รูปภาพ</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="base-tab13" data-toggle="tab" aria-controls="tab13" href="#tab13"
                  aria-expanded="false"><del>การเปลี่ยนชื่อ</del></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="base-tab14" data-toggle="tab" aria-controls="tab14" href="#tab14" 
                  aria-expanded="false">ขึ้นทะเบียนทหาร/ลักษณะร่างกาย</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="base-tab15" data-toggle="tab" aria-controls="tab15" href="#tab15"
                  aria-expanded="false">เหล่าทหาร</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="base-tab16" data-toggle="tab" aria-controls="tab16" href="#tab16"
                  aria-expanded="false"><del>บิดามารดา,บรรพบุรุษ</del></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="base-tab17" data-toggle="tab" aria-controls="tab17" href="#tab17" 
                  aria-expanded="false"><del>คู่สมรส</del></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="base-tab18" data-toggle="tab" aria-controls="tab18" href="#tab18"
                  aria-expanded="false"><del>บุตร-ธิดา</del></a>
              </li>  
            </ul>
            <!-- section INCLUDE PAGE ---------- ---------- -->
            <div class="tab-content px-0 pt-1">
                <div role="tabpanel" class="tab-pane active" id="tab11" aria-expanded="true" aria-labelledby="base-tab11">
                    <!-- ---------------ข้อมูลบุคคล-------------- -->
                    <?php include 'detailtab1.php'; ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="tab12" aria-labelledby="base-tab12">
                    <!-- ---------------รูปภาพ-------------- -->
                    <?php include 'detailtab2.php'; ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="tab13" aria-labelledby="base-tab13">
                    <!-- ---------------การเปลี่ยนชื่อ-------------- -->
                    <?php include 'detailtab3.php'; ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="tab14" aria-labelledby="base-tab14">
                    <!-- ---------------ขึ้นทะเบียนทหาร-------------- -->
                    <?php include 'detailtab4.php'; ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="tab15" aria-labelledby="base-tab15">                              
                    <!-- ---------------เหล่าทหาร-------------- -->
                    <?php include 'detailtab5.php'; ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="tab16" aria-labelledby="base-tab16">
                    <!-- ---------------บิดา-มารดาบรรพบุรุษ-------------- -->
                    <?php include 'detailtab6.php'; ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="tab17" aria-labelledby="base-tab17">
                    <!-- ---------------คู่สมรส-------------- -->
                    <?php include 'detailtab7.php'; ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="tab18" aria-labelledby="base-tab18">
                    <!-- ---------------บุตร-ธิดา-------------- -->
                    <?php include 'detailtab8.php'; ?>
                </div>
            </div>
        </div>
      <!-- ---------- ---------- ภูมิลำนำ ---------- ---------- -->
        <div role="tabpanel" class="tab-pane" id="tab_mp2" aria-expanded="false" aria-labelledby="main-profile_2">
            <?php include 'address.php'; ?>
        </div>
      <!-- ---------- ---------- การศึกษา/ความรู้/ความชำนาญ ---------- ---------- -->
        <div role="tabpanel" class="tab-pane" id="tab_mp3" aria-expanded="false" aria-labelledby="main-profile_3">
            <!-- section SUB TAB ---------- ---------- -->
            <ul class="nav nav-tabs nav-top-border no-hover-bg">
              <li class="nav-item">
                <a class="nav-link active" id="education-tab11" data-toggle="tab" aria-controls="edu_tab11" href="#edu_tab11" 
                  aria-expanded="true">การศึกษาก่อนเข้ารับราชการ</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="education-tab12" data-toggle="tab" aria-controls="edu_tab12" href="#edu_tab12"
                  aria-expanded="false">การศึกษาระหว่างรับราชการ</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="education-tab13" data-toggle="tab" aria-controls="edu_tab13" href="#edu_tab13"
                  aria-expanded="false"><del>ความรู้พิเศษ</del></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="education-tab14" data-toggle="tab" aria-controls="edu_tab14" href="#edu_tab14" 
                  aria-expanded="false"><del>เลขหมายความชำนาญทหารอากาศ</del></a>
              </li>
            </ul>
            <!-- section INCLUDE PAGE ---------- ---------- -->
            <div class="tab-content px-0 pt-1">
                <div role="tabpanel" class="tab-pane active" id="edu_tab11" aria-labelledby="education-tab11">
                    <!-- ---------------การศึกษาก่อนเข้ารับราชการ-------------- -->
                    <?php include 'education_tab1.php'; ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="edu_tab12" aria-labelledby="education-tab12">
                    <!-- ---------------การศึกษาระหว่างรับราชการ-------------- -->
                    <?php include 'education_tab2.php'; ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="edu_tab13" aria-labelledby="education-tab13">
                    <!-- ---------------ความรู้พิเศษ-------------- -->
                    <?php include 'education_tab3.php'; ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="edu_tab14" aria-labelledby="education-tab14">
                    <!-- ---------------เลขหมายความชำนาญทหารอากาศ-------------- -->
                    <?php include 'education_tab4.php'; ?>
                </div>
            </div>
        </div>
      <!-- ---------- ---------- ตำแหน่ง/เครื่องราชอิสริยาภรณ์ ---------- ---------- -->
        <div role="tabpanel" class="tab-pane" id="tab_mp4" aria-expanded="false" aria-labelledby="main-profile_4">
            <!-- section SUB TAB ---------- ---------- -->
            <ul class="nav nav-tabs nav-top-border no-hover-bg">
              <li class="nav-item">
                <a class="nav-link active" id="position-tab11" data-toggle="tab" aria-controls="posi_tab11" href="#posi_tab11" 
                  aria-expanded="true">การเลื่อนยศ</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="position-tab12" data-toggle="tab" aria-controls="posi_tab12" href="#posi_tab12"
                  aria-expanded="false">การดำรงตำแหน่ง</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="position-tab13" data-toggle="tab" aria-controls="posi_tab13" href="#posi_tab13"
                  aria-expanded="false"><del>รายการ/ตำแหน่งพิเศษ</del></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="position-tab14" data-toggle="tab" aria-controls="posi_tab14" href="#posi_tab14" 
                  aria-expanded="false">เครื่องราชอิสริยาภรณ์</a>
              </li>
            </ul>
            <!-- section INCLUDE PAGE ---------- ---------- -->
            <div class="tab-content px-0 pt-1">
                <div role="tabpanel" class="tab-pane active" id="posi_tab11" aria-labelledby="position-tab11">
                    <!-- ---------------การเลื่อนยศ-------------- -->
                    <?php include 'position_tab1.php'; ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="posi_tab12" aria-labelledby="position-tab12">
                    <!-- ---------------การดำรงตำแหน่ง-------------- -->
                    <?php include 'position_tab2.php'; ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="posi_tab13" aria-labelledby="position-tab13">
                    <!-- ---------------รายการ/ตำแหน่งพิเศษ-------------- -->
                    <?php include 'position_tab3.php'; ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="posi_tab14" aria-labelledby="position-tab14">
                    <!-- ---------------เครื่องราชอิสริยาภรณ์-------------- -->
                    <?php include 'position_tab4.php'; ?>
                </div>
            </div>
        </div>
      <!-- ---------- ---------- ค่าตอบแทนเงินเดือน ---------- ---------- -->
        <div role="tabpanel" class="tab-pane" id="tab_mp5" aria-expanded="false" aria-labelledby="main-profile_5">
            <!-- section SUB TAB ---------- ---------- -->
            <ul class="nav nav-tabs nav-top-border no-hover-bg">
              <li class="nav-item">
                <a class="nav-link active" id="salary-tab11" data-toggle="tab" aria-controls="sal_tab11" href="#sal_tab11" 
                  aria-expanded="true">ค่าตอบแทน</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="salary-tab12" data-toggle="tab" aria-controls="sal_tab12" href="#sal_tab12"
                  aria-expanded="false"><del>ทวีคูณ</del></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="salary-tab13" data-toggle="tab" aria-controls="sal_tab13" href="#sal_tab13"
                  aria-expanded="false"><del>ลาอุปสมบท/ประกอบพิธ๊ฮัจย์</del></a>
              </li>
            </ul>
            <!-- section INCLUDE PAGE ---------- ---------- -->
            <div class="tab-content px-0 pt-1">
                <div role="tabpanel" class="tab-pane active" id="sal_tab11" aria-labelledby="salary-tab11">
                    <!-- ---------------ค่าตอบแทน-------------- -->
                    <?php include 'salary_tab1.php'; ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="sal_tab12" aria-labelledby="salary-tab12">
                    <!-- ---------------ทวีคูณ-------------- -->
                    <?php include 'salary_tab2.php'; ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="sal_tab13" aria-labelledby="salary-tab13">
                    <!-- ---------------ลาอุปสมบท/ประกอบพิธ๊ฮัจย์-------------- -->
                    <?php include 'salary_tab3.php'; ?>
                </div>
            </div>
        </div>
      <!-- ---------- ---------- ความประพฤติ ---------- ---------- -->
        <div role="tabpanel" class="tab-pane" id="tab_mp6" aria-expanded="false" aria-labelledby="main-profile_6">
            <!-- section SUB TAB ---------- ---------- -->
            <ul class="nav nav-tabs nav-top-border no-hover-bg">
              <li class="nav-item">
                <a class="nav-link active" id="conduct-tab11" data-toggle="tab" aria-controls="con_tab11" href="#con_tab11" 
                  aria-expanded="true">ความดี/ความชอบ/ความสามารถพิเศษ</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="conduct-tab12" data-toggle="tab" aria-controls="con_tab12" href="#con_tab12"
                  aria-expanded="false">ความผิด</a>
              </li>
            </ul>
            <!-- section INCLUDE PAGE ---------- ---------- -->
            <div class="tab-content px-0 pt-1">
                <div role="tabpanel" class="tab-pane active" id="con_tab11" aria-labelledby="salary-tab11">
                    <!-- ---------------ความดี/ความชอบ/ความสามารถพิเศษ-------------- -->
                    <?php include 'conduct_tab1.php'; ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="con_tab12" aria-labelledby="salary-tab12">
                    <!-- ---------------ความผิด-------------- -->
                    <?php include 'conduct_tab2.php'; ?>
                </div>
            </div>
        </div>
      <!-- ---------- ---------- สูญเสีย ---------- ---------- -->
        <div role="tabpanel" class="tab-pane" id="tab_mp7" aria-expanded="false" aria-labelledby="main-profile_7">
            <?php include 'lose.php'; ?>
        </div>
      <!-- ---------- ---------- ECL ---------- ---------- -->
      <div role="tabpanel" class="tab-pane" id="tab_mp8" aria-expanded="false" aria-labelledby="main-profile_8">
            <?php include 'ecl_tab.php'; ?>
        </div>
      <!-- ---------- ---------- งานประเมิน ---------- ---------- -->
      <div role="tabpanel" class="tab-pane" id="tab_mp9" aria-expanded="false" aria-labelledby="main-profile_9">
            <?php //include 'FileName.php'; ?>
        </div>
      <!-- ---------- ---------- ประวัติรายงาน 904 ---------- ---------- -->
      <div role="tabpanel" class="tab-pane" id="tab_mp10" aria-expanded="false" aria-labelledby="main-profile_10">
            <?php //include 'FileName.php'; ?>
        </div>
    </div>

                            </div>
                          </div>

  <!---------- ---------- ---------- ---------- START TAB ---------- ---------- ---------- ---------- -->

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>

      </div>
    </div>

  </section>
  <a class="nav-link nav-menu-main menu-toggle hidden-xs" href="" id="auto_panel"></a>

<!-- footer -->
<?php include '../include/footer.php'; ?>
<!-- <script src="../../Controllers/profileController.js"></script> -->

<script type="text/javascript">
    $(document).ready(function() {
        $('#auto_panel').trigger('click');
        console.log("ready");
        change_autorefreshdiv();
    });

    function change_autorefreshdiv() {
        // $('#prefixPage').addClass('active');
    }

    function toggle(source) {
      var checkboxes = document.querySelectorAll('.checkAll');
      for (var i = 0; i < checkboxes.length; i++) {
          if (checkboxes[i] != source)
              checkboxes[i].checked = source.checked;
      }
    }

    function editSubmit()
    {
      if(confirm('ท่านแน่ใจว่า ต้องการแก้ไขข้อมูลในส่วนนี้')==true)
      {
        return true;
      }
      else
      {
        return false;
      }
    }
</script>


