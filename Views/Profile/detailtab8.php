<div class="tab-pane" id="tab18" aria-labelledby="base-tab18">
  <a href="#" class="btn btn-social btn-min-width mb-1" style="background-color:#0f1733; color:white;">
    <span class="la la-plus-circle" style="color:white; font-weight: bold;font-size: 18px"></span>เพิ่ม</a>
  <a href="#" class="btn btn-social btn-min-width mb-1" style="background-color:#0f1733; color:white;">
    <span class="la la-trash-o" style="color:white; font-weight: bold;font-size: 18px"></span>ลบ</a>

  <section>
    <table class="table table-striped table-borderless table-hover bootstrap-3 ">
      <thead>
        <tr align="center" style="background-color:#0f1733; color:whitesmoke;">
          <th><input type="checkbox" class="checkAll" onclick="toggle(this);" /></th>
          <th></th>
          <th>ลำดับที่</th>
          <th>ยศ</th>
          <th>คำนำหน้าชื่อ</th>
          <th>ชื่อ</th>
          <th>นามสกุล</th>
          <th>วันเดือนปีเกิด</th>
        </tr>
      </thead>
      <tbody align="center">
        <tr>
          <td><input type="checkbox" class="checkAll" /></td>
          <td>
            <a href="#" data-toggle="modal" data-target="#from_child"><i class="la la-pencil-square-o" style="color:#0f1733;"></i></a>
            <a href="#"><i class="la la-trash-o" style="color:#0f1733;"></i></a>
          </td>
          <td>1</td>
          <td>พล.อ.พ.หญิง</td>
          <td>นางสาว</td>
          <td>พิรุฬห์ลักษณ์ โภคาสุข</td>
          <td>โภคาสุข</td>
          <td>11 พ.ค. 2562</td>
        </tr>
        <tr>
          <td><input type="checkbox" class="checkAll" /></td>
          <td>
            <a href="#" data-toggle="modal" data-target="#from_child"><i class="la la-pencil-square-o" style="color:#0f1733;"></i></a>
            <a href="#"><i class="la la-trash-o" style="color:#0f1733;"></i></a>
          </td>
          <td>2</td>
          <td>พล.อ.พ.หญิง</td>
          <td>นางสาว</td>
          <td>พิรุฬห์ลักษณ์ โภคาสุข</td>
          <td>โภคาสุข</td>
          <td>11 พ.ค. 2562</td>
        </tr>
      </tbody>
    </table>
  </a>

  <div class="modal animated slideInUp text-left modal_custom1" id="from_child" tabindex="-1" role="dialog" aria-labelledby="modalSettingRegis"  aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <!---------- ---------- Start Content ---------- ---------- -->
          <div class="card-body">
              <div class="model-header" style="background-color:#0f1733;">
              <!-- -------------------- -->
                <div class="row">
                  <div class="col-md-11">
                    <h6 class="model-title text-white px-2 pt-2 py-1">บันทึก/แก้ไข ข้อมูลบุตร-ธิดา :  
                      <?php echo $data_HrtRank['HrtRankAbbrTh']." ".$data_Person['PersonName']."   ".$data_Person['SurName']."  หมายเลขประจำตัว : ".$data_Person['AirForceID']; ?></h6>
                  </div>
                  <div class="col-md-1">
                    <h4 class="model-title text-white pt-2"><a data-dismiss="modal"><i class="fa fa-times-circle-o"></i></a></h4>
                  </div>
                </div>
              <!-- -------------------- -->
              </div>
              <div class="model-body">
              <!-- -------------------- -->
                  <div class="card collapse-icon accordion-icon-rotate active px-1 pt-1">
                    <div class="card-header bg-success">
                      <a data-toggle="collapse" href="#motab1_s8_h1" aria-expanded="true" aria-controls="motab1_s8_h1" class="card-title lead white">
                        <h6><U>ส่วนที่ 1</U> ข้อมูลบุตร-ธิดา</h6></a>
                    </div>
                    <div id="motab1_s8_h1" role="tabpanel" class="card-collapse collapse show" aria-expanded="true">
                      <div class="card-content">

                        <div class="row"> 
                          <div class="col-md-6 ">
                            <div class="card-body" id="NTT">ลำดับบุตร-ธิดา :
                              <input class="input form-control" style="width: 100%;" placeholder=" ">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="card-body ">เพศ :
                              <div class="card-content">
                                <div class="card-body">
                                  <div class="d-inline-block custom-control custom-radio mr-1">
                                    <input type="radio" class="custom-control-input" name="type_sex_child" id="type_sex_ch1">
                                    <label class="custom-control-label">ชาย</label>
                                  </div>
                                  <div class="d-inline-block custom-control custom-radio mr-1">
                                    <input type="radio" class="custom-control-input" name="type_sex_child" id="type_sex_ch2">
                                    <label class="custom-control-label">หญิง</label>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div> 

                        <div class="row"> 
                          <div class="col-md-6"> 
                            <div class="card-block">
                              <div class="card-body "> หมายเลขประจำตัวราชการ :
                                <input class="input form-control" style="width: 100%;" placeholder=" ">
                              </div>
                            </div>
                          </div> 
                          <div class="col-md-6"> 
                            <div class="card-block">
                              <div class="card-body "> ยศ :
                                <select class="select2 form-control" style="width: 100%;">
                                  <option></option>
                                </select>
                              </div>
                            </div>
                          </div> 
                        </div> 

                        <div class="row"> 
                          <div class="col-md-6"> 
                            <div class="card-block">
                              <div class="card-body "> เลขประจำตัวประชาชน :
                                <input class="input form-control" style="width: 100%;" placeholder=" ">
                              </div>
                            </div>
                          </div> 
                          <div class="col-md-6"> 
                            <div class="card-block">
                              <div class="card-body "> คำนำหน้าชื่อ :
                                <select class="select2 form-control" style="width: 100%;">
                                  <option></option>
                                </select>
                              </div>
                            </div>
                          </div> 
                        </div> 

                        <div class="row"> 
                          <div class="col-md-6"> 
                            <div class="card-block">
                              <div class="card-body "> ชื่อ :
                                <input class="input form-control" style="width: 100%;" placeholder=" ">
                              </div>
                            </div>
                          </div> 
                          <div class="col-md-6"> 
                            <div class="card-block">
                              <div class="card-body "> นามสกุล :
                                <input class="input form-control" style="width: 100%;" placeholder=" ">
                              </div>
                            </div>
                          </div> 
                        </div> 

                        <hr> <!----เส้นคัน--->

                        <div class="row">           
                          <div class="col-md-12">
                            <div class="card-body ">สถานภาพบุตร-ธิดา :
                              <div class="card-content">
                                <div class="card-body">
                                  <div class="d-inline-block custom-control custom-radio mr-1">
                                    <input type="radio" class="custom-control-input" name="Child_Status" id="Child_Status_1">
                                    <label class="custom-control-label">บุตร</label>
                                  </div>
                                  <div class="d-inline-block custom-control custom-radio mr-1">
                                    <input type="radio" class="custom-control-input" name="Child_Status" id="Child_Status_2">
                                    <label class="custom-control-label">รับรองบุตร</label>
                                  </div>
                                  <div class="d-inline-block custom-control custom-radio mr-1">
                                    <input type="radio" class="custom-control-input" name="Child_Status" id="Child_Status_3">
                                    <label class="custom-control-label">บุตรบุญธรรม</label>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="row"> 
                          <div class="col-md-6"> 
                            <div class="card-block">
                              <div class="card-body "> หลักฐาน การเกิด/รับรองบุตร/บุตรบุญธรรม :
                                <input class="input form-control" style="width: 100%;" placeholder=" ">
                              </div>
                            </div>
                          </div> 
                          <div class="col-md-6"> 
                            <div class="card-block">
                              <div class="card-body "> เลขที่ :
                                <input class="input form-control" style="width: 100%;" placeholder=" ">
                              </div>
                            </div>
                          </div> 
                        </div> 

                        <div class="row"> 
                          <div class="col-md-6">
                            <div class="card-block">
                              <div class="input-group col-12 datep">
                                <label class="label-control col-12 pl-0">ลงวันที่ :</label>
                                <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                                <div class="input-group-append">
                                  <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-3 pt-1"> 
                            <div class="card-block">
                              <div class="card-body "> จังหวัด :
                                <select class="select2 form-control" style="width: 100%;">
                                  <option></option>
                                </select>
                              </div>
                            </div>
                          </div> 
                          <div class="col-md-3 pt-1"> 
                            <div class="card-block">
                              <div class="card-body "> อำเภอ :
                                <select class="select2 form-control" style="width: 100%;">
                                  <option></option>
                                </select>
                              </div>
                            </div>
                          </div> 
                        </div> 

                        <div class="row"> 
                          <div class="col-md-6">
                            <div class="card-block">
                              <div class="input-group col-12 datep">
                                <label class="label-control col-12 pl-0">วัน/เดือน/ปีเกิด  :</label>
                                <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                                <div class="input-group-append">
                                  <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6 pt-2">
                            <div class="card-body" id="NTT">แนบไฟล์หลักฐานการสมรส :
                              <input type="file" name="filUpload[]" id="filUpload" multiple="multiple" >
                            </div>
                          </div>
                        </div> 

                        <div class="row">           
                          <div class="col-md-12">
                            <div class="card-body ">สถานภาพ :
                              <div class="card-content">
                                <div class="card-body">
                                  <div class="d-inline-block custom-control custom-radio mr-1">
                                    <input type="radio" class="custom-control-input" name="Child_Status_lift" id="Child_Status_l1">
                                    <label class="custom-control-label">ปกติ</label>
                                  </div>
                                  <div class="d-inline-block custom-control custom-radio mr-1">
                                    <input type="radio" class="custom-control-input" name="Child_Status_lift" id="Child_Status_l2">
                                    <label class="custom-control-label">สูญเสีย</label>
                                  </div>
                                  <div class="d-inline-block custom-control custom-radio mr-1">
                                    <input type="radio" class="custom-control-input" name="Child_Status_lift" id="Child_Status_l3">
                                    <label class="custom-control-label">ยกเลิก(บุตรบุญธรรม)</label>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>
                  <!-- ---------- -->
                  <div class="card collapse-icon accordion-icon-rotate active px-1 pt-1">
                    <div class="card-header bg-success">
                      <a data-toggle="collapse" href="#motab1_s8_h2" aria-expanded="true" aria-controls="motab1_s8_h2" class="card-title lead white">
                        <h6><U>ส่วนที่ 2</U> ข้อมูลบิดา</h6></a>
                    </div>
                    <div id="motab1_s8_h2" role="tabpanel" class="card-collapse collapse show" aria-expanded="true">
                      <div class="card-content">

                        <div class="row"> 
                          <div class="col-md-6"> 
                            <div class="card-block">
                              <div class="card-body "> หมายเลขประจำตัวราชการ :
                                <input class="input form-control" style="width: 100%;" placeholder=" ">
                              </div>
                            </div>
                          </div> 
                          <div class="col-md-6"> 
                            <div class="card-block">
                              <div class="card-body "> ยศ :
                                <select class="select2 form-control" style="width: 100%;">
                                  <option></option>
                                </select>
                              </div>
                            </div>
                          </div> 
                        </div> 
                        <div class="row"> 
                          <div class="col-md-6"> 
                            <div class="card-block">
                              <div class="card-body "> เลขประจำตัวประชาชน :
                                <input class="input form-control" style="width: 100%;" placeholder=" ">
                              </div>
                            </div>
                          </div> 
                          <div class="col-md-6"> 
                            <div class="card-block">
                              <div class="card-body "> คำนำหน้าชื่อ :
                                <select class="select2 form-control" style="width: 100%;">
                                  <option></option>
                                </select>
                              </div>
                            </div>
                          </div> 
                        </div> 
                        <div class="row"> 
                          <div class="col-md-6"> 
                            <div class="card-block">
                              <div class="card-body "> ชื่อ :
                                <input class="input form-control" style="width: 100%;" placeholder=" ">
                              </div>
                            </div>
                          </div> 
                          <div class="col-md-6"> 
                            <div class="card-block">
                              <div class="card-body "> นามสกุล :
                                <input class="input form-control" style="width: 100%;" placeholder=" ">
                              </div>
                            </div>
                          </div> 
                        </div> 
                        <div class="row">   
                          <div class="col-md-6">
                            <div class="card-block">
                              <div class="card-body ">เชื้อชาติ :
                                <select class="select2 form-control" style="width: 100%;">
                                  <option value="" >   กรุณาเลือกเชื้อชาติ </option>
                                  <?php
                                    $sql_HrtNation = "SELECT HrtNationID,HrtNationNameTh From HrtNation where 1=1 ";
                                    $query_HrtNation = sqlsrv_query($conn, $sql_HrtNation );
                                    $data_HrtNation[] = array();
                                    while($row_HrtNation = sqlsrv_fetch_array($query_HrtNation, SQLSRV_FETCH_ASSOC ))
                                    { $data_HrtNation[] = $row_HrtNation ; }

                                    if($data_HrtNation){
                                      foreach($data_HrtNation as $key_HrtNation => $val_HrtNation ){
                                      ?>
                                        <option value="<?php echo $val_HrtNation['HrtRankID']; ?>"
                                          <?php if($val_HrtNation['HrtNationID'] == (int)$data_Person['NationCode']){ echo "selected"; }?> >
                                          <?php echo $val_HrtNation['HrtNationNameTh']; ?> </option>
                                      <?php
                                      }
                                    }   
                                  ?>
                                </select>                                 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="card-block">
                              <div class="card-body ">สัญชาติ :
                                <select class="select2 form-control" style="width: 100%;">
                                  <option value="" >   กรุณาเลือกสัญชาติ </option>
                                  <?php
                                    $sql_HrtNation = "SELECT HrtNationID,HrtNationNameTh From HrtNation where 1=1 ";
                                    $query_HrtNation = sqlsrv_query($conn, $sql_HrtNation );
                                    $data_HrtNation[] = array();
                                    while($row_HrtNation = sqlsrv_fetch_array($query_HrtNation, SQLSRV_FETCH_ASSOC ))
                                    { $data_HrtNation[] = $row_HrtNation ; }

                                    if($data_HrtNation){
                                      foreach($data_HrtNation as $key_HrtNation => $val_HrtNation ){
                                      ?>
                                        <option value="<?php echo $val_HrtNation['HrtNationID']; ?>"
                                          <?php if($val_HrtNation['HrtNationID'] == (int)$data_Person['NationCode']){ echo "selected"; }?> >
                                          <?php echo $val_HrtNation['HrtNationNameTh']; ?> </option>
                                      <?php
                                      }
                                    }
                                  ?>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">  
                          <div class="col-md-6">
                            <div class="card-block">
                              <div class="card-body ">ศาสนา :
                                <select class="select2 form-control" style="width: 100%;">
                                <option value="" >   กรุณาเลือกศาสนา </option>
                                  <?php
                                    $sql_HrtReligion = "SELECT HrtReligionId,HrtReligionName From HrtReligion where 1=1 ";
                                    $query_HrtReligion = sqlsrv_query($conn, $sql_HrtReligion );
                                    $data_HrtReligion[] = array();
                                    while($row_HrtReligion = sqlsrv_fetch_array($query_HrtReligion, SQLSRV_FETCH_ASSOC ))
                                    { $data_HrtReligion[] = $row_HrtReligion ; }

                                    if($data_HrtReligion){
                                      foreach($data_HrtReligion as $key_HrtReligion => $val_HrtReligion ){
                                      ?>
                                        <option value="<?php echo $val_HrtReligion['HrtReligionId']; ?>"
                                          <?php if($val_HrtReligion['HrtReligionId'] == (int)$data_Person['ReligionID']){ echo "selected"; }?> >
                                          <?php echo $val_HrtReligion['HrtReligionName']; ?> </option>
                                      <?php
                                      }
                                    }
                                  ?>
                                </select>                          
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-6"></div>
                        </div>

                      </div>
                    </div>
                  </div>
                  <!-- ---------- -->
                  <div class="card collapse-icon accordion-icon-rotate active px-1 pt-1">
                    <div class="card-header bg-success">
                      <a data-toggle="collapse" href="#motab1_s8_h3" aria-expanded="true" aria-controls="motab1_s8_h3" class="card-title lead white">
                        <h6><U>ส่วนที่ 3</U> ข้อมูลมารดา</h6></a>
                    </div>
                    <div id="motab1_s8_h3" role="tabpanel" class="card-collapse collapse show" aria-expanded="true">
                      <div class="card-content">

                        <div class="row"> 
                          <div class="col-md-6"> 
                            <div class="card-block">
                              <div class="card-body "> หมายเลขประจำตัวราชการ :
                                <input class="input form-control" style="width: 100%;" placeholder=" ">
                              </div>
                            </div>
                          </div> 
                          <div class="col-md-6"> 
                            <div class="card-block">
                              <div class="card-body "> ยศ :
                                <select class="select2 form-control" style="width: 100%;">
                                  <option></option>
                                </select>
                              </div>
                            </div>
                          </div> 
                        </div> 
                        <div class="row"> 
                          <div class="col-md-6"> 
                            <div class="card-block">
                              <div class="card-body "> เลขประจำตัวประชาชน :
                                <input class="input form-control" style="width: 100%;" placeholder=" ">
                              </div>
                            </div>
                          </div> 
                          <div class="col-md-6"> 
                            <div class="card-block">
                              <div class="card-body "> คำนำหน้าชื่อ :
                                <select class="select2 form-control" style="width: 100%;">
                                  <option></option>
                                </select>
                              </div>
                            </div>
                          </div> 
                        </div> 
                        <div class="row"> 
                          <div class="col-md-6"> 
                            <div class="card-block">
                              <div class="card-body "> ชื่อ :
                                <input class="input form-control" style="width: 100%;" placeholder=" ">
                              </div>
                            </div>
                          </div> 
                          <div class="col-md-6"> 
                            <div class="card-block">
                              <div class="card-body "> นามสกุล :
                                <input class="input form-control" style="width: 100%;" placeholder=" ">
                              </div>
                            </div>
                          </div> 
                        </div> 
                        <div class="row">   
                          <div class="col-md-6">
                            <div class="card-block">
                              <div class="card-body ">เชื้อชาติ :
                                <select class="select2 form-control" style="width: 100%;">
                                  <option value="" >   กรุณาเลือกเชื้อชาติ </option>
                                  <?php
                                    $sql_HrtNation = "SELECT HrtNationID,HrtNationNameTh From HrtNation where 1=1 ";
                                    $query_HrtNation = sqlsrv_query($conn, $sql_HrtNation );
                                    $data_HrtNation[] = array();
                                    while($row_HrtNation = sqlsrv_fetch_array($query_HrtNation, SQLSRV_FETCH_ASSOC ))
                                    { $data_HrtNation[] = $row_HrtNation ; }

                                    if($data_HrtNation){
                                      foreach($data_HrtNation as $key_HrtNation => $val_HrtNation ){
                                      ?>
                                        <option value="<?php echo $val_HrtNation['HrtRankID']; ?>"
                                          <?php if($val_HrtNation['HrtNationID'] == (int)$data_Person['NationCode']){ echo "selected"; }?> >
                                          <?php echo $val_HrtNation['HrtNationNameTh']; ?> </option>
                                      <?php
                                      }
                                    }   
                                  ?>
                                </select>                                 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="card-block">
                              <div class="card-body ">สัญชาติ :
                                <select class="select2 form-control" style="width: 100%;">
                                  <option value="" >   กรุณาเลือกสัญชาติ </option>
                                  <?php
                                    $sql_HrtNation = "SELECT HrtNationID,HrtNationNameTh From HrtNation where 1=1 ";
                                    $query_HrtNation = sqlsrv_query($conn, $sql_HrtNation );
                                    $data_HrtNation[] = array();
                                    while($row_HrtNation = sqlsrv_fetch_array($query_HrtNation, SQLSRV_FETCH_ASSOC ))
                                    { $data_HrtNation[] = $row_HrtNation ; }

                                    if($data_HrtNation){
                                      foreach($data_HrtNation as $key_HrtNation => $val_HrtNation ){
                                      ?>
                                        <option value="<?php echo $val_HrtNation['HrtNationID']; ?>"
                                          <?php if($val_HrtNation['HrtNationID'] == (int)$data_Person['NationCode']){ echo "selected"; }?> >
                                          <?php echo $val_HrtNation['HrtNationNameTh']; ?> </option>
                                      <?php
                                      }
                                    }
                                  ?>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">  
                          <div class="col-md-6">
                            <div class="card-block">
                              <div class="card-body ">ศาสนา :
                                <select class="select2 form-control" style="width: 100%;">
                                <option value="" >   กรุณาเลือกศาสนา </option>
                                  <?php
                                    $sql_HrtReligion = "SELECT HrtReligionId,HrtReligionName From HrtReligion where 1=1 ";
                                    $query_HrtReligion = sqlsrv_query($conn, $sql_HrtReligion );
                                    $data_HrtReligion[] = array();
                                    while($row_HrtReligion = sqlsrv_fetch_array($query_HrtReligion, SQLSRV_FETCH_ASSOC ))
                                    { $data_HrtReligion[] = $row_HrtReligion ; }

                                    if($data_HrtReligion){
                                      foreach($data_HrtReligion as $key_HrtReligion => $val_HrtReligion ){
                                      ?>
                                        <option value="<?php echo $val_HrtReligion['HrtReligionId']; ?>"
                                          <?php if($val_HrtReligion['HrtReligionId'] == (int)$data_Person['ReligionID']){ echo "selected"; }?> >
                                          <?php echo $val_HrtReligion['HrtReligionName']; ?> </option>
                                      <?php
                                      }
                                    }
                                  ?>
                                </select>                          
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-6"></div>
                        </div>

                      </div>
                    </div>
                  </div>
              <!-- -------------------- -->
              </div>
            <br><!-- ---------- เว้นระยะห่าง ---------- -->
              <div class="tab-content px-1 pt-1">
                <div class="form-actions center" align="center">
                  <button type="button" class="btn btn-success round btn-min-width mr-1 mb-1" id="submit" name="submit" data-target="#modalConfirm" onclick="insertOrganizationGroupType()">
                    <i class="fa fa-save"></i>&nbsp;บันทึก</button>
                  <button type="button" class="btn btn-danger round btn-min-width mr-1 mb-1" id="type-error" data-dismiss="modal">
                    <i class="fa fa-times-circle-o"></i>&nbsp;ยกเลิก</button>
                </div>
              </div>
          </div>
        <!---------- ---------- End ---------- ---------- -->
      </div> 
    </div> 
  </div>

</div>