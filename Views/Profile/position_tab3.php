<div class="tab-pane" id="posi_tab13" aria-labelledby="position-tab13">
  <div class="row">
    <div class="col-12">
      <div class="float-left">
        <a href="#" class="btn btn1 mb-1 round"><span class="fa fa-plus-circle"></span> เพิ่ม</a>
        <a href="#" class="btn btn1 mb-1 round"><span class="fa fa-trash-o"></span> ลบ</a>
      </div>
    </div>
  </div> 
  <table class="table table-striped table-borderless table-hover bootstrap-3 ">
    <thead>
      <tr align="center" style="background-color:#0f1733; color:whitesmoke;">
        <th><input type="checkbox" class="checkAll" onclick="toggle(this);" /></th>
        <th></th>
        <th>ลำดับที่</th>
        <th>ตำแหน่ง</th>
        <th>ประเภทคำสั่ง</th>
        <th>สภาพตำแหน่ง</th>
        <th>วันที่มีผล</th>
      </tr>
    </thead>
    <tbody align="center">
      <tr>
        <td><input type="checkbox" class="checkAll" /></td>
        <td>
          <a href="#" data-toggle="modal" data-target="#from_posi3"><i class="la la-pencil-square-o" style="color:#0f1733;"></i></a>
          <a href="#"><i class="la la-trash-o" style="color:#0f1733;"></i></a>
        </td>
        <td>1</td>
        <td>ราชองครักษ์พิเศษ</td>
        <td>ให้ดำรงตำแหน่ง</td>
        <td>ตำแหน่งพิเศษ</td>
        <td>15 ม.ค.2563</td>
      </tr>
    </tbody>
  </table>
  <div class="modal animated slideInUp text-left modal_custom1" id="from_posi3" tabindex="-1" role="dialog" aria-labelledby="modalSettingRegis"  aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <!---------- ---------- Start Content ---------- ---------- -->
          <div class="card-body">
              <div class="model-header" style="background-color:#0f1733;">
              <!-- -------------------- -->
                <div class="row">
                  <div class="col-md-11">
                    <h6 class="model-title text-white px-2 pt-2 py-1">บันทึก/แก้ไข ราชการ/ตำแหน่งพิเศษ 
                      <?php echo $data_HrtRank['HrtRankAbbrTh']." ".$data_Person['PersonName']."   ".$data_Person['SurName']."  หมายเลขประจำตัว : ".$data_Person['AirForceID']; ?></h6>
                  </div>
                  <div class="col-md-1">
                    <h4 class="model-title text-white pt-2"><a data-dismiss="modal"><i class="fa fa-times-circle-o"></i></a></h4>
                  </div>
                </div>
              <!-- -------------------- -->
              </div>
    <div class="model-body">
              <!-- -------------------- -->
            <div class="row"> 
                <div class="col-md-3"> 
                    <div class="card-block">
                        <div class="card-body ">ประเภทกำลังพล :
                        <select class="select2 form-control" style="width: 100%;">
                            <option></option>
                        </select>
                        </div>
                    </div>
                    </div>
                <div class="col-md-3"> 
                    <div class="card-block">
                        <div class="card-body ">สภาพตำแหน่ง :
                        <select class="select2 form-control" style="width: 100%;">
                            <option></option>
                        </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6"> 
                <div class="card-block">
                    <div class="card-body ">โครงสร้าง :
                    <select class="select2 form-control" style="width: 100%;">
                        <option></option>
                    </select>
                    </div>
                </div>
            </div>
            </div>

            
            <div class="row"> 
              <div class="col-md-3"> 
                <div class="card-block">
                  <div class="card-body ">สังกัด :
                      <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                      </select>
                  </div>
                </div>
              </div>
              <div class="col-md-3"> 
                <div class="card-block">
                  <div class="card-body ">หน่วยสังกัด :
                    <select class="select2 form-control" style="width: 100%;">
                        <option></option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-6"></div>
            </div>

            <div class="row"> 
              <div class="col-md-12 ">
                  <div class="card-body" id="NTT">ตำแหน่ง :
                    <textarea class="form-control" id="tareaColor1" style="width: 100%;" rows="4"></textarea>
                  </div>
              </div> 
            </div>

            <div class="row"> 
                  <div class="col-md-3"> 
                    <div class="card-block">
                      <div class="card-body ">อัตรา :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3"> 
                    <div class="card-block">
                      <div class="card-body ">เงินเดือนอัตรา :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3"> 
                    <div class="card-block">
                      <div class="card-body ">เหล่า :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3"> 
                    <div class="card-block">
                      <div class="card-body ">ลชทอ. :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div>
            </div>

            <div class="row"> 
                <div class="col-md-12 ">
                    <div class="card-body" id="NTT">หมายเหตุ :
                    <textarea class="input form-control"></textarea>
                </div>
                </div> 
            </div>

            <div class="row">  
                <div class="col-md-6">
                    <div class="card-body ">ประเภทคำสั่ง :
                        <div class="card-content">
                        <div class="card-body">
                            <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input" name="Edu_hit_st1" id="edu_hst_1">
                            <label class="custom-control-label">ให้ดำรงตำแหน่ง</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input" name="Edu_hit_st1" id="edu_hst_2">
                            <label class="custom-control-label">ให้พ้นจากตำแหน่ง</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"> 
                    <div class="card-block">
                        <div class="card-body ">คำสั่ง :
                        <select class="select2 form-control" style="width: 100%;">
                            <option></option>
                        </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"> 
                    <div class="card-block">
                        <div class="card-body" id="NTT">เลขที่คำสั่ง :
                        <input class="input form-control" style="width: 100%;" placeholder=" ">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row"> 
                <div class="col-md-6">
                    <div class="card-block">
                        <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">ลง  :</label>
                        <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                        <div class="input-group-append">
                            <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="card-block">
                        <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">วันที่มีผล  :</label>
                        <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                        <div class="input-group-append">
                            <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row"> 
                  <div class="col-md-3 ">
                    <div class="card-body" id="NTT">ลำดับที่คำสั่ง :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div> 
            </div>  
              <!-- -------------------- -->
    </div>
            <br><!-- ---------- เว้นระยะห่าง ---------- -->
              <div class="tab-content px-1 pt-1">
                <div class="form-actions center" align="center">
                  <button type="button" class="btn btn-success round btn-min-width mr-1 mb-1" id="submit" name="submit" data-target="#modalConfirm" onclick="insertOrganizationGroupType()">
                    <i class="fa fa-save"></i>&nbsp;บันทึก</button>
                  <button type="button" class="btn btn-danger round btn-min-width mr-1 mb-1" id="type-error" data-dismiss="modal">
                    <i class="fa fa-times-circle-o"></i>&nbsp;ยกเลิก</button>
                </div>
              </div>
          </div>
        <!---------- ---------- End ---------- ---------- -->
      </div> 
    </div> 
  </div>








</div>