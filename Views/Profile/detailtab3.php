<div class="tab-pane" id="tab13" aria-labelledby="base-tab13">
  <a href="#" class="btn btn-social btn-min-width mb-1" style="background-color:#0f1733; color:white;">
    <span class="la la-plus-circle" style="color:white; font-weight: bold;font-size: 18px"></span>เพิ่ม</a>
  <a href="#" class="btn btn-social btn-min-width mb-1" style="background-color:#0f1733; color:white;">
    <span class="la la-trash-o" style="color:white; font-weight: bold;font-size: 18px"></span>ลบ</a>

  <section>
    <table class="table table-striped table-borderless table-hover bootstrap-3 ">
      <thead>
        <tr align="center" style="background-color:#0f1733; color:whitesmoke;">
          <th><input type="checkbox" class="checkAll" onclick="toggle(this);" /></th>
          <th></th>
          <th>ลำดับที่</th>
          <th>ชื่อใหม่</th>
          <th>นามสกุลใหม่</th>
          <th>ชื่อเดิม</th>
          <th>นามสกุลเดิม</th>
          <th>วันที่มีผล</th>
        </tr>
      </thead>
      <tbody align="center">
        <tr>
          <td><input type="checkbox" class="checkAll" /></td>
          <td>
            <a href="#" data-toggle="modal" data-target="#from_editname"><i class="la la-pencil-square-o" style="color:#0f1733;"></i></a>
            <a href="./delete.php"><i class="la la-trash-o" style="color:#0f1733;"></i></a>
          </td>
          <td>1</td>
          <td>กิตติศักดิ์</td>
          <td>สวนสอน</td>
          <td>กิตติศักดิ์1</td>
          <td>สวนสอน2</td>
          <td>18 ก.ค. 2560</td>
        </tr>
        <tr>
          <td><input type="checkbox" class="checkAll" /></td>
          <td>
            <a href="#" data-toggle="modal" data-target="#from_editname"><i class="la la-pencil-square-o" style="color:#0f1733;"></i></a>
            <a href="./detail.php"><i class="la la-trash-o" style="color:#0f1733;"></i></a>
          </td>
          <td>2</td>
          <td>กิตตินันท์</td>
          <td>อารมณ์</td>
          <td>กิตติศักดิ์1</td>
          <td>สวนสอน2</td>
          <td>20 ก.ค. 2560</td>
        </tr>
      </tbody>
    </table>
  </section>

  <div class="modal animated slideInUp text-left modal_custom1" id="from_editname" tabindex="-1" role="dialog" aria-labelledby="modalSettingRegis"  aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <!---------- ---------- Start Content ---------- ---------- -->
          <div class="card-body">
              <div class="model-header" style="background-color:#0f1733;">
              <!-- -------------------- -->
                <div class="row">
                  <div class="col-md-11">
                    <h6 class="model-title text-white px-2 pt-2 py-1">บันทึก/แก้ไข การเปลี่ยนชื่อ/สกุล :  
                      <?php echo $data_HrtRank['HrtRankAbbrTh']." ".$data_Person['PersonName']."   ".$data_Person['SurName']."  หมายเลขประจำตัว : ".$data_Person['AirForceID']; ?></h6>
                  </div>
                  <div class="col-md-1">
                    <h4 class="model-title text-white pt-2"><a data-dismiss="modal"><i class="fa fa-times-circle-o"></i></a></h4>
                  </div>
                </div>
              <!-- -------------------- -->
              </div>
              <div class="model-body">
              <!-- -------------------- -->
                <div class="row pt-2"> 
                  <div class="col-md-6">
                    <div class="card-body" id="NTT">ชื่อเดิม :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div> 
                  <div class="col-md-6">
                    <div class="card-body" id="NTT">นามสกุลเดิม :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div> 
                </div> 

                <div class="row"> 
                  <div class="col-md-6">
                    <div class="card-body" id="NTT">ชื่อใหม่ :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div> 
                  <div class="col-md-6">
                    <div class="card-body" id="NTT">นามสกุลใหม่ :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div> 
                </div> 

                <hr> <!----เส้นคัน--->

                <div class="row"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">หลักฐาน.(ทอ.) :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div> 
                  <div class="col-md-3 ">
                    <div class="card-body" id="NTT">ลำดับที่ในคำสั่ง :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div> 
                  <div class="col-md-3 ">
                    <div class="card-body" id="NTT">เลขที่คำสั่ง :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div> 
                </div> 

                <div class="row"> 
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">วัน/เดือน/ปี ที่ลง :</label>
                        <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">วัน/เดือน/ปี ที่มีผล :</label>
                        <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> 

                <hr> <!----เส้นคัน--->

                <div class="row"> 
                  <div class="col-md-12 ">
                    <div class="card-body" id="NTT">หลักฐาน(ทะเบียนราษฏร์) :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div> 
                </div>

                <div class="row"> 
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">วันที่ลง(ทะเบียนราษฎร์) :</label>
                        <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 pt-1">
                    <div class="card-body" id="NTT">แนบไฟล์หลักฐาน(ทะเบียนราษฎร์) :
                      <input type="file" name="filUpload[]" id="filUpload" multiple="multiple" >
                    </div>
                  </div> 
                </div> 
              <!-- -------------------- -->
              </div>
            <br><!-- ---------- เว้นระยะห่าง ---------- -->
              <div class="tab-content px-1 pt-1">
                <div class="form-actions center" align="center">
                  <button type="button" class="btn btn-success round btn-min-width mr-1 mb-1" id="submit" name="submit" data-target="#modalConfirm" onclick="insertOrganizationGroupType()">
                    <i class="fa fa-save"></i>&nbsp;บันทึก</button>
                  <button type="button" class="btn btn-danger round btn-min-width mr-1 mb-1" id="type-error" data-dismiss="modal">
                    <i class="fa fa-times-circle-o"></i>&nbsp;ยกเลิก</button>
                </div>
              </div>
          </div>
        <!---------- ---------- End ---------- ---------- -->
      </div> 
    </div> 
  </div>

</div>