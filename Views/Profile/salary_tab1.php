<?php
  $sql_Salary_t18 = "SELECT sal1.SalSeq,sal1.SalBonTypeID,sal1.AddamtCode,sal1.SalTimeCode,sal1.Remark,sal1.Remark2,sal1.Remark3,
                            sal1.SourceOfCommID,sal1.CommDocNo,sal1.CommDocDate,sal1.CommEffDate,sal1.CommEffEndDate,sal1.SalBudgetYear,
                            sal1.SalID,sal1.Salary,sal1.SubtMPay,sal1.SalaryPay,sal1.SalaryAdd,sal1.SalaryExpense,sal1.SalCalID,
                            sal2.SalDetail, sal3.AddAmtName
          FROM HrtSalHist sal1 LEFT JOIN HrtSal sal2 ON sal1.SalID = sal2.SalID
          LEFT JOIN HrtAddAmt sal3 ON sal1.AddamtCode = sal3.AddAmtCode
          WHERE sal1.PersonID = '$PersonID' 
          ORDER BY sal1.SalSeq ASC";
  $query_Salary_t18 = sqlsrv_query($conn,$sql_Salary_t18);
  $data_Salary_t18 = array();
  while($row_Salary_t18 = sqlsrv_fetch_array($query_Salary_t18, SQLSRV_FETCH_ASSOC ))
  { $data_Salary_t18[] = $row_Salary_t18; }
   echo $sql_Position_t18;

    //---------- ----- ชนิดของบำเหน็จ
    $sql_SalBonType = "SELECT SalBonTypeID,SalBonTypeName FROM HrtSalBonType WHERE 1=1 ";
    $query_SalBonType = sqlsrv_query($conn,$sql_SalBonType);
    $data_SalBonType = array();
    while($row_SalBonType = sqlsrv_fetch_array($query_SalBonType, SQLSRV_FETCH_ASSOC ))
    { $data_SalBonType[$row_SalBonType['SalBonTypeID']] = $row_SalBonType['SalBonTypeName']; }

    //---------- ----- ชั้น
    $sql_SalCal = "SELECT SalCalID,SalCalName FROM HrtSalCal WHERE 1=1 ";
    $query_SalCal = sqlsrv_query($conn,$sql_SalCal);
    $data_SalCal = array();
    while($row_SalCal = sqlsrv_fetch_array($query_SalCal, SQLSRV_FETCH_ASSOC ))
    { $data_SalCal[$row_SalCal['SalCalID']] = $row_SalCal['SalCalName']; }
    //echo $sql_SalCal;
    //---------- ----- ขั้นเงินเดือน
    $sql_HrtSal = "SELECT SalID,SalDetail FROM HrtSal WHERE 1=1 ORDER BY SalLevelCode ASC";
    $query_HrtSal = sqlsrv_query($conn,$sql_HrtSal);
    $data_HrtSal = array();
    while($row_HrtSal = sqlsrv_fetch_array($query_HrtSal, SQLSRV_FETCH_ASSOC ))
    { $data_HrtSal[$row_HrtSal['SalID']] = $row_HrtSal['SalDetail']; }
    //foreach($data_HrtSal as $key_HrtSal => $val_HrtSal){ echo $key_HrtSal." : ".$val_HrtSal." / "; }

?>

<div class="tab-pane" id="sal_tab11" aria-labelledby="salary-tab11">
  <div class="row">
    <div class="col-12">
      <div class="float-left">
      <a href="#" data-toggle="modal" data-target="#from_sal1" class="btn btn1 mb-1 round"><span class="fa fa-plus-circle"></span> เพิ่ม</a>
        <a href="#" class="btn btn1 mb-1 round"><span class="fa fa-trash-o"></span> ลบ</a>
      </div>
    </div>
  </div> 
  <table class="table table-striped table-borderless table-hover bootstrap-3 ">
    <thead>
      <tr align="center" style="background-color:#0f1733; color:whitesmoke;">
        <th><input type="checkbox" class="checkAll" onclick="toggle(this);" /></th>
        <th></th>
        <th>ลำดับที่</th>
        <th>ประเภทเงิน</th>
        <th>ประเภทบำเหน็จ</th>
        <th><del style="color:#FFFF00">ชั้น</del></th>
        <th>ชั้นเงินเดือน</th>
        <th>จำนวน</th>
        <th>เบิกลด</th>
        <th>ค่าตอบแทน</th>
        <th>ค่าตอบแทนพิเศษ</th>
        <th>วันที่มีผล</th>
      </tr>
    </thead>
    <tbody align="center">
    <?php
        if($data_Salary_t18){
        foreach($data_Salary_t18 as $key_salary18 => $val_salary18 ){
          $set_box_id = $PersonID.$val_salary18['SalSeq'];
     ?>
          <input type="text" id="ipt18_1_<?=$set_box_id?>" style="display:none" value="<?=$val_salary18['SalSeq']?>">
          <input type="text" id="ipt18_2_<?=$set_box_id?>" style="display:none" value="<?=$val_salary18['SalBonTypeID']?>">
          <input type="text" id="ipt18_3_<?=$set_box_id?>" style="display:none" value="<?=$val_salary18['AddamtCode']?>">
          <input type="text" id="ipt18_4_<?=$set_box_id?>" style="display:none" value="<?=$val_salary18['SalTimeCode']?>">
          <input type="text" id="ipt18_5_<?=$set_box_id?>" style="display:none" value="<?=$val_salary18['Remark']?>">
          <input type="text" id="ipt18_6_<?=$set_box_id?>" style="display:none" value="<?=$val_salary18['Remark2']?>">
          <input type="text" id="ipt18_7_<?=$set_box_id?>" style="display:none" value="<?=$val_salary18['Remark3']?>">
          <input type="text" id="ipt18_8_<?=$set_box_id?>" style="display:none" value="<?=$val_salary18['SourceOfCommID']?>">
          <input type="text" id="ipt18_9_<?=$set_box_id?>" style="display:none" value="<?=$val_salary18['CommDocNo']?>">
          <input type="text" id="ipt18_10_<?=$set_box_id?>" style="display:none" value="<?=$val_salary18['CommDocDate']?>">
          <input type="text" id="ipt18_11_<?=$set_box_id?>" style="display:none" value="<?=$val_salary18['CommEffDate']?>">
          <input type="text" id="ipt18_12_<?=$set_box_id?>" style="display:none" value="<?=$val_salary18['CommEffEndDate']?>">
          <input type="text" id="ipt18_13_<?=$set_box_id?>" style="display:none" value="<?=$val_salary18['SalBudgetYear']?>">
          <input type="text" id="ipt18_14_<?=$set_box_id?>" style="display:none" value="<?=$val_salary18['SalID']?>">
          <input type="text" id="ipt18_15_<?=$set_box_id?>" style="display:none" value="<?=$val_salary18['Salary']?>">
          <input type="text" id="ipt18_16_<?=$set_box_id?>" style="display:none" value="<?=$val_salary18['SubtMPay']?>">
          <input type="text" id="ipt18_17_<?=$set_box_id?>" style="display:none" value="<?=$val_salary18['SalaryPay']?>">
          <input type="text" id="ipt18_18_<?=$set_box_id?>" style="display:none" value="<?=$val_salary18['SalaryAdd']?>">
          <input type="text" id="ipt18_19_<?=$set_box_id?>" style="display:none" value="<?=$val_salary18['SalaryExpense']?>">
          <input type="text" id="ipt18_20_<?=$set_box_id?>" style="display:none" value="<?=$val_salary18['SalCalID']?>">
          <input type="text" id="ipt18_21_<?=$set_box_id?>" style="display:none" value="<?=$val_salary18['SalID_t18']?>">
      <tr>
        <td><input type="checkbox" class="checkAll" /></td>
        <td>
        <a href="#" data-toggle="modal" data-target="#from_sal1" onClick="editSalary('<?=$set_box_id?>');"><i class="la la-pencil-square-o" style="color:#0f1733;"></i></a>
          <a href="#"><i class="la la-trash-o" style="color:#0f1733;"></i></a>
        <td><?php echo $val_salary18['SalSeq']; ?></td>
        <td><?php echo $val_salary18['AddAmtName']; ?></td>
        <td><?php echo $data_SalBonType[$val_salary18['SalBonTypeID']]; ?></td>
        <td><?php echo $data_SalCal[$val_salary18['SalCalID']]; ?></td>
        <td><?php echo $val_salary18['SalDetail']; ?></td>
        <td><?php echo number_format($val_salary18['Salary']); ?></td>
        <td><?php echo number_format($val_salary18['SubtMPay']); ?></td>
        <td><?php echo number_format($val_salary18['SalaryPay']); ?></td>
        <td><?php echo number_format($val_salary18['SalaryAdd']); ?></td>
        <td><?php echo DateOnlyDisplay($val_salary18['CommEffDate'],8); ?></td>
      <?php
          }
        }
      ?>
      </tr>
    </tbody>
  </table>
  <div class="modal animated slideInUp text-left modal_custom1" id="from_sal1" tabindex="-1" role="dialog" aria-labelledby="modalSettingRegis"  aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <!---------- ---------- Start Content ---------- ---------- -->
          <div class="card-body">
              <div class="model-header" style="background-color:#0f1733;">
              <!-- -------------------- -->
                <div class="row">
                  <div class="col-md-11">
                    <h6 class="model-title text-white px-2 pt-2 py-1">บันทึก/แก้ไข เงินเดือน เบี้ยหวัด บำเหน็จ บำนาญ 
                      <?php echo $data_HrtRank['HrtRankAbbrTh']." ".$data_Person['PersonName']."   ".$data_Person['SurName']."  หมายเลขประจำตัว : ".$data_Person['AirForceID']; ?></h6>
                  </div>
                  <div class="col-md-1">
                    <h4 class="model-title text-white pt-2"><a data-dismiss="modal" onClick="ClearForm_t18();"><i class="fa fa-times-circle-o"></i></a></h4>
                  </div>
                </div>
              <!-- -------------------- -->
              </div>
              <div class="model-body">
              <!-- -------------------- -->
          <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">
              <div class="row"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">ปีงบประมาณ :
                        <input class="input form-control" style="width: 100%;" name="SalBudgetYear_t18" id="SalBudgetYear_t18">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">วาระ :
                        <select class="select2 form-control" style="width: 100%;" name="SalTimeCode_t18" id="SalTimeCode_t18">
                          <option value="">โปรดเลือก วาระ</option>
                            <?php
                                $sql_HrtSalTime = "SELECT SalTimeCode,SalTimeName From HrtSalTime where 1=1 ";
                                $query_HrtSalTime = sqlsrv_query($conn, $sql_HrtSalTime );
                                $data_HrtSalTime = array();
                                while($row_HrtSalTime = sqlsrv_fetch_array($query_HrtSalTime, SQLSRV_FETCH_ASSOC ))
                                { $data_HrtSalTime[] = $row_HrtSalTime ; }

                                if($data_HrtSalTime){
                                  foreach($data_HrtSalTime as $key_HrtSalTime => $val_HrtSalTime ){
                                  ?>
                                    <option value="<?php echo $val_HrtSalTime['SalTimeCode'];?>">
                                      <?php echo $val_HrtSalTime['SalTimeName']; ?> </option>
                                  <?php
                                  }
                                }   
                            ?>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">ประเภทเงิน :
                        <select class="select2 form-control" style="width: 100%;"  name="AddAmtCode_t18" id="AddAmtCode_t18">
                        <option value="">โปรดเลือก ประเภทเงิน</option>
                            <?php
                                $sql_AmtCode = "SELECT AddAmtCode,AddAmtName,AddAmtAbbrName From HrtAddAmt where 1=1 ";
                                $query_AmtCode = sqlsrv_query($conn, $sql_AmtCode );
                                $data_AmtCode = array();
                                while($row_AmtCode = sqlsrv_fetch_array($query_AmtCode, SQLSRV_FETCH_ASSOC ))
                                { $data_AmtCode[] = $row_AmtCode ; }

                                if($data_AmtCode){
                                  foreach($data_AmtCode as $key_AmtCode => $val_AmtCode ){
                                  ?>
                                    <option value="<?php echo $val_AmtCode['AddAmtCode'];?>">
                                      <?php echo $val_AmtCode['AddAmtName']." : ".$val_AmtCode['AddAmtAbbrName']; ?> </option>
                                  <?php
                                  }
                                }   
                            ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">ประเภทบำเหน็จ :
                        <select class="select2 form-control" style="width: 100%;" name="SalBonTypeID_t18" id="SalBonTypeID_t18">
                          <option value="">โปรดเลือก ประเภทบำเหน็จ</option>
                            <?php
                                $sql_SalBonType = "SELECT SalBonTypeID,SalBonTypeName From HrtSalBonType where 1=1 ";
                                $query_SalBonType = sqlsrv_query($conn, $sql_SalBonType );
                                $data_SalBonType = array();
                                while($row_SalBonType = sqlsrv_fetch_array($query_SalBonType, SQLSRV_FETCH_ASSOC ))
                                { $data_SalBonType[] = $row_SalBonType ; }

                                if($data_SalBonType){
                                  foreach($data_SalBonType as $key_SalBonType => $val_SalBonType ){
                                  ?>
                                    <option value="<?php echo $val_SalBonType['SalBonTypeID'];?>">
                                      <?php echo $val_SalBonType['SalBonTypeName']; ?> </option>
                                  <?php
                                  }
                                }   
                            ?>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">ขั้น :
                        <select class="select2 form-control" style="width: 100%;" id="SalCalID_t18" name="SalCalID_t18">
                          <option value="">โปรดเลือก ขั้น</option>
                          <?php
                          foreach($data_SalCal as $key_SalCal => $val_SalCal){
                            ?>
                          <option value="<?php echo $key_SalCal;?>"><?php echo $val_SalCal; ?></option>
                            <?php
                            }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6"> 
                  
                  </div>
                </div>

   <!------------------- ส่วนที่ 1 -4  ----------------------------->

        <div class="card collapse-icon accordion-icon-rotate active">
          <div class="card-header bg-success">
            <a data-toggle="collapse" href="#motab5_s1_h1" aria-expanded="true" aria-controls="motab5_s1_h1" class="card-title lead white">
              <h6><U>ส่วนที่ 1</U>  เงินเดือน </h6></a>
          </div>
          <div id="motab5_s1_h1" role="tabpanel" class="card-collapse collapse show" aria-expanded="true">
            <div class="card-content">
              <div class="row"> 
                    <div class="col-md-6"> 
                      <div class="card-block">
                        <div class="card-body ">ชั้น/ขั้นเงินเดือน :
                          <select class="select2 form-control" style="width: 100%;"name="SalID_t18" id="SalID_t18">
                            <option value="">โปรดเลือก ขั้น</option>
                            <?php
                                foreach($data_HrtSal as $key_HrtSal => $val_HrtSal){
                                ?>
                                  <option value="<?php echo $key_HrtSal;?>"><?php echo $val_HrtSal; ?></option>
                                <?php
                                }
                            ?>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6 ">
                      <div class="card-body" >จำนวน :
                        <input class="input form-control" style="width: 100%;" name="Salary_t18" id="Salary_t18">
                      </div>
                    </div> 
              </div>

              <div class="row"> 
                  <div class="col-md-6 ">
                    <div class="card-body" >เบิกลด :
                      <input class="input form-control" style="width: 100%;" name="SubtMPay_t18" id="SubtMPay_t18">
                    </div>
                  </div> 
                  <div class="col-md-6 ">
                    <div class="card-body" >เงินค่าตอบแทน :
                      <input class="input form-control" style="width: 100%;" name="SalaryPay_t18" id="SalaryPay_t18">
                    </div>
                  </div> 
              </div> 

              <div class="row"> 
                  <div class="col-md-6 ">
                    <div class="card-body" >เงินตอบแทนพิเศษ :
                      <input class="input form-control" style="width: 100%;" name="SalaryAdd_t18" id="SalaryAdd_t18">
                    </div>
                  </div> 
                  <div class="col-md-6 ">
                    <div class="card-body" >ค่าครองชีพพิเศษ :
                      <input class="input form-control" style="width: 100%;" name="SalaryExpense_t18" id="SalaryExpense_t18">
                    </div>
                  </div> 
              </div> 

              <div class="row"> 
                  <div class="col-md-12 ">
                    <div class="card-body">รายละเอียด :
                    <input class="input form-control" style="width: 100%;" name="Remark_t18" id="Remark_t18">
                    </div>
                  </div> 
              </div>
          </div>
      </div>

      <br>
          <div class="card collapse-icon accordion-icon-rotate active">
            <div class="card-header bg-success">
              <a data-toggle="collapse" href="#motab5_s1_h2" aria-expanded="true" aria-controls="motab5_s1_h2"class="card-title lead white">
                <h6><U>ส่วนที่ 2</U> ปรับเงินเดือน</h6></a>
            </div>
            <div id="motab5_s1_h2" role="tabpanel" class="card-collapse collapse show" aria-expanded="true">
              <div class="card-content">
                <div class="row"> 
                      <div class="col-md-6"> 
                        <div class="card-block">
                          <div class="card-body ">ชั้น/ขั้นเงินเดือน :
                            <select class="select2 form-control" style="width: 100%;">
                              <option value="">โปรดเลือก ขั้น</option>
                              <?php
                                  foreach($data_HrtSal as $key_HrtSal => $val_HrtSal){
                                  ?>
                                    <option value="<?php echo $key_HrtSal;?>"><?php echo $val_HrtSal; ?></option>
                                  <?php
                                  }
                              ?>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6 ">
                        <div class="card-body" >จำนวน :
                          <input class="input form-control" style="width: 100%;" placeholder=" ">
                        </div>
                      </div> 
                </div>

                <div class="row"> 
                    <div class="col-md-6 ">
                      <div class="card-body" >เบิกลด :
                        <input class="input form-control" style="width: 100%;" name="" id="">
                      </div>
                    </div> 
                    <div class="col-md-6 ">
                      <div class="card-body" >เงินค่าตอบแทน :
                        <input class="input form-control" style="width: 100%;" >
                      </div>
                    </div> 
                </div> 

                <div class="row"> 
                    <div class="col-md-6 ">
                      <div class="card-body" >เงินตอบแทนพิเศษ :
                        <input class="input form-control" style="width: 100%;">
                      </div>
                    </div> 
                    <div class="col-md-6 ">
                      <div class="card-body" >ค่าครองชีพพิเศษ :
                        <input class="input form-control" style="width: 100%;" >
                      </div>
                    </div> 
                </div> 

                <div class="row"> 
                    <div class="col-md-12 ">
                      <div class="card-body" >รายละเอียด :
                      <input class="input form-control" style="width: 100%;" name="Remark2_t18" id="Remark2_t18">
                      </div>
                    </div> 
                </div>
              </div>
            </div>
          </div>


      <br>
          <div class="card collapse-icon accordion-icon-rotate active">
            <div class="card-header bg-success">
              <a data-toggle="collapse" href="#motab5_s1_h3" aria-expanded="true" aria-controls="motab5_s1_h3"class="card-title lead white">
                <h6><U>ส่วนที่ 3</U> เลื่อนขั้นรับเงินเดือน</h6></a>
            </div>
            <div id="motab5_s1_h3" role="tabpanel" class="card-collapse collapse show" aria-expanded="true">
              <div class="card-content">
              <div class="row"> 
                    <div class="col-md-6"> 
                      <div class="card-block">
                        <div class="card-body ">ชั้น/ขั้นเงินเดือน :
                          <select class="select2 form-control" style="width: 100%;">
                            <option value="">โปรดเลือก ขั้น</option>
                            <?php
                                foreach($data_HrtSal as $key_HrtSal => $val_HrtSal){
                                ?>
                                  <option value="<?php echo $key_HrtSal;?>"><?php echo $val_HrtSal; ?></option>
                                <?php
                                }
                            ?>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6 ">
                      <div class="card-body" >จำนวน :
                        <input class="input form-control" style="width: 100%;" >
                      </div>
                    </div> 
              </div>

              <div class="row"> 
                  <div class="col-md-6 ">
                    <div class="card-body" >เบิกลด :
                      <input class="input form-control" style="width: 100%;" >
                    </div>
                  </div> 
                  <div class="col-md-6 ">
                    <div class="card-body" >เงินค่าตอบแทน :
                      <input class="input form-control" style="width: 100%;">
                    </div>
                  </div> 
              </div> 

              <div class="row"> 
                  <div class="col-md-6 ">
                    <div class="card-body" >เงินตอบแทนพิเศษ :
                      <input class="input form-control" style="width: 100%;" >
                    </div>
                  </div> 
                  <div class="col-md-6 ">
                    <div class="card-body" >ค่าครองชีพพิเศษ :
                      <input class="input form-control" style="width: 100%;" >
                    </div>
                  </div> 
              </div> 

              <div class="row"> 
                  <div class="col-md-12 ">
                    <div class="card-body" >รายละเอียด :
                    <input class="input form-control" style="width: 100%;" name="Remark3_t18" id="Remark3_t18">
                    </div>
                  </div> 
              </div>
          </div>
        </div>
      </div>
      <br>
          <div class="card collapse-icon accordion-icon-rotate active">
            <div class="card-header bg-success">
              <a data-toggle="collapse" href="#motab5_s1_h4" aria-expanded="true" aria-controls="motab5_s1_h4"class="card-title lead white">
                <h6><U>ส่วนที่ 4</U> คำสั่ง</h6></a>
            </div>
            <div id="motab5_s1_h4" role="tabpanel" class="card-collapse collapse show" aria-expanded="true">
              <div class="card-content">
                <div class="row"> 
                    <div class="col-md-6"> 
                      <div class="card-block">
                        <div class="card-body ">คำสั่ง :
                          <select class="select2 form-control" style="width: 100%;" name="SourceOfCommID_t18" id="SourceOfCommID_t18">
                            <option value="">โปรดเลือก คำสั่ง</option>
                            <?php
                                $sql_SOC = "SELECT SourceOfCommID,SourceOfCommName From HrtSourceOfComm where 1=1 ";
                                $query_SOC = sqlsrv_query($conn, $sql_SOC );
                                $data_SOC = array();
                                while($row_SOC = sqlsrv_fetch_array($query_SOC, SQLSRV_FETCH_ASSOC ))
                                { $data_SOC[] = $row_SOC ; }

                                if($data_SOC){
                                  foreach($data_SOC as $key_SOC => $val_SOC ){
                                  ?>
                                    <option value="<?php echo $val_SOC['SourceOfCommID'];?>">
                                      <?php echo $val_SOC['SourceOfCommName']; ?> </option>
                                  <?php
                                  }
                                }   
                            ?>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6"></div>
                </div>

              <div class="row"> 
                <div class="col-md-6 ">
                  <div class="card-body" >เลขที่คำสั่ง :
                    <input class="input form-control" style="width: 100%;" name="CommDocNo_t18" id="CommDocNo_t18">
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="card-block">
                    <div class="input-group col-12 datep">
                      <label class="label-control col-12 pl-0">ลง  :</label>
                      <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                      <div class="input-group-append">
                        <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                      </div>
                    </div>
                  </div>
                </div> 
              </div>

              <div class="row"> 
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="input-group col-12 datep">
                      <label class="label-control col-12 pl-0">วันที่ผล  :</label>
                      <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                      <div class="input-group-append">
                        <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="input-group col-12 datep">
                      <label class="label-control col-12 pl-0">วันที่สิ้นสุด  :</label>
                      <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                      <div class="input-group-append">
                        <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row"> 
                <div class="col-md-6 ">
                  <div class="card-body" >ลำดับที่ในคำสั่ง :
                    <input class="input form-control" style="width: 100%;" name=""id="">
                  </div>
                </div> 
              </div> 

              </div>
            </div>
          </div>
      <br>
      </div>
      <!-- -----------------------------จบส่วนที่1-4--------------------------- -->

            <input type="text" name="PersonID_t18" id="PersonID_t18" style="display:none" value="<?=$PersonID?>">
          </form>
              <!-- -------------------- -->
              </div>
            <br><!-- ---------- เว้นระยะห่าง ---------- -->
              <div class="tab-content px-1 pt-1">
                <div class="form-actions center" align="center">
                  <button type="button" class="btn btn-success round btn-min-width mr-1 mb-1" id="submit" name="submit" data-target="#modalConfirm" onclick="insertOrganizationGroupType()">
                    <i class="fa fa-save"></i>&nbsp;บันทึก</button>
                  <button type="button" class="btn btn-danger round btn-min-width mr-1 mb-1" id="type-error" data-dismiss="modal"  onClick="ClearForm_t18();">
                    <i class="fa fa-times-circle-o"></i>&nbsp;ยกเลิก</button>
                </div>
              </div>
          </div>
        <!---------- ---------- End ---------- ---------- -->
      </div> 
    </div> 

  </div>

<script type="text/javascript">
 function editSalary(condition_value)
  {
    $('#SalSeq_t18').val($('#ipt18_1_'+condition_value).val());
    $('#SalBonTypeID_t18').val($('#ipt18_2_'+condition_value).val()).trigger("change");
    $('#AddAmtCode_t18').val($('#ipt18_3_'+condition_value).val()).trigger("change");
    $('#SalTimeCode_t18').val($('#ipt18_4_'+condition_value).val()).trigger("change");
    $('#Remark_t18').val($('#ipt18_5_'+condition_value).val());
    $('#Remark2_t18').val($('#ipt18_6_'+condition_value).val());
    $('#Remark3_t18').val($('#ipt18_7_'+condition_value).val());
    $('#SourceOfCommID_t18').val($('#ipt18_8_'+condition_value).val()).trigger("change");
    $('#CommDocNo_t18').val($('#ipt18_9_'+condition_value).val());
    $('#CommDocDate_t18').val($('#ipt18_10_'+condition_value).val());
    $('#CommEffDate_t18').val($('#ipt18_11_'+condition_value).val());
    $('#CommEffEndDate_t18').val($('#ipt18_12_'+condition_value).val());
    $('#SalBudgetYear_t18').val($('#ipt18_13_'+condition_value).val());
    $('#SalID_t18').val($('#ipt18_14_'+condition_value).val());
    $('#Salary_t18').val($('#ipt18_15_'+condition_value).val());
    $('#SubtMPay_t18').val($('#ipt18_16_'+condition_value).val());
    $('#SalaryPay_t18').val($('#ipt18_17_'+condition_value).val());
    $('#SalaryAdd_t18').val($('#ipt18_18_'+condition_value).val());
    $('#SalaryExpense_t18').val($('#ipt18_19_'+condition_value).val());
    $('#SalCalID_t18').val($('#ipt18_20_'+condition_value).val()).trigger("change");
    $('#SalID_t18').val($('#ipt18_21_'+condition_value).val()).trigger("change");
  }
  function ClearForm_t18()
  {
      var list_object_M = new Array('SalSeq_t18','SalBonTypeID_t18','AddAmtCode_t18',
      'SalTimeCode_t18','Remark_t18','Remark2_t18','Remark3_t18','SourceOfCommID_t18',
      'CommDocNo_t18','CommDocDate_t18','CommEffDate_t18','CommEffEndDate_t18','SalBudgetYear_t18',
      'SalID_t18','Salary_t18','SubtMPay_t18','SalaryPay_t18','SalaryAdd_t18','SalaryExpense_t18','SalCalID_t18','SalID_t18');
      for(clear_value = 0; clear_value <= 20; clear_value++){
        $('#'+list_object_M[clear_value]).val('');
        if(clear_value < 8){ $('#'+list_object_M[clear_value]); }
      }
  }
</script>
</div>