<?php //---------- header
error_reporting(~E_NOTICE); 
    require_once "../../config.php";
    require_once '../../Model/Ducklab/func.php';
    require_once '../include/header.php';
  
    $menu1 ="HRTOFFICIAL";
    $menu2 ="PROFILE"; 
?>

<!-- menu -->
<?php include '../include/menu.php'; ?>

<?php  

  if( $_POST['action']== "search")
  { 
      $where ="";
      $check_ops = 0;
        //---------- OPS-1
          if(!empty($_POST['HrtAirForceID'])){
              $check_ops = 1;
              $where.= " AirForceID = '{$_POST['HrtAirForceID']}' ";
          }
        //---------- OPS-2
          if(!empty($_POST['HrtPerCardID']))
          {
              if($check_ops > 0){
                  $check_ops = 2;
                  $where.= " AND PerCardID = '{$_POST['HrtPerCardID']}' ";
              } else {
                  $check_ops = 1;
                  $where.= " PerCardID = '{$_POST['HrtPerCardID']}' ";
              }
          }
        //---------- OPS-3
          if(!empty($_POST['HrtName']))
          {
              $expHrtName = explode(" ", $_POST['HrtName']);
              if((!empty($expHrtName[0]) && !empty($expHrtName[1])) && $check_ops > 0){ //---------- ชื่อ และ นามสกุล ตรงๆ
                $check_ops = 3;
                $where.= " AND (PersonName = '{$expHrtName[0]}' AND SurName = '{$expHrtName[1]}') ";
              } elseif(!empty($expHrtName[0]) && !empty($expHrtName[1])){
                $check_ops = 1;
                $where.= " (PersonName = '{$expHrtName[0]}' AND SurName = '{$expHrtName[1]}') ";
              }
              else
              {
                  if($_POST['HrtName'][0] == '*' && $check_ops > 0){ //---------- *นามสกุล
                      $check_ops = 3;
                      $where.= " AND SurName LIKE '".substr($_POST['HrtName'],1)."%' ";
                  } elseif($_POST['HrtName'][0] == '*'){
                      $check_ops = 1;
                      $where.= " SurName LIKE '".substr($_POST['HrtName'],1)."%' ";
                  } elseif(substr($_POST['HrtName'],-1) == '*' && $check_ops > 0){ //---------- ชื่อ*
                      $check_ops = 3;
                      $exp_sn = explode("*", $_POST['HrtName']);
                      $where.= " AND PersonName LIKE '{$exp_sn[0]}%' ";
                  } elseif(substr($_POST['HrtName'],-1) == '*'){
                      $check_ops = 1;
                      $exp_sn = explode("*", $_POST['HrtName']);
                      $where.= " PersonName LIKE '{$exp_sn[0]}%' ";
                  } elseif(($_POST['HrtName'][0] != ' ' && substr($_POST['HrtName'],-1) != ' ') && $check_ops > 0){ //---------- ส่วนใดส่วนหนึ่งของ ชื่อ หรือ นามสกุล
                    $check_ops = 3;
                    $where.= " AND PersonName LIKE '%{$_POST['HrtName']}%' OR SurName LIKE '%{$_POST['HrtName']}%' ";
                  } elseif($_POST['HrtName'][0] != ' ' && substr($_POST['HrtName'],-1) != ' '){
                    $check_ops = 1;
                    $where.= " PersonName LIKE '%{$_POST['HrtName']}%' OR SurName LIKE '%{$_POST['HrtName']}%' ";
                  }
              } 
          }
        //---------- OPS-4
          if(!empty($_POST['HrtSoundDex']))
          {
              if($check_ops > 0){
                  $check_ops = 4;
                  $where.= " AND SoundDex LIKE  '%".$_POST['HrtSoundDex']."%' ";
              } else {
                  $check_ops = 1;
                  $where.= " SoundDex LIKE  '%".$_POST['HrtSoundDex']."%' ";
              }
          }
        //---------- OPS-5
          if(!empty($_POST['set_HrtPersonTypeName']))
          {
              if($check_ops > 0){
                  $check_ops = 5;
                  $where.= " AND PersonTypeID = '{$_POST['set_HrtPersonTypeName']}' ";
              } else {
                  $check_ops = 1;
                  $where.= " PersonTypeID = '{$_POST['set_HrtPersonTypeName']}' ";
              }
          }
  
          // if( !empty($_POST['HrtRankNameTh']) ){
          //    $check_ops=1;
          //    $where.= " AND HrtRankNameTh LIKE  '%".$_POST['HrtRankNameTh']."%' ";
          // }
  
          // if( !empty($_POST['HrtRankNameTh']) ){
          //    $check_ops=1;
          //    $where.= " AND HrtRankNameTh LIKE  '%".$_POST['HrtRankNameTh']."%' ";
          // } //----- 
  

          if($check_ops > 0){
              $sql_search ="SELECT  PersonID,AirForceID,SoundDex,RankID,PersonName,SurName,PosPositionID,PersonSts,PrefixID
              FROM HrtPerson where PersonSts <> 'D' AND ".$where." 
              ORDER BY PersonSts DESC, PersonTypeID ASC, RankID ASC, PersonName ASC, SurName ASC";

              $query_search = sqlsrv_query($conn, $sql_search );
              $data_search = array();

              while($row=sqlsrv_fetch_array($query_search, SQLSRV_FETCH_ASSOC )){
                  $data_search[] = $row ; 
              }
              //echo $sql;

              //---------- ----- คำนำหน้าชื่อ
              $sql_HrtPrefix = "SELECT HrtPrefixID,HrtPrefixAbbrNameTH FROM HrtPrefix 
                                WHERE HrtPrefixID IN ('8','9','10','11','12','14','17','19','20','21','22','23','24','26','27','28','29','30','33','34','35','36') 
                                ORDER BY HrtPrefixID ASC";
              $query_HrtPrefix = sqlsrv_query($conn,$sql_HrtPrefix);
              $data_HrtPrefix = array();
              while($row_HrtPrefix = sqlsrv_fetch_array($query_HrtPrefix, SQLSRV_FETCH_ASSOC ))
              { $data_HrtPrefix[$row_HrtPrefix['HrtPrefixID']] = $row_HrtPrefix['HrtPrefixAbbrNameTH']; }

              //---------- ----- ยศ
              $sql_RankAbbr = "SELECT HrtRankID,HrtRankAbbrTh FROM HrtRank WHERE ((HrtRankID > '30000' AND HrtRankID < '40000') OR HrtRankID > '50000') ORDER BY HrtRankID ASC";
              $query_RankAbbr = sqlsrv_query($conn,$sql_RankAbbr);
              $data_RankAbbr = array();
              while($row_RankAbbr = sqlsrv_fetch_array($query_RankAbbr, SQLSRV_FETCH_ASSOC ))
              { $data_RankAbbr[$row_RankAbbr['HrtRankID']] = $row_RankAbbr['HrtRankAbbrTh']; }
          
          }else{
              
          }
      //---------- ---------- ---------- ----------//
  }
/*
    $sql04 = "SELECT HrtPosCode,HrtPosName From HrtPos where 1=1 ";

    $query04 = sqlsrv_query($conn, $sql04 );
    $data04=array();

    while($row04=sqlsrv_fetch_array($query04, SQLSRV_FETCH_ASSOC )){
        $data04[] = $row04 ; 
    }
*/
?>

<script type="text/javascript">
</script>

  <section>

      <div class="app-content content">
        <div class="content-wrapper">

          <div class="content-header row">
            <div class="content-header-left col-12 mb-2">
              <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
              <h3 class="content-header-title">ข้อมูลบุคคล <?php //echo $sql_search; ?></h3>
            </div>
          </div>

          <div class="content-body">            
            <section id="bootstrap3">
              <div class="row">
                <div class="col-12">
                  <div class="card">
                    <div class="card-content collapse show">
                      <div class="card-body card-dashboard">
                        <p class="card-text"></p>
                        <nav aria-label="breadcrumb">
                          <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../home/index.php">งานประวัติข้าราชการ</a></li>
                            <li class="breadcrumb-item active" aria-current="page">ระบบค้นหาข้อมูลบุคคล</li>
                          </ol>
                        </nav>
                        <div class="content-body">
                          <section class="horizontal-grid" id="horizontal-grid">
                            <div class="row">
                              <div class="col-md-12">
                                <div class="box_h1 card">
                                  <div class="card-header card-head-inverse">
                                    <h4 class="card-title text-white"> ค้นหา <a class="nav-menu-main menu-toggle" href="" id="auto_panel">-</a></h4> 
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                      <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                                      </ul>
                                    </div>
                                  </div>

                                  <div class="card-content collapse show">
                                    <div class="card-body">
          <!----------------------------------------------------------------------------------------------------------------->
          <form action="" method="post" id="frmsearch">
            
              <div class="form-body">
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group"><span id="label_AirForceID">เลขบัตรประจำตัวราชการ:</span>
                      <input type="text" class="form-control"  placeholder=" " name="HrtAirForceID" id="cHrtAirForceID"
                        autocomplete=off value="<?php echo $_POST['HrtAirForceID'];?>" OnChange="CheckForce10(this.value,'cHrtAirForceID');">
                      <span id="error_AirForceID" class="text-danger" style="display:none">เลขบัตรประจำตัวราชการ 10 หรือ 12 หลักค่ะ</span>
                    </div>
                  </div>
        
                  <div class="col-md-4">
                    <div class="form-group"><span id="label_PerCardID">เลขประจำตัวบัตรประชาชน:</span>
                      <input type="text" class="form-control" placeholder="" name="HrtPerCardID" id="cHrtPerCardID"
                        autocomplete=off value="<?php echo $_POST['HrtPerCardID'];?>" OnChange="CheckMember13(this.value,'cHrtPerCardID');">
                      <span id="error_PerCardID" class="text-danger" style="display:none">รหัสประชาชนไม่ครบ 13 หลักค่ะ</span>
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="form-group">ชื่อ - นามสกุล:
                      <input type="text" class="form-control" placeholder=" " name="HrtName" id="cHrtName" 
                        autocomplete=off value="<?php echo $_POST['HrtName'];?>" OnChange="$('#frmsearch').attr('onsubmit','return true;');">
                    </div>
                  </div> 
                </div>                    

                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">หมายเลขแฟ้มประวัติ:
                      <input type="text" class="form-control" placeholder="" name="HrtSoundDex" id="cHrtSoundDex" 
                        autocomplete=off value="<?php echo $_POST['HrtSoundDex'];?>" OnChange="$('#frmsearch').attr('onsubmit','return true;');">
                    </div>
                  </div>              

                  <div class="col-md-4">
                    <div class="form-group">ประเภทกำลังพล:
                      <select  class="select2 form-control" style="width: 100%;" name="set_HrtPersonTypeName" id="cPersonType" > 
                          <option value="0">  กรุณาเลือกประเภทกำลังพล  </option>
                          <?php
                              $sql_4ops_5 = "SELECT HrtPersonTypeID,HrtPersonTypeName From HrtPersonType where 1=1 ";

                                  $query_4ops_5 = sqlsrv_query($conn, $sql_4ops_5 );
                                  $data_4ops_5=array();
                              
                                  while($row_4ops_5=sqlsrv_fetch_array($query_4ops_5, SQLSRV_FETCH_ASSOC )){
                                      $data_4ops_5[] = $row_4ops_5 ; 
                                  } 
                              if($data_4ops_5){
                                  foreach($data_4ops_5 as $key => $val ){ //HrtPosCode,HrtPosName 
                                  ?>
                                <option value="<?php echo $val['HrtPersonTypeID']; ?>"
                                <?php if( $val['HrtPersonTypeID'] == $_POST['set_HrtPersonTypeName']) { echo "selected"; } ?> > <?php echo $val['HrtPersonTypeName']; ?>
                                </option>
                                  <?php
                                  }
                              } 
                          ?> 
                      </select>
                    </div>
                  </div>        

                  <!-- <div class="col-md-4">
                    <div class="form-group">ตำแหน่ง:
                      <select  class="select2 form-control"style="width: 100%;" name="set_HrtPosName" id="set_HrtPosName" >
                          <option value="">  กรุณาเลือกประเภทตำแหน่ง  </option>
                      </select>
                    </div>
                  </div> -->
                </div>

                <div class="text-center">
                  <input type="hidden" name="action" value="search">
                  <button type="submit" class="btn round btn1 btncustom1 mr-1 " id="btnsubmit"> 
                    <i class="fa fa-search"></i> ค้นหา </button>
                    <button type="reset" class="btn round btn1 btncustom1" name="repeat" id="repeat" 
                      <?php if($_POST['set_HrtPersonTypeName']) { ?> onClick="ClearForm_search('2');" <?php } else { ?>onClick="ClearForm_search('1');" <?php } ?>>
                    <i class=" fa fa-repeat"></i> ล้างค่า </button>
                </div>
          </form>
          <!----------------------------------------------------------------------------------------------------------------->
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </div>
                          </section>
                        </div>

      <br>
      <a href="add_new.php" target="_blank" class="btn btn-social btn-min-width mb-1" style="background-color:#0f1733; color:white;">
          <span class="la la-plus-circle" style="color:white; font-weight: bold;font-size: 18px"></span> เพิ่ม </a>
      <!-- <a href="#" class="btn btn-social btn-min-width mb-1" style="background-color:#0f1733; color:white;">
          <span class="la la-trash-o" style="color:white; font-weight: bold;font-size: 18px"></span> ลบ </a> -->
<!-- ---------- ./create.php---------- ./delete.php---------- ---------- ---------- Show DATA ---------- ---------- ---------- ---------- ---------- -->
      <br><br>
      <table id="PersonTable"  class="table table-striped table-borderless table-hover table_custom1">
          <thead>
            <tr>
              <!-- <th width="3%"><input type="checkbox" class="checkAll" onclick="toggle(this);" /></th> -->
              <th width="7%">  </th>
              <th width="5%">ลำดับที่</th>
              <th width="15%">เลขประจำตัวข้าราชการ</th>
              <th width="10%">หมายเลขแฟ้ม</th>
              <th width="5%">ยศ</th>
              <th width="13%">ชื่อ</th>
              <th width="12%">นามสกุล</th>
              <th width="20%">ตำแหน่ง</th>
              <th width="10%">สถานะ</th>
            </tr>
          </thead>
          <tbody >
              <?php 
                if($data_search)
                {
                    $arr_PersonSts = array("N" => "พ้นจากราชการ", "Y" => "ยังอยู่ในราชการ");
                    $i=1; 
                    foreach( $data_search as $key => $val )
                    {
                        // $sql_pos_name = " SELECT HrtPosition.HrtPosOrgAbbrName 
                        //                   FROM HrtPosition left join HrtPosPerson
                        //                   ON HrtPosition.HrtPositionID = HrtPosPerson.HrtPositionID AND HrtPosition.HrtPosNum = HrtPosPerson.HrtPosNo
                        //                   WHERE HrtPosPerson.HrtPersonID = '". $val['PersonID']."' ";
                        $sql_pos_name = " SELECT HrtPosOrgAbbrName 
                                          FROM HrtPosition
                                          WHERE HrtPositionID = '". $val['PosPositionID']."' ";

                        $query_pos_name = sqlsrv_query($conn, $sql_pos_name );
                            if( $query_pos_name === false ) {
                              die( print_r( sqlsrv_errors(), true));
                            }
                        $data_pos_name = array();

                        while($row_pos_name = sqlsrv_fetch_array($query_pos_name, SQLSRV_FETCH_ASSOC )){
                            $data_pos_name[] = $row_pos_name ; 
                        }

                        ?>   

                        <tr> 
                          <!-- <td><input type="checkbox" class="checkAll" onclick="toggle(this);" /></td> -->
                          <td>
                            <a href="./detail.php?PersonID=<?php echo $val['PersonID']  ;?>"><i class="la la-pencil-square-o" style="color:#0f1733;"></i></a>
                            <a href="#" data-toggle="modal" data-target="#modal_delete" onClick="setDataDelete('<?=$val['PersonID']?>','<?=$$data_HrtPrefix[$val['PrefixID']]." ".$val['PersonName']." ".$val['SurName']?>');">
                              <i class="la la-trash-o" style="color:#0f1733;"></i></a>
                            
                          </td>
                          <td><?php echo $i; ?></td> 
                          <td><?php echo $val['AirForceID']; ?></td> 
                          <td><?php echo $val['SoundDex']; ?></td> 
                          <td><?php echo $data_RankAbbr[$val['RankID']]; ?></td> 
                          <td><?php echo $data_HrtPrefix[$val['PrefixID']].$val['PersonName']; ?></td> 
                          <td><?php echo $val['SurName']; ?></td> 
                          <td>  
                            <?php 
                            foreach($data_pos_name as $key_pn => $val_pn ){ echo  $val_pn['HrtPosOrgAbbrName']; }
                            ?>
                          </td> 
                          <td align="center"><?php echo $arr_PersonSts[$val['PersonSts']]; ?></td>                     
                        </tr>

                        <?php
                        $i++;
                    } 
                }else if($_POST['action']){
                  ?>
                    <tr><td colspan="10" align="center"><h5 class="pt-1">ไม่พบข้อมูลที่ค้นหา</h5></td></tr>
                  <?php
                }
              ?>        
          </tbody>
      </table>
<!-- ---------- ---------- ---------- ---------- ---------- End DATA ---------- ---------- ---------- ---------- ---------- -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>

        </div>
      </div>
      
  </section>
<!-- ---------- ---------- ---------- ---------- ---------- MODEL DELETE ---------- ---------- ---------- ---------- ---------- -->
  <div class="modal fade text-left modal_custom1" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="modalSettingRegis"  aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <form action="../../Model/Ducklab/hist.inc.php" method="post">
          <div class="my-2 mx-2 px-2 py-1">
            <div class="text-center"> 
              <span class=" fontcolor2 "> <i class="la  la-trash-o la10"></i> </span>
              <div class="pb-3 pt-1"><h3> ต้องการลบ [ <span id="text_delete_is" style="color:#FF0000;"></span> ] ใช่หรือไม่ ?</h3></div>
              <input type="text" name="PersonID_Delete" id="PersonID_Delete" style="display:none" value="">
              <input type="text" name="Option_search1" style="display:none" value="<?php echo $_POST['HrtAirForceID']; ?>">
              <input type="text" name="Option_search2" style="display:none" value="<?php echo $_POST['HrtPerCardID']; ?>">
              <input type="text" name="Option_search3" style="display:none" value="<?php echo $_POST['HrtName']; ?>">
              <input type="text" name="Option_search4" style="display:none" value="<?php echo $_POST['HrtSoundDex']; ?>">
              <input type="text" name="Option_search5" style="display:none" value="<?php echo $_POST['set_HrtPersonTypeName']; ?>">
            </div>
            <div class="col-12 d-flex justify-content-center"> 
              <div class="row"> 
                <a href="#" class="btn btn-min-width mb-1 mr-1  round btn-success" role="button" data-dismiss="modal"> ยกเลิก </a>
                <span id="btn_delete"><button type="submit" class="btn btn-min-width mb-1 round btn-danger" name="goMove" value="submit_delete"><i class="fa fa-trash"></i> ยืนยัน </button></span> 
              </div>
            </div>
          </div> 
        </form>
      </div> 
    </div> 
</div>
<!-- ---------- ---------- ---------- ---------- ---------- footer ---------- ---------- ---------- ---------- ---------- -->
<?php include '../include/footer.php'; ?>
<script src="../../Controllers/Ducklab/duck.script.js"></script> 
<script src="../../Controllers/Ducklab/contents.script.js"></script> 
<script type="text/javascript">
      $(document).ready(function() { 
          $('#auto_panel').trigger('click');
    //    .click(function() {
    //     $("#frmsearch").submit();
    //    });
          $('#PersonTable').DataTable( { } );
      }); 

  function CheckForce10(condition_value,change_select_id) 
	{  
        $('#label_PerCardID').removeAttr('class', 'text-danger');
        $('#cHrtPerCardID').removeAttr('style', 'border:1px solid #FF0000');
        $('#error_PerCardID').hide();
			if($('#'+change_select_id).val().length == 10 || $('#'+change_select_id).val().length == 12) 
			{
          //$('#frmsearch').submit();
          $('#frmsearch').attr('onsubmit','return true;');
      }
      else
      {
          $('#label_AirForceID').attr('class', 'text-danger').appendTo('#label_AirForceID');
          $('#'+change_select_id).attr('style', 'border:1px solid #FF0000').appendTo('#'+change_select_id);
          $('#error_AirForceID').show();
					//alert('เลขบัตรประจำตัวราชการ 10 หรือ 12 หลักค่ะ');
          $('#'+change_select_id).val('');
          $('#'+change_select_id).focus();
          $('#frmsearch').attr('onsubmit','return false;');
      }
  }

  function CheckMember13(condition_value,change_select_id) 
	{  
        $('#label_AirForceID').removeAttr('class', 'text-danger');
        $('#cHrtAirForceID').removeAttr('style', 'border:1px solid #FF0000');
        $('#error_AirForceID').hide();
			var error_id13 = 0;
			if($('#'+change_select_id).val().length != 13) 
			{
          $('#label_PerCardID').attr('class', 'text-danger').appendTo('#label_PerCardID');
          $('#'+change_select_id).attr('style', 'border:1px solid #FF0000').appendTo('#'+change_select_id);
          $('#error_PerCardID').text("รหัสประชาชนไม่ครบ 13 หลักค่ะ").appendTo('#error_PerCardID');
          $('#error_PerCardID').show();
          //alert('รหัสประชาชนไม่ครบ 13 หลักค่ะ');
          error_id13++;
          $('#'+change_select_id).val('');
          $('#'+change_select_id).focus();
          $('#frmsearch').attr('onsubmit','return false;');
			}
			else 
			{
					for(i=0, sum=0; i < 12; i++)
					{   sum += parseFloat(condition_value.charAt(i))*(13-i);   }
					if((11-sum%11)%10!=parseFloat(condition_value.charAt(12)))
					{
            $('#label_PerCardID').attr('class', 'text-danger').appendTo('#label_PerCardID');
            $('#'+change_select_id).attr('style', 'border:1px solid #FF0000').appendTo('#'+change_select_id);
            $('#error_PerCardID').text("รหัสประชาชนไม่ถูกต้องค่ะ").appendTo('#error_PerCardID');
            $('#error_PerCardID').show();
						//alert('รหัสประชาชนไม่ถูกต้องค่ะ');
            error_id13++;
            $('#'+change_select_id).val('');
            $('#'+change_select_id).focus();
            $('#frmsearch').attr('onsubmit','return false;');
					}
			}
      if(error_id13 == 0){
        //$('#frmsearch').submit();
        $('#frmsearch').attr('onsubmit','return true;');
      }
  }

  function ClearForm_search(condition_value)
  { //,'set_HrtPosName'
      var list_object_M = new Array('cPersonType','cHrtAirForceID','cHrtPerCardID','cHrtName','cHrtSoundDex');
      for(clear_value = 0; clear_value <= 4; clear_value++){
        if(list_object_M[clear_value] == 'cPersonType'){
          $('#'+list_object_M[clear_value]).val('0').trigger("change");
          if(condition_value == 2) {
            $('#'+list_object_M[clear_value]).val($('#'+list_object_M[clear_value]).val()).remove();
          }
        }
        else if(list_object_M[clear_value] == 'cHrtAirForceID'){
          $('#label_AirForceID').removeAttr('class', 'text-danger');
          $('#'+list_object_M[clear_value]).removeAttr('style', 'border:1px solid #FF0000');
          $('#error_AirForceID').hide();
        }
        else if(list_object_M[clear_value] == 'cHrtPerCardID'){
          $('#label_PerCardID').removeAttr('class', 'text-danger');
          $('#'+list_object_M[clear_value]).removeAttr('style', 'border:1px solid #FF0000');
          $('#error_PerCardID').hide();
        }
        else{
          $('#'+list_object_M[clear_value]).attr('value', '').appendTo('#'+list_object_M[clear_value]);
        }
        $('#frmsearch').attr('onsubmit','return true;');
      }
  }

  function setDataDelete(condition_value1,condition_value2)
  {
    $('#PersonID_Delete').val(condition_value1);
    $('#text_delete_is').text(condition_value2).appendTo('#text_delete_is');
  }

</script>
