<?php
  //---------- ----- การปรับยศ    
  $sql_HrtRank_t14 = "SELECT rh.RankID,rh.RankType,rh.RankSeq,rh.SourceOfCommID,rh.CommDocNo,rh.RankBookDate,rh.RankDate,rh.Remark,r.HrtRankAbbrTh
  FROM HrtRankHist rh LEFT JOIN HrtRank r ON rh.RankID = r.HrtRankID
  WHERE rh.PersonID = '$PersonID' ORDER BY rh.RankDate DESC, rh.RankSeq ASC";
//  echo $sql_HrtRank_t14;
  $query_HrtRank_t14 = sqlsrv_query($conn, $sql_HrtRank_t14 );
  $data_HrtRank_t14 = array(); $nhudeaw = 0;
  while($row_HrtRank_t14 = sqlsrv_fetch_array($query_HrtRank_t14, SQLSRV_FETCH_ASSOC ))
  { $data_HrtRank_t14[] = $row_HrtRank_t14; $nhudeaw++; }
?>

<div class="tab-pane" id="posi_tab11" aria-labelledby="position-tab11">
  <div class="row">
    <div class="col-12">
      <div class="float-left">
        <a href="#" data-toggle="modal" data-target="#from_posi1" class="btn btn1 mb-1 round"><span class="fa fa-plus-circle"></span> เพิ่ม</a>
        <a href="#" class="btn btn1 mb-1 round"><span class="fa fa-trash-o"></span> ลบ</a>
      </div>
    </div>
  </div> 
  <table class="table table-striped table-borderless table-hover bootstrap-3 ">
    <thead>
      <tr align="center" style="background-color:#0f1733; color:whitesmoke;">
        <th><input type="checkbox" class="checkAll" onclick="toggle(this);" /></th>
        <th></th>
        <th>ลำดับที่</th>
        <th>สถานภาพ</th>
        <th>ชั้นยศ</th>
        <th>วันเดือนปีที่รับยศ</th>
      </tr>
    </thead>
    <tbody align="center">
    <?php
      $arr_RankType = array("0" => "ถอดถอน", "1" => "แต่งตั้ง", "NULL" => "-");
      if($data_HrtRank_t14){
      foreach($data_HrtRank_t14 as $key_rk14 => $val_rk14 ){
        $set_box_id = $val_rk14['RankID'];
     ?>
          <input type="text" id="ipt14_1_<?=$set_box_id?>" style="display:none" value="<?=$val_rk14['RankType']?>">
          <input type="text" id="ipt14_2_<?=$set_box_id?>" style="display:none" value="<?=$val_rk14['RankID']?>">
          <input type="text" id="ipt14_3_<?=$set_box_id?>" style="display:none" value="<?=$val_rk14['RankDate']?>">
          <input type="text" id="ipt14_4_<?=$set_box_id?>" style="display:none" value="<?=$val_rk14['SourceOfCommID']?>">
          <input type="text" id="ipt14_5_<?=$set_box_id?>" style="display:none" value="<?=$val_rk14['RankSeq']?>">
          <input type="text" id="ipt14_6_<?=$set_box_id?>" style="display:none" value="<?=$val_rk14['CommDocNo']?>">
          <input type="text" id="ipt14_7_<?=$set_box_id?>" style="display:none" value="<?=$val_rk14['RankBookDate']?>">
          <input type="text" id="ipt14_8_<?=$set_box_id?>" style="display:none" value="<?=$val_rk14['Remark']?>">
      <tr>
        <td><input type="checkbox" class="checkAll" /></td>
        <td>
          <a href="#" data-toggle="modal" data-target="#from_posi1" onClick="editHrtRank('<?=$set_box_id?>');"><i class="la la-pencil-square-o" style="color:#0f1733;"></i></a>
          <a href="#"><i class="la la-trash-o" style="color:#0f1733;"></i></a>
        </td>
        <td><?php echo ($key_rk14+1); ?></td>
        <td><?php echo $arr_RankType[$val_rk14['RankType']]; ?></td>
        <td><?php echo $val_rk14['HrtRankAbbrTh']; ?></td>
        <td><?php echo DateOnlyDisplay($val_rk14['RankDate'],8); ?></td>
      </tr>
      <?php 
      }
    }   ?>
    </tbody>
  </table>
  
  <div class="modal animated slideInUp text-left modal_custom1" id="from_posi1" tabindex="-1" role="dialog" aria-labelledby="modalSettingRegis"  aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <!---------- ---------- Start Content ---------- ---------- -->
          <div class="card-body">
              <div class="model-header" style="background-color:#0f1733;">
              <!-- -------------------- -->
                <div class="row">
                  <div class="col-md-11">
                    <h6 class="model-title text-white px-2 pt-2 py-1">บันทึก/แก้ไข การเลิ่อนยศ 
                      <?php echo $data_HrtRank['HrtRankAbbrTh']." ".$data_Person['PersonName']."   ".$data_Person['SurName']."  หมายเลขประจำตัว : ".$data_Person['AirForceID']; ?></h6>
                  </div>
                  <div class="col-md-1">
                    <h4 class="model-title text-white pt-2"><a data-dismiss="modal" onClick="ClearForm_t14();"><i class="fa fa-times-circle-o"></i></a></h4>
                  </div>
                </div>
              <!-- -------------------- -->
              </div>
              <div class="model-body">
              <!-- -------------------- -->
          <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">
              
              <div class="row"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">สถานภาพยศ :
                        <select class="select2 form-control" style="width: 100%;" name="RankType_t14" id="RankType_t14">
                          <option value="">โปรดเลือก สถานภาพยศ</option>
                          <option value="1">แต่งตั้ง</option>
                          <option value="0">ถอดถอน</option>
                        </select>
                      </div>
                    </div>
                  </div> 
                  <div class="col-md-6 ">
                    <div class="card-body">ชั้นยศ :
                      <select class="select2 form-control" style="width: 100%;" name="RankID_t14" id="RankID_t14"> 
                          <option>โปรดเลือก ชั้นยศ</option>
                          <?php
                              $sql_RankName = "SELECT HrtRankID,HrtRankNameTh From HrtRank where 1=1 ";
                              $query_RankName = sqlsrv_query($conn, $sql_RankName );
                              $data_RankName[] = array();
                              while($row_RankName = sqlsrv_fetch_array($query_RankName, SQLSRV_FETCH_ASSOC ))
                              { $data_RankName[] = $row_RankName ; }

                              if($data_RankName){
                                foreach($data_RankName as $key_RankName => $val_RankName ){
                                ?>
                                  <option value="<?php echo $val_RankName['HrtRankID']; ?>">
                                    <?php echo $val_RankName['HrtRankNameTh']; ?> </option>
                                  <?php
                                }
                              }   
                          ?>
                        </select>
                    </div>
                  </div> 
              </div>

              <div class="row"> 
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">วันที่มีผล  :</label>
                        <input type="text" class="form-control pickadate-translations" placeholder="" id="RankDate_t14" name="RankDate_t14" style="width: 80%;"
                          data-value="<?php echo GetToday('');?>" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">คำสั่ง :
                        <select class="select2 form-control" style="width: 100%;" name="SourceOfCommID_t14" id="SourceOfCommID_t14"> 
                          <option>โปรดเลือก คำสั่ง</option>
                          <?php
                              $sql_SOC = "SELECT SourceOfCommID,SourceOfCommName From HrtSourceOfComm where 1=1 ";
                              $query_SOC = sqlsrv_query($conn, $sql_SOC );
                              $data_SOC = array();
                              while($row_SOC = sqlsrv_fetch_array($query_SOC, SQLSRV_FETCH_ASSOC ))
                              { $data_SOC[] = $row_SOC ; }

                              if($data_SOC){
                                foreach($data_SOC as $key_SOC => $val_SOC ){ //HrtPosCode,HrtPosName 
                                ?>
                                  <option value="<?php echo $val_SOC['SourceOfCommID'];?>">
                                    <?php echo $val_SOC['SourceOfCommName']; ?> </option>
                                <?php
                                }
                              }   
                          ?>
                        </select>
                      </div>
                    </div>
                  </div> 
             </div>
                <br>
                <div class="row">
                  <div class="col-md-12 ">
                    <div class="card-block">
                      <div class="card-body "><h6>ตามคำสั่ง</h6></div>
                    </div>
                  </div>
              </div>

            <div class="row"> 
              <div class="col-md-3 ">
                <div class="card-body">ลำดับที่ :
                  <input class="input form-control" style="width: 100%;" name="RankSeq_t14" id="RankSeq_t14">
                </div>
              </div> 
                <div class="col-md-3 ">
                  <div class="card-body">เลขที่คำสั่ง :
                    <input class="input form-control" style="width: 100%;" name="CommDocNo_t14" id="CommDocNo_t14">
                  </div>
                </div> 
              <div class="col-md-6">
                <div class="card-block">
                  <div class="input-group col-12 datep">
                    <label class="label-control col-12 pl-0">ลง  :</label>
                    <input type="text" class="form-control pickadate-translations" placeholder="" id="RankBookDate_t14" name="RankBookDate_t14" style="width: 80%;"
                      data-value="<?php echo GetToday('');?>" />
                    <div class="input-group-append">
                      <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="row"> 
              <div class="col-md-12 ">
                <div class="card-body">หมายเหตุ :
                  <textarea class="form-control" name="Remark_t14" id="Remark_t14" style="width: 100%;" rows="4"></textarea>
                </div>
              </div> 
            </div>  

            <input type="text" name="PersonID_t14" id="PersonID_t14" style="display:none" value="<?=$PersonID?>">
          </form>
              <!-- -------------------- -->
              </div>
            <!-- <br>---------- เว้นระยะห่าง ---------- -->
              <div class="tab-content px-1 pt-1">
                <div class="form-actions center" align="center">
                  <button type="button" class="btn btn-success round btn-min-width mr-1 mb-1" id="submit" name="submit" data-target="#modalConfirm" onclick="insertOrganizationGroupType()">
                    <i class="fa fa-save"></i>&nbsp;บันทึก</button>
                  <button type="button" class="btn btn-danger round btn-min-width mr-1 mb-1" id="type-error" data-dismiss="modal" onClick="ClearForm_t14();">
                    <i class="fa fa-times-circle-o"></i>&nbsp;ยกเลิก</button>
                </div>
              </div>
          </div>
        <!---------- ---------- End ---------- ---------- -->
      </div> 
    </div> 
  </div>

</div>

<script type="text/javascript">
  function editHrtRank(condition_value)
  {
    $('#RankType_t14').val($('#ipt14_1_'+condition_value).val()).trigger("change");
    $('#RankID_t14').val($('#ipt14_2_'+condition_value).val()).trigger("change");
    $('#SourceOfCommID_t14').val($('#ipt14_4_'+condition_value).val()).trigger("change");
    $('#RankDate_t14').val($('#ipt14_3_'+condition_value).val());
    $('#RankSeq_t14').val($('#ipt14_5_'+condition_value).val());
    $('#CommDocNo_t14').val($('#ipt14_6_'+condition_value).val());
    $('#RankBookDate_t14').val($('#ipt14_7_'+condition_value).val());
    $('#Remark_t14').val($('#ipt14_8_'+condition_value).val());
  }

  function ClearForm_t14()
  {
      var list_object_M = new Array('RankType_t14','RankID_t14','SourceOfCommID_t14','RankDate_t14','RankSeq_t14','CommDocNo_t14','RankBookDate_t14','Remark_t14');
      for(clear_value = 0; clear_value <= 7; clear_value++){
        $('#'+list_object_M[clear_value]).val('');
        if(clear_value < 3){ $('#'+list_object_M[clear_value]).trigger("change"); }
      }
  }
</script>