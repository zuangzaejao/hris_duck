<div class="tab-pane" id="edu_tab14" aria-labelledby="education-tab14">
  <div class="row">
    <div class="col-12">
      <div class="float-left">
        <a href="#" class="btn btn1 mb-1 round"><span class="fa fa-plus-circle"></span> เพิ่ม</a>
        <a href="#" class="btn btn1 mb-1 round"><span class="fa fa-trash-o"></span> ลบ</a>
      </div>
    </div>
  </div> 
  <table class="table table-striped table-borderless table-hover bootstrap-3 ">
    <thead>
      <tr align="center" style="background-color:#0f1733; color:whitesmoke;">
        <th><input type="checkbox" class="checkAll" onclick="toggle(this);" /></th>
        <th></th>
        <th>ลำดับที่</th>
        <th>ลชทอ./เลขหมายรายงาน</th>
        <th>ประเภท</th>
        <th>จำพวกทหาร</th>
        <th>วันที่มีผล</th>
        <th>คำสั่ง</th>
        <th>เลขที่คำสั่ง</th>
        <th>ลง</th>
        <th>การเปลี่ยนแปลง</th>
      </tr>
    </thead>
    <tbody align="center">
      <tr>
        <td><input type="checkbox" class="checkAll" /></td>
        <td>
          <a href="#" data-toggle="modal" data-target="#from_edu4"><i class="la la-pencil-square-o" style="color:#0f1733;"></i></a>
          <a href="#"><i class="la la-trash-o" style="color:#0f1733;"></i></a>
        </td>
        <td>1</td>
        <td>0001</td>
        <td>เลขหมายรายงาน </td>
        <td>1 ก.ค.2558</td>
        <td>ทอ.(เฉพาะ)</td>
        <td>104/51</td>
        <td>1 ก.ค.2558</td>
        <td>แต่งตั้ง</td>
        <td>นบ.</td> 
      </tr>
    </tbody>
  </table>

  <div class="modal animated slideInUp text-left modal_custom1" id="from_edu4" tabindex="-1" role="dialog" aria-labelledby="modalSettingRegis"  aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <!---------- ---------- Start Content ---------- ---------- -->
          <div class="card-body">
              <div class="model-header" style="background-color:#0f1733;">
              <!-- -------------------- -->
                <div class="row">
                  <div class="col-md-11">
                    <h6 class="model-title text-white px-2 pt-2 py-1">บันทึก/แก้ไข เลขหมายความชำนาญทหารอากาศ
                      <?php echo $data_HrtRank['HrtRankAbbrTh']." ".$data_Person['PersonName']."   ".$data_Person['SurName']."  หมายเลขประจำตัว : ".$data_Person['AirForceID']; ?></h6>
                  </div>
                  <div class="col-md-1">
                    <h4 class="model-title text-white pt-2"><a data-dismiss="modal"><i class="fa fa-times-circle-o"></i></a></h4>
                  </div>
                </div>
              <!-- -------------------- -->
              </div>
              <div class="model-body">
              <!-- -------------------- -->

                <div class="row"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">คำสั่ง :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div> 
                  <div class="col-md-3 ">
                    <div class="card-body" id="NTT">ลำดับที่ในคำสั่ง :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div> 
                  <div class="col-md-3 ">
                    <div class="card-body" id="NTT">เลขที่คำสั่ง :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div>  
                </div> 

                <div class="row"> 
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">ลง :</label>
                        <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">วันที่มีผล :</label>
                        <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">ลชทอ.ประทวน/สัญญาบัตร :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div> 
                  <div class="col-md-6 ">
                  </div>
                </div> 

                <div class="row">  
                  <div class="col-md-12">
                    <div class="card-body ">การเปลี่ยนแปลง :
                      <div class="card-content">
                        <div class="card-body">
                          <div class="d-inline-block custom-control custom-radio mr-2">
                            <input type="radio" class="custom-control-input" name="Edu_hit_st1" id="edu_hst_1">
                            <label class="custom-control-label">แต่งตั้ง</label>
                          </div>
                          <div class="d-inline-block custom-control custom-radio mr-2">
                            <input type="radio" class="custom-control-input" name="Edu_hit_st1" id="edu_hst_2">
                            <label class="custom-control-label">เลื่อนระดับ</label>
                          </div>
                          <div class="d-inline-block custom-control custom-radio mr-2">
                            <input type="radio" class="custom-control-input" name="Edu_hit_st1" id="edu_hst_3">
                            <label class="custom-control-label">เปลี่ยนแปลง</label>
                          </div>
                          <div class="d-inline-block custom-control custom-button mr-2">
                          <a href="#" class="btn btn1 mb-1 round"><span ></span> ออกคำสั่งถอดถอน ลชทอ.</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

              <div class="row">
                  <div class="col-md-12 ">
                    <div class="card-block">
                      <div class="card-body "><h5>แต่งตั้ง</h5></div>
                    </div>
                  </div>
              </div>

              <div class="row"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">ประเภท :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div> 
                  <div class="col-md-3"> 
                    <div class="card-block">
                      <div class="card-body ">ลขทอ./ เลขหมายรายงาน :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div> 
              </div> 

              <div class="row"> 
                  <div class="col-md-2"> 
                    <div class="card-block">
                      <div class="card-body "> อักษรนำ :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div> 
                  <div class="col-md-2"> 
                    <div class="card-block">
                      <div class="card-body ">&nbsp;  
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div> 
                  <div class="col-md-2"> 
                    <div class="card-block">
                      <div class="card-body ">&nbsp;
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2"> 
                    <div class="card-block">
                      <div class="card-body "> อักษรตาม 
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2"> 
                    <div class="card-block">
                      <div class="card-body ">&nbsp;
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2"> 
                    <div class="card-block">
                      <div class="card-body ">&nbsp;
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div>
              </div>  
              
              <div class="row"> 
                  <div class="col-md-12 ">
                    <div class="card-body" id="NTT">หมายเหตุ :
                      <textarea class="form-control" id="tareaColor1" rows="4"></textarea>
                    </div>
                  </div> 
                </div> 
              <!-- -------------------- -->
              </div>
            <!-- <br>---------- เว้นระยะห่าง ---------- -->
              <div class="tab-content px-1 pt-1">
                <div class="form-actions center" align="center">
                  <button type="button" class="btn btn-success round btn-min-width mr-1 mb-1" id="submit" name="submit" data-target="#modalConfirm" onclick="insertOrganizationGroupType()">
                    <i class="fa fa-save"></i>&nbsp;บันทึก</button>
                  <button type="button" class="btn btn-danger round btn-min-width mr-1 mb-1" id="type-error" data-dismiss="modal">
                    <i class="fa fa-times-circle-o"></i>&nbsp;ยกเลิก</button>
                </div>
              </div>
          </div>
        <!---------- ---------- End ---------- ---------- -->
      </div> 
    </div> 
  </div>

</div>