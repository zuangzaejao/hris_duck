<div class="tab-pane" id="edu_tab12" aria-labelledby="education-tab12">

  <div class="row">
    <div class="col-12">
      <div class="float-left">
        <a href="#" class="btn btn1 mb-1 round"><span class="fa fa-plus-circle"></span> เพิ่ม</a>
        <a href="#" class="btn btn1 mb-1 round"><span class="fa fa-trash-o"></span> ลบ</a>
      </div>
    </div>
  </div> 
  <table class="table table-striped table-borderless table-hover bootstrap-3 ">
    <thead>
      <tr align="center" style="background-color:#0f1733; color:whitesmoke;">
        <th><input type="checkbox" class="checkAll" onclick="toggle(this);" /></th>
        <th></th>
        <th>ลำดับที่</th>
        <th>ประเภทการศึกษา</th>
        <th>หลักสูตร</th>
        <th>คุณวุฒิที่ได้รับ</th>
        <th>สาขาวิชา</th>
        <th>รุ่น</th>
        <th>ปีจบ</th>
      </tr>
    </thead>
    <tbody align="center">
      <tr>
        <td><input type="checkbox" class="checkAll" /></td>
        <td>
          <a href="#" data-toggle="modal" data-target="#from_edu2"><i class="la la-pencil-square-o" style="color:#0f1733;"></i></a>
          <a href="#"><i class="la la-trash-o" style="color:#0f1733;"></i></a>
        </td>
        <td>1</td>
        <td>ก่อนรับราชการ</td>
        <td>โรงเรียนนายเรืออากาศเยอรมัน </td>
        <td>ประกาศนียบัตร</td>
        <td>-</td>
        <td>21</td>
        <td>2561</td>
      </tr>
    </tbody>
  </table>

  <div class="modal animated slideInUp text-left modal_custom1" id="from_edu2" tabindex="-1" role="dialog" aria-labelledby="modalSettingRegis"  aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <!---------- ---------- Start Content ---------- ---------- -->
          <div class="card-body">
              <div class="model-header" style="background-color:#0f1733;">
              <!-- -------------------- -->
                <div class="row">
                  <div class="col-md-11">
                    <h6 class="model-title text-white px-2 pt-2 py-1">บันทึก/แก้ไข การศึกษาระหว่างรับราชการ 
                      <?php echo $data_HrtRank['HrtRankAbbrTh']." ".$data_Person['PersonName']."   ".$data_Person['SurName']."  หมายเลขประจำตัว : ".$data_Person['AirForceID']; ?></h6>
                  </div>
                  <div class="col-md-1">
                    <h4 class="model-title text-white pt-2"><a data-dismiss="modal"><i class="fa fa-times-circle-o"></i></a></h4>
                  </div>
                </div>
              <!-- -------------------- -->
              </div>
              <div class="model-body">
              <!-- -------------------- -->
                <div class="row">  
                  <div class="col-md-12">
                    <div class="card-body ">สถานะ :
                      <div class="card-content">
                        <div class="card-body">
                          <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input" name="Edu_hit_st1" id="edu_hst_1">
                            <label class="custom-control-label">สำเร็จ</label>
                          </div>
                          <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input" name="Edu_hit_st1" id="edu_hst_2">
                            <label class="custom-control-label">ไม่สำเร็จ</label>
                          </div>
                          <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input" name="Edu_hit_st1" id="edu_hst_3">
                            <label class="custom-control-label">กำลังศึกษา</label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">ประเภทหลักสูตร :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div> 
                  <div class="col-md-6 ">
                    <div class="card-block">
                      <div class="card-body ">ประเภทการศึกษา :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>                
                      </div>
                    </div>
                  </div> 
                </div> 

                <div class="row"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">ชื่อหลักสูตร :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div> 
                  <div class="col-md-6 ">
                    <div class="card-block">
                      <div class="card-body ">ระดับการศึกษา :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>                
                      </div>
                    </div>
                  </div> 
                </div> 

                <div class="row"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">คุณวุฒิที่ได้รับ :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div> 
                  <div class="col-md-6 ">
                    <div class="card-block">
                      <div class="card-body ">สาขา :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>                
                      </div>
                    </div>
                  </div> 
                </div> 

                <div class="row"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">อนุสาขา :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div> 
                  <div class="col-md-6 ">
                    <div class="card-block">
                      <div class="card-body ">ทาง/กลุ่มวิชา :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>                
                      </div>
                    </div>
                  </div> 
                </div> 

                <div class="row"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">วิชาเอก :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div> 
                  <div class="col-md-6 ">
                    <div class="card-block">
                      <div class="card-body ">แขนงวิชา :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>                
                      </div>
                    </div>
                  </div> 
                </div> 

                <div class="row"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">สถาบันการศึกษา :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div> 
                  <div class="col-md-6 ">
                    <div class="card-block">
                      <div class="card-body ">กลุ่มสถาบันการศึกษา :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>                
                      </div>
                    </div>
                  </div> 
                </div> 

                <div class="row"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">ประเทศ :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div> 
                  <div class="col-md-6 ">
                    <div class="card-block">
                      <div class="card-body ">ทุนการศึกษา :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>                
                      </div>
                    </div>
                  </div> 
                </div> 

                <div class="row">  
                  <div class="col-md-6">
                    <div class="card-body ">ทุน :
                      <div class="card-content">
                        <div class="card-body">
                          <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input" name="type_budget" id="type_bg_1">
                            <label class="custom-control-label">ผูกพัน</label>
                          </div>
                          <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input" name="type_budget" id="type_bg_2">
                            <label class="custom-control-label">ไม่ผูกพัน</label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3"> 
                    <div class="card-block">
                      <div class="card-body ">ปีที่สำเร็จการศึกษา :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div> 
                  <div class="col-md-3">
                    <div class="card-body" id="NTT">รุ่น :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div> 
                </div>

                <div class="row"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">ผลัด :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div> 
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">วันที่เริ่มศึกษา  :</label>
                        <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> 

                <div class="row"> 
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">วันที่สำเร็จการศึกษา  :</label>
                        <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">วันที่รายงานตัว  :</label>
                        <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> 

                <div class="row"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">ประเภทผลการศึกษา :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div> 
                  <div class="col-md-3 ">
                    <div class="card-body" id="NTT">จำนวนผู้เข้ารับการศึกษา :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div> 
                  <div class="col-md-3 ">
                    <div class="card-body" id="NTT">สอบได้ลำดับที่ :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div> 
                </div> 

                <div class="row"> 
                  <div class="col-md-3 ">
                    <div class="card-body" id="NTT">สอบได้คะแนนร้อยละ :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div> 
                  <div class="col-md-3 ">
                    <div class="card-body" id="NTT">สอบได้เกรดเฉลี่ย :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">ผลการประเมินการศึกษา :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div> 
                </div> 

                <div class="row"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">ผลการประเมินความประพฤติ :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div> 
                  <div class="col-md-6 ">
                    <div class="card-body" id="NTT">คะแนนประเมิณคุณลักษณะส่วนบุคคล :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div> 
                </div> 

                <div class="row"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body "> อนุมัติ ผบ.ทอ. :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div> 
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">รับคำสั่งฯ เมื่อ  :</label>
                        <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> 

                <div class="row"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">ตาม :
                        <select class="select2 form-control" style="width: 100%;">
                          <option></option>
                        </select>
                      </div>
                    </div>
                  </div> 
                  <div class="col-md-6 ">
                    <div class="card-body" id="NTT">ที่ :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div> 
                </div> 

                <div class="row"> 
                  <div class="col-md-6">
                    <div class="card-block">
                      <div class="input-group col-12 datep">
                        <label class="label-control col-12 pl-0">ลง :</label>
                        <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                        <div class="input-group-append">
                          <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 ">
                    <div class="card-body" id="NTT">แนบไฟล์หลักฐาน :
                      <input type="file" name="filUpload[]" id="filUpload" multiple="multiple" >
                    </div>
                  </div> 
                </div> 

                <div class="row"> 
                  <div class="col-md-12 ">
                    <div class="card-body" id="NTT">หลักฐาน :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div> 
                </div> 

                <div class="row"> 
                  <div class="col-md-12 ">
                    <div class="card-body" id="NTT">หมายเหตุ :
                      <textarea class="form-control" id="tareaColor1" rows="4"></textarea>
                    </div>
                  </div> 
                </div> 
              <!-- -------------------- -->
              </div>
            <!-- <br>---------- เว้นระยะห่าง ---------- -->
              <div class="tab-content px-1 pt-1">
                <div class="form-actions center" align="center">
                  <button type="button" class="btn btn-success round btn-min-width mr-1 mb-1" id="submit" name="submit" data-target="#modalConfirm" onclick="insertOrganizationGroupType()">
                    <i class="fa fa-save"></i>&nbsp;บันทึก</button>
                  <button type="button" class="btn btn-danger round btn-min-width mr-1 mb-1" id="type-error" data-dismiss="modal">
                    <i class="fa fa-times-circle-o"></i>&nbsp;ยกเลิก</button>
                </div>
              </div>
          </div>
        <!---------- ---------- End ---------- ---------- -->
      </div> 
    </div> 
  </div>

</div>