<?php //---------- header
error_reporting(~E_NOTICE); 
    require_once "../../config.php";
    require_once '../../Model/Ducklab/func.php';
    require_once '../include/header.php';
  
    $menu1 ="HRTOFFICIAL";
    $menu2 ="PROFILE"; 
?>

<!-- menu -->
<?php include '../include/menu.php'; ?>

<?php  

  if( $_POST['action']== "search")
  { 
      $where ="";
      $check_ops = 0;
        //---------- OPS-1
          if(!empty($_POST['HrtAirForceID'])){
              $check_ops = 1;
              $where.= " AirForceID = '{$_POST['HrtAirForceID']}' ";
          }
        //---------- OPS-2
          if(!empty($_POST['HrtPerCardID']))
          {
              if($check_ops > 0){
                  $check_ops = 2;
                  $where.= " AND PerCardID = '{$_POST['HrtPerCardID']}' ";
              } else {
                  $check_ops = 1;
                  $where.= " PerCardID = '{$_POST['HrtPerCardID']}' ";
              }
          }
        //---------- OPS-3
          if(!empty($_POST['HrtName']))
          {
              if($_POST['HrtName'][0] == '*' && $check_ops > 0){ 
                  $check_ops = 3;
                  $where.= " AND PersonName LIKE '%".substr($_POST['HrtName'],1)."%' ";
              } elseif($_POST['HrtName'][0] == '*'){
                  $check_ops = 1;
                  $where.= " PersonName LIKE  '%".substr($_POST['HrtName'],1)."%' ";
              } elseif(substr($_POST['HrtName'],-1) == '*' && $check_ops > 0){
                  $check_ops = 3;
                  $exp_sn = explode("*", $_POST['HrtName']);
                  $where.= " AND SurName LIKE '%".$exp_sn[0]."%' ";
              } elseif(substr($_POST['HrtName'],-1) == '*'){
                  $check_ops = 1;
                  $exp_sn = explode("*", $_POST['HrtName']);
                  $where.= " SurName LIKE '%".$exp_sn[0]."%' ";
              }
          }
        //---------- OPS-4
          if(!empty($_POST['HrtSoundDex']))
          {
              if($check_ops > 0){
                  $check_ops = 4;
                  $where.= " AND SoundDex LIKE  '%".$_POST['HrtSoundDex']."%' ";
              } else {
                  $check_ops = 1;
                  $where.= " SoundDex LIKE  '%".$_POST['HrtSoundDex']."%' ";
              }
          }
        //---------- OPS-5
          if(!empty($_POST['set_HrtPersonTypeName']))
          {
              if($check_ops > 0){
                  $check_ops = 5;
                  $where.= " AND PersonTypeID = '".$_POST['set_HrtPersonTypeName']."' ";
              } else {
                  $check_ops = 1;
                  $where.= " PersonTypeID = '".$_POST['set_HrtPersonTypeName']."' ";
              }
          }
  
          // if( !empty($_POST['HrtRankNameTh']) ){
          //    $check_ops=1;
          //    $where.= " AND HrtRankNameTh LIKE  '%".$_POST['HrtRankNameTh']."%' ";
          // }
  
          // if( !empty($_POST['HrtRankNameTh']) ){
          //    $check_ops=1;
          //    $where.= " AND HrtRankNameTh LIKE  '%".$_POST['HrtRankNameTh']."%' ";
          // }
  

          if($check_ops > 0){
              $sql_search ="SELECT PersonID,AirForceID,SoundDex,RankID,PersonName,SurName,PosPositionID,PersonSts
              FROM HrtPerson where ".$where." 
              ORDER BY PersonSts DESC, RankID ASC, PersonName ASC";

              $query_search = sqlsrv_query($conn, $sql_search );
              $data_search = array();

              while($row=sqlsrv_fetch_array($query_search, SQLSRV_FETCH_ASSOC )){
                  $data_search[] = $row ; 
              }
              //echo $sql;
          }else{
              
          }
      //---------- ---------- ---------- ----------//
  }
/*
    $sql04 = "SELECT HrtPosCode,HrtPosName From HrtPos where 1=1 ";

    $query04 = sqlsrv_query($conn, $sql04 );
    $data04=array();

    while($row04=sqlsrv_fetch_array($query04, SQLSRV_FETCH_ASSOC )){
        $data04[] = $row04 ; 
    }
*/
?>

<script type="text/javascript">
</script>

  <section>

      <div class="app-content content">
        <div class="content-wrapper">

          <div class="content-header row">
            <div class="content-header-left col-12 mb-2">
              <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
              <h3 class="content-header-title">ข้อมูลบุคคล <?php //echo $sql_search; ?></h3>
            </div>
          </div>

          <div class="content-body">            
            <section id="bootstrap3">
              <div class="row">
                <div class="col-12">
                  <div class="card">
                    <div class="card-content collapse show">
                      <div class="card-body card-dashboard">
                        <p class="card-text"></p>
                        <nav aria-label="breadcrumb">
                          <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../home/index.php">งานประวัติข้าราชการ</a></li>
                            <li class="breadcrumb-item active" aria-current="page">ระบบค้นหาข้อมูลบุคคล</li>
                          </ol>
                        </nav>
                        <div class="content-body">
                          <section class="horizontal-grid" id="horizontal-grid">
                            <div class="row">
                              <div class="col-md-12">
                                <div class="box_h1 card">
                                  <div class="card-header card-head-inverse">
                                    <h4 class="card-title text-white"> ค้นหา </h4> 
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                      <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                                      </ul>
                                    </div>
                                  </div>

                                  <div class="card-content collapse show">
                                    <div class="card-body">
          <!----------------------------------------------------------------------------------------------------------------->
          <form action="" method="post" id="frmsearch">
            
              <div class="form-body">
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">เลขบัตรประจำตัวราชการ:
                      <input type="text" class="form-control"  placeholder=" " name="HrtAirForceID" id="cHrtAirForceID"
                        autocomplete=off value="<?php echo $_POST['HrtAirForceID'];?>" >
                    </div>
                  </div>
        
                  <div class="col-md-4">
                    <div class="form-group">เลขประจำตัวบัตรประชาชน:
                      <input type="text" class="form-control" placeholder="" name="HrtPerCardID" id="cHrtPerCardID"
                        autocomplete=off value="<?php echo $_POST['HrtPerCardID'];?>" OnChange="CheckMember13(this.value,'cHrtPerCardID');">
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="form-group">ชื่อ - นามสกุล:
                      <input type="text" class="form-control" placeholder=" " name="HrtName" id="cHrtName" 
                        autocomplete=off value="<?php echo $_POST['HrtName'];?>">
                    </div>
                  </div> 
                </div>                    

                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">หมายเลขแฟ้มประวัติ:
                      <input type="text" class="form-control" placeholder="" name="HrtSoundDex" id="cHrtSoundDex" 
                        autocomplete=off value="<?php echo $_POST['HrtSoundDex'];?>">
                    </div>
                  </div>              

                  <div class="col-md-4">
                    <div class="form-group">ประเภทกำลังพล:
                      <select  class="select2 form-control" style="width: 100%;" name="set_HrtPersonTypeName" id="cHrtPersonTypeName" > 
                          <option value="0">  กรุณาเลือกประเภทกำลังพล  </option>
                          <?php
                              $sql_4ops_5 = "SELECT HrtPersonTypeID,HrtPersonTypeName From HrtPersonType where 1=1 ";

                                  $query_4ops_5 = sqlsrv_query($conn, $sql_4ops_5 );
                                  $data_4ops_5=array();
                              
                                  while($row_4ops_5=sqlsrv_fetch_array($query_4ops_5, SQLSRV_FETCH_ASSOC )){
                                      $data_4ops_5[] = $row_4ops_5 ; 
                                  } 
                              if($data_4ops_5){
                                  foreach($data_4ops_5 as $key => $val ){ //HrtPosCode,HrtPosName 
                                  ?>
                                <option value="<?php echo $val['HrtPersonTypeID']; ?>"
                                <?php if( $val['HrtPersonTypeID'] == $_POST['set_HrtPersonTypeName']) { echo "selected"; } ?> > <?php echo $val['HrtPersonTypeName']; ?>
                                </option>
                                  <?php
                                  }
                              } 
                          ?> 
                      </select>
                    </div>
                  </div>        

                  <!-- <div class="col-md-4">
                    <div class="form-group">ตำแหน่ง:
                      <select  class="select2 form-control"style="width: 100%;" name="set_HrtPosName" id="set_HrtPosName" >
                          <option value="">  กรุณาเลือกประเภทตำแหน่ง  </option>
                      </select>
                    </div>
                  </div> -->
                </div>

                <div class="text-center">
                  <input type="hidden" name="action" value="search">
                  <button type="button" class="btn round btn1 btncustom1 mr-1 " id="btnsubmit" > 
                    <i class="fa fa-search"></i> ค้นหา </button>
                  <button type="reset" class="btn round btn1 btncustom1" name="repeat" id="repeat" onClick="ClearForm_search();">
                    <i class=" fa fa-repeat"></i> ล้างค่า </button>
                </div>
          </form>
          <!----------------------------------------------------------------------------------------------------------------->
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </div>
                          </section>
                        </div>

      <br>
      <a href="#" class="btn btn-social btn-min-width mb-1" style="background-color:#0f1733; color:white;">
          <span class="la la-plus-circle" style="color:white; font-weight: bold;font-size: 18px"></span> เพิ่ม </a>
      <a href="#" class="btn btn-social btn-min-width mb-1" style="background-color:#0f1733; color:white;">
          <span class="la la-trash-o" style="color:white; font-weight: bold;font-size: 18px"></span> ลบ </a>
<!-- ---------- ./create.php---------- ./delete.php---------- ---------- ---------- Show DATA ---------- ---------- ---------- ---------- ---------- -->
      <br><br>
      <table id="PersonTable"  class="table table-striped table-borderless table-hover table_custom1">
          <thead>
            <tr>
              <th><input type="checkbox" class="checkAll" onclick="toggle(this);" /></th>
              <th></th>
              <th>ลำดับที่</th>
              <th>เลขประจำตัวข้าราชการ</th>
              <th>หมายเลขแฟ้ม</th>
              <th>ยศ</th>
              <th>ชื่อ</th>
              <th>นามสกุล</th>
              <th>ตำแหน่ง</th>
              <th>สถานะ</th>
            </tr>
          </thead>
          <tbody >
              <?php 
                if($data_search)
                {
                    $arr_PersonSts = array("N" => "พ้นจากราชการ", "Y" => "ยังอยู่ในราชการ");
                    $i=1; 
                    foreach( $data_search as $key => $val )
                    {
                        $sql_rank_name = " SELECT HrtRankAbbrTh 
                                            FROM HrtRank
                                            WHERE HrtRankID = '". $val['RankID']."' ";

                        $query_rank_name = sqlsrv_query($conn, $sql_rank_name );
                        $data_rank_name = array();

                        while($row_rank_name = sqlsrv_fetch_array($query_rank_name, SQLSRV_FETCH_ASSOC )){
                          $data_rank_name[] = $row_rank_name ; 
                        }
                        
                        // $sql_pos_name = " SELECT HrtPosition.HrtPosOrgAbbrName 
                        //                   FROM HrtPosition left join HrtPosPerson
                        //                   ON HrtPosition.HrtPositionID = HrtPosPerson.HrtPositionID AND HrtPosition.HrtPosNum = HrtPosPerson.HrtPosNo
                        //                   WHERE HrtPosPerson.HrtPersonID = '". $val['PersonID']."' ";
                        $sql_pos_name = " SELECT HrtPosOrgAbbrName 
                                          FROM HrtPosition
                                          WHERE HrtPositionID = '". $val['PosPositionID']."' ";

                        $query_pos_name = sqlsrv_query($conn, $sql_pos_name );
                            if( $query_pos_name === false ) {
                              die( print_r( sqlsrv_errors(), true));
                            }
                        $data_pos_name = array();

                        while($row_pos_name = sqlsrv_fetch_array($query_pos_name, SQLSRV_FETCH_ASSOC )){
                            $data_pos_name[] = $row_pos_name ; 
                        }

                        ?>   

                        <tr> 
                          <td><input type="checkbox" class="checkAll" onclick="toggle(this);" /></td>
                          <td>
                            <a href="./detail.php?PersonID=<?php echo $val['PersonID']  ;?>"><i class="la la-pencil-square-o" style="color:#0f1733;"></i></a>
                            <a href="./delete.php"><i class="la la-trash-o" style="color:#0f1733;"></i></a>
                          </td>
                          <td><?php echo $i; ?></td> 
                          <td><?php echo $val['AirForceID']; ?></td> 
                          <td><?php echo $val['SoundDex']; ?></td> 
                          <td>
                            <?php
                            foreach($data_rank_name as $key_rn => $val_rn) { echo  $val_rn['HrtRankAbbrTh']; } 
                            ?>
                          </td> 
                          <td><?php echo $val['PersonName']; ?></td> 
                          <td><?php echo $val['SurName']; ?></td> 
                          <td>  
                            <?php 
                            foreach($data_pos_name as $key_pn => $val_pn ){ echo  $val_pn['HrtPosOrgAbbrName']; }
                            ?>
                          </td> 
                          <td align="center"><?php echo $arr_PersonSts[$val['PersonSts']]; ?></td>                     
                        </tr>

                        <?php
                        $i++;
                    } 
                }else if($_POST['action']){
                  ?>
                    <tr><td colspan="10" align="center"><h5 class="pt-1">ไม่พบข้อมูลที่ค้นหา</h5></td></tr>
                  <?php
                }
              ?>        
          </tbody>
      </table>
<!-- ---------- ---------- ---------- ---------- ---------- End DATA ---------- ---------- ---------- ---------- ---------- -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>

        </div>
      </div>

  </section>
  
  <!-- footer -->
<?php include '../include/footer.php'; ?>
<script src="../../Controllers/Ducklab/duck.script.js"></script> 
<script src="../../Controllers/Ducklab/contents.script.js"></script> 
<script type="text/javascript">
    $(document).ready(function() { 

      $("#btnsubmit").click(function() {
      // alert( "Handler for .click() called." );
         $("#frmsearch").submit();
      });
     
     $('#PersonTable').DataTable( {
   
      } );
    }); 

    function CheckMember13(condition_value,change_select_id) 
	{  
			var error_id13 = 0;
			if($('#'+change_select_id).val().length != 13) 
			{
					alert('รหัสประชาชนไม่ครบ 13 หลักค่ะ');
          $('#'+change_select_id).val('');
          $('#'+change_select_id).focus();
			}
			else 
			{
					for(i=0, sum=0; i < 12; i++)
					{   sum += parseFloat(condition_value.charAt(i))*(13-i);   }
					if((11-sum%11)%10!=parseFloat(condition_value.charAt(12)))
					{
						alert('รหัสประชาชนไม่ถูกต้องค่ะ');
            $('#'+change_select_id).val('');
            $('#'+change_select_id).focus();
					}
			}
  }

  function ClearForm_search()
  { //,'set_HrtPosName'
      var list_object_M = new Array('cHrtPersonTypeName','cHrtAirForceID','cHrtPerCardID','cHrtName','cHrtSoundDex');
      for(clear_value = 0; clear_value <= 4; clear_value++){
        $('#'+list_object_M[clear_value]).val('');
        $('#'+list_object_M[clear_value]).attr('value', '').appendTo('#'+list_object_M[clear_value]);
        if(clear_value == 0){ $('#'+list_object_M[clear_value]).val('0').trigger("change"); }
      }
  }
</script>
