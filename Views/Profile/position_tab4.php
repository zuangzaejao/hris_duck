<?php
  //---------- ----- เครื่องราช
  $sql_insigna = "SELECT  ish.PersonID,ish.InsignaCode,ish.GivInsignaDate,ish.GaetteBook,ish.GazetteSection,ish.GazettePage,
  ish.GazetteSeq,ish.GazetteDate,ish.RecvInsignaDate,ish.RetuISInsignaDate,isn.InsignaName  
  FROM HrtInsignaHist ish LEFT JOIN HrtInsigna isn ON (ish.InsignaCode = isn.InsignaCode)
  WHERE ish.PersonID = '$PersonID' ";
  $query_insigna = sqlsrv_query($conn, $sql_insigna );
  $data_insigna = array();
  while($row_insigna =sqlsrv_fetch_array($query_insigna, SQLSRV_FETCH_ASSOC ))
  { $data_insigna[] = $row_insigna; }

  //echo $sql_insigna;
?>
<div class="tab-pane" id="posi_tab14" aria-labelledby="position-tab14">
  <div class="row">
    <div class="col-12">
      <div class="float-left">
      <a href="#" data-toggle="modal" data-target="#from_posi4" class="btn btn1 mb-1 round"><span class="fa fa-plus-circle"></span> เพิ่ม</a>
        <a href="#" class="btn btn1 mb-1 round"><span class="fa fa-trash-o"></span> ลบ</a>
      </div>
    </div>
  </div> 
  <table class="table table-striped table-borderless table-hover bootstrap-3 ">
    <thead>
    <tr align="center" style="background-color:#0f1733; color:whitesmoke;">
        <th><input type="checkbox" class="checkAll" onclick="toggle(this);" /></th>
        <th></th>
        <th>ลำดับที่</th>
        <th>ชั้นเครื่องราชที่ได้รับ</th>
        <th>วันที่พระราชทาน</th>
        <th>เล่มที่</th>
        <th>ตอนที่</th>
        <th>หน้าที่</th>
        <th>ตอนที่</th>
        <th>ลงวันที่</th>
        <th>วันที่ได้รับ</th>
        <th>หลักฐานที่ได้รับ</th>
        <th>วันที่ส่ง</th>
      </tr>
    </thead>
    <tbody align="center">
    <?php if($data_insigna){
      foreach($data_insigna as $key_isn => $val_isn ){
        $set_box_id = $val_isn['InsignaCode'];
     ?>   
          <input type="text" id="ipt17_1_<?=$set_box_id?>" style="display:none" value="<?=$val_isn['InsignaCode']?>">
          <input type="text" id="ipt17_2_<?=$set_box_id?>" style="display:none" value="<?=$val_isn['GivInsignaDate']?>">
          <input type="text" id="ipt17_3_<?=$set_box_id?>" style="display:none" value="<?=$val_isn['GaetteBook']?>">
          <input type="text" id="ipt17_4_<?=$set_box_id?>" style="display:none" value="<?=$val_isn['GazetteSection']?>">
          <input type="text" id="ipt17_5_<?=$set_box_id?>" style="display:none" value="<?=$val_isn['GazettePage']?>">
          <input type="text" id="ipt17_6_<?=$set_box_id?>" style="display:none" value="<?=$val_isn['GazetteSeq']?>">
          <input type="text" id="ipt17_7_<?=$set_box_id?>" style="display:none" value="<?=$val_isn['GazetteDate']?>">
          <input type="text" id="ipt17_8_<?=$set_box_id?>" style="display:none" value="<?=$val_isn['RecvInsignaDate']?>">
          <input type="text" id="ipt17_9_<?=$set_box_id?>" style="display:none" value="<?=$val_isn['RetuISInsignaDate']?>">
      <tr>
        <td><input type="checkbox" class="checkAll" /></td>
        <td>
        <a href="#" data-toggle="modal" data-target="#from_posi4" onClick="editHrtInsigna('<?=$set_box_id?>');"><i class="la la-pencil-square-o" style="color:#0f1733;"></i></a>
          <a href="#"><i class="la la-trash-o" style="color:#0f1733;"></i></a>
        </td>
        <td><?php echo ($key_isn+1)." ".$val_isn['InsignaCode']; ?></td>
        <td><?php echo $val_isn['InsignaName']; ?></td>
        <td><?php echo $val_isn['GivInsignaDate']; ?></td>
        <td><?php echo $val_isn['GaetteBook']; ?></td>
        <td><?php echo $val_isn['GazetteSection']; ?></td>
        <td><?php echo $val_isn['GazettePage']; ?></td>
        <td><?php echo $val_isn['GazetteSeq']; ?></td>
        <td><?php echo $val_isn['GazetteDate']; ?></td>
        <td><?php echo $val_isn['RecvInsignaDate']; ?></td>
        <td><?php //echo $val_isn['RecvInsignaDate']; ?></td>
        <td><?php echo $val_isn['RetuISInsignaDate']; ?></td>
      </tr>
      <?php
      }
    }   ?>
    </tbody>
  </table>
  <div class="modal animated slideInUp text-left modal_custom1" id="from_posi4" tabindex="-1" role="dialog" aria-labelledby="modalSettingRegis"  aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <!---------- ---------- Start Content ---------- ---------- -->
          <div class="card-body">
              <div class="model-header" style="background-color:#0f1733;">
              <!-- -------------------- -->
                <div class="row">
                  <div class="col-md-11">
                    <h6 class="model-title text-white px-2 pt-2 py-1">บันทึก/แก้ไข เครื่องราชอิสริยาภรณ์ 
                      <?php echo $data_HrtRank['HrtRankAbbrTh']." ".$data_Person['PersonName']."   ".$data_Person['SurName']."  หมายเลขประจำตัว : ".$data_Person['AirForceID']; ?></h6>
                  </div>
                  <div class="col-md-1">
                    <h4 class="model-title text-white pt-2"><a data-dismiss="modal" onClick="ClearForm_t17();"><i class="fa fa-times-circle-o"></i></a></h4>
                  </div>
                </div>
              <!-- -------------------- -->
              </div>
              <div class="model-body">
              <!-- -------------------- -->
        <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">

              <div class="row"> 
                  <div class="col-md-6"> 
                    <div class="card-block">
                      <div class="card-body ">ชั้นเครื่องราชที่ได้รับ :
                      <select class="select2 form-control" style="width: 100%;" name="InsignaName_t17" id="InsignaName_t17"> 
                          <option value="">โปรดเลือก </option>
                          <?php
                              $sql_Insigna2 = "SELECT InsignaCode,InsignaName From HrtInsigna where 1=1 ";
                              $query_Insigna2 = sqlsrv_query($conn, $sql_Insigna2 );
                              $data_Insigna2[] = array();
                              while($row_Insigna2 = sqlsrv_fetch_array($query_Insigna2, SQLSRV_FETCH_ASSOC ))
                              { $data_Insigna2[] = $row_Insigna2 ; }

                              if($data_Insigna2){
                                foreach($data_Insigna2 as $key_Insigna2 => $val_Insigna2 ){
                                ?>
                                  <option value="<?php echo $val_Insigna2['InsignaCode']; ?>">
                                    <?php echo $val_Insigna2['InsignaName']; ?> </option>
                                  <?php
                                }
                              }   
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6"> 
                    
                  </div>
              </div> 

              <br>
              <div class="row">
                <div class="col-md-12 ">
                  <div class="card-block">
                    <div class="card-body "><h6>ตามคำสั่ง</h6></div>
                  </div>
                </div>
              </div>

              <div class="row"> 
                <div class="col-md-3 ">
                  <div class="card-body" >เล่มที่ :
                    <input class="input form-control" style="width: 100%;" name="GaetteBook_t17" id="GaetteBook_t17">
                  </div>
                </div> 
                <div class="col-md-3 ">
                  <div class="card-body" >ตอนที่ :
                    <input class="input form-control" style="width: 100%;"name="GazetteSection_t17" id="GazetteSection_t17">
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="card-body" >หน้าที่ :
                    <input class="input form-control" style="width: 100%;"name="GazettePage_t17" id="GazettePage_t17">
                  </div>
                </div> 
                <div class="col-md-3 ">
                  <div class="card-body" >ลำดับที่ :
                    <input class="input form-control" style="width: 100%;" name="GazetteSeq_t17" id="GazetteSeq_t17">
                  </div>
                </div>  
            </div>

            <div class="row"> 
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="input-group col-12 datep">
                      <label class="label-control col-12 pl-0">ลงวันที่  :</label>
                      <input type="text" class="form-control pickadate-translations" placeholder="" id="GazetteDate_17" name="GazetteDate_17" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                      <div class="input-group-append">
                        <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="card-block">
                    <div class="input-group col-12 datep">
                      <label class="label-control col-12 pl-0">วันที่พระราชทานเครื่องราชฯ  :</label>
                      <input type="text" class="form-control pickadate-translations" placeholder="" id="GivInsignaDate_t17" name="GivInsignaDate_t17" style="width: 80%;" 
                      data-value="<?php echo GetToday('');?>" />
                      <div class="input-group-append">
                        <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
           
            <br>
            <div class="row"> 
                  <div class="col-md-12 ">
                    <div class="card-body" >หลักฐานอื่นๆ :
                    <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div> 
            </div>

            <div class="row"> 
              <div class="col-md-12 ">
                <div class="card-body" >แนบไฟล์หลักฐาน :
                  <input type="file" name="filUpload[]" id="filUpload" multiple="multiple" >
                </div>
              </div>
            </div>

            <div class="row"> 
              <div class="col-md-6">
                <div class="card-block">
                  <div class="input-group col-12 datep">
                    <label class="label-control col-12 pl-0">วันที่ได้รับ  :</label>
                    <input type="text" class="form-control pickadate-translations" placeholder="" id="RecvInsignaDate_t17"
                     name="RecvInsignaDate_t17" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                    <div class="input-group-append">
                      <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 ">
                    <div class="card-body" >หลักฐาน :
                      <input class="input form-control" style="width: 100%;" placeholder=" ">
                    </div>
                  </div>
            </div>

            <div class="row"> 
              <div class="col-md-6">
                <div class="card-block">
                  <div class="input-group col-12 datep">
                    <label class="label-control col-12 pl-0">วันที่ส่งคืน  :</label>
                    <input type="text" class="form-control pickadate-translations" placeholder="" 
                    id="RetuISInsignaDate_t17" name="RetuISInsignaDate_t17" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                    <div class="input-group-append">
                      <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 ">
                  <div class="card-body" >หลักฐาน :
                    <input class="input form-control" style="width: 100%;" placeholder=" ">
                  </div>
              </div>
            </div>
            <input type="text" name="PersonID_t17" id="PersonID_t17" style="display:none" value="<?=$PersonID?>">
      </form> 

              <!-- -------------------- -->
              </div>
            <br><!-- ---------- เว้นระยะห่าง ---------- -->
              <div class="tab-content px-1 pt-1">
                <div class="form-actions center" align="center">
                  <button type="button" class="btn btn-success round btn-min-width mr-1 mb-1" id="submit" name="submit" data-target="#modalConfirm" onclick="insertOrganizationGroupType()">
                    <i class="fa fa-save"></i>&nbsp;บันทึก</button>
                  <button type="button" class="btn btn-danger round btn-min-width mr-1 mb-1" id="type-error" data-dismiss="modal" onClick="ClearForm_t17();">
                    <i class="fa fa-times-circle-o"></i>&nbsp;ยกเลิก</button>
                </div>
              </div>
          </div>
        <!---------- ---------- End ---------- ---------- -->
      </div> 
    </div> 
  </div>
</div>
<script type="text/javascript">
  function editHrtInsigna(condition_value)
  {
    $('#InsignaName_t17').val($('#ipt17_1_'+condition_value).val()).trigger("change");
    $('#GivInsignaDate_t17').val($('#ipt17_2_'+condition_value).val());
    $('#GaetteBook_t17').val($('#ipt17_3_'+condition_value).val());
    $('#GazetteSection_t17').val($('#ipt17_4_'+condition_value).val());
    $('#GazettePage_t17').val($('#ipt17_5_'+condition_value).val());
    $('#GazetteSeq_t17').val($('#ipt17_6_'+condition_value).val());
    $('#GazetteDate_17').val($('#ipt17_7_'+condition_value).val());
    $('#RecvInsignaDate_t17').val($('#ipt17_8_'+condition_value).val());
    $('#RetuISInsignaDate_t17').val($('#ipt17_9_'+condition_value).val());
    
  }

  function ClearForm_t17()
  {
      var list_object_M = new Array('InsignaName_t17','GivInsignaDate_t17','GaetteBook_t17','GazetteSection_t17','GazettePage_t17','GazetteDate_17','GazetteSeq_t17','RecvInsignaDate_t17','RetuISInsignaDate_t17');
      for(clear_value = 0; clear_value <= 8; clear_value++){
        $('#'+list_object_M[clear_value]).val('');
        if(clear_value == 0){ $('#'+list_object_M[clear_value]).trigger("change"); }
      }
  }
</script>

