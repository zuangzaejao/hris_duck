<div class="tab-pane" id="con_tab12" aria-labelledby="conduct-tab12">
  <div class="row">
    <div class="col-12">
      <div class="float-left">
        <a href="#" class="btn btn1 mb-1 round"><span class="fa fa-plus-circle"></span> เพิ่ม</a>
        <a href="#" class="btn btn1 mb-1 round"><span class="fa fa-trash-o"></span> ลบ</a>
      </div>
    </div>
  </div> 
  
  <section>
    <table  class="table table-striped table-borderless table-hover bootstrap-3 ">
      <thead>
        <tr align="center" style="background-color:#0f1733; color:whitesmoke;">
          <th><input type="checkbox" class="checkAll" onclick="toggle(this);" /></th>
          <th></th>
          <th>ลำดับที่</th>
          <th>กรณีความผิด</th>
          <th>ตั้งแต่</th>
          <th>ถึง</th>
          <th>ทัณฑ์ที่ได้รับ</th>
          <th>ตั้งแต่</th>
          <th>ถึง</th>
          <th>ผู้ลงทัณฑ์</th>
        </tr>
      </thead>
      <tbody align="center">
        <tr>
          <td><input type="checkbox" class="checkAll" /></td>
          <td>
          <a href="#" data-toggle="modal" data-target="#from_los2"><i class="la la-pencil-square-o" style="color:#0f1733;"></i></a>
          <a href="#"><i class="la la-trash-o" style="color:#0f1733;"></i></a>
        </td>
          <td>1</td>
          <td>ใช้กิริยาวาจาไม่สมควร หรือประพฤติไม่สมควร</td>
          <td>1 ก.ค.2558</td>
          <td>1 ก.ค.2558</td>
          <td>ขัง 30 วัน</td>
          <td>1 ก.ค.2558</td>
          <td>1 ก.ค.2558</td>
          <td>ขัง 30 วัน</td>
        </tr>
      </tbody>
    </table>
  </section>

  <div class="modal animated slideInUp text-left modal_custom1" id="from_los2" tabindex="-1" role="dialog" aria-labelledby="modalSettingRegis"  aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <!---------- ---------- Start Content ---------- ---------- -->
          <div class="card-body">
              <div class="model-header" style="background-color:#0f1733;">
              <!-- -------------------- -->
                <div class="row">
                  <div class="col-md-11">
                    <h6 class="model-title text-white px-2 pt-2 py-1">บันทึก/แก้ไข ประวัติความผิด 
                      <?php echo $data_HrtRank['HrtRankAbbrTh']." ".$data_Person['PersonName']."   ".$data_Person['SurName']."  หมายเลขประจำตัว : ".$data_Person['AirForceID']; ?></h6>
                  </div>
                  <div class="col-md-1">
                    <h4 class="model-title text-white pt-2"><a data-dismiss="modal"><i class="fa fa-times-circle-o"></i></a></h4>
                  </div>
                </div>
              <!-- -------------------- -->
              </div>
              <div class="model-body">
              <!-- -------------------- -->
                <div class="card collapse-icon accordion-icon-rotate active pt-1">
                  <div class="card-header bg-success">
                    <a data-toggle="collapse" href="#motab6_s2_h1" aria-expanded="true" aria-controls="motab6_s2_h1"class="card-title lead white">
                    <h6><U>ส่วนที่ 1</U> ประวัติความผิด</h6></a>
                  </div>
                  <div id="motab6_s2_h1" role="tabpanel" class="card-collapse collapse show" aria-expanded="true">
                    <div class="card-content">
                      <div class="row"> 
                        <div class="col-md-3"> 
                          <div class="card-block">
                            <div class="card-body ">เดือนที่กระทำความผิด :
                              <select class="select2 form-control" style="width: 100%;">
                                <option></option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3"> 
                          <div class="card-block">
                            <div class="card-body ">ปีที่กระทำความผิด :
                              <select class="select2 form-control" style="width: 100%;">
                                <option></option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 ">
                          <div class="card-body" id="NTT">ลงโทษวินัยตาม :
                            <input class="input form-control" style="width: 100%;" placeholder=" ">
                          </div>
                        </div> 
                      </div>

                      <div class="row"> 
                        <div class="col-md-6"> 
                          <div class="card-block">
                            <div class="card-body ">กลุ่มกรณีความผิด :
                              <select class="select2 form-control" style="width: 100%;">
                                <option></option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 ">
                          <div class="card-body" id="NTT">กรณีความผิด :
                            <input class="input form-control" style="width: 100%;" placeholder=" ">
                          </div>
                        </div> 
                      </div>  

                      <div class="row"> 
                        <div class="col-md-12 ">
                          <div class="card-body" id="NTT">รายละเอียดเพิ่มเติม :
                          <input class="input form-control" style="width: 100%;" placeholder=" ">
                          </div>
                        </div> 
                      </div>
                    </div>
                  </div>
                </div>
                <!-- ------------------ ส่วนที่ 2 -------------------------------->
                <div class="card collapse-icon accordion-icon-rotate active pt-1">
                  <div class="card-header bg-success">
                    <a data-toggle="collapse" href="#motab6_s2_h2" aria-expanded="true" aria-controls="motab6_s2_h2"class="card-title lead white">
                      <h6><U>ส่วนที่ 2</U> ประวัติความผิด</h6></a>
                  </div>
                  <div id="motab6_s2_h2" role="tabpanel" class="card-collapse collapse show" aria-expanded="true">
                    <div class="card-content">
                      <div class="row"> 
                        <div class="col-md-6"> 
                          <div class="card-block">
                            <div class="card-body ">ผู้สั่งลงทัณฑ์ :
                              <select class="select2 form-control" style="width: 100%;">
                                <option></option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 ">
                          <div class="card-body" id="NTT">ผู้สั่งลงทัณฑ์ เพิ่มเติม :
                            <input class="input form-control" style="width: 100%;" placeholder=" ">
                          </div>
                        </div> 
                      </div>

                      <div class="row"> 
                        <div class="col-md-6">
                          <div class="card-block">
                            <div class="input-group col-12 datep">
                              <label class="label-control col-12 pl-0">วันที่กระทำความผิดตั้งแต่  :</label>
                              <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                              <div class="input-group-append">
                                <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="card-block">
                            <div class="input-group col-12 datep">
                              <label class="label-control col-12 pl-0">ถึงวันที่  :</label>
                              <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                              <div class="input-group-append">
                                <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="row"> 
                        <div class="col-md-6">
                          <div class="card-block">
                            <div class="input-group col-12 datep">
                              <label class="label-control col-12 pl-0">วันที่กรณีความผิดได้ข้อติ  :</label>
                              <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                              <div class="input-group-append">
                                <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="card-body" id="NTT">ครั้งที่ 2 :
                            <input class="input form-control" style="width: 100%;" placeholder=" ">
                          </div>
                        </div>
                        <div class="col-md-3"></div>
                      </div>

                      <div class="row"> 
                        <div class="col-md-6"> 
                          <div class="card-block">
                            <div class="card-body ">ทัณฑ์ที่ได้รับ :
                              <select class="select2 form-control" style="width: 100%;">
                                <option></option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3 ">
                          <div class="card-body" id="NTT">ระยะเวลาในการลงทัณฑ์ :
                            <input class="input form-control" style="width: 100%; text-align:right" placeholder=" " value="">
                          </div>
                        </div>
                        <div class="col-md-1 pt-2">
                          <div class="card-body" id="NTT">&nbsp;วัน</div>
                        </div>  
                      </div>  

                      <div class="row"> 
                        <div class="col-md-6">
                          <div class="card-block">
                            <div class="input-group col-12 datep">
                              <label class="label-control col-12 pl-0">วันที่ได้รับทัณฑ์ ตั้งแต่  :</label>
                              <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                              <div class="input-group-append">
                                <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="card-block">
                            <div class="input-group col-12 datep">
                              <label class="label-control col-12 pl-0">ถึงวันที่  :</label>
                              <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                              <div class="input-group-append">
                                <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="row"> 
                        <div class="col-md-12 ">
                          <div class="card-body" id="NTT">หมายเหตุ :
                          <textarea class="input form-control"></textarea>
                          </div>
                        </div> 
                      </div>

                      <div class="row"> 
                        <div class="col-md-6"> 
                          <div class="card-block">
                            <div class="card-body ">ผู้รับทัณฑ์ :
                              <select class="select2 form-control" style="width: 100%;">
                                <option></option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 ">
                          <div class="card-body" id="NTT">ผู้รับทัณฑ์เพิ่มเติม :
                            <input class="input form-control" style="width: 100%;" placeholder=" ">
                          </div>
                        </div> 
                      </div>
                    </div>
                  </div>
                </div>
                <!-- ------------------ ส่วนที่ 3 -------------------------------->
                <div class="card collapse-icon accordion-icon-rotate active pt-1">
                  <div class="card-header bg-success">
                    <a data-toggle="collapse" href="#motab6_s1_h3" aria-expanded="true" aria-controls="motab6_s1_h3"class="card-title lead white">
                      <h6><U>ส่วนที่ 3</U> ประวัติความผิด(หลักฐาน)</h6></a>
                  </div>
                  <div id="motab6_s1_h3" role="tabpanel" class="card-collapse collapse show" aria-expanded="true">
                    <div class="card-content">
                      <div class="row">
                        <div class="col-md-12 ">
                          <div class="card-block">
                            <div class="card-body "><h6>หลักฐานการตรวจสอบ</h6></div>
                          </div>
                        </div>
                      </div>

                      <div class="row"> 
                        <div class="col-md-6"> 
                          <div class="card-block">
                            <div class="card-body ">คำสั่ง :
                              <select class="select2 form-control" style="width: 100%;">
                                <option></option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3 ">
                          <div class="card-body" id="NTT">เลขที่หนังสือ :
                            <input class="input form-control" style="width: 100%;" placeholder=" ">
                          </div>
                        </div> 
                        <div class="col-md-3 "></div>
                      </div>

                      <div class="row"> 
                        <div class="col-md-6">
                          <div class="card-block">
                            <div class="input-group col-12 datep">
                              <label class="label-control col-12 pl-0">ลงวันที่  :</label>
                              <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                              <div class="input-group-append">
                                <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6"></div>
                      </div>

                      <div class="row">
                        <div class="col-md-12 pt-1">
                          <div class="card-block">
                            <div class="card-body "><h6>หลักฐานการล้างมลทิน</h6></div>
                          </div>
                        </div>
                      </div>

                      <div class="row"> 
                        <div class="col-md-3 ">
                          <div class="card-body" id="NTT">รกจ. เล่มที่ :
                            <input class="input form-control" style="width: 100%;" placeholder=" ">
                          </div>
                        </div> 
                        <div class="col-md-3 ">
                          <div class="card-body" id="NTT">ตอนที่ :
                            <input class="input form-control" style="width: 100%;" placeholder=" ">
                          </div>
                        </div>
                        <div class="col-md-3 ">
                          <div class="card-body" id="NTT">หน้าที่ :
                            <input class="input form-control" style="width: 100%;" placeholder=" ">
                          </div>
                        </div> 
                        <div class="col-md-3 ">
                          <div class="card-body" id="NTT">ลำดับที่ :
                            <input class="input form-control" style="width: 100%;" placeholder=" ">
                          </div>
                        </div>  
                      </div> 

                      <div class="row"> 
                        <div class="col-md-6">
                          <div class="card-block">
                            <div class="input-group col-12 datep">
                              <label class="label-control col-12 pl-0">วันที่ได้รับ  :</label>
                              <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                              <div class="input-group-append">
                                <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="card-block">
                            <div class="input-group col-12 datep">
                              <label class="label-control col-12 pl-0">วันที่ส่งคืน  :</label>
                              <input type="text" class="form-control pickadate-translations" placeholder="" id="startdate" name="startdate" style="width: 80%;" data-value="<?php echo GetToday('');?>" />
                              <div class="input-group-append">
                                <span class="input-group-text"><span class="la la-calendar-o"></span></span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div> 
                    </div>
                  </div>
                </div>
              <!-- -------------------- -->
              </div>
            <br><!-- ---------- เว้นระยะห่าง ---------- -->
              <div class="tab-content px-1 pt-1">
                <div class="form-actions center" align="center">
                  <button type="button" class="btn btn-success round btn-min-width mr-1 mb-1" id="submit" name="submit" data-target="#modalConfirm" onclick="insertOrganizationGroupType()">
                    <i class="fa fa-save"></i>&nbsp;บันทึก</button>
                  <button type="button" class="btn btn-danger round btn-min-width mr-1 mb-1" id="type-error" data-dismiss="modal">
                    <i class="fa fa-times-circle-o"></i>&nbsp;ยกเลิก</button>
                </div>
              </div>
          </div>
        <!---------- ---------- End ---------- ---------- -->
      </div> 
    </div> 
  </div>









   </div>
 







