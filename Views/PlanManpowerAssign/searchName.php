  <!-- header -->
  <?php include '../include/header.php'; ?>
  <?php include '../../Model/Ducklab/func.php'; ?>
  <?php  
    //Recruitment and appointment
    $menu1 = "PERSONASSIGN" ;
    $menu2 = "searchName" ;

  ?>
  <!-- menu -->
  <?php include '../include/menu.php'; ?>
  
  <section>
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-2">
            <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
            <h3 class="content-header-title">ค้นหารายชื่อที่บรรจุ </h3>
          </div>
        </div>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb"> 
            <li class="breadcrumb-item"><a href="#">ระบบงานสรรหาและบรรจุกำลังพล</a></li>  
            <li class="breadcrumb-item active" aria-current="page">ค้นหารายชื่อที่บรรจุ </li>
          </ol>
        </nav>
        <div class="content-body">
          <!-- Bootstrap 3 table -->
          <section id="bootstrap3">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-content collapse show">
                    <div class="card-body card-dashboard">
                      
                      <div class="row"> 
                        <div class="col-sm-12">
                          <div class="box_h1 card">
                            <div class="card-header card-head-inverse  ">
                              <h4 class="card-title text-white"> ค้นหา </h4> 
                              <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                              <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                  <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                                </ul>
                              </div>
                            </div>
                            <div class="card-content collapse show">
                              <div class="card-body">
                                <div class="row">
                                  <div class=" col-md-8  m-auto ">
                                  
                                  <form class="form">
                                      <div class="form-body"> 

                                          <div class="row">
                                            <div class="col-md-3">

                                            <div class="form-group">  
                                              <label for="exampleInputEmail1">แผนการบรรจุ:</label>
                                                  <select id="projectinput5" name="interested" class="form-control ">
                                                    <option value=""></option>
                                                    <option value=""></option>
                                                    <option value=""></option>
                                                  </select>
                                            </div>

                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group"> 
                                                    <label for="exampleInputEmail1">ปีงบประมาณ:</label>
                                                    <input class="form-control" type="text" placeholder="">
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                            <label for="exampleInputEmail1">ประเภทกำลังพล:</label>
                                                  <select id="projectinput5" name="interested" class="form-control ">
                                                    <option value=""></option>
                                                    <option value=""></option>
                                                    <option value=""></option>
                                                  </select>
                                                </div>
                                            </div>
                                            </div>
                                           
                                            <div class="row">
                                            <div class="col-md-6">
                                              <div class="form-group">
                                                <label for="exampleInputEmail1">คุณวุฒิ:</label>
                                                <select id="" name="" class="form-control">
                                                  <option value="">มัธยมศึกษาตอนต้น</option>
                                                  <option value="">มัธยมศึกษาตอนปลาย</option>
                                                  <option value="">ปวช.</option>
                                                  <option value="">ปวส.</option>
                                                  <option value="">ปริญญาตรี</option>
                                                  <option value="">ปริญญาโท</option>
                                                  <option value="">ปริญญาเอก</option>
                                                </select>
                                              </div>
                                            </div> 

                                            <div class="col-md-6">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">ระดับการศึกษา:</label>
                                                  <select id="projectinput5" name="interested" class="form-control ">
                                                    <option value=""></option>
                                                    <option value=""></option>
                                                    <option value=""></option>
                                                  </select>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">เลขอ้างอิงกลุ่มข้อมูล:</label>
                                                  <select id="projectinput5" name="interested" class="form-control ">
                                                    <option value=""></option>
                                                    <option value=""></option>
                                                    <option value=""></option>
                                                  </select>
                                                </div>
                                            </div>


                                            <div class="col-md-6">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">ชื่อ-สกุล:</label>
                                                  <select id="projectinput5" name="interested" class="form-control ">
                                                    <option value=""></option>
                                                    <option value=""></option>
                                                    <option value=""></option>
                                                  </select>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">ตำแหน่ง:</label>
                                                  <select id="projectinput5" name="interested" class="form-control ">
                                                    <option value=""></option>
                                                    <option value=""></option>
                                                    <option value=""></option>
                                                  </select>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">เลขที่ร่าง:</label>
                                              <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                                                        <label class="form-check-label" for="exampleRadios1">
                                                            บันทึกเลขร่าง
                                                        </label>
                                                        <select id="projectinput5" name="interested" class="form-control ">
                                                                    <option value=""></option>
                                                                    <option value=""></option>
                                                         </select>
                                                        </div>
                                                        <div class="form-check mt-1">
                                                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                                                        <label class="form-check-label" for="exampleRadios2">
                                                            ยังไม่ได้บันทึกเลขร่าง
                                                        </label>
                                                        </div>
                                                        <div class="form-check mt-1">
                                                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3" value="option3">
                                                        <label class="form-check-label" for="exampleRadios3">
                                                            ทั้งหมด
                                                        </label>
                                                        </div>
                                                </div>
                                            </div>




                                          </div>
                                    
                                        </div>  <!-- ./boxsearchplan2 -->
                                      
                                      </div> 

                                      <div class="text-center">
                                        <button type="button" class="btn round btn1 btncustom1 mr-1">
                                          <i class="la la-search"></i> ค้นหา
                                        </button>
                                        <button type="button" class="btn round btn1 btncustom1">
                                          <i class="la la-repeat"></i> ประมวลรายชื่อเพื่อบรรจุ
                                        </button>
                                        <button type="button" class="btn round btn1 btncustom1">
                                          <i class="la la-repeat"></i> ล้างค่า
                                        </button>
                                      </div>

                                    </form>

                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div> 
                      
                     

                      <div class="row">
                        <div class="col-6 ">
                          <button class="btn round btn1 btncustom1">  <i class="la la-plus-circle"></i> <span > เพิ่ม</span> </button>
                          <button class="btn round btn1 btncustom1">  <i class="la la-trash-o"></i> <span > ลบ</span> </button>
                        </div>
                        <div class="col-6 text-right"> 
                          <button class="btn round btn1 btncustom1"  data-toggle="modal" data-target="#modalSettingCalcu">  <i class="la la-calculator"></i> <span >คำนวณเลขประจำตัวข้าราชการ</span> </button>
                        </div>
                      </div>
                      
                      <div class="box_tb1">
                      <div class="table-responsive">
                        <table id="planManpowerTable3" class="table table-bordered tb1 table-striped table-hover"  >
                          <thead>
                            <tr>
                              <th>   </th>
                              <th>   </th>
                              <th> ลำดับที่ </th>
                              <th> เลขที่คำสั่ง</th> 
                              <th> หมายเลขประจำตัว</th> 
                              <th> ยศ</th>   
                              <th> ชื่อสกุล</th>  
                              <th> สังกัด </th> 
                              <th> สังกัด</th>
                              <th> เหล่า</th>
                              <th> แต่งตั้งยศ</th>
                              <th> ระดับการศึกษา</th>
                              <th>อัตรา/ชั้นเงินเดือน</th>
                            </tr>
                          </thead>
                          <tbody> 
                            <tr > 
                              <td class="text-center">  
                              <a><input type="checkbox" class="checkAll" /> </a>
                              </td>
                              <td class="text-center"> 
                              <a href="profile.php" class="fontcolor1" > <i class="la la-pencil-square-o"></i>  </a>
                              <td class="text-center"> 1 </td> 
                              <td class="text-center"> 020_02098/2562 </td>
                              <td class="text-center"> <input class="form-control" type="text" placeholder="020_02098/2562"></td>
                              <td class="text-center">  ร.ต.</td>
                              <td class="text-center">  นายกิตติศักดิ์ สมประกอบ </td>   
                              <td class="text-center">  รร.การบิน </td>   
                              <td class="text-center"> พยาบาล แผนกการเงิน</td>
                              <td class="text-center">  ขส. </td>
                              <td class="text-center">  ร.ต. </td> 
                              <td class="text-center">  ปริญญาตรี</td>   
                              <td class="text-center">  <select id="projectinput5" name="interested" class="form-control ">
                                                    <option value="">น./22</option>
                                                    <option value="">น./22</option>
                                                   
                                                  </select></td>       
                            </tr> 
                            
                           

                          </tbody>
                        </table>
                      </div>

                        <div class="text-center">
                        <a href="#" data-toggle="modal" data-target="#modalConfirm" class="btn btn-social btn-min-width mb-1 mr-1 round" role="button" style="background-color:green; color:white;">
                        <span class="la la-save px-1"style="color:white; font-weight: bold;font-size: 18px"></span><span class="px-1">บันทึกเลขร่างคำสั่ง</span></a> 

                                    
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <!--/ Bootstrap 3 table -->
        </div>
      </div>
    </div>
  </section>

  
  <!-- Modal -->
  <!-- Modal  id="#modalSettingCalcu"" -->
  <div class="modal fade text-left modal_custom1" id="modalSettingCalcu" tabindex="-1" role="dialog" aria-labelledby="modalSettingCalcu"  aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header bg_custom1">
            <h5 class="modal-title"> คำนวณเลขราชการ</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <form class="form_custom1">
            <div class="modal-body">
              <div class="row">
                <div class="col-8 m-auto">  

                
                  
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <div  class="m-auto ">
                <button type="button" class="btn btn2 btn-success   round"   data-dismiss="modal"> <i class="fa fa-save"></i> คำนวณ </button>
                <button type="button" class="btn btn2 btn-danger   round"  data-dismiss="modal"> <i class="fa fa-times-circle-o"></i> ยกเลิก </button> 
              </div>
            </div>
          </form>
        </div>
      </div>
  </div>
 

    
 
  <!-- footer -->
  
  
  <?php include '../include/footer.php'; ?> 
  <script src="../../Controllers/planManpowerAssignController.js"></script>
  <script type="text/javascript">
    $(document).ready(function() { 
 

    }); 
  </script>
  