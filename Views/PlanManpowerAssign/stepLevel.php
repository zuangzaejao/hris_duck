  <!-- header -->
  <?php include '../include/header.php'; ?>
  <?php include '../../Model/Ducklab/func.php'; ?>
  <?php  
    //Recruitment and appointment
    $menu1 = "PERSONASSIGN" ;
    $menu2 = "stepLevel" ;

  ?>
  <!-- menu -->
  <?php include '../include/menu.php'; ?>
  
  <section>
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
          <h3 class="content-header-title">  บันทึก/แก้ไข น.ประทวนเลื่อนฐานะ(คอค.) </h3>
        </div>
      </div>
      <nav aria-label="breadcrumb">
      <ol class="breadcrumb"> 
            <li class="breadcrumb-item"><a href="../home/index.php">หน้าแรก </a></li>  
            <li class="breadcrumb-item "  > <a href="/index.php"> บันทึก/แก้ไข น.ประทวนเลื่อนฐานะ(คอค.)  </a> </li>
          
          </ol>
      </nav>
      <div class="content-body">
         <!-- Bootstrap 3 table -->
         <section id="bootstrap3">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">


                    <div class="row"> 
                      <div class="col-md-12 col-lg-12 m-auto">
                        <div class="box_h2 card">
                          <div class="card-header">
                            <h4 class="card-title"><span class="fontcolor1"><u> ส่วนที่ 1</u> </span>&nbsp;&nbsp;สอบเพื่อเลือนฐานะ</h4> 
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                              </ul>
                            </div>
                          </div>
                          <div class="card-content collapse show">
                            <div class="card-body">
                              <div class="row">
                                <div class="col-md-12  m-auto">
                                 
                                  <form class="form form_custom1 ">
                                    <div class="form-body">

                                       <div class="row">
                                            <div class="col-md-3">
                                              <div class="form-group">
                                              <label for="exampleInputEmail1">แผนการบรรจุ:</label>
                                                <select id="" name="" class="form-control ">
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>
                                              </div>
                                            </div> 
                                            <div class="col-md-3">
                                              <div class="form-group">
                                              <label for="exampleInputEmail1">ปีงบประมาณ:</label>
                                              <input type="text" id="" class="form-control " placeholder="่" name="">  

                                              </div>
                                            </div>

                                            <div class="col-md-3">
                                              <div class="form-group">
                                              <label for="exampleInputEmail1">ประเภทกำลังพล:</label>
                                                <select id="" name="" class="form-control ">
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>
                                              </div>
                                            </div> 

                                            <div class="col-md-3">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">หมายเลขประจำตัว:</label>
                                              <input type="text" id="" class="form-control " placeholder="่" name="">  
                                              </div>
                                            </div>

                                            <div class="col-md-3">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">คุณวุฒิ:</label>
                                              <input type="text" id="" class="form-control " placeholder="่" name="" disabled>  
                                              </div>
                                            </div> 

                                            <div class="col-md-3">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">ยศ:</label>
                                              <input type="text" id="" class="form-control " placeholder="่" name="" disabled>  
                                              </div>
                                            </div> 

                                            <div class="col-md-6">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">ชื่อ-สกุล:</label>
                                              <input type="text" id="" class="form-control " placeholder="่" name="" disabled>  
                                              </div>
                                            </div> 


                                            <div class="col-md-3">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">สังกัด:</label>
                                              <input type="text" id="" class="form-control " placeholder="่" name="" disabled>  
                                              </div>
                                            </div> 

                                            <div class="col-md-3">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">เหล่า:</label>
                                              <input type="text" id="" class="form-control " placeholder="่" name="" disabled>  
                                              </div>
                                            </div> 

                                            <div class="col-md-6">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">จำพวก:</label>
                                              <input type="text" id="" class="form-control " placeholder="่" name="" disabled>  
                                              </div>
                                            </div> 

                                            <div class="col-md-12">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">ตำแหน่ง:</label>
                                              <input type="text" id="" class="form-control " placeholder="่" name="" disabled>  
                                              </div>
                                            </div> 

                                            <div class="col-md-3">
                                              <div class="form-group">
                                              <label for="exampleInputEmail1">ใช้คุณวุฒิ:</label>
                                                <select id="" name="" class="form-control ">
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>
                                              </div>
                                            </div> 



                                          </div>

                                  
                                    </div>  
                                    </div>  
                                  </form>

                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    
<!--ส่วนที่ 2 ประวัติ -->

                      <div class="row"> 
                      <div class="col-md-12 col-lg-12 m-auto">
                        <div class="box_h2 card">
                          <div class="card-header">
                            <h4 class="card-title"><span class="fontcolor1"><u> ส่วนที่ 2</u> </span>&nbsp;&nbsp;ตำแหน่งที่พิจารณาบรรจุ</h4> 
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                              </ul>
                            </div>
                          </div>

                          <div class="card-content collapse show">
                            <div class="card-body">
                              <div class="row">
                                <div class="col-md-10  m-auto">
                                 
                                  <form class="form form_custom1 ">
                                    <div class="form-body">

                                       <div class="row">
                                           
                                          
                                          
                                       <div class="col-md-12">
                                              <div class="form-group">
                                              <label for="exampleInputEmail1">ตำแหน่งที่พิจารณาบรรจุ</label>
                                                <select id="" name="" class="form-control ">
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>
                                              </div>
                                            </div> 

                                        </div>

                                    

                                  
                                    </div>  
                                    </div>  
                                  </form>

                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div> 

                     







                    <div class="row m-auto mb-3"> 
                    <div class="col-12 d-flex justify-content-center"> 
                    <a href="#" data-toggle="modal" data-target="#modalConfirm" class="btn btn-social btn-min-width mb-1 mr-1 round" role="button" style="background-color:green; color:white;">
                        <span class="la la-save"style="color:white; font-weight: bold;font-size: 18px"></span>บันทึก</a> 
                    <a href="#" class="btn btn-social btn-min-width mb-1 round" role="button" style="background-color:red; color:white;">
                        <span class="fa fa-times-circle-o"style="color:white; font-weight: bold;font-size: 18px"></span>ยกเลิก</a> 
                    </div>
                    </div>



                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--/ Bootstrap 3 table -->
      </div>
    </div>
  </div>
  </section>



<!-- Modal  id="modalConfirm"" -->
<div class="modal fade text-left modal_custom1" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="modalSettingCalcu"  aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header bg_custom1">
            <h5 class="modal-title"> บันทึกเลขร่างคำสั่ง</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <form class="form_custom1">
            <div class="modal-body">
              <div class="row">
                <div class="col-6 m-auto"> 

                    <div class="form-group">  
                        <label for="positionno"> คำสั่ง : </label>
                        <select id="projectinput5" name="interested" class="form-control ">
                                                                    <option value=""></option>
                                                                    <option value=""></option>
                         </select>                     
                   </div>

                      <div class="form-group">  
                        <label for="positionno"> เลขคำสั่ง : </label>
                        <input type="text" class="form-control" id="" name="" value="" >
                      </div>

                    
                        <div class="form-group">
                                          <label for="startdate"> ลง:   </label> 
                                          <div class="input-group datep">
                                            <input type="text"  style="width:86%" class="form-control form2 pickadate-translations" placeholder="" id="startdate" name="startdate" data-value="<?php echo GetToday('');?>" />
                                            <div class="input-group-append">
                                              <span class="input-group-text">
                                                <span class="la la-calendar-o"></span>
                                              </span>
                                            </div>
                                          </div>
                       </div>
                  

                       <div class="form-group">
                                          <label for="startdate"> วันที่:   </label> 
                                          <div class="input-group datep">
                                            <input type="text"  style="width:86%" class="form-control form2 pickadate-translations" placeholder="" id="startdate" name="startdate" data-value="<?php echo GetToday('');?>" />
                                            <div class="input-group-append">
                                              <span class="input-group-text">
                                                <span class="la la-calendar-o"></span>
                                              </span>
                                            </div>
                                          </div>
                       </div>
                
                  
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <div  class="m-auto ">
                <button type="button" class="btn btn2 btn-success   round"   data-dismiss="modal"> <i class="fa fa-save"></i> บันทึก </button>
                <button type="button" class="btn btn2 btn-danger   round"  data-dismiss="modal"> <i class="fa fa-times-circle-o"></i> ยกเลิก </button> 
              </div>
            </div>
          </form>
        </div>
      </div>
  </div>

 
  <!-- footer -->
  <?php include '../include/footer.php'; ?> 

  <script src="../../Controllers/planManpowerController.js"></script>
  <script type="text/javascript">
    $(document).ready(function() { 

      $("#planManTypeId").change(function(){
        // alert( $(this).val() );
         $('.tb1').addClass('hidden');

         $('#planManTypeId2').addClass('hidden'); 
         if( $(this).val() == 1 || $(this).val() == 0){
            $('#planManpowerTable1').removeClass('hidden');
         }else if( $(this).val() ==2 ){ 
            $('#planManTypeId2').removeClass('hidden'); 
            $('#planManpowerTable2').removeClass('hidden'); 
         }else if( $(this).val() == 3 ) {
            $('#planManpowerTable4').removeClass('hidden');
         }

      });

      $("#planManTypeId2").change(function(){
        $('.tb1').addClass('hidden');
         if( $(this).val() ==22 ){
            $('#planManpowerTable3').removeClass('hidden'); 
          }else{
            $('#planManpowerTable2').removeClass('hidden'); 
          }
      });


    }); 
  </script>
  