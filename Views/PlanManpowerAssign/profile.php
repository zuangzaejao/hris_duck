  <!-- header -->
  <?php include '../include/header.php'; ?>
  <?php include '../../Model/Ducklab/func.php'; ?>
  <?php  
    //Recruitment and appointment
    $menu1 = "PERSONASSIGN" ;
    $menu2 = "profile" ;

  ?>
  <!-- menu -->
  <?php include '../include/menu.php'; ?>
  
  <section>
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
          <h3 class="content-header-title"> การบรรจุกำลังพล </h3>
        </div>
      </div>
      <nav aria-label="breadcrumb">
      <ol class="breadcrumb"> 
            <li class="breadcrumb-item"><a href="../home/index.php">หน้าแรก </a></li>  
            <li class="breadcrumb-item "  > <a href="/index.php">การบรรจุกำลังพล ประจำปี 2562 </a> </li>
            <li class="breadcrumb-item active" aria-current="page"> ข้อมูลผู้รับการบรรจุ </li>
          </ol>
      </nav>
      <div class="content-body">
         <!-- Bootstrap 3 table -->
         <section id="bootstrap3">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">


                    <div class="row"> 
                      <div class="col-md-12 col-lg-12 m-auto">
                        <div class="box_h2 card">
                          <div class="card-header">
                            <h4 class="card-title"><span class="fontcolor1"><u> ส่วนที่ 1</u> </span>&nbsp;&nbsp;ข้อมูลบุคคล</h4> 
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                              </ul>
                            </div>
                          </div>
                          <div class="card-content collapse show">
                            <div class="card-body">
                              <div class="row">
                                <div class="col-md-12  m-auto">
                                 
                                  <form class="form form_custom1 ">
                                    <div class="form-body">

                                       <div class="row">
                                            <div class="col-md-3">
                                              <div class="form-group">
                                              <label for="exampleInputEmail1">ประเภทกำลังพล:</label>
                                                <select id="" name="" class="form-control ">
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>
                                              </div>
                                            </div> 
                                            <div class="col-md-3">
                                              <div class="form-group">
                                              <label for="exampleInputEmail1">ประเภทคุณวุฒิ:</label>
                                                <select id="" name="" class="form-control ">
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>
                                              </div>
                                            </div>
                                            <div class="col-md-3">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">หมายเลขประจำตัว:</label>
                                              <input type="text" id="" class="form-control " placeholder="่" name="">  
                                              </div>
                                            </div>
                                            <div class="col-md-3">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">เลขอ้างอิงกลุ่มข้อมูล:</label>
                                              <input type="text" id="" class="form-control " placeholder="่" name="">  
                                              </div>
                                            </div> 
                                          </div>

                                  
                                    </div>  
                                    </div>  
                                  </form>

                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    
<!--ส่วนที่ 2 ประวัติ -->

                      <div class="row"> 
                      <div class="col-md-12 col-lg-12 m-auto">
                        <div class="box_h2 card">
                          <div class="card-header">
                            <h4 class="card-title"><span class="fontcolor1"><u> ส่วนที่ 2</u> </span>&nbsp;&nbsp;ประวัติ</h4> 
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                              </ul>
                            </div>
                          </div>

                          <div class="card-content collapse show">
                            <div class="card-body">
                              <div class="row">
                                <div class="col-md-10  m-auto">
                                 
                                  <form class="form form_custom1 ">
                                    <div class="form-body">

                                       <div class="row">
                                            <div class="col-md-3">
                                              <div class="form-group">
                                              <label for="exampleInputEmail1"> ยศ:</label>
                                                <select id="" name="" class="form-control ">
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>
                                              </div>
                                            </div> 
                                            <div class="col-md-3">
                                              <div class="form-group">
                                              <label for="exampleInputEmail1">เหล่า:</label>
                                                <select id="" name="" class="form-control ">
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>
                                              </div>
                                            </div>
                                            <div class="col-md-3">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">คำนำหน้าชื่อ:</label>
                                              <select id="" name="" class="form-control ">
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>                                              </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">ชื่อ:</label>
                                              <input type="text" id="" class="form-control " placeholder="่" name="">  
                                              </div>
                                            </div> 

                                            <div class="col-md-6">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">นามสกุล:</label>
                                              <input type="text" id="" class="form-control " placeholder="่" name="">  
                                              </div>
                                            </div> 

                                            <div class="col-md-6">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">ชื่อภาษาอังกฤษ:</label>
                                              <input type="text" id="" class="form-control " placeholder="่" name="">  
                                              </div>
                                            </div> 

                                            <div class="col-md-6">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">นามสกุลภาษาอังกฤษ:</label>
                                              <input type="text" id="" class="form-control " placeholder="่" name="">  
                                              </div>
                                            </div> 


                                            <div class="col-md-6">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">เลขประจำตัวประชาชน:</label>
                                              <input type="text" id="" class="form-control " placeholder="่" name="">  
                                              </div>
                                            </div> 

                                            <div class="col-md-6">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">วันเดือนปีเกิด:</label>
                                                    <div class="input-group datep">
                                                    <input type="text"  style="width:86%" class="form-control form2 pickadate-translations" placeholder="" id="startdate" name="startdate" data-value="<?php echo GetToday('');?>" />
                                                        <div class="input-group-append">
                                                          <span class="input-group-text">
                                                            <span class="la la-calendar-o"></span>
                                                          </span>
                                                        </div>
                                                    </div> 
                                              </div>
                                            </div> 

                                            <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="card-body" id="gender">เพศ:                                                                     
                                                  <div class="card-content">
                                                      <div class="card-body">
                                                      <div class="d-inline-block custom-control custom-radio mr-1">
                                                          <input type="radio" class="custom-control-input" name="sexRadio" id="radio1" checked>
                                                          <label class="custom-control-label"  for="radio1">เพศชาย</label> 
                                                      </div>

                                                      <div class="d-inline-block custom-control custom-radio mr-1">
                                                          <input type="radio" class="custom-control-input" name="sexRadio" id="radio2">
                                                          <label class="custom-control-label"  for="radio2">เพศหญิง</label> 
                                                      </div>
                                                    </div>
                                                </div>
                                              </div>
                                              </div> 
                                          </div>                                                                           
                                                                                                                                      
                                          <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="card-body" id="gender">สถานะภาพ:                                                                     
                                                  <div class="card-content">
                                                      <div class="card-body">
                                                      <div class="d-inline-block custom-control custom-radio mr-1">
                                                          <input type="radio" class="custom-control-input" name="status" id="statusradio1" checked>
                                                          <label class="custom-control-label"  for="statusradio1">โสด</label> 
                                                      </div>

                                                      <div class="d-inline-block custom-control custom-radio mr-1">
                                                          <input type="radio" class="custom-control-input" name="status" id="statusradio2">
                                                          <label class="custom-control-label"  for="statusradio2">สมรส</label> 
                                                      </div>

                                                      <div class="d-inline-block custom-control custom-radio mr-1">
                                                          <input type="radio" class="custom-control-input" name="status" id="statusradio3">
                                                          <label class="custom-control-label"  for="statusradio3">หย่า</label> 
                                                      </div>


                                                      <div class="d-inline-block custom-control custom-radio mr-1">
                                                          <input type="radio" class="custom-control-input" name="status" id="statusradio4">
                                                          <label class="custom-control-label"  for="statusradio4">หม้าย</label> 
                                                      </div>

                                                    </div>
                                                </div>
                                              </div>
                                              </div>  
                                          </div>     

                                           <div class="col-md-6">
                                              <div class="form-group">
                                              <label for="exampleInputEmail1">สัญชาติ:</label>
                                                <select id="" name="" class="form-control ">
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>
                                              </div>
                                            </div>


                                            <div class="col-md-6">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">เชื่อชาติ:</label>
                                              <select id="" name="" class="form-control ">
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>                                              </div>
                                            </div>
                                                                                             
                                                                                                                              

                                            <div class="col-md-6">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">ศาสนา:</label>
                                              <select id="" name="" class="form-control ">
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>                                                    </div>
                                            </div> 


                                        </div>

                                  
                                    </div>  
                                    </div>  
                                  </form>

                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div> 

                      <!-- ส่วนที่3 ที่อยู่ -->
                    
                      <div class="row"> 
                      <div class="col-md-12 col-lg-12 m-auto">
                        <div class="box_h2 card">
                          <div class="card-header">
                            <h4 class="card-title"><span class="fontcolor1"><u> ส่วนที่ 3</u> </span>&nbsp;&nbsp;ที่อยู่</h4> 
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                              </ul>
                            </div>
                          </div>

                          <div class="card-content collapse show">
                            <div class="card-body">
                              <div class="row">

                                <div class="col-md-10  m-auto">
                                 
                                  <form class="form form_custom1 ">
                                    <div class="form-body">

                                       <div class="row">
                                              <div class="col-md-3">
                                                <div class="form-group">
                                                <label for="exampleInputEmail1">ที่อยู่เลขที่:</label>
                                                <input type="text" id="" class="form-control " placeholder="่" name="">  

                                                </div>
                                              </div> 

                                              <div class="col-md-3">
                                                <div class="form-group">
                                                <label for="exampleInputEmail1">ชื่อหมู่บ้าน/อาคาร:</label>
                                                <input type="text" id="" class="form-control " placeholder="่" name="">  

                                                </div>
                                              </div>

                                            <div class="col-md-3">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">หมู่:</label>
                                              <input type="text" id="" class="form-control " placeholder="่" name="">  
                                            
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">ซอย:</label>
                                              <input type="text" id="" class="form-control " placeholder="่" name="">  
                                             
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">ถนน:</label>
                                              <input type="text" id="" class="form-control " placeholder="่" name="">  
                                         
                                                </div>
                                            </div>


                                            <div class="col-md-3">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">ตำบล/แขวง:</label>
                                              <input type="text" id="" class="form-control " placeholder="่" name="">  
                                             
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">อำเภอ:</label>
                                              <input type="text" id="" class="form-control " placeholder="่" name="">  
                                             
                                                </div>
                                            </div>


                                            <div class="col-md-3">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">จังหวัด:</label>
                                              <select id="" name="" class="form-control ">
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>                                              
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">รหัสไปรษณีย์:</label>
                                              <select id="" name="" class="form-control ">
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>                                              
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">โทรศัพท์:</label>
                                              <input type="text" id="" class="form-control " placeholder="่" name="">  
                                             
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">ประเทศ:</label>
                                              <input type="text" id="" class="form-control " placeholder="่" name="">  
                                             
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                        <div class="col-md-12">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">ที่อยู่รวม:</label>
                                              <textarea  id="textarea" rows="3" class="form-control " placeholder="่" name="" > </textarea> 
                                             
                                                </div>
                                            </div>
                                        
                                        </div>

                                    </div>  
                                    </div>  
                                  </form>

                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div> 


                    <div class="row"> 
                      <div class="col-md-12 col-lg-12 m-auto">
                        <div class="box_h2 card">
                          <div class="card-header">
                            <h4 class="card-title"><span class="fontcolor1"><u> ส่วนที่ 4</u> </span>&nbsp;&nbsp; รายละเอียดข้อมูล </h4> 
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                              </ul>
                            </div>
                          </div>
                          <div class="card-content collapse show">
                            <div class="card-body">
                              <div class="row">
                                <div class=" col-md-10  m-auto ">
                                 
                                  <form class="form form_custom1 ">
                                    <div class="form-body">

                                       <div class="row">
                                            <div class="col-md-3">
                                              <div class="form-group">
                                              <label for="exampleInputEmail1">เครื่องหมาย:</label>
                                                <select id="" name="" class="form-control">
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>
                                              </div>
                                            </div> 
                                            <div class="col-md-3">
                                              <div class="form-group"> 
                                              <label for="exampleInputEmail1">ปี:</label> 
                                                <select id="" name="" class="form-control ">
                                                  
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>
                                              </div>
                                            </div>
                                            <div class="col-md-3">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">จังหวัด:</label>
                                                <select id="" name="" class="form-control ">
                                                
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>
                                              </div>
                                            </div>
                                            <div class="col-md-3">
                                              <div class="form-group"> 
                                              <label for="exampleInputEmail1">ข้อมูล:</label>
                                                <input type="text" id="" class="form-control " placeholder="่" name="">  
                                              </div>
                                            </div> 
                                          </div>

                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="startdate"> วันที่ขึ้นทะเบียน:   </label> 
                                          <div class="input-group datep">
                                            <input type="text"  style="width:86%" class="form-control form2 pickadate-translations" placeholder="" id="startdate" name="startdate" data-value="<?php echo GetToday('');?>" />
                                            <div class="input-group-append">
                                              <span class="input-group-text">
                                                <span class="la la-calendar-o"></span>
                                              </span>
                                            </div>
                                          </div>
                                        </div>
                                      </div>

                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="enddate"> ขึ้นทะเบียนทหหารเพราะ:   </label> 
                                          <select id="" name="" class="form-control ">
                                                 
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>
                                        </div>
                                      </div> 

                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="enddate"> กำเนิดแรกบรรจุ:   </label> 
                                          <select id="" name="" class="form-control ">
                                                 
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>
                                        </div>
                                      </div> 

                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="enddate"> คุณวุฒิแรกบรรจุ:   </label> 
                                          <select id="" name="" class="form-control ">
                                                 
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>
                                        </div>
                                      </div> 

                                   
                                   
                                    </div> 

                                    </div>  
                                  </form>

                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>  <!-- /.row  -->


<!-- ส่วนที่ 5 -->


                  <div class="row"> 
                      <div class="col-md-12 col-lg-12 m-auto">
                        <div class="box_h2 card">
                          <div class="card-header">
                            <h4 class="card-title"><span class="fontcolor1"><u> ส่วนที่ 5</u> </span>&nbsp;&nbsp; การศึกษา</h4> 
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                              </ul>
                            </div>
                          </div>
                          <div class="card-content collapse show">
                            <div class="card-body">
                              <div class="row">
                                <div class=" col-md-10  m-auto ">
                                 
                                  <form class="form form_custom1 ">
                                    <div class="form-body">

                                       <div class="row">
                                            <div class="col-md-6">
                                              <div class="form-group">
                                              <label for="exampleInputEmail1">ระดับการศึกษา:</label>
                                                <select id="" name="" class="form-control">
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>
                                              </div>
                                            </div> 
                                            <div class="col-md-6">
                                              <div class="form-group"> 
                                              <label for="exampleInputEmail1">ประเทศ:</label> 
                                                <select id="" name="" class="form-control ">
                                                  
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>
                                              </div>
                                            </div>


                                            <div class="col-md-6">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">สถานศึกษาที่จบ:</label>
                                                <select id="" name="" class="form-control ">
                                                
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>
                                              </div>
                                            </div>

                                            <div class="col-md-6">
                                              <div class="form-group"> 
                                              <label for="exampleInputEmail1">คุณวุฒิการศึกษา:</label>
                                              <select id="" name="" class="form-control ">
                                                <option value=""></option>
                                                <option value=""></option>
                                              </select>                                              
                                              </div>
                                            </div> 

                                            <div class="col-md-6">
                                              <div class="form-group"> 
                                              <label for="exampleInputEmail1">สาขาวิชา:</label>
                                              <select id="" name="" class="form-control ">
                                                
                                                <option value=""></option>
                                                <option value=""></option>
                                              </select>                                              
                                              </div>
                                            </div> 

                                            <div class="col-md-6">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">วิชาเอก:</label>
                                                <select id="" name="" class="form-control ">
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>
                                              </div>
                                            </div>

                                            
                                            <div class="col-md-6">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">วิชาโท:</label>
                                                <select id="" name="" class="form-control ">
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>
                                              </div>
                                            </div>
                                          </div>

                                  
                                    <div class="row">

                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="startdate"> วันที่เริ่มศึกษา:   </label> 
                                          <div class="input-group datep">
                                            <input type="text"  style="width:86%" class="form-control form2 pickadate-translations" placeholder="" id="startdate" name="startdate" data-value="<?php echo GetToday('');?>" />
                                            <div class="input-group-append">
                                              <span class="input-group-text">
                                                <span class="la la-calendar-o"></span>
                                              </span>
                                            </div>
                                          </div>
                                        </div>
                                      </div>

                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="startdate"> วันที่สำเร็จศึกษา:   </label> 
                                          <div class="input-group datep">
                                            <input type="text"  style="width:86%" class="form-control form2 pickadate-translations" placeholder="" id="startdate" name="startdate" data-value="<?php echo GetToday('');?>" />
                                            <div class="input-group-append">
                                              <span class="input-group-text">
                                                <span class="la la-calendar-o"></span>
                                              </span>
                                            </div>
                                          </div>
                                        </div>
                                      </div>

                                      <div class="col-md-3">
                                        <div class="form-group">
                                          <label for="enddate"> ปีที่จบ:   </label> 
                                          <input type="text" id="" class="form-control " placeholder="่" name="">  

                                        </div>
                                      </div> 

                                      <div class="col-md-3">
                                        <div class="form-group">
                                          <label for="enddate"> รุ่น:   </label> 
                                          <input type="text" id="" class="form-control " placeholder="่" name="">  

                                        </div>
                                      </div> 

                                      <div class="col-md-3">
                                        <div class="form-group">
                                          <label for="enddate"> เกรดเฉลี่ย:   </label> 
                                          <input type="text" id="" class="form-control " placeholder="่" name="">  

                                        </div>
                                      </div> 
                                    </div> 

                                    </div>  
                                  </form>

                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>  <!-- /.row  -->

<!-- ส่วนที่ 6 -->


                  <div class="row"> 
                      <div class="col-md-12 col-lg-12 m-auto">
                        <div class="box_h2 card">
                          <div class="card-header">
                            <h4 class="card-title"><span class="fontcolor1"><u> ส่วนที่ 6</u> </span>&nbsp;&nbsp;ปัจจัย</h4> 
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                              </ul>
                            </div>
                          </div>
                          <div class="card-content collapse show">
                            <div class="card-body">
                              <div class="row">
                                <div class=" col-md-10  m-auto ">
                                 
                                  <form class="form form_custom1 ">
                                    <div class="form-body">

                                       <div class="row">
                                            <div class="col-md-6">
                                              <div class="form-group">
                                              <label for="exampleInputEmail1">ประเภทคำสั่งบรรจุ:</label>
                                                <select id="" name="" class="form-control">
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>
                                              </div>
                                            </div> 
                                            <div class="col-md-6">
                                              <div class="form-group"> 
                                              <label for="exampleInputEmail1">ตำแหน่ง:</label> 
                                                <select id="" name="" class="form-control ">
                                                  
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>
                                              </div>
                                            </div>


                                            <div class="col-md-6">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">อัตราเงินเดือน:</label>
                                                <select id="" name="" class="form-control ">
                                                
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>
                                              </div>
                                            </div>

                                            <div class="col-md-6">
                                              <div class="form-group"> 
                                              <label for="exampleInputEmail1">จำพวก:</label>
                                              <select id="" name="" class="form-control ">
                                                <option value=""></option>
                                                <option value=""></option>
                                              </select>                                              
                                              </div>
                                            </div> 

                                            <div class="col-md-6">
                                              <div class="form-group"> 
                                              <label for="exampleInputEmail1">เหล่า:</label>
                                              <select id="" name="" class="form-control ">
                                                
                                                <option value=""></option>
                                                <option value=""></option>
                                              </select>                                              
                                              </div>
                                            </div> 


                                  
                                   

                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="startdate"> วันที่เริ่มศึกษา:   </label> 
                                          <div class="input-group datep">
                                            <input type="text"  style="width:86%" class="form-control form2 pickadate-translations" placeholder="" id="startdate" name="startdate" data-value="<?php echo GetToday('');?>" />
                                            <div class="input-group-append">
                                              <span class="input-group-text">
                                                <span class="la la-calendar-o"></span>
                                              </span>
                                            </div>
                                          </div>
                                        </div>
                                      </div>

                                  </form>

                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>  <!-- /.row  -->


<!-- ส่วนที่ 7 -->

                      <div class="row"> 
                      <div class="col-md-12 col-lg-12 m-auto">
                        <div class="box_h2 card">
                          <div class="card-header">
                            <h4 class="card-title"><span class="fontcolor1"><u> ส่วนที่ 7</u> </span>&nbsp;&nbsp;แต่งตั้งยศและค่าตอบแทน</h4> 
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                              </ul>
                            </div>
                          </div>
                          <div class="card-content collapse show">
                            <div class="card-body">
                              <div class="row">
                                <div class=" col-md-10  m-auto ">
                                <form class="form form_custom1 ">
                                    <div class="form-body">

                                       <div class="row">
                                            <div class="col-md-6">
                                              <div class="form-group">
                                              <label for="exampleInputEmail1">คุณวุฒิ:</label>
                                                <select id="" name="" class="form-control">
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>
                                              </div>
                                            </div> 
                                            <div class="col-md-6">
                                              <div class="form-group"> 
                                              <label for="exampleInputEmail1">แต่งตั้งยศ:</label> 
                                                <select id="" name="" class="form-control ">
                                                  
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>
                                              </div>
                                            </div>


                                            <div class="col-md-6">
                                              <div class="form-group">  
                                              <label for="exampleInputEmail1">ระดับชั้น:</label>
                                                <select id="" name="" class="form-control ">
                                                
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>
                                              </div>
                                            </div>

                                            <div class="col-md-6">
                                              <div class="form-group"> 
                                              <label for="exampleInputEmail1">เงินเดือน:</label>
                                              <select id="" name="" class="form-control ">
                                                <option value=""></option>
                                                <option value=""></option>
                                              </select>                                              
                                              </div>
                                            </div> 
                                    </div>  

                                    <div class="row">
                                            <div class="col-md-12">
                                            <table id="planManpowerTable" class="table table-striped table-borderless table-hover">
                                              <thead>
                                                <tr style="background-color:#0f1733; color:whitesmoke; text-align: center;">
                                                  <th>คะแนนสอบ/เกรดเฉลี่ย</th>
                                                  <th>ภาษาอังกฤษ </th>
                                                  <th>ประสบการณ์</th> 
                                                  <th>กห.ที่ 539/2555</th> 
                                                  <th>วุฒิเดียวกัน/สูงกว่า</th> 
                                                  <th>ขาดแคลน</th>
                                                  <th>ใบประกอบวิชาชีพ</th> 
                        
                                                </tr>
                                              </thead>
                                              <tbody> 
                                                <tr class="text-center">
                                                    <td class="text-center"> <input type="checkbox" class="" /> <input class="form-control" type="text"  placeholder="0.5"></td>
                                                    <td class="text-center"> <input type="checkbox" class="" /><input class="form-control" type="text"  placeholder="0.5"></td>

                                                    <td class="text-center"><input type="checkbox" class="" /><input class="form-control" type="text"  placeholder="-"></td>
                                                    <td class="text-center"><input type="checkbox" class="" /><input class="form-control" type="text"  placeholder="-"></td>
                                                    <td class="text-center"><input type="checkbox" class="" /><input class="form-control" type="text" placeholder="-"></td>
                                                    <td class="text-center"><input type="checkbox" class="" /><input class="form-control" type="text"  placeholder="-"></td>
                                                    <td class="text-center"><input type="checkbox" class="" /><input class="form-control" type="text" placeholder="-"></td>

                                                </tr>
                                                </tbody> 
                                                </table>
                                            </div>
                                    </div>
                                  </form>

                                  

                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>  <!-- /.row  -->

                    <div class="row m-auto mb-3"> 
                    <div class="col-12 d-flex justify-content-center"> 
                    <a href="#" data-toggle="modal" data-target="#modalConfirm" class="btn btn-social btn-min-width mb-1 mr-1 round" role="button" style="background-color:green; color:white;">
                        <span class="la la-save"style="color:white; font-weight: bold;font-size: 18px"></span>บันทึก</a> 
                    <a href="#" class="btn btn-social btn-min-width mb-1 round" role="button" style="background-color:red; color:white;">
                        <span class="fa fa-times-circle-o"style="color:white; font-weight: bold;font-size: 18px"></span>ยกเลิก</a> 
                    </div>
                    </div>



                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--/ Bootstrap 3 table -->
      </div>
    </div>
  </div>
  </section>


  <div class="modal fade text-left modal_custom1" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="modalConfirm"  aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <form>
          <div class="my-2 mx-2 px-2 py-2">
            <div class="text-center"> 
              <img src="../../Asset/Images/send.png">
              <div class="py-3">
                <h1> ยืนการส่งข้อมูลหรือไม่ ?</h1>
                <h6>กรุณาเลือกปุ่มใช่หรือไม่ใช่่</h6>
              </div>
            </div> 
            <div class="col-12 d-flex justify-content-center">
              <div class="row"> 
                  <a href="#" class="btn btn-min-width mb-1 mr-1 round btn-success"   data-dismiss="modal" role="button" >
                  <span></span>ใช่</a> 
                  <a href="#" class="btn btn-min-width mb-1 round btn-danger"   data-dismiss="modal" role="button" >
                  <span></span> ไม่ใช่ </a> 
              </div>
            </div>
          </div>
        </form>
      </div> 
    </div> 
  </div>


 
  <!-- footer -->
  <?php include '../include/footer.php'; ?> 

  <script src="../../Controllers/planManpowerController.js"></script>
  <script type="text/javascript">
    $(document).ready(function() { 

      $("#planManTypeId").change(function(){
        // alert( $(this).val() );
         $('.tb1').addClass('hidden');

         $('#planManTypeId2').addClass('hidden'); 
         if( $(this).val() == 1 || $(this).val() == 0){
            $('#planManpowerTable1').removeClass('hidden');
         }else if( $(this).val() ==2 ){ 
            $('#planManTypeId2').removeClass('hidden'); 
            $('#planManpowerTable2').removeClass('hidden'); 
         }else if( $(this).val() == 3 ) {
            $('#planManpowerTable4').removeClass('hidden');
         }

      });

      $("#planManTypeId2").change(function(){
        $('.tb1').addClass('hidden');
         if( $(this).val() ==22 ){
            $('#planManpowerTable3').removeClass('hidden'); 
          }else{
            $('#planManpowerTable2').removeClass('hidden'); 
          }
      });


    }); 
  </script>
  