  <!-- header -->
  <?php include '../include/header.php'; ?>
  <?php include '../../Model/Ducklab/func.php'; ?>
  <?php  
    //Recruitment and appointment
    $menu1 = "PERSONASSIGN" ;
    $menu2 = "searchAssign" ;

  ?>
  <!-- menu -->
  <?php include '../include/menu.php'; ?>
  
  <section>
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-2">
            <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
            <h3 class="content-header-title">บันทึก/แก้ไข การบรรจุกำลังพล </h3>
          </div>
        </div>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb"> 
            <li class="breadcrumb-item"><a href="../home/index.php">หน้าแรก </a></li>  
            <li class="breadcrumb-item active" aria-current="page"> บันทึก/แก้ไข การบรรจุกำลังพล </li>
          </ol>
        </nav>
        <div class="content-body">
          <!-- Bootstrap 3 table -->
          <section id="bootstrap3">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-content collapse show">
                    <div class="card-body card-dashboard">
                      
                      <div class="row"> 

                        <div class="col-sm-8">
                          <div class="box_h1 card">
                            <div class="card-header card-head-inverse  ">
                              <h4 class="card-title text-white"> ใส่ข้อมูลตำแหน่ง </h4> 
                              
                            </div>
                                  <div class="card-content collapse show">
                                    <div class="card-body">
                                      <div class="row">
                                        <div class=" col-md-12  m-auto ">

                         <div class="row">
                         <div class="col-7">
                          <div> ปีงบประมาณ : 2562 รายชื่อทั้งหมดจำนวน 50 คน เหลือใส่ข้อมูลอีก 47 คน</div>
                          <div class="text-danger">หมายเหตุ : กรุณาใส่รายชื่อจากตารางทางด้านซ้าย ลากซ้ายลงในชื่อนามสกุล</div>
                        </div>
                         
                        <div class="col-5 text-right">
                          <button class="btn round btn1 btncustom1 my-1">  <i class="la la-repeat"></i> <span > สุ่มรายชื่อ</span> </button>
                          <button class="btn round btn1 btncustom1 my-1">  <i class="la la-user-times"></i> <span > นำชื่อออก</span> </button>
                        </div>
                        </div>

                           
                                        <div class="table-responsive">
                                             <table id="planManpowerTable3" class="table table-bordered tb1 table-striped table-hover"  >
                                                  <thead>
                                                    <tr>
                                                      
                                                      <th> ลำดับที่ </th>
                                                      <th> รหัสกลุ่ม </th>
                                                      <th> คุณวุฒิ </th>
                                                      <th> ตำแหน่ง </th>
                                                      <th> จังหวัด</th> 
                                                      <th style="width:30%;" > ชื่อ-สกุล</th>  
                                                    </tr>
                                                  </thead>

                                                  <tbody> 
                                                    <tr> 
                                                      <td rowspan="3" class="text-center"> 1 </td> 
                                                      <td rowspan="3" class="text-center"> DOF-01</td>
                                                      <td rowspan="3">ประกาศนียบัตรวิชาชีพชั้นสูง สาขาวิชาปฎิบัติการฉุกเฉินการแพทย์หรือเวชกิจฉุกเฉินและได้รับใบประกาศนียบัตรเจ้าพนักงานฉุกเฉินการแพทย์ </td>
                                                            <td > พยาบาล แผนกปฎิบัติการ 2 กปก.ศปพ.พอ. </td>   
                                                            <td class="text-center"> นครศรี </td>   
                                                            <td class="text-center"></td>   
                                                    </tr> 

                                                    <tr>
                                                            <td > พยาบาล แผนกปฎิบัติการ 2 กปก.ศปพ.พอ. </td>   
                                                            <td class="text-center"> จันทบุรี</td>   
                                                            <td class="text-center"></td>     
                                                    </tr>

                                                    <tr>
                                                            <td > พยาบาล แผนกปฎิบัติการ 2 กปก.ศปพ.พอ. </td>   
                                                            <td class="text-center"> ยโสทร</td>   
                                                            <td class="text-center"></td>     
                                                    </tr>

                                                    <tr> 
                                                      <td rowspan="3" class="text-center"> 1 </td> 
                                                      <td rowspan="3" class="text-center"> DOF-01</td>
                                                      <td rowspan="3">ประกาศนียบัตรวิชาชีพชั้นสูง สาขาวิชาปฎิบัติการฉุกเฉินการแพทย์หรือเวชกิจฉุกเฉินและได้รับใบประกาศนียบัตรเจ้าพนักงานฉุกเฉินการแพทย์ </td>
                                                            <td > พยาบาล แผนกปฎิบัติการ 2 กปก.ศปพ.พอ. </td>   
                                                            <td class="text-center"> ขอนแก่น </td>   
                                                            <td class="text-center"></td>   
                                                    </tr> 

                                                    <tr>
                                                            <td > พยาบาล แผนกปฎิบัติการ 2 กปก.ศปพ.พอ. </td>   
                                                            <td class="text-center"> เชียงใหม่</td>   
                                                            <td class="text-center"></td>     
                                                    </tr>

                                                    <tr>
                                                            <td > พยาบาล แผนกปฎิบัติการ 2 กปก.ศปพ.พอ. </td>   
                                                            <td class="text-center">พิษณุโลก</td>   
                                                            <td class="text-center"></td>     
                                                    </tr>             
                                            
                                            </tbody>
                                          </table>
                                            </div>           
                                        
                                        </div>


                                      </div>
                                    </div>
                                  </div>
                          </div>
                        </div>


                        <div class="col-sm-4">
                        <div class="box_h1 card ">

                              <div class="card-header card-head-inverse">
                                    <h4 class="card-title text-white">  รายชื่อ</h4> 
                              </div>

                              <div><input class="form-control m-1" type="text"  style="width:89%;" placeholder="ค้นหาชื่อ"></div>
                              <div class="p-1" >
                              <h5 >รายชื่อทั้งหมด 50 คน </h5>
                              <!-- <button type="button" class="btn btn-primary"> นายกิตติศักดิ์ อามี</button> -->
                              <div  class="btn btn-primary "> นายกิตติศักดิ์ อามี</div>
                              <div  class="btn btn-primary"> อรวรรณ ประพฤติดี</div>



                             





                              </div>



                        
                        </div>
                        </div>












                      </div> 
                              
                            
                        </div>
                      </div>






                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <!--/ Bootstrap 3 table -->
        </div>
      </div>
    </div>
  </section>

  
  <!-- Modal -->
  <div class="modal fade text-left modal_custom1" id="modalSettingAssign" tabindex="-1" role="dialog" aria-labelledby="modalSettingAssign"  aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header bg_custom1">
            <h5 class="modal-title"> บันทึก/แก้ไข วันเวลาเปิด/ปิด รับสมัคร</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form class="form_custom1">
            <div class="modal-body">
              <div class="row">
                <div class="col-8 m-auto">  

                <div class="boxsearchplan2">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">  
                        <label for="positionno"> ปีงบประมาณ : </label>
                        <input type="text" class="form-control" id="" name="" value="2562" disabled >
                      </div>
                    </div>  
                    <div class="col-md-6">
                      <div class="form-group">  
                        <label for="positionno"> ชื่อแผน : </label>
                        <input type="text" class="form-control" id="" name="" value="แผนการบรรจุประจำปี 2562" disabled>
                      </div>
                    </div> 
                    
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="pb-2 hidden">
                        <span class="fontcustom1"> *กรุณากดปุ่ม Import เพื่อนำเข้าข้อมูล เพื่อสามารถกรอกข้อมูลในลำดับถัดไปได้ </span>
                      </div>
                      <button type="button" class="btn round btn1 btncustom1 mr-1">
                        <i class="fa fa-upload"></i>&nbsp; Import 
                      </button> 
                      <span class="boxcustom3">  ข้อมูลผู้มีรายชื่อสอบผ่านจริงและสำรอง2562.xlsx
                        <a href="#" class="btn round fontcolor2"> 
                          <i class="fa fa-times-circle-o"></i>
                        </a>
                      </span>
                    </div>   
                  </div> 
                </div>
                  
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <div  class="m-auto ">
                <button type="button" class="btn btn2 btn-success   round"   data-dismiss="modal"> <i class="fa fa-save"></i> บันทึก </button>
                <button type="button" class="btn btn2 btn-danger   round"  data-dismiss="modal"> <i class="fa fa-times-circle-o"></i> ยกเลิก </button> 
              </div>
            </div>
          </form>
        </div>
      </div>
  </div>
 

    
 
  <!-- footer -->
  
  
  <?php include '../include/footer.php'; ?> 
  <script src="../../Controllers/planManpowerAssignController.js"></script>
  <script type="text/javascript">
    $(document).ready(function() { 
 

    }); 
  </script>
  