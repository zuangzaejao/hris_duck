  <!-- header -->
  <?php include '../include/header.php'; ?>
  <?php include '../../Model/Ducklab/func.php'; ?>
  <?php  
  //Recruitment and appointment
  $menu1 = "PERSONASSIGN" ;
  $menu2 = "PSETTING" ;
  $menu3 = "SETTING1" ;
  ?>
  <!-- menu -->
  <?php include '../include/menu.php'; ?>

  <section>
  <div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-12">
                <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
                    <h3 class="content-header-title">ตั้งค่าปัจจัยตามจำพวกและคุณวุฒิ</h3>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="../home/index.php">ระบบงานสรรหาและบรรจุกำลังพล</a></li>
                                <li class="breadcrumb-item active" aria-current="page">ตั้งค่าปัจจัยตามจำพวกและคุณวุฒิ</li>
                                
                            </ol>
                        </nav>

      <div class="content-body">
         <!-- Bootstrap 3 table -->
         <section id="bootstrap3">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <p class="card-text"></p>
                    
                    <div class="row"> 
                        <div class="col-md-3 col-sm-12">
                          <div class="form-group">  
                            <select class="  form-control block " >
                              <option value=""> ปริญญาตรีหลักสูตร 4 ปี </option>
                              <option value=""> ปริญญาตรีหลักสูตร 5 ปี</option>
                              <option value=""> ปริญญาตรีหลักสูตร 6 ปี </option>
                              <option value=""> 2560 </option>
                              <option value=""> 2559 </option>
                            </select>
                          </div>
                        </div> 
                    </div>
                    <div class="row">
                      <div class="col-12">
                        <div class="box1 txtdesc">
                            ข้อมูลจำนวน <span> 36 </span> รายการ
                        </div>
                      </div>
                    </div>
                    
                    <table id="planManpowerTable" class="table table-striped table-borderless table-hover " style="width:100%;">
                      <thead>
                        <tr style="background-color:#0f1733; color:whitesmoke;text-align: center;">
                          <th> </th>
                          <th>ลำดับที่ </th>
                          <th>จำพวก</th>
                          <th>คะแนนสอบ/เกรดเฉลี่ย</th>
                          <th>ภาษาอังกฤษ </th>
                          <th>ประสบการณ์</th> 
                          <th>กห.ที่ 539/2555</th> 
                          <th>วุฒิเดียวกัน/สูงกว่า</th> 
                          <th>ขาดแคลน</th>
                          <th>ใบประกอบวิชาชีพ</th> 
 
                        </tr>
                      </thead>
                      <tbody> 
                        <tr>
                            <td class="text-center" > 
                              <a href="#"  data-toggle="modal" data-target="#bootstrap" title="บันทึก/แก้ไข ปัจจัยตามจำพวกและคุณวุฒิ"> <i class="la la-pencil-square-o" style="color:#0f1733;"> </i> </a> 

                            </td>
                            <td class="text-center"> 1 </td>
                            <td class="text-center"> สารบรรณ </td>
                            <td class="text-center"> 0.5 </td>
                            <td class="text-center"> 0.5 </td>
                            <td class="text-center"> 0.5 </td>
                            <td class="text-center"> - </td>
                            <td class="text-center"> - </td>
                            <td class="text-center"> - </td>
                            <td class="text-center"> - </td>
                        </tr>
                        <tr>
                            <td class="text-center" > 
                              <a href="#" title="บันทึก/แก้ไข ปัจจัยตามจำพวกและคุณวุฒิ"> <i class="la la-pencil-square-o" style="color:#0f1733;"> </i> </a> 

                            </td>
                            <td class="text-center"> 2 </td>
                            <td class="text-center"> กำลังพล </td>
                            <td class="text-center"> 0.5 </td>
                            <td class="text-center"> 0.5 </td>
                            <td class="text-center"> 0.5 </td>
                            <td class="text-center"> 1 </td>
                            <td class="text-center"> - </td>
                            <td class="text-center"> - </td>
                            <td class="text-center"> - </td>
                        </tr>

                       <tr>
                            <td class="text-center" > 
                              <a href="#" title="บันทึก/แก้ไข ปัจจัยตามจำพวกและคุณวุฒิ"> <i class="la la-pencil-square-o" style="color:#0f1733;"> </i> </a> 

                            </td>
                            <td class="text-center"> 3 </td>
                            <td class="text-center"> ข่าวกรอง </td>
                            <td class="text-center"> 0.5 </td>
                            <td class="text-center"> 0.5 </td>
                            <td class="text-center"> 0.5 </td>
                            <td class="text-center"> - </td>
                            <td class="text-center"> - </td>
                            <td class="text-center"> - </td>
                            <td class="text-center"> - </td>
                        </tr>

                        <tr>
                            <td class="text-center" > 
                              <a href="#" title="บันทึก/แก้ไข ปัจจัยตามจำพวกและคุณวุฒิ"> <i class="la la-pencil-square-o" style="color:#0f1733;"> </i> </a> 

                            </td>
                            <td class="text-center"> 4 </td>
                            <td class="text-center"> ส่งกำลังบำรุง </td>
                            <td class="text-center"> 0.5 </td>
                            <td class="text-center"> 0.5 </td>
                            <td class="text-center"> 0.5 </td>
                            <td class="text-center"> - </td>
                            <td class="text-center"> - </td>
                            <td class="text-center"> - </td>
                            <td class="text-center"> 0.5 </td>
                        </tr>


                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--/ Bootstrap 3 table -->
      </div>
    </div>
  </div>
  </section>

  <!-- //////////////////////////////////////////// Modal -->////////////////////////////////////////////////////// -->
 <div class="modal fade text-left modal_custom1" id="bootstrap" tabindex="-1" role="dialog" aria-labelledby="modalSettingRegis"  aria-hidden="true">
    <div class="modal-dialog modal-lg modal_size1" role="document">
        <div class="modal-content">
          <div class="modal-header bg_custom1">
            <h5 class="modal-title" id="modalSettingRegis"> บันทึก/แก้ไข ปัจจัยตามจำพวกและคุณวุฒิ</h5>  
        
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>

          </div>



          <form>
            <div class="row mt-2 mx-5">
                <div class="col-12 m-auto"> 
                <fieldset disabled>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                             <label for="formGroupExampleInput">คุณวุฒิ:</label>
                            <input type="text" class="form-control" id="formGroupExampleInput"  placeholder="ปริญญาตรีหลักสูตร 4 ปี">
                        </div>

                        <div class="form-group col-md-6">
                                <label for="formGroupExampleInput">จำพวก:</label>
                                <input type="text" class="form-control" id="formGroupExampleInput"  placeholder="สารบรรณ">
                                </div>
   
                    </div>
                    </fieldset>
                  </div></div>

                    <div class="row m-auto">
                    <div class="col-12"> 
                    <table id="planManpowerTable" class="table table-striped table-borderless table-hover">
                      <thead>
                        <tr style="background-color:#0f1733; color:whitesmoke; text-align: center;">
                          <th>คะแนนสอบ/เกรดเฉลี่ย</th>
                          <th>ภาษาอังกฤษ </th>
                          <th>ประสบการณ์</th> 
                          <th>กห.ที่ 539/2555</th> 
                          <th>วุฒิเดียวกัน/สูงกว่า</th> 
                          <th>ขาดแคลน</th>
                          <th>ใบประกอบวิชาชีพ</th> 
 
                        </tr>
                      </thead>
                      <tbody> 
                        <tr class="text-center">
                            <td class="text-center"> <input class="form-control" type="text" style="width:80px;" placeholder="0.5"></td>
                            <td class="text-center"> <input class="form-control" type="text" style="width:80px;" placeholder="0.5"></td>

                            <td class="text-center"><input class="form-control" type="text" style="width:80px;" placeholder="-"></td>
                            <td class="text-center"><input class="form-control" type="text" style="width:80px;" placeholder="-"></td>
                            <td class="text-center"><input class="form-control" type="text" style="width:80px;" placeholder="-"></td>
                            <td class="text-center"><input class="form-control" type="text" style="width:80px;" placeholder="-"></td>
                            <td class="text-center"><input class="form-control" type="text" style="width:80px;" placeholder="-"></td>

                        </tr>
                        </tbody> 
                        </table>
                  </div>

                  </div>

                    <div class="row m-auto mb-3"> 
                    <div class="col-12 d-flex justify-content-center"> 
                    <a href="#" class="btn btn-social btn-min-width mb-1 mr-1 round" role="button" style="background-color:green; color:white;">
                        <span class="la la-save"style="color:white; font-weight: bold;font-size: 18px"></span>บันทึก</a> 
                    <a href="#" class="btn btn-social btn-min-width mb-1 round" role="button" style="background-color:red; color:white;">
                        <span class="fa fa-times-circle-o"style="color:white; font-weight: bold;font-size: 18px"></span>ยกเลิก</a> 
                    </div>
                    </div>
              
            
            
        </form>


        </div> 
    </div> 
</div>

  <!-- ///////////////////////////////////////////// -->
 
  
  

    

  <!-- footer -->
  <?php include '../include/footer.php'; ?>
  <script src="../../Controllers/Ducklab/duck.script.js"></script>
   
  <script type="text/javascript">
    $(document).ready(function() { 
     
    }); 
  </script>
  