  <!-- header -->
  <?php include '../include/header.php'; ?>
  <?php include '../../Model/Ducklab/func.php'; ?>
  <?php  
    //Recruitment and appointment
    $menu1 = "PERSONASSIGN" ;
    $menu2 = "assignCalculate_index" ;

  ?>
  <!-- menu -->
  <?php include '../include/menu.php'; ?>
  
  <section>
    <div class="app-content content">
      <div class="content-wrapper">
                <div class="content-header row">
                        <div class="content-header-left col-md-6 col-12 mb-2">
                            <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
                            <h3 class="content-header-title">การบรรจุกำลังพล </h3>
                        </div>
                </div>
                    <nav aria-label="breadcrumb">
                    <ol class="breadcrumb"> 
                        <li class="breadcrumb-item"><a href="../home/index.php">หน้าแรก </a></li>  
                        <li class="breadcrumb-item active" aria-current="page"> การบรรจุกำลังพล ประจำปี 2562 </li>
                    </ol>
                    </nav>


        <div class="content-body">
                                    <!-- Bootstrap 3 table -->
                                    <section id="bootstrap3">
                                        <div class="row">
                                        <div class="col-12">
                                            <div class="card">
                                            <div class="card-content collapse show">
                                                <div class="card-body card-dashboard">
                                                
                                                <div class="row"> 
                                                    <div class="col-sm-12">
                                                    <div class="box_h1 card">
                                                        <div class="card-header card-head-inverse  ">
                                                        <h4 class="card-title text-white"> ค้นหา </h4> 
                                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                                        <div class="heading-elements">
                                                            <ul class="list-inline mb-0">
                                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                                                            </ul>
                                                        </div>
                                                        </div>
                                                        <div class="card-content collapse show">
                                                        <div class="card-body">
                                                            <div class="row">
                                                            <div class=" col-md-8  m-auto ">
                                                            
                                                                <form class="form">
                                                                <div class="form-body"> 

                                                                    <div class="row">
                                                                        <div class="col-md-3">

                                                                        <div class="form-group">  
                                                                        <label for="exampleInputEmail1">แผนการบรรจุ:</label>
                                                                            <select id="projectinput5" name="interested" class="form-control ">
                                                                                <option value=""></option>
                                                                                <option value=""></option>
                                                                                <option value=""></option>
                                                                            </select>
                                                                        </div>

                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <div class="form-group"> 
                                                                                <label for="exampleInputEmail1">ปีงบประมาณ:</label>
                                                                                <input class="form-control" type="text" placeholder="Default input">
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-3">
                                                                        <label for="exampleInputEmail1">ประเภทกำลังพล:</label>
                                                                            <select id="projectinput5" name="interested" class="form-control ">
                                                                                <option value=""></option>
                                                                                <option value=""></option>
                                                                                <option value=""></option>
                                                                            </select>
                                                                            </div>
                                                                        </div>
                                                                        </div>
                                                                    
                                                                        <div class="row">
                                                                        <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="exampleInputEmail1">คุณวุฒิ:</label>
                                                                            <select id="" name="" class="form-control">
                                                                            <option value="">มัธยมศึกษาตอนต้น</option>
                                                                            <option value="">มัธยมศึกษาตอนปลาย</option>
                                                                            <option value="">ปวช.</option>
                                                                            <option value="">ปวส.</option>
                                                                            <option value="">ปริญญาตรี</option>
                                                                            <option value="">ปริญญาโท</option>
                                                                            <option value="">ปริญญาเอก</option>
                                                                            </select>
                                                                        </div>
                                                                        </div> 

                                                                        <div class="col-md-6">
                                                                        <div class="form-group">  
                                                                        <label for="exampleInputEmail1">ตำแหน่ง:</label>
                                                                            <select id="projectinput5" name="interested" class="form-control ">
                                                                                <option value=""></option>
                                                                                <option value=""></option>
                                                                                <option value=""></option>
                                                                            </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                
                                                                    </div>  <!-- ./boxsearchplan2 -->
                                                                
                                                                </div> 

                                                                <div class="text-center">
                                                                    <button type="button" class="btn round btn1 btncustom1 mr-1">
                                                                    <i class="la la-search"></i> ค้นหา
                                                                    </button>
                                                                    <button type="button" class="btn round btn1 btncustom1">
                                                                    <i class="la la-repeat"></i> ล้างค่า
                                                                    </button>
                                                                </div>

                                                                </form>

                                                            </div>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div> 
                                                
                                                <div class="row px-2"> 
                                                    <div class="col-md-3 col-sm-12">
                                                        <div class="form-group">  
                                                        <select  id="filter1" class="  form-control block " >
                                                            <option value=""> ยังไม่ลงตำแหน่ง </option> 
                                                            <option value=""> ลงตำแหน่งแล้ว</option> 
                                                        </select>
                                                        </div> 
                                                    </div> 

                                                    
                                                </div>

                                                <div class="row px-2"> 
                                                    <div class="col-sm-12">
                                                    <div class="box1 txtdesc text-bold-600">
                                                        ข้อมูลจำนวน <span> 500 </span> อัตรา  
                                                    </div>
                                                    </div> 

                                                    <div class="col-sm-12">    
                                                    <div class="box1 txtdesc">
                                                        หมายเหตุ <div class="ele1"></div> แถบสีแดง หมายถึง รายการที่ยังไม่ได้ทำรายการแก้ไขข้อมูลและบรรจุ โดยการบรรจุจำเป็นต้องระบุเลขข้าราชการ จึงจะเสร็จสิ้นกระบวนการบรรจุ
                                                    </div>
                                                    </div> 

                                                </div>


                                                <div class="row px-2">
                                                    <div class="col-6 ">
                                                    <button class="btn round btn1 btncustom1">  <i class="la la-print"></i> <span > ปริ้น </span> </button>
                                                    <button class="btn round btn1 btncustom1">  <i class="la la-file-excel-o"></i> <span > Export Excel</span> </button>
                                                    </div>
                                                    <div class="col-6 text-right"> 
                                                    <button class="btn round btn1 btncustom1"  data-toggle="modal" data-target="#modalSettingCalcu"> <i class="la la-calculator"></i> <span >คำนวณเลขราชการ </span> </button>
                                                    <button class="btn round btn1 btncustom1"  data-toggle="modal" data-target="#modalSettingAssign">  <i class="la la-download"></i> <span > Import Excel </span> </button>
                                                    </div>
                                                </div>


                                                    <div class="row">
                                                    <div class="col-12">
                                                        <div class="table-responsive">
                                                        <table id="planManpowerTable3" class="table table-bordered tb1 table-striped table-hover"  >
                          <thead>
                            <tr>
                              <th> <input type="checkbox" class="checkAll"
                                                              onclick="toggle(this);" />   </th>
                              <th> ลำดับที่ </th>
                              <th> กลุ่ม </th>
                              <th> รหัสตำแหน่ง </th>
                              <th style="width:20%;"> คุณวุฒิ </th>
                              <th> ตำแหน่ง</th> 
                              <th> เพศ </th> 
                              <th> อัตรา </th>
                              <th> จำนวนอัตรา </th>
                              <th> เหล่า </th> 
                              <th> จังหวัด</th> 
                              <th> ลชทอ. </th>
                              <th> จำพวก </th>    
                              <th> ชื่อสกุล</th>  
                            </tr>
                          </thead>

                          <tbody> 
                            <tr class="bordertr1"> 
                              <td rowspan="3" class="text-center"> 
                                <a><input type="checkbox" class="checkAll" /> </a>
                                <a href="profile.php" class="fontcolor1" > <i class="la la-pencil-square-o"></i>  </a>
                              </td>
                              <td rowspan="3" class="text-center"> 1 </td> 
                              <td rowspan="3" class="text-center"> DOF-01</td>
                              <td rowspan="3" class="text-center"> 30130</td>
                              <td rowspan="3">ประกาศนียบัตรวิชาชีพชั้นสูง สาขาวิชาปฎิบัติการฉุกเฉินการแพทย์หรือเวชกิจฉุกเฉินและได้รับใบประกาศนียบัตรเจ้าพนักงานฉุกเฉินการแพทย์ </td>

                                    <td > พยาบาล แผนกปฎิบัติการ 2 กปก.ศปพ.พอ. </td>   
                                    <td> ชาย/หญิง </td> 
                                    <td class="text-center"> จ.อ. </td>  
                                    <td class="text-center"> 2 </td>
                                    <td class="text-center"> น.บ. </td> 
                                    <th class="text-center"> สุราษธานี</th> 
                                    <td class="text-center"> 1413</td>
                                    <td class="text-center"> สารบรรณ </td> 
                                    <td class="text-center"></td>   

                            </tr> 

                            <tr>
                                <td >  พยาบาล แผนกแพทย์ กรก.รร.นน. </td>   
                                <td> ชาย/หญิง </td> 
                                <td class="text-center"> จ.อ. </td>  
                                <td class="text-center"> 2 </td>
                                <td class="text-center"> น.บ. </td>
                                <th class="text-center"> สุราษธานี</th> 
                                <td class="text-center"> 1413</td>
                                <td class="text-center"> สารบรรณ </td> 
                                <td class="text-center"> </td>   
                            </tr>

                            <tr>
                            <td >  พยาบาล แผนกแพทย์ กรก.รร.นน. </td>    
                                <td> ชาย/หญิง </td> 
                                <td class="text-center"> จ.อ. </td>  
                                <td class="text-center"> 2 </td>
                                <td class="text-center"> น.บ. </td> 
                                <th class="text-center"> สุราษธานี</th> 
                                <td class="text-center"> 1413</td>
                                <td class="text-center"> สารบรรณ </td> 
                                <td class="text-center"> </td>   
                            </tr>

                                                                <!-- /// -->

                            <tr class="bordertr1"> 
                              <td rowspan="3" class="text-center"> 
                                <a><input type="checkbox" class="checkAll" /> </a>
                                <a href="profile.php" class="fontcolor1" > <i class="la la-pencil-square-o"></i>  </a>
                              </td>
                              <td rowspan="3" class="text-center"> 1 </td> 
                              <td rowspan="3" class="text-center"> DOF-01</td>
                              <td rowspan="3" class="text-center"> 30130</td>
                              <td rowspan="3">ประกาศนียบัตรวิชาชีพชั้นสูง สาขาวิชาปฎิบัติการฉุกเฉินการแพทย์หรือเวชกิจฉุกเฉินและได้รับใบประกาศนียบัตรเจ้าพนักงานฉุกเฉินการแพทย์ </td>

                              <td > พยาบาล แผนกปฎิบัติการ 2 กปก.ศปพ.พอ. </td>   
                                    <td> ชาย/หญิง </td> 
                                    <td class="text-center"> จ.อ. </td>  
                                    <td class="text-center"> 2 </td>
                                    <td class="text-center"> น.บ. </td> 
                                    <th class="text-center"> สุราษธานี</th> 
                                    <td class="text-center"> 1413</td>
                                    <td class="text-center"> สารบรรณ </td> 
                                    <td class="text-center"> </td>   

                            </tr> 

                            <tr>
                            <td > พยาบาล แผนกปฎิบัติการ 2 กปก.ศปพ.พอ. </td>   
                                <td> ชาย/หญิง </td> 
                                <td class="text-center"> จ.อ. </td>  
                                <td class="text-center"> 2 </td>
                                <td class="text-center"> น.บ. </td> 
                                <th class="text-center"> สุราษธานี</th> 
                                <td class="text-center"> 1413</td>
                                <td class="text-center"> สารบรรณ </td> 
                                <td class="text-center"> </td>   
                            </tr>

                            <tr>
                            <td > พยาบาล แผนกปฎิบัติการ 2 กปก.ศปพ.พอ. </td>   
                                <td> ชาย/หญิง </td> 
                                <td class="text-center"> จ.อ. </td>  
                                <td class="text-center"> 2 </td>
                                <td class="text-center"> น.บ. </td>
                                <th class="text-center"> สุราษธานี</th>  
                                <td class="text-center"> 1413</td>
                                <td class="text-center"> สารบรรณ </td> 
                                <td class="text-center"> </td>   
                            </tr>

                            <tr class=""> 
                              <td rowspan="3" class="text-center"> 
                                <a><input type="checkbox" class="checkAll" /> </a>
                                <a href="profile.php" class="fontcolor1" > <i class="la la-pencil-square-o"></i>  </a>
                              
                              </td>
                              <td rowspan="3" class="text-center"> 1 </td> 
                              <td rowspan="3" class="text-center"> DOF-01</td>
                              <td rowspan="3" class="text-center"> 30130</td>
                              <td rowspan="3">ประกาศนียบัตรวิชาชีพชั้นสูง สาขาวิชาปฎิบัติการฉุกเฉินการแพทย์หรือเวชกิจฉุกเฉินและได้รับใบประกาศนียบัตรเจ้าพนักงานฉุกเฉินการแพทย์ </td>

                              <td > พยาบาล แผนกปฎิบัติการ 2 กปก.ศปพ.พอ. </td>   
                                    <td> ชาย/หญิง </td> 
                                    <td class="text-center"> จ.อ. </td>  
                                    <td class="text-center"> 2 </td>
                                    <td class="text-center"> น.บ. </td>
                                    <th class="text-center"> สุราษธานี</th>  
                                    <td class="text-center"> 1413</td>
                                    <td class="text-center"> สารบรรณ </td> 
                                    <td class="text-center"> นายกิตติศักดิ์ สมประกอบ</td>   

                            </tr> 

                            <tr>
                            <td > พยาบาล แผนกปฎิบัติการ 2 กปก.ศปพ.พอ. </td>   
                                <td> ชาย/หญิง </td> 
                                <td class="text-center"> จ.อ. </td>  
                                <td class="text-center"> 2 </td>
                                <td class="text-center"> น.บ. </td> 
                                <th class="text-center"> สุราษธานี</th> 
                                <td class="text-center"> 1413</td>
                                <td class="text-center"> สารบรรณ </td> 
                                <td class="text-center"> นายกิตติศักดิ์ สมประกอบ</td>   
                            </tr>

                            <tr>
                            <td > พยาบาล แผนกปฎิบัติการ 2 กปก.ศปพ.พอ. </td>   
                                <td> ชาย/หญิง </td> 
                                <td class="text-center"> จ.อ. </td>  
                                <td class="text-center"> 2 </td>
                                <td class="text-center"> น.บ. </td> 
                                <th class="text-center"> สุราษธานี</th> 
                                <td class="text-center"> 1413</td>
                                <td class="text-center"> สารบรรณ </td> 
                                <td class="text-center"> นายกิตติศักดิ์ สมประกอบ</td>   
                            </tr>
                           

                          </tbody>
                        </table>

                                                        </div>
                                                    </div>
                                                    </div>


                                                    <div class="text-center"> 
                                                    <button class="btn round btn1 btncustom1">  <i class="la la-send"></i> <span > ร่างคำสั่ง </span> </button>
                                                    </div>



                                                

                                                        </div>
                                                        </div>

                                                    </div>
                                                    </div>
                                                </div>
                                                
                                            </section>
                                            <!--/ Bootstrap 3 table -->
                </div>
      </div>
    </div>
  </section>

  
  <!-- Modal  id="modalSettingAssign" -->
  <div class="modal fade text-left modal_custom1" id="modalSettingAssign" tabindex="-1" role="dialog" aria-labelledby="modalSettingAssign"  aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header bg_custom1">
            <h5 class="modal-title"> บันทึก/แก้ไข วันเวลาเปิด/ปิด รับสมัคร</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form class="form_custom1">
            <div class="modal-body">
              <div class="row">
                <div class="col-8 m-auto">  

                <div class="boxsearchplan2">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">  
                        <label for="positionno"> ปีงบประมาณ : </label>
                        <input type="text" class="form-control" id="" name="" value="2562" disabled >
                      </div>
                    </div>  
                    <div class="col-md-6">
                      <div class="form-group">  
                        <label for="positionno"> ชื่อแผน : </label>
                        <input type="text" class="form-control" id="" name="" value="แผนการบรรจุประจำปี 2562" disabled>
                      </div>
                    </div> 
                    
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="pb-2 hidden">
                        <span class="fontcustom1"> *กรุณากดปุ่ม Import เพื่อนำเข้าข้อมูล เพื่อสามารถกรอกข้อมูลในลำดับถัดไปได้ </span>
                      </div>
                      <button type="button" class="btn round btn1 btncustom1 mr-1">
                        <i class="fa fa-upload"></i>&nbsp; Import 
                      </button> 
                      <span class="boxcustom3">  ข้อมูลผู้มีรายชื่อสอบผ่านจริงและสำรอง2562.xlsx
                        <a href="#" class="btn round fontcolor2"> 
                          <i class="fa fa-times-circle-o"></i>
                        </a>
                      </span>
                    </div>   
                  </div> 
                </div>
                  
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <div  class="m-auto ">
                <button type="button" class="btn btn2 btn-success   round"   data-dismiss="modal"> <i class="fa fa-save"></i> บันทึก </button>
                <button type="button" class="btn btn2 btn-danger   round"  data-dismiss="modal"> <i class="fa fa-times-circle-o"></i> ยกเลิก </button> 
              </div>
            </div>
          </form>
        </div>
      </div>
  </div>

  <!-- Modal  id="#modalSettingCalcu"" -->
  <div class="modal fade text-left modal_custom1" id="modalSettingCalcu" tabindex="-1" role="dialog" aria-labelledby="modalSettingCalcu"  aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header bg_custom1">
            <h5 class="modal-title"> คำนวณเลขราชการ</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <form class="form_custom1">
            <div class="modal-body">
              <div class="row">
                <div class="col-8 m-auto">  

                
                  
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <div  class="m-auto ">
                <button type="button" class="btn btn2 btn-success   round"   data-dismiss="modal"> <i class="fa fa-save"></i> คำนวณ </button>
                <button type="button" class="btn btn2 btn-danger   round"  data-dismiss="modal"> <i class="fa fa-times-circle-o"></i> ยกเลิก </button> 
              </div>
            </div>
          </form>
        </div>
      </div>
  </div>

  <!-- footer -->
  
  
  <?php include '../include/footer.php'; ?> 
  <script src="../../Controllers/planManpowerAssignController.js"></script>
  <script type="text/javascript">
    $(document).ready(function() { 
 

    }); 
  </script>
  