  <!-- header -->
  <?php include '../include/header.php'; ?>
  <?php include '../../Model/Ducklab/func.php'; ?>
  <?php  
    //Recruitment and appointment
    $menu1 = "PERSONASSIGN" ;
    $menu2 = "PlanManpower" ;

  ?>
  <!-- menu -->
  <?php include '../include/menu.php'; ?>
  
  <section>
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
          <h3 class="content-header-title"> การบรรจุกำลังพล </h3>
        </div>
      </div>
      <nav aria-label="breadcrumb">
      <ol class="breadcrumb"> 
            <li class="breadcrumb-item"><a href="../home/index.php">หน้าแรก </a></li>  
            <li class="breadcrumb-item "  > <a href="index.php">การบรรจุกำลังพล ประจำปี 2562 </a> </li>
            <li class="breadcrumb-item active" aria-current="page">  การบรรจุ </li>
          </ol>
      </nav>
      <div class="content-body">
         <!-- Bootstrap 3 table -->
         <section id="bootstrap3">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard"> 
                  
   
                    <div class="row"> 
                      <div class="col-md-12 ">
                        <div class="box_h1 card">
                          <div class="card-header card-head-inverse">
                            <h4 class="card-title  text-white">  กรอกข้อมูลตำแหน่ง </h4> 
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                              </ul>
                            </div>
                          </div>
                          <div class="card-content collapse show">
                            <div class="card-body">
                              <div class="row">
                                <div class=" col-md-10 m-auto ">  

                                  <div class="row">
                                    <div class=" col-md-12 ">  
                                      <div class="box1 txtdesc">
                                        <p>  ปีงบประมาณ : 2562  </p> 
                                        <p>  ข้อมูลจำนวนตำแหน่ง <span> 5 </span> ตำแหน่ง  จำนวน 10 คน </p>
                                        <p> <span class="fontcustom1">  
                                            หมายเหตุ : กรุณาเลือกรายชื่อผู้สมัครเพื่อลงตำแหน่ง </span>
                                        </p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class=" col-md-12 ">   
                                      <table id="planManpowerTable2" class="table  tb1   "  >
                                        <thead>
                                          <tr>
                                            <th> ลำดับที่ </th> 
                                            <th> ตำแหน่ง  </th>
                                            <th>  จังหวัด </th> 
                                            <th> ชื่อ -สกุล  </th>  
                                          </tr>
                                        </thead>
                                        <tbody> 
                                        
                                          <tr>
                                            <td class="text-center"> 1 </td>
                                            <td class=""> อาจารย์ ภาควิชาการพยาบาลผู้ใหญ่ กองการศึกษา วพอ.พอ.  </td> 
                                            <td class="text-center">
                                             กรุงเทพมหานคร
                                            </td>  
                                            <td class="formset1">   
                                              <div class="form-group">
                                                <select class="form-control select2  "  style="width:100%" >
                                                  <option value="">  นางสาวจริงใจ  เสริมดี </option>
                                                </select> 
                                              </div>
                                              
                                            </td>
                                          </tr> 

                                          <tr>
                                            <td class="text-center"> 2 </td>
                                            <td class=""> นายทหารพัฒนากาลัง แผนกพัฒนากาลังพล กคพ.สปพ.กพ.ทอ. </td> 
                                            <td class="text-center">
                                             นครนายก 
                                             <br>
                                             ลพบุรี
                                            </td>  
                                            <td class="formset1">   
                                              <div class="form-group">
                                                <select class="form-control select2  "  style="width:100%" >
                                                  <option value="">  นางสาวจริงใจ  เสริมดี </option>
                                                </select> 
                                              </div>
                                              <div class="form-group">
                                                <select class="form-control select2"  style="width:100%">
                                                  <option value="">  นางสาวจริงใจ  เสริมดี </option>
                                                </select> 
                                              </div>
                                            </td>
                                          </tr> 

                                          <tr>
                                            <td class="text-center"> 3 </td>
                                            <td class=""> นายทหารนโยบายและแผน ผนผ.กนผ.สนพ.กพ.ทอ. </td> 
                                            <td class="text-center">
                                              เชียงใหม่
                                              <br> 
                                              ขอนแก่น
                                            </td>
                                            <td class="formset1">    
                                              <div class="form-group">
                                                <select class="form-control select2  "  style="width:100%" >
                                                  <option value="">  นางสาวจริงใจ  เสริมดี </option>
                                                </select> 
                                              </div>
                                              <div class="form-group">
                                                <select class="form-control select2"  style="width:100%">
                                                  <option value="">  นางสาวจริงใจ  เสริมดี </option>
                                                </select> 
                                              </div>
                                            </td>
                                          </tr>

                                          <tr>
                                            <td class="text-center"> 4 </td>
                                            <td class=""> นายทหารรายงานและหลักฐาน แปนกรายงานและหลักฐาน   </td>
                                            <td class="text-center">   

                                            กรุงเทพมหานคร
                                            <br> 
                                              ขอนแก่น
                                              <br> 
                                              สงขลา
                                            </td>  
                                            <td class="formset1">   
                                              <div class="form-group">
                                                <select class="form-control select2  "  style="width:100%" >
                                                  <option value="">  นางสาวจริงใจ  เสริมดี </option>
                                                </select> 
                                              </div>
                                              <div class="form-group">
                                                <select class="form-control select2"  style="width:100%">
                                                  <option value="">  นางสาวจริงใจ  เสริมดี </option>
                                                </select> 
                                              </div>
                                              <div class="form-group">
                                                <select class="form-control select2"  style="width:100%">
                                                  <option value="">  นางสาวจริงใจ  เสริมดี </option>
                                                </select> 
                                              </div>
                                            

                                            </td>
                                          </tr>

                                        </tbody>
                                      </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>  <!-- /.row  -->

 

                    <div class="row"> 
                      <div class="col-sm-10 m-auto">
                        <div  class="text-center">
                          <button type="button" class="btn btn2 btn-success   round" data-toggle="modal" data-target="#modalConfirm" > <i class="fa fa-save"></i> บันทึก </button>
                          <button type="button" class="btn btn2 btn-danger   round"  data-dismiss="modal"> <i class="fa fa-times-circle-o"></i> ยกเลิก </button> 
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--/ Bootstrap 3 table -->
      </div>
    </div>
  </div>
  </section>


  <div class="modal fade text-left modal_custom1" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="modalConfirm"  aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <form>
          <div class="my-2 mx-2 px-2 py-2">
            <div class="text-center"> 
              <img src="../../Asset/Images/send.png">
              <div class="py-3">
                <h1> ยืนการส่งข้อมูลหรือไม่ ?</h1>
                <h6>กรุณาเลือกปุ่มใช่หรือไม่ใช่่</h6>
              </div>
            </div> 
            <div class="col-12 d-flex justify-content-center">
              <div class="row"> 
                  <a href="#" class="btn btn-min-width mb-1 mr-1 round btn-success" role="button" >
                  <span></span>ใช่</a> 
                  <a href="#" class="btn btn-min-width mb-1 round btn-danger"   data-dismiss="modal" role="button">
                  <span></span> ไม่ใช่ </a> 
              </div>
            </div>
          </div>
        </form>
      </div> 
    </div> 
  </div>


 
  <!-- footer -->
  <?php include '../include/footer.php'; ?> 

  <script src="../../Controllers/planManpowerController.js"></script>
  <script type="text/javascript">
    $(document).ready(function() { 

      $("#planManTypeId").change(function(){
        // alert( $(this).val() );
         $('.tb1').addClass('hidden');

         $('#planManTypeId2').addClass('hidden'); 
         if( $(this).val() == 1 || $(this).val() == 0){
            $('#planManpowerTable1').removeClass('hidden');
         }else if( $(this).val() ==2 ){ 
            $('#planManTypeId2').removeClass('hidden'); 
            $('#planManpowerTable2').removeClass('hidden'); 
         }else if( $(this).val() == 3 ) {
            $('#planManpowerTable4').removeClass('hidden');
         }

      });

      $("#planManTypeId2").change(function(){
        $('.tb1').addClass('hidden');
         if( $(this).val() ==22 ){
            $('#planManpowerTable3').removeClass('hidden'); 
          }else{
            $('#planManpowerTable2').removeClass('hidden'); 
          }
      });


    }); 
  </script>
  