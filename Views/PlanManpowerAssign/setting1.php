  <!-- header -->
  <?php include '../include/header.php'; ?>
  <?php  
  //Recruitment and appointment
  $menu1 = "PERSONASSIGN" ;
  $menu2 = "PSETTING" ;
  $menu3 = "SETTING1" ;
  ?>
  <!-- menu -->
  <?php include '../include/menu.php'; ?>
   
  <style>
      /* ol > li > a {color:#222233;} */
      .toggle.ios,
      .toggle-on.ios,
      .toggle-off.ios {
          border-radius: 20rem;
      }

      .toggle.ios .toggle-handle {
          border-radius: 20rem;
      }
  </style>

<section>

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-12">
                <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
                    <h3 class="content-header-title">ตั้งค่าปริญญาและคุณวุฒิ</h3>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="../home/index.php">ระบบงานสรรหาและบรรจุกำลังพล</a></li>
                                <li class="breadcrumb-item active" aria-current="page">ตั้งค่าปริญญาและคุณวุฒิ</li>
                                
                            </ol>
                        </nav>
            </div>
        </div>


<div class="shadow p-0 bg-white rounded">
    <div class="card collapse-icon accordion-icon-rotate active">
        <div id="headingCollapse31" class="card-header">
        <a data-toggle="collapse" href="#collapse31" aria-expanded="true" aria-controls="collapse31" class="card-title lead white" style="color:white;" >
        <h6 style="background-color:#0f1733;color:white;line-height:40px;padding-left:10px;">บันทึก/แก้ไข ปริญญาและคุณวุฒิ</h6></a></div>
        <div id="collapse31" role="tabpanel" aria-labelledby="headingCollapse31" class="card-collapse collapse show" aria-expanded="true" >  


<div class="row my-1 px-2"> 
<div class="col-lg-10 m-auto">  

<div>
 
 <button class="btn round btn1 btncustom1">  <i class="la la-plus-circle"></i> <span > เพิ่ม </span> </button>                         
</div>
    <div class="table-responsive">
    <table class="table table-striped table-border table-hover">

        <thead class="text-center">
            <tr class="justify-content-center"style="background-color:#0f1733; color:whitesmoke;">
                <th style="width:100px;"></th>
                <th style="width:100px;">ลำดับ</th>
                <th>คุณวุฒิ</th>
                <th>เงินขั้นต่ำ</th>
                <th>สถานะ</th>
            </tr>
        </thead>
        <tbody class="text-center">

            <tr class="justify-content-center">
                    <td><a href="#"><i class="la la-trash-o" style="color:#0f1733;"></a></i></td>
                    <td>1</td>
                    <td><input class="form-control" type="text" placeholder="น.ประทวนเลือนฐานะ"></td>
                    <td><input class="form-control" type="text" placeholder="8000"></td>
                    <td>  <input type="checkbox" checked data-toggle="toggle"
                                                              data-style="ios" data-on="ใช้งาน" data-off="ยกเลิก"
                                                              data-onstyle="success" data-offstyle="danger"
                                                              data-size="sm"></td>
            </tr>

            <tr class="justify-content-center">
                    <td><a href="#"><i class="la la-trash-o" style="color:#0f1733;"></a></i></td>
                    <td>2</td>
                    <td><input class="form-control" type="text" placeholder="ปริญญาตรีหลักสูตร 4 ปี"></td>
                    <td><input class="form-control" type="text" placeholder="10000"></td>
                    <td>  <input type="checkbox" checked data-toggle="toggle"
                                                              data-style="ios" data-on="ใช้งาน" data-off="ยกเลิก"
                                                              data-onstyle="success" data-offstyle="danger"
                                                              data-size="sm"></td>
            </tr>

            <tr class="justify-content-center">
                    <td><a href="#"><i class="la la-trash-o" style="color:#0f1733;"></a></i></td>
                    <td>3</td>
                    <td><input class="form-control" type="text" placeholder="ปริญญาตรีหลักสูตร 5 ปี"></td>
                    <td><input class="form-control" type="text" placeholder="14000"></td>
                    <td>  <input type="checkbox" checked data-toggle="toggle"
                                                              data-style="ios" data-on="ใช้งาน" data-off="ยกเลิก"
                                                              data-onstyle="success" data-offstyle="danger"
                                                              data-size="sm"></td>
            </tr>

            
            <tr class="justify-content-center">
                    <td><a href="#"><i class="la la-trash-o" style="color:#0f1733;"></a></i></td>
                    <td>4</td>
                    <td><input class="form-control" type="text" placeholder="ปริญญาตรีหลักสูตร 6 ปี"></td>
                    <td><input class="form-control" type="text" placeholder="18000"></td>
                    <td>  <input type="checkbox" checked data-toggle="toggle"
                                                              data-style="ios" data-on="ใช้งาน" data-off="ยกเลิก"
                                                              data-onstyle="success" data-offstyle="danger"
                                                              data-size="sm"></td>
            </tr>

            <tr class="justify-content-center">
                    <td><a href="#"><i class="la la-trash-o" style="color:#0f1733;"></a></i></td>
                    <td>5</td>
                    <td><input class="form-control" type="text" placeholder=""></td>
                    <td><input class="form-control" type="text" placeholder=""></td>
                    <td>  <input type="checkbox" checked data-toggle="toggle"
                                                              data-style="ios" data-on="ใช้งาน" data-off="ยกเลิก"
                                                              data-onstyle="success" data-offstyle="danger"
                                                              data-size="sm"></td>
            </tr>

            <tr class="justify-content-center">
                    <td><a href="#"><i class="la la-trash-o" style="color:#0f1733;"></a></i></td>
                    <td>6</td>
                    <td><input class="form-control" type="text" placeholder=""></td>
                    <td><input class="form-control" type="text" placeholder=""></td>
                    <td>  <input type="checkbox" checked data-toggle="toggle"
                                                              data-style="ios" data-on="ใช้งาน" data-off="ยกเลิก"
                                                              data-onstyle="success" data-offstyle="danger"
                                                              data-size="sm"></td>
            </tr>                         
                                                  
                                                 
        </tbody>                               
    </table>
    </div>   
</div>
</div>




<div class="row my-2"> 
            <div class="col-12 d-flex justify-content-center"> 
                <a href="#" class="btn  mb-1 mr-1 round btn-success" role="button" >
                    <span class="fa fa-save"></span> บันทึกข้อมูล</a> 
                <a href="#" class="btn mb-1 round btn-danger" role="button">
                    <span class="fa fa-close"> </span> ยกเลิก </a> 
            </div>       
</div>

<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
</div>
</section>


                  
            
     
  <!-- footer -->
  <?php include '../include/footer.php'; ?> 