  <!-- header -->
  <?php include '../include/header.php'; ?>
  <?php include '../../Model/Ducklab/func.php'; ?>
  <?php  
    //Recruitment and appointment
    $menu1 = "PERSONASSIGN" ;
    $menu2 = "PlanManpowerAssign" ;

  ?>
  <!-- menu -->
  <?php include '../include/menu.php'; ?>
  
  <section>
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
          <h3 class="content-header-title"> การบรรจุกำลังพล </h3>
        </div>
      </div>
      <nav aria-label="breadcrumb">
      <ol class="breadcrumb"> 
            <li class="breadcrumb-item"><a href="../home/index.php">หน้าแรก </a></li>  
            <li class="breadcrumb-item "  > <a href="/index.php">การบรรจุกำลังพล ประจำปี 2562 </a> </li>
            <li class="breadcrumb-item active" aria-current="page"> ข้อมูลผู้รับการบรรจุ </li>
          </ol>
      </nav>
      <div class="content-body">
         <!-- Bootstrap 3 table -->
         <section id="bootstrap3">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <p class="card-text"></p> 
                    <div class="row"> 
                      <div class="col-sm-12">
                        <div class="box1 txtdesc">
                            ข้อมูลการบรรจุ
                        </div>
                      </div> 
                    </div>

                    <div class="row"> 
                      <div class="col-12">
                        <div class="box_h1 card">
                          <div class="card-header card-head-inverse  ">
                            <h4 class="card-title text-white"> ประวัติ  </h4> 
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                              </ul>
                            </div>
                          </div>
                          <div class="card-content collapse show">
                            <div class="card-body">
                              <div class="row">
                                <div class=" col-md-2 "> 
                                    <img src="/Asset/Images/user1.png" class="img_profile" >
                                </div>

 
                                <div class=" col-md-10 fontcustom2">  

                                  <div class="row">
                                    <div class=" col-md-12 ">
                                      <p> หมายเลขประจำตัว : 3232665970</p>
                                    </div> 
                                  </div> 
                                  <div class="row">
                                    <div class=" col-md-12 ">
                                      <p> ชื่อ-สกุล : ร.อ. กิตติศักดิ์  สวนสอน </p>
                                    </div> 
                                  </div>
                                  <div class="row">
                                    <div class=" col-md-6 ">
                                      <p> เหล่า : สบ.  </p>
                                    </div> 
                                    <div class=" col-md-6 ">
                                      <p> จำพวก : กำลังพล </p>
                                    </div> 
                                  </div>
                                  <div class="row">
                                    <div class=" col-md-12 ">
                                      <p> สังกัด : กพ.ทอ.  </p>
                                    </div>  
                                  </div>
                                  <div class="row">
                                    <div class=" col-md-12 ">
                                      <p> ตำแหน่ง : ปฏิบัติการเครื่องจักรคำนวณ แผนกวิเคราะห์ </p>
                                    </div>  
                                  </div>
                                  <div class="row">
                                    <div class=" col-md-6 ">
                                      <p> วันเดือนปีที่บรรจุ :  20 มกราคม 2560  </p>
                                    </div> 
                                    <div class=" col-md-6 ">
                                      <p> วันเดือนปียศปัจจุบัน :  20 มกราคม 2560  </p>
                                    </div>   
                                  </div>
                                  <div class="row">
                                    <div class=" col-md-6 ">
                                      <p> คุณวุฒิ :  ปริญญาตรี </p>
                                    </div> 
                                    <div class=" col-md-6 ">
                                      <p> การครองยศ :  -  </p>
                                    </div>   
                                  </div>

                                  <div class="row">
                                    <div class=" col-md-12 ">
                                       <button type="button" class="btn round btn1   btn-lg mr-1 mb-2">
                                            คำนวณเลขข้าราชการ 
                                        </button>
                                        <span class="fontcustom1">  
                                          กรุณากดปุ่มเพื่อให้ระบบคำนวณเลขข้าราชการ  </span>
                                    </div>  
                                  </div> 

                                </div>

                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div> 
 
                    <div class="row hidden"> 
                      <div class="col-md-3 col-sm-12">
                        <div class="box1 txtdesc">
                            ข้อมูลจำนวน <span> 500 </span> อัตรา
                        </div>
                      </div> 
                    </div>
                    <div class="row hidden">
                      <div class="col-6 ">
                         <button class="btn round btn1 btncustom1">  <i class="la la-print"></i> <span > ปริ้น </span> </button>
                         <button class="btn round btn1 btncustom1">  <i class="la la-file-excel-o"></i> <span > Export Excel</span> </button>
                      </div>
                      <div class="col-6 text-right">
                        <a href="" class="fontcolor1" data-toggle="modal" data-target="#modalPlanPublic"  > <i class="la la-pencil-square-o"></i>  </a>

                        <button class="btn round btn1 btncustom1"  data-toggle="modal" data-target="#modalSettingRegis">  <i class="la la-calendar"></i> <span >  เปิด/ปิด รับสมัคร </span> </button>
                      </div>
                    </div>

                    <div class="row"> 
                      <div class="col-md-12">
                         
                      </div> 
                    </div>

                    <div class="row"> 
                      <div class="col-md-12 col-lg-10 m-auto">
                        <div class="box_h2 card">
                          <div class="card-header">
                            <h4 class="card-title"><span class="fontcolor1"><u> ส่วนที่ 1</u> </span>&nbsp;&nbsp; รายละเอียดข้อมูล </h4> 
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                              </ul>
                            </div>
                          </div>
                          <div class="card-content collapse show">
                            <div class="card-body">
                              <div class="row">
                                <div class=" col-md-8  m-auto ">
                                 
                                  <form class="form form_custom1 ">
                                    <div class="form-body">

                                       <div class="row">
                                            <div class="col-md-3">
                                              <div class="form-group">
                                                <select id="" name="" class="form-control ">
                                                  <option value="">เครื่องหมาย</option>
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>
                                              </div>
                                            </div> 
                                            <div class="col-md-3">
                                              <div class="form-group">  
                                                <select id="" name="" class="form-control ">
                                                  <option value="">ปี</option>
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>
                                              </div>
                                            </div>
                                            <div class="col-md-3">
                                              <div class="form-group">  
                                                <select id="" name="" class="form-control ">
                                                  <option value="">จังหวัด</option>
                                                  <option value=""></option>
                                                  <option value=""></option>
                                                </select>
                                              </div>
                                            </div>
                                            <div class="col-md-3">
                                              <div class="form-group">  
                                                <input type="text" id="" class="form-control " placeholder="่" name="">  
                                              </div>
                                            </div> 
                                          </div>

                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="startdate"> ตั้งเเต่วันที่ :   </label> 
                                          <div class="input-group datep">
                                            <input type="text" class="form-control form2 pickadate-translations" placeholder="" id="startdate" name="startdate" data-value="<?php echo GetToday('');?>" />
                                            <div class="input-group-append">
                                              <span class="input-group-text">
                                                <span class="la la-calendar-o"></span>
                                              </span>
                                            </div>
                                          </div>
                                        </div>
                                      </div>

                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="enddate"> ถึงวันที่ :   </label> 
                                          <div class="input-group datep">
                                            <input type="text" class="form-control form2 pickadate-translations" placeholder="" id="enddate" name="enddate" data-value="2019-05-27" />
                                            <div class="input-group-append">
                                              <span class="input-group-text">
                                                <span class="la la-calendar-o"></span>
                                              </span>
                                            </div>
                                          </div>
                                        </div>
                                      </div> 
                                    </div>  
                                    </div>  
                                  </form>

                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>  <!-- /.row  -->


                    <div class="row"> 
                      <div class="col-md-12 col-lg-10 m-auto">
                        <div class="box_h2 card">
                          <div class="card-header">
                            <h4 class="card-title"><span class="fontcolor1"><u> ส่วนที่ 2</u> </span>&nbsp;&nbsp; อัตราเงินเดือน </h4> 
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                              </ul>
                            </div>
                          </div>
                          <div class="card-content collapse show">
                            <div class="card-body">
                              <div class="row">
                                <div class="col-md-10 m-auto">
                                  <div class="row">
                                    <div class="col-md-3 col-10">
                                      <div class="form-group">
                                        <label for="startdate"> อัตราเงินเดือน : 
                                        </label>
                                        <select id="" name="" class="form-control ">
                                          <option value="">อัตราเงินเดือน</option>
                                          <option value=""></option>
                                          <option value=""></option>
                                        </select>
                                      </div>
                                    </div> 
                                  </div> 
                                  <div class="row">
                                    <div class="col-md-3 col-10">
                                      <div class="form-group">
                                        <label for="startdate"> ปัจจัยเพิ่มเติม : 
                                        </label>  
                                        <br>
                                        <button class="btn round btn1 btncustom1">  <i class="la la-plus"></i> <span > เพิ่ม </span> </button> 
                                      </div>
                                    </div> 
                                  </div> 
                      
                                  <div class="row">
                                    <div class=" col-md-12 ">   
                                      <table id="planManpowerTable2" class="table  tb1   "  >
                                        <thead>
                                          <tr> 
                                            <th>   </th> 
                                            <th> ลำดับที่ </th> 
                                            <th> ความสามารถ  </th>
                                            <th> อัตราเพิ่ม </th> 
                                          </tr>
                                        </thead>
                                        <tbody> 
                                        
                                          <tr>
                                            <td class="text-center">   
                                              <a href="" class="fontcolor1" data-toggle="modal" data-target="#modalSettingAssign"> <i class="la la-trash"></i>  </a>
                                            </td>
                                            <td class="text-center"> 1 </td>
                                            <td class="">   
                                              <select id="" name="" class="form-control ">
                                                <option value="">ประกาศนียบัตรนักบิน</option>
                                                <option value=""></option>
                                                <option value=""></option>
                                              </select>
                                            </td>
                                            <td class="text-center">   
                                              <div class="form-group">  
                                                <input type="text" id="" class="form-control " placeholder="" name="" value="1.0">  
                                              </div>
                                            </td>  
                                          </tr>  
                                          <tr>
                                            <td class="text-center">   
                                              <a href="" class="fontcolor1" data-toggle="modal" data-target="#modalSettingAssign"> <i class="la la-trash"></i>  </a>
                                            </td>
                                            <td class="text-center"> 2 </td>
                                            <td class="">   
                                              <select id="" name="" class="form-control ">
                                                <option value="">ความสามารถเพิ่มเติม</option>
                                                <option value=""></option>
                                                <option value=""></option>
                                              </select>
                                            </td>
                                            <td class="text-center">   
                                              <div class="form-group">  
                                                <input type="text" id="" class="form-control " placeholder="" name="" value="0.5">  
                                              </div>
                                            </td>  
                                          </tr>

                                        </tbody>
                                      </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>  <!-- /.row  -->
                    <div class="row"> 
                      <div class="col-sm-10 m-auto ">
                      <span class="fontcustom1">  *หมายเหตุ ปุ่ม "คำนวณเลขข้าราชการ" เพื่อคำนวณเลขจึงจะสามารถทำการบรรจุอย่างได้   </span>
                      </div>
                    </div>

 
                    <div class="row"> 
                      <div class="col-sm-10 m-auto">
                        <div  class="text-center">
                          <button type="button" class="btn btn2 btn-success   round" data-toggle="modal" data-target="#modalConfirm" > <i class="fa fa-save"></i> บันทึก </button>
                          <button type="button" class="btn btn2 btn-danger   round"  data-dismiss="modal"> <i class="fa fa-times-circle-o"></i> ยกเลิก </button> 
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--/ Bootstrap 3 table -->
      </div>
    </div>
  </div>
  </section>


  <div class="modal fade text-left modal_custom1" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="modalConfirm"  aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <form>
          <div class="my-2 mx-2 px-2 py-2">
            <div class="text-center"> 
              <img src="../../Asset/Images/send.png">
              <div class="py-3">
                <h1> ยืนการส่งข้อมูลหรือไม่ ?</h1>
                <h6>กรุณาเลือกปุ่มใช่หรือไม่ใช่่</h6>
              </div>
            </div> 
            <div class="col-12 d-flex justify-content-center">
              <div class="row"> 
                  <a href="#" class="btn btn-min-width mb-1 mr-1 round btn-success"   data-dismiss="modal" role="button" >
                  <span></span>ใช่</a> 
                  <a href="#" class="btn btn-min-width mb-1 round btn-danger"   data-dismiss="modal" role="button" >
                  <span></span> ไม่ใช่ </a> 
              </div>
            </div>
          </div>
        </form>
      </div> 
    </div> 
  </div>


 
  <!-- footer -->
  <?php include '../include/footer.php'; ?> 

  <script src="../../Controllers/planManpowerController.js"></script>
  <script type="text/javascript">
    $(document).ready(function() { 

      $("#planManTypeId").change(function(){
        // alert( $(this).val() );
         $('.tb1').addClass('hidden');

         $('#planManTypeId2').addClass('hidden'); 
         if( $(this).val() == 1 || $(this).val() == 0){
            $('#planManpowerTable1').removeClass('hidden');
         }else if( $(this).val() ==2 ){ 
            $('#planManTypeId2').removeClass('hidden'); 
            $('#planManpowerTable2').removeClass('hidden'); 
         }else if( $(this).val() == 3 ) {
            $('#planManpowerTable4').removeClass('hidden');
         }

      });

      $("#planManTypeId2").change(function(){
        $('.tb1').addClass('hidden');
         if( $(this).val() ==22 ){
            $('#planManpowerTable3').removeClass('hidden'); 
          }else{
            $('#planManpowerTable2').removeClass('hidden'); 
          }
      });


    }); 
  </script>
  