  <!-- header -->
  <?php include '../include/header.php'; ?>
  <?php include '../../Model/Ducklab/func.php'; ?>
  <?php  
    //Recruitment and appointment
    $menu1 = "PERSONASSIGN" ;
    $menu2 = "searchName2" ;

  ?>
  <!-- menu -->
  <?php include '../include/menu.php'; ?>
  
  <section>
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-2">
            <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
            <h3 class="content-header-title">ค้นหารายชื่อที่บรรจุ </h3>
          </div>
        </div>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb"> 
            <li class="breadcrumb-item"><a href="#">ระบบงานสรรหาและบรรจุกำลังพล</a></li>  
            <li class="breadcrumb-item active" aria-current="page">ค้นหารายชื่อที่บรรจุ </li>
          </ol>
        </nav>
        <div class="content-body">
          <!-- Bootstrap 3 table -->
          <section id="bootstrap3">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-content collapse show">
                    <div class="card-body card-dashboard">
                      
                      <div class="row"> 
                        <div class="col-sm-12">
                          <div class="box_h1 card">
                            <div class="card-header card-head-inverse  ">
                              <h4 class="card-title text-white"> ค้นหา </h4> 
                              <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                              <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                  <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                                </ul>
                              </div>
                            </div>
                            <div class="card-content collapse show">
                              <div class="card-body">
                                <div class="row">
                                  <div class=" col-md-6  m-auto ">
                                  
                                  <form class="form">
                                      <div class="form-body"> 

                                          <div class="row">
                                            <div class="col-md-6">
                                            <div class="form-group">  
                                              <label for="exampleInputEmail1">แผนการบรรจุ:</label>
                                                  <select id="projectinput5" name="interested" class="form-control ">
                                                    <option value=""></option>
                                                    <option value=""></option>
                                                    <option value=""></option>
                                                  </select>
                                            </div>

                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group"> 
                                                    <label for="exampleInputEmail1">ปีงบประมาณ:</label>
                                                    <input class="form-control" type="text" placeholder="Default input">
                                                </div>
                                            </div>


                                            <div class="col-md-6">
                                              <div class="form-group"> 
                                                <label for="exampleInputEmail1">เลขที่ร่าง:</label>

                                                        <div class="form-check py-0"> 
                                                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                                                        <label class="form-check-label" for="exampleRadios1">
                                                            บันทึกเลขร่าง
                                                        </label>
                                                        
                                                        </div>
                                                        <div class="form-check py-0">
                                                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                                                        <label class="form-check-label" for="exampleRadios2">
                                                            ยังไม่ได้บันทึกเลขร่าง
                                                        </label>
                                                        </div>
                                                        <div class="form-check py-0">
                                                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3" value="option3">
                                                        <label class="form-check-label" for="exampleRadios3">
                                                            ทั้งหมด
                                                        </label>
                                                        </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="form-group"> 
                                                    <label for="exampleInputEmail1"></label>
                                                    <select id="projectinput5" name="interested" class="form-control ">
                                                                    <option value=""></option>
                                                                    <option value=""></option>
                                                         </select>
                                                </div>
                                            </div>

                                        </div>

                                        </div>  <!-- ./boxsearchplan2 -->
                                        
                                        <div class="row">
                                                <div class=" col-lg-12 text-center">
                                                    <button type="button" class="btn round btn1 btncustom1 mr-1">
                                                    <i class="la la-search"></i> ค้นหา
                                                    </button>
                                                
                                                    <button type="button" class="btn round btn1 btncustom1">
                                                    <i class="la la-repeat"></i> ล้างค่า
                                                    </button>
                                                </div>
                                          </div>
                                    
                                      
                                    

                                    

                                    </form>

                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div> 
                      
                     

                      <div class="row">
                        <div class="col-6 ">
                          <button class="btn round btn1 btncustom1">  <i class="la la-print"></i> <span >พิมพ์คำสั่งบรรจุ</span> </button>
                       
                        </div>
                       
                      </div>
                      
                      <div class="box_tb1">
                      <div class="table-responsive">
                        <table id="planManpowerTable3" class="table table-bordered tb1 table-striped table-hover"  >
                          <thead>
                            <tr>
                              <th>   </th>
                              <th>   </th>
                              <th> ลำดับที่ </th>
                              <th> ลำดับที่บรรจุ</th> 
                              <th> เลขที่ร่าง</th> 
                              <th> เลขที่คำสั่งอนุมัติ</th> 
                              <th> หมายเลขประจำตัว</th> 
                              <th> ยศ</th>   
                              <th> ชื่อสกุล</th>  
                              <th> ตำแหน่ง</th> 
                            </tr>
                          </thead>
                          <tbody> 
                            <tr > 
                              <td class="text-center">  
                              <a><input type="checkbox" class="checkAll" /> </a>
                              </td>
                              <td class="text-center"> 
                              <a href="profile.php" class="fontcolor1" > <i class="la la-pencil-square-o"></i>  </a>
                              <td class="text-center"> 1 </td> 
                              <td class="text-center">  </td>
                              <td class="text-center"> 020_02098/2562 </td>
                              <td class="text-center"> </td>
                              <td class="text-center"> 256190008400</td>
                              <td class="text-center">  ร.ต.</td>
                              <td class="text-center">  นายกิตติศักดิ์ สมประกอบ </td>   
                              
                              <td class="text-center">  รองหัวหน้าหมวดควบคุม </td> 
                                 
                            </tr> 
                            
                           

                          </tbody>
                        </table>
                    </div>
                        <div class="text-center">
                        <button  data-toggle="modal" data-target="#modalConfirm" type="button" class="btn round btn1 btncustom1" style="background-color:green; color:white;"><i class="la la-save"></i> บันทึกเลขร่างคำสั่ง</button>
                        <button type="button" class="btn round btn1 btncustom1"><i class="la la-send"></i> <span class="px-1">ส่งข้อมูลให้แผนกย้าย </span></button>
                                
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <!--/ Bootstrap 3 table -->
        </div>
      </div>
    </div>
  </section>

  
  <!-- Modal -->
  <!-- Modal  id="#modalSettingCalcu"" -->
  <div class="modal fade text-left modal_custom1" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="modalSettingCalcu"  aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header bg_custom1">
            <h5 class="modal-title"> บันทึกเลขร่างคำสั่ง</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <form class="form_custom1">
            <div class="modal-body">
              <div class="row">
                <div class="col-6 m-auto"> 

                    <div class="form-group">  
                        <label for="positionno"> คำสั่ง : </label>
                        <select id="projectinput5" name="interested" class="form-control ">
                                                                    <option value=""></option>
                                                                    <option value=""></option>
                         </select>                     
                   </div>

                      <div class="form-group">  
                        <label for="positionno"> เลขคำสั่ง : </label>
                        <input type="text" class="form-control" id="" name="" value="" >
                      </div>

                    
                        <div class="form-group">
                                          <label for="startdate"> ลง:   </label> 
                                          <div class="input-group datep">
                                            <input type="text"  style="width:86%" class="form-control form2 pickadate-translations" placeholder="" id="startdate" name="startdate" data-value="<?php echo GetToday('');?>" />
                                            <div class="input-group-append">
                                              <span class="input-group-text">
                                                <span class="la la-calendar-o"></span>
                                              </span>
                                            </div>
                                          </div>
                       </div>
                  

                       <div class="form-group">
                                          <label for="startdate"> วันที่:   </label> 
                                          <div class="input-group datep">
                                            <input type="text"  style="width:86%" class="form-control form2 pickadate-translations" placeholder="" id="startdate" name="startdate" data-value="<?php echo GetToday('');?>" />
                                            <div class="input-group-append">
                                              <span class="input-group-text">
                                                <span class="la la-calendar-o"></span>
                                              </span>
                                            </div>
                                          </div>
                       </div>
                
                  
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <div  class="m-auto ">
                <button type="button" class="btn btn2 btn-success   round"   data-dismiss="modal"> <i class="fa fa-save"></i> บันทึก </button>
                <button type="button" class="btn btn2 btn-danger   round"  data-dismiss="modal"> <i class="fa fa-times-circle-o"></i> ยกเลิก </button> 
              </div>
            </div>
          </form>
        </div>
      </div>
  </div>
 

    
 
  <!-- footer -->
  
  
  <?php include '../include/footer.php'; ?> 
  <script src="../../Controllers/planManpowerAssignController.js"></script>
  <script type="text/javascript">
    $(document).ready(function() { 
 

    }); 
  </script>
  