
  <!-- header -->
  <?php session_start(); ?>
<?php include '../../Model/Ducklab/duck.class.php'; ?>
<?php include '../../Model/Ducklab/contents.class.php'; ?>
<?php include '../include/header.php'; ?>
<?php include '../../Model/Ducklab/func.php'; ?>
  <!-- menu -->
<?php include '../include/menu.php'; ?>
<?php
    $cls_ana = new ContentsClass();
    $data_role = $cls_ana->LoadOnce('role',array('id'=>$_SESSION["data_account"]['role'])) ;
    $data_job = $cls_ana->Load('analysis_system_job') ;
?>
  <style>
/* ol > li > a {color:#222233;} */
.toggle.ios,
.toggle-on.ios,
.toggle-off.ios {
    border-radius: 20rem;
}

.toggle.ios .toggle-handle {
    border-radius: 20rem;
}


</style>

  <section>

      <div class="app-content content">
          <div class="content-wrapper">
                                            <div class="content-header row">
                                                <div class="content-header-left col-12 mb-2">
                                                    <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
                                                    <h3 class="content-header-title">บันทึกข้อมูลเสนอขอบรรจุกําลังพลของ นขต.ทอ.</h3>
                                                                        <nav aria-label="breadcrumb">
                                                                            <ol class="breadcrumb">
                                                                            <li class="breadcrumb-item"><a
                                                                                        href="../home/index.php">ระบบงานวิเคราะห์ความต้องการกำลังพลประจำปี</a></li>
                                                                                <li class="breadcrumb-item active" aria-current="page">
                                                                                บรรจุกำลังพล</li>
                                                                            </ol>
                                                                        </nav>
                                                </div>
                                            </div>
<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- /////////////////////////////////////////////////บันทึก/แก้ไข เสนอขอบรรจุกําลังพลของ นขต.ทอ./////////////////////////////////////////////////////////////// -->
<?php //echo '<pre>',print_r($_SESSION["data_account"]['id'],1),'</pre>';
    $account_id = $_SESSION["data_account"]['id'];
    if($_POST['action']=='save'){
        $cls_ana = new ContentsClass();

        $now   = new DateTime;
        $data_time_now = $now->format( 'Y-m-d H:i:s' );

        $data_save = array(
            'account_id'=>$account_id,
            'create_at'=>$data_time_now
        );
        $save_data = $cls_ana->AddData('analysis_system',$data_save);
        $analysis_system_id = $save_data['code'];

        $data_save = array(
            'analysis_system_id'=>$analysis_system_id,
            'account_id'=>$account_id,
            'year'=>$_POST['year'],
            'affiliation_id'=>$_POST['affiliation_id'],
            'department_id'=>$_POST['department_id'],
            'troops_id'=>$_POST['troops_id'],
            'step4'=>$_POST['step4'],
            'step5_1'=>$_POST['step5_1'],
            'step5_2'=>$_POST['step5_2'],
            'step5_3'=>$_POST['step5_3']
        );
        $analysis_system_data_save = $cls_ana->AddData('analysis_system_data',$data_save);

        
        $analysis_system_data_id = $analysis_system_data_save['code'];
        for ($i=1; $i <= 4; $i++) { 
            $data_save = array(
            'analysis_system_data_id'=>$analysis_system_data_id,
            'order_step3'=>$i,
            'stock'=>$_POST['step3_'.$i.'_1'],
            'add_stock'=>$_POST['step3_'.$i.'_2'],
            'remove_stock'=>$_POST['step3_'.$i.'_3'],
            'sum_stock'=>$_POST['step3_'.$i.'_4']
            );
            $cls_ana->AddData('analysis_system_data_step3',$data_save);
        }




    }
?>
<form method="POST" id="analysis_system_form" action="">
<div class="shadow p-0 bg-white rounded border mb-2">

    <div class="card collapse-icon accordion-icon-rotate active ">
            <div id="headingCollapse31" class="card-header">
            <a data-toggle="collapse" href="#collapse31" aria-expanded="true" aria-controls="collapse31" class="card-title lead white" style="color:white;" >
            <h6 style="background-color:#0f1733;color:white;line-height:40px;padding-left:10px;">บันทึก/แก้ไข เสนอขอบรรจุกําลังพลของ นขต.ทอ.</h6></a></div>
                <div id="collapse31" role="tabpanel" aria-labelledby="headingCollapse31" class="card-collapse collapse show" aria-expanded="true" >  



                    <div class="row">
                            <div class="col-10 m-auto"> 
                                <div class="form-row px-2 m-auto">


                                            <div class="form-group col-md-6">
                                                <label for="formGroupExampleInput">เลขที่เสนอความต้องการ :</label>
                                                <fieldset disabled>
                                                 <input type="text" id="" class="form-control text-center" placeholder="322330086500"> </fieldset>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label for="formGroupExampleInput">วันที่บันทึกข้อมูล :</label>
                                                <fieldset disabled>
                                                 <input type="text" id="" class="form-control text-center" placeholder="16 ม.ค. 2562"> </fieldset>
                                            </div>

                            </div>
                        </div>
                    </div>



        </div> <!-- id="headingCollapse31" class="card-header" -->
    </div> <!-- close card collapse-icon accordion-icon-rotate active -->
</div><!-- close shadow -->

<!-- ////////////////////////////////////////////ส่วนที่ 1 เสนอขอบรรจุกําลังพลของ นขต.ทอ.//////////////////////////////////////////////////////////////////// -->

                <div class="shadow p-0 bg-white rounded">
                    <div class="card collapse-icon accordion-icon-rotate active">

                        <div id="headingCollapse32" class="card-header">
                        <a data-toggle="collapse" href="#collapse32" aria-expanded="true" aria-controls="collapse32" class="card-title lead white">
                        <h6 style="background-color:#D8E0FA;color:#000;line-height:40px;padding-left:10px;"> ส่วนที่ 1 เสนอขอบรรจุกําลังพลของ นขต.ทอ.</h6></a></div>
                        <div id="collapse32" role="tabpanel" aria-labelledby="headingCollapse32" class="card-collapse collapse show" aria-expanded="true" >  

                        <div class="row">
                            <div class="col-10 m-auto"> 


                                    <div class="row">

                                        <div class="form-group col-md-3">
                                                        <label for="formGroupExampleInput">ปีงบประมาณ :</label>
                                                        <input type="text" class="form-control border-danger" id="formGroupExampleInput" placeholder="2562" name='year'>
                                        </div>

                                        <div class="form-group col-md-3">
                                                <label for="formGroupExampleInput">สังกัดที่ขอบรรจุ:</label>
                                                <select class="form-control border-danger" id="exampleFormControlSelect1" name='affiliation_id'>
                                                    <option value='1'>คปอ.11</option>
                                                    <option value='2'>คปอ.22</option>
                                                </select>
                                        </div>

                                        <div class="form-group col-md-6">
                                                <label for="formGroupExampleInput2">หน่วยงานที่ขอบรรจุ:</label>
                                                <select class="form-control border-danger" id="exampleFormControlSelect1" name='department_id'>
                                                    <option value='1'>กคว.คปอ.</option>
                                                    <option value='2'>กคว.คปอ.</option>
                                                </select>
                                        </div>

                                        <div class="w-100"></div>

                                            <div class="form-group col-md-6">
                                                    <label for="formGroupExampleInput">กําลังพลที่ต้องการ:</label>
                                                    <select class="form-control border-danger" id="exampleFormControlSelect1" name='troops_id'>
                                                        <option value='1'>ประทวน</option>
                                                        <option value='2'>สัญญาบัตร</option>
                                                    </select>
                                            </div>

                                        <div class="form-group col-md-6">
                                                <h6 class="py-1">เพศ:</h6>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="gender" id="exampleRadios1" value="3" checked>
                                                        <label class="form-check-label" for="exampleRadios1">
                                                            ชาย/หญิง
                                                        </label>
                                                    </div>

                                                    <div class="form-check form-check-inline">

                                                        <input class="form-check-input m-1" type="radio" name="gender" id="exampleRadios2" value="1">

                                                        <label class="form-check-label m-1" for="exampleRadios2">
                                                            ชาย
                                                        </label>
                                                    </div>

                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="gender" id="exampleRadios2" value="2">
                                                        <label class="form-check-label" for="exampleRadios3">
                                                            หญิง
                                                        </label>
                                                    </div>

                                        </div>

                                        <div class="w-100"></div>

                                        <div class="form-group col-12">
                                                        <label for="formGroupExampleInput">ตําแหน่งที่ขอบรรจุ:</label>
                                                        <input type="text" class="form-control border-danger" id="formGroupExampleInput" placeholder="นายทหารค้นหาและช่วยชีวิต กคว.คปอ.">
                                        </div>

                                </div>

                                <div class="row">
                                    <div class="col-lg-8">
                                    <div class="row">
                                                            <div class="form-group col-md-3">
                                                                <label for="formGroupExampleInput">อัตราอนุมัติ:</label>
                                                                <input type="text" class="form-control" id="formGroupExampleInput" placeholder="4">
                                                            </div>
                                                            <div class="form-group col-md-3">
                                                                <label for="formGroupExampleInput">กรอบการบรรจุ:</label>
                                                                <input type="text" class="form-control" id="formGroupExampleInput" placeholder="4">
                                                            </div>
                                                            <div class="form-group col-md-3">
                                                                <label for="formGroupExampleInput">จํานวนที่บรรจุ:</label>
                                                                <input type="text" class="form-control" id="formGroupExampleInput" placeholder="3">
                                                            </div>

                                                            <div class="form-group col-md-3">
                                                                <label for="formGroupExampleInput">อัตรา:</label>
                                                                <select class="form-control" id="exampleFormControlSelect1">
                                                                    <option>ร.ต.</option>
                                                                    <option>ร.ต.</option>
                                                                </select>
                                                             </div>
                                </div>
                                    </div>


                                    <div class="col-lg-4">

                                                    <table class="table table-striped table-border table-hover">
                                                    <thead style="background-color:#0f1733;color:white">
                                                        <tr>
                                                        <th scope="col">ลชทอ. หน้าที่</th>
                                                        <th scope="col">เหล่า</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                        <td>-</td>
                                                        <td>นบ.</td>
                                                        </tr>
                                                        <tr>
                                                        <td>-</td>
                                                        <td>นบ.</td>
                                                        </tr>
                                                    </tbody>
                                                    </table>
                                    </div>

                            </div>

                            <div class="row">
                                    <div class="col-lg-12">
                                    <table class="table table-striped table-border table-hover">
                                    <thead class="text-center">
                                <tr class="justify-content-center"style="background-color:#0f1733; color:whitesmoke;">
                                                        <th scope="col">เลือก</th>
                                                        <th scope="col">ลำดับ</th>
                                                        <th scope="col">ยศ</th>
                                                        <th scope="col">ชื่อ-นามสกุล</th>
                                                        <th scope="col">สาเหตุในการขอบรรจุกำลังพล</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="text-center">
                                                        <?php foreach ($data_job as $key => $value) {?>
                                                        <tr class="justify-content-center">
                                                            <td><input type="checkbox" class="checkAll" /></td>
                                                            <td><?php echo $key+1;?></td>
                                                            <td><?php echo $value['rank'];?></td>
                                                            <td><?php echo $value['first_name']." ".$value['last_name'];?></td> 
                                                            <td>
                                                            <select class="form-control" id="" name='job_step1_'>
                                                                    <option value='1'>สูญเสียกำลังพล</option>
                                                                    <option value='2'>เตรียมกำลังพลไว้ทดแทนผู้เกษียญอายุราชการ</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <?php }?>
                                                    </tbody>
                                                    </table>

                                    </div>
                            </div>
                            <hr>

                                                <div class="form-group col-md-3">
                                                                <label for="formGroupExampleInput">จำนวนที่ต้องบรรจุ:</label>
                                                                <fieldset disabled>
                                                 <input type="text" id="disabledTextInput" class="form-control text-center" placeholder="4"> </fieldset>
                                                </div>


                        </div>


                    </div> <!-- close card collapse-icon accordion-icon-rotate active -->
                    </div> <!-- close shadow -->

        </div>
    </div>
</section>

<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

<!-- ///////////////////////////////////////////ส่วนที่ 2 เสนอขอบรรจุกําลังพลของ นขต.ทอ.////////////////////////////////////////////////////////////////////// -->
<section>
    <div class="app-content content">
        <div class="content-wrapper">

                <div class="shadow p-0 bg-white rounded mt-1">

                    <div class="card collapse-icon accordion-icon-rotate active ">
                            <div id="headingCollapse33" class="card-header">
                            <a data-toggle="collapse" href="#collapse33" aria-expanded="true" aria-controls="collapse33" class="card-title lead white" style="color:white;" >
                            <h6 style="background-color:#D8E0FA;color:#000;line-height:40px;padding-left:10px;"> ส่วนที่ 2 เสนอขอบรรจุกําลังพลของ นขต.ทอ.</h6></a></div>
                                <div id="collapse33" role="tabpanel" aria-labelledby="headingCollapse33" class="card-collapse collapse show" aria-expanded="true" >

                                <div class="m-1">

                                <div> ลำดับ ยศ ชื่อ-สกุล </div>
                                <!-- ///////////////////////////  -->

                                <div class="card collapse-icon accordion-icon-rotate active ">
                                 <div id="headingCollapse40" class="card-header" style="color:#fff;">
                                 <a data-toggle="collapse" href="#collapse40" aria-expanded="true" aria-controls="collapse40" class="card-title lead white" style="color:#fff;" >
                                <h6 style="background-color:#0f1733;color:#fff;line-height:40px;padding-left:10px;"> 1.  ร.อ.คมสัน  ทองสีมา</h6></a></div>
                                <div id="collapse40" role="tabpanel" aria-labelledby="headingCollapse40" class="card-collapse collapse show"  aria-expanded="true" >

                                    <div class="row">

                                        <div class="form-group col-md-6 px-5">
                                                   <h6 class="py-1" > เลือกประเภทกําลังพล:</h6>

                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="exampleRadios0" id="exampleRadios1" value="option1" checked>
                                                        <label class="form-check-label" for="exampleRadios1">
                                                        กําลังพลตามข้อผูกพัน
                                                        </label>
                                                    </div>

                                                    <div class="form-check form-check-inline">

                                                        <input class="form-check-input m-1" type="radio" name="exampleRadios0" id="exampleRadios2" value="option2">

                                                        <label class="form-check-label m-1" for="exampleRadios2">
                                                        กําลังพลพิเศษ
                                                        </label>
                                                    </div>

                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="exampleRadios0" id="exampleRadios2" value="option3">
                                                        <label class="form-check-label" for="exampleRadios3">
                                                        เลื่อนฐานะ
                                                        </label>
                                                    </div>

                                            </div>

                                            <div class="form-group col-md-6">
                                                    <h6 class="py-1">คุณวุฒิ:</h6>


                                                    <select class="form-control" id="exampleFormControlSelect1" >
                                                            <option>นนอ.</option>
                                                            <option>นนอ.</option>
                                                        </select>


                                            </div>

                                        <div class="float-left mx-2">
                                        <a href="#" class="btn btn-social btn-min-width mb-1 mr-1 round bg-hr">
                                         <span class="fa fa-plus-circle"> </span> เพิ่ม</a> </div>

                                        <div class="table-responsive mx-2">
                                        <table class="table table-striped table-border table-hover">
                                            <thead class="text-center">
                                                <tr class="justify-content-center"style="background-color:#0f1733; color:whitesmoke;">
                                                      <th></th>
                                                      <th>ลำดับที่</th>
                                                      <th>สายวิชา</th>
                                                      <th>ประเภทสายวิทยาการ</th>
                                                  </tr>
                                              </thead>

                                              <tbody class="text-center">
                                                <tr>
                                                        <td>
                                                            <a href="./delete.php"><i class="la la-trash-o"
                                                                    style="color:#0f1733;"></i></a>
                                                        </td>
                                                        <td>1</td>
                                                        <td>
                                                            <select class="form-control" id="exampleFormControlSelect1" >
                                                                <option>วิศวกรรมอากาศยานและการบิน</option>
                                                                <option>วิศวกรรมอากาศยานและการบิน</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                                <select class="form-control" id="exampleFormControlSelect1" >
                                                                    <option>ยก.ทอ.</option>
                                                                    <option>ยก.ทอ.</option>
                                                                </select>
                                                        </td>
                                                  </tr>
                                              </tbody>

                                                </table>
                                                </div>


                                    </div>

                                    <!-- //////////////////////////////////////////////// -->

                                    <div class="mx-2">
                                            <div class="form-group">
                                            <label for="exampleFormControlTextarea1">คุณสมบัติเฉพาะ :</label>
                                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                                            </div>

                                    </div>

                                    <!-- //////////////////////////////////////////////// -->
                                    <div style="background-color:#ccc; line-height:50px;"> 
                                    <div class="row px-1">
                                            <div class="col-4">
                                                สายวิทยาการ :
                                                    <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="exampleRadios0" id="exampleRadios2" value="option3">
                                                    <label class="form-check-label" for="exampleRadios3">อนุมัติ </label></div>

                                                    <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="exampleRadios0" id="exampleRadios2" value="option3">
                                                    <label class="form-check-label" for="exampleRadios3">ไม่อนุมัติ </label></div>

                                            </div>


                                         <div class="col-4">
                                                งานวิเคราะห์:
                                                    <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="exampleRadios0" id="exampleRadios2" value="option3">
                                                    <label class="form-check-label" for="exampleRadios3">อนุมัติ </label></div>

                                                    <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="exampleRadios0" id="exampleRadios2" value="option3">
                                                    <label class="form-check-label" for="exampleRadios3">ไม่อนุมัติ </label>
                                                    </div>
                                                    
                                        </div>

                                        

                                         <div class="col-4">
                                                งานแผน :
                                                    <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="exampleRadios0" id="exampleRadios2" value="option3">
                                                    <label class="form-check-label" for="exampleRadios3">อนุมัติ </label></div>

                                                    <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="exampleRadios0" id="exampleRadios2" value="option3">
                                                    <label class="form-check-label" for="exampleRadios3">ไม่อนุมัติ </label>
                                                    </div>
                                                    
                                        </div>

                                    </div>
                                    </div>
                                  



                            
                            </div>
                        </div> <!-- id="headingCollapse31" class="card-header" -->
                    </div> <!-- close card collapse-icon accordion-icon-rotate active -->


                        </div> <!-- id="headingCollapse31" class="card-header" -->
                    </div> <!-- close card collapse-icon accordion-icon-rotate active -->
                </div><!-- close shadow -->     

<!-- ///////////////////////////////////////////ส่วนที่ 3 เสนอขอบรรจุกําลังพลของ นขต.ทอ.////////////////////////////////////////////////////////////////////// -->


                <div class="shadow p-0 bg-white rounded mt-1">

                    <div class="card collapse-icon accordion-icon-rotate active ">
                            <div id="headingCollapse34" class="card-header">
                            <a data-toggle="collapse" href="#collapse34" aria-expanded="true" aria-controls="collapse34" class="card-title lead white" style="color:white;" >
                            <h6 style="background-color:#D8E0FA;color:#000;line-height:40px;padding-left:10px;"> ส่วนที่ 3 เสนอขอบรรจุกําลังพลของ นขต.ทอ.</h6></a></div>
                                <div id="collapse34" role="tabpanel" aria-labelledby="headingCollapse34" class="card-collapse collapse show" aria-expanded="true" >  


                                                <div class="row">
                                                        <div class="col-10 m-auto"> 

                                                        <div class="row">
                                    <div class="col-lg-12">
                                    <table class="table table-striped table-border table-hover">
                                                    <thead class="text-center">
                                                        <tr style="background-color:#0f1733; color:whitesmoke;">
                                                        <th scope="col">ประเภท</th>
                                                        <th scope="col">บรรจุในหน่วย</th>
                                                        <th scope="col">มาช่วยราชการ</th>
                                                        <th scope="col">ไปช่วยราชการ</th>
                                                        <th scope="col">คงเหลือปฏิบัติงานจริง</th>
                                                        </tr>
                                                    </thead>
                                            <tbody class="text-center">
                                                        <tr>
                                                            <td>สัญญาบัตร</td>
                                                            <td><input type="number" class="form-control border-danger text-center step3-1 step3-1-1" id=""  placeholder="" name="step3_1_1" value=""></td>
                                                            <td><input type="number" class="form-control border-danger text-center step3-1 step3-1-2" id=""  placeholder="" name="step3_1_2" value=""></td>
                                                            <td><input type="number" class="form-control border-danger text-center step3-1 step3-1-3" id=""  placeholder="" name="step3_1_3" value=""></td>
                                                            <td><fieldset disabled>
                                                                <input type="number" id="" class="form-control text-center step3-1-4" placeholder="" > </fieldset>
                                                                <input type="hidden" class="step3-1-4" name="step3_1_4">
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>ประทวน</td>
                                                            <td><input type="number" class="form-control border-danger text-center step3-2 step3-2-1" id="" name="step3_2_1" placeholder=""></td>
                                                            <td><input type="number" class="form-control border-danger text-center step3-2 step3-2-2" id="" name="step3_2_2" placeholder=""></td>
                                                            <td><input type="number" class="form-control border-danger text-center step3-2 step3-2-3" id="" name="step3_2_3" placeholder=""></td>
                                                            <td><fieldset disabled>
                                                                <input type="number" id="" class="form-control text-center step3-2-4" placeholder=""> </fieldset>
                                                                <input type="hidden" class="step3-2-4" name="step3_2_4">
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>พนักงานราชการ</td>
                                                            <td><input type="number" class="form-control border-danger text-center step3-3 step3-3-1" id="" name="step3_3_1" placeholder=""></td>
                                                            <td><input type="number" class="form-control border-danger text-center step3-3 step3-3-2" id="" name="step3_3_2" placeholder=""></td>
                                                            <td><input type="number" class="form-control border-danger text-center step3-3 step3-3-3" id="" name="step3_3_3" placeholder=""></td>
                                                            <td><fieldset disabled>
                                                                <input type="number" id="" class="form-control text-center step3-3-4" placeholder="" > </fieldset>
                                                                <input type="hidden" class="step3-3-4" name="step3_3_4">
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>ลูกจ้างประจำ</td>
                                                            <td><input type="number" class="form-control border-danger text-center step3-4 step3-4-1" id="" name="step3_4_1" placeholder=""></td>
                                                            <td><input type="number" class="form-control border-danger text-center step3-4 step3-4-2" id="" name="step3_4_2" placeholder=""></td>
                                                            <td><input type="number" class="form-control border-danger text-center step3-4 step3-4-3" id="" name="step3_4_3" placeholder=""></td>
                                                            <td><fieldset disabled>
                                                                <input type="number" id="" class="form-control text-center step3-4-4" placeholder="" > </fieldset>
                                                                <input type="hidden" class="step3-4-4" name="step3_4_4">
                                                            </td>

                                                        </tr>

                                                    </tbody>
                                        </table>

                                    </div>
                            </div>

                                                        </div>

                                                </div>


                        </div> <!-- id="headingCollapse31" class="card-header" -->
                    </div> <!-- close card collapse-icon accordion-icon-rotate active -->
                </div><!-- close shadow -->   

<!-- ///////////////////////////////////////////ส่วนที่ 4 สาเหตุในการขอบรรจุกําลังพลเพิ่มเติม////////////////////////////////////////////////////////////////////// -->


<div class="shadow p-0 bg-white rounded mt-1">

<div class="card collapse-icon accordion-icon-rotate active ">
        <div id="headingCollapse35" class="card-header">
        <a data-toggle="collapse" href="#collapse35" aria-expanded="true" aria-controls="collapse35" class="card-title lead white" style="color:white;" >
        <h6 style="background-color:#D8E0FA;color:#000;line-height:40px;padding-left:10px;">ส่วนที่ 4 สาเหตุในการขอบรรจุกําลังพลเพิ่มเติม</h6></a></div>
            <div id="collapse35" role="tabpanel" aria-labelledby="headingCollapse35" class="card-collapse collapse show" aria-expanded="true" >


                            <div class="row">
                                    <div class="col-10 m-auto"> 
                                        <div class="form-row px-2 m-auto">

                                        <div class="form-group col-md-8">
                                            <label for="exampleFormControlTextarea1">สาเหตุในการขอบรรจุกําลังพลเพิ่มเติม :</label>
                                            <textarea class="form-control border-danger" id="" name="step4" rows="3"></textarea>
                                        </div>


                                    </div>
                                </div>
                            </div>


    </div> <!-- id="headingCollapse31" class="card-header" -->
</div> <!-- close card collapse-icon accordion-icon-rotate active -->
</div><!-- close shadow -->   

<!-- ///////////////////////////////////////////ส่วนที่ 5 ภารกิจและหน้าที่////////////////////////////////////////////////////////////////////// -->


<div class="shadow p-0 bg-white rounded mt-1">

<div class="card collapse-icon accordion-icon-rotate active ">
        <div id="headingCollapse36" class="card-header">
        <a data-toggle="collapse" href="#collapse36" aria-expanded="true" aria-controls="collapse36" class="card-title lead white" style="color:white;" >
        <h6 style="background-color:#D8E0FA;color:#000;line-height:40px;padding-left:10px;">ส่วนที่ 5 ภารกิจและหน้าที่</h6></a></div>
            <div id="collapse36" role="tabpanel" aria-labelledby="headingCollapse36" class="card-collapse collapse show" aria-expanded="true" >  


                            <div class="row">
                                    <div class="col-10 m-auto"> 
                                        <div class="form-row px-2 m-auto">

                                        <div class="form-group col-md-8">
                                            <label for="">ภารกิจของหน่วยงาน :</label>
                                            <textarea class="form-control border-danger" id="" name="step5_1" rows="3"></textarea>
                                        </div>

                                        <div class="form-group col-md-8">
                                            <label for="">หน้าที่ของตําแหน่ง :</label>
                                            <textarea class="form-control border-danger" id="" name="step5_2" rows="3"></textarea>
                                        </div>

                                        <div class="form-group col-md-8">
                                            <label for="">ปริมาณงาน 3 ปี ย้อนหลัง (ตามปีงบประมาณ) :</label>
                                            <textarea class="form-control border-danger" id="" name="step5_3" rows="3"></textarea>
                                        </div>

                                            
                                    </div>
                                </div>
                            </div>


    </div> <!-- id="headingCollapse31" class="card-header" -->
</div> <!-- close card collapse-icon accordion-icon-rotate active -->
</div><!-- close shadow -->   




         </div> <!-- close app-content content -->
     </div> <!-- close content-wrapper -->
</section> <!-- close /section -->
    <input type="hidden" name="action" value="save">
</form>
<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->




     <div class="row mt-2"> 
            <div class=" col-12 m-auto">
                            <div class="col-12 d-flex justify-content-center"> 
                                <a href="#" class="btn btn-social btn-min-width mb-1 round" role="button" style="background-color:green; color:white;" onclick="$('#analysis_system_form').submit()">
                                    <span class="la la-save"style="color:white; font-weight: bold;font-size: 18px"></span>บันทึก</a> 
                                <!-- <a href="#" class="btn btn-social btn-min-width mb-1 round" role="button" style="background-color:green; color:white;">
                                    <span class="la la-save"style="color:white; font-weight: bold;font-size: 18px"></span>บันทึกเลขที่ร่างส่งตามเงื่อนไขค้นหา</a>  -->

                            </div>

            </div>
     </div> 


<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
 

<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <script src="http://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
      crossorigin="anonymous"></script>

  <script type="text/javascript">
$(document).ready(function() {
    console.log("ready");
    change_autorefreshdiv();
});

function change_autorefreshdiv() {
    // $('#prefixPage').addClass('active');
}

function toggle(source) {
    var checkboxes = document.querySelectorAll('.checkAll');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i] != source)
            checkboxes[i].checked = source.checked;
    }
}

$( ".step3-1" ).change(function() {
    var data1 = parseInt($( ".step3-1-1" ).val(),10);
    var data2 = parseInt($( ".step3-1-2" ).val(),10);
    var data3 = parseInt($( ".step3-1-3" ).val(),10);
    var data4 = data1+data2-data3;
    if(data4!=null){
        $( ".step3-1-4" ).val(data4);
    }
});
$( ".step3-2" ).change(function() {
    var data1 = parseInt($( ".step3-2-1" ).val(),10);
    var data2 = parseInt($( ".step3-2-2" ).val(),10);
    var data3 = parseInt($( ".step3-2-3" ).val(),10);
    var data4 = data1+data2-data3;
    if(data4){
        $( ".step3-2-4" ).val(data4);
    }
});
$( ".step3-3" ).change(function() {
    var data1 = parseInt($( ".step3-3-1" ).val(),10);
    var data2 = parseInt($( ".step3-3-2" ).val(),10);
    var data3 = parseInt($( ".step3-3-3" ).val(),10);
    var data4 = data1+data2-data3;
    if(data4){
        $( ".step3-3-4" ).val(data4);
    }
});
$( ".step3-4" ).change(function() {
    var data1 = parseInt($( ".step3-4-1" ).val(),10);
    var data2 = parseInt($( ".step3-4-2" ).val(),10);
    var data3 = parseInt($( ".step3-4-3" ).val(),10);
    var data4 = data1+data2-data3;
    if(data4){
        $( ".step3-4-4" ).val(data4);
    }
});


  </script>



  <!-- footer -->
  <?php include '../include/footer.php'; ?>