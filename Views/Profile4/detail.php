<!-- header -->
<?php
    require_once "../../config.php";
    require_once '../../Model/Ducklab/func.php';
    require_once '../include/header.php';
  
    $menu1 ="HRTOFFICIAL";
    $menu2 ="PROFILE"; 

    $PersonID=$_REQUEST['PersonID'];
?>

<!-- menu -->
<?php include '../include/menu.php'; ?>

<?php
    //---------------------------------------------------------------------// 
    $sql_topPanel = "SELECT HR.HrtRankAbbrTh,HP.HrtPersonID,HP.HrtName +'  '+ HP.HrtSurName As FullName,HP.HrtAirForceID
        FROM HrtPerson AS HP left join HrtRank AS HR on HR.HrtRankID = HP.HrtRankID 
        WHERE HP.HrtPersonID = '$PersonID' ";
    $query_topPanel = sqlsrv_query($conn,$sql_topPanel); 
    $data_topPanel = array();

    while($row_topPanel = sqlsrv_fetch_array($query_topPanel, SQLSRV_FETCH_ASSOC )){
        $data_topPanel = $row_topPanel ;
    }
    //---------------------------------------------------------------------//
?>

  <section>

    <div class="app-content content">
      <div class="content-wrapper">

      <div class="content-header row">
            <div class="content-header-left col-12 mb-2">
              <div style="width:7px;height:30px;background-color:#1a1d52; float:left; margin-right:10px;"></div>
              <h3 class="content-header-title">ข้อมูลบุคคล <?php //echo $sql_search; ?></h3>
            </div>
          </div>

          <div class="content-body">            
            <section id="bootstrap3">
              <div class="row">
                <div class="col-12">
                  <div class="card">
                    <div class="card-content collapse show">
                      <div class="card-body card-dashboard">
                        <p class="card-text"></p>
                        <nav aria-label="breadcrumb">
                          <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../home/index.php">งานประวัติข้าราชการ</a></li>
                            <li class="breadcrumb-item active" aria-current="page">ข้อมูลประวัติข้าราชการ</li>
                          </ol>
                        </nav>
                        <div class="content-body">
                          <section class="horizontal-grid" id="horizontal-grid">
                            <div class="row">
                              <div class="col-md-12">
                                <div class="box_h1 card">
                                  <div class="card-header card-head-inverse">
                                    <h4 class="card-title text-white"><?php
                                      echo $data_topPanel['HrtRankAbbrTh']." ".$data_topPanel['FullName']."  หมายเลขประจำตัว : ".$data_topPanel['HrtAirForceID'];
                                    ?></h4> 
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                      <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                                      </ul>
                                    </div>
                                  </div>

                                  <div class="card-content collapse show">
                                    <div class="card-body">
          <!---------- ---------- ---------- ---------- เริ่ม กล่องข้อมูลและรูปด้านบน ---------- ---------- ---------- ---------- -->
                      <div class="profile" style="background-color:#f4f5fa; color:whitesmoke;padding:1%; height: 180px;width: 100%;">
                        <div class="image" style="background-color:#BEBEBE;height: 160px;width: 132px;">
                          <div class="profile_data" style="height: 160px;width: 450px;margin-left:150px;padding:1%;">
                            <p class="text">
                              <div class="row">  
                                <div class="col-md-12">
                                  <h6>หมายเลขประจำตัว : <?php echo $data_topPanel['HrtAirForceID'] ;?></h6>
                                </div>
                              </div> 
                              <h6></h6>  
                              <div class="row">  
                                <div class="col-md-6">
                                  <h6>ยศ : <?php  echo $data_topPanel['HrtRankAbbrTh'] ;?></h6>  
                                </div>    
                                <div class="col-md-6">
                                  <h6>ชื่อ-นามสกุล : <?php echo $data_topPanel['FullName'] ?></h6>  
                                </div>
                              </div> 
                              <h6></h6>   
                              <div class="row">   
                                <div class="col-md-6">
                                  <h6>เหล่า : <?php ?></h6>   
                                </div>   
                                <div class="col-md-6">
                                  <h6>จำพวก : <?php ?></h6>   
                                </div>   
                              </div>      
                              <h6></h6>  
                              <div class="row">   
                                <div class="col-md-12">
                                  <h6>สังกัด : <?php ?></h6>   
                                </div>      
                              </div>     
                              <h6></h6>  
                              <div class="row">   
                                <div class="col-md-12">
                                  <h6>ตำแหน่ง : <?php ?></h6>      
                                </div>      
                              </div>    
                            </p>
                          </div>
                        </div>
                      </div>
          <!---------- ---------- ---------- ---------- จบ กล่องข้อมูลและรูปด้านบน ---------- ---------- ---------- ---------- -->
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </div>
                          </section>
                        </div>


<div class="profile_head"
style="background-color:#f5f5f5;height: auto;width: 100%;margin-left:0px;padding:1%;">
<br>
<div class="card">
<div class="card-header">
<h5 class="card-title">ประวัติข้าราชการ</h5>
</div>
<div class="card-content">
<div class="card-body">
<?php require_once '../include/profile_table.php'; ?>


<div class="tab-content px-1 pt-1">

<!-- ---------------ข้อมูลบุคคล-------------- -->
<!--////*********************************************************************************************************111111111111111111111111111111111111-->
<?php include 'detailtab1.php'; ?>
<!--////*********************************************************************************************************111111111111111111111111111111111111-->

<!-- ---------------รูปภาพ-------------- -->
<!--////*********************************************************************************************************111111111111111111111111111111111111-->
<?php //include 'detailtab2.php'; ?>
<!--/// /*********************************************************************************************************111111111111111111111111111111111111-->

<!-- ---------------การเปลี่ยนชื่อ-------------- -->
<!--////*********************************************************************************************************111111111111111111111111111111111111-->
<?php //include 'detailtab3.php'; ?>
<!--////*********************************************************************************************************111111111111111111111111111111111111-->

<!-- ---------------ขึ้นทะเบียนทหาร-------------- -->
<!--////*********************************************************************************************************111111111111111111111111111111111111-->
<?php// include 'detailtab4.php'; ?>
<!--////*********************************************************************************************************111111111111111111111111111111111111-->

<!-- ---------------เหล่าทหาร-------------- -->
<!--////*********************************************************************************************************111111111111111111111111111111111111-->
<?php //include 'detailtab5.php'; ?>
<!--////*********************************************************************************************************111111111111111111111111111111111111-->

<!-- ---------------บิดา-มารดาบรรพบุรุษ-------------- -->
<!--////*********************************************************************************************************111111111111111111111111111111111111-->
<?php //include 'detailtab6.php'; ?>
<!--////*********************************************************************************************************111111111111111111111111111111111111-->

<!-- ---------------คู่สมรส-------------- -->
<!--////*********************************************************************************************************111111111111111111111111111111111111-->
<?php //include 'detailtab7.php'; ?>
<!--////*********************************************************************************************************111111111111111111111111111111111111-->


<!-- ---------------บุตร-ธิดา-------------- -->
<!--////*********************************************************************************************************111111111111111111111111111111111111-->
<?php  //include 'detailtab8.php'; ?>
<!--////*********************************************************************************************************111111111111111111111111111111111111-->


                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 </section>


<!-- footer -->
<?php include '../include/footer.php'; ?>
<!-- <script src="../../Controllers/profileController.js"></script> -->

<script type="text/javascript">
$(document).ready(function() {
console.log("ready");
change_autorefreshdiv();
});

function change_autorefreshdiv() {
// $('#prefixPage').addClass('active');
}

function toggle(source) {
var checkboxes = document.querySelectorAll('.checkAll');
for (var i = 0; i < checkboxes.length; i++) {
if (checkboxes[i] != source)
checkboxes[i].checked = source.checked;
}
}
</script>


