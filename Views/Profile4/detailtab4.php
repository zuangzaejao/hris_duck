
<div class="tab-pane" id="tab14"
aria-labelledby="base-tab14">
<div class="container">
<div class="card-content">
<div class="card-body">
  <!-----------------------------////////////////////////////////--------------------------------->   
    <div class="card collapse-icon accordion-icon-rotate active">
        <div id="headingCollapse35" class="card-header bg-success">
            <a data-toggle="collapse"
                href="#collapse35"
                aria-expanded="true"
                aria-controls="collapse35"
                class="card-title lead white">
                <h6><U>ส่วนที่ 1</U>
                    การขึ้นทะเบียนทหาร
                </h6>
            </a>
        </div>
        <div id="collapse35"
           role="tabpanel"
           aria-labelledby="headingCollapse35"
            class="card-collapse collapse show"
            aria-expanded="true">
            <div class="card-content">
  <!-----------------------------////////////////////////////////--------------------------------->   
        <div class="row">
            <div class="col-md-6">
                <div class="card-block">
                    <div class="input-group">
                        วันขึ้นทะเบียนทหาร :
                        <input
                            type="text"
                            class="form-control pickadate-disable-dates"
                            placeholder=" "
                            aria-describedby="button-addon4">
                        <div class="input-group-append">
                            <button
                                class="btn btn-primary"
                                type="button"
                                style=" padding-bottom: 1px; padding-top: 1px;">
                                <i class="la la-calendar-o"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-body" id="File_number">
                    เครื่องหมาย :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>
            </div>
        </div>
        <!-----------------------------////////////////////////////////--------------------------------->   
        <div class="row">           
            <div class="col-md-6">
                <div class="card-block">
                    <div class="card-body" id="noncomm_and_comm">
                        ขึ้นทะเบียนทหารเพราะ :
                        <select
                            class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศโท/พล.อ.ท.
                                </option>                                        
                            </optgroup>                          
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-block">
                    <div class="input-group">
                        วันล้วง :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input
                            type="text"
                            class="form-control pickadate-disable-dates"
                            placeholder="12-20-2019"
                            aria-describedby="button-addon4">
                        <div
                            class="input-group-append">
                            <button
                                class="btn btn-primary"
                                type="button"
                                style=" padding-bottom: 1px; padding-top: 1px;">
                                <i class="la la-calendar-o"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-----------------------------////////////////////////////////--------------------------------->   
        <div class="row">                 
            <div class="col-md-6">
                <div class="card-block">
                    <div class="input-group"> วันร้อง :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="text"
                            class="form-control pickadate-disable-dates"
                            placeholder="วันร้อง"
                            aria-describedby="button-addon4">
                        <div class="input-group-append">
                            <button
                                class="btn btn-primary"
                                type="button"
                                style=" padding-bottom: 1px; padding-top: 1px;"><i
                                class="la la-calendar-o"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-block">
                    <div class="input-group">
                        วันปลดประจำการ :
                        <input
                            type="text"
                            class="form-control pickadate-disable-dates"
                            placeholder="วันปลดประจำการ"
                            aria-describedby="button-addon4">
                        <div
                            class="input-group-append">
                            <button
                                class="btn btn-primary"
                                type="button"
                                style=" padding-bottom: 1px; padding-top: 1px;"><i
                                class="la la-calendar-o"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-----------------------------////////////////////////////////--------------------------------->   
        </div>
        </div>
    <br>
  <!-----------------------------////////////////////////////////--------------------------------->   
    <!-- ----ส่วนที่1------      -->
    
  <!-----------------------------////////////////////////////////--------------------------------->   

<div class="card collapse-icon accordion-icon-rotate active">
    <div id="headingCollapse36" class="card-header bg-success">
        <a data-toggle="collapse"
            href="#collapse36"
            aria-expanded="true"
            aria-controls="collapse36"
            class="card-title lead white">
            <h6><U>ส่วนที่ 2</U>
                ปลดเป็นทหารกองหนุนประเภท 2
            </h6>
        </a>
    </div>
        
  <!-----------------------------////////////////////////////////---------------------------------> 
    <div id="collapse36"
        role="tabpanel"
        aria-labelledby="headingCollapse36"
        class="card-collapse collapse show"
        aria-expanded="true">
        <div class="card-content">   
            
  <!-----------------------------////////////////////////////////--------------------------------->         
    <div class="row">  
        <div class="col-md-6">
            <div class="card-block">
                <div class="input-group">
                    กองหนุนชั้นที่ 1 :
                    <input
                        type="text"
                        class="form-control pickadate-disable-dates"
                        placeholder="25 กรกฏาคม 2562"
                        aria-describedby="button-addon4">
                    <div
                        class="input-group-append">
                        <button
                            class="btn btn-primary"
                            type="button"
                            style=" padding-bottom: 1px; padding-top: 1px;"><i
                            class="la la-calendar-o"></i></button>
                    </div>
                </div>
            </div>
        </div>
            <div class="col-md-6">
                <div class="card-block">
                    <div class="input-group">
                        กองหนุนชั้นที่ 2 :
                        <input
                            type="text"
                            class="form-control pickadate-disable-dates"
                            placeholder="25 กรกฏาคม 2562"
                            aria-describedby="button-addon4">
                        <div
                            class="input-group-append">
                            <button
                                class="btn btn-primary"
                                type="button"
                                style=" padding-bottom: 1px; padding-top: 1px;"><i
                                class="la la-calendar-o"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-----------------------------////////////////////////////////---------------------------------> 
        <div class="row">  
            <div class="col-md-6">
                <div class="card-block">
                    <div class="input-group">
                        กองหนุนชั้นที่ 3 :
                        <input
                            type="text"
                            class="form-control pickadate-disable-dates"
                            placeholder="25 กรกฏาคม 2562"
                            aria-describedby="button-addon4">
                        <div
                            class="input-group-append">
                            <button
                                class="btn btn-primary"
                                type="button"
                                style=" padding-bottom: 1px; padding-top: 1px;"><i
                                class="la la-calendar-o"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-block">
                    <div class="input-group">
                        พ้นราชการ :
                        <input
                            type="text"
                            class="form-control pickadate-disable-dates"
                            placeholder="พ้นราชการ"
                            aria-describedby="button-addon4">
                        <div
                            class="input-group-append">
                            <button
                                class="btn btn-primary"
                                type="button"
                                style=" padding-bottom: 1px; padding-top: 1px;"><i
                                class="la la-calendar-o"></i></button>
                        </div>
                    </div>
                </div>
            </div>
         </div>            
  <!-----------------------------////////////////////////////////---------------------------------> 
             </div>  
        </div>
    </div>
    <br>

    <!-- ----ส่วนที่2--- -->

    <div class="card collapse-icon accordion-icon-rotate active">
        <div id="headingCollapse37" class="card-header bg-success">
            <a data-toggle="collapse"
                href="#collapse37"
                aria-expanded="true"
                aria-controls="collapse37"
                class="card-title lead white">
                <h6><U>ส่วนที่ 3</U>
                    ลักษณะร่างกาย

                </h6>
            </a>
        </div>
      <!-----------------------------////////////////////////////////---------------------------------> 
        <div id="collapse37"
            role="tabpanel"
            aria-labelledby="headingCollapse37"
            class="card-collapse collapse show"
            aria-expanded="true">
            <div class="card-content">
            
       <!-----------------------------////////////////////////////////---------------------------------> 
       <div class="row">  
            <div class="col-md-3">
                <div class="card-block">
                    <div class="card-body ">
                        กลุ่มเลือด :
                        <select
                            class="select2 form-control"
                            style="width: 100%;">
                            <optgroup
                                label="สัญญาบัตร">
                                <option
                                    value="AK">
                                    เลือก
                                </option>
                                <option
                                    value="HI">
                                    พลอากาศเอก/พล.อ.อ.
                                </option>                 
                            </optgroup>                                   
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card-block">
                    <div class="card-body ">
                        ส่วนสูง :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card-block">
                    <div class="card-body ">
                        น้ำหนัก:
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card-block">
                    <div class="card-body ">
                        รอบอกหายใจออก :
                        <input
                            class="input form-control"
                            style="width: 100%;"
                            placeholder=" ">
                    </div>
                </div>
            </div>
      </div>
      <!-----------------------------////////////////////////////////---------------------------------> 
      <div class="row">  
        <div class="col-md-3">
            <div class="card-block">
                <div class="card-body ">
                    รอบอกหายใจเข้า :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card-block">
                <div class="card-body ">
                    รอบเอว :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card-block">
                <div class="card-body ">
                    ขนาดเสื้อ :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card-block">
                <div class="card-body ">
                    ขนาดกางเกง :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>
            </div>
        </div>
      </div>
      <!-----------------------------////////////////////////////////---------------------------------> 
      <div class="row">  
        <div class="col-md-3">
            <div class="card-block">
                <div class="card-body ">
                    ขนาดรองเท้า :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card-block">
                <div class="card-body ">
                    รูปร่าง :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card-block">
                <div class="card-body ">
                    สีผิว :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card-block">
                <div class="card-body ">
                    ตำหนิ/แผลเป็น :
                    <input
                        class="input form-control"
                        style="width: 100%;"
                        placeholder=" ">
                </div>
            </div>
        </div>
    </div>              
     <!-----------------------------////////////////////////////////---------------------------------> 
        </div>
     </div>
    <br>
<!-----------------------------////////////////////////////////---------------------------------> 
<!-----------------------------////////////////////////////////---------------------------------> 
<!-----------------------------////////////////////////////////---------------------------------> 
    <!-- -------ส่วนที่3 -->
    <div class="tab-content px-1 pt-1">
       
        <div class="form-actions center"
            align="center">
            <button type="button"
                class="btn btn-success  round btn-min-width mr-1 mb-1"
                id="submit"
                name="submit"
                data-target="#modalConfirm"
                onclick="insertOrganizationGroupType()"><i class="fa fa-save"></i> &nbsp;บันทึก </button>
            <button type="button"
                class="btn btn-danger  round btn-min-width mr-1 mb-1"
                id="type-error"><i class="fa fa-times-circle-o"></i> &nbsp;ยกเลิก</button>
        </div>
    </div>
</div>
</div>
</div>
<br>
</div>
</div>
</div>