
<div class="tab-pane" id="tab17"
aria-labelledby="base-tab17">


<a href="./create.php"
class="btn btn-social btn-min-width mb-1"
style="background-color:#0f1733; color:white;">
<span class="la la-plus-circle"
style="color:white; font-weight: bold;font-size: 18px"></span>
เพิ่ม
</a>
<a href="./delete.php"
class="btn btn-social btn-min-width mb-1"
style="background-color:#0f1733; color:white;">

<span class="la la-trash-o"
style="color:white; font-weight: bold;font-size: 18px"></span>
ลบ
</a>
<table
class="table table-striped table-borderless table-hover bootstrap-3 ">
<thead>
<tr align="center"
    style="background-color:#0f1733; color:whitesmoke;">
    <th><input type="checkbox"
            class="checkAll"
            onclick="toggle(this);" />
    </th>
    <th></th>
    <th>ลำดับที่</th>
    <th>ยศ</th>
    <th>คำนำหน้าชื่อ</th>
    <th>ชื่อ-สกุล</th>
    <th>สกุลเดิม</th>
    <th>วันเดือนปีจดทะเบียนสมรส</th>
    <th>สถานภาพ</th>

</tr>
</thead>
<tbody align="center">
<tr>
    <td><input type="checkbox"
            class="checkAll" /></td>
    <td>
        <a
            href="/Hris/Views/Spouse/index.php"><i
                class="la la-pencil-square-o"
                style="color:#0f1733;"></i></a>
        <a href="./delete.php"><i
                class="la la-trash-o"
                style="color:#0f1733;"></i></a>
    </td>
    <td>1</td>
    <td>พล.อ.พ.หญิง</td>
    <td>-</td>
    <td>พิรุฬห์ลักษณ์ โภคาสุข</td>
    <td>ข่มอาวุธ</td>
    <td>11 พ.ค. 2562</td>
    <td>สมรส</td>

</tr>
<tr>
    <td><input type="checkbox"
            class="checkAll" /></td>
    <td>
        <a
            href="/Hris/Views/Spouse/index.php"><i
                class="la la-pencil-square-o"
                style="color:#0f1733;"></i>
            <a href="./detail.php"><i
                    class="la la-trash-o"
                    style="color:#0f1733;"></i>
    </td>
    <td>2</td>
    <td>พล.อ.พ.หญิง</td>
    <td>-</td>
    <td>พิรุฬห์ลักษณ์ โภคาสุข</td>
    <td>ข่มอาวุธ</td>
    <td>11 พ.ค. 2562</td>
    <td>หย่า</td>

</tr>
</tbody>

</table>



</div>
