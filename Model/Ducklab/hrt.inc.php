<?php
header('Access-Control-Allow-Origin: *');
header("Content-type: text/html; charset=utf-8");

require_once "duck.class.php";
require_once "func.php";
require_once "hrt.class.php";


function AddDataSet(){
	$clshrt = new HrtClass();

	$table  = $_POST['table'];
	$data  	= $_POST['data'];

	$result = $clshrt->AddData($table,$data);

	echo json_encode($result);
}

function AddDataID(){
	$clshrt = new HrtClass();

	$table  = $_POST['table'];
	$data  	= $_POST['data'];

	$result = $clshrt->AddDataID($table,$data);

	echo json_encode($result);
}

function EditDataSet(){
	$clshrt = new HrtClass();

	$table  = $_POST['table'];
	$data  	= $_POST['data'];
	$where  = $_POST['where'];

	$result = $clshrt->EditData($table,$data,$where);

	echo json_encode($result);
}

function DeleteDataSet(){
	$clshrt = new HrtClass();

	$table  = $_POST['table'];
	$where  = $_POST['where'];

	$result = $clshrt->DeleteData($table,$where);

	echo json_encode($result);

}
 
function EditDataContents(){

		$clshrt = new HrtClass();

		$table  = $_POST['table'];
		$data  	= $_POST['data'];
		$data_content  	= $_POST['data_content'];
		$where  = $_POST['where'];

		foreach($data_content as $key => $value ){
			$data_informations = ContentFormat( $value );
			$data[$key] = $data_informations ;
		}

		$result = $clshrt->EditData($table,$data,$where);
		echo json_encode($result);
}

function AddDataContents(){

		$clshrt = new HrtClass();

		$table  = $_POST['table'];
		$data  	= $_POST['data'];
		$data_content  	= $_POST['data_content'];

		if($data_content){
			foreach($data_content as $key => $value ){
				$data_informations = ContentFormat( $value );
				$data[$key] = $data_informations ;
			}
		}

		$result = $clshrt->AddData($table,$data,$where);
		echo json_encode($result);
}

function LoadAllData(){
	$clshrt = new HrtClass();

	$table = $_POST['table'];
	$where = $_POST['where'];
	$orderby = $_POST['orderby'];
	$limit = $_POST['limit'];

	$result=$clshrt->Load($table, $where , $orderby,$limit);
	echo json_encode($result);
} 

function LoadFieldData(){
	$clshrt = new HrtClass();

	$table = $_POST['table'];
	$where = $_POST['where'];
	$field = $_POST['field'];
	$orderby = $_POST['orderby'];
	$limit = $_POST['limit'];

	$result=$clshrt->LoadFields($table, $field , $where , $orderby,$limit);
	echo json_encode($result);
}

function LoadOneRow(){
	$clshrt = new HrtClass();

	$table = $_POST['table'];
	$where = $_POST['where'];


	$result=$clshrt->LoadOnce($table, $where  );
	echo json_encode($result);
}

function LoadLikeTitle(){
	$clshrt = new HrtClass();

	$table = $_POST['table'];
	$where = $_POST['where'];
	$wherelike = $_POST['wherelike'];
	$orderby = $_POST['orderby'];
	$limit = $_POST['limit'];

	$result=$clshrt->LoadLikeTitle($table, $where , $wherelike, $orderby,$limit );
	echo json_encode($result);
}

function StatusDataSetEX(){
	$clshrt = new HrtClass();

	$table 		= $_POST['table'];
	$data  		= $_POST['data'];
	$data_name  = $_POST['data_name'];

	$status_n = $data_name['status_n'] ;
	$id_n = $data_name['id_n'] ;

	$data_new[''.$status_n.''] = $data['status'];
	$where_new[''.$id_n.''] = $data['id'];


	$result = $clshrt->EditData($table,$data_new,$where_new);

	echo json_encode($result);
}
////////////////////////////////////////////////////////////////////////////////////////////

function LoadChiefSpc(){
	$clshrt = new HrtClass();


	$where = $_POST['where'];

	$result=$clshrt->LoadChiefSpc( $where );
	echo json_encode($result);
} 

///////////////////////////////////////////////////////////////////////////////////////////////

function LoadArm(){
	$clshrt = new HrtClass();


	$where = $_POST['where'];

	$result=$clshrt->LoadArm( $where );
	echo json_encode($result);
} 
////////////////////////////////////////////////////////////////////////////////////////////

function LoadSkillSuffix(){
	$clshrt = new HrtClass();


	$where = $_POST['where'];

	$result=$clshrt->LoadSkillSuffix( $where );
	echo json_encode($result);
} 

///////////////////////////////////////////////////////////////////////////////////////////////

function LoadSkillPrefix(){
	$clshrt = new HrtClass();


	$where = $_POST['where'];

	$result=$clshrt->LoadSkillPrefix( $where );
	echo json_encode($result);
} 
///////////////////////////////////////////////////////////////////////////////////////////////
function LoadAmphur(){
	$clshrt = new HrtClass();


	$where = $_POST['where'];

	$result=$clshrt->LoadAmphur( $where );
	echo json_encode($result);
} 
///////////////////////////////////////////////////////////////////////////////////////////////
function LoadCountDistByField(){
	$clshrt = new HrtClass();
	$table  = $_POST['table'];
	$field  = $_POST['field'];
	$where  = $_POST['where'];
	 
	$result = $clshrt->LoadCountDistByField($table,$field, $where); 
	
	echo json_encode($result); 
   }
// ///////////////////////////////////////////////////////////////////////////////////////////////


function LoadDataSkill(){
	$clshrt = new HrtClass(); 
	$where  = $_POST['where'];
	 
	$result = $clshrt->LoadDataSkill( $where); 
	
	echo json_encode($result); 
   }

///////////////////////////////////////////////////////////////////////////////////////////////

function LoadSpc(){
	$clshrt = new HrtClass();


	$where = $_POST['where'];

	$result=$clshrt->LoadSpc( $where );
	echo json_encode($result);
} 
/////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////
switch($_REQUEST["mode"]){
	case "AddDataSet" : AddDataSet(); break;
	case "AddDataID" : AddDataID(); break;
	case "EditDataSet" : EditDataSet(); break;
	case "DeleteDataSet" : DeleteDataSet(); break;
	case "AddDataContents" : AddDataContents(); break;
	case "EditDataContents" : EditDataContents(); break; 
	case "LoadAllData" : LoadAllData(); break; 
	case "LoadFieldData" : LoadFieldData(); break;
	case "LoadOneRow" : LoadOneRow(); break;
	
	case "LoadDataSkill" : LoadDataSkill(); break;
	case "LoadLikeTitle" : LoadLikeTitle(); break;
	case "StatusDataSetEX" : StatusDataSetEX(); break;
	case "LoadCountDistByField" : LoadCountDistByField(); break;
	////////////////////////////////////////////////////////////////////////////////////////
	
	case "LoadChiefSpc" : LoadChiefSpc(); break;
	case "LoadSpc" : LoadSpc(); break;
	
	////////////////////////////////////////////////////////////////////////////////////////
	case "LoadArm" : LoadArm(); break;
	
	////////////////////////////////////////////////////////////////////////////////////////

	case "LoadSkillSuffix" : LoadSkillSuffix(); break;
	
	////////////////////////////////////////////////////////////////////////////////////////
	
	case "LoadSkillPrefix" : LoadSkillPrefix(); break;
	
	////////////////////////////////////////////////////////////////////////////////////////
	case "LoadAmphur" : LoadAmphur(); break; 

	case "LoadDataSkill" : LoadDataSkill(); break; 
	
	////////////////////////////////////////////////////////////////////////////////////////
	default : echo '{"success":"FAIL INC HrtClass"}';
}
?>
