<?php
header('Access-Control-Allow-Origin: *');
header("Content-type: text/html; charset=utf-8");

class OrgClass extends DuckClass{
	////////////////////////////////////////////////////////////////////////////////////////////
	public function LoadOrg($table, $where=array(), $orderby='', $limit=''){
		$qrywhere ="";
		if(!empty($where)){
		  foreach((array)$where as $i => $item){
			$item = mssql_escape($item);
			$qrywhere .= " $i = '$item' AND ";
		  }
		}
		if(!empty($orderby)){
		  $orderby = "ORDER BY $orderby";
		}
		if(!empty($limit)){
		  $limit = "LIMIT $limit";
		}
			$sql="
				  SELECT *
				  FROM $table
				  WHERE
					$qrywhere
					1 = 1
				  $orderby
				  $limit
		";
		 //  echo '------------------------------------------------------------'.$sql;
		  //$result['sql'] = $sql;

		 
		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result[] =  $row;
        }
		 
		

        return $result;
	}
	/*
	public function LoadOnce($table, $where=array() ){
		$qrywhere ="" ;
		if(!empty($where)){
		  foreach((array)$where as $i => $item){
			$item = mssql_escape($item);
			$qrywhere .= " $i = '$item' AND ";
		  }
		} 

		$sql = " SELECT * FROM  $table  WHERE $qrywhere 1=1  "; 
		//echo  "<br>:: ".$sql;
		$stmt = $this->consqls->prepare($sql); 
		$stmt->execute();
        $result = $stmt->fetch( PDO::FETCH_ASSOC );
         
        return $result;
   
	}*/
	
	public function LoadOrgStrucByOrgType( $OrgTypeID , $orgParentDID){
		 
		 	//$OrgTypeID = mssql_escape_string($OrgTypeID);
		 
		/*
		$sql = "  
				SELECT l0.*  
				FROM OrgLevel0 l0  
				LEFT JOIN OrgLevel1 l1 ON (l1.)
				WHERE 
					OrgLevType = '".$OrgTypeID."'  

 
		"; */
			 
		// $sql ="
		// SELECT l2.*  
		// FROM OrgLevel2 l2
		 
		// WHERE 
		// 	OrgLevType = '".$OrgTypeID."' 
			
		
		// " ; 

		$sql = "
		
		" ; 

		$sql ="
		SELECT l2.*  
		FROM OrgLevel2 l2
		
		$orgParentDID
		 
			
		
		" ; 
		 
		//AND  OrgAF = '1' 

		//echo $sql;
 
		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result[] =  $row;
        }
		 
		return $result;
	}
	
	public function LoadLv2FindLv1($OrgTypeID){
		$OrgTypeID = mssql_escape_string($OrgTypeID);
		$sql ="
		SELECT o1.OrgLevDID as OrgLevDID1 , o1.OrgLevName as OrgLevName1 , o0.OrgLevDID as OrgLevDID0, o0.OrgLevName as OrgLevName0 
  
		FROM OrgLevel2Parent op2 
		LEFT JOIN OrgLevel1 o1 ON (o1.OrgLevDID = op2.OrgParentCode ) 
		LEFT JOIN OrgLevel1Parent op1 ON (op1.OrgLevID = o1.OrgLevID ) 
		LEFT JOIN OrgLevel0 o0 ON (o0.OrgLevID = op1.OrgParentCode ) 
		WHERE op2.OrgLevDID = '".$OrgTypeID."'
		
		" ; 
		//echo $sql;
		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result =  $row;
        }
		 
		return $result;
	}


	public function LoadLevel2ByDID( $OrgLevel2DID){
		$sql = "
			SELECT * FROM OrgLevel2 WHERE OrgLevDID = '".$OrgLevel2DID."'
		";
		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result =  $row;
        }
		 
		return $result;
	}

	public function LoadLevelXByParent(  $OrgCodeParent ,$OrgTypeID="" ,$x){
		$qr = "";
		if(!empty($OrgTypeID)){
			$qr .= " AND  o.OrgLevType =  '".$OrgTypeID."'   "  ;
		}

		$tablep = "OrgLevel".$x."Parent " ; 
		$tablen = "OrgLevel".$x ; 
		$sql ="
			SELECT  o.OrgLevID , o.OrgLevDID , o.OrgLevNID , o.OrgLev ,o.OrgLevName ,o.OrgLevAbbrName ,o.LocCode
 
			FROM $tablep op 
			LEFT JOIN  $tablen o ON (o.OrgLevID = op.OrgLevID )

			WHERE 
				op.OrgParentCode = '".$OrgCodeParent."'  
				AND o.OrgLevDelete = '0' 
				 
				AND o.OrgLevState = 'N' 
				$qr 
				AND 1=1 
		" ;

		// echo $sql ;
		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result[] =  $row;
        }
		 
		return $result;
	}

	public function LoadLevel1ByParent(  $OrgCodeParent ,$OrgTypeID="" ){
		$qr = "";
		if(!empty($OrgTypeID)){
			$qr .= " AND  o.OrgLevType =  '".$OrgTypeID."'   "  ;
		}
		$sql ="
			SELECT  o.OrgLevID , o.OrgLevDID , o.OrgLevNID , o.OrgLev ,o.OrgLevName ,o.OrgLevAbbrName ,o.LocCode
 
			FROM OrgLevel1Parent op 
			LEFT JOIN  OrgLevel1 o ON (o.OrgLevID = op.OrgLevID )

			WHERE 
				op.OrgParentCode = '".$OrgCodeParent."'  
				AND o.OrgLevDelete = '0' 
				 
				AND o.OrgLevState = 'N' 
				$qr 
				AND 1=1 
		" ;

		// echo $sql ;
		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result[] =  $row;
        }
		 
		return $result;
	}

	public function LoadLevel2ByParent(  $OrgCodeParent ,$OrgTypeID="" ){
		$qr = "";
		if(!empty($OrgTypeID)){
			$qr .= " AND  o.OrgLevType =  '".$OrgTypeID."'   "  ;
		}
		$sql ="
			SELECT  o.OrgLevID , o.OrgLevDID , o.OrgLevNID , o.OrgLev ,o.OrgLevName ,o.OrgLevAbbrName ,o.LocCode
 
			FROM OrgLevel2Parent op 
			LEFT JOIN  OrgLevel2 o ON (o.OrgLevID = op.OrgLevID )

			WHERE 
				op.OrgParentCode = '".$OrgCodeParent."'  
				AND o.OrgLevDelete = '0' 
				 
				AND o.OrgLevState = 'N' 
				$qr 
				AND 1=1 
		" ;

		// echo $sql ;
		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result[] =  $row;
        }
		 
		return $result;
	}

	public function LoadLevel3ByParent(  $OrgCodeParent ,$OrgTypeID="" ){
		$qr = "";
		if(!empty($OrgTypeID)){
			$qr .= " AND  o3.OrgLevType =  '".$OrgTypeID."'   "  ;
		}
		$sql ="
			SELECT  o3.OrgLevID , o3.OrgLevDID , o3.OrgLevNID , o3.OrgLev ,o3.OrgLevName ,o3.OrgLevAbbrName ,o3.LocCode
 
			FROM OrgLevel3Parent op3 
			LEFT JOIN  OrgLevel3 o3 ON (o3.OrgLevID = op3.OrgLevID )

			WHERE 
				op3.OrgParentCode = '".$OrgCodeParent."'  
				AND o3.OrgLevDelete = '0' 
				AND o3.OrgLevState = 'N' 
				$qr 
				AND 1=1 
		" ;

		//echo $sql ;
		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result[] =  $row;
        }
		 
		return $result;
	}

	public function LoadLevel4ByParent(  $OrgCodeParent ,$OrgTypeID="" ){
		$qr = "";
		if(!empty($OrgTypeID)){
			$qr .= " AND  o.OrgLevType =  '".$OrgTypeID."'   "  ;
		}
		$sql ="
			SELECT  o.OrgLevID , o.OrgLevDID , o.OrgLevNID , o.OrgLev ,o.OrgLevName ,o.OrgLevAbbrName ,o.LocCode
 
			FROM OrgLevel4Parent op 
			LEFT JOIN  OrgLevel4 o ON (o.OrgLevID = op.OrgLevID )

			WHERE 
				op.OrgParentCode = '".$OrgCodeParent."'  
				AND o.OrgLevDelete = '0' 
				 
				AND o.OrgLevState = 'N' 
				$qr 
				AND 1=1 
		" ;

		// echo $sql ;
		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result[] =  $row;
        }
		 
		return $result;
	}

	public function LoadLevel5ByParent(  $OrgCodeParent ,$OrgTypeID="" ){
		$qr = "";
		if(!empty($OrgTypeID)){
			$qr .= " AND  o.OrgLevType =  '".$OrgTypeID."'   "  ;
		}
		$sql ="
			SELECT  o.OrgLevID , o.OrgLevDID , o.OrgLevNID , o.OrgLev ,o.OrgLevName ,o.OrgLevAbbrName ,o.LocCode
 
			FROM OrgLevel5Parent op 
			LEFT JOIN  OrgLevel5 o ON (o.OrgLevID = op.OrgLevID )

			WHERE 
				op.OrgParentCode = '".$OrgCodeParent."'  
				AND o.OrgLevDelete = '0' 
				 
				AND o.OrgLevState = 'N' 
				$qr 
				AND 1=1 
		" ;
 
		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result[] =  $row;
        }
		 
		return $result;
	}

	public function LoadLevel6ByParent(  $OrgCodeParent ,$OrgTypeID="" ){
		$qr = "";
		if(!empty($OrgTypeID)){
			$qr .= " AND  o.OrgLevType =  '".$OrgTypeID."'   "  ;
		}
		$sql ="
			SELECT  o.OrgLevID , o.OrgLevDID , o.OrgLevNID , o.OrgLev ,o.OrgLevName ,o.OrgLevAbbrName ,o.LocCode
 
			FROM OrgLevel6Parent op 
			LEFT JOIN  OrgLevel6 o ON (o.OrgLevID = op.OrgLevID )

			WHERE 
				op.OrgParentCode = '".$OrgCodeParent."'  
				AND o.OrgLevDelete = '0' 
				 
				AND o.OrgLevState = 'N' 
				$qr 
				AND 1=1 
		" ;

		// echo $sql ;
		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result[] =  $row;
        }
		 
		return $result;
	}

	public function LoadLevel7ByParent(  $OrgCodeParent ,$OrgTypeID="" ){
		$qr = "";
		if(!empty($OrgTypeID)){
			$qr .= " AND  o.OrgLevType =  '".$OrgTypeID."'   "  ;
		}
		$sql ="
			SELECT  o.OrgLevID , o.OrgLevDID , o.OrgLevNID , o.OrgLev ,o.OrgLevName ,o.OrgLevAbbrName ,o.LocCode
 
			FROM OrgLevel7Parent op 
			LEFT JOIN  OrgLevel7 o ON (o.OrgLevID = op.OrgLevID )

			WHERE 
				op.OrgParentCode = '".$OrgCodeParent."'  
				AND o.OrgLevDelete = '0' 
				 
				AND o.OrgLevState = 'N' 
				$qr 
				AND 1=1 
		" ;

		// echo $sql ;
		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result[] =  $row;
        }
		 
		return $result;
	}

	public function LoadLevel8ByParent(  $OrgCodeParent ,$OrgTypeID="" ){
		$qr = "";
		if(!empty($OrgTypeID)){
			$qr .= " AND  o.OrgLevType =  '".$OrgTypeID."'   "  ;
		}
		$sql ="
			SELECT  o.OrgLevID , o.OrgLevDID , o.OrgLevNID , o.OrgLev ,o.OrgLevName ,o.OrgLevAbbrName ,o.LocCode
 
			FROM OrgLevel8Parent op 
			LEFT JOIN  OrgLevel8 o ON (o.OrgLevID = op.OrgLevID )

			WHERE 
				op.OrgParentCode = '".$OrgCodeParent."'  
				AND o.OrgLevDelete = '0' 
				 
				AND o.OrgLevState = 'N' 
				$qr 
				AND 1=1 
		" ;

		// echo $sql ;
		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result[] =  $row;
        }
		 
		return $result;
	}

	public function LoadLevelByParent(  $OrgCodeParent ,$OrgTypeID="" , $tablename  ){
		$qr = "";
		if(!empty($OrgTypeID)){
			$qr .= " AND  o.OrgLevType =  '".$OrgTypeID."'   "  ;
		}
		$sql ="
			SELECT  o.OrgLevID , o.OrgLevDID , o.OrgLevNID , o.OrgLev ,o.OrgLevName ,o.OrgLevAbbrName ,o.LocCode
 
			FROM ".$tablename."Parent op 
			LEFT JOIN  ".$tablename." o ON (o.OrgLevID = op.OrgLevID )

			WHERE 
				op.OrgParentCode = '".$OrgCodeParent."'  
				AND o.OrgLevDelete = '0' 
				 
				AND o.OrgLevState = 'N' 
				$qr 
				AND 1=1 
		" ;
		//echo $sql;
		 
		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result[] =  $row;
        }
		 
		return $result;
	}


	public function LoadLevel1ByOrgType($OrgType){

		$sql = "
			SELECT o1.*  
			FROM OrgLevel0  o0 
			RIGHT JOIN OrgLevel1Parent op1 ON ( op1.OrgParentCode = o0.OrgLevDID )
			RIGHT JOIN OrgLevel1 o1 ON ( o1.OrgLevID = op1.OrgLevID )
			WHERE 
				o0.OrgLevType = '".$OrgType."'
		" ; 

		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result[] =  $row;
        }
		// $this->consqls = null;
		return $result;
	}

	public function LoadLevel2ByOrgType($OrgType){

		$sql = "
			SELECT o2.*  
			FROM OrgLevel0  o0 
			RIGHT JOIN OrgLevel2Parent op2 ON ( op2.OrgParentCode = o0.OrgLevDID )
			RIGHT JOIN OrgLevel2 o2 ON ( o2.OrgLevID = op2.OrgLevID )
			WHERE 
				o0.OrgLevType = '".$OrgType."' 

			ORDER BY o2.OrgLevName ASC 
		" ; 

		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result[] =  $row;
        }
		// $this->consqls = null;
		return $result;
	}
 

	public function LoadRankSort($ptype){ 
		/*
			SELECT DISTINCT(h.hrtranksort) AS RankSort , 
		( select TOP(1) HrtRankID  FROM HrtRank  WHERE  hrtranksort =h.hrtranksort    ) AS RankID  , 
		( select TOP(1) HrtRankAbbrTh  FROM HrtRank  WHERE  hrtranksort =h.hrtranksort    ) AS HrtRankAbbrTh  , 
		( select TOP(1) HrtRankNameTh  FROM HrtRank  WHERE  hrtranksort =h.hrtranksort    ) AS HrtRankNameTh  
		FROM [HrtRank]   h  
		WHERE 
		h.hrtranksort <> 'NULL' 
		AND h.HrtRankDelete = 0 
		AND h.HrtRankActive = 1 
		AND h.HrtRankID LIKE '3%' 

		*/
		$querywhere ="";
		if($ptype){
			$querywhere = " h.PersonTypeID = '$ptype'  AND " ;
		}
		$sql = "	 
			SELECT 
			HrtRankID , HrtRankAbbrTh , HrtRankNameTh , HrtRankSort
			FROM [HrtRank]   h  
			WHERE 
			h.HrtRankSort <> 'NULL' 
			AND h.HrtRankDelete = 0 
			AND h.HrtRankActive = 1  
			AND h.HrtRankID LIKE '30%'  
			AND h.HrtRankID LIKE '%1' 
			AND 
			$querywhere 
			1=1
		" ;
		//	echo $sql;
		//$result['sql'] =  $sql;
		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result[] =  $row;
        }
		// $this->consqls = null;
		return $result;

	}


	public function LoadDataOrgParent($orgLevDID){

		$clsorg = new OrgClass();  

		$levelchar = substr($orgLevDID ,0,2 ) ;
		$level = (int)$levelchar;   
		 
		 
		$orgLev  = $level;
		$orgLevDID  = $orgLevDID;

		$nowOrg =  $orgLevDID ;


		$orgNameX ="";
		$orgNameAbbrX = "";
		$orgNameAbbrSemiX = "";
		

		for( $x=$orgLev ; $x>1 ; $x-- ){
 

			$x = (int)$x ;
			$y = ( (int)$x)-1 ;
			$tableP = 'OrgLevel'.$x.'Parent' ;   //$orgLevDID
			$tableO = 'OrgLevel'.$x ; 
			$table1 = 'OrgLevel'.$y ; 

			$result['unitM'][$x]['OrgLevelDID'] =  $nowOrg ;   
			$result['unitM'][$x]['tableP'] =  $tableP ; 
	
			$result['unitM'][$x]['detail'] = $clsorg->LoadOnce($tableO, array('OrgLevDID'=>$nowOrg ));
			$result['unitM'][$x]['parent'] = $clsorg->LoadOnce($tableP, array('OrgLevDID'=>$nowOrg ));
		

			if($result['unitM'][$x]['parent'] ){ 
				$nowOrg = $result['unitM'][$x]['parent']['OrgParentCode'];
				$result['unitM'][$x]['parent']['unit'] = $clsorg->LoadOnce($table1, array('OrgLevDID'=> $nowOrg ));
				  
			}

			$orgNameX = $orgNameX ." ".$result['unitM'][$x]['detail']['OrgLevName'];
			$orgNameAbbrX = $orgNameAbbrX ." ".$result['unitM'][$x]['detail']['OrgLevAbbrName'];
			 
			if($result['unitM'][$x]['detail']['OrgLev'] >= 35){
				$orgNameAbbrSemiX = $orgNameAbbrSemiX ." ".$result['unitM'][$x]['detail']['OrgLevName']; ;
			}else{
				$orgNameAbbrSemiX = $orgNameAbbrSemiX ." ".$result['unitM'][$x]['detail']['OrgLevAbbrName'];;
			}

		}
 


		$result['listname']['orgName'] =   $orgNameX ; 
		$result['listname']['orgNameAbbr'] = $orgNameAbbrX ; 
		$result['listname']['orgNameAbbrSemi'] =  $orgNameAbbrSemiX ; 
		$result['listname']['orgAF'] = '' ; 

		return $result ;
	}



	public function LoadPositionByOrgLevelx($OrgLevelDID){ //"04000056"
	 
		$sql = "   
			SELECT 
		 
			hp.HrtPosCode , hp.HrtRankID , 
			( SELECT TOP(1)  h.HrtPosNum   FROM HrtPosition h  WHERE h.HrtPosCode= hp.HrtPosCode AND h.HrtRankID = hp.HrtRankID AND h.OrgLevelDID = '$OrgLevelDID') AS HrtPosNum , 
			( SELECT TOP(1)  h.HrtPositionID   FROM HrtPosition h  WHERE h.HrtPosCode= hp.HrtPosCode AND h.HrtRankID = hp.HrtRankID AND h.OrgLevelDID = '$OrgLevelDID'  ) AS HrtPositionID , 
			( SELECT TOP(1)  h.HrtPosName   FROM HrtPosition h  WHERE h.HrtPosCode= hp.HrtPosCode AND h.HrtRankID = hp.HrtRankID   ) AS HrtPosName , 
			( SELECT TOP(1)  pos.HrtPosAbbrName   FROM HrtPos  pos  WHERE pos.HrtPosCode= hp.HrtPosCode   ) AS HrtPosAbbrName , 
			( SELECT  TOP(1) h.HrtPosPrintSeq  FROM HrtPosition h  WHERE h.HrtPosCode= hp.HrtPosCode AND h.HrtRankID = hp.HrtRankID AND h.OrgLevelDID = '$OrgLevelDID' ) AS HrtPosPrintSeq ,
			( SELECT  TOP(1) h.CancleCommDocDate  FROM HrtPosition h  WHERE h.HrtPosCode= hp.HrtPosCode AND h.HrtRankID = hp.HrtRankID AND h.OrgLevelDID = '$OrgLevelDID'  ) AS CancleCommDocDate , 
			( SELECT  TOP(1) h.HrtPositionActive  FROM HrtPosition h  WHERE h.HrtPosCode= hp.HrtPosCode AND h.HrtRankID = hp.HrtRankID AND h.OrgLevelDID = '$OrgLevelDID' ) AS HrtPositionActive ,
			( SELECT TOP(1) HrtRankAbbrTh  FROM HrtRank  WHERE  HrtRankID = hp.HrtRankID    ) AS RankName

			FROM 
				HrtPosition  hp 
			WHERE  hp.OrgLevelDID = '$OrgLevelDID'
			GROUP BY hp.HrtPosCode ,hp.HrtRankID
			ORDER BY  HrtPosPrintSeq  DESC 

		" ;
		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result[] =  $row;
        }
		// $this->consqls = null;
		return $result;

	}

	public function LoadPersonByPositionNo($positionID ,$posNo ){
	 //AND hst.HrtPosNum =  hpp.PosNo
		$qrywhere = '';
	 	if($posNo){
			$qrywhere .= " AND hpp.PosNo = '$posNo' ";
		}
		$sql = "
			SELECT hpp.ID , hpp.PositionID , hpp.PosNo , hpp.DirectpayCode ,hpp.PosPerStatus, hpp.SourceOfCommID , hpp.CommDocNo , hpp.CommDocDate, hpp.CommEffDate, hpp.CommLevelID , hpp.PosPerStatusPsr , hpp.SourceOfCommIDPsr , hpp.CommDocNoPsr , hpp.CommDocDatePsr ,hpp.CommEffDatePsr, hpp.CommItemNoPsr , hpp.PersonID , hpp.BudgetYearPlan, hpp.IsReservedByPlan  , hp.PerCardID, hp.AirForceID, hp.PrefixID,  hp.RankID, hp.SubtMPay ,hp.SalID , hp.PersonName, hp.SurName, hp.SpcID, hp.ArmCode , hp.SkillNo01Prefix , hp.SkillNo01, hp.SkillNo01Suffix , hp.SkillNo02Prefix , hp.SkillNo02 , hp.SkillNo02Suffix , hp.SkillNo03Prefix , hp.SkillNo03 , hp.SkillNo03Suffix ,  a.ArmName , a.ArmAbbrName ,hpp.PosPerStatus , hppt.PosPerStatusDesc ,hppt.PosPerStatusPrn , sc.SourceOfCommName , sc.SourceOfCommFullName , sal.SalDetailAbbr , r.HrtRankNameTh  , r.HrtRankAbbrTh
			FROM HrtPosPerson hpp 
			LEFT JOIN  HrtPosition hst ON ( hst.HrtPositionID =  hpp.PositionID  )
			LEFT JOIN  HrtPerson hp ON ( hp.PersonID =  hpp.PersonID )
			LEFT JOIN  HrtArm a ON ( a.ArmCode =   hp.ArmCode)
			LEFT JOIN  HrtPosPerStatus hppt ON ( hppt.PosPerStatus = hpp.PosPerStatus )
			LEFT JOIN  HrtSourceOfComm  sc ON ( sc.SourceOfCommID = hpp.SourceOfCommID )
			LEFT JOIN  HrtSal  sal ON ( sal.SalID = hp.SalID )
			LEFT JOIN  HrtRank r ON ( r.HrtRankID = hp.RankID )
			WHERE 
			hpp.PositionID = '".$positionID."' 
			AND hst.HrtPositionActive = '1' 
			AND hpp.PosPersonActive = '1' 
			$qrywhere
			AND 1=1 
			ORDER BY hpp.PosNo
			 
		";
		//  echo $sql."<br>" ;
		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result[] =  $row;
        } 
		return $result; 
	}

	public function LoadPositionDetail($PositionID){
		$sql = "
			SELECT hp.*, r.HrtRankNameTh  , r.HrtRankAbbrTh, p.HrtPosName AS PosName , p.HrtPosAbbrName AS PosNameAbbr , p.HrtPersonTypeID as PersonType 
			FROM HrtPosition  hp 
			LEFT JOIN HrtPos  p ON ( p.HrtPosCode = hp.HrtPosCode )
			LEFT JOIN HrtRank r ON ( r.HrtRankID = hp.HrtRankID )
			WHERE 
			hp.HrtPositionID = '".$PositionID."' 
		";
		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result =  $row;
        } 
		return $result;
	}

 
	public function  LoadPosStrucAddAmt($PositionID, $AddAmtCode){
		$qrywhere = "";
		if($AddAmtCode){
			$qrywhere = " AND  hps.AddAmtCode = '".$AddAmtCode."' " ;
		}
		$sql ="
			SELECT hps.* ,  ha.AddamtName , ha.AddamtAbbrName 
			FROM HrtPosStrucAddAmt  hps   
			LEFT JOIN HrtAddamt ha ON ( ha.AddamtCode = hps.AddAmtCode  )
			WHERE 
			hps.PositionID = '".$PositionID."' 
			$qrywhere 
			AND 1=1 
		"; 
		//echo $sql;
		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result[] =  $row;
        } 
		return $result;
	}


	 public function LoadPosStrucSkill($PositionID){
		$sql ="
			SELECT hps.* ,  hs.SkillName , hs.SkillAbbrName , ha.ArmName , ha.ArmAbbrName 
			FROM HrtPosStrucSkill  hps   
			LEFT JOIN HrtSkill hs ON ( hs.SkillNo = hps.SkillNo  )
			LEFT JOIN HrtArm ha ON ( ha.ArmCode = hps.ArmCode  )
			WHERE 
			hps.PositionID = '".$PositionID."' 
		 
			AND 1=1 
			"; 
			//echo $sql;
			$stmt = $this->consqls->prepare($sql);
			$stmt->execute();
			$result = array();
			while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
			{
				$result[] =  $row;
			} 
			return $result;
	 }
	
}


?>
