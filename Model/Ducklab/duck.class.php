<?php
header('Access-Control-Allow-Origin: *');
header("Content-type: text/html; charset=utf-8");

class DuckClass{

	public $serverName = "" ; 
    public $dbname = "" ; 
    public $userdb = "" ; 
    public $passdb = "" ; 
    public $consqls = "";

	public function __construct(){

	 
	 
        // $this->serverName =  "LAPTOP-CLE23AOM\SQLEXPRESS";
        // $this->dbname =  "HRIS_NEW3";
        // $this->userdb =  "";
		// $this->passdb =  ""; 

		$this->serverName =  "DESKTOP-L98371R";
        $this->dbname =  "HRIS_NEW6";
        $this->userdb =  "";
		$this->passdb =  ""; 


		$this->consqls	= "";
        $this->connectDb(); 
	}

	private function connectDb(){
		 $this->consqls = new PDO( "sqlsrv:Server=".$this->serverName." ; Database =".$this->dbname."", $this->userdb, $this->passdb, array(PDO::SQLSRV_ATTR_DIRECT_QUERY => true));   

	 
		$this->consqls->setAttribute(PDO::SQLSRV_ATTR_ENCODING, PDO::SQLSRV_ENCODING_UTF8);
 


		if($this->consqls){
           // echo "<span style='color:green'> SqlServ:: Connection success </span>"; 
		}else{
			echo "<span style='color:red'> SqlServ:: Connection fail </span>";
			echo "<pre>".die(print_r(sqlsrv_errors(), true))."<pre>";
		}

	} 

	 
	public function LoadOnce($table, $where=array() ){
		$qrywhere ="" ;
		if(!empty($where)){
		  foreach((array)$where as $i => $item){
			$item = mssql_escape($item);
			$qrywhere .= " $i = '$item' AND ";
		  }
		} 

		$sql = " SELECT * FROM  $table  WHERE $qrywhere 1=1  "; 
		//echo  "<br>:: ".$sql;
		$stmt = $this->consqls->prepare($sql); 
		$stmt->execute();
        $result = $stmt->fetch( PDO::FETCH_ASSOC );
    	 
        return $result;
   
	}

	public function LoadFields($table, $fields=array() , $where=array(), $orderby='', $limit=''){
		$qrywhere ="";
		$qlimit ="";
		if(!empty($where)){
		  foreach((array)$where as $i => $item){
			  if($item || $item ==0){
				$item = mssql_escape($item);
				$qrywhere .= " $i = '$item' AND ";
			  }
			
		  }
		}
		$fieldslist ="";
		if(!empty($fields)){
		  	foreach((array)$fields as $j => $item2){
				if($item2 || $item2 ==0){
					$item2 = mssql_escape($item2);
					$fieldslist .= " $j,";
				}
			}
		  $fieldslist= substr( $fieldslist, 0 ,-1);
		}else{
			$fieldslist = "*";
		}
		if(!empty($orderby)){
		  $orderby = "ORDER BY $orderby";
		}
		if(!empty($limit)){
		  $qlimit = " TOP($limit) ";
		}
			$sql="
				  SELECT $qlimit $fieldslist 
				  FROM $table
				  WHERE
					$qrywhere
					1 = 1
				  $orderby
				  
		";
		 //  echo '------------------------------------------------------------'.$sql;
		//   $result['sql'] = $sql;
 
		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result[] =  $row;
        } 

        return $result;
	}


	public function Load($table, $where=array(), $orderby='', $limit=''){
		$qrywhere ="";
		$qlimit ="";
		if(!empty($where)){
		  	foreach((array)$where as $i => $item){
				if( $item || $item=='0' ){ 
					//$item = mssql_escape($item);
					$item = $item;
					$qrywhere .= " $i = '$item' AND "; 
		 	 	}
			}
		}
		if(!empty($orderby)){
		  $orderby = "ORDER BY $orderby";
		}
		if(!empty($limit)){
			$qlimit = "TOP ($limit)";
		}
			$sql="
				  SELECT $qlimit *
				  FROM $table
				  WHERE
					$qrywhere
					1 = 1
				  $orderby
				  
		";
		//    echo '------------------------------------------------------------'.$sql;
		 // $result['sql'] = $sql;

		 
		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result[] =  $row;
        }
		
		

        return $result;
	}
	
	public function LoadLike($table, $where=array(), $wherelike=array(),  $orderby='', $limit=''){
		$qrywhere ="";
		if(!empty($where)){
		  foreach((array)$where as $i => $item){
			$item = mssql_escape($item);
			$qrywhere .= " $i = '$item' AND ";
		  }
		}
		if(!empty($wherelike)){
		  foreach((array)$wherelike as $i => $item){
			$item = mssql_escape($item);
			$qrywhere .= " $i LIKE '%$item%' AND ";
		  }
		}
		if(!empty($orderby)){
		  $orderby = "ORDER BY $orderby";
		}
		if(!empty($limit)){
		  $limit = "LIMIT $limit";
		}
			$sql="
				  SELECT *
				  FROM  $table 
				  WHERE
					$qrywhere
					1 = 1
				  $orderby
				  $limit
		";
 

		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result[] =  $row;
        }
		
		

        return $result;
	}
	
	
	public function AddData($table='', $data=array()){
		if(!empty($data)){
			$attribute_arr = array();
			$values_arr = array();

			foreach($data as $fields => $val){
				$attribute_arr[] = $fields;
				//$values_arr[] ="'".mssql_escape_string($val)."'";
				if($val == 'NULL'){
					$values_arr[] = " NULL ";
				}else{
					$values_arr[] ="'".$val."'";
				}
				
			
			}
			$attribute = implode(',', $attribute_arr);
			$values = implode(',', $values_arr);
			$sql="
					INSERT INTO $table ($attribute)
					VALUES($values);
			"; 

		 	$stmt = $this->consqls->prepare($sql);
		 
	 		//$stmt->execute();
			if($stmt->execute()){
				$result['success'] = 'COMPLETE'; 
				$result['code']  = $this->consqls->lastInsertId();
			}else{
				$result['success'] = 'FAIL';
				$result['sql'] = $sql; 
			}
			 
		}else{
			$result['success'] = 'FAIL NOT FOUND'; 
		} 
		//
		return $result; 
	}

	public function AddDataID($table='', $data=array()){
		if(!empty($data)){
		 $attribute_arr = array();
		 $values_arr = array();
	  
		 foreach($data as $fields => $val){
		  $attribute_arr[] = $fields; 
		  if($val == 'NULL'){
		   $values_arr[] = " NULL ";
		  }else{
		   $values_arr[] ="'".$val."'";
		  }
		 
		 }
		 $attribute = implode(',', $attribute_arr);
		 $values = implode(',', $values_arr);
		 $sql="
		   INSERT INTO $table ($attribute)
		   VALUES($values);
		 "; 
	  
		  $stmt = $this->consqls->prepare($sql);
		 
		  //$stmt->execute();
		 if($stmt->execute()){
		  $result['success'] = 'COMPLETE'; 
		  //$result['code']  = $this->consqls->lastInsertId();
		 }else{
		  $result['success'] = 'FAIL';
		  $result['sql'] = $sql; 
		 }
		  
		}else{
		 $result['success'] = 'FAIL NOT FOUND'; 
		} 
		//
		return $result; 
	   }


	public function AddData2($table='', $data=array()){
		if(!empty($data)){
			$attribute_arr = array();
			$values_arr = array();

			foreach($data as $fields => $val){
				$attribute_arr[] = $fields;
				$values_arr[] ="'".mssql_escape_string($val)."'";
			//	$values_arr[] ="'".$val."'";
			}
			$attribute = implode(',', $attribute_arr);
			$values = implode(',', $values_arr);
			$sql="
					INSERT INTO $table ($attribute)
					VALUES($values);
			"; 

		 	$stmt = $this->consqls->prepare($sql);
		 
	 		$stmt->execute();
			if($stmt->execute()){
				$result['success'] = 'COMPLETE'; 
				$result['code']  = $this->consqls->lastInsertId();
			}else{
				$result['success'] = 'FAIL';
				$result['sql'] = $sql; 
			}
			 
		}else{
			$result['success'] = 'FAIL NOT FOUND'; 
		} 
		
		return $result; 
	}

	public function EditData($table='', $data=array(), $where=array()){
		if(!empty($data)){
			$attribute_arr = array();
			$where_arr = array();

			foreach($data as $fields => $value){
				//$value = mssql_escape_string($value);
				//$attribute_arr[] = " $fields = '".$value."' ";
				if($value == 'NULL'){
					$attribute_arr[] = "  $fields = NULL ";
				}else{
					$attribute_arr[] ="   $fields = '".$value."'   ";
				}
				
			}
			foreach($where as $fields => $value){
				//$value = mssql_escape_string($value);
				$where_arr[] = " $fields = '".$value."' ";
			}
			$attribute = implode(', ', $attribute_arr);
			$whereqry = implode(' AND ', $where_arr);

			$sql="SELECT * FROM $table WHERE $whereqry ";
		 
			$stmt = $this->consqls->prepare($sql);
			$stmt->execute();
			$result = array();
			while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
			{
				$result[] =  $row;
			}
			

			$sql="
			UPDATE $table SET
			  $attribute
			WHERE
			  $whereqry
			
			";

			// echo $sql;
			$result['sql'] = $sql;

			 $stmt = $this->consqls->prepare($sql);
			 $stmt->execute();
			if($stmt->execute()){
				$result['success'] = 'COMPLETE';
			}else{
				$result['success'] = 'FAIL';

			}
		}else{
			$result['success'] = 'FAIL';
			//$this->error[] = 'NOT FOUND DATA';
		}
		//$this->consqls = null; 
		return $result;
	}

	public function DeleteData($table='', $where=array()){
		if(!empty($where)){
			  $where_arr = array();

			  foreach($where as $fields => $value){
				// $value = mysqli_real_escape_string($this->mysqli,$value);
				// $where_arr[] = " `$fields` = '".mysqli_real_escape_string($this->mysqli,$value)."' ";
			
				$where_arr[] = " $fields  = '".$value."' ";
			  }
			  $whereqry = implode(' AND ', $where_arr);


			  $sql="
				DELETE FROM
				  $table
				WHERE
				  $whereqry
			  ";
			  $result['sql'] = $sql;
			  $stmt = $this->consqls->prepare($sql);
			  $stmt->execute();
			  if(  $stmt->execute()  ){
				$result['success'] = 'COMPLETE';
			  }else{
				$result['success'] = 'FAIL';
			  }

		}else{
			$result['success'] = 'FAIL';

		}

		return $result;
	}

	public function LoadCount($table, $where=array()){
		$qrywhere ="";
		if(!empty($where)){
		  foreach((array)$where as $i => $item){
			//$item = mssql_escape_string($item);
			$qrywhere .= "  $i  = '$item' AND ";
		  }
		}
		 
			$sql="
		  SELECT
			count(*) as cnt
		  FROM
			$table
		  WHERE
			$qrywhere
			1=1

		";

		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
		$result = array();
		while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
		{
			$result  =  $row;
		}
	
		$this->consqls = null;

		return $result;
	}

	public function LoadSum($table, $where=array() , $field){
		$qrywhere  = "";
		if(!empty($where)){
		  foreach((array)$where as $i => $item){
			//$item = mysqli_real_escape_string($this->mysqli,$item);
			$item =    $item ;
			$qrywhere .= " `$i` = '$item' AND ";
		  }
		}
		if(!empty($orderby)){
		  $orderby = "ORDER BY $orderby";
		}
			$sql="
		  SELECT
			 SUM($field) AS cnt_sum
		  FROM
			$table
		  WHERE
			$qrywhere
			1

		";
		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result[] =  $row;
        }
	 

		return $result;
	}
	
	public function LoadCountNot($table, $where=array() ,$wherenot=array()){
		$qrywhere ="";
		$qrywherenot ="";
		
		if(!empty($where)){
		  foreach((array)$where as $i => $item){
			//$item = mysqli_real_escape_string($this->mysqli,$item);
			$item =   $item ;
			$qrywhere .= " `$i` = '$item' AND ";
		  }
		}
		if(!empty($wherenot)){
		  foreach((array)$wherenot as $in => $itemn){
			//$item = mysqli_real_escape_string($this->mysqli,$itemn);
			$item =   $itemn ;
			$qrywherenot .= "NOT `$in` = '$itemn' AND ";
		  }
		}
 
			$sql="
		  SELECT
			count(*) as cnt
		  FROM
			`$table`
		  WHERE
			$qrywherenot
			$qrywhere
			1
		";

		$stmt = $this->consqls->prepare($sql);
		$stmt->execute(); 
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result  =  $row;
        }
	 

		return $result;
	}
	
	public function LoadDistByField($table,$field, $where=array()){

		$qrywhere = ""; 
		if(!empty($where)){
		  foreach((array)$where as $i => $item){
			//$item = mysqli_real_escape_string($this->mysqli,$item);
			$item =   $item ;
			$qrywhere .= " $i = '$item' AND ";
		  }
		}
	 
		$sql=" 
			SELECT
				DISTINCT($field) AS field  
			FROM
					`$table`
			  WHERE
			$qrywhere
			1

		";
	
		$stmt = $this->consqls->prepare($sql);
		$stmt->execute(); 
		$result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result[]  =  $row;
        }

		return $result;
	}
	
	public function LoadCountDistByField($table,$field, $where=array() , $wherenot=array()){
		$qrywhere  ="";
		if(!empty($where)){
		  foreach((array)$where as $i => $item){

			  if($item || $item ==0 ){
				//$item = mysqli_real_escape_string($this->mysqli,$item);
				$item =    $item ;
				$qrywhere .= "  $i  = '$item' AND ";
			  }
		  }
		}
		$qrywherenot  ="";
		if(!empty($wherenot)){
		  foreach((array)$wherenot as $i => $item){

			  if($item || $item ==0 ){ 
				$item =    $item ;
				$qrywherenot .= "  $i  <> '$item' AND ";
			  }
		  }
		}
		 
		 
		$sql=" 
			SELECT
				COUNT(DISTINCT($field)) AS cnt
			FROM
					 $table 
			  WHERE
			$qrywhere
			$qrywherenot
			1=1 

		";
		 //$result['sql'] = $sql;
	 
			$stmt = $this->consqls->prepare($sql);
			$stmt->execute();  
			while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
			{
				$result  =  $row;
			}
	

		return $result;
	}

	function LoadLikeTitle($table, $where=array(), $wherelike=array(), $orderby='', $limit=''){
		$qrywhere = array();
		if(!empty($where)){
			foreach((array)$where as $i => $item){ 
			  //$item = mysqli_real_escape_string($this->mysqli,$item);
			  $qrywhere .= " $i  = '$item' AND ";
			}
		  }
		if(!empty($wherelike)){
			foreach((array)$wherelike as $i => $item){
				$item = mysqli_real_escape_string($this->mysqli,$item);
				$qrywhere .= " ( `$i`  LIKE '_$item%'  AND Left($i,1) in ('เ','โ','ไ','ใ') ) OR (  `$i`  LIKE '$item%' )  AND "; 
			  }
			}
		 
		 
		if(!empty($orderby)){
		  $orderby = "ORDER BY $orderby";
		}
		if(!empty($limit)){
		  $limit = "LIMIT $limit";
		}
			$sql="
				  SELECT *
				  FROM `$table`
				  WHERE
					$qrywhere
					1 = 1
				  $orderby
				  $limit
		";
	 
		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
		$result = array();  
		while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
		{
			$result[]  =  $row;
		}

		return $result;
	}

	public function LoadFieldsMaxID($table, $fields , $where=array()){
		 
			$sql="
			SELECT MAX($fields) AS lastid
			FROM $table
			 
		";
		  
 
		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result=  $row;
        }
		
		

        return $result;
	} 

	public function LoadIdenCurrent($table ){
		 
		$sql="
		SELECT IDENT_CURRENT('$table')  AS lastid
		 
		"; 

		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
		$result = array();
		while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
		{
			$result=  $row;
		}
		
		return $result;
	} 

	

}



function mssql_escape($data) {
    if(is_numeric($data))
        return $data;
    $unpacked = unpack('H*hex', $data);
    return '0x' . $unpacked['hex'];
}

function mssql_escape_string($data) {
	if ( !isset($data) or empty($data) ) { 
		return '';
	}
	
	if(is_numeric($data)){
		return $data;
	} 

	$non_displayables = array(
		'/%0[0-8bcef]/', // url encoded 00-08, 11, 12, 14, 15
		'/%1[0-9a-f]/', // url encoded 16-31
		'/[\x00-\x08]/',	// 00-08
		'/\x0b/',	// 11
		'/\x0c/', 	// 12
		 '/[\x0e-\x1f]/' // 14-31
	);

	foreach ( $non_displayables as $regex ){
		$data = preg_replace( $regex, '', $data );
	}
	$data = str_replace("'", "''", $data );
	return $data;
}


/*

$clsDuck = new DuckClass();
 
//$dataRank = $clsDuck->LoadOnce('HrtRank',array('HrtRankID'=>'10211'));
$dataRank = $clsDuck->Load('HrtRank',array(),'','');


if($dataRank){
    echo "<pre>".print_r( $dataRank ,true)."</pre>";   
}else{
    echo "No data";   
}
*/
 

?>
