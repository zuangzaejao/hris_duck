<?php
header('Access-Control-Allow-Origin: *');
header("Content-type: text/html; charset=utf-8");

class HrtClass extends DuckClass{
	////////////////////////////////////////////////////////////////////////////////////////////
	 

	public function LoadChiefSpc(  $where ){
		$qrywhere ="";
		if(!empty($where)){
		  foreach((array)$where as $i => $item){
			$item = mssql_escape($item);
			$qrywhere .= " $i = '$item' AND ";
		  }
		}
	 
			$sql="
			      Select HrtChiefSpc.HrtChiefSpcID,HrtPersonTypeName,HrtChiefSpcName,HrtChiefSpcNameAbbr,HrtPersonType.HrtPersonTypeID,HrtChiefSpcActive,HrtChiefSpcDelete
			      from HrtChiefSpc 
			      left join HrtPersonType 
			      on HrtChiefSpc.HrtPersonTypeID = HrtPersonType.HrtPersonTypeID
				" ;
				 

		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result[] =  $row;
        }
		
		

        return $result;
			

	}
	/////////////////////////////////////////////////////////////////////////////////
	public function LoadSpc(  $where ){
		$qrywhere ="";
		if(!empty($where)){
		  foreach((array)$where as $i => $item){
			$item = mssql_escape($item);
			$qrywhere .= " $i = '$item' AND ";
		  }
		}
	 
			$sql="
			Select	
			HA.ArmCode,HA.ArmName,
			HC.ChiefSpcID,HC.ChiefSpcName,
			HPT.HrtPersonTypeID,HPT.HrtPersonTypeName,
			HS.ArmCode,HS.ChiefSpcID,HS.PersonTypeID,HS.SpcID,
			HS.SpcName,HS.SpcAbbrName,HS.SpcActive,HS.SpcDelete,
			HS.SpcTypeCreateBy,HS.SpcTypeCreateDate,HS.SpcTypeUpdateBy,HS.SpcTypeUpdateDate

			from HrtSpc as HS 
			left join HrtChiefSpc as HC on HS.ChiefSpcID=HC.ChiefSpcID AND HS.PersonTypeID=HC.PersonTypeID 
			left join HrtPersonType as HPT on HS.PersonTypeID = HPT.HrtPersonTypeID
			left join HrtArm as HA on HS.ArmCode = HA.ArmCode	
			WHERE 
			$qrywhere 
			1=1 
			 Order by SpcID 
				" ;
				 

		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result[] =  $row;
        }
		
		

        return $result;
			

	}
	/////////////////////////////////////////////////////////////////////////////////
	public function LoadArm(  $where ){
		$qrywhere ="";
		if(!empty($where)){
		  foreach((array)$where as $i => $item){
			$item = mssql_escape($item);
			$qrywhere .= " $i = '$item' AND ";
		  }
		}
	 
			$sql="
				Select 
				SkillNo,ArmCode,HrtArmName,SpcID,HrtSpcName,SkillName,SkillAbbrName,SkillActive,SkillDelete,CreateUserID,CreateDate,UpdateUserID,UpdateDate
				from HrtSkill 
				inner join HrtSpc 
				on HrtSkill.SpcID = HrtSpc.HrtSpcID
				inner join HrtArm
				on HrtSkill.ArmCode =HrtArm.HrtArmCode
			 
				" ;
				 

		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result[] =  $row;
        }
		
		
		
        return $result;
			

	}
	/////////////////////////////////////////////////////////////////////////////////
	public function LoadSkillSuffix(  $where ){
		$qrywhere ="";
		if(!empty($where)){
		  foreach((array)$where as $i => $item){
			$item = mssql_escape($item);
			$qrywhere .= " $i = '$item' AND ";
		  }
		}
	 
			$sql="
					Select HrtSkillSuffix.HrtSkillSuffixID, HrtSkillSuffixName,HrtSkillSuffixFullName,HrtPersonType.HrtPersonTypeID,HrtPersonTypeName,HrtSkillSuffixActive,HrtSkillSuffixDelete
					from HrtSkillSuffix
					left join HrtPersonType 
					on HrtSkillSuffix.HrtPersonTypeID = HrtPersonType.HrtPersonTypeID
				" ;
				 

		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result[] =  $row;
        }
		
		

        return $result;
			

	}
	/////////////////////////////////////////////////////////////////////////////////
	public function LoadSkillPrefix(  $where ){
		$qrywhere ="";
		if(!empty($where)){
		  foreach((array)$where as $i => $item){
			$item = mssql_escape($item);
			$qrywhere .= " $i = '$item' AND ";
		  }
		}
	 
			$sql="
					Select HrtSkillPrefix.HrtSkillPrefixID, HrtSkillPrefixName,HrtSkillPrefixFullName,HrtPersonType.HrtPersonTypeID,HrtPersonTypeName,HrtSkillPrefixActive,HrtSkillPrefixDelete
					from HrtSkillPrefix
					left join HrtPersonType 
					on HrtSkillPrefix.HrtPersonTypeID = HrtPersonType.HrtPersonTypeID
			
				" ;
				 

		$stmt = $this->consqls->prepare($sql);
		$stmt->execute();
        $result = array();
        while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
        {
            $result[] =  $row;
        }
		
		

        return $result;
			

	}

	////////////////////////////////////////////////////////////////////////////////////////////
	
	/////////////////////////////////////////////////////////////////////////////////
		public function LoadAmphur(  $where ){
			$qrywhere ="";
			if(!empty($where)){
			  foreach((array)$where as $i => $item){
				$item = mssql_escape($item);
				$qrywhere .= " $i = '$item' AND ";
			  }
			}
		 
				$sql="
						Select HrtAmphurID,HrtAmphurCode,HrtAmphurName,HrtAmphurAbbrName,HA.HrtGeoId,HA.HrtProvinceID,HP.HrtProvinceName,HrtAmphurActive,HrtAmphurDelete
						From HrtAmphur AS HA
						Left join HrtProvince As HP
						on HA.HrtProvinceId = HP.HrtProvinceID
				
					" ;
					 
	
			$stmt = $this->consqls->prepare($sql);
			$stmt->execute();
			$result = array();
			while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
			{
				$result[] =  $row;
			}
			
			
	
			return $result;
				
	
		}

////////////////////////////////////////////////////////////////////////////
		public function LoadDataSkill(  $where ){
			$qrywhere ="";
			if(!empty($where)){
			  foreach((array)$where as $i => $item){
				$item = mssql_escape($item);
				$qrywhere .= " HrtSkill.$i = '$item' AND ";
			  }
			}
		 
				$sql="
					
					Select 
					SkillNo,ArmCode,HrtArmName,SpcID,HrtSpcName,SkillName,SkillAbbrName,SkillActive,SkillDelete,CreateUserID,CreateDate,UpdateUserID,UpdateDate
					from HrtSkill 
					inner join HrtSpc 
					on HrtSkill.SpcID = HrtSpc.HrtSpcID
					inner join HrtArm
					on HrtSkill.ArmCode =HrtArm.HrtArmCode
						WHERE 
						$qrywhere 
						1=1 
				
					" ;
					 
	
			$stmt = $this->consqls->prepare($sql);
			$stmt->execute();
			$result = array();
			while($row = $stmt->fetch( PDO::FETCH_ASSOC ))
			{
				$result[] =  $row;
			}
			
			
	
			return $result;
				
	
		}
		////////////////////////////////////////////////////////////////////////////////////////////
		// public function checkmax(){
		
	
		// 	return $result;
				
	
		// }
	}

?>
