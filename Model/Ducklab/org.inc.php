<?php
header('Access-Control-Allow-Origin: *');
header("Content-type: text/html; charset=utf-8");

require_once "duck.class.php";
require_once "func.php";
require_once "org.class.php";


function AddDataSet(){
	$clsorg = new OrgClass();

	$table  = $_POST['table'];
	$data  	= $_POST['data'];

	$result = $clsorg->AddData($table,$data);

	echo json_encode($result);
}

function AddDataID(){
	$clsorg = new OrgClass();

	$table  = $_POST['table'];
	$data  	= $_POST['data'];

	$result = $clsorg->AddDataID($table,$data);

	echo json_encode($result);
}



function EditDataSet(){
	$clsorg = new OrgClass();

	$table  = $_POST['table'];
	$data  	= $_POST['data'];
	$where  = $_POST['where'];

	$result = $clsorg->EditData($table,$data,$where);

	echo json_encode($result);
}

function DeleteDataSet(){
	$clsorg = new OrgClass();

	$table  = $_POST['table'];
	$where  = $_POST['where'];

	$result = $clsorg->DeleteData($table,$where);

	echo json_encode($result);

}
 
function EditDataContents(){

		$clsorg = new OrgClass();

		$table  = $_POST['table'];
		$data  	= $_POST['data'];
		$data_content  	= $_POST['data_content'];
		$where  = $_POST['where'];

		foreach($data_content as $key => $value ){
			$data_informations = ContentFormat( $value );
			$data[$key] = $data_informations ;
		}

		$result = $clsorg->EditData($table,$data,$where);
		echo json_encode($result);
}

function AddDataContents(){

		$clsorg = new OrgClass();

		$table  = $_POST['table'];
		$data  	= $_POST['data'];
		$data_content  	= $_POST['data_content'];

		if($data_content){
			foreach($data_content as $key => $value ){
				$data_informations = ContentFormat( $value );
				$data[$key] = $data_informations ;
			}
		}

		$result = $clsorg->AddData($table,$data,$where);
		echo json_encode($result);
}

function LoadAllData(){
	$clsorg = new OrgClass();

	$table = $_POST['table'];
	$where = $_POST['where'];
	$orderby = $_POST['orderby'];
	$limit = $_POST['limit'];

	$result=$clsorg->Load($table, $where , $orderby,$limit);
	echo json_encode($result);
} 

function LoadFieldData(){
	$clsorg = new OrgClass();

	$table = $_POST['table'];
	$where = $_POST['where'];
	$field = $_POST['field'];
	$orderby = $_POST['orderby'];
	$limit = $_POST['limit'];

	$result=$clsorg->LoadFields($table, $field , $where , $orderby,$limit);
	echo json_encode($result);
}

function LoadOneRow(){
	$clsorg = new OrgClass();

	$table = $_POST['table'];
	$where = $_POST['where'];


	$result=$clsorg->LoadOnce($table, $where  );
	echo json_encode($result);
}

function LoadLikeTitle(){
	$clsorg = new OrgClass();

	$table = $_POST['table'];
	$where = $_POST['where'];
	$wherelike = $_POST['wherelike'];
	$orderby = $_POST['orderby'];
	$limit = $_POST['limit'];

	$result=$clsorg->LoadLikeTitle($table, $where , $wherelike, $orderby,$limit );
	echo json_encode($result);
}

function StatusDataSetEX(){
	$clsorg = new OrgClass();

	$table 		= $_POST['table'];
	$data  		= $_POST['data'];
	$data_name  = $_POST['data_name'];

	$status_n = $data_name['status_n'] ;
	$id_n = $data_name['id_n'] ;

	$data_new[''.$status_n.''] = $data['status'];
	$where_new[''.$id_n.''] = $data['id'];


	$result = $clsorg->EditData($table,$data_new,$where_new);

	echo json_encode($result);
}
////////////////////////////////////////////////////////////////////////////////////////////

function LoadOrgStrucByOrgType(){
	$clsorg = new OrgClass();
	$OrgTypeID = $_POST['OrgTypeID'];
	$orgParentDID = '';

	$result=$clsorg->LoadOrgStrucByOrgType( $OrgTypeID  , $orgParentDID );

	echo json_encode($result);
}


function LoadDataOrgLev(){
	$clsorg = new OrgClass();

	$orgLevDID = $_POST['orgLevDID'];
	$orgLevCode = $_POST['orgLevCode']; 
	$orgLev = $_POST['orgLev']; 

	$table= "OrgLevel".$orgLev ; 
	$where  = array( 'OrgLevDID' =>$orgLevDID );
 

	$result['dept']=$clsorg->LoadOnce($table, $where);

	 	//$result['orgLev'] =  $orgLev;

	 /*
	for( $x=$orgLev ; $x>0 ; $x-- ){

		$x = (int)$x ;
		$tableP = 'OrgLevel'.$x.'Parent' ;   //$orgLevDID
		$tableO = 'OrgLevel'.$x ; 

		 $result['unitM'][$x]['OrgLevelDID'] =  $orgLevDID ;   
		 $result['unitM'][$x]['tableP'] =  $tableP ; 
		// $result[$x]['detail'] = $clsorg->LoadOnce($tableP, array('OrgLevelDID'=>$orgLevDID ));
		$result['unitM'][$x]['detail'] = $clsorg->LoadOnce($tableP, array('OrgLevDID'=>$orgLevDID ));
		//$result[$x]['detail']['OrgLevID']
		// $result[$x]['detail']['OrgLevID'] ; 

		if($result['unitM'][$x]['detail'] ){
			$result['unitM'][$x]['detail']['unit'] = $clsorg->LoadOnce($tableO, array('OrgLevID'=>$result[$x]['detail']['OrgLevID'] ));
		}
	}*/

	$result['dept']['prov']  ="";
	//OrgLevDID
	$result['dept']['level']=$clsorg->LoadOnce('OrgLevel', array('OrgLevelID'=>$result['dept']['OrgLev'] ));
	if( $result['dept']['LocCode']  != null  ){
		$LocCode = substr($result['dept']['LocCode'] ,0,2)  ; 
		$prov = $clsorg->LoadOnce('HrtProvince', array('HrtProvinceCode'=> $LocCode )); 
	}
	
	$result['dept']['prov'] = $prov['HrtProvinceName'] ;
	$result['dept']['provcode'] = $LocCode;
	
	//$tablePr1 = 'OrgLevel'.($orgLev-1);
	// $wherep = array(); 
	// //$result['dept']['deptp'] ; =$clsorg->LoadOnce($table."Parent", $where);
	


	// $result=$clsorg->LoadOnce($table, array( ''=>$result['dept']));
	echo json_encode($result);
} 

//LoadOrgByParentCode


function LoadOrgByParentCode(){
	$clsorg = new OrgClass();
	/*
	$table = $_POST['table'];
	$where = $_POST['where'];
	$orderby = $_POST['orderby'];
	$limit = $_POST['limit'];

	$result = $clsorg->Load($table,  array('OrgParentCode'=> $OrgParentCode ) , $orderby,$limit);
	*/

	$OrgCodeParent = $_POST['OrgCodeParent'];
	$OrgLevelDID = $_POST['OrgLevelDID'];
	$tablename = $_POST['tablename'];
	// $OrgCodeParent = '00000000' ; //$_POST['OrgCodeParent'];
	// $OrgLevelDID = $_POST['OrgLevelDID'];
	// $tablename = 'OrgLevel1' ;

	$result = $clsorg->LoadLevelByParent(  $OrgCodeParent ,$OrgLevelDID , $tablename  )  ; 
 
	echo json_encode($result);
} 

 
/*
function CreateOrgData(){
	$clsorg = new OrgClass();

	$table  = $_POST['table'];
	$data  	= $_POST['data'];
	$orgParentCode = $_POST['orgParentCode'];
	$levelnow = $_POST['levelnow'];
	$tablePar = $table."Parent";



	//$clsorg = new OrgClass();
	$result['lastid'] = $clsorg->LoadFieldsMaxID($table,'OrgLevID' , ''); 
	$lastid = $result['lastid']['lastid']+1 ;
	$data['OrgLevID'] = $lastid ;
	
	$data['OrgLevDID']  ='0'.$levelnow.''.$lastid  ;
	//$clsorg = new OrgClass();
	$result['dataFromAjax'] = $data ;
	$result['data'] = $clsorg->AddData($table,$data);
	$datacodePK =  $lastid ;
	$datacodePKDID = "0".$levelnow.$datacodePK ;

	//$result['data'] code id ID
	//$clsorg = new OrgClass();
	//$result['updatedelete'] = $clsorg->EditData($table,array('OrgLevID'=> $lastid),array('OrgLevDelete'=>'0'));

	 
	//$levelnow
	//$clsorg = new OrgClass();
	$result['lastidp'] = $clsorg->LoadFieldsMaxID($tablePar,'OrgLevParent_ID' , '');
	$OrgLevParent_ID = $result['lastidp']['lastid']+1 ;
	
	$dataParent = array('OrgLevParent_ID'=> $OrgLevParent_ID , 'OrgParentCode'=> $orgParentCode ,'OrgLevID'=> $datacodePK  , 'OrgLevDID'=>$datacodePKDID , 'OrgLevel'=>$levelnow  );
	//$clsorg = new OrgClass();
	$result['dataParent'] = $clsorg->AddData($tablePar,$dataParent);
	 
 
	echo json_encode($result);
}
*/

function utf8_strlen($s) {
	$c = strlen($s); $l = 0;
	for ($i = 0; $i < $c; ++$i)
	if ((ord($s[$i]) & 0xC0) != 0x80) ++$l;
	return $l;
	}

function CreateOrg0Data(){
 
	$clsorg = new OrgClass();

	$table  = $_POST['table'];
	$data  	= $_POST['data'];
  

	//$clsorg = new OrgClass();
	$result['lastid'] = $clsorg->LoadFieldsMaxID('OrgLevel0','OrgLevID' , ''); 
	$lastid = $result['lastid']['lastid']+1 ;
	$data['OrgLevID'] = $lastid ;
	$lastcode ="";
	$numstr = utf8_strlen($lastid) ;
	for($i=$numstr;$i<6;$i++){
		$lastcode .="0" ; 
	}
	
	$data['OrgLevDID']  ='00'.$lastcode.''.$lastid  ;
 
	//$result['dataFromAjax'] = $data ;
	$result = $clsorg->AddData($table,$data);
	echo json_encode($result); 
} 

function CreateOrgData(){
	$clsorg = new OrgClass();

	$table  = $_POST['table'];
	$data  	= $_POST['data'];
	$orgParentCode = $_POST['orgParentCode'];
	$levelnow = $_POST['levelnow'];
	$tablePar = $table."Parent";



	//$clsorg = new OrgClass();
	$result['lastid'] = $clsorg->LoadFieldsMaxID($table,'OrgLevID' , ''); 
	$lastid = $result['lastid']['lastid']+1 ;
	$data['OrgLevID'] = $lastid ;
	$lastcode ="";
	$numstr = utf8_strlen($lastid) ;
	for($i=$numstr;$i<6;$i++){
		$lastcode .="0" ; 
	}
	
	$data['OrgLevDID']  ='0'.$levelnow.$lastcode.''.$lastid  ;
 
	$result['dataFromAjax'] = $data ;
	$result['data'] = $clsorg->AddData($table,$data);
	$datacodePK =  $lastid ;
	$datacodePKDID = "0".$levelnow.$datacodePK ;

	 
	$result['lastidp'] = $clsorg->LoadFieldsMaxID($tablePar,'OrgLevParent_ID' , '');
	$OrgLevParent_ID = $result['lastidp']['lastid']+1 ;
	
	//$dataParent = array('OrgLevParent_ID'=> $OrgLevParent_ID , 'OrgParentCode'=> $orgParentCode ,'OrgLevID'=> $datacodePK  , 'OrgLevDID'=>$datacodePKDID , 'OrgLevel'=>$levelnow  );
	$dataParent = array('OrgParentCode'=> $orgParentCode ,'OrgLevID'=> $datacodePK  , 'OrgLevDID'=>$datacodePKDID , 'OrgLevel'=>$levelnow  );
 
	$result['dataParent'] = $clsorg->AddData($tablePar,$dataParent);
	 
 
	echo json_encode($result);
}

function EditOrgData(){
	$clsorg = new OrgClass();

	$table  = $_POST['table'];
	$data  	= $_POST['data']; 
	$OrgLevDID  	= $_POST['OrgLevDID']; 

	$result['dataFromAjax'] = $data ;
	$result['data'] = $clsorg->EditData($table,$data,array('OrgLevDID'=>$OrgLevDID));

 
	 
 
	echo json_encode($result);
}

function LoadLevel1ByOrgType(){
	$clsorg = new OrgClass();
 
	$OrgType  	= $_POST['OrgType'];
	$result = $clsorg->LoadLevel1ByOrgType($OrgType); 
	echo json_encode($result); 
}

function LoadLevel2ByOrgType(){
	$clsorg = new OrgClass();
 
	$OrgType  	= $_POST['OrgType'];
	$result = $clsorg->LoadLevel2ByOrgType($OrgType); 
	echo json_encode($result); 
}

function LoadPositionByOrgLevelx(){
	$clsorg = new OrgClass();

	$OrgLevelDID = $_POST['OrgLevelDID'];
  
	$result=$clsorg->LoadPositionByOrgLevelx($OrgLevelDID);

	echo json_encode($result);
} 

function LoadPosPersonByOrgLevelx(){
	$clsorg = new OrgClass();

	$OrgLevelDID = $_POST['OrgLevelDID'];
	$position = array();
	$result = array();
	$result_a = $clsorg->LoadPositionByOrgLevelx($OrgLevelDID);
	$position = $result_a ;
	if($position){
		foreach($position as $k1 => $v1 ){
			$result[$k1] = $position[$k1];
			$result[$k1]['posperson'] = $clsorg->LoadPersonByPositionNo($v1['HrtPositionID'],'');
		}
	}
	echo json_encode($result);
} 


function AddPositionDataSet(){
	$clsorg = new OrgClass();

	$table  = $_POST['table'];
	$data  	= $_POST['data'];
	$table2  = 'HrtPosPerson';
	$data2	= $_POST['data_p'];
	$posnum =  $_POST['posnum'];
	$result = $clsorg->AddData($table,$data);

	$PositionID = $result['code'] ; 
	//PositionID
	if($posnum>0){
		for( $i=1 ;$i<=$posnum ; $i++){
		//	$data['PositionID'] = $PositionID ;
			$data2['PosNo'] = $i ;
			$data2['PosNumber'] = $i ;
			$result['Posperson'] = $clsorg->AddData($table2,$data2);
		}
	}
	

	echo json_encode($result);
}

function UpdatePositionDataSet(){
	$clsorg = new OrgClass();

	$table  = $_POST['table'];
	$data  	= $_POST['data'];
	$table2  = 'HrtPosPerson';
	$data2	= $_POST['data_p'];
	$posnum =  $_POST['posnum'];
	$where =  $_POST['where'];

	$result = $clsorg->EditData($table, $data ,$where );
	$PositionID = $where['HrtPositionID'] ; 
	//PositionID 
	if($posnum>0){
		for( $i=1 ;$i<=$posnum ; $i++){
		 	$data['PositionID'] = $PositionID ;
			$data2['PosNo'] = $i ;
			$data2['PosNumber'] = $i ;
			//$result['Posperson'] = $clsorg->EditData($table2,$data2);
		}
	}
	
	echo json_encode($result);
}

function LoadOrgforCreate(){
	
	 
	$clsorg = new OrgClass(); 
	$orgLevDIDParent  = $_POST['orgLevDIDParent'];

	$parentLevel = substr($orgLevDIDParent,1,1);
	
	if($parentLevel==0){
		$nextLevel = 1;
	}
	$nextLevel += $parentLevel+1 ;

	$result['lastid'] = $clsorg->LoadFieldsMaxID('OrgLevel'.$nextLevel,'OrgLevID' , ''); 
	$lastid = $result['lastid']['lastid']+1 ;
	$data['OrgLevID'] = $lastid ;
	$lastcode ="";
	$numstr = utf8_strlen($lastid) ;
	for($i=$numstr;$i<6;$i++){
		$lastcode .="0" ; 
	}
	
	$data['OrgLevDID']  ='0'.$nextLevel.$lastcode.''.$lastid  ;
	$data['nextLevel'] = $nextLevel;
	echo json_encode($data);


}


function LoadCountDistByField(){
	
	 
	$clsorg = new OrgClass(); 
	$table  = $_POST['table'];
	$field  = $_POST['field'];
	$where  = $_POST['where'];
	$wherenot  = $_POST['wherenot'];
  
	$result = $clsorg->LoadCountDistByField($table,$field, $where ,$wherenot); 
 
	echo json_encode($result);


}

function LoadRankSort(){
	
	 
	$clsorg = new OrgClass();  
	 $ptype  = $_POST['PersonType'];
	$result = $clsorg->LoadRankSort( $ptype); 
 
	echo json_encode($result);


}


function LoadDataOrgParent(){
	$clsorg = new OrgClass();  
	$orgLev  = $_POST['orgLev'];
	$orgLevDID  = $_POST['orgLevDID'];

	$nowOrg =  $orgLevDID ;
	for( $x=$orgLev ; $x>1 ; $x-- ){

		

		$x = (int)$x ;
		$y = ( (int)$x)-1 ;
		$tableP = 'OrgLevel'.$x.'Parent' ;   //$orgLevDID
		$tableO = 'OrgLevel'.$x ; 
		$table1 = 'OrgLevel'.$y ; 

		 $result['unitM'][$x]['OrgLevelDID'] =  $nowOrg ;   
		 $result['unitM'][$x]['tableP'] =  $tableP ; 
 
		$result['unitM'][$x]['detail'] = $clsorg->LoadOnce($tableO, array('OrgLevDID'=>$nowOrg ));
		$result['unitM'][$x]['parent'] = $clsorg->LoadOnce($tableP, array('OrgLevDID'=>$nowOrg ));
	 

		if($result['unitM'][$x]['parent'] ){ 
			$nowOrg = $result['unitM'][$x]['parent']['OrgParentCode'];
			$result['unitM'][$x]['parent']['unit'] = $clsorg->LoadOnce($table1, array('OrgLevDID'=> $nowOrg ));
		}
	}

	echo json_encode($result);
}

function LoadPosStrucAddAmt(){
	
	 
	$clsorg = new OrgClass();  
	$PositionID  = $_POST['PositionID'];
	$AddAmtCode  = $_POST['AddAmtCode'];
	$result = $clsorg->LoadPosStrucAddAmt( $PositionID, $AddAmtCode ); 
 
	echo json_encode($result);


}

function LoadPositionDetail(){
	$clsorg = new OrgClass();  
	$PositionID  = $_POST['PositionID']; 
	$result = $clsorg->LoadPositionDetail( $PositionID ); 
	
	
	$OrgLevelDID = $result['OrgLevelDID'] ;
	$result['OrgData'] =$clsorg->LoadDataOrgParent($OrgLevelDID);


	echo json_encode($result);
}

function LoadDataOrgSel(){
	$clsorg = new OrgClass();  
	$OrgTypeID  = $_POST['OrgTypeID'];
	$OrgLevel2DID  = $_POST['OrgLevel2DID'];

	 
	
	$result['lev2parent'] = $clsorg->LoadOnce( 'OrgLevel2Parent' , array('OrgLevDID'=>$OrgLevel2DID ));
	$result['lev2'] = $clsorg->LoadOnce( 'OrgLevel2' , array('OrgLevDID'=>$OrgLevel2DID ));
	//$OrgSub =$clsorg->Load('OrgLevel2Parent', array('OrgLevDID'=>$OrgLevel2DID  ) ;
	//$result['lev2'] = $clsorg->LoadLevel2ByParent(  $result['lev2parent']['OrgParentCode'] ,$OrgTypeID  );
	$x=0;
	$result['lev3'] = $clsorg->LoadLevel3ByParent(  $OrgLevel2DID ,$OrgTypeID  );
	if($result['lev3']){
		foreach( $result['lev3']  as $k3 => $v3 ){
			$result['orgdetail'][$x]['orgname'] = ''.$v3['OrgLevAbbrName'] .$result['lev2']['OrgLevAbbrName']; 
			$result['orgdetail'][$x]['orgdid'] = $v3['OrgLevDID'];
			$result['lev3'][$k3]['OrgName'] = ''.$v3['OrgLevAbbrName'] .$result['lev2']['OrgLevAbbrName']; 
			$result['lev4'] = $clsorg->LoadLevel4ByParent(  $v3['OrgLevDID'] ,$OrgTypeID  );
			if($result['lev4']){
				foreach( $result['lev4']  as $k4 => $v4 ){
					$result['orgdetail'][$x]['orgname'] =  $v4['OrgLevAbbrName'] .$v3['OrgLevAbbrName'] .$result['lev2']['OrgLevAbbrName']; 
					$result['orgdetail'][$x]['orgdid'] = $v4['OrgLevDID'];
					$result['lev4'][$k4]['OrgName'] = $v4['OrgLevAbbrName'] .$v3['OrgLevAbbrName'] .$result['lev2']['OrgLevAbbrName']; 
					$result['lev5'] = $clsorg->LoadLevel5ByParent(  $v4['OrgLevDID'] ,$OrgTypeID  );
					if($result['lev5']){
						foreach( $result['lev5']  as $k5 => $v5 ){
							$result['orgdetail'][$x]['orgname'] =  $v5['OrgLevAbbrName'] .$v4['OrgLevAbbrName'] .$v3['OrgLevAbbrName'] .$result['lev2']['OrgLevAbbrName']; 
							$result['orgdetail'][$x]['orgdid'] = $v5['OrgLevDID'];
							$result['lev5'][$k5]['OrgName'] = $v5['OrgLevAbbrName'] .$v4['OrgLevAbbrName'] .$v3['OrgLevAbbrName'].$result['lev2']['OrgLevAbbrName']; 
							$result['lev6'] = $clsorg->LoadLevel6ByParent(  $v5['OrgLevDID'] ,$OrgTypeID  );
							if($result['lev6']){
								foreach( $result['lev6']  as $k6 => $v6 ){
									$result['orgdetail'][$x]['orgname'] =  $v6['OrgLevAbbrName']. $v5['OrgLevAbbrName']  .$v4['OrgLevAbbrName'] .$v3['OrgLevAbbrName'].$result['lev2']['OrgLevAbbrName'];
									$result['orgdetail'][$x]['orgdid'] = $v6['OrgLevDID'];
									$result['lev6'][$k6]['OrgName'] = $v6['OrgLevAbbrName']. $v5['OrgLevAbbrName']  .$v4['OrgLevAbbrName'] .$v3['OrgLevAbbrName'].$result['lev2']['OrgLevAbbrName']; 
									$result['lev7'] = $clsorg->LoadLevel7ByParent(  $v6['OrgLevDID'] ,$OrgTypeID  );
									if($result['lev7']){
										foreach( $result['lev7']  as $k7 => $v7 ){
											$result['orgdetail'][$x]['orgname'] = $v7['OrgLevAbbrName'].$v6['OrgLevAbbrName']. $v5['OrgLevAbbrName']  .$v4['OrgLevAbbrName'] .$v3['OrgLevAbbrName'].$result['lev2']['OrgLevAbbrName']; 
											$result['orgdetail'][$x]['orgdid'] = $v7['OrgLevDID'];
											$result['lev7'][$k7]['OrgName'] = $v7['OrgLevAbbrName'].$v6['OrgLevAbbrName']. $v5['OrgLevAbbrName']  .$v4['OrgLevAbbrName'] .$v3['OrgLevAbbrName'].$result['lev2']['OrgLevAbbrName']; 
											$result['lev8'] = $clsorg->LoadLevel8ByParent(  $v7['OrgLevDID'] ,$OrgTypeID  );
											if($result['lev8']){
												foreach( $result['lev8']  as $k8 => $v8 ){
													$result['orgdetail'][$x]['orgname'] = $v8['OrgLevAbbrName'].$v7['OrgLevAbbrName'].$v6['OrgLevAbbrName']. $v5['OrgLevAbbrName']  .$v4['OrgLevAbbrName'] .$v3['OrgLevAbbrName'].$result['lev2']['OrgLevAbbrName']; 
													$result['orgdetail'][$x]['orgdid'] = $v8['OrgLevDID'];
													$result['orgname'] = $v8['OrgLevAbbrName'].$v7['OrgLevAbbrName'].$v6['OrgLevAbbrName']. $v5['OrgLevAbbrName']  .$v4['OrgLevAbbrName'] .$v3['OrgLevAbbrName'].$result['lev2']['OrgLevAbbrName']; 
													$result['lev8'][$k8]['OrgName'] = $v8['OrgLevAbbrName'].$v7['OrgLevAbbrName'].$v6['OrgLevAbbrName']. $v5['OrgLevAbbrName']  .$v4['OrgLevAbbrName'] .$v3['OrgLevAbbrName'].$result['lev2']['OrgLevAbbrName']; 

													$x++ ;
												} 
											}
											$x++ ; 
										} 
									}
									$x++ ;
								} 
							}
							$x++ ;
						} 
					} 
					$x++ ; 
				}  
			}
			$x++ ; 
		} 
	}

	/*
	$nowOrg =  $orgLevDID ;

	for( $x=2 ; $x>1 ; $x++ ){ 
		$x = (int)$x ;
		$y = ( (int)$x)-1 ;
		$tableP = 'OrgLevel'.$x.'Parent' ;   //$orgLevDID
		$tableO = 'OrgLevel'.$x ; 
		$table1 = 'OrgLevel'.$y ; 

		 $result['unitM'][$x]['OrgLevelDID'] =  $nowOrg ;   
		 $result['unitM'][$x]['tableP'] =  $tableP ; 
 
		$result['unitM'][$x]['detail'] = $clsorg->LoadOnce($tableO, array('OrgLevDID'=>$nowOrg ));
		$result['unitM'][$x]['parent'] = $clsorg->LoadOnce($tableP, array('OrgLevDID'=>$nowOrg ));
	 

		if($result['unitM'][$x]['parent'] ){ 
			$nowOrg = $result['unitM'][$x]['parent']['OrgParentCode'];
			$result['unitM'][$x]['parent']['unit'] = $clsorg->LoadOnce($table1, array('OrgLevDID'=> $nowOrg ));
		}
	}*/

	echo json_encode($result);
}

function LoadPosStrucSkill(){
	$clsorg = new OrgClass();  
	$PositionID  = $_POST['PositionID']; 
	$result = $clsorg->LoadPosStrucSkill( $PositionID ); 
 
	echo json_encode($result);
}




function LoadDataOrgParentShort(){
	$clsorg = new OrgClass();  
	$orgLevDID  = $_POST['orgLevDID']; 
	$result = $clsorg->LoadDataOrgParent( $orgLevDID ); 
 
	echo json_encode($result);
}

function LoadPosPersonDetail(){
	$clsorg = new OrgClass();  
	$HrtPositionID  = $_POST['HrtPositionID']; 
	$HrtPosNo  = $_POST['HrtPosNo']; 
	$orgLevDID  = $_POST['OrgLevDID']; 
	$where['PositionID'] = $HrtPositionID;
	$where['PosNo'] = $HrtPosNo;

	$result=$clsorg->Load('HrtPosPerson', $where , ' PosNo ASC','');
	$result['orgDetail'] = $clsorg->LoadDataOrgParent( $orgLevDID ); 
	$result['skill'] = $clsorg->LoadPosStrucSkill( $HrtPositionID ); 
 
 
	echo json_encode($result);
}

///////////////////////////////////////////////////////////////////////////////////////////////


switch($_REQUEST["mode"]){
	case "AddDataSet" : AddDataSet(); break;
	case "AddDataID" : AddDataID(); break;
	case "EditDataSet" : EditDataSet(); break;
	case "DeleteDataSet" : DeleteDataSet(); break;
	case "AddDataContents" : AddDataContents(); break;
	case "EditDataContents" : EditDataContents(); break; 
	case "LoadAllData" : LoadAllData(); break; 
	case "LoadFieldData" : LoadFieldData(); break;
	case "LoadOneRow" : LoadOneRow(); break;
	case "LoadLikeTitle" : LoadLikeTitle(); break;
	case "StatusDataSetEX" : StatusDataSetEX(); break;
	
	////////////////////////////////////////////////////////////////////////////////////////
	
	case "LoadOrgStrucByOrgType" : LoadOrgStrucByOrgType(); break;
	case "LoadDataOrgLev" : LoadDataOrgLev(); break;

	case "LoadOrgByParentCode" : LoadOrgByParentCode(); break;
	case "CreateOrgData" : CreateOrgData(); break;
	case "EditOrgData" : EditOrgData(); break;

	case "LoadLevel1ByOrgType" : LoadLevel1ByOrgType(); break;
	case "LoadLevel2ByOrgType" : LoadLevel2ByOrgType(); break;

	case "LoadPositionByOrgLevelx" : LoadPositionByOrgLevelx(); break;
	case "LoadPosPersonByOrgLevelx" : LoadPosPersonByOrgLevelx(); break;

	case "AddPositionDataSet" : AddPositionDataSet(); break;
	case "UpdatePositionDataSet" : UpdatePositionDataSet(); break;

	case "LoadOrgforCreate" : LoadOrgforCreate(); break;
	case "CreateOrg0Data" : CreateOrg0Data(); break;

	case "LoadCountDistByField" : LoadCountDistByField(); break;
	case "LoadRankSort" : LoadRankSort(); break;
	////////////////////////////////////////////////////////////////////////////////////////


	case "LoadDataOrgParent" : LoadDataOrgParent(); break;
	case "LoadDataOrgParentShort" : LoadDataOrgParentShort(); break;
	case "LoadPosPersonDetail" : LoadPosPersonDetail(); break;
	
	
	case "LoadPosStrucAddAmt" : LoadPosStrucAddAmt(); break;


	case "LoadPositionDetail" : LoadPositionDetail(); break;
	case "LoadDataOrgSel" : LoadDataOrgSel(); break;

	case "LoadPosStrucSkill" : LoadPosStrucSkill(); break;
	

	////////////////////////////////////////////////////////////////////////////////////////
	default : echo '{"success":"FAIL INC orgClass"}';
}
?>
