<?php
header('Access-Control-Allow-Origin: *');
header("Content-type: text/html; charset=utf-8");

class PlanManpowerClass extends DuckClass{
	////////////////////////////////////////////////////////////////////////////////////////////
	public function LoadPlanHeader($fyear){

		if(!empty($where)){
			foreach((array)$where as $i => $item){
			$item = mysqli_real_escape_string($this->mysqli,$item);
			$qrywhere .= " $i = '$item' AND ";
			}
		}
		if(!empty($orderby)){
			$orderby = "ORDER BY $orderby";
		}
		if(!empty($limit)){
			$limit = "LIMIT $limit";
		}
			$sql="
					SELECT * 
					FROM plan_header 
					WHERE PlanHeaderDisplay ='Y' 
					
					AND PlanHeaderYear = '$fyear'  
		";
	 
		$myquery = $this->mysqli->query($sql);
		while($row = $myquery->fetch_assoc() ) {
			$result[] = $row;
		}
		$myquery->free();

		return $result; 
	}

	public function LoadPlanRegis($fiscalyear,$planmantype,$draftnumber){
	 

		$result[0]['titlename'] = 'นาย' ;
		$result[0]['fname'] = 'ธนพล' ;
		$result[0]['lname'] = 'สุขดี' ; 
		$result[0]['position_id'] = '6400' ;
		$result[0]['positionname'] = 'นายทหารบัญชี' ;
		$result[0]['gender'] = 'ชาย' ;
		$result[0]['e_degree'] = 'ปริญญาตรี' ;
		$result[0]['e_groupmajor'] = 'บริหารธุรกิจ' ;
		$result[0]['e_major'] = 'การบัญชี การธนาคาร' ; 

		$result[0]['titlename'] = 'นาย' ;
		$result[0]['fname'] = 'กรวิทย์' ;
		$result[0]['lname'] = 'เลิศหล้า' ; 
		$result[0]['position_id'] = '6400' ;
		$result[0]['positionname'] = 'นายทหารบัญชี' ;
		$result[0]['gender'] = 'ชาย' ;
		$result[0]['e_degree'] = 'ปริญญาตรี' ;
		$result[0]['e_groupmajor'] = 'บริหารธุรกิจ' ;
		$result[0]['e_major'] = 'การบัญชี' ; 


		$result[0]['titlename'] = 'นางสาว' ;
		$result[0]['fname'] = 'ศิริวรรณ' ;
		$result[0]['lname'] = 'งามพริ้ง' ; 
		$result[0]['position_id'] = '6400' ;
		$result[0]['positionname'] = 'นายทหารบัญชี' ;
		$result[0]['gender'] = 'หญิง' ;
		$result[0]['e_degree'] = 'ปริญญาตรี' ;
		$result[0]['e_groupmajor'] = 'บริหารธุรกิจ' ;
		$result[0]['e_major'] = 'การบัญชี' ; 

		return $result; 
	}

	public function LoadPlanRegisById($planId){
		

		$result[0]['titlename'] = 'นาย' ;
		$result[0]['fname'] = 'ธนพล' ;
		$result[0]['lname'] = 'สุขดี' ; 
		$result[0]['position_id'] = '6400' ;
		$result[0]['positionname'] = 'นายทหารบัญชี' ;
		$result[0]['gender'] = 'ชาย' ;
		$result[0]['e_degree'] = 'ปริญญาตรี' ;
		$result[0]['e_groupmajor'] = 'บริหารธุรกิจ' ;
		$result[0]['e_major'] = 'การบัญชี การธนาคาร' ; 

		$result[0]['titlename'] = 'นาย' ;
		$result[0]['fname'] = 'กรวิทย์' ;
		$result[0]['lname'] = 'เลิศหล้า' ; 
		$result[0]['position_id'] = '6400' ;
		$result[0]['positionname'] = 'นายทหารบัญชี' ;
		$result[0]['gender'] = 'ชาย' ;
		$result[0]['e_degree'] = 'ปริญญาตรี' ;
		$result[0]['e_groupmajor'] = 'บริหารธุรกิจ' ;
		$result[0]['e_major'] = 'การบัญชี' ; 


		$result[0]['titlename'] = 'นางสาว' ;
		$result[0]['fname'] = 'ศิริวรรณ' ;
		$result[0]['lname'] = 'งามพริ้ง' ; 
		$result[0]['position_id'] = '6400' ;
		$result[0]['positionname'] = 'นายทหารบัญชี' ;
		$result[0]['gender'] = 'หญิง' ;
		$result[0]['e_degree'] = 'ปริญญาตรี' ;
		$result[0]['e_groupmajor'] = 'บริหารธุรกิจ' ;
		$result[0]['e_major'] = 'การบัญชี' ; 
		return $result; 
	}

	public function LoadDataRegis($fiscalyear,$planmantype,$draftnumber){
		

		$result[0]['titlename'] = 'นาย' ;
		$result[0]['fname'] = 'ธนพล' ;
		$result[0]['lname'] = 'สุขดี' ; 
		$result[0]['position_id'] = '6400' ;
		$result[0]['positionname'] = 'นายทหารบัญชี' ;
		$result[0]['gender'] = 'ชาย' ;
		$result[0]['e_degree'] = 'ปริญญาตรี' ;
		$result[0]['e_groupmajor'] = 'บริหารธุรกิจ' ;
		$result[0]['e_major'] = 'การบัญชี การธนาคาร' ; 

		$result[0]['titlename'] = 'นาย' ;
		$result[0]['fname'] = 'กรวิทย์' ;
		$result[0]['lname'] = 'เลิศหล้า' ; 
		$result[0]['position_id'] = '6400' ;
		$result[0]['positionname'] = 'นายทหารบัญชี' ;
		$result[0]['gender'] = 'ชาย' ;
		$result[0]['e_degree'] = 'ปริญญาตรี' ;
		$result[0]['e_groupmajor'] = 'บริหารธุรกิจ' ;
		$result[0]['e_major'] = 'การบัญชี' ; 


		$result[0]['titlename'] = 'นางสาว' ;
		$result[0]['fname'] = 'ศิริวรรณ' ;
		$result[0]['lname'] = 'งามพริ้ง' ; 
		$result[0]['position_id'] = '6400' ;
		$result[0]['positionname'] = 'นายทหารบัญชี' ;
		$result[0]['gender'] = 'หญิง' ;
		$result[0]['e_degree'] = 'ปริญญาตรี' ;
		$result[0]['e_groupmajor'] = 'บริหารธุรกิจ' ;
		$result[0]['e_major'] = 'การบัญชี' ; 

		return $result; 
	}

	public function LoadDataPersonNew($fiscalyear,$draftnumber,$personStatusType){
		

		$result[0]['titlename'] = 'นาย' ;
		$result[0]['fname'] = 'ธนพล' ;
		$result[0]['lname'] = 'สุขดี' ; 
		$result[0]['position_id'] = '6400' ;
		$result[0]['positionname'] = 'นายทหารบัญชี' ;
		$result[0]['gender'] = 'ชาย' ;
		$result[0]['e_degree'] = 'ปริญญาตรี' ;
		$result[0]['e_groupmajor'] = 'บริหารธุรกิจ' ;
		$result[0]['e_major'] = 'การบัญชี การธนาคาร' ; 

		return $result; 

	}

	
}


?>
