/////////////////////////////////////////////////////////////////////////////////////
////////////////////           org   Script          ///////////////////////////
var org = {
    url: '/Model/Ducklab/org.inc.php'
};

//////////////////////////////////////////////////////////////////////////////////////////////////
org.callModalAdd=function(FuncName, table) {

    //alert(FuncName);
    var btn_add = '<button type="button" class="btn btn-success" onclick="' + FuncName + '();"> <i class="fa fa-save"> </i> ยืนยันการเพิ่มข้อมูล </button>';
    $("#btn_add").html(btn_add);

    $('#modal_add').modal('show');
};

org.callModalEdit=function(FuncName, table, table_id, detail) {

    //$("#txt_mo_name").html(detail);

    var btn_edit = '<button type="button" class="btn btn-warning" onclick="' + FuncName + '();"> <i class="fa fa-trash"> </i> ยืนยันการแก้ไข </button>';
    $("#btn_edit").html(btn_edit);

    $('#modal_edit').modal('show');
};

org.callModalDelete=function(table, table_id, detail, link) {

    //callDeleteFixField
    $("#txt_mo_name").html("ต้องการยืนยันการลบข้อมูล " + detail);

    var btn_delete = '<a href="#" class="btn btn-min-width mb-1 round btn-danger" onclick="org.callDelete(\'' + table + '\',\'' + table_id + '\',\'' + link + '\');"><i class="fa fa-trash"> </i> ยืนยัน </a> ';
    $("#btn_delete").html(btn_delete);

    $('#modal_delete').modal('show');
};

org.callModalDeleteFixField=function(table, table_id, detail, id_name, status_name, link) {

    //callDeleteFixField
    $("#txt_mo_name").html("ต้องการยืนยันการลบข้อมูล " + detail);

    var btn_delete = '<a href="#" class="btn btn-min-width mb-1 round btn-danger" onclick="org.callDeleteFixField(\'' + table + '\',\'' + table_id + '\',\'' + id_name + '\',\'' + status_name + '\',\'' + link + '\');"><i class="fa fa-trash"> </i> ยืนยัน </a> ';
    $("#btn_delete").html(btn_delete);

    $('#modal_delete').modal('show');
};

org.callModalDisable=function(table, table_id, detail, link) {


    $("#txt_detail_dis").html("ต้องการยืนยันการลบข้อมูล " + detail);

    var btn_disable = '<button type="button" class="btn btn-danger" onclick="org.callDisable(\'' + table + '\',\'' + table_id + '\',\'' + link + '\');"> <i class="fa fa-trash"> </i> ยืนยัน </button>';
    $("#btn_disable").html(btn_disable);

    $('#modal_disable').modal('show');
};


org.callModalDeleteTruly=function(table, table_id, detail) {
    $("#txt_mo_name").html(detail);

    var btn_delete = '<button type="button" class="btn btn-danger" onclick="org.callDeleteTruly(\'' + table + '\',\'' + table_id + '\');"> <i class="fa fa-trash"> </i> ยืนยันการลบ </button>';
    $("#btn_delete").html(btn_delete);

    $('#modal_delete').modal('show');
};

org.callDelete=function(table, table_id, link) {
    //alert(table);
    //alert(table_id);
    if (!fieldname) {
        var fieldname = 'id';
    }
    var DataSet = {
        table: table,
        data: {
            deleted: '1',
        },
        where: { id: table_id }
    };
    //console.log(DataSet);

    $.ajax({
        url: org.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $('#modal_delete').modal('hide');
        },
        success: function(data) {
            //console.log(data);

            if (data.success == "COMPLETE") {

                duck.ModalSShow();

                // setTimeout(duck.ModalSHide, 3000);
                if (link == 'backlink') {
                    setTimeout(duck.OpenBack, 500);
                } else {
                    setTimeout(duck.ReloadPage, 3000);
                }
            } else {
                duck.ModalWShow();
            }

        },
        complete: function() {},
        error: function(data) {
            console.log('Please check scripts callDelete');
            console.log(data);
            duck.NotiDanger();
        }
    });
};

org.callDisable=function(table, table_id, link) {
    //alert(table);
    //alert(table_id);
    $('#modal_disable').modal('hide');
    if (!fieldname) {
        var fieldname = 'id';
    }
    var DataSet = {
        table: table,
        data: {
            enable: 'N',
        },
        where: { id: table_id }
    };
    //console.log(DataSet);

    $.ajax({
        url: org.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $('#modal_delete').modal('hide');
        },
        success: function(data) {
            //console.log(data);

            if (data.success == "COMPLETE") {

                duck.ModalSShow();

                // setTimeout(duck.ModalSHide, 3000);
                if (link == 'backlink') {
                    setTimeout(duck.OpenBack, 500);
                } else {
                    setTimeout(duck.ReloadPage, 3000);
                }
            } else {
                duck.ModalWShow();
            }

        },
        complete: function() {},
        error: function(data) {
            console.log('Please check scripts callDelete');
            console.log(data);
            duck.NotiDanger();
        }
    });
};

org.callDeleteFixField=function(table, table_id, id_name, status_name, link) {
    var DataSet = {
        table: table,
        data: {
            status: '1', //status_name
            id: table_id, //status_name
        },
        data_name: {
            status_n: status_name,
            id_n: id_name,
        }
    };
    //console.log(DataSet);

    $.ajax({
        url: org.url + '?mode=StatusDataSetEX',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $('#modal_delete').hide();
            $("#modalLoading").show();
        },
        success: function(data) {
            if (data.success == "COMPLETE") {
                duck.SwalSuccess('ลบข้อมูลสำเร็จ');
                // duck.NotiSuccess();



                duck.ModalSShow();
                if (link == 'backlink') {
                    setTimeout(duck.OpenBack, 500);
                } else {
                    setTimeout(duck.ReloadPage, 1500);
                }
            } else {

                duck.NotiWarning();
            }
        },
        complete: function() {},
        error: function(data) {
            console.log('Please check scripts callDelete');
            console.log(data);
            duck.NotiDanger();
        }
    });
};



org.callDeleteTruly=function(table, table_id) {

    var DataSet = {
        table: table,
        where: { id: table_id }
    };
    //console.log(DataSet);

    $.ajax({
        url: org.url + '?mode=DeleteDataSet',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $('#modal_delete').modal('hide');
        },
        success: function(data) {
            //console.log(data);

            if (data.success == "COMPLETE") {
                alert("ลบรายการสำเร็จ");
                window.location.reload();;

            } else {
                alert("ลบรายการไม่สำเร็จ กรุณาลองใหม่อีกครั้ง");

            }

        },
        complete: function() {},
        error: function(data) {
            console.log('Please check scripts callDelete');
            console.log(data);
        }
    });


};

org.callSetEnable=function(table_name, table_id, refto) {
    var x = 'N';
    if (!refto) {
        refto = "enable";
    }
    if ($("#" + refto + table_id).is(':checked') == true) {
        x = 'Y';
    }
    var DataSet = {
        table: table_name,
        data: {
            enable: x,
        },
        where: { id: table_id }
    };

    $.ajax({
        url: org.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {},
        success: function(data) {
            if (data.success == "COMPLETE") {
                duck.NotiSuccess();

            } else {
                //	duck.ModalWShow();
                duck.NotiWarning();
            }
        },
        complete: function() {},
        error: function(data) {
            console.log('Please check scripts callSetEnable');
            console.log(data);
            duck.NotiDanger();
        }
    });
};


org.callSetDisplay=function(table_name, table_id, refto) {
    var x = 'N';
    if (!refto) {
        refto = "display";
    }
    if ($("#" + refto + table_id).is(':checked') == true) {
        x = 'Y';
    }
    var DataSet = {
        table: table_name,
        data: {
            display: x,
        },
        where: { id: table_id }
    };

    $.ajax({
        url: org.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {},
        success: function(data) {
            if (data.success == "COMPLETE") {
                duck.NotiSuccess();
                /*
                 duck.ModalSShow();

                // setTimeout(duck.ModalSHide, 3000);
                if(link=='backlink'){
                	setTimeout( duck.OpenBack   , 500   );
                }else{
                	setTimeout( duck.ReloadPage   , 3000   );
                }
                */
            } else {
                //	duck.ModalWShow();
                duck.NotiWarning();
            }

        },
        complete: function() {},
        error: function(data) {
            console.log('Please check scripts callSetDisplay');
            console.log(data);
            duck.NotiDanger();
        }
    });
};
/*
org.LoadProvince=function(v_val,element_id){
	if(v_val){
*/

org.SetFrom=function(action, dataid, pageaction) {

    $('#dataid').val(dataid);
    $('#action').val(action);
    $("#formset").attr('action', pageaction)
        //console.log(action+" || "+data_id);
    $("#formset").submit();
};


org.SetFromAction=function(action, data_id, actionlink) {
    $("#form_set").attr("action", actionlink);
    $('#data_id').val(data_id);
    $('#action').val(action);

    $("#form_set").submit();
};
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


org.checkFieldmore=function(DataSet, element_id) {

    console.log(DataSet);
    $.ajax({
        url: org.url + '?mode=LoadCountDistByField',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {},
        success: function(result, status, xhr) {

            console.log(result);
            // console.log(status);
            // console.log(xhr);

            if (result['cnt'] == 0) {
                $("#" + element_id).removeClass("danger");
                $("#" + element_id).removeClass("border-danger");
                $("#btncreatecf").removeAttr('disabled');
                $("#btneditcf").removeAttr('disabled');
            } else {
                $("#" + element_id).addClass("danger");
                $("#" + element_id).addClass("border-danger");
                $("#btncreatecf").attr('disabled', 'disabled');
                $("#btneditcf").attr('disabled', 'disabled');
            }
        },
        complete: function() {},
        error: function(xhr, status, error) {
            console.log('Please check scripts checkFieldmore');
            console.log(xhr);
            duck.NotiDanger();
        }
    });

};


///////////////////    OrgGroupType : ประเภทโครงสร้างองค์กร  /////////////////
org.LoadOrgGroupType=function() {
    var DataSet = {
        table: 'OrgGroupType',
        field: {
            OrgGroupTypeID: '',
            OrgGroupTypeName: '',
            OrgGroupTypeActive: ''
        },
        where: {
            OrgGroupTypeDelete: '0',
        },
        orderby: "",
        limit: ""
    };
    console.log(DataSet);

    $.ajax({
        //url:org.url+'?mode=LoadAllData',
        url: org.url + '?mode=LoadFieldData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';


                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:org.SetFrom(\'EDIT\',\'' + value1.OrgGroupTypeID + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['OrgGroupTypeID'] + '"></i></a>\
							<a href="javascript:org.callModalDeleteFixField(\'OrgGroupType\',\'' + value1['OrgGroupTypeID'] + '\'\,\'ประเภทโครงสร้าง\',\'OrgGroupTypeID\',\'OrgGroupTypeDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1['OrgGroupTypeID'] + '</td>\
						<td>' + value1['OrgGroupTypeName'] + '</td>\
						<td class="text-center">' + org.SetActive(value1['OrgGroupTypeActive']) + '</td>\
					</tr>\
					';
                    //	<a href="javascript:org.callModalDelete('OrgGroupType','<?php echo $value['id'];?>',' <?php echo $menuname;?> ','militaryrank_data.php');" class="dropdown-item "><i class="ft-trash-2"></i> ลบ </a>

                    $('#OrganizationGroupType> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#OrganizationGroupType').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadOrgGroupType');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

org.CreateOrgGroupType=function() {
    //alert('XX');

    $('#modal_createcf').modal('hide');
    var active = 0;
    if ($("input[name=OrgGroupTypeActive]:checked").val() == "on") {
        var active = 1;
    }
    //$("."+stdyear).prop('checked', false);
    var exDat = {
        table: "OrgGroupType",
        data: {
            OrgGroupTypeName: $("#OrgGroupTypeName").val(),
            OrgGroupTypeActive: active,
            OrgGroupTypeDelete: '0',
            OrgGroupTypeCreateBy: $("#OrgGroupTypeCreateBy").val(),
            OrgGroupTypeCreateDate: '',
            OrgGroupTypeUpdateBy: $("#OrgGroupTypeUpdateBy").val(),
            OrgGroupTypeUpdateDate: ''
        }
    };

    console.log(exDat)

    $.ajax({
        url: org.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {
            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + data.code;
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

org.GetOrgGroupType=function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'OrgGroupType',
        where: {
            OrgGroupTypeID: dataid,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: org.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);

            if (result) {
                $("#OrgGroupTypeName").val(result.OrgGroupTypeName);
                if (result.OrgGroupTypeActive == 0) {
                    $("#OrgGroupTypeActive").bootstrapToggle('off');
                } else {
                    $("#OrgGroupTypeActive").bootstrapToggle('on');
                }
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetOrgGroupType');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

org.EditOrgGroupType=function() {

    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();
    var active = 0;
    if ($("input[name=OrgGroupTypeActive]:checked").val() == "on") {
        var active = 1;
    }
    //$("."+stdyear).prop('checked', false);
    var exDat = {
        table: "OrgGroupType",
        data: {
            OrgGroupTypeName: $("#OrgGroupTypeName").val(),
            OrgGroupTypeActive: active,
            OrgGroupTypeUpdateBy: $("#OrgGroupTypeUpdateBy").val(),
            OrgGroupTypeUpdateDate: ""
        },
        where: {
            OrgGroupTypeID: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: org.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

org.LoadOrgGroupTypeSel=function(v_val, element_id) {
    if (v_val) {

    }
    var exDat = {
        table: "OrgGroupType",
        where: {
            OrgGroupTypeActive: 1,
            OrgGroupTypeDelete: 0
        },
        orderby: " OrgGroupTypeName   ASC ",
        //	orderby : " CONVERT (   OrgGroupTypeName  USING tis620 ) ASC " ,
        limit: "",
    };

    $.ajax({
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        success: function(data) {
            //console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกประเภทโครงสร้าง").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.OrgGroupTypeID == v_val) {
                    $(".select2-chosen").text(result.OrgGroupTypeName);
                    $('<option>').attr('value', result.OrgGroupTypeID).attr('selected', 'selected').text(result.OrgGroupTypeName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.OrgGroupTypeID).text(result.OrgGroupTypeName).appendTo('#' + element_id);
                }
            });

        },
        error: function(data) {
            console.log(data);
            console.log('check LoadOrgGroupTypeSel');
            duck.NotiDanger();
        }
    });

};

org.GetOrgGroupTypeName=function(OrgGroupTypeID) {

    var DataSet = {
        table: 'OrgGroupType',

        where: {
            OrgGroupTypeID: OrgGroupTypeID,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: org.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {

        },
        success: function(result, status, xhr) {

            console.log(result);

            if (result) {
                return result.OrgGroupTypeName;
            }
        },
        complete: function() {

        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetOrgGroupType');
            console.log(xhr);
            //duck.NotiDanger();
        }
    });
};

///////////////////    OrgPartTable : ส่วนราชการ  /////////////////

org.LoadOrgPart=function() {
    var DataSet = {
        table: "OrgPart",
        where: {
            OrgPartDelete: 0
        },
        orderby: "",
        limit: ""
    };
    console.log(DataSet);

    $.ajax({
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';


                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:org.SetFrom(\'EDIT\',\'' + value1.OrgPartID + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['OrgPartID'] + '"></i></a>\
							<a href="javascript:org.callModalDeleteFixField(\'OrgPart\',\'' + value1['OrgPartID'] + '\'\,\'ส่วนราชการ\',\'OrgPartID\',\'OrgPartDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1.OrgPartCode + '</td>\
						<td >' + value1.OrgPart + '</td>\
						<td class="text-center">' + org.SetActive(value1['OrgPartActive']) + '</td>\
					</tr>\
					';


                    $('#OrgPartTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#OrgPartTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadOrgPartTable');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

org.LoadOrgPartLv1=function() {
    var DataSet = {
        table: "OrgLevel1",
        where: {
            OrgLevelDelete: 0
        },
        orderby: "",
        limit: ""
    };
    console.log(DataSet);

    $.ajax({
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';


                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:org.SetFrom(\'EDIT\',\'' + value1.OrgPartID + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['OrgPartID'] + '"></i></a>\
							<a href="javascript:org.callModalDeleteFixField(\'OrgType\',\'' + value1['OrgPartID'] + '\'\,\'ส่วนราชการ\',\'OrgPartID\',\'OrgPartIDDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1.OrgPartID + '</td>\
						<td >' + value1.OrgPart + '</td>\
						<td class="text-center">' + org.SetActive(value1['OrgPartActive']) + '</td>\
					</tr>\
					';


                    $('#OrgPartTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#OrgPartTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadOrgPartTable');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

org.GetOrgPart=function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'OrgPart',
        where: {
            OrgPartID: dataid,
        }
    };
    console.log(DataSet);
    $.ajax({
        url: org.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);

            if (result) {
                $("#OrgPart").val(result.OrgPart);
                $("#OrgPartCode").val(result.OrgPartCode);
                if (result.OrgPartActive == 0) {
                    $("#OrgPartActive").bootstrapToggle('off');
                } else {
                    $("#OrgPartActive").bootstrapToggle('on');
                }
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetOrgPart');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

org.EditOrgPart=function() {
    $('#modal_editcf').modal('hide');

    var dataid = $("#dataid").val();
    var active = 0;
    if ($("input[name=OrgPartActive]:checked").val() == "on") {
        var active = 1;
    }

    var exDat = {
        table: "OrgPart",
        data: {
            OrgPartName: $("#OrgPartName").val(),
            OrgPartActive: active,
        },
        where: {
            OrgPartID: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: org.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

org.CreateOrgPart=function() {
    $('#modal_createcf').modal('hide');


    var active = 0;
    if ($("input[name=OrgPartActive]:checked").val() == "on") {
        var active = 1;
    }

    var exDat = {
        table: "OrgPart",
        data: {
            OrgPart: $("#OrgPart").val(),
            OrgPartActive: active,
            OrgPartCode: $("#OrgPartCode").val()
        }
    };

    console.log(exDat)

    $.ajax({
        url: org.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

org.LoadOrgPartSel=function(v_val, element_id) {
    if (v_val) {

    }
    var exDat = {
        table: "OrgPart",
        where: {
            OrgPartActive: 1,
            OrgPartDelete: 0
        },
        orderby: "",
        limit: "",
    };

    $.ajax({
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        success: function(data) {
            //console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกส่วนราชการ").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.OrgPartCode == v_val) {
                    $(".select2-chosen").text(result.OrgPart);
                    $('<option>').attr('value', result.OrgPartCode).attr('selected', 'selected').text(result.OrgPartCode + ':' + result.OrgPart).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.OrgPartCode).text(result.OrgPartCode + ':' + result.OrgPart).appendTo('#' + element_id);
                }
            });

        },
        error: function(data) {
            console.log(data);
            console.log('check LoadOrgPartSel');
            duck.NotiDanger();
        }
    });

};
///////////////////    OrgLevel : ฐานะหน่วย  /////////////////

org.LoadOrgLevel=function() {
    var DataSet = {
        table: 'OrgLevel',
        where: {
            OrgLevelDelete: '0',
        },
        orderby: "",
        limit: ""
    };
    console.log(DataSet);

    $.ajax({
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {
                $.each(result, function(key1, value1) {
                    var contents = '';
                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:org.SetFrom(\'EDIT\',\'' + value1.OrgLevelID + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['OrgLevelID'] + '"></i></a>\
							<a href="javascript:org.callModalDeleteFixField(\'OrgLevel\',\'' + value1['OrgLevelID'] + '\'\,\'ฐานะหน่วย\',\'OrgLevelID\',\'OrgLevelDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1.OrgLevelID + '</td>\
						<td >' + value1.OrgLevelName + '</td>\
						<td class="text-center">' + org.SetActive(value1['OrgLevelActive']) + '</td>\
					</tr>\
					';
                    $('#OrgLevelTable> tbody').append(contents);
                });
            }

        },
        complete: function() {
            $('#OrgLevelTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadOrgLevel');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

org.GetOrgLevel=function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'OrgLevel',
        where: {
            OrgLevelID: dataid,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: org.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);

            if (result) {
                $("#OrgLevelName").val(result.OrgLevelName);
                $("#OrgLevelID").val(result.OrgLevelID);
                if (result.OrgLevelActive == 0) {
                    $("#OrgLevelActive").bootstrapToggle('off');
                } else {
                    $("#OrgLevelActive").bootstrapToggle('on');
                }
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetOrgLevel');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

org.EditOrgLevel=function() {
    $('#modal_editcf').modal('hide');

    var dataid = $("#dataid").val();
    var active = 0;
    if ($("input[name=OrgLevelActive]:checked").val() == "on") {
        var active = 1;
    }

    var exDat = {
        table: "OrgLevel",
        data: {
            OrgLevelName: $("#OrgLevelName").val(),
            OrgLevelActive: active,
            OrgLevelUpdateBy: $("#OrgLevelUpdateBy").val(),
            OrgLevelUpdateDate: $("#OrgLevelUpdateDate").val(),
        },
        where: {
            OrgLevelID: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: org.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

org.CreateOrgLevel=function() {
    $('#modal_createcf').modal('hide');
    var active = 0;
    if ($("input[name=OrgLevelActive]:checked").val() == "on") {
        var active = 1;
    }
    //$("."+stdyear).prop('checked', false);
    var exDat = {
        table: "OrgLevel",
        data: {
            OrgLevelName: $("#OrgLevelName").val(),
            OrgLevelActive: active,
            OrgLevelDelete: '0',
            OrgLevelCreateBy: $("#OrgLevelCreateBy").val(),
            OrgLevelCreateDate: $("#OrgLevelCreateDate").val(),
            OrgLevelUpdateBy: $("#OrgLevelCreateBy").val(),
            OrgLevelUpdateDate: $("#OrgLevelCreateDate").val()
        }
    };

    console.log(exDat)

    $.ajax({
        url: org.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {
            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + data.code;
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

org.LoadOrgLevelSel=function(v_val, element_id) {
    if (v_val) {

    }
    var exDat = {
        table: "OrgLevel",
        where: {
            OrgLevelActive: 1,
            OrgLevelDelete: 0
        },
        orderby: "",
        limit: "",
    };

    $.ajax({
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        success: function(data) {
            //console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกฐานะหน่วย").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.OrgLevelID == v_val) {
                    $(".select2-chosen").text(result.OrgLevelName);
                    $('<option>').attr('value', result.OrgLevelID).attr('selected', 'selected').text(result.OrgLevelName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.OrgLevelID).text(result.OrgLevelName).appendTo('#' + element_id);
                }
            });

        },
        error: function(data) {
            console.log(data);
            console.log('check LoadOrgLevelSel');
            duck.NotiDanger();
        }
    });

};
///////////////////    OrgType : โครงสร้างองค์กร  /////////////////

org.LoadOrgType=function() {
    var DataSet = {
        table: 'OrgType',
        field: {
            OrgTypeID: '',
            OrgTypeName: '',
            OrgTypeActive: '',
            OrgTypeStartDate: '',
            OrgTypeEndDate: ''
        },
        where: {
            OrgTypeDelete: '0',
        },
        orderby: "",
        limit: ""
    };
    console.log(DataSet);

    $.ajax({
        url: org.url + '?mode=LoadFieldData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';


                    var OrgTypeStartDate = "-";
                    var OrgTypeEndDate = "-";
                    if (value1['OrgTypeStartDate'] != null) {
                        OrgTypeStartDate = duck.ConvertDate(value1['OrgTypeStartDate'], 'mini', 'th');

                    }
                    if (value1['OrgTypeEndDate'] != null) {
                        OrgTypeEndDate = duck.ConvertDate(value1['OrgTypeEndDate'], 'mini', 'th');
                    }

                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:org.SetFrom(\'EDIT\',\'' + value1.OrgTypeID + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['OrgTypeID'] + '"></i></a>\
							<a href="javascript:org.callModalDeleteFixField(\'OrgType\',\'' + value1['OrgTypeID'] + '\'\,\'โครงสร้าง\',\'OrgTypeID\',\'OrgTypeDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1.OrgTypeID + '</td>\
						<td >' + value1.OrgTypeName + '</td>\
						<td class="text-center">' + OrgTypeStartDate + '</td>\
						<td class="text-center">' + OrgTypeEndDate + '</td>\
						<td class="text-center">' + org.SetActive(value1['OrgTypeActive']) + '</td>\
					</tr>\
					';
                    $('#OrgType> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#OrgType').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadOrgType');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

org.GetOrgType=function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'OrgType',
        where: {
            OrgTypeID: dataid,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: org.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);

            //startDate 
            $("#OrgTypeStartDate").attr('data-value', result.OrgTypeStartDate);


            if (result) {
                $("#OrgTypeName").val(result.OrgTypeName);
                if (result.OrgTypeActive == 0) {
                    $("#OrgTypeActive").bootstrapToggle('off');
                } else {
                    $("#OrgTypeActive").bootstrapToggle('on');
                }
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetOrgType');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

org.EditOrgType=function() {

    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();
    var active = 0;
    var OrgTypeAirForce = 0;
    var OrgTypeExtraMoney = 0;
    var OrgTypeCurrent = 0;

    // if( $("input[name=OrgTypeActive]:checked").val() =="on"){
    // active = 1 ;
    // }
    //teachdate : $("input[name=teachdate_submit]").val(),
    //gender : $('input[type=radio][name=gender]:checked').val(),

    var checkBox = document.getElementById("OrgTypeActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }

    if ($("#OrgTypeAirForce").prop('checked')) {
        OrgTypeAirForce = 1;
    }
    if ($("#OrgTypeExtraMoney").prop('checked')) {
        OrgTypeExtraMoney = 1;
    }
    if ($("#OrgTypeCurrent").prop('checked')) {
        OrgTypeCurrent = 1;
    }

    //$("."+stdyear).prop('checked', false);
    var exDat = {
        table: "OrgType",
        data: {
            OrgTypeName: $("#OrgTypeName").val(),
            OrgGroupTypeID: $("#OrgGroupTypeID").val(),
            OrgTypeStartDate: $("input[name=OrgTypeStartDate_submit]").val() + " 00:00:00.000",
            OrgTypeEndDate: $("input[name=OrgTypeEndDate_submit]").val() + " 00:00:00.000",
            OrgTypeAirForce: OrgTypeAirForce,
            OrgTypeExtraMoney: OrgTypeExtraMoney,
            OrgTypeCurrent: OrgTypeCurrent,
            OrgTypeActive: active,
            OrgTypeUpdateBy: $("#OrgTypeUpdateBy").val(),
            OrgTypeUpdateDate: $("#OrgTypeUpdateDate").val()
        },
        where: {
            OrgTypeID: dataid
        }
    };

    console.log(exDat);

    $.ajax({
        url: org.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            org.EditOrg0FromOrgType();

            // $("#modalLoading").hide();
            // if( data.success == "COMPLETE"){
            // 	var link = "index.php";
            // 	duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ'); 
            // 	setTimeout( duck.OpenPage   , 2000 , link ,'_self' );
            // }else{
            // 	duck.NotiWarning();
            // } 

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });

};

org.CreateOrgType=function() {

    $('#modal_createcf').modal('hide');


    var active = 0;
    var OrgTypeAirForce = 0;
    var OrgTypeExtraMoney = 0;
    var OrgTypeCurrent = 0;


    var checkBox = document.getElementById("OrgTypeActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }

    if ($("#OrgTypeAirForce").prop('checked')) {
        OrgTypeAirForce = 1;
    }
    if ($("#OrgTypeExtraMoney").prop('checked')) {
        OrgTypeExtraMoney = 1;
    }
    if ($("#OrgTypeCurrent").prop('checked')) {
        OrgTypeCurrent = 1;
    }


    var exDat = {
        table: "OrgType",
        data: {
            OrgTypeName: $("#OrgTypeName").val(),
            OrgGroupTypeID: $("#OrgGroupTypeID").val(),
            OrgTypeStartDate: $("input[name=OrgTypeStartDate_submit]").val() + " 00:00:00.000",
            OrgTypeEndDate: $("input[name=OrgTypeEndDate_submit]").val() + " 00:00:00.000",
            OrgTypeAirForce: OrgTypeAirForce,
            OrgTypeExtraMoney: OrgTypeExtraMoney,
            OrgTypeCurrent: OrgTypeCurrent,
            OrgTypeActive: active,
            OrgTypeDelete: '0',
            OrgTypeCreateBy: $("#OrgTypeCreateBy").val(),
            OrgTypeCreateDate: $("#OrgTypeCreateDate").val(),
            OrgTypeUpdateBy: $("#OrgTypeUpdateBy").val(),
            OrgTypeUpdateDate: $("#OrgTypeUpdateDate").val()
        }
    };

    console.log(exDat)

    $.ajax({
        url: org.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                // var link = "edit.php?dataid="+data.code ;
                // duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ'); 
                // setTimeout( duck.OpenPage   , 2000 , link ,'_self' );

                org.CreateOrg0FromOrgType(data.code);
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

org.LoadOrgTypeSel=function(v_val, element_id) {
    if (v_val) {

    }
    var exDat = {
        table: "OrgType",
        where: {
            OrgTypeActive: 1,
            OrgTypeDelete: 0
        },
        orderby: " OrgTypeName   ASC ",
        limit: "",
    };

    $.ajax({
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        success: function(data) {
            //console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกโครงสร้าง").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.OrgTypeID == v_val) {
                    $(".select2-chosen").text(result.OrgTypeName);
                    $('<option>').attr('value', result.OrgTypeID).attr('selected', 'selected').text(result.OrgTypeName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.OrgTypeID).text(result.OrgTypeName).appendTo('#' + element_id);
                }
            });

        },
        error: function(data) {
            console.log(data);
            console.log('check LoadOrgTypeSel');
            duck.NotiDanger();
        }
    });
};

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////                 ////////////////////////////////////////
///////////////////////////////////                 ////////////////////////////////////////
///////////////////////////////////                 ////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////

///////////////////    OrgLevel0-OrgLevel8   : หน่วยงานทั้งหลาย  /////////////////
org.LoadOrg0Sel=function(selfDID, OrgTypeID, element_id, tablename) {
    //('',$("#OrgTypeID").val(),'OrgLevel0','OrgLevel0');//selfDID,OrgTypeID,element_id,tablename

    if (selfDID) {

    }
    var exDat = {
        table: tablename,
        where: {
            OrgLevType: OrgTypeID,
            OrgLevActive: 1,
            OrgLevDelete: 0
        },
        orderby: "  OrgLevName   ASC ",
        limit: "",
    };


    $.ajax({
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        success: function(data) {
            //console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกหน่วยงาน").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.OrgLevDID == selfDID) {
                    $(".select2-chosen").text(result.OrgLevName);
                    $('<option>').attr('value', result.OrgLevDID).attr('data-nameab', result.OrgLevAbbrName).attr('data-name', result.OrgLevName).attr('selected', 'selected').text(result.OrgLevName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.OrgLevDID).attr('data-nameab', result.OrgLevAbbrName).attr('data-name', result.OrgLevName).text(result.OrgLevName).appendTo('#' + element_id);
                }
            });

        },
        error: function(data) {
            console.log(data);
            console.log('check LoadOrg0Sel');
            duck.NotiDanger();
        }
    });

};

org.LoadOrgSel=function(selfDID, OrgCodeParent, element_id, tablename) {

    if (selfDID) {

    }
    var exDat = {
        OrgCodeParent: OrgCodeParent,
        OrgLevelDID: '',
        tablename: tablename
    };


    console.log(exDat);
    //	alert(tablename);
    $.ajax({
        url: org.url + '?mode=LoadOrgByParentCode',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        success: function(data) {
            //console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกหน่วยงาน").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.OrgLevDID == selfDID) {
                    $(".select2-chosen").text(result.OrgLevName);
                    $('<option>').attr('value', result.OrgLevDID).attr('data-nameab', result.OrgLevAbbrName).attr('data-name', result.OrgLevName).attr('selected', 'selected').text(result.OrgLevName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.OrgLevDID).attr('data-nameab', result.OrgLevAbbrName).attr('data-name', result.OrgLevName).text(result.OrgLevName).appendTo('#' + element_id);
                }
            });

        },
        error: function(data) {
            console.log(data);
            console.log('check LoadOrgSel');
            duck.NotiDanger();
        }
    });
};


org.CreateOrg=function() {

    $('#modal_createcf').modal('hide');

    var levelnow = $("#levelnow").val();
    var lastparent = $("#lastparent").val();

    if (levelnow == 0) {
        org.CreateOrg0();
        return false;
    }

    var OrgAF = 0;
    var OrgActive = 0;

    var checkBox = document.getElementById("OrgAF");
    if (checkBox.checked == true) {
        OrgAF = "1";
    }

    var checkBox = document.getElementById("OrgActive");
    if (checkBox.checked == true) {
        OrgActive = "1";
    }

    var table = "OrgLevel" + levelnow;
    var exDat = {
        table: table,
        data: {
            OrgLev: $("#OrgLevelID").val(),
            OrgLevName: $("#OrgName").val(),
            OrgLevAbbrName: $("#OrgNameAbbr").val(),
            LocCode: $("#ProvinceCode").val(),
            OrgLevType: $("#OrgLevType").val(),
            OrgLevel: levelnow,
            OrgLevState: 'N',
            OrgLevDelete: '0',
            OrgLevActive: OrgActive,
            OrgAF: OrgAF
        },
        orgParentCode: lastparent,
        levelnow: levelnow
    };

    console.log(exDat)

    $.ajax({
        url: org.url + '?mode=CreateOrgData',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();

            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + data.code;
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                //duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });

};

org.CreateOrg0=function() {
    //	alert('CreateOrg0');


    var OrgAF = 0;
    var OrgActive = 0;

    var checkBox = document.getElementById("OrgAF");
    if (checkBox.checked == true) {
        OrgAF = "1";
    }

    var checkBox = document.getElementById("OrgActive");
    if (checkBox.checked == true) {
        OrgActive = "1";
    }

    var levelnow = $("#levelnow").val();
    var lastparent = $("#lastparent").val();

    var table = "OrgLevel" + levelnow;
    var exDat = {
        table: table,
        data: {
            OrgLev: $("#OrgLevelID").val(),
            OrgLevName: $("#OrgName").val(),
            OrgLevAbbrName: $("#OrgNameAbbr").val(),
            LocCode: $("#ProvinceCode").val(),
            OrgLevState: 'N',
            OrgLevel: 0,
            OrgLevDelete: '0',
            OrgLev: '0',
            OrgLevActive: OrgActive,
            OrgLevType: $("#OrgTypeID").val(),
        },
        orgParentCode: lastparent,
        levelnow: levelnow
    };

    console.log(exDat)

    $.ajax({
        url: org.url + '?mode=CreateOrgData',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();

            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + data.code;
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

org.CreateOrg0FromOrgType=function(OrgType) {


    var levelnow = 0;

    var OrgName0 = $('#OrgName0').val();

    var table = "OrgLevel0";
    var exDat = {
        table: table,
        data: {
            OrgLev: '0',
            OrgLevName: OrgName0,
            OrgLevAbbrName: OrgName0,
            LocCode: '000000',
            OrgLevState: 'N',
            OrgLevel: 0,
            OrgLevDelete: '0',
            OrgLev: '0',
            OrgLevActive: 1,
            OrgLevType: OrgType,
            OrgLevSubUnit: '340774'
        },
        levelnow: levelnow
    };

    console.log(exDat)

    $.ajax({
        url: org.url + '?mode=CreateOrg0Data',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();

            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + OrgType;
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });


};

org.EditOrg0FromOrgType=function() {
    var OrgName0ID = $('#OrgName0ID').val();
    var OrgName0 = $('#OrgName0').val();

    var table = "OrgLevel0";
    var exDat = {
        table: table,
        data: {
            OrgLevName: OrgName0,
            OrgLevAbbrName: OrgName0
        },
        where: {
            OrgLevID: OrgName0ID
        }
    };
    console.log(exDat);

    $.ajax({
        url: org.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

org.LoadOrgStrucByOrgType=function(orgtype, element_id) {
    var DataSet = {
        OrgTypeID: orgtype,
    };
    console.log(DataSet);

    //LoadOrgLevel2ByOrgType
    var v_val = "";

    $.ajax({
        url: org.url + '?mode=LoadOrgStrucByOrgType',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(data, status, xhr) {
            console.log(data);

            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกหน่วยงาน").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.OrgLevDID == v_val) {
                    $(".select2-chosen").text(result.OrgLevAbbrName);
                    $('<option>').attr('value', result.OrgLevDID).attr('selected', 'selected').text(result.OrgLevAbbrName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.OrgLevDID).text(result.OrgLevAbbrName).appendTo('#' + element_id);
                }
            });


        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadOrgStrucByOrgType');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

org.LoadDataOrgLev=function(orgLevDID) {


    $("#box_edit").removeClass("hidden");
    $("#box_create").addClass("hidden");

    $("#btn_create1").removeAttr('disabled', 'disabled');


    $(".box_" + orgLevDID).addClass("deptnow");


    $('#orgLevDIDView').val(orgLevDID);

    if (orgLevDID) {
        var orgLevCode = orgLevDID.substr(0, 2);
        var orgLev = orgLevDID.substr(1, 1);
        //orgLev
    } else {
        return false;
    }

    // console.log("orgLevDID:"+orgLevDID);
    // console.log("orgLev:"+orgLev);
    if (orgLev == 2) {
        $('#OrgPartCodeE').removeAttr('disabled');
    } else {
        $('#OrgPartCodeE').attr('disabled', 'disabled');
    }


    $("#levelnow").val(orgLev);

    var DataSet = {
        orgLevDID: orgLevDID,
        orgLevCode: orgLevCode,
        orgLev: orgLev,

    };
    console.log(DataSet);

    $.ajax({
        //url:org.url+'?mode=LoadAllData',
        url: org.url + '?mode=LoadDataOrgLev',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {
            console.log(result);

            $("#orgStrucNum").val();
            $("#orgTypeName").val();

            $("#OrgLevDID").val(result.dept.OrgLevDID);
            $("#divisionID").val(orgLev);

            $("#orgSubUnitAbbr").val(); // ชื่อสังกัด
            $("#orgLevelName").val(result.dept.level.OrgLevelName);
            $("#orgSubUnitName").val(result.dept.OrgLevName);
            $("#OrgSubUnitCtxtE").val(result.dept.OrgLevName);

            $("#orgListAbbr").val(result.dept.OrgLevAbbrName);
            $("#orgStrucLong").val();
            $("#orgListAbbrLong").val();
            $("#orgListAbbrLongAbbr").val();

            //$("#ProvinceE").val(result.dept.LocCode); 

            //	org.LoadOrgPartSel(result.dept.OrgLevPartID,'OrgPartCodeE');  
            org.LoadOrgLevelSel(result.dept.OrgLev, 'OrgLevelE');
            org.LoadOrgSubUnitSel(result.dept.OrgLevSubUnit, 'OrgSubUnitE', result.dept.OrgLev);



            org.LoadOrgPartSel('', 'OrgPartCode');
            org.LoadOrgLevelSel('', 'OrgLevel');
            org.LoadSourceofCommSel(result.dept.CommandActive, 'CommandActive');
            org.LoadSourceofCommSel(result.dept.CommandCancle, 'CommandCancle');

            $("#CommandActiveCode").val(result.dept.CommandActiveCode);
            $("#CommandCancleCode").val(result.dept.CommandCancleCode);





            if (result.dept.CommandActiveDate != null)
                $('#CommandActiveDate').pickadate('picker').set('select', new Date(result.dept.CommandActiveDate));
            if (result.dept.CommandActiveEFFDate != null)
                $('#CommandActiveEFFDate').pickadate('picker').set('select', new Date(result.dept.CommandActiveEFFDate));
            if (result.dept.CommandCancleDate != null)
                $('#CommandCancleDate').pickadate('picker').set('select', new Date(result.dept.CommandCancleDate));
            if (result.dept.CommandCancleEFFDate != null)
                $('#CommandCancleEFFDate').pickadate('picker').set('select', new Date(result.dept.CommandCancleEFFDate));


            if (result.dept.OrgAF == 0) {
                $("#OrgAFE").bootstrapToggle('off');
            } else {
                $("#OrgAFE").bootstrapToggle('on');
            }

            //org.LoadProvinceSel(result.dept.LocCode,'ProvinceE');

            var deptallfull = "";
            var deptallmini = "";
            //['OrgLevAbbrName']
            /*
            if( result.unitM){
            	for( var i in  result.unitM){ 
            		
            		if( result.unitM[i].detail.unit){
            			console.log('X');
            		}
            	}

            }*/


            $("#orgStrucLong").val("");
            $("#orgListAbbrLong").val("");

            var orgSubUnitAbbr = $("#orgSubUnitAbbr").val();
            var orgSubUnitName = $("#orgSubUnitName").val();
            var orgListAbbr = $("#orgListAbbr").val();

            var orgSubUnitAbbrtxt = orgSubUnitName + " " + orgSubUnitAbbr;
            var orgSubUnitNametxt = orgListAbbr + " " + orgSubUnitAbbr;
            $("#orgStrucLong").val(orgSubUnitAbbrtxt);
            $("#orgListAbbrLong").val(orgSubUnitNametxt);
            //OrgLevAbbrName



            org.LoadDataOrgParent(orgLevDID);
            // LoadDataLevel2  || OrgLevPartID || OrgLevType || OrgLev 

            /*
            	
            	if( result) {
            	
            		$.each( result , function( key1, value1 ) {
            			 
            		});
            	} */
        },
        complete: function() {
            $("#modalLoading").hide();
            //setTimeout( function(){ $("#modalLoading").hide(); }   , 1500);
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadDataOrgLev');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

org.LoadOrgAFDetail=function(orglev2) {

    console.log('org.LoadOrgAFDetail:' + orglev2);

    var DataSet = {
        table: 'OrgLevel2',
        where: {
            OrgLevDID: orglev2,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: org.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);


            //result.OrgLevType 

            /*
		//	$("#orgTypeName").val();
			$("#OrgPartCodeC").val(result.OrgLevPartID);
			$("#Province").val(result.LocCode);
			 
		//	$("#orgTypeName").val();
			$("#OrgPartCodeE").val(result.OrgLevPartID);
			$("#ProvinceE").val(result.LocCode);
			*/

            org.LoadOrgPartSel(result.OrgLevPartID, 'OrgPartCodeE');
            org.LoadProvinceSel(result.LocCode, 'ProvinceE');

            org.LoadOrgPartSel(result.OrgLevPartID, 'OrgPartCodeC');
            org.LoadProvinceSel(result.LocCode, 'Province');


        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts org.LoadOrgAFDetail');
            console.log(xhr);
            duck.NotiDanger();
        }
    });

};

org.LoadOrg1SelByOrgTypeID=function(selfDID, OrgTypeID, element_id) {


    //LoadLevel1ByOrgType
    var DataSet = {
        OrgType: OrgTypeID,
    };
    console.log(DataSet);

    var v_val = "";

    $.ajax({
        url: org.url + '?mode=LoadLevel1ByOrgType',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(data, status, xhr) {
            console.log(data);

            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกหน่วยงาน").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.OrgLevDID == v_val) {
                    $(".select2-chosen").text(result.OrgLevName);
                    $('<option>').attr('value', result.OrgLevDID).attr('selected', 'selected').text(result.OrgLevName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.OrgLevDID).text(result.OrgLevName).appendTo('#' + element_id);
                }
            });


        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadOrgStrucByOrgType');
            console.log(xhr);
            duck.NotiDanger();
        }
    });

};

org.LoadOrg2SelByOrgTypeID=function(selfDID, OrgTypeID, element_id) {


    var DataSet = {
        OrgType: OrgTypeID,
    };
    console.log(DataSet);

    var v_val = selfDID;

    $.ajax({
        url: org.url + '?mode=LoadLevel2ByOrgType',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(data, status, xhr) {
            console.log(data);

            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกสังกัด").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.OrgLevDID == v_val) {
                    $(".select2-chosen").text(result.OrgLevAbbrName);
                    $('<option>').attr('value', result.OrgLevDID).attr('selected', 'selected').text(result.OrgLevAbbrName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.OrgLevDID).text(result.OrgLevAbbrName).appendTo('#' + element_id);
                }
            });


        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadOrg2SelByOrgTypeID');
            console.log(xhr);
            duck.NotiDanger();
        }
    });

};


// org.LoadDataOrgParent
org.LoadDataOrgParent=function(orgLevDID) {

    console.log(orgLevDID);
    if (orgLevDID) {
        var orgLevCode = orgLevDID.substr(0, 2);
        var orgLev = orgLevDID.substr(1, 1);
        //orgLev
    } else {
        return false;
    }

    var orgLev;
    var orgLevDID;


    var DataSet = {
        orgLev: orgLev,
        orgLevDID: orgLevDID,
    };
    //console.log(DataSet);

    var dataOrg;
    $.ajax({
        url: org.url + '?mode=LoadDataOrgParent',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);

            var orgName = '';
            var orgNameAbbr = '';
            var orgNameAbbrSemi = '';

            if(result.unitM){
                var unitM = result.unitM;
                var x = [];

                var y = parseInt(orgLev) - 2;
                //console.log(y); 
                for (var i in unitM) {
                    //	console.log(i + ":"+ unitM[i].OrgLevelDID );

                    x[y] = unitM[i].OrgLevelDID;
                    if (unitM[i].detail) {
                        
                        
 
                        if (unitM[i].detail.OrgLev >= 35) {
                            if(unitM[i].detail.OrgLevAbbrName == null){
                                orgNameAbbrSemi =   orgNameAbbr; 
                            }else{
                               orgNameAbbrSemi = unitM[i].detail.OrgLevName + " " + orgNameAbbr;
                            }
                            
                        } else { 
                            if(unitM[i].detail.OrgLevAbbrName == null){
                                orgNameAbbrSemi =   orgNameAbbr;
                               
                            }else{
                                orgNameAbbrSemi = unitM[i].detail.OrgLevAbbrName + "" + orgNameAbbr;
                            }
                           
                        }

                        orgName = unitM[i].detail.OrgLevName + " " + orgName;
                        if(unitM[i].detail.OrgLevAbbrName == null){ 
                            orgNameAbbr =  orgNameAbbr;
                        }else{ 
                            orgNameAbbr = unitM[i].detail.OrgLevAbbrName + "" + orgNameAbbr;
                        }
                      
                    }
                    y--;
                }
            }
            // console.log(orgName);  
            // console.log(orgNameAbbr);  
            //  console.log(orgNameAbbrSemi);  

            $('.inp_orgName').val(orgName);
            $('.inp_orgNameAbbr').val(orgNameAbbr);
            $('.inp_orgNameAbbrSemi').val(orgNameAbbrSemi);

            $('.box_inp_orgName').text(orgName);
            $('.box_inp_orgNameAbbr').text(orgNameAbbr);
            $('.box_inp_orgNameAbbrSemi').text(orgNameAbbrSemi);

            var dataOrg = [orgName, orgNameAbbr, orgNameAbbrSemi];
            console.log(dataOrg);
            // return dataOrg ;
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts org.LoadDataOrgParent');
            console.log(xhr);
            duck.NotiDanger();
        }
    });



};
 



////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////                 ////////////////////////////////////////
///////////////////////////////////                 ////////////////////////////////////////
///////////////////////////////////                 ////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////



///////////////////    OrgSubTable : ชื่อหน่วยงาน  /////////////////

org.LoadOrgSubUnit=function() {

    var DataSet = {
        table: 'OrgSubUnit',
        where: {
            OrgLevelID: $("#OrgLevelID").val(),
            OrgSubUnitDelete: '0',
        },
        orderby: "",
        limit: ""
    };
    console.log(DataSet);

    $.ajax({
        //url:org.url+'?mode=LoadAllData',
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
            if ($('#dataTableSet').val() == 1) {
                $('#PosTable').DataTable().clear().draw();
            }
        },
        success: function(result, status, xhr) {

            $('#PosTable').DataTable().destroy();

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';

                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:org.SetFrom(\'EDIT\',\'' + value1.OrgSubUnitID + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['OrgSubUnitID'] + '"></i></a>\
							<a href="javascript:org.callModalDeleteFixField(\'OrgSubUnit\',\'' + value1['OrgSubUnitID'] + '\'\,\'ชื่อหน่วยงาน\',\'OrgSubUnitID\',\'OrgSubUnitDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1['OrgSubUnitID'] + '</td>\
						<td class="text-left">' + value1['OrgSubUnit'] + '</td>\
						<td class="text-left">' + value1['OrgSubUnit'] + '</td>\
						<td class="text-center">' + org.SetActive(value1['OrgSubUnitActive']) + '</td>\
					</tr>\
					';
                    //	<a href="javascript:org.callModalDelete('OrgGroupType','<?php echo $value['id'];?>',' <?php echo $menuname;?> ','militaryrank_data.php');" class="dropdown-item "><i class="ft-trash-2"></i> ลบ </a>

                    $('#OrgSubUnitTable> tbody').append(contents);
                });

                $('#dataTableSet').val('1');
            }
        },
        complete: function() {
            $('#OrgSubUnitTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadOrgSubUnit');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};


org.GetOrgSubUnit=function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'OrgSubUnit',
        where: {
            OrgSubUnitID: dataid,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: org.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);


            if (result) {
                $("#OrgSubUnit").val(result.OrgSubUnit);
                $("#OrgSubUnitAbbr").val(result.OrgSubUnitAbbr);
                $("#OrgSubUnitSemiAbbr").val(result.OrgSubUnitSemiAbbr);



                if (result.OrgSubUnitActive == 0) {
                    $("#OrgSubUnitActive").bootstrapToggle('off');
                } else {
                    $("#OrgSubUnitActive").bootstrapToggle('on');
                }

                org.LoadOrgLevelSel(result.OrgLevelID, 'OrglevelID');
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetOrgSubUnit');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

org.EditOrgSubUnit=function() {

    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();
    var active = 0;


    var checkBox = document.getElementById("OrgSubUnitActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }

    var exDat = {
        table: "OrgSubUnit",
        data: {
            OrgSubUnit: $("#OrgSubUnit").val(),
            OrgSubUnitAbbr: $("#OrgSubUnitAbbr").val(),
            OrgSubUnitSemiAbbr: $("#OrgSubUnitSemiAbbr").val(),
            OrglevelID: $("#OrglevelID").val(),
            OrgSubUnitActive: active,
            OrgSubUnitUpdateBy: $("#OrgSubUnitUpdateBy").val(),
            OrgSubUnitUpdateDate: $("#OrgSubUnitUpdateDate").val()
        },
        where: {
            OrgSubUnitID: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: org.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                //var link = "index.php";
                var link = "edit.php?dataid=" + dataid;
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

org.CreateOrgUnit=function() {
    $('#modal_createcf').modal('hide');

    var dataid = $("#dataid").val();
    var active = 0;


    var checkBox = document.getElementById("OrgSubUnitActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }

    var exDat = {
        table: "OrgSubUnit",
        data: {
            OrgSubUnit: $("#OrgSubUnit").val(),
            OrgSubUnitAbbr: $("#OrgSubUnitAbbr").val(),
            OrgSubUnitSemiAbbr: $("#OrgSubUnitSemiAbbr").val(),
            OrglevelID: $("#OrglevelID").val(),
            OrgSubUnitActive: active,
            OrgSubUnitDelete: 0,
            OrgSubUnitCreateBy: $("#OrgSubUnitUpdateBy").val(),
            OrgSubUnitCreateDate: $("#OrgSubUnitUpdateDate").val(),
            OrgSubUnitUpdateBy: $("#OrgSubUnitUpdateBy").val(),
            OrgSubUnitUpdateDate: $("#OrgSubUnitUpdateDate").val()
        }
    };

    console.log(exDat)

    $.ajax({
        url: org.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();

            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + data.code;
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }


        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

org.LoadOrgSubUnitSel=function(v_val, element_id, orglevelID) {
    if (v_val) {

    }
    var exDat = {
        table: "OrgSubUnit",
        where: {
            OrgSubUnitActive: 1,
            OrgSubUnitDelete: 0,
            orglevelID: orglevelID
        },
        orderby: " OrgSubUnit    ASC ",
        limit: "",
    };

    $.ajax({
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        success: function(data) {
            //console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกชื่อหน่วยงาน").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.OrgSubUnitID == v_val) {
                    $(".select2-chosen").text(result.OrgSubUnit);
                    $('<option>').attr('value', result.OrgSubUnitID).attr('data-pname', result.OrgSubUnit).attr('data-abbrname', result.OrgSubUnitAbbr).attr('data-semiabbrname', result.OrgSubUnitSemiAbbr).attr('selected', 'selected').text(result.OrgSubUnit).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.OrgSubUnitID).attr('data-pname', result.OrgSubUnit).attr('data-abbrname', result.OrgSubUnitAbbr).attr('data-semiabbrname', result.OrgSubUnitSemiAbbr).text(result.OrgSubUnit).appendTo('#' + element_id);
                }
            });

        },
        error: function(data) {
            console.log(data);
            console.log('check LoadOrgSubUnitSel');
            duck.NotiDanger();
        }
    });
};

///////////////////    PersonType : ประเภทกำลังพล  /////////////////

org.LoadPersonType=function() {
    var DataSet = {
        table: 'HrtPersonType',
        where: {
            HrtPersonTypeDelete: '0',
        },
        orderby: "",
        limit: ""
    };
    console.log(DataSet);

    $.ajax({
        //url:org.url+'?mode=LoadAllData',
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';

                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:org.SetFrom(\'EDIT\',\'' + value1.HrtPersonTypeID + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['HrtPersonTypeID'] + '"></i></a>\
							<a href="javascript:org.callModalDeleteFixField(\'HrtPersonType\',\'' + value1['HrtPersonTypeID'] + '\'\,\'ประเภทกำลังพล\',\'HrtPersonTypeID\',\'HrtPersonTypeDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1['HrtPersonTypeID'] + '</td>\
						<td class="text-left">' + value1['HrtPersonTypeName'] + '</td>\
						<td class="text-center">' + org.SetActive(value1['HrtPersonTypeActive']) + '</td>\
					</tr>\
					';
                    //	<a href="javascript:org.callModalDelete('OrgGroupType','<?php echo $value['id'];?>',' <?php echo $menuname;?> ','militaryrank_data.php');" class="dropdown-item "><i class="ft-trash-2"></i> ลบ </a>

                    $('#PersonType> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#PersonType').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadPersonType');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

org.GetNamePersonType=function() {


};

org.LoadPersonTypeSel=function(v_val, element_id) {
    if (v_val) {

    }
    var exDat = {
        table: "HrtPersonType",
        where: {
            HrtPersonTypeActive: 1,
            HrtPersonTypeDelete: 0
        },
        orderby: " HrtPersonTypeID   ASC ",
        limit: "",
    };

    $.ajax({
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        success: function(data) {
            //console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกประเภทกำลังพล").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.HrtPersonTypeID == v_val) {
                    $(".select2-chosen").text(result.HrtPersonTypeName);
                    $('<option>').attr('value', result.HrtPersonTypeID).attr('selected', 'selected').text(result.HrtPersonTypeName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.HrtPersonTypeID).text(result.HrtPersonTypeName).appendTo('#' + element_id);
                }
            });

        },
        error: function(data) {
            console.log(data);
            console.log('check LoadPersonTypeSel');
            duck.NotiDanger();
        }
    });
};

///////////////////    ChiefSpc : สายวิทยาการ  /////////////////

org.LoadChiefSpc=function() {
    var DataSet = {
        table: 'HrtChiefSpc',
        where: {
            HrtChiefSpcDelete: '0',
        },
        orderby: "",
        limit: ""
    };
    console.log(DataSet);

    $.ajax({
        //url:org.url+'?mode=LoadAllData',
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';


                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:org.SetFrom(\'EDIT\',\'' + value1.HrtChiefSpcID + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['HrtChiefSpcID'] + '"></i></a>\
							<a href="javascript:org.callModalDeleteFixField(\'HrtChiefSpc\',\'' + value1['HrtChiefSpcID'] + '\'\,\'สายวิทยาการ\',\'HrtChiefSpcID\',\'HrtChiefSpcDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1['HrtChiefSpcID'] + '</td>\
						<td class="text-center">' + value1['HrtPersonTypeID'] + '</td>\
						<td class="text-left">' + value1['HrtChiefSpcName'] + '</td>\
						<td class="text-left">' + value1['HrtChiefSpcNameAbbr'] + '</td>\
						<td class="text-center">' + org.SetActive(value1['HrtChiefSpcActive']) + '</td>\
					</tr>\
					';


                    $('#ChiefSpcTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#ChiefSpcTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadChiefSpcTable');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

org.LoadChiefSpcSel=function(v_val, element_id, personType) {
    if (v_val) {

    }
    if(!personType){
        personType = 1 ;
    }


    var exDat = {
        table: "HrtChiefSpc",
        where: {
            ChiefSpcActive: 1,
            ChiefSpcDelete: 0,
            PersonTypeID: personType
        },
        orderby: "ChiefSpcName ",
        limit: "",
    };
    console.log(exDat);
    $.ajax({
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(data) {
            console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกสายวิทยาการ").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.ChiefSpcID == v_val) {
                    $(".select2-chosen").text(result.ChiefSpcID + ": " + result.ChiefSpcName);
                    $('<option>').attr('value', result.ChiefSpcID).attr('selected', 'selected').text(result.ChiefSpcID + ": " + result.ChiefSpcName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.ChiefSpcID).text(result.ChiefSpcID + ": " + result.ChiefSpcName).appendTo('#' + element_id);
                }
            });
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            console.log('check LoadChiefSpcSel');
            duck.NotiDanger();
        }
    });
};
///////////////////    HrtPosTypeGroup : กลุ่มประเภทตำแหน่ง  /////////////////
org.LoadPosTypeGroupSel=function(v_val, element_id, ref_val) {
    if (v_val) {

    }
    var HrtPersonTypeID = "";
    if (ref_val) {
        HrtPersonTypeID = ref_val;
    }
    var exDat = {
        table: "HrtPosTypeGroup",
        where: {
            HrtPosTypeGroupActive: 1,
            HrtPosTypeGroupDelete: 0,
            HrtPersonTypeID: HrtPersonTypeID
        },
        orderby: " HrtPosTypeGroupName   ASC ",
        limit: "",
    };
    console.log(v_val);
    console.log(ref_val);
    $.ajax({
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        success: function(data) {
            //console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกกลุ่มประเภทตำแหน่ง").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.HrtPosTypeGroupID == v_val) {
                    $(".select2-chosen").text(result.HrtPosTypeGroupName);
                    $('<option>').attr('value', result.HrtPosTypeGroupID).attr('selected', 'selected').text(result.HrtPosTypeGroupName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.HrtPosTypeGroupID).text(result.HrtPosTypeGroupName).appendTo('#' + element_id);
                }
            });
        },
        error: function(data) {
            console.log(data);
            console.log('check LoadPosTypeGroupSel');
            duck.NotiDanger();
        }
    });
};
///////////////////    HrtPosType : ประเภทตำแหน่ง  /////////////////
org.LoadPosTypeSel=function(v_val, element_id, ref_val) {
    if (v_val) {

    }
    var HrtPosTypeGroupID = "";
    if (ref_val) {
        HrtPosTypeGroupID = ref_val;
    }
    var exDat = {
        table: "HrtPosType",
        where: {
            PosTypeActive: 1,
            PosTypeDelete: 0,
            PosGrpID: HrtPosTypeGroupID
        },
        orderby: " PosTypeName   ASC ",
        limit: "",
    };

    $.ajax({
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        success: function(data) {
            //console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("ประเภทตำแหน่ง").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.PosTypeID == v_val) {
                    $(".select2-chosen").text(result.PosTypeName);
                    $('<option>').attr('value', result.PosTypeID).attr('selected', 'selected').text(result.PosTypeName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.PosTypeID).text(result.PosTypeName).appendTo('#' + element_id);
                }
            });
        },
        error: function(data) {
            console.log(data);
            console.log('check LoadPosTypeSel');
            duck.NotiDanger();
        }
    });
};

org.LoadPosTypeSel2=function(v_val, element_id, ref_val) {
    if (v_val) {

    }
    var HrtPosTypeGroupID = "";
    if (ref_val) {
        HrtPosTypeGroupID = ref_val;
    }
    var exDat = {
        table: "HrtPosType",
        where: {
            HrtPosTypeActive: 1,
            HrtPosTypeDelete: 0,
            HrtPosTypeGroupID: HrtPosTypeGroupID
        },
        orderby: " HrtPosTypeName   ASC ",
        limit: "",
    };

    $.ajax({
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        success: function(data) {
            //console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("ประเภทตำแหน่ง").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.HrtPosTypeID == v_val) {
                    $(".select2-chosen").text(result.HrtPosTypeName);
                    $('<option>').attr('value', result.HrtPosTypeID).attr('selected', 'selected').text(result.HrtPosTypeName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.HrtPosTypeID).text(result.HrtPosTypeName).appendTo('#' + element_id);
                }
            });
        },
        error: function(data) {
            console.log(data);
            console.log('check LoadPosTypeSel');
            duck.NotiDanger();
        }
    });
};

///////////////////    Pos : ชื่อตำแหน่ง  /////////////////
org.LoadPos=function() {
    var orgTypeID = "";
    var personType = "";

    if ($("#OrgTypeID").val()) {
        orgTypeID = $("#OrgTypeID").val();
    }
    if ($("#PersonTypeID").val()) {
        personType = $("#PersonTypeID").val();
    }

    //orgTypeID

    var DataSet = {
        table: 'HrtPos',
        field: {
            HrtPosCode: '',
            HrtPosName: '',
            HrtPosAbbrName: '',
            HrtPersonTypeID: '',
            HrtPosCodeActive: ''
        },
        where: {
            HrtPosCodeDelete: 0,
            HrtPersonTypeID: personType

        },
        orderby: "",
        limit: ""
    };
    console.log(DataSet);

    $.ajax({
        //url: org.url+'?mode=LoadAllData', 
        url: org.url + '?mode=LoadFieldData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
            if ($('#dataTableSet').val() == 1) {
                $('#PosTable').DataTable().clear().draw();
            }
        },
        success: function(result, status, xhr) {

            $('#PosTable').DataTable().destroy();

            console.log(result);
            if (result) {
                $.each(result, function(key1, value1) {
                    var contents = '';

                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:org.SetFrom(\'EDIT\',\'' + value1.HrtPosCode + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['HrtPosCode'] + '"></i></a>\
							<a href="javascript:org.callModalDeleteFixField(\'HrtPos\',\'' + value1['HrtPosCode'] + '\'\,\'ตำแหน่ง\',\'HrtPosCode\',\'HrtPosCodeDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1.HrtPosCode + '</td>\
						<td>' + value1.HrtPosName + '</td>\
						<td>' + value1.HrtPosAbbrName + '</td>\
						<td class="text-center">' + org.SetActive(value1['HrtPosCodeActive']) + '</td>\
					</tr>\
					';
                    //<td class="text-center"> '+value1.HrtPersonTypeID+'</td>\							
                    $('#PosTable> tbody').append(contents);
                });

                $('#dataTableSet').val('1');
            }
        },
        complete: function() {
            $('#PosTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadPosTable');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

org.GetPos=function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'HrtPos',
        where: {
            HrtPosCode: dataid,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: org.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);

            if (result) {
                org.LoadOrgTypeSel(result.OrgTypeID, 'OrgTypeID');
                org.LoadPersonTypeSel(result.HrtPersonTypeID, 'HrtPersonTypeID');
                org.LoadPosTypeGroupSel(result.HrtPosGrpID, 'HrtPosTypeGroup', result.HrtPersonTypeID); //HrtPosTypeGroup
                org.LoadPosTypeSel(result.HrtPosTypeID, 'HrtPosType', result.HrtPosGrpID);
                org.LoadPosHeadStatusSel(result.HrtPosHeadSts, 'HrtPosHeadStatus')
                if (result.HrtTypeOfPos == 1) {
                    $('#HrtTypeOfPos').trigger('click');
                }
                if (result.HrtFlagNotDisp == 1) {
                    $('#HrtFlagNotDisp').trigger('click');
                }
                $("#HrtPosCode").val(result.HrtPosCode);
                $("#HrtPosName").val(result.HrtPosName);
                $("#HrtPosAbbrName").val(result.HrtPosAbbrName);
                if (result.HrtPosCodeActive == 0) {
                    $("#HrtPosCodeActive").bootstrapToggle('off');
                } else {
                    $("#HrtPosCodeActive").bootstrapToggle('on');
                }
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetPos');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

org.EditPos=function() {
    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();
    var active = 0;

    var checkBox = document.getElementById("HrtPosCodeActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }
    var HrtTypeOfPos = 0;
    if ($("#HrtTypeOfPos").is(':checked') == true) {
        HrtTypeOfPos = 1;
    }
    var HrtFlagNotDisp = 0;
    if ($("#HrtFlagNotDisp").is(':checked') == true) {
        HrtFlagNotDisp = 1;
    }

    var exDat = {
        table: "HrtPos",
        data: {
            OrgTypeID: $("#OrgTypeID").val(),
            HrtPersonTypeID: $("#HrtPersonTypeID").val(),
            HrtPosName: $("#HrtPosName").val(),
            HrtPosAbbrName: $("#HrtPosAbbrName").val(),
            HrtPosGrpID: $("#HrtPosTypeGroup").val(),
            HrtPosTypeID: $("#HrtPosType").val(),
            HrtPosHeadSts: $("#HrtPosHeadStatus").val(),
            HrtTypeOfPos: HrtTypeOfPos,
            HrtFlagNotDisp: HrtFlagNotDisp,
            HrtPosCodeActive: active,
            HrtPosCodeUpdateBy: $("#HrtPosCodeUpdateBy").val(),
            HrtPosCodeUpdateDate: $("#HrtPosCodeUpdateDate").val()
        },
        where: {
            HrtPosCode: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: org.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + dataid;
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

org.CreatePos=function() {
    $('#modal_createcf').modal('hide');


    var active = 0;


    var checkBox = document.getElementById("HrtPosCodeActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }
    var HrtTypeOfPos = 0;
    if ($("#HrtTypeOfPos").is(':checked') == true) {
        HrtTypeOfPos = 1;
    }
    var HrtFlagNotDisp = 0;
    if ($("#HrtFlagNotDisp").is(':checked') == true) {
        HrtFlagNotDisp = 1;
    }
    var exDat = {
        table: "HrtPos",
        data: {
            OrgTypeID: $("#OrgTypeID").val(),
            HrtPersonTypeID: $("#HrtPersonTypeID").val(),
            HrtPosName: $("#HrtPosName").val(),
            HrtPosAbbrName: $("#HrtPosAbbrName").val(),
            HrtPosGrpID: $("#HrtPosTypeGroup").val(),
            HrtPosTypeID: $("#HrtPosType").val(),
            HrtPosHeadSts: $("#HrtPosHeadStatus").val(),
            HrtTypeOfPos: HrtTypeOfPos,
            HrtFlagNotDisp: HrtFlagNotDisp,
            HrtPosCodeActive: active,
            HrtPosCodeDelete: 0,
            HrtPosCodeCreateBy: $("#HrtPosCodeCreateBy").val(),
            HrtPosCodeCreateDate: $("#HrtPosCodeCreateDate").val(),
            HrtPosCodeUpdateBy: $("#HrtPosCodeCreateBy").val(),
            HrtPosCodeUpdateDate: $("#HrtPosCodeCreateDate").val()
        }
    };

    console.log(exDat)

    $.ajax({
        url: org.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            // if( data.success == "COMPLETE"){
            // 	var link = "index.php";
            // 	duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ'); 
            // 	setTimeout( duck.OpenPage   , 2000 , link ,'_self' );
            // }else{
            // 	duck.NotiWarning();
            // }

            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + data.code;
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }


        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

org.LoadPosCodeSel=function(v_val, element_id, orgType, personType) {
    if (v_val) {

    }

    var exDat = {
        table: "HrtPos",
        where: {
            HrtPosCodeActive: 1,
            HrtPosCodeDelete: 0,
            HrtPersonTypeID: personType
        },
        orderby: " HrtPosName   ASC ",
        limit: "",
    };
    console.log(exDat);
    $.ajax({
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(data) {
            console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกชื่อตำแหน่ง").appendTo('#' + element_id);
            $.each(data, function(i, result) {

                if (result.HrtPosCode == v_val) {
                    $(".select2-chosen").text(result.HrtPosName);
                    $('<option>').attr('value', result.HrtPosCode).attr('data-pname', result.HrtPosName).attr('data-abbrname', result.HrtPosAbbrName).attr('data-postype', result.HrtPosTypeID).attr('data-grpid', result.HrtPosGrpID).attr('selected', 'selected').text(result.HrtPosName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.HrtPosCode).attr('data-pname', result.HrtPosName).attr('data-abbrname', result.HrtPosAbbrName).attr('data-postype', result.HrtPosTypeID).attr('data-grpid', result.HrtPosGrpID).text(result.HrtPosName).appendTo('#' + element_id);
                }
            });
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            console.log('check LoadPosCodeSel');
            duck.NotiDanger();
        }
    });
};

///////////////////    PosHeadStatus :  ระดับชั้นตำแหน่ง  /////////////////

org.LoadPosHeadStatusSel=function(v_val, element_id) {
    if (v_val) {

    }

    var exDat = {
        table: "HrtPosHeadStatus",
        where: {
            PosHeadStatusActive: 1,
            PosHeadStatusDelete: 0
        },
        orderby: "",
        limit: "",
    };
    console.log(exDat);
    $.ajax({
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(data) {
            console.log(data);
            $('#' + element_id).find('option').remove()
                // $('<option>').attr('value','').text("กรุณาเลือกระดับชั้นตำแหน่ง").appendTo('#'+element_id);
            $('<option>').attr('value', '').text(" ===== ไม่ระบุ =====").appendTo('#' + element_id);
            $.each(data, function(i, result) {

                if (result.HrtPosHeadStatusID == v_val) {
                    $(".select2-chosen").text(result.PosHeadStatusName);
                    $('<option>').attr('value', result.HrtPosHeadStatusID).attr('selected', 'selected').text(result.PosHeadStatusName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.HrtPosHeadStatusID).text(result.PosHeadStatusName).appendTo('#' + element_id);
                }
            });
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            console.log('check LoadPosCodeSel');
            duck.NotiDanger();
        }
    });
};
///////////////////////////////////////////////////////////////////////////////////////////
/////////////////////   Position : กรอบอัตรากำลังพล  /////////////////

org.LoadPositionByOrgLevelDID=function(OrgLevelDID) {
    console.log(OrgLevelDID);

    
    $("#OrgLevelDIDtxt").val(OrgLevelDID);
    var OrgLevel2 = $("#OrgLevel2x").val();

    var DataSet = {
        OrgLevelDID: OrgLevelDID
    };


    console.log(DataSet);

    $.ajax({
        //	url: org.url+'?mode=LoadAllData', 
        //	url: org.url+'?mode=LoadFieldData',
        url: org.url + '?mode=LoadPositionByOrgLevelx',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
            if ($('#dataTableSet').val() == 1) {
                $('#PositionTable').DataTable().clear().draw();
            }
        },
        success: function(result, status, xhr) {

            $('#PositionTable').DataTable().destroy();

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var RankName = "-";
                    var PosPrintSeq = "-";
                    var CancleCommDocDate = "-";
                    if (value1.RankName != null) {
                        RankName = value1.RankName;
                    }
                    if (value1.HrtPosPrintSeq != null) {
                        PosPrintSeq = value1.HrtPosPrintSeq; //HrtPosPrintSeq
                    }
                    if (value1.CancleCommDocDate != null) {
                        CancleCommDocDate = value1.CancleCommDocDate;
                    }
                    var contents = '';
                    //	<a href="javascript:org.SetFrom(\'EDIT\',\'' + value1.HrtPosCode + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['HrtPosCode'] + '"></i></a>\
						
                    contents += '\
					<tr>\
                        <td class="text-center">\
                            <a href="edit.php?position=' + value1.HrtPositionID + '&OrgLevelDID='+OrgLevelDID+'&OrgLevel2='+OrgLevel2+'"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['HrtPosCode'] + '"></i></a>\
						    <a href="javascript:org.callModalDeleteFixField(\'HrtPos\',\'' + value1['HrtPosCode'] + '\'\,\'ตำแหน่ง\',\'HrtPosCode\',\'HrtPosCodeDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-left">' + value1.HrtPosName + '<span class="box_inp_orgNameAbbr"></span> </td>\
						<td class="text-center">' + RankName + '</td>\
						<td class="text-center">' + value1.HrtPosNum + '</td>\
						<td class="text-center">' + PosPrintSeq + '</td>\
						<td class="text-center">' + org.SetActive(value1['HrtPositionActive']) + '</td>\
						<td class="text-center">' + CancleCommDocDate + '</td>\
					</tr>\
					';

                    $('#PositionTable> tbody').append(contents);
                });

                $('#dataTableSet').val('1');

            }
        },
        complete: function() {
            // $('#PositionTable').DataTable({
            //     "paging": true,
            //     "lengthChange": true,
            //     "ordering": true,
            //     "info": true,
            //     "searching": true
            // });


            $("#modalLoading").hide();
            org.LoadDataOrgParent(OrgLevelDID);

        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadPositionByOrgLevelDID');
            console.log(xhr);
            duck.NotiDanger();
        }
    });

};

org.CallPositionByOrgLevelDID=function(OrgLevelDID) {
    console.log("CallPositionByOrgLevelDID:: " + OrgLevelDID);
};

org.CreatePosition=function() {
    $('#modal_createcf').modal('hide');

    var active = 0;
    var checkBox = document.getElementById("HrtPositionActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }
   

    var CommandActiveDateC = null;
    var CommandActiveEFFDateC = null;
    var CommandCancleDateC = null;
    var CommandActiveEFFDateC = null;


    if ($("input[name=CommandActiveDateC_submit]").val()) {
        var CommandActiveDateC = $("input[name=CommandActiveDateC_submit]").val();
    } else {
        var CommandActiveDateC = 'NULL';
    }
    if ($("input[name=CommandActiveEFFDateC_submit]").val()) {
        var CommandActiveEFFDateC = $("input[name=CommandActiveEFFDateC_submit]").val();
    } else {
        var CommandActiveEFFDateC = 'NULL';
    }
    if ($("input[name=CommandCancleDateC_submit]").val()) {
        var CommandCancleDateC = $("input[name=CommandCancleDateC_submit]").val();
    } else {
        var CommandCancleDateC = 'NULL';
    }
    if ($("input[name=CommandCancleEFFDateC_submit]").val()) {
        var CommandCancleEFFDateC = $("input[name=CommandCancleEFFDateC_submit]").val();
    } else {
        var CommandCancleEFFDateC = 'NULL';
    }


    /* 
	
	[ID]
      ,[PositionID]
      ,[PersonTypeID]
      ,[PosNo]
      ,[OrgTypeID]
      ,[PosCode]
      ,[PosTypeID]
      ,[PosGrpID]
      ,[RankID]
      ,[OrgID]
      ,[OrgSeq]
      ,[PersonID]
      ,[PosStatus]
      ,[SalaryAmt]
      ,[PosBlankDate]
      ,[RecStatus]
      ,[PosPersonActive]
      ,[PosPersonDelete]
      ,[PosDateCur]
      ,[CommPosCur]
      ,[CommDateCur]
      ,[CreateUserID]
      ,[CreateDate]
      ,[UpdateUserID]
      ,[updateDate]
      ,[PosNumber]
      ,[PosDateExp]
      ,[PosPerStatus]
      ,[RankEnd]
      ,[PosPrintSeq]
      ,[UpdateDate2]
      ,[SourceOfCommID]
      ,[CommLevelID]
      ,[CommDocNo]
      ,[CommDocDate]
      ,[CommEffDate]
      ,[AirforceID]
      ,[ReservePersonID]
      ,[ReserveAirforceID]
      ,[IsReservedByPlan]
      ,[BudgetYearPlan]
      ,[ApprovReservedDate]
      ,[IsMoveByPsr]
      ,[SourceOfCommIDPsr]
      ,[CommDocNoPsr]
      ,[CommDocDatePsr]
      ,[CommItemNo]
      ,[ReqQualifyCode]
      ,[CommDocNoPlan]
      ,[CommEffDatePlan]
      ,[CommItemNoPsr]
      ,[PosPerStatusPsr]
      ,[CommEffDatePsr]
      ,[IsBooking]
      ,[BookingPersonID]
      ,[BookingSystem]
      ,[Option1]
      ,[Option2]
      ,[Option3]
      ,[Option4]
      ,[Option5]
      ,[BookingDate]
      ,[DirectpayCode]
	*/



    var exDat = {
        table: "HrtPosition",
        data: {
            OrgTypeID: $("#OrgTypeID").val(),
            //HrtPersonTypeID 	: $("#HrtPersonTypeID").val() ,
            OrgLevelDID: $("#OrgLevelDID").val(),
            //	HrtPersonTypeID 	: $("#HrtPersonTypeID").val() ,
            HrtPosCode: $("#PosID").val(),
            HrtRankID: $("#RankFixID").val(),
            HrtPosName: $("#PosIDName").val(), 
            HrtPosAbbrName : $("#PosNameAbbr").val(), 

            HrtPosGrpID : $("#HrtPosGrpID").val(), 
            HrtPosTypeID : $("#HrtPosTypeID").val(), 
            HrtChiefSpcID1: $("#HrtChiefSpc1").val(),
            HrtChiefSpcID2: $("#HrtChiefSpc2").val(),
            HrtPosPrintSeq: $("#HrtPosPrintSeq").val(),
            HrtPosNum: $("#PositionNum").val(),

            // HrtPosAbbrName :  $("#HrtPosAbbrName").val() ,  
            // HrtPosGrpID :  $("#HrtPosTypeGroup").val() ,
            // HrtPosTypeID : $("#HrtPosType").val() , $("input[name=OrgTypeStartDate_submit]").val()
            SourceOfCommID: $("#CommandActiveC").val(),
            CommLevelID: '',
            CommDocNo: $("#CommandActiveCodeC").val(),

            CommDocDate: CommandActiveDateC,
            CommEffDate: CommandActiveEFFDateC,
            PosSpoudeSTS: '',
            IsChiefOrMgt: '',
            CancleSourceOfCommID: $("#CommandCancleC").val(),
            CancleCommLevelID: '',
            CancleCommDocNo: $("#CommandCancleDateC").val(),
            CancleCommDocDate: CommandCancleDateC,
            CancleCommEffDate: CommandCancleEFFDateC,
            // CommandActiveCodeC CommandActiveDateC CommandActiveEFFDateC  CommandCancleCodeC 
            // CommandCancleDateC CommandCancleEFFDateC CommandCancleEFFDateC


            HrtPositionActive: active,
            HrtPositionDelete: 0,
            HrtPositionCreateBy: $("#HrtPositionCreateBy").val(),
            HrtPositionCreateDate: $("#HrtPositionCreateDate").val(),
            HrtPositionUpdateBy: $("#HrtPositionCreateBy").val(),
            HrtPositionUpdateDate: $("#HrtPositionCreateDate").val()
        },
        posnum: $("#PositionNum").val(), // **
        data_p: {
            PositionID: $("#PositionCode").val(),
            PersonTypeID: $('#HrtPersonTypeID').val(),
            OrgTypeID: $("#OrgTypeID").val(),
            PosCode: $("#PosID").val(),
            PosTypeID:  $("#HrtPosGrpID").val(),
            PosGrpID:  $("#HrtPosTypeID").val(),
            RankID: $("#RankFixID").val(),
            RecStatus: 'A',
            PosPersonActive: active,
            PosPersonDelete: 0,
            Option4: '',
            DirectpayCode: '777777777777'
                // 729000001325
        
        }
    };

    console.log(exDat)

    $.ajax({
        url: org.url + '?mode=AddPositionDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);

            var OrgLevelDID = $("#OrgLevelDID").val();
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "detail.php?OrgLevel2=" + $("#OrgLevel2").val() + "&OrgTypeID=" + $("#OrgTypeID").val() + "&OrgLevelDIDSelect=" + OrgLevelDID;
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }


        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

org.EditPosition=function(){
    $('#modal_editcf').modal('hide');

    var PositionID = $("#PositionID").val() ; 

    var active = 0;
    var checkBox = document.getElementById("HrtPositionActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }
   

    var CommandActiveDateC = null;
    var CommandActiveEFFDateC = null;
    var CommandCancleDateC = null;
    var CommandActiveEFFDateC = null;


    if ($("input[name=CommandActiveDateC_submit]").val()) {
        var CommandActiveDateC = $("input[name=CommandActiveDateC_submit]").val();
    } else {
        var CommandActiveDateC = 'NULL';
    }
    if ($("input[name=CommandActiveEFFDateC_submit]").val()) {
        var CommandActiveEFFDateC = $("input[name=CommandActiveEFFDateC_submit]").val();
    } else {
        var CommandActiveEFFDateC = 'NULL';
    }
    if ($("input[name=CommandCancleDateC_submit]").val()) {
        var CommandCancleDateC = $("input[name=CommandCancleDateC_submit]").val();
    } else {
        var CommandCancleDateC = 'NULL';
    }
    if ($("input[name=CommandCancleEFFDateC_submit]").val()) {
        var CommandCancleEFFDateC = $("input[name=CommandCancleEFFDateC_submit]").val();
    } else {
        var CommandCancleEFFDateC = 'NULL';
    }
 

    var exDat = {
        table: "HrtPosition",
        data: {
            OrgTypeID: $("#OrgTypeID").val(),
            OrgLevelDID: $("#OrgLevelDID").val(),
            HrtPosCode: $("#PosID").val(),
            HrtRankID: $("#RankFixID").val(),
            HrtPosName: $("#PosIDName").val(),

            HrtPosGrpID : $("#HrtPosGrpID").val(), 
            HrtPosTypeID : $("#HrtPosTypeID").val(), 
            HrtChiefSpcID1: $("#HrtChiefSpc1").val(),
            HrtChiefSpcID2: $("#HrtChiefSpc2").val(),
            HrtPosPrintSeq: $("#HrtPosPrintSeq").val(),
         //   HrtPosNum: $("#PositionNum").val(),

            SourceOfCommID: $("#CommandActiveC").val(),
            CommLevelID: '',
            CommDocNo: $("#CommandActiveCodeC").val(),

            CommDocDate: CommandActiveDateC,
            CommEffDate: CommandActiveEFFDateC,
            PosSpoudeSTS: '',
            IsChiefOrMgt: '',
            CancleSourceOfCommID: $("#CommandCancleC").val(),
            CancleCommLevelID: '',
            CancleCommDocNo: $("#CommandCancleDateC").val(),
            CancleCommDocDate: CommandCancleDateC,
            CancleCommEffDate: CommandCancleEFFDateC,
            // CommandActiveCodeC CommandActiveDateC CommandActiveEFFDateC  CommandCancleCodeC 
            // CommandCancleDateC CommandCancleEFFDateC CommandCancleEFFDateC


            HrtPositionActive: active,
            HrtPositionDelete: 0,
            HrtPositionCreateBy: $("#HrtPositionCreateBy").val(),
            HrtPositionCreateDate: $("#HrtPositionCreateDate").val(),
            HrtPositionUpdateBy: $("#HrtPositionCreateBy").val(),
            HrtPositionUpdateDate: $("#HrtPositionCreateDate").val()
        },
        posnum: $("#PositionNum").val(), // **
        data_p: {
            PositionID: $("#PositionCode").val(),
            PersonTypeID: $('#HrtPersonTypeID').val(),
            OrgTypeID: $("#OrgTypeID").val(),
            PosCode: $("#PosID").val(),
            PosTypeID:  $("#HrtPosGrpID").val(),
            PosGrpID:  $("#HrtPosTypeID").val(),
            RankID: $("#RankFixID").val(),
            RecStatus: 'A',
            PosPersonActive: active,
            PosPersonDelete: 0,
            Option4: '',
            DirectpayCode: '777777777777'   // 729000001325 
        },
        where : { 
            HrtPositionID : PositionID
        }
    };

    console.log(exDat)

    $.ajax({
        url: org.url + '?mode=UpdatePositionDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);

            var OrgLevelDID = $("#OrgLevelDID").val();
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "detail.php?OrgLevel2=" + $("#OrgLevel2").val() + "&OrgTypeID=" + $("#OrgTypeID").val() + "&OrgLevelDIDSelect=" + OrgLevelDID;
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }


        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
/////////////////////   PosPerson : ทำเนียบกำลังพล  /////////////////
org.LoadPosPersonByOrgLevelDID=function(OrgLevelDID) {
    console.log(OrgLevelDID);

    
    $("#OrgLevelDIDtxt").val(OrgLevelDID);
    $("#OrgLevelDIDSelect").val(OrgLevelDID);
    var OrgLevel2 = $("#OrgLevel2x").val();
    var OrgTypeID = $("#OrgTypeID").val();

    var DataSet = {
        OrgLevelDID: OrgLevelDID
    };

    
    console.log(DataSet);

    $.ajax({ 
        url: org.url + '?mode=LoadPosPersonByOrgLevelx',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
            if ($('#dataTableSet').val() == 1) {
                //$('#PositionTable').DataTable().clear().draw();
              //  $('#PositionTable1').DataTable().clear().draw();
                
            }
        },
        success: function(result, status, xhr) {

            //$('#PositionTable').DataTable().destroy();
           // $('#PositionTable1').DataTable().destroy();
            $('#PositionTable1> tbody').empty();

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var RankName = "-";
                    var PosPrintSeq = "-";
                    var CancleCommDocDate = "-";
                    if (value1.RankName != null) {
                        RankName = value1.RankName;
                    }
                    if (value1.HrtPosPrintSeq != null) {
                        PosPrintSeq = value1.HrtPosPrintSeq; //HrtPosPrintSeq
                    }
                    if (value1.CancleCommDocDate != null) {
                        CancleCommDocDate = value1.CancleCommDocDate;
                    }
              

                    var contents1 = '';
                    contents1 += '\
					<tr class="bg2">\
                        <td class="text-left" colspan="5">' + value1.HrtPosName + ' <span class="box_inp_orgNameAbbrSemi"></span> </td>\
						<td class="text-center">' + value1.HrtPosNum  +':'+PosPrintSeq +'</td>\
						<td class="text-center">' +  RankName + '</td>\
						<td class="text-center" colspan="7"> </td>\
					</tr>\
					';
                    $('#PositionTable1> tbody').append(contents1);
                    
                
                    var contents2 = ''; 
                    $.each(value1.posperson, function(key2, value2) {

                         
                        var  Reserve ;
                        var  RankName ;
                        var  PName ;
                        var  SName ;
                        var  AirForceID ;
                        var PosPerStatusPrn="" ;
                        var SalDetailAbbr="" ;
                        var SubtMPay="" ;
                        var ArmAbbrName="" ;
                        var skill1="" ;
                        var skill2="" ;
                        var skill3="" ;
                        var commDate ="" ;
                        var commEff ="" ;
                      
                       // Reserve = "(";
                        if(value2.IsReservedByPlan=='Y')  Reserve  = " (แผนปี " ; 
                        if(value2.BudgetYearPlan!=null)  Reserve += value2.BudgetYearPlan +")" ; 
                        //Reserve += ")" ;
                        if(value2.HrtRankAbbrTh!=null)  RankName  = value2.HrtRankAbbrTh ; 
                        if(value2.PersonName!=null)  PName  = value2.PersonName ; 
                        if(value2.SurName!=null)  SName  = value2.SurName ;  
                        if(value2.AirForceID!=null)  AirForceID  = value2.AirForceID ; 
                        if(value2.PosPerStatusPrn!=null)  PosPerStatusPrn  = value2.PosPerStatusPrn ; 
                        if(value2.SalDetailAbbr!=null )  SalDetailAbbr  = value2.SalDetailAbbr ; 
                        if(value2.SubtMPay!=null )  SubtMPay  = value2.SubtMPay ; 
                        if(value2.ArmAbbrName!=null )  ArmAbbrName = value2.ArmAbbrName ; 
                        
                        if(value2.SkillNo01Prefix!=null)  skill1 = value2.SkillNo01Prefix ; 
                        if(value2.SkillNo01!=null)  skill1 += value2.SkillNo01 ; 
                        if(value2.SkillNo01Suffix!=null)  skill1 += value2.SkillNo01Suffix ; 
                        if(value2.SkillNo02Prefix!=null)  skill2 = value2.SkillNo02Prefix ; 
                        if(value2.SkillNo02!=null)  skill2 += value2.SkillNo02 ; 
                        if(value2.SkillNo02Suffix!=null)  skill2 += value2.SkillNo02Suffix ; 
                        if(value2.SkillNo03Prefix!=null)  skill3 = value2.SkillNo03Prefix ; 
                        if(value2.SkillNo03!=null)  skill3 += value2.SkillNo03 ; 
                        if(value2.SkillNo03Suffix!=null)  skill3 += value2.SkillNo03Suffix ; 

                        if(value2.CommEffDate!=null && value2.CommEffDate!='')  commDate   = duck.ConvertDate(value2.CommEffDate,'mini','th') ;  
                        if(value2.SourceOfCommName!=null)  commEff  = value2.SourceOfCommName ; 
                        if(value2.CommDocNo!=null)  commEff  += value2.CommDocNo ; 

                         
                        
                        if(!Reserve) Reserve = ""; 
                        if(!RankName) RankName = "-";
                        if(!PName) PName = "";
                        if(!SName) SName = "";
                        if(!AirForceID) AirForceID = "-";
                        if(!ArmAbbrName) ArmAbbrName = "-";
                        if(!skill1) skill1 = "-";
                        if(!skill2) skill2 = "-";
                        if(!skill3) skill3 = "";
                        if(!SubtMPay) SubtMPay = "-";
                        if(!SalDetailAbbr) SalDetailAbbr = "-";
                        if(!commDate) commDate = "-";
                        if(!commEff) commEff = "-";

                        var PSName = '';
                        if(Reserve=='' && PName=='' && SName =='' )  PSName = '-';
                        if(Reserve!='')  PSName = Reserve;
                        if(PName!='' )  PSName = PName+" "+SName;
                        //onclick="$(\'#modalEditPosPerson\').modal();" 
                        contents2 += '\
                        <tr class="bg3">\
                        <td class="text-center">\
                            <a href="pospersondet.php?positionID='+value2.PositionID+'&posNo='+value2.PosNo+'&OrgLevelDID='+OrgLevelDID+'&OrgLevel2='+OrgLevel2+'&OrgTypeID='+OrgTypeID+'" >\
                            <i class="la la-pencil-square-o icon_action  iconedit"  "></i> </a></td>\
                        <td class="text-center" > '+value2.DirectpayCode +'</td>\
                        <td class="text-center" > '+RankName +'</td>\
                        <td class="text-left"> '+PSName+'</td>\
                        <td class="text-center" > '+AirForceID +'</td>\
                        <td class="text-center" >  </td>\
                        <td class="text-center" > '+PosPerStatusPrn+' </td>\
                        <td class="text-center" >'+ArmAbbrName +' </td>\
                        <td class="text-center" >'+skill1 +'  </td>\
                        <td class="text-center" >'+skill2+ "<br>"+ skill3+'  </td>\
                        <td class="text-center" >'+SalDetailAbbr+'</td>\
                        <td class="text-center" >'+SubtMPay+' </td>\
                        <td class="text-center" >'+commDate+'</td>\
                        <td class="text-center" >'+commEff+'</td>\
                        </tr>\
                        ';
                    });
                      
                        
                        $('#PositionTable1> tbody').append(contents2);
                    
                });

                $('#dataTableSet').val('1');

            }
        },
        complete: function() {
            // $('#PositionTable').DataTable({
            //     "paging": true,
            //     "lengthChange": true,
            //     "ordering": true,
            //     "info": true,
            //     "searching": true
            // });


            $("#modalLoading").hide();
            org.LoadDataOrgParent(OrgLevelDID);

        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadPositionByOrgLevelDID');
            console.log(xhr);
            duck.NotiDanger();
        }
    });

};

/////////////////////   Person : ข้อมูลบุคคล  /////////////////
org.LoadPersonSel=function(v_val,element_id){
    if (v_val) {

    }

    var exDat = {
        table: "HrtPerson",
        where: { 
            PersonSts : 'Y'
        },
        orderby: "",
        limit: "",
    };
    console.log(exDat);
    $.ajax({
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(data) {
            console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.PersonID == v_val) {
                    $(".select2-chosen").text(result.PersonName+" "+result.SurName);
                    $('<option>').attr('value', result.PersonID).attr('selected', 'selected').text(result.PersonName+" "+result.SurName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.PersonID).text(result.PersonName+" "+result.SurName).appendTo('#' + element_id);
                }
            });
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            console.log('check  LoadPersonSel');
            duck.NotiDanger();
        }
    });
};

/////////////////////   PosPerStatus : สถานะการครองตำแหน่ง  /////////////////
org.LoadPosPerStatusSel=function(v_val,element_id){
    if (v_val) {

    }

    var exDat = {
        table: "HrtPosPerStatus",
        where: { 
            RecStatus : 'A'
        },
        orderby: " PosPerStatusDesc ",
        limit: "",
    };
    //console.log(exDat);
    $.ajax({
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            //$("#modalLoading").show();
        },
        success: function(data) {
            //console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกประเภทการครองตำแหน่ง").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.PosPerStatus == v_val) {
                    $(".select2-chosen").text(result.PosPerStatusDesc);
                    $('<option>').attr('value', result.PosPerStatus).attr('data-nameabbr', result.PosPerStatusPrn).attr('selected', 'selected').text(result.PosPerStatusDesc).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.PosPerStatus).attr('data-nameabbr', result.PosPerStatusPrn).text(result.PosPerStatusDesc).appendTo('#' + element_id);
                }
            });
        },
        complete: function() {
            //$("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            console.log('check  LoadPosPerStatusSel');
            duck.NotiDanger();
        }
    });
};

/////////////////////   PosStrucAddAmt : เงินเพิ่มประจำตำแหน่ง  /////////////////
org.LoadPosStrucAddAmt=function(PositionID,tb_id){
     
    var DataSet = {
        PositionID: PositionID,
        AddAmtCode: ''
    };
    //console.log(DataSet);
    
    $.ajax({ 
        url: org.url + '?mode=LoadPosStrucAddAmt',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            //$("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            $('#'+tb_id+'> tbody').empty();

            //console.log(result);
            if (result) {
                $.each(result, function(key1, value1) {
                    var contents = '';
                    

                    var PosStrucAddAmt = value1.PosStrucAddAmt ;
                    if(PosStrucAddAmt == null || PosStrucAddAmt==''){
                        PosStrucAddAmt = "";
                    }else{
                        PosStrucAddAmt = parseInt(value1.PosStrucAddAmt) ;
                        PosStrucAddAmt = PosStrucAddAmt.toLocaleString();
                    }

                    var PosStrucRewardAmt = value1.PosStrucRewardAmt ;
                    if(PosStrucRewardAmt == null || PosStrucRewardAmt=='' ){
                        PosStrucRewardAmt = "";
                    }else{
                        PosStrucRewardAmt = parseInt(value1.PosStrucRewardAmt) ;
                        PosStrucRewardAmt = PosStrucRewardAmt.toLocaleString();
                    }

                    //value1.PositionID
                    //value1.AddAmtCode
                    contents += '\
					<tr>\
						<td class="text-center">\
                            <a href="javascript:" onclick="$(\'#modalAddamt\').modal();org.GetPosStrucAddAmt(\''+value1.PositionID+'\'\,\''+value1.AddAmtCode+'\'\ );"><i class="la la-pencil-square-o icon_action  iconedit" ></i></a>\
                            <a href="javascript:org.callModalDeletePosStrucAddAmt(\''+value1.PositionID+'\'\,\''+value1.AddAmtCode+'\'\ );"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td>'+value1.AddamtName+'</td>\
						<td class="text-center">'+ PosStrucAddAmt +'</td>\
						<td class="text-center">'+ PosStrucRewardAmt +'</td>\
					</tr>\
					';						
                    $('#'+tb_id+'> tbody').append(contents);
                });
 
            }else{
                var contents = '<tr><td colspan="5" class="text-center"> -- ไม่มีเงินเพิ่ม -- </td></tr>';
                $('#'+tb_id+'> tbody').append(contents);
            }
        },
        complete: function() {
           // $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadPosStrucAddAmt');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

org.GetPosStrucAddAmt=function(PositionID,AddAmtCode){
    
   // alert(PositionID+" :: "+AddAmtCode); 
    
    $("#btneditcf_Addamt").removeClass('hidden');
    $("#btncreatecf_Addamt").addClass('hidden');

    var DataSet = {
        PositionID: PositionID,
        AddAmtCode: AddAmtCode 
    };
    console.log(DataSet);
    
    $.ajax({ 
        url: org.url + '?mode=LoadPosStrucAddAmt',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) { 
            var value1 =  result[0];
            console.log(value1); 
            
            var PosStrucAddAmt = value1.PosStrucAddAmt ;
            if(PosStrucAddAmt == null || PosStrucAddAmt=='' ){
                PosStrucAddAmt = "";
            }else{
                PosStrucAddAmt = parseInt(value1.PosStrucAddAmt) ;
                PosStrucAddAmt = PosStrucAddAmt.toLocaleString();
            }

            var PosStrucRewardAmt = value1.PosStrucRewardAmt ;
            if(PosStrucRewardAmt == null || PosStrucRewardAmt=='' ){
                PosStrucRewardAmt = "";
            }else{
                PosStrucRewardAmt = parseInt(value1.PosStrucRewardAmt) ;
                PosStrucRewardAmt = PosStrucRewardAmt.toLocaleString();
            }

            org.LoadAddamtSel(value1.AddAmtCode,'AddAmtCode')
            $("#AddAmtCodeOld").val(value1.AddAmtCode);
            $("#PosStrucAddAmt").val(PosStrucAddAmt);
            $("#PosStrucRewardAmt").val(PosStrucRewardAmt);
            $("#NumApprove").val(value1.NumApprove);

            
            org.LoadSourceofCommSel(value1.SourceOfCommID,'AddamtSourceOfCommID'); 
            $("#AddamtCommDocNo").val(value1.CommDocNo);
        
            if (value1.CommDocDate != null)
            $('#AddamtCommDocDate').pickadate('picker').set('select', new Date(value1.CommDocDate));
            if (value1.CommEffDate != null)
            $('#AddamtCommEffDate').pickadate('picker').set('select', new Date(value1.CommEffDate));
            if (value1.CommExpDate != null)
            $('#AddamtCommExpDate').pickadate('picker').set('select', new Date(value1.CommExpDate));

             
        },
        complete: function() {
             $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadPosStrucAddAmt');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

org.EditPosStrucAddAmt=function(){

      
    var DataSet = {
        PositionID: PositionID,
        AddAmtCode: AddAmtCode 
    };

    
    

    var dataid = $("#dataid").val();
    var active = 0;


    if($("input[name=AddamtCommDocDate_submit]").val()){
        var AddamtCommDocDate = $("input[name=AddamtCommDocDate_submit]").val() ;
    }else{
        var AddamtCommDocDate =   'NULL'  ;
    }
    if($("input[name=AddamtCommEffDate_submit]").val()){
        var AddamtCommEffDate = $("input[name=AddamtCommEffDate_submit]").val() ;
    }else{
        var AddamtCommEffDate =   'NULL'  ;
    }
    if($("input[name=AddamtCommExpDate_submit]").val()){
        var AddamtCommExpDate = $("input[name=AddamtCommExpDate_submit]").val() ;
    }else{
        var AddamtCommExpDate =   'NULL'  ;
    }

    
    var PosStrucAddAmt = $("#PosStrucAddAmt").val() ;
    var PosStrucRewardAmt = $("#PosStrucRewardAmt").val() ;
    PosStrucAddAmt = PosStrucAddAmt.replace(',','');
    PosStrucRewardAmt = PosStrucRewardAmt.replace(',','');
    var NumApprove = $("#NumApprove").val() ;  
    if(!NumApprove){
        NumApprove ='';
    }
    if(!PosStrucRewardAmt){
        PosStrucRewardAmt ='';
    }
    var exDat = {
        table: "HrtPosStrucAddamt",
        data: {
            AddAmtCode : $("#AddAmtCode").val() ,
            PosStrucAddAmt :  PosStrucAddAmt,
            PosStrucRewardAmt : PosStrucRewardAmt ,
            NumApprove :  NumApprove,
            SourceOfCommID : $("#AddamtSourceOfCommID").val() ,
            CommDocNo : $("#AddamtCommDocNo").val() ,
            CommDocDate : AddamtCommDocDate  ,
            CommEffDate : AddamtCommEffDate  ,
            CommExpDate : AddamtCommExpDate ,
           // PersonTypeID : $("#PersonTypeID").val() ,
           // FlagAddAmtType : '1' ,
            CommTypeID : '' ,
            UpdateUserID : $("#UpdateUserID").val() ,
            UpdateDate : $("#UpdateDate").val() ,
        },
        where: {
            PositionID: $("#PositionID").val() , 
            AddAmtCode: $("#AddAmtCodeOld").val()
        }
    };

    console.log(exDat)

    $.ajax({
        url: org.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modalAddamt').modal('hide');
            $('#modal_editcf_Addamt').modal('hide');
            // $('#modalAddamt').modal('hide');
            // $('#modal_editcf_Addamt').hide();
             $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
          
            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + dataid;
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
               // setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            } 
           
            org.LoadPosStrucAddAmt( $("#PositionID").val(),'PosAddamtTable');
        },
        complete: function() {
             $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

org.CreatePosStrucAddAmt=function(){

       
    if($("input[name=AddamtCommDocDate_submit]").val()){
        var AddamtCommDocDate = $("input[name=AddamtCommDocDate_submit]").val() ;
    }else{
        var AddamtCommDocDate =   'NULL'  ;
    }
    if($("input[name=AddamtCommEffDate_submit]").val()){
        var AddamtCommEffDate = $("input[name=AddamtCommEffDate_submit]").val() ;
    }else{
        var AddamtCommEffDate =   'NULL'  ;
    }
    if($("input[name=AddamtCommExpDate_submit]").val()){
        var AddamtCommExpDate = $("input[name=AddamtCommExpDate_submit]").val() ;
    }else{
        var AddamtCommExpDate =   'NULL'  ;
    }

    
    var PosStrucAddAmt = $("#PosStrucAddAmt").val() ;
    var PosStrucRewardAmt = $("#PosStrucRewardAmt").val() ;
    PosStrucAddAmt = PosStrucAddAmt.replace(',','');
    PosStrucRewardAmt = PosStrucRewardAmt.replace(',','');
    var NumApprove = $("#NumApprove").val() ;  
    if(!NumApprove){
        NumApprove ='';
    }
    if(!PosStrucRewardAmt){
        PosStrucRewardAmt ='';
    }
    var exDat = {
        table: "HrtPosStrucAddamt",
        data: {
            PositionID: $("#PositionID").val() ,  
            AddAmtCode : $("#AddAmtCode").val() ,
            PosStrucAddAmt :  PosStrucAddAmt,
            PosStrucRewardAmt : PosStrucRewardAmt ,
            NumApprove :  NumApprove,
            SourceOfCommID : $("#AddamtSourceOfCommID").val() ,
            CommDocNo : $("#AddamtCommDocNo").val() ,
            CommDocDate : AddamtCommDocDate  ,
            CommEffDate : AddamtCommEffDate  ,
            CommExpDate : AddamtCommExpDate ,
            PersonTypeID : $("#PersonTypeID").val() ,
            FlagAddAmtType : '1' ,
            CommTypeID : '' ,
            CreateUserID : $("#CreateUserID").val() ,
            CreateDate : $("#CreateDate").val() ,
            UpdateUserID : $("#UpdateUserID").val() ,
            UpdateDate : $("#UpdateDate").val() ,
        } 
    };

    console.log(exDat)

    $.ajax({
        url: org.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() { 
            $('#modalAddamt').modal('hide');
            $('#modal_createcf_Addamt').modal('hide'); 
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data); 
            if (data.success == "COMPLETE") { 
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
               // setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            } 
           
            org.LoadPosStrucAddAmt( $("#PositionID").val(),'PosAddamtTable');
        },
        complete: function() {
             $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

org.callModalDeletePosStrucAddAmt=function( PositionID, AddAmtCode ) {

    //callDeleteFixField
    $("#txt_mo_name").html("ต้องการยืนยันการลบข้อมูลเงินเพิ่มประจำตำแหน่ง");

    var btn_delete = '<a href="#" class="btn btn-min-width mb-1 round btn-danger" onclick="org.callDeletePosStrucAddAmtTruly(\'' + PositionID + '\',\'' + AddAmtCode + '\');"><i class="fa fa-trash"> </i> ยืนยัน </a> ';
    $("#btn_delete").html(btn_delete);

    $('#modal_delete').modal('show');
};

org.callDeletePosStrucAddAmtTruly=function(  PositionID, AddAmtCode ) {
  
    var DataSet = {
        table: 'HrtPosStrucAddamt',
        where: { 
            PositionID: PositionID,
            AddAmtCode: AddAmtCode,
        }
    };
    //console.log(DataSet);

    $.ajax({
        url: org.url + '?mode=DeleteDataSet',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $('#modal_delete').modal('hide');
        },
        success: function(data) { 
            if (data.success == "COMPLETE") {
                duck.SwalSuccess('ลบข้อมูลสำเร็จ'); 
            } else {
                duck.NotiWarning();
            } 
            org.LoadPosStrucAddAmt( $("#PositionID").val(),'PosAddamtTable');
        },
        complete: function() {},
        error: function(data) {
            console.log('Please check scripts callDeletePosStrucAddAmtTruly');
            console.log(data);
        }
    });
};
/////////////////////   AddAmt : เงินเพิ่ม  /////////////////
org.LoadAddamtSel=function(v_val,element_id){
    if (v_val) {

    }

    var exDat = {
        table: "HrtAddamt",
        where: { 
            AddamtDelete : '0',
            AddamtActive : '1'
        },
        orderby: " AddamtName ",
        limit: "",
    };
    //console.log(exDat);
    $.ajax({
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            //$("#modalLoading").show();
        },
        success: function(data) {
            //console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกประเภทเงินเพิ่ม").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.AddamtCode == v_val) {
                    $(".select2-chosen").text(result.AddamtName);
                    $('<option>').attr('value', result.AddamtCode).attr('data-nameabbr', result.AddamtAbbrName).attr('selected', 'selected').text(result.AddamtName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.AddamtCode).attr('data-nameabbr', result.AddamtAbbrName).text(result.AddamtName).appendTo('#' + element_id);
                }
            });
        },
        complete: function() {
            //$("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            console.log('check  LoadAddamtSel');
            duck.NotiDanger();
        }
    });
};


/////////////////////   LoadPosStrucSkill : ลชทอ.ตำแหน่ง  /////////////////
org.LoadPosStrucSkill=function(PositionID,tb_id){
     
    var DataSet = {
        PositionID: PositionID,
    };
    //console.log(DataSet);
    
    $.ajax({ 
        url: org.url + '?mode=LoadPosStrucSkill',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            //$("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            $('#'+tb_id+'> tbody').empty();

            //console.log(result);
            if (result) {
                $.each(result, function(key1, value1) {
                    var contents = '';
                    
                    /*
                    var PosStrucAddAmt = value1.PosStrucAddAmt ;
                     */
                    contents += '\
					<tr>\
						<td class="text-center">\
                            <a href="javascript:" onclick="$(\'#modalAddamt\').modal();org.GetPosStrucAddAmt(\''+value1.PositionID+'\'\,\''+value1.SkillNo+'\'\ );"><i class="la la-pencil-square-o icon_action  iconedit" ></i></a>\
                            <a href="javascript:org.callModalDeletePosStrucAddAmt(\''+value1.PositionID+'\'\,\''+value1.SkillNo+'\'\ );"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td>'+value1.SkillNo+" "+'</td>\
						<td class="text-center">'+ value1.ArmAbbrName +'</td>\
					</tr>\
					';						
                    $('#'+tb_id+'> tbody').append(contents); 
                });
 
            }else{
                var contents = '<tr><td colspan="5" class="text-center"> -- ไม่มีเงินเพิ่ม -- </td></tr>';
                $('#'+tb_id+'> tbody').append(contents);
            }
        },
        complete: function() {
           // $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadPosStrucAddAmt');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////    RankFix : อัตราเงินเดือน  /////////////////
org.LoadRankFixID=function(v_val, element_id, ref_val) {
    if (v_val) {

    }
    if (ref_val) {
        PersonTypeID = ref_val;
    }
    var exDat = {
        table: "HrtRank",
        where: {
            HrtRankActive: 1,
            HrtRankDelete: 0,
            PersonType: PersonTypeID
        },
        orderby: "",
        limit: "",
    };
    console.log(exDat);
    $.ajax({
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(data) {
            console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกอัตราเงินเดือน").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.HrtRankID == v_val) {
                    $(".select2-chosen").text(result.HrtRankAbbrTh);
                    $('<option>').attr('value', result.HrtRankID).attr('selected', 'selected').text(result.HrtRankAbbrTh).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.HrtRankID).text(result.HrtRankAbbrTh).appendTo('#' + element_id);
                }
            });
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            console.log('check  LoadRankFixID');
            duck.NotiDanger();
        }
    });
};

org.LoadRankFix=function(v_val, element_id, ref_val) {
    if (v_val) {

    }
    var PersonTypeID = '';
    if (ref_val) {
        PersonTypeID = ref_val;
    }
    var exDat = {
        table: "HrtRankFix",
        where: {
            RankFixActive: 1,
            RankFixDelete: 0,
            PersonTypeID: PersonTypeID
        },
        orderby: "",
        limit: "",
    };
    //console.log(exDat);
    $.ajax({
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(data) {
           // console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกอัตราเงินเดือน").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.RankCode == v_val) {
                    $(".select2-chosen").text(result.RankFixAbbrTh);
                    $('<option>').attr('value', result.RankCode).attr('selected', 'selected').text(result.RankFixAbbrTh).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.RankCode).text(result.RankFixAbbrTh).appendTo('#' + element_id);
                }
            });
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            console.log('check  LoadRankFix');
            duck.NotiDanger();
        }
    });
};

org.LoadRankSortSel2=function(v_val, element_id) {
    if (v_val) {

    }

    var exDat = {
        table: "HrtRankSort",
        where: {

        },
        orderby: "",
        limit: "",
    };
    console.log(exDat);
    $.ajax({
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(data) {
            console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกอัตราเงินเดือน").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.RankSort == v_val) {
                    $(".select2-chosen").text(result.RankSortName);
                    $('<option>').attr('value', result.RankSort).attr('selected', 'selected').text(result.RankSortName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.RankSort).text(result.RankSortName).appendTo('#' + element_id);
                }
            });
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            console.log('check  LoadRankFixID');
            duck.NotiDanger();
        }
    });


};

org.LoadRankSortSel=function(v_val, element_id, ref_val) {
    if (v_val) {

    }
    var PersonTypeID = "";
    if (ref_val) {
        PersonTypeID = ref_val;
    }

    var exDat = {
        table: "HrtRankSort",
        where: {
            PersonType: PersonTypeID
        },
        orderby: "",
        limit: "",
    };
    console.log(exDat);
    $.ajax({
        url: org.url + '?mode=LoadRankSort',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(data) {
            console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกอัตราเงินเดือน").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.HrtRankID == v_val) {
                    $(".select2-chosen").text(result.HrtRankAbbrTh);
                    $('<option>').attr('value', result.HrtRankID).attr('selected', 'selected').text(result.HrtRankAbbrTh).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.HrtRankID).text(result.HrtRankAbbrTh).appendTo('#' + element_id);
                }
            });
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            console.log('check  LoadRankSortSel');
            duck.NotiDanger();
        }
    });


};

///////////////////    Pass : สาเหตุปลด/สูญเสีย  /////////////////
org.LoadPass=function() {
    var DataSet = {
        table: 'HrtPass',
        where: {
            HrtPassDelete: '0',
        },
        orderby: "",
        limit: ""
    };
    console.log(DataSet);

    $.ajax({
        //url:org.url+'?mode=LoadAllData',
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';


                    if (value1['HrtPassAbbrName'] == null)
                        value1['HrtPassAbbrName'] = "-";

                    if (value1['HrtPassFlag'] == null)
                        value1['HrtPassFlag'] = "-";

                    if (value1['HrtTypeReserve'] == null)
                        value1['HrtTypeReserve'] = "-";

                    if (value1['HrtLevelReserve'] == null)
                        value1['HrtLevelReserve'] = "-";

                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:org.SetFrom(\'EDIT\',\'' + value1.HrtPassID + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['HrtPassID'] + '"></i></a>\
							<a href="javascript:org.callModalDeleteFixField(\'HrtPass\',\'' + value1['HrtPassID'] + '\'\,\'สาเหตุปลด/สูญเสีย\',\'HrtPassID\',\'HrtPassDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1['HrtPassID'] + '</td>\
						<td class="text-left">' + value1['HrtPassName'] + '</td>\
						<td class="text-left">' + value1['HrtPassAbbrName'] + '</td>\
						<td class="text-center">' + value1['HrtPassFlag'] + '</td>\
						<td class="text-center">' + value1['HrtTypeReserve'] + '</td>\
						<td class="text-center">' + value1['HrtLevelReserve'] + '</td>\
						<td class="text-center">' + org.SetActive(value1['HrtPassActive']) + '</td>\
					</tr>\
					';


                    $('#PassTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#PassTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadPassTable');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};


///////////////////    Skill : เลขลชทอ.  /////////////////
org.LoadSkillSel=function(v_val,element_id,personID) {
    var DataSet = {
        table: 'HrtSkill',
        where: {
            RecStatus: 'A',
            HrtPersonID: personID
        },
        orderby: " SkillNo ASC    ",
        limit: ""
    };
    //console.log(DataSet);
    $.ajax({
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
          //  $("#modalLoading").show();
        },
        success: function(data) {
            //console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกเลข ลชทอ.").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.SkillNo == v_val) {
                    $(".select2-chosen").text( result.SkillNo+": "+result.SkillName);
                    $('<option>').attr('value', result.SkillNo).attr('data-armcode', result.ArmCode).attr('data-spcid', result.SpcID).attr('selected', 'selected').text(result.SkillNo+": "+result.SkillName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.SkillNo).attr('data-armcode', result.ArmCode).attr('data-spcid', result.SpcID).text(result.SkillNo+": "+result.SkillName).appendTo('#' + element_id);
                }
            });
        },
        complete: function() {
         //   $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            console.log('check  LoadSkillSel');
            duck.NotiDanger();
        }
    });
};

///////////////////    SkillPrefix : อักษรนำเลข ลชทอ.  /////////////////
org.LoadSkillPrefix=function() {
    var DataSet = {
        table: 'HrtSkillPrefix',
        where: {
            HrtSkillPrefixDelete: 0,
        },
        orderby: "",
        limit: ""
    };
    console.log(DataSet);

    $.ajax({
        //url:org.url+'?mode=LoadAllData',
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';


                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:org.SetFrom(\'EDIT\',\'' + value1.HrtSkillPrefixID + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['HrtSkillPrefixID'] + '"></i></a>\
							<a href="javascript:org.callModalDeleteFixField(\'HrtSkillPrefix\',\'' + value1['HrtSkillPrefixID'] + '\'\,\'อักษรนำเลข ลชทอ.\',\'HrtSkillPrefixID\',\'HrtSkillPrefixDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1['HrtSkillPrefixID'] + '</td>\
						<td class="text-center">' + value1['HrtPersonTypeID'] + '</td>\
						<td class="text-left">' + value1['HrtSkillPrefixName'] + '</td>\
						<td class="text-center">' + org.SetActive(value1['HrtSkillPrefixActive']) + '</td>\
					</tr>\
					';


                    $('#SkillPrefixTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#SkillPrefixTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadSkillPrefixTable');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

org.LoadSkillPrefixSel=function(v_val,element_id,personID) {
    var DataSet = {
        table: 'HrtSkillPrefix',
        where: {
            HrtSkillPrefixDelete: 0,
            HrtSkillPrefixActive: 1,
            HrtPersonID: personID
        },
        orderby: " HrtSkillPrefixName ASC ",
        limit: ""
    };
   // console.log(DataSet);
    $.ajax({
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(data) {
           // console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกคำนำหน้า ลชทอ.").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.HrtSkillPrefixID == v_val) {
                    $(".select2-chosen").text(result.HrtSkillPrefixName);
                    $('<option>').attr('value', result.HrtSkillPrefixID).attr('selected', 'selected').text(result.HrtSkillPrefixName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.HrtSkillPrefixID).text(result.HrtSkillPrefixName).appendTo('#' + element_id);
                }
            });
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            console.log('check  LoadSkillPrefixSel');
            duck.NotiDanger();
        }
    });
};
///////////////////    SkillSuffix : อักษรตามเลข ลชทอ  /////////////////
org.LoadSkillSuffix=function() {
    var DataSet = {
        table: 'HrtSkillSuffix',
        where: {
            HrtSkillSuffixDelete: '0',
        },
        orderby: "",
        limit: ""
    };
    console.log(DataSet);

    $.ajax({
        //url:org.url+'?mode=LoadAllData',
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';


                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:org.SetFrom(\'EDIT\',\'' + value1.HrtSkillSuffixID + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['HrtSkillSuffixID'] + '"></i></a>\
							<a href="javascript:org.callModalDeleteFixField(\'HrtSkillSuffix\',\'' + value1['HrtSkillSuffixID'] + '\'\,\'อักษรตามเลข ลชทอ.\',\'HrtSkillSuffixID\',\'HrtSkillSuffixDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1['HrtSkillSuffixID'] + '</td>\
						<td class="text-center">' + value1['HrtPersonTypeID'] + '</td>\
						<td class="text-left">' + value1['HrtSkillSuffixName'] + '</td>\
						<td class="text-center">' + org.SetActive(value1['HrtSkillSuffixActive']) + '</td>\
					</tr>\
					';


                    $('#SkillSuffixTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#SkillSuffixTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadSkillSuffixTable');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

///////////////////    ArmCode : เหล่า  /////////////////
org.LoadArmSel=function(v_val,element_id) {
    var DataSet = {
        table: 'HrtArm',
        where: {
            ArmActive: 1 , 
            ArmDelete: 0 
        },
        orderby: " ArmCode ASC    ",
        limit: ""
    };
    //console.log(DataSet);
    $.ajax({
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
           // $("#modalLoading").show();
        },
        success: function(data) {
         //   console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกเหล่า").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.ArmCode == v_val) {
                    $(".select2-chosen").text( result.ArmCode+": "+result.ArmName);
                    $('<option>').attr('value', result.ArmCode).attr('selected', 'selected').text(result.ArmCode+": "+result.ArmName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.ArmCode).text(result.ArmCode+": "+result.ArmName).appendTo('#' + element_id);
                }
            });
        },
        complete: function() {
         //   $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            console.log('check  LoadArmSel');
            duck.NotiDanger();
        }
    });
};

///////////////////    Province  : จังหวัด  /////////////////
org.LoadProvinceSel=function(v_val, element_id) {
    if (v_val) {

    }
    var exDat = {
        table: "HrtProvince",
        where: {

        },
        orderby: " HrtProvinceName   ASC ",
        //	orderby : " CONVERT (   OrgGroupTypeName  USING tis620 ) ASC " ,
        limit: "",
    };

    $.ajax({
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        success: function(data) {
            //console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกจังหวัด").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.HrtProvinceCode + "0000" == v_val) {
                    $(".select2-chosen").text(result.HrtProvinceName);
                    $('<option>').attr('value', result.HrtProvinceCode + "0000").attr('selected', 'selected').text(result.HrtProvinceName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.HrtProvinceCode + "0000").text(result.HrtProvinceName).appendTo('#' + element_id);
                }
            });

        },
        error: function(data) {
            console.log(data);
            console.log('check LoadProvinceSel');
            duck.NotiDanger();
        }
    });

};

///////////////////    SourceofComm : ประเภทคำสั่ง  /////////////////
org.LoadSourceofCommSel=function(v_val, element_id) {
    if (v_val) {

    }
    var exDat = {
        table: "HrtSourceOfComm",
        where: {

        },
        orderby: " SourceOfCommName   ASC ",
        limit: "",
    };

    $.ajax({
        url: org.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        success: function(data) {
            //console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกคำสั่ง").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.SourceOfCommID == v_val) {
                    $(".select2-chosen").text(result.SourceOfCommName);
                    $('<option>').attr('value', result.SourceOfCommID).attr('selected', 'selected').text(result.SourceOfCommName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.SourceOfCommID).text(result.SourceOfCommName).appendTo('#' + element_id);
                }
            });

        },
        error: function(data) {
            console.log(data);
            console.log('check LoadSourceofCommSel');
            duck.NotiDanger();
        }
    });

};

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////   For Person registration    ///////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
org.LoadDataOrgSel=function(selfDID, OrgTypeID , OrgLevel2DID, element_id){
   
    var v_val = selfDID  ;
    var exDat = { 
        OrgTypeID: OrgTypeID,
        OrgLevel2DID: OrgLevel2DID,
    };

    $.ajax({
        url: org.url + '?mode=LoadDataOrgSel',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        success: function(data) {
           // console.log(data);
             
            $('#' + element_id).find('option').remove();
            $('<option>').attr('value', '').text("กรุณาเลือกชื่อหน่วยงาน").appendTo('#' + element_id);
            $.each(data.orgdetail, function(i, result) {
                if (result.orgdid == v_val) {
                    $(".select2-chosen").text(result.orgname);
                    $('<option>').attr('value', result.orgdid).attr('selected', 'selected').text(result.orgname).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.orgdid).text(result.orgname).appendTo('#' + element_id);
                }
            });  
        },
        error: function(data) {
            console.log(data);
            console.log('check LoadDataOrgSel');
            duck.NotiDanger();
        }
    });
};

org.LoadPositionByOrgLevelDIDSel=function(selfId ,element_id,OrgLevelDID) {
    //console.log(OrgLevelDID);

    var v_val = selfId;
    $("#OrgLevelDIDtxt").val(OrgLevelDID); 

    var DataSet = {
        OrgLevelDID: OrgLevelDID
    };


   // console.log(DataSet);

    $.ajax({
        url: org.url + '?mode=LoadPositionByOrgLevelx',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(data, status, xhr) {
 
            //HrtPositionID: "303224", HrtPosName 
           // console.log(data);
            $('#' + element_id).find('option').remove();
            $('<option>').attr('value', '').text("กรุณาเลือกตำแหน่ง").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.HrtPositionID == v_val) {
                    $(".select2-chosen").text(result.HrtPosName);
                    $('<option>').attr('value', result.HrtPositionID).attr('selected', 'selected').text(result.HrtPosName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.HrtPositionID).text(result.HrtPosName).appendTo('#' + element_id);
                }
            }); 
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadPositionByOrgLevelDID');
            console.log(xhr);
            duck.NotiDanger();
        }
    });

};

org.LoadPosPersonByOrgLevelDIDSel=function(HrtPositionID ,HrtPosNo ,element_id,OrgLevelDID) {
    //console.log(OrgLevelDID);
  
    var v_val = HrtPositionID;

    var DataSet = {
        OrgLevelDID: OrgLevelDID
    };
 
    console.log(DataSet);

    $.ajax({ 
        url: org.url + '?mode=LoadPosPersonByOrgLevelx',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").modal('show');
        },
        success: function(data, status, xhr) {
  

           // console.log(data);
            $('#' + element_id).find('option').remove();
            $('<option>').attr('value', '').text("กรุณาเลือกตำแหน่ง").appendTo('#' + element_id);
            $.each(data, function(i, result) { 
                 if (result.HrtPositionID == v_val   ) { // && result.HrtPosNum == HrtPosNo
                    $(".select2-chosen").text(result.HrtPosName);
                    $('<option>').attr('value', result.HrtPositionID).attr('data-posno', result.HrtPosNum).attr('data-posname', result.HrtPosName).attr('data-abbrname', result.HrtPosAbbrName).attr('selected', 'selected').text(result.HrtPosName+" ("+result.HrtPosNum+")").appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.HrtPositionID).attr('data-posno', result.HrtPosNum).attr('data-posname', result.HrtPosName).attr('data-abbrname', result.HrtPosAbbrName).text(result.HrtPosName+" ("+result.HrtPosNum+")").appendTo('#' + element_id);
                }
            });  
            
        },
        complete: function() {
            $("#modalLoading").hide(); 
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadPositionByOrgLevelDID');
            console.log(xhr);
            duck.NotiDanger();
        }
    });

};

org.LoadPosPersonDetail=function(HrtPositionID ,HrtPosNo, OrgLevDID){
    var DataSet = {
        HrtPositionID: HrtPositionID ,
        HrtPosNo : HrtPosNo,
        OrgLevDID : OrgLevDID
    };  
    //console.log(DataSet);

    $.ajax({
        url: org.url + '?mode=LoadPosPersonDetail',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(data, status, xhr) {
          //  console.log(data); 
           
            var PosOrg =  $('#OrgPosition_t15').find(':selected').data("posname")+  data.orgDetail.listname.orgName ;
            $("#HrtPosOrgName_t15").val(PosOrg); 
            var PosAbbrOrg =  $('#OrgPosition_t15').find(':selected').data("abbrname")+  data.orgDetail.listname.orgNameAbbrSemi ;
            $("#HrtPosOrgAbbrName_t15").val(PosAbbrOrg);
            org.LoadPosPerStatusSel( data[0].PosPerStatus,'PosPerStatus_t15');  
            org.LoadRankFix( data[0].RankID,'RankID_t15','');

            var ArmAbbrName="" ;
            var SkillNo="" ;
            $("#HrtArmAbbrName_t15").val('');
            $("#SkillNo_t15").val('');
            if(data.skill){
                $.each(data.skill, function(i, result) {
                    if(result.ArmAbbrName!=null){
                        ArmAbbrName =   ArmAbbrName+""+result.ArmAbbrName+"/" ;
                    } 
                    SkillNo =  SkillNo+""+result.SkillNo+"/" ;  
                }); 
                ArmAbbrName = ArmAbbrName.substring(0 , ArmAbbrName.length-1);
                SkillNo = SkillNo.substring(0 , SkillNo.length-1 );
            } 
            $("#HrtArmAbbrName_t15").val(ArmAbbrName);
            $("#SkillNo_t15").val(SkillNo);

        
 

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadPosPersonDetail');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};
  
org.LoadDataPositionById=function(PositionID){
    var DataSet = {
        PositionID: PositionID 
    };
   // console.log(DataSet);
    
    $.ajax({ 
        url: org.url + '?mode=LoadPositionDetail',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            //$("#modalLoading").show();
        },
        success: function(result, status, xhr) {

           // console.log(result);

            //org.LoadDataOrgParent
            org.LoadOrgTypeSel(result.OrgTypeID,'OrgTypeID_t15');
            
            var OrgLevel2 = '';
           
            if(result.OrgData.unitM[2] ){
                OrgLevel2 =  result.OrgData.unitM[2].OrgLevelDID; 
            } 
           
            org.LoadOrg2SelByOrgTypeID(OrgLevel2,$("#OrgTypeID_t15").val(),'OrgLevel2_t15'); 

            org.LoadDataOrgSel(result.OrgLevelDID ,$("#OrgTypeID_t15").val(),OrgLevel2,'OrgLevelAll_t15')


            org.LoadPosPersonByOrgLevelDIDSel( PositionID ,'','OrgPosition_t15',result.OrgLevelDID);

          
        },
        complete: function() {
           // $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadPositionDetail');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};
*/
// LoadOrg2SelByOrgTypeID
// LoadDataOrgSel
// LoadPosPersonByOrgLevelDIDSel
// LoadPosPersonDetail

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////// 


org.SetActive=function(dataactive) {
    if (dataactive == 1) {
        var active = '<i class="la la-toggle-on" style="color: green; font-size:30px; "></i>';
    } else {
        var active = '<i class="la la-toggle-off" style="color: red;font-size:30px;"></i>';
    }

    return active;
};

org.CheckCheckbox=function() {

    if (checkBox.checked == true) {

    } else {

    }
    return
};

/////////////////////////////////////////////////////////////////////////////////////
$(document).ready(function() {

    $(".input-group-append").click(function() {
        $('.pickadate-translations').trigger("click")
    });



});

function collapsedMenu() {
    //	console.log('maindiv') 
    $("#maindiv").removeClass('menu-expanded');
    $("#maindiv").addClass('menu-collapsed');
}
jQuery(function($) {
    setTimeout(collapsedMenu, 200);
});
//EditOrgType
////////////////////          END org Script         ///////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////