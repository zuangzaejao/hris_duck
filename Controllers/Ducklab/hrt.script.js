/////////////////////////////////////////////////////////////////////////////////////
////////////////////           hrt   Script          ///////////////////////////
var hrt = {
    url: '/Model/Ducklab/hrt.inc.php'
};

//////////////////////////////////////////////////////////////////////////////////////////////////
hrt.callModalAdd = function(FuncName, table) {

    //alert(FuncName);
    var btn_add = '<button type="button" class="btn btn-success" onclick="' + FuncName + '();"> <i class="fa fa-save"> </i> ยืนยันการเพิ่มข้อมูล </button>';
    $("#btn_add").html(btn_add);

    $('#modal_add').modal('show');
};

hrt.callModalEdit = function(FuncName, table, table_id, detail) {

    //$("#txt_mo_name").html(detail);

    var btn_edit = '<button type="button" class="btn btn-warning" onclick="' + FuncName + '();"> <i class="fa fa-trash"> </i> ยืนยันการแก้ไข </button>';
    $("#btn_edit").html(btn_edit);

    $('#modal_edit').modal('show');
};

hrt.callModalDelete = function(table, table_id, detail, link) {

    //callDeleteFixField
    $("#txt_mo_name").html("ต้องการยืนยันการลบข้อมูล " + detail);

    var btn_delete = '<a href="#" class="btn btn-min-width mb-1 round btn-danger" onclick="hrt.callDelete(\'' + table + '\',\'' + table_id + '\',\'' + link + '\');"><i class="fa fa-trash"> </i> ยืนยัน </a> ';
    $("#btn_delete").html(btn_delete);

    $('#modal_delete').modal('show');
};

hrt.callModalDeleteFixField = function(table, table_id, detail, id_name, status_name, link) {

    //callDeleteFixField
    $("#txt_mo_name").html("ต้องการยืนยันการลบข้อมูล " + detail);

    var btn_delete = '<a href="#" class="btn btn-min-width mb-1 round btn-danger" onclick="hrt.callDeleteFixField(\'' + table + '\',\'' + table_id + '\',\'' + id_name + '\',\'' + status_name + '\',\'' + link + '\');"><i class="fa fa-trash"> </i> ยืนยัน </a> ';
    $("#btn_delete").html(btn_delete);

    $('#modal_delete').modal('show');
};

hrt.callModalDisable = function(table, table_id, detail, link) {


    $("#txt_detail_dis").html("ต้องการยืนยันการลบข้อมูล " + detail);

    var btn_disable = '<button type="button" class="btn btn-danger" onclick="hrt.callDisable(\'' + table + '\',\'' + table_id + '\',\'' + link + '\');"> <i class="fa fa-trash"> </i> ยืนยัน </button>';
    $("#btn_disable").html(btn_disable);

    $('#modal_disable').modal('show');
};


hrt.callModalDeleteTruly = function(table, table_id, detail) {
    $("#txt_mo_name").html(detail);

    var btn_delete = '<button type="button" class="btn btn-danger" onclick="hrt.callDeleteTruly(\'' + table + '\',\'' + table_id + '\');"> <i class="fa fa-trash"> </i> ยืนยันการลบ </button>';
    $("#btn_delete").html(btn_delete);

    $('#modal_delete').modal('show');
};

hrt.callDelete = function(table, table_id, link) {
    //alert(table);
    //alert(table_id);
    if (!fieldname) {
        var fieldname = 'id';
    }
    var DataSet = {
        table: table,
        data: {
            deleted: '1',
        },
        where: { id: table_id }
    };
    //console.log(DataSet);

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $('#modal_delete').modal('hide');
        },
        success: function(data) {
            //console.log(data);

            if (data.success == "COMPLETE") {

                duck.ModalSShow();

                // setTimeout(duck.ModalSHide, 3000);
                if (link == 'backlink') {
                    setTimeout(duck.OpenBack, 500);
                } else {
                    setTimeout(duck.ReloadPage, 3000);
                }
            } else {
                duck.ModalWShow();
            }

        },
        complete: function() {},
        error: function(data) {
            console.log('Please check scripts callDelete');
            console.log(data);
            duck.NotiDanger();
        }
    });
};
// 
hrt.callDisable = function(table, table_id, link) {
    //alert(table);
    //alert(table_id);
    $('#modal_disable').modal('hide');
    if (!fieldname) {
        var fieldname = 'id';
    }
    var DataSet = {
        table: table,
        data: {
            enable: 'N',
        },
        where: { id: table_id }
    };
    //console.log(DataSet);

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $('#modal_delete').modal('hide');
        },
        success: function(data) {
            //console.log(data);

            if (data.success == "COMPLETE") {

                duck.ModalSShow();

                // setTimeout(duck.ModalSHide, 3000);
                if (link == 'backlink') {
                    setTimeout(duck.OpenBack, 500);
                } else {
                    setTimeout(duck.ReloadPage, 3000);
                }
            } else {
                duck.ModalWShow();
            }

        },
        complete: function() {},
        error: function(data) {
            console.log('Please check scripts callDelete');
            console.log(data);
            duck.NotiDanger();
        }
    });
};

hrt.callDeleteFixField = function(table, table_id, id_name, status_name, link) {
    var DataSet = {
        table: table,
        data: {
            status: '1', //status_name
            id: table_id, //status_name
        },
        data_name: {
            status_n: status_name,
            id_n: id_name,
        }
    };
    //console.log(DataSet);

    $.ajax({
        url: hrt.url + '?mode=StatusDataSetEX',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $('#modal_delete').hide();
            $("#modalLoading").show();
        },
        success: function(data) {
            if (data.success == "COMPLETE") {
                duck.SwalSuccess('ลบข้อมูลสำเร็จ');
                // duck.NotiSuccess();



                // duck.ModalSShow();
                if (link == 'backlink') {
                    setTimeout(duck.OpenBack, 500);
                } else {
                    setTimeout(duck.ReloadPage, 1500);
                }
            } else {

                duck.NotiWarning();
            }
        },
        complete: function() {},
        error: function(data) {
            console.log('Please check scripts callDeleteFixField');
            console.log(data);
            duck.NotiDanger();
        }
    });
};



hrt.callDeleteTruly = function(table, table_id) {

    var DataSet = {
        table: table,
        where: { id: table_id }
    };
    //console.log(DataSet);

    $.ajax({
        url: hrt.url + '?mode=DeleteDataSet',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $('#modal_delete').modal('hide');
        },
        success: function(data) {
            //console.log(data);

            if (data.success == "COMPLETE") {
                alert("ลบรายการสำเร็จ");
                window.location.reload();;

            } else {
                alert("ลบรายการไม่สำเร็จ กรุณาลองใหม่อีกครั้ง");

            }

        },
        complete: function() {},
        error: function(data) {
            console.log('Please check scripts callDelete');
            console.log(data);
        }
    });


};

hrt.callSetEnable = function(table_name, table_id, refto) {
    var x = 'N';
    if (!refto) {
        refto = "enable";
    }
    if ($("#" + refto + table_id).is(':checked') == true) {
        x = 'Y';
    }
    var DataSet = {
        table: table_name,
        data: {
            enable: x,
        },
        where: { id: table_id }
    };

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {},
        success: function(data) {
            if (data.success == "COMPLETE") {
                duck.NotiSuccess();

            } else {
                //	duck.ModalWShow();
                duck.NotiWarning();
            }
        },
        complete: function() {},
        error: function(data) {
            console.log('Please check scripts callSetEnable');
            console.log(data);
            duck.NotiDanger();
        }
    });
};

hrt.callSetDisplay = function(table_name, table_id, refto) {
    var x = 'N';
    if (!refto) {
        refto = "display";
    }
    if ($("#" + refto + table_id).is(':checked') == true) {
        x = 'Y';
    }
    var DataSet = {
        table: table_name,
        data: {
            display: x,
        },
        where: { id: table_id }
    };

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {},
        success: function(data) {
            if (data.success == "COMPLETE") {
                duck.NotiSuccess();
                /*
                 duck.ModalSShow();

                // setTimeout(duck.ModalSHide, 3000);
                if(link=='backlink'){
                	setTimeout( duck.OpenBack   , 500   );
                }else{
                	setTimeout( duck.ReloadPage   , 3000   );
                }
                */
            } else {
                //	duck.ModalWShow();
                duck.NotiWarning();
            }

        },
        complete: function() {},
        error: function(data) {
            console.log('Please check scripts callSetDisplay');
            console.log(data);
            duck.NotiDanger();
        }
    });
};

hrt.LoadProvince = function(v_val, element_id) {
    if (v_val) {
        //		alert(v_val);
    }
    var exDat = {
        table: "provinces",
        where: { enable: "Y" },
        orderby: " CONVERT (   provincesname_th  USING tis620 ) ASC ",
        limit: "",
    };

    $.ajax({
        url: hrt.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        success: function(data) {
            //console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกจังหวัด").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.id == v_val) {
                    $(".select2-chosen").text(result.provincesname_th);
                    $('<option>').attr('value', result.id).attr('selected', 'selected').text(result.provincesname_th).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.id).text(result.provincesname_th).appendTo('#' + element_id);
                }
            });

        },
        error: function(data) {
            console.log(data);
            console.log('check LoadProvince');
            duck.NotiDanger();
        }
    });

};


hrt.SetFrom = function(action, dataid, pageaction) {

    $('#dataid').val(dataid);
    $('#action').val(action);
    $("#formset").attr('action', pageaction)
        //console.log(action+" || "+data_id);
    $("#formset").submit();
};


hrt.SetFromAction = function(action, data_id, actionlink) {
    $("#form_set").attr("action", actionlink);
    $('#data_id').val(data_id);
    $('#action').val(action);

    $("#form_set").submit();
};
////////////////////////////////////////////////////////////////////////////////////////////        ///////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////		///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////    hrtType : โครงสร้างองค์กร  /////////////////									   ///////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////		///////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////		///////////////////////////////////////////////////////////////////////////////////////////////////
////ยังไม่แก้ส่วนข้างใน
hrt.LoadRankFix = function() {
    var DataSet = {
        table: 'HrtRankFix',
        where: {
            RankFixDelete: '0',
        },
        orderby: "",
        limit: "1000"
    };


    console.log(DataSet);

    $.ajax({
        url: hrt.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';


                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:hrt.SetFrom(\'EDIT\',\'' + value1.RankFixID + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['RankFixID'] + '"></i></a>\
							<a href="javascript:hrt.callModalDeleteFixField(\'HrtRankFix\',\'' + value1['RankFixID'] + '\'\,\'อัตรา\',\'RankFixID\',\'RankFixDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1['RankFixAbbrTh'] + '</td>\
						<td class="text-center">' + value1['RankFixNameTh'] + '</td>\
						<td class="text-center">' + hrt.SetActive(value1['RankFixActive']) + '</td>\
					</tr>\
					';


                    $('#RankFixTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#RankFixTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadRankFix');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.GetRankFix = function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'HrtRankFix',
        where: {
            RankFixID: dataid,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: hrt.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);

            //startDate 


            if (result) {
                $("#RankFixAbbrTh").val(result.RankFixAbbrTh);
                $("#RankFixNameTh").val(result.RankFixNameTh);

                if (result.RankFixActive == 0) {
                    $("#RankFixActive").bootstrapToggle('off');
                } else {
                    $("#RankFixActive").bootstrapToggle('on');
                }
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetRankFix');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.EditRankFix = function() {
    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();
    var active = 0;

    var checkBox = document.getElementById("RankFixActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtRankFix",
        data: {
            RankFixAbbrTh: $("#RankFixAbbrTh").val(),
            RankFixNameTh: $("#RankFixNameTh").val(),
            RankFixActive: active,
            RankFixUpdateBy: $("#RankFixUpdateBy").val(),
            RankFixUpdateDate: $("#RankFixUpdateDate").val()
        },
        where: {
            RankFixID: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

hrt.CreateRankFix = function() {
    $('#modal_createcf').modal('hide');


    var active = 0;

    var checkBox = document.getElementById("RankFixActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtRankFix",
        data: {
            RankFixAbbrTh: $("#RankFixAbbrTh").val(),
            RankFixNameTh: $("#RankFixNameTh").val(),
            RankFixActive: active,
            RankFixDelete: '0',
            RankFixCreateBy: $("#RankFixCreateBy").val(),
            RankFixCreateDate: $("#RankFixCreateDate").val() + ".000",
            RankFixUpdateBy: $("#RankFixCreateBy").val(),
            RankFixUpdateDate: $("#RankFixCreateDate").val() + ".000"
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + data.code;
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};


////////////////////////////////////////////////////////////////////////////////////////////
//00000000000000000000000000000000000000000000000
////////////////////////////////////////////////////////////////////////////////////////////
////ยังไม่แก้ส่วนข้างใน
hrt.LoadMilitaryType = function() {
    var DataSet = {
        table: 'HrtPersonTypeFix',
        where: {
            HrtPersonTypeFixDelete: '0',
        },
        orderby: "",
        limit: ""
    };


    console.log(DataSet);

    $.ajax({
        url: hrt.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';

                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:hrt.SetFrom(\'EDIT\',\'' + value1.HrtPersonTypeFixID + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['HrtPersonTypeFixID'] + '"></i></a>\
							<a href="javascript:hrt.callModalDeleteFixField(\'HrtPersonTypeFix\',\'' + value1['HrtPersonTypeFixID'] + '\'\,\'ประเภททหาร\',\'HrtPersonTypeFixID\',\'HrtPersonTypeFixDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1['HrtPersonTypeFixID'] + '</td>\
						<td class="text-center">' + value1['HrtPersonTypeFixName'] + '</td>\
						<td class="text-center">' + hrt.SetActive(value1['HrtPersonTypeFixActive']) + '</td>\
					</tr>\
					';


                    $('#PersonTypeFixTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#PersonTypeFixTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadMilitaryType');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.GetMilitaryType = function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'HrtPersonTypeFix',
        where: {
            HrtPersonTypeFixID: dataid,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: hrt.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);

            //startDate 


            if (result) {
                $("#HrtPersonTypeFixID").val(result.HrtPersonTypeFixID);
                $("#HrtPersonTypeFixName").val(result.HrtPersonTypeFixName);
                if (result.HrtPersonTypeFixActive == 0) {
                    $("#HrtPersonTypeFixActive").bootstrapToggle('off');
                } else {
                    $("#HrtPersonTypeFixActive").bootstrapToggle('on');
                }
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetMilitaryType');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.EditMilitaryType = function() {
    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();
    var active = 0;

    var checkBox = document.getElementById("HrtPersonTypeFixActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtPersonTypeFix",
        data: {
            HrtPersonTypeFixID: $("#HrtPersonTypeFixID").val(),
            HrtPersonTypeFixName: $("#HrtPersonTypeFixName").val(),
            HrtPersonTypeFixActive: active,
            HrtPersonTypeFixUpdateBy: $("#HrtPersonTypeFixUpdateBy").val(),
            HrtPersonTypeFixUpdateDate: $("#HrtPersonTypeFixUpdateDate").val()
        },
        where: {
            HrtPersonTypeFixID: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

hrt.CreateMilitaryType = function() {
    $('#modal_createcf').modal('hide');


    var active = 0;

    var checkBox = document.getElementById("HrtPersonTypeFixActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtPersonTypeFix",
        data: {
            HrtPersonTypeFixID: $("#HrtPersonTypeFixID").val(),
            HrtPersonTypeFixName: $("#HrtPersonTypeFixName").val(),
            HrtPersonTypeFixActive: active,
            HrtPersonTypeFixDelete: '0',
            HrtPersonTypeFixCreateBy: $("#HrtPersonTypeFixCreateBy").val(),
            HrtPersonTypeFixCreateDate: $("#HrtPersonTypeFixCreateDate").val() + ".000",
            HrtPersonTypeFixUpdateBy: $("#HrtPersonTypeFixCreateBy").val(),
            HrtPersonTypeFixUpdateDate: $("#HrtPersonTypeFixCreateDate").val() + ".000"
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            // if (data.success == "COMPLETE") {
            var link = "edit.php?dataid=" + data.code;
            duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
            setTimeout(duck.OpenPage, 2000, link, '_self');
            // } else {
            //     duck.NotiWarning();
            // }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};


////////////////////////////////////////////////////////////////////////////////////////////
///000000000000000000000000000000000000000000000000000
////////////////////////////////////////////////////////////////////////////////////////////
////ยังไม่แก้ส่วนข้างใน
hrt.LoadAirForceMilitaryNumber = function() {
    var DataSet = {
        table: 'HrtSkill',
        where: {
            SkillDelete: '0',
        },
        orderby: "",
        limit: ""
    };


    console.log(DataSet);

    $.ajax({
        // url: hrt.url + '?mode=LoadAllData',
        url: '/Model/Ducklab/hrt.inc.php?mode=LoadArm',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';


                    if (value1['SkillAbbrName'] == null)
                        value1['SkillAbbrName'] = "-";
                    if (value1['HrtArmName'] == null)
                        value1['HrtArmName'] = "-";
                    if (value1['HrtSpcName'] == null)
                        value1['HrtSpcName'] = "-";
                    if (value1['SkillName'] == null)
                        value1['SkillName'] = "-";

                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:hrt.SetFrom(\'EDIT\',\'' + value1.SkillNo + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['SkillNo'] + '"></i></a>\
							<a href="javascript:hrt.callModalDeleteFixField(\'HrtSkill\',\'' + value1['SkillNo'] + '\'\,\'เลขหมายความชำนาญทหารอากาศ\',\'SkillNo\',\'SkillDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1['HrtArmName'] + '</td>\
                        <td class="text-center">' + value1['HrtSpcName'] + '</td>\
                        <td class="text-center">' + value1['SkillNo'] + '</td>\
                        <td class="text-center">' + value1['SkillName'] + '</td>\
                        <td class="text-center">' + value1['SkillAbbrName'] + '</td>\
						<td class="text-center">' + hrt.SetActive(value1['SkillActive']) + '</td>\
					</tr>\
					';


                    $('#SkillTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#SkillTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadAirForceMilitaryNumber');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.GetAirForceMilitaryNumber = function() {
    var DataSet = {

        where: {
            SkillNo: $("#dataid").val()
        }
    };
    console.log(DataSet);
    $.ajax({
        url: hrt.url + '?mode=LoadDataSkill',
        //  url: '/Model/Ducklab/hrt.class.php?HrtClass=LoadDataSkill',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {

        },
        // hrt.GetAirForceMilitaryNumber = function() {
        //     var dataid = $("#dataid").val();
        //     var DataSet = {
        //         table: 'HrtSkill',
        //         where: {
        //             SkillNo: dataid,
        //         }
        //     };
        //     //console.log(DataSet);
        //     $.ajax({
        //         url: hrt.url + '?mode=LoadOneRow',
        //         type: 'POST',
        //         dataType: 'json',
        //         data: DataSet,
        //         beforeSend: function() {
        //             $("#modalLoading").show();
        //         },
        success: function(result, status, xhr) {

            console.log(result);

            //startDate 

            if (result) {
                var resultx = result[0];
                // $("#ArmCode").val(result.ArmCode);
                // $("#SpcID").val(result.SpcID);
                $("#SkillNo").val(resultx.SkillNo);
                $("#SkillName").val(resultx.SkillName);
                $("#SkillAbbrName").val(resultx.SkillAbbrName);
                if (resultx.SkillActive == 0) {
                    $("#SkillActive").bootstrapToggle('off');
                } else {
                    $("#SkillActive").bootstrapToggle('on');
                }


                hrt.HrtArmSel(resultx.ArmCode, 'HrtArmName');
                hrt.HrtSpcSel(resultx.SpcID, 'HrtSpcName');

            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetAirForceMilitaryNumber');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.EditAirForceMilitaryNumber = function() {
    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();
    var active = 0;

    var checkBox = document.getElementById("SkillActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }

    var exDat = {
        table: "HrtSkill",
        data: {
            // ArmCode: $("#ArmCode").val(),
            // SpcID: $("#SpcID").val(),
            ArmCode: $("#HrtArmName").val(),
            SpcID: $("#HrtSpcName").val(),
            SkillNo: $("#SkillNo").val(),
            SkillName: $("#SkillName").val(),
            SkillAbbrName: $("#SkillAbbrName").val(),
            SkillActive: active,
            UpdateUserID: $("#UpdateUserID").val(),
            UpdateDate: $("#UpdateDate").val()
        },
        where: {
            SkillNo: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

hrt.CreateAirForceMilitaryNumber = function() {

    $('#modal_createcf').modal('hide');


    var active = 0;

    var checkBox = document.getElementById("SkillActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }

    var exDat = {
        table: "HrtSkill",
        data: {
            // ArmCode: $("#ArmCode").val(),
            // SpcID: $("#SpcID").val(),
            ArmCode: $("#HrtArmName").val(),
            SpcID: $("#HrtSpcName").val(),
            SkillNo: $("#SkillNo").val(),
            SkillName: $("#SkillName").val(),
            SkillAbbrName: $("#SkillAbbrName").val(),
            SkillActive: active,
            SkillDelete: '0',
            CreateUserID: $("#CreateUserID").val(),
            CreateDate: $("#CreateDate").val() + ".000",
            UpdateUserID: $("#CreateUserID").val(),
            UpdateDate: $("#CreateDate").val() + ".000"
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + data.code;
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};


////////////////////////////////////////////////////////////////////////////////////////////
///000000000000000000000000000000000000000000000000000
////////////////////////////////////////////////////////////////////////////////////////////
hrt.LoadPrefix = function() {
    var DataSet = {
        table: 'HrtPrefix',
        where: {
            HrtPrefixDelete: '0',
        },
        orderby: "",
        limit: ""
    };


    console.log(DataSet);

    $.ajax({
        url: hrt.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';

                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:hrt.SetFrom(\'EDIT\',\'' + value1.HrtPrefixID + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['HrtPrefixID'] + '"></i></a>\
							<a href="javascript:hrt.callModalDeleteFixField(\'HrtPrefix\',\'' + value1['HrtPrefixID'] + '\'\,\'คำนำหน้า\',\'HrtPrefixID\',\'HrtPrefixDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1['HrtPrefixNameTH'] + '</td>\
						<td class="text-center">' + value1['HrtPrefixAbbrNameTH'] + '</td>\
						<td class="text-center">' + hrt.SetActive(value1['HrtPrefixActive']) + '</td>\
					</tr>\
					';


                    $('#PrefixTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#PrefixTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadPrefix');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.GetPrefixs = function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'HrtPrefix',
        where: {
            HrtPrefixID: dataid,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: hrt.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);

            //startDate 


            if (result) {
                $("#HrtPrefixNameTH").val(result.HrtPrefixNameTH);
                $("#HrtPrefixAbbrNameTH").val(result.HrtPrefixAbbrNameTH);
                if (result.HrtPrefixActive == 0) {
                    $("#HrtPrefixActive").bootstrapToggle('off');
                } else {
                    $("#HrtPrefixActive").bootstrapToggle('on');
                }
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetPrefixs');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.EditPrefixs = function() {
    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();
    var active = 0;

    var checkBox = document.getElementById("HrtPrefixActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtPrefix",
        data: {
            HrtPrefixNameTH: $("#HrtPrefixNameTH").val(),
            HrtPrefixAbbrNameTH: $("#HrtPrefixAbbrNameTH").val(),
            HrtPrefixActive: active,
            HrtPrefixTypeUpdateBy: $("#HrtPrefixTypeUpdateBy").val(),
            HrtPrefixTypeUpdateDate: $("#HrtPrefixTypeUpdateDate").val()
        },
        where: {
            HrtPrefixID: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

hrt.CreatePrefixs = function() {
    $('#modal_createcf').modal('hide');


    var active = 0;

    var checkBox = document.getElementById("HrtPrefixActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtPrefix",
        data: {
            HrtPrefixNameTH: $("#HrtPrefixNameTH").val(),
            HrtPrefixAbbrNameTH: $("#HrtPrefixAbbrNameTH").val(),
            HrtPrefixActive: active,
            HrtPrefixDelete: '0',
            HrtPrefixTypeCreateBy: $("#HrtPrefixTypeCreateBy").val(),
            HrtPrefixTypeCreateDate: $("#HrtPrefixTypeCreateDate").val() + ".000",
            HrtPrefixTypeUpdateBy: $("#HrtPrefixTypeCreateBy").val(),
            HrtPrefixTypeUpdateDate: $("#HrtPrefixTypeCreateDate").val() + ".000"
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + data.code;
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};


////////////////////////////////////////////////////////////////////////////////////////////
///1111111111111111111111111111111111111111111111111
////////////////////////////////////////////////////////////////////////////////////////////

hrt.LoadNation = function() {
    var DataSet = {
        table: 'HrtNation',
        where: {
            HrtNationDelete: '0',
        },
        orderby: "",
        limit: ""
    };


    console.log(DataSet);

    $.ajax({
        url: hrt.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';

                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:hrt.SetFrom(\'EDIT\',\'' + value1.HrtNationID + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['HrtNationID'] + '"></i></a>\
							<a href="javascript:hrt.callModalDeleteFixField(\'HrtNation\',\'' + value1['HrtNationID'] + '\'\,\'สัญชาติ\',\'HrtNationID\',\'HrtNationDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1['HrtNationNameTh'] + '</td>\
						<td class="text-center">' + hrt.SetActive(value1['HrtNationActive']) + '</td>\
					</tr>\
					';


                    $('#NationTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#NationTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadNation');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.GetNation = function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'HrtNation',
        where: {
            HrtNationID: dataid,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: hrt.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);

            //startDate 


            if (result) {
                $("#HrtNationNameTh").val(result.HrtNationNameTh);

                if (result.HrtNationActive == 0) {
                    $("#HrtNationActive").bootstrapToggle('off');
                } else {
                    $("#HrtNationActive").bootstrapToggle('on');
                }
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetNation');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.EditNation = function() {
    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();
    var active = 0;

    var checkBox = document.getElementById("HrtNationActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtNation",
        data: {
            HrtNationNameTh: $("#HrtNationNameTh").val(),
            HrtNationActive: active,
            HrtNationUpdateBy: $("#HrtNationUpdateBy").val(),
            HrtNationUpdateDate: $("#HrtNationUpdateDate").val()
        },
        where: {
            HrtNationID: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

hrt.CreateNation = function() {
    $('#modal_createcf').modal('hide');


    var active = 0;

    var checkBox = document.getElementById("HrtNationActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtNation",
        data: {
            HrtNationNameTh: $("#HrtNationNameTh").val(),
            HrtNationActive: active,
            HrtNationDelete: '0',
            HrtNationCreateBy: $("#HrtNationCreateBy").val(),
            HrtNationCreateDate: $("#HrtNationCreateDate").val() + ".000",
            HrtNationUpdateBy: $("#HrtNationCreateBy").val(),
            HrtNationUpdateDate: $("#HrtNationCreateDate").val() + ".000"
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + data.code;
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

////////////////////////////////////////////////////////////////////////////////////////////
////22222222222222222222222222222222222222222222222222
///////////////////////////////////////////////////////////////////////////////////////////

hrt.LoadOrigin = function() {
    var DataSet = {
        table: 'HrtNation',
        where: {
            HrtNationDelete: '0',
        },
        orderby: "",
        limit: ""
    };


    console.log(DataSet);

    $.ajax({
        url: hrt.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';

                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:hrt.SetFrom(\'EDIT\',\'' + value1.HrtNationID + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['HrtNationID'] + '"></i></a>\
							<a href="javascript:hrt.callModalDeleteFixField(\'HrtNation\',\'' + value1['HrtNationID'] + '\'\,\'เชื้อชาติ\',\'HrtNationID\',\'HrtNationDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1['HrtNationNameTh'] + '</td>\
						<td class="text-center">' + hrt.SetActive(value1['HrtNationActive']) + '</td>\
					</tr>\
					';


                    $('#OriginTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#OriginTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadOrigin');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.GetOrigin = function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'HrtNation',
        where: {
            HrtNationID: dataid,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: hrt.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);

            //startDate 


            if (result) {
                $("#HrtNationNameTh").val(result.HrtNationNameTh);

                if (result.HrtNationActive == 0) {
                    $("#HrtNationActive").bootstrapToggle('off');
                } else {
                    $("#HrtNationActive").bootstrapToggle('on');
                }
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetOrigin');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.EditOrigin = function() {
    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();
    var active = 0;

    var checkBox = document.getElementById("HrtNationActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtNation",
        data: {
            HrtNationNameTh: $("#HrtNationNameTh").val(),
            HrtNationActive: active,
            HrtNationUpdateBy: $("#HrtNationUpdateBy").val(),
            HrtNationUpdateDate: $("#HrtNationUpdateDate").val()
        },
        where: {
            HrtNationID: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

hrt.CreateOrigin = function() {
    $('#modal_createcf').modal('hide');


    var active = 0;

    var checkBox = document.getElementById("HrtNationActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtNation",
        data: {
            HrtNationNameTh: $("#HrtNationNameTh").val(),
            HrtNationActive: active,
            HrtNationDelete: '0',
            HrtNationCreateBy: $("#HrtNationCreateBy").val(),
            HrtNationCreateDate: $("#HrtNationCreateDate").val() + ".000",
            HrtNationUpdateBy: $("#HrtNationCreateBy").val(),
            HrtNationUpdateDate: $("#HrtNationCreateDate").val() + ".000"
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + data.code;
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};
////////////////////////////////////////////////////////////////////////////////////////////
////333333333333333333333333333333333333333333333333333
////////////////////////////////////////////////////////////////////////////////////////////
hrt.LoadReligion = function() {
    var DataSet = {
        table: 'HrtReligion',
        where: {
            HrtReligionDelete: '0',
        },
        orderby: "",
        limit: ""
    };


    console.log(DataSet);

    $.ajax({
        url: hrt.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';

                    contents += '\
				<tr>\
					<td class="text-center">\
						<a href="javascript:hrt.SetFrom(\'EDIT\',\'' + value1.HrtReligionID + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['HrtReligionID'] + '"></i></a>\
						<a href="javascript:hrt.callModalDeleteFixField(\'HrtReligion\',\'' + value1['HrtReligionID'] + '\'\,\'เชื้อชาติ\',\'HrtReligionID\',\'HrtReligionDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
					</td>\
					<td class="text-center">' + (key1 + 1) + '</td>\
					<td class="text-center">' + value1['HrtReligionName'] + '</td>\
					<td class="text-center">' + hrt.SetActive(value1['HrtReligionActive']) + '</td>\
				</tr>\
				';


                    $('#ReligionTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#ReligionTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadReligion');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.GetReligion = function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'HrtReligion',
        where: {
            HrtReligionID: dataid,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: hrt.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);

            //startDate 


            if (result) {
                $("#HrtReligionName").val(result.HrtReligionName);

                if (result.HrtReligionActive == 0) {
                    $("#HrtReligionActive").bootstrapToggle('off');
                } else {
                    $("#HrtReligionActive").bootstrapToggle('on');
                }
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetReligion');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.EditReligion = function() {
    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();
    var active = 0;

    var checkBox = document.getElementById("HrtReligionActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtReligion",
        data: {
            HrtReligionName: $("#HrtReligionName").val(),
            HrtReligionActive: active,
            HrtReligionUpdateBy: $("#HrtReligionUpdateBy").val(),
            HrtReligionUpdateDate: $("#HrtReligionUpdateDate").val()
        },
        where: {
            HrtReligionID: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

hrt.CreateReligion = function() {
    $('#modal_createcf').modal('hide');


    var active = 0;

    var checkBox = document.getElementById("HrtReligionActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtReligion",
        data: {
            HrtReligionName: $("#HrtReligionName").val(),
            HrtReligionActive: active,
            HrtReligionDelete: '0',
            HrtReligionCreateBy: $("#HrtReligionCreateBy").val(),
            HrtReligionCreateDate: $("#HrtReligionCreateDate").val() + ".000",
            HrtReligionUpdateBy: $("#HrtReligionCreateBy").val(),
            HrtReligionUpdateDate: $("#HrtReligionCreateDate").val() + ".000"
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + data.code;
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};
////////////////////////////////////////////////////////////////////////////////////////////
////444444444444444444444444444444444444444444444444444444444444444
////////////////////////////////////////////////////////////////////////////////////////////
hrt.LoadCountry = function() {
    var DataSet = {
        table: 'HrtCountry',
        where: {
            CountryDelete: '0',
        },
        orderby: "",
        limit: ""
    };


    console.log(DataSet);

    $.ajax({
        url: hrt.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';


                    if (value1['CountryEngNameEN'] == null)
                        value1['CountryEngNameEN'] = "-";


                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:hrt.SetFrom(\'EDIT\',\'' + value1.CountryID + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['CountryID'] + '"></i></a>\
							<a href="javascript:hrt.callModalDeleteFixField(\'HrtCountry\',\'' + value1['CountryID'] + '\'\,\'ประเทศ\',\'CountryID\',\'CountryDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1['CountryNameTH'] + '</td>\
						<td class="text-center">' + value1['CountryEngNameEN'] + '</td>\
						<td class="text-center">' + hrt.SetActive(value1['CountryActive']) + '</td>\
					</tr>\
					';


                    $('#CountryTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#CountryTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadCountry');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};
hrt.GetCountry = function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'HrtCountry',
        where: {
            CountryID: dataid,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: hrt.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);

            //startDate 


            if (result) {
                $("#CountryNameTH").val(result.CountryNameTH);
                $("#CountryEngNameEN").val(result.CountryEngNameEN);

                if (result.CountryActive == 0) {
                    $("#CountryActive").bootstrapToggle('off');
                } else {
                    $("#CountryActive").bootstrapToggle('on');
                }
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetCountry');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.EditCountry = function() {
    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();
    var active = 0;

    var checkBox = document.getElementById("CountryActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtCountry",
        data: {
            CountryNameTH: $("#CountryNameTH").val(),
            CountryEngNameEN: $("#CountryEngNameEN").val(),
            CountryActive: active,
            CountryUpdateBy: $("#CountryUpdateBy").val(),
            CountryUpdateDate: $("#CountryUpdateDate").val()
        },
        where: {
            CountryID: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

hrt.CreateCountry = function() {
    $('#modal_createcf').modal('hide');


    var active = 0;

    var checkBox = document.getElementById("CountryActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtCountry",
        data: {
            CountryNameTH: $("#CountryNameTH").val(),
            //CountryEngNameEN
            CountryEngNameEN: $("#CountryEngNameEN").val(),
            CountryActive: active,
            CountryDelete: '0',
            CountryCreateBy: $("#CountryCreateBy").val(),
            CountryCreateDate: $("#CountryCreateDate").val() + ".000",
            CountryUpdateBy: $("#CountryCreateBy").val(),
            CountryUpdateDate: $("#CountryCreateDate").val() + ".000"
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + data.code;
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

////////////////////////////////////////////////////////////////////////////////////////////
////55555555555555555555555555555555555555555555555555555555555555555555555
////////////////////////////////////////////////////////////////////////////////////////////
hrt.LoadProvince = function() {
    var DataSet = {
        table: 'HrtProvince',
        where: {
            HrtProvinceDelete: '0',
        },
        orderby: "",
        limit: ""
    };


    console.log(DataSet);

    $.ajax({
        url: hrt.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';


                    if (value1['HrtProvinceAbbrName'] == null)
                        value1['HrtProvinceAbbrName'] = "-";


                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:hrt.SetFrom(\'EDIT\',\'' + value1.HrtProvinceID + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['HrtProvinceID'] + '"></i></a>\
							<a href="javascript:hrt.callModalDeleteFixField(\'HrtProvince\',\'' + value1['HrtProvinceID'] + '\'\,\'จังหวัด\',\'HrtProvinceID\',\'HrtProvinceDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1['HrtProvinceCode'] + '</td>\
						<td class="text-center">' + value1['HrtProvinceName'] + '</td>\
						<td class="text-center">' + hrt.SetActive(value1['HrtProvinceActive']) + '</td>\
					</tr>\
					';


                    $('#ProvinceTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#ProvinceTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadProvince');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.GetProvince = function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'HrtProvince',
        where: {
            HrtProvinceID: dataid,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: hrt.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);

            //startDate 


            if (result) {
                $("#HrtProvinceCode").val(result.HrtProvinceCode);
                $("#HrtProvinceName").val(result.HrtProvinceName);
                // $("#HrtProvinceAbbrName").val(result.HrtProvinceAbbrName);

                if (result.HrtProvinceActive == 0) {
                    $("#HrtProvinceActive").bootstrapToggle('off');
                } else {
                    $("#HrtProvinceActive").bootstrapToggle('on');
                }
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetProvince');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.EditProvince = function() {
    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();
    var active = 0;

    var checkBox = document.getElementById("HrtProvinceActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtProvince",
        data: {
            HrtProvinceCode: $("#HrtProvinceCode").val(),
            HrtProvinceName: $("#HrtProvinceName").val(),
            // HrtProvinceAbbrName: $("#HrtProvinceAbbrName").val(),
            HrtProvinceActive: active,
            HrtProvinceTypeUpdateBy: $("#HrtProvinceTypeUpdateBy").val(),
            HrtProvinceTypeUpdateDate: $("#HrtProvinceTypeUpdateDate").val()
        },
        where: {
            HrtProvinceID: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

hrt.CreateProvince = function() {
    $('#modal_createcf').modal('hide');


    var active = 0;

    var checkBox = document.getElementById("HrtProvinceActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtProvince",
        data: {
            HrtProvinceCode: $("#HrtProvinceCode").val(),
            HrtProvinceName: $("#HrtProvinceName").val(),
            // HrtProvinceAbbrName: $("#HrtProvinceAbbrName").val(),
            HrtProvinceActive: active,
            HrtProvinceDelete: '0',
            HrtProvinceTypeCreateBy: $("#HrtProvinceTypeCreateBy").val(),
            HrtProvinceTypeCreateDate: $("#HrtProvinceTypeCreateDate").val() + ".000",
            HrtProvinceTypeUpdateBy: $("#HrtProvinceTypeCreateBy").val(),
            HrtProvinceTypeUpdateDate: $("#HrtProvinceTypeCreateDate").val() + ".000"
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + data.code;
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

////////////////////////////////////////////////////////////////////////////////////////////
////6666666666666666666666666666666666666666666666666666666666666666666666
/////////////////////////////////////////////////////////////////////////////////////////
hrt.LoadAmphur = function() {
    var DataSet = {
        where: {
            HrtAmphurDelete: '0',
        },
    };

    console.log(DataSet);

    $.ajax({
        // url: hrt.url + '?mode=LoadAllData',
        // url: '/Model/Ducklab/hrt.inc.php?mode=LoadChiefSpc', ดูแบบ
        url: '/Model/Ducklab/hrt.inc.php?mode=LoadAmphur',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';

                    contents += '\
					<tr>\
                        <td class="text-center">\
                            <a href="edit.php?Amphur=&Province"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['HrtAmphurID'] + '"></i></a>\
							<a href="javascript:hrt.callModalDeleteFixField(\'HrtAmphur\',\'' + value1['HrtAmphurID'] + '\'\,\'เขต/อำเภอ\',\'HrtAmphurID\',\'HrtAmphurDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
                        <td class="text-center">' + value1['HrtAmphurCode'] + '</td>\
                        <td class="text-center">' + value1['HrtAmphurName'] + '</td>\
                        <td class="text-center">' + value1['HrtProvinceName'] + '</td>\
						<td class="text-center">' + hrt.SetActive(value1['HrtAmphurActive']) + '</td>\
					</tr>\
					';


                    $('#AmphurTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#AmphurTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadAmphur');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.GetAmphur = function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'HrtAmphur',
        where: {
            HrtAmphurID: dataid,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: hrt.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);

            //startDate 


            if (result) {
                $("#HrtAmphurName").val(result.HrtAmphurName);
                $("#HrtAmphurCode").val(result.HrtAmphurCode);

                if (result.HrtAmphurActive == 0) {
                    $("#HrtAmphurActive").bootstrapToggle('off');
                } else {
                    $("#HrtAmphurActive").bootstrapToggle('on');
                }
                hrt.HrtProvinceSel(result.HrtProvinceID, 'HrtProvince');
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetAmphur');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.EditAmphur = function() {
    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();

    var active = 0;

    var checkBox = document.getElementById("HrtAmphurActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtAmphur",
        data: {

            HrtAmphurCode: $("#HrtAmphurCode").val(),
            HrtAmphurName: $("#HrtAmphurName").val(),
            // HrtProvinceID: $("#HrtProvinceName").val(),
            HrtAmphurActive: active,
            HrtAmphurTypeUpdateBy: $("#HrtAmphurTypeUpdateBy").val(),
            HrtAmphurTypeUpdateDate: $("#HrtAmphurTypeUpdateDate").val()

        },
        where: {
            HrtAmphurID: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

hrt.CreateAmphur = function() {
    $('#modal_createcf').modal('hide');


    var active = 0;

    var checkBox = document.getElementById("HrtAmphurActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtAmphur",
        data: {
            HrtAmphurName: $("#HrtAmphurName").val(),
            HrtAmphurCode: $("#HrtAmphurCode").val(),
            HrtAmphurActive: active,
            HrtAmphurDelete: '0',
            HrtAmphurTypeCreateBy: $("#HrtAmphurTypeCreateBy").val(),
            HrtAmphurTypeCreateDate: $("#HrtAmphurTypeCreateDate").val() + ".000",
            HrtAmphurTypeUpdateBy: $("#HrtAmphurTypeCreateBy").val(),
            HrtAmphurTypeUpdateDate: $("#HrtAmphurTypeCreateDate").val() + ".000"
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + data.code;
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

////////////////////////////////////////////////////////////////////////////////////////////
////7777777777777777777777777777777777777777777777777777777777777777777
////////////////////////////////////////////////////////////////////////////////////////////
hrt.LoadDistrict = function() {
    var DataSet = {
        table: 'HrtDistrict',
        where: {
            HrtDistrictDelete: '0',
        },
        orderby: "",
        limit: "1000"
    };


    console.log(DataSet);

    $.ajax({
        url: hrt.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';


                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:hrt.SetFrom(\'EDIT\',\'' + value1.HrtDistrictID + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['HrtDistrictID'] + '"></i></a>\
							<a href="javascript:hrt.callModalDeleteFixField(\'HrtDistrict\',\'' + value1['HrtDistrictID'] + '\'\,\'จังหวัด\',\'HrtDistrictID\',\'HrtDistrictDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1['HrtDistrictCode'] + '</td>\
						<td class="text-center">' + value1['HrtDistrictName'] + '</td>\
						<td class="text-center">' + hrt.SetActive(value1['HrtDistrictActive']) + '</td>\
					</tr>\
					';


                    $('#DistrictTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#DistrictTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadDistrict');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.GetDistrict = function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'HrtDistrict',
        where: {
            HrtDistrictID: dataid,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: hrt.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);

            //startDate 


            if (result) {
                $("#HrtDistrictCode").val(result.HrtDistrictCode);
                $("#HrtDistrictName").val(result.HrtDistrictName);

                if (result.HrtDistrictActive == 0) {
                    $("#HrtDistrictActive").bootstrapToggle('off');
                } else {
                    $("#HrtDistrictActive").bootstrapToggle('on');
                }
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetDistrict');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.EditDistrict = function() {
    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();
    var active = 0;

    var checkBox = document.getElementById("HrtDistrictActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtDistrict",
        data: {
            HrtDistrictCode: $("#HrtDistrictCode").val(),
            HrtDistrictName: $("#HrtDistrictName").val(),
            HrtDistrictActive: active,
            HrtDistrictUpdateBy: $("#HrtDistrictUpdateBy").val(),
            HrtDistrictUpdateDate: $("#HrtDistrictUpdateDate").val()
        },
        where: {
            HrtDistrictID: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};


hrt.CreateDistrict = function() {
    $('#modal_createcf').modal('hide');


    var active = 0;

    var checkBox = document.getElementById("HrtDistrictActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtDistrict",
        data: {
            HrtDistrictCode: $("#HrtDistrictCode").val(),
            HrtDistrictName: $("#HrtDistrictName").val(),
            HrtDistrictActive: active,
            HrtDistrictDelete: '0',
            HrtDistrictCreateBy: $("#HrtDistrictCreateBy").val(),
            HrtDistrictCreateDate: $("#HrtDistrictCreateDate").val() + ".000",
            HrtDistrictUpdateBy: $("#HrtDistrictCreateBy").val(),
            HrtDistrictUpdateDate: $("#HrtDistrictCreateDate").val() + ".000"
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + data.code;
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

////////////////////////////////////////////////////////////////////////////////////////////
//HrtDistrictID			int
//HrtDistrictCode		nvarchar(50)
//HrtDistrictName			nvarchar(100)
//HrtAmphurID		int
//HrtProviceID
//HrtGeoID
//HrtDistrictCreateBy
//HrtDistrictCreateDate     datetime
//HrtDistrictUpdateBy
//HrtDistrictUpdateDate      datetime
//HrtDistrictActive
//HrtDistrictDelete
///////////////////////////////////////////////////////////////////////////////////////////
////8888888888888888888888888888888888888888888888888888888888888888888
////////////////////////////////////////////////////////////////////////////////////////////

hrt.LoadEduCourse = function() {
    var DataSet = {
        table: 'HrtEduCourse',
        where: {
            HrtEduMainDelete: '0',
        },
        orderby: "",
        limit: "1000"
    };


    console.log(DataSet);

    $.ajax({
        url: hrt.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';


                    if (value1['HrtEduMainCourseAbbrName'] == null)
                        value1['HrtEduMainCourseAbbrName'] = "-";


                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:hrt.SetFrom(\'EDIT\',\'' + value1.HrtEduMainID + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['HrtEduMainID'] + '"></i></a>\
							<a href="javascript:hrt.callModalDeleteFixField(\'HrtEduCourse\',\'' + value1['HrtEduMainID'] + '\'\,\'หลักสูตรการศึกษา\',\'HrtEduMainID\',\'HrtEduMainDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1['HrtEduMainCourseName'] + '</td>\
						<td class="text-center">' + value1['HrtEduMainCourseAbbrName'] + '</td>\
						<td class="text-center">' + hrt.SetActive(value1['HrtEduMainActive']) + '</td>\
					</tr>\
					';


                    $('#EduCourseTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#EduCourseTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadEduCourse');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.GetEduCourse = function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'HrtEduCourse',
        where: {
            HrtEduMainID: dataid,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: hrt.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);

            //startDate 


            if (result) {
                $("#HrtEduMainCourseName").val(result.HrtEduMainCourseName);
                $("#HrtEduMainCourseAbbrName").val(result.HrtEduMainCourseAbbrName);

                if (result.HrtEduMainActive == 0) {
                    $("#HrtEduMainActive").bootstrapToggle('off');
                } else {
                    $("#HrtEduMainActive").bootstrapToggle('on');
                }
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetEduCourse');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.EditEduCourse = function() {


    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();
    var active = 0;

    var checkBox = document.getElementById("HrtEduMainActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtEduCourse",
        data: {
            HrtEduMainCourseName: $("#HrtEduMainCourseName").val(),
            HrtEduMainCourseAbbrName: $("#HrtEduMainCourseAbbrName").val(),
            HrtEduMainActive: active,
            HrtEduMainUpdateBy: $("#HrtEduMainUpdateBy").val(),
            HrtEduMainUpdateDate: $("#HrtEduMainUpdateDate").val()
        },
        where: {
            HrtEduMainID: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

hrt.CreateEduCourse = function() {

    $('#modal_createcf').modal('hide');

    console.log('Test')
    var active = 0;

    var checkBox = document.getElementById("HrtEduMainActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtEduCourse",
        data: {
            HrtEduMainCourseName: $("#HrtEduMainCourseName").val(),
            HrtEduMainCourseAbbrName: $("#HrtEduMainCourseAbbrName").val(),
            HrtEduMainActive: active,
            HrtEduMainDelete: '0',
            HrtEduMainCreateBy: $("#HrtEduMainCreateBy").val(),
            HrtEduMainCreateDate: $("#HrtEduMainCreateDate").val() + ".000",
            HrtEduMainUpdateBy: $("#HrtEduMainCreateBy").val(),
            HrtEduMainUpdateDate: $("#HrtEduMainCreateDate").val() + ".000"
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + data.code;
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};
////////////////////////////////////////////////////////////////////////////////////////////
//HrtEduMainCourseAbbrName
//HrtEduMainCreateBy		int
//HrtEduMainCreateDate		datetime
//HrtEduMainUpdateBy		int
//HrtEduMainUpdateDate		datetime
////////////////////////////////////////////////////////////////////////////////////////////
//AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
////////////////////////////////////////////////////////////////////////////////////////////
hrt.LoadRank = function() {
    var DataSet = {
        table: 'HrtRank',
        where: {
            HrtRankDelete: '0',
        },
        orderby: "",
        limit: ""
    };


    console.log(DataSet);

    $.ajax({
        url: hrt.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';


                    if (value1['HrtRankAbbrTh'] == null)
                        value1['HrtRankAbbrTh'] = "-";

                    if (value1['HrtRankNameTh'] == null)
                        value1['HrtRankNameTh'] = "-";




                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:hrt.SetFrom(\'EDIT\',\'' + value1.HrtRankID + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['HrtRankID'] + '"></i></a>\
							<a href="javascript:hrt.callModalDeleteFixField(\'HrtRank\',\'' + value1['HrtRankID'] + '\'\,\'ยศ\',\'HrtRankID\',\'HrtRankDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1['HrtRankAbbrTh'] + '</td>\
						<td class="text-center">' + value1['HrtRankNameTh'] + '</td>\
						<td class="text-center">' + hrt.SetActive(value1['HrtRankActive']) + '</td>\
					</tr>\
					';


                    $('#RankTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#RankTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadRank');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.GetRank = function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'HrtRank',
        where: {
            HrtRankID: dataid,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: hrt.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);

            //startDate 


            if (result) {
                $("#HrtRankAbbrTh").val(result.HrtRankAbbrTh);
                $("#HrtRankNameTh").val(result.HrtRankNameTh);

                if (result.HrtRankActive == 0) {
                    $("#HrtRankActive").bootstrapToggle('off');
                } else {
                    $("#HrtRankActive").bootstrapToggle('on');
                }
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetRank');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.EditRank = function() {
    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();
    var active = 0;

    var checkBox = document.getElementById("HrtRankActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtRank",
        data: {
            HrtRankAbbrTh: $("#HrtRankAbbrTh").val(),
            HrtRankNameTh: $("#HrtRankNameTh").val(),
            HrtRankActive: active,
            HrtRankUpdateBy: $("#HrtRankUpdateBy").val(),
            HrtRankUpdateDate: $("#HrtRankUpdateDate").val()
        },
        where: {
            HrtRankID: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

hrt.CreateRank = function() {
    $('#modal_createcf').modal('hide');


    var active = 0;

    var checkBox = document.getElementById("HrtRankActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtRank",
        data: {
            HrtRankAbbrTh: $("#HrtRankAbbrTh").val(),
            HrtRankNameTh: $("#HrtRankNameTh").val(),
            HrtRankActive: active,
            HrtRankDelete: '0',
            HrtRankCreateBy: $("#HrtRankCreateBy").val(),
            HrtRankCreateDate: $("#HrtRankCreateDate").val() + ".000",
            HrtRankUpdateBy: $("#HrtRankCreateBy").val(),
            HrtRankUpdateDate: $("#HrtRankCreateDate").val() + ".000"
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + data.code;
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};
////////////////////////////////////////////////////////////////////////////////////////////
//HrtRankCreateBy		int
//HrtRankCreateDate		datetime
//HrtRankUpdateBy		int
//HrtRankUpdateDate		datetime
////////////////////////////////////////////////////////////////////////////////////////////
//BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB
////////////////////////////////////////////////////////////////////////////////////////////
hrt.LoadArm = function() {
    var DataSet = {
        table: 'HrtArm',
        where: {
            ArmDelete: '0',
        },
        orderby: "",
        limit: ""
    };


    console.log(DataSet);

    $.ajax({
        url: hrt.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';

                    // result[0].ArmCode


                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:hrt.SetFrom(\'EDIT\',\'' + value1.ArmCode + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['ArmCode'] + '"></i></a>\
							<a href="javascript:hrt.callModalDeleteFixField(\'HrtArm\',\'' + value1['ArmCode'] + '\'\,\'ยศ\',\'ArmCode\',\'ArmDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
                        <td class="text-center">' + value1['ArmCode'] + '</td>\
                        <td class="text-center">' + value1['ArmName'] + '</td>\
						<td class="text-center">' + value1['ArmAbbrName'] + '</td>\
						<td class="text-center">' + hrt.SetActive(value1['ArmActive']) + '</td>\
					</tr>\
					';


                    $('#ArmTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#ArmTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadArm');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.GetArm = function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'HrtArm',
        where: {
            ArmCode: dataid,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: hrt.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);

            //startDate 


            if (result) {
                $("#ArmCode").val(result.ArmCode);
                $("#ArmName").val(result.ArmName);
                $("#ArmAbbrName").val(result.ArmAbbrName);

                if (result.ArmActive == 0) {
                    $("#ArmActive").bootstrapToggle('off');
                } else {
                    $("#ArmActive").bootstrapToggle('on');
                }
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetArm');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};
/// EditArm  เป็น int ใส่ค่าตัวอักษรไม่ได้ // EditArm ยังไม่เพิ่ม HrtArmCode เพราะว่ามันคือPk  ถ้าเพิ่มลงมันสามารถแก้ไขได้แต่ไม่สามารถเพิ่มโดยกำหนดได้เพราะเราset Auto PK 
hrt.EditArm = function() {
    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();
    var active = 0;

    var checkBox = document.getElementById("ArmActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtArm",
        data: {
            ArmCode: $("#ArmCode").val(),
            ArmName: $("#ArmName").val(),
            ArmAbbrName: $("#ArmAbbrName").val(),
            ArmActive: active,
            UpdateUserID: $("#UpdateUserID").val(),
            UpdateDate: $("#UpdateDate").val()
        },
        where: {
            ArmCode: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

hrt.CreateArm = function() {
    $('#modal_createcf').modal('hide');


    var active = 0;

    var checkBox = document.getElementById("ArmActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtArm",
        data: {
            ArmCode: $("#ArmCode").val(),
            ArmName: $("#ArmName").val(),
            ArmAbbrName: $("#ArmAbbrName").val(),
            ArmActive: active,
            ArmDelete: '0',
            CreateUserID: $("#CreateUserID").val(),
            CreateDate: $("#CreateDate").val() + ".000",
            UpdateUserID: $("#CreateUserID").val(),
            UpdateDate: $("#CreateDate").val() + ".000"
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=AddDataID',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + $("#ArmCode").val();
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};
////////////////////////////////////////////////////////////////////////////////////////////
//CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
////////////////////////////////////////////////////////////////////////////////////////////
hrt.LoadSpc = function() {
    var DataSet = {
        table: 'HrtSpc',
        where: {
            SpcDelete: '0',
        },
        orderby: "",
        limit: ""
    };


    console.log(DataSet);

    $.ajax({
        url: hrt.url + '?mode=LoadSpc',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';
                    if (value1['SpcName'] == null)
                        value1['SpcName'] = "-";
                    if (value1['SpcAbbrName'] == null)
                        value1['SpcAbbrName'] = "-";
                    if (value1['HrtPersonTypeName'] == null)
                        value1['HrtPersonTypeName'] = "-";
                    if (value1['ChiefSpcName'] == null)
                        value1['ChiefSpcName'] = "-";
                    if (value1['ArmName'] == null)
                        value1['ArmName'] = "-";
                    if (value1['SpcID'] == null)
                        value1['SpcID'] = "-";


                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:hrt.SetFrom(\'EDIT\',\'' + value1.SpcID + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['SpcID'] + '"></i></a>\
							<a href="javascript:hrt.callModalDeleteFixField(\'HrtSpc\',\'' + value1['SpcID'] + '\'\,\'จำพวกทหาร\',\'SpcID\',\'SpcDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
                        <td class="text-center">' + (key1 + 1) + '</td>\
                        <td class="text-center">' + value1['SpcID'] + '</td>\
						<td class="text-center">' + value1['SpcName'] + '</td>\
                        <td class="text-center">' + value1['SpcAbbrName'] + '</td>\
                        <td class="text-center">' + value1['ArmName'] + '</td>\
                        <td class="text-center">' + value1['HrtPersonTypeName'] + '</td>\
                        <td class="text-center">' + value1['ChiefSpcName'] + '</td>\
						<td class="text-center">' + hrt.SetActive(value1['SpcActive']) + '</td>\
					</tr>\
					';


                    $('#SpcTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#SpcTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadSpc');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.GetSpc = function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'HrtSpc',
        // table1: 'HrtChiefSpc',
        // table2: 'HrtPersonType',
        // table3: 'HrtArm',
        // data: 'data',
        where: {
            SpcID: dataid,
        }
    };
    console.log(DataSet);
    $.ajax({
        // url: hrt.url + '?mode=LoadFourRow',
        url: hrt.url + '?mode=LoadSpc',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);

            //startDate 
            var data = result[0];

            if (data) {

                $("#SpcID").val(result[0].SpcID);
                $("#SpcName").val(data.SpcName);
                $("#SpcAbbrName").val(data.SpcAbbrName);

                if (data.SpcActive == 0) {
                    $("#SpcActive").bootstrapToggle('off');
                } else {
                    $("#SpcActive").bootstrapToggle('on');
                }
                hrt.LoadPersonTypeSel(data.PersonTypeID, 'HrtPersonTypeName');
                hrt.LoadChiefSpcSel(data.ChiefSpcID, 'ChiefSpcName');
                hrt.HrtArmSel(data.ArmCode, 'ArmName');

            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetSpc');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.EditSpc = function() {
    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();
    var active = 0;

    var checkBox = document.getElementById("SpcActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtSpc",
        data: {
            PersonTypeID: $("#HrtPersonTypeName").val(),
            ChiefSpcID: $("#ChiefSpcName").val(),
            ArmCode: $("#ArmName").val(),
            SpcID: $("#SpcID").val(),
            SpcName: $("#SpcName").val(),
            SpcAbbrName: $("#SpcAbbrName").val(),
            SpcActive: active,
            SpcTypeUpdateBy: $("#SpcTypeUpdateBy").val(),
            SpcTypeUpdateDate: $("#SpcTypeUpdateDate").val()
        },
        where: {
            SpcID: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

hrt.CreateSpc = function() {
    $('#modal_createcf').modal('hide');


    var active = 0;

    var checkBox = document.getElementById("SpcActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtSpc",
        data: {
            PersonTypeID: $("#HrtPersonTypeName").val(),
            ChiefSpcID: $("#ChiefSpcName").val(),
            ArmCode: $("#ArmName").val(),
            SpcID: $("#SpcID").val(),
            SpcName: $("#SpcName").val(),
            SpcAbbrName: $("#SpcAbbrName").val(),
            SpcActive: active,
            SpcDelete: '0',
            SpcTypeCreateBy: $("#SpcTypeCreateBy").val(),
            SpcTypeCreateDate: $("#SpcTypeCreateDate").val() + ".000",
            SpcTypeUpdateBy: $("#SpcTypeCreateBy").val(),
            SpcTypeUpdateDate: $("#SpcTypeCreateDate").val() + ".000"
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=AddDataID',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {

                var link = "edit.php?dataid=" + $("#SpcID").val();
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};
////////////////////////////////////////////////////////////////////////////////////////////
//CCCCCCCC1111111111111111111111111111111111111111111111111111111111111111111เพิ่ม
////////////////////////////////////////////////////////////////////////////////////////////
hrt.LoadPass = function() {
    var DataSet = {
        table: 'HrtPass',
        where: {
            HrtPassDelete: '0',
        },
        orderby: "",
        limit: ""
    };


    console.log(DataSet);

    $.ajax({
        url: hrt.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';


                    if (value1['HrtPassAbbrName'] == null)
                        value1['HrtPassAbbrName'] = "-";


                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:hrt.SetFrom(\'EDIT\',\'' + value1.HrtPassID + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['HrtPassID'] + '"></i></a>\
							<a href="javascript:hrt.callModalDeleteFixField(\'HrtPass\',\'' + value1['HrtPassID'] + '\'\,\'สาเหตุปลด\',\'HrtPassID\',\'HrtPassDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1['HrtPassName'] + '</td>\
						<td class="text-center">' + value1['HrtPassAbbrName'] + '</td>\
						<td class="text-center">' + hrt.SetActive(value1['HrtPassActive']) + '</td>\
					</tr>\
					';


                    $('#PassTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#PassTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadPass');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.GetPass = function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'HrtPass',
        where: {
            HrtPassID: dataid,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: hrt.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);

            //startDate 


            if (result) {
                $("#HrtPassName").val(result.HrtPassName);
                $("#HrtPassAbbrName").val(result.HrtPassAbbrName);

                if (result.HrtPassActive == 0) {
                    $("#HrtPassActive").bootstrapToggle('off');
                } else {
                    $("#HrtPassActive").bootstrapToggle('on');
                }
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetPass');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.EditPass = function() {
    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();
    var active = 0;

    var checkBox = document.getElementById("HrtPassActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtPass",
        data: {
            HrtPassName: $("#HrtPassName").val(),
            HrtPassAbbrName: $("#HrtPassAbbrName").val(),
            HrtPassActive: active,
            HrtPassTypeUpdateBy: $("#HrtPassTypeUpdateBy").val(),
            HrtPassTypeUpdateDate: $("#HrtPassTypeUpdateDate").val()
        },
        where: {
            HrtPassID: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

hrt.CreatePass = function() {
    $('#modal_createcf').modal('hide');


    var active = 0;

    var checkBox = document.getElementById("HrtPassActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtPass",
        data: {
            HrtPassName: $("#HrtPassName").val(),
            HrtPassAbbrName: $("#HrtPassAbbrName").val(),
            HrtPassActive: active,
            HrtPassDelete: '0',
            HrtPassTypeCreateBy: $("#HrtPassTypeCreateBy").val(),
            HrtPassTypeCreateDate: $("#HrtPassTypeCreateDate").val() + ".000",
            HrtPassTypeUpdateBy: $("#HrtPassTypeCreateBy").val(),
            HrtPassTypeUpdateDate: $("#HrtPassTypeCreateDate").val() + ".000"
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + data.code;
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};
////////////////////////////////////////////////////////////////////////////////////////////
//CCCCCCCC22222222222222222222222222222222222222222222222222222222222222222222เพิ่ม
////////////////////////////////////////////////////////////////////////////////////////////
//
//
//
//
//หัวหน้าสาวิทยาการยังมีปัญหา
//
//
//
//
//
////////////////////////////////////////////////////////////////////////////////////////////
//CCCCCCCC333333333333333333333333333333333333333333333333333333333333333333333พิ่ม
////////////////////////////////////////////////////////////////////////////////////////////
hrt.LoadChiefSpc = function() {
    var DataSet = {
        where: {
            HrtChiefSpcDelete: '0',
        }
    };
    console.log(DataSet);
    $.ajax({
        //	url: hrt.url+'?mode=LoadAllData', //LoadChiefSpc
        url: '/Model/Ducklab/hrt.inc.php?mode=LoadChiefSpc',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';

                    if (value1['HrtChiefSpcNameAbbr'] == null)
                        value1['HrtChiefSpcNameAbbr'] = "-";


                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="edit.php?ChiefSpc=&PersonType"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['HrtChiefSpcID'] + '"></i></a>\
							<a href="javascript:hrt.callModalDeleteFixField(\'HrtChiefSpc\',\'' + value1['HrtChiefSpcID'] + '\'\,\'สายวิทยาการ\',\'HrtChiefSpcID\',\'HrtChiefSpcDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
                        <td class="text-center">' + (key1 + 1) + '</td>\
                        <td class="text-center">' + value1['HrtChiefSpcID'] + '</td>\
                        <td class="text-center">' + value1['HrtPersonTypeName'] + '</td>\
                        <td class="text-center">' + value1['HrtChiefSpcName'] + '</td>\
						<td class="text-center">' + value1['HrtChiefSpcNameAbbr'] + '</td>\
						<td class="text-center">' + hrt.SetActive(value1['HrtChiefSpcActive']) + '</td>\
					</tr>\
					';


                    $('#ChiefSpcTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#ChiefSpcTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadChiefSpc');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.GetChiefSpc = function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'HrtChiefSpc',
        where: {
            HrtChiefSpcID: dataid,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: hrt.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            console.log(status);
            console.log(xhr);
            //startDate 
            if (result) {

                $("#HrtChiefSpcID").val(result.HrtChiefSpcID);
                $("#HrtChiefSpcName").val(result.HrtChiefSpcName);
                $("#HrtChiefSpcNameAbbr").val(result.HrtChiefSpcNameAbbr);
                if (result.HrtChiefSpcActive == 0) {
                    $("#HrtChiefSpcActive").bootstrapToggle('off');
                } else {
                    $("#HrtChiefSpcActive").bootstrapToggle('on');
                }

                hrt.LoadPersonTypeSel(result.HrtPersonTypeID, 'HrtPersonType');
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetChiefSpc');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};
///////////////////////////////////////////////////
hrt.EditChiefSpc = function() {
    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();
    var active = 0;

    var checkBox = document.getElementById("HrtChiefSpcActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtChiefSpc",
        data: {
            // HrtPersonTypeID: $("#HrtPersonType").val(), ///
            HrtChiefSpcID: $("#HrtChiefSpcID").val(),
            HrtChiefSpcName: $("#HrtChiefSpcName").val(),
            HrtChiefSpcNameAbbr: $("#HrtChiefSpcNameAbbr").val(),
            HrtChiefSpcActive: active,
            HrtChiefSpcUpdateBy: $("#HrtChiefSpcUpdateBy").val(),
            HrtChiefSpcUpdateDate: $("#HrtChiefSpcUpdateDate").val()
        },
        where: {
            HrtChiefSpcID: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

hrt.CreateChiefSpc = function() {
    $('#modal_createcf').modal('hide');


    var active = 0;

    var checkBox = document.getElementById("HrtChiefSpcActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtChiefSpc",
        data: {
            HrtPersonTypeID: $("#HrtPersonType").val(),
            HrtChiefSpcID: $("#HrtChiefSpcID").val(),
            HrtChiefSpcName: $("#HrtChiefSpcName").val(),
            HrtChiefSpcNameAbbr: $("#HrtChiefSpcNameAbbr").val(),
            HrtChiefSpcActive: active,
            HrtChiefSpcDelete: '0',
            HrtChiefSpcCreateBy: $("#HrtChiefSpcCreateBy").val(),
            HrtChiefSpcCreateDate: $("#HrtChiefSpcCreateDate").val() + ".000",
            HrtChiefSpcUpdateBy: $("#HrtChiefSpcCreateBy").val(),
            HrtChiefSpcUpdateDate: $("#HrtChiefSpcCreateDate").val() + ".000"
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + data.code;
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};
////////////////////////////////////////////////////////////////////////////////////////////
///////////////////    SkillSuffix : อักษรตามเลข ลชทอ  /////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
//EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
////////////////////////////////////////////////////////////////////////////////////////////
hrt.LoadSkillSuffix = function() {
    var DataSet = {
        table: 'HrtSkillSuffix',
        where: {
            HrtSkillSuffixDelete: '0',
        }

    };
    console.log(DataSet);

    $.ajax({
        //url:hrt.url+'?mode=LoadAllData',
        url: '/Model/Ducklab/hrt.inc.php?mode=LoadSkillSuffix',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';


                    if (value1['HrtSkillSuffixName'] == null)
                        value1['HrtSkillSuffixName'] = "-";

                    if (value1['HrtSkillSuffixFullName'] == null)
                        value1['HrtSkillSuffixFullName'] = "-";



                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="edit.php?SkillSuffix=&PersonType"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['HrtSkillSuffixID'] + '"></i></a>\
							<a href="javascript:hrt.callModalDeleteFixField(\'HrtSkillSuffix\',\'' + value1['HrtSkillSuffixID'] + '\'\,\'อักษรตามเลข ลชทอ.\',\'HrtSkillSuffixID\',\'HrtSkillSuffixDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
					</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1['HrtPersonTypeName'] + '</td>\
						<td class="text-center">' + value1['HrtSkillSuffixName'] + '</td>\
						<td class="text-center">' + value1['HrtSkillSuffixFullName'] + '</td>\
						<td class="text-mAuto">' + hrt.SetActive(value1['HrtSkillSuffixActive']) + '</td>\
					</tr>\
					';


                    $('#SkillSuffixTable> tbody').append(contents);

                });
            }
        },
        complete: function() {
            $('#SkillSuffixTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadSkillSuffix');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.GetSkillSuffix = function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'HrtSkillSuffix',
        where: {
            HrtPersonTypeID: dataid, //////////////////////////////////////////////////////ผิด
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: hrt.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);

            //startDate 


            if (result) {
                //$("#HrtPersonTypeID").val(result.HrtPersonTypeID);
                $("#HrtSkillSuffixName").val(result.HrtSkillSuffixName);
                $("#HrtSkillSuffixFullName").val(result.HrtSkillSuffixFullName);

                if (result.HrtSkillSuffixActive == 0) {
                    $("#HrtSkillSuffixActive").bootstrapToggle('off');
                } else {
                    $("#HrtSkillSuffixActive").bootstrapToggle('on');
                }

                hrt.LoadPersonTypeSel(result.HrtPersonTypeID, 'HrtPersonType');
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetSkillSuffix');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.EditSkillSuffix = function() {
    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();
    var active = 0;

    var checkBox = document.getElementById("HrtSkillSuffixActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtSkillSuffix",
        data: {
            HrtPersonTypeID: $("#HrtPersonType").val(),
            HrtSkillSuffixName: $("#HrtSkillSuffixName").val(),
            HrtSkillSuffixFullName: $("#HrtSkillSuffixFullName").val(),
            HrtSkillSuffixActive: active,
            HrtSkillSuffixTypeUpdateBy: $("#HrtSkillSuffixTypeUpdateBy").val(),
            HrtSkillSuffixTypeUpdateDate: $("#HrtSkillSuffixTypeUpdateDate").val()
        },
        where: {
            HrtSkillSuffixID: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};
/////////CreateSkillSuffix ยังไม่ได้แก้ 
hrt.CreateSkillSuffix = function() {
    $('#modal_createcf').modal('hide');


    var active = 0;

    var checkBox = document.getElementById("HrtChiefSpcActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtChiefSpc",
        data: {
            HrtPersonTypeID: $("#HrtPersonType").val(),
            HrtChiefSpcID: $("#HrtChiefSpcID").val(),
            HrtChiefSpcName: $("#HrtChiefSpcName").val(),
            HrtChiefSpcNameAbbr: $("#HrtChiefSpcNameAbbr").val(),
            HrtChiefSpcActive: active,
            HrtChiefSpcDelete: '0',
            HrtChiefSpcCreateBy: $("#HrtChiefSpcCreateBy").val(),
            HrtChiefSpcCreateDate: $("#HrtChiefSpcCreateDate").val() + ".000",
            HrtChiefSpcUpdateBy: $("#HrtChiefSpcCreateBy").val(),
            HrtChiefSpcUpdateDate: $("#HrtChiefSpcCreateDate").val() + ".000"
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + data.code;
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

/////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
//HrtChiefSpcCreateBy			int
//HrtChiefSpcCreateDate			datetime
//HrtChiefSpcUpdateBy			int
//HrtChiefSpcUpdateDate			datetime
//ไม่AUTO
////////////////////////////////////////////////////////////////////////////////////////////
//DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
////////////////////////////////////////////////////////////////////////////////////////////
hrt.LoadPosTypeGroup = function() {
    var DataSet = {
        table: 'HrtPosTypeGroup',
        where: {
            HrtPosTypeGroupDelete: '0',
        },
        orderby: "",
        limit: "1000"
    };
    console.log(DataSet);

    $.ajax({

        url: hrt.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';

                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:hrt.SetFrom(\'EDIT\',\'' + value1.HrtPosTypeGroupID + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['HrtPosTypeGroupID'] + '"></i></a>\
							<a href="javascript:hrt.callModalDeleteFixField(\'HrtPosTypeGroup\',\'' + value1['HrtPosTypeGroupID'] + '\'\,\'ประเภทกำลังพล\',\'HrtPosTypeGroupID\',\'HrtPosTypeGroupDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-left">' + value1['HrtPosTypeGroupName'] + '</td>\
						<td class="text-center">' + hrt.SetActive(value1['HrtPosTypeGroupActive']) + '</td>\
					</tr>\
					';
                    //	<a href="javascript:org.callModalDelete('OrgGroupType','<?php echo $value['id'];?>',' <?php echo $menuname;?> ','militaryrank_data.php');" class="dropdown-item "><i class="ft-trash-2"></i> ลบ </a>

                    $('#PosTypeGroupTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#PosTypeGroupTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadPosTypeGroup');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.GetPosTypeGroup = function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'HrtPosTypeGroup',
        where: {
            HrtPosTypeGroupID: dataid,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: hrt.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);

            //startDate 


            if (result) {
                $("#HrtPosTypeGroupID").val(result.HrtPosTypeGroupID);
                $("#HrtPosTypeGroupName").val(result.HrtPosTypeGroupName);

                if (result.HrtPosTypeGroupActive == 0) {
                    $("#HrtPosTypeGroupActive").bootstrapToggle('off');
                } else {
                    $("#HrtPosTypeGroupActive").bootstrapToggle('on');
                }
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetPosTypeGroup');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.EditPosTypeGroup = function() {
    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();
    var active = 0;

    var checkBox = document.getElementById("HrtPosTypeGroupActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtPosTypeGroup",
        data: {
            HrtPosTypeGroupID: $("#HrtPosTypeGroupID").val(),
            HrtPosTypeGroupName: $("#HrtPosTypeGroupName").val(),
            HrtPosTypeGroupActive: active,
            HrtPosTypeGroupUpdateBy: $("#HrtPosTypeGroupUpdateBy").val(),
            HrtPosTypeGroupUpdateDate: $("#HrtPosTypeGroupUpdateDate").val()
        },
        where: {
            HrtPosTypeGroupID: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

hrt.CreatePosTypeGroup = function() {
    $('#modal_createcf').modal('hide');


    var active = 0;

    var checkBox = document.getElementById("HrtPosTypeGroupActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtPosTypeGroup",
        data: {
            HrtPosTypeGroupName: $("#HrtPosTypeGroupName").val(),
            HrtPosTypeGroupActive: active,
            HrtPosTypeGroupDelete: '0',
            HrtPosTypeGroupCreateBy: $("#HrtPosTypeGroupCreateBy").val(),
            HrtPosTypeGroupCreateDate: $("#HrtPosTypeGroupCreateDate").val() + ".000",
            HrtPosTypeGroupUpdateBy: $("#HrtPosTypeGroupCreateBy").val(),
            HrtPosTypeGroupUpdateDate: $("#HrtPosTypeGroupCreateDate").val() + ".000"
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + data.code;
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};
////////////////////////////////////////////////////////////////////////////////////////////
//DDDDDDDDD1111111111111111111111111111111111111111111111111111111111111111111เพิ่ม
////////////////////////////////////////////////////////////////////////////////////////////
//มี
hrt.LoadPosEmpGroup = function() {
    var DataSet = {
        table: 'HrtPosEmpGroup',
        where: {
            HrtPosEmpGroupDelete: '0',
        },
        orderby: "",
        limit: ""
    };


    console.log(DataSet);

    $.ajax({
        url: hrt.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';

                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:hrt.SetFrom(\'EDIT\',\'' + value1.HrtPosEmpGroupCode + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['HrtPosEmpGroupCode'] + '"></i></a>\
							<a href="javascript:hrt.callModalDeleteFixField(\'HrtPosEmpGroup\',\'' + value1['HrtPosEmpGroupCode'] + '\'\,\'กลุ่มพนักงาน\',\'HrtPosEmpGroupCode\',\'HrtPosEmpGroupDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1['HrtPosEmpGroupName'] + '</td>\
						<td class="text-center">' + hrt.SetActive(value1['HrtPosEmpGroupActive']) + '</td>\
					</tr>\
					';


                    $('#PosEmpGroupTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#PosEmpGroupTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadPosEmpGroup');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.GetPosEmpGroup = function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'HrtPosEmpGroup',
        where: {
            HrtPosEmpGroupCode: dataid,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: hrt.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);

            //startDate 


            if (result) {
                $("#HrtPosEmpGroupName").val(result.HrtPosEmpGroupName);

                if (result.HrtPosEmpGroupActive == 0) {
                    $("#HrtPosEmpGroupActive").bootstrapToggle('off');
                } else {
                    $("#HrtPosEmpGroupActive").bootstrapToggle('on');
                }
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetPosEmpGroup');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.EditPosEmpGroup = function() {
    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();
    var active = 0;

    var checkBox = document.getElementById("HrtPosEmpGroupActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtPosEmpGroup",
        data: {
            HrtPosEmpGroupName: $("#HrtPosEmpGroupName").val(),
            HrtPosEmpGroupActive: active,
            HrtPosEmpGroupUpdateBy: $("#HrtPosEmpGroupUpdateBy").val(),
            HrtPosEmpGroupUpdateDate: $("#HrtPosEmpGroupUpdateDate").val()
        },
        where: {
            HrtPosEmpGroupCode: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

hrt.CreatePosEmpGroup = function() {
    $('#modal_createcf').modal('hide');


    var active = 0;

    var checkBox = document.getElementById("HrtPosEmpGroupActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtPosEmpGroup",
        data: {
            HrtPosEmpGroupName: $("#HrtPosEmpGroupName").val(),
            HrtPosEmpGroupActive: active,
            HrtPosEmpGroupDelete: '0',
            HrtPosEmpGroupCreateBy: $("#HrtPosEmpGroupCreateBy").val(),
            HrtPosEmpGroupCreateDate: $("#HrtPosEmpGroupCreateDate").val() + ".000",
            HrtPosEmpGroupUpdateBy: $("#HrtPosEmpGroupCreateBy").val(),
            HrtPosEmpGroupUpdateDate: $("#HrtPosEmpGroupCreateDate").val() + ".000"
        }
    };

    console.log(exDat)

    $.ajax({
        url: org.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + data.code;
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

////////////////////////////////////////////////////////////////////////////////////////////
//HrtPosEmpGroupCreateBy			int
//HrtPosEmpGroupCreateDate			datetime
//HrtPosEmpGroupUpdateBy			int
//HrtPosEmpGroupUpdateDate			datetime
//ไม่AUTO

////////////////////////////////////////////////////////////////////////////////////////////
//DDDDDDDDD2222222222222222222222222222222222222222222222222222222222222222222เพิ่ม
////////////////////////////////////////////////////////////////////////////////////////////
//มี
hrt.LoadPosType = function() {
    var DataSet = {
        table: 'HrtPosType',
        where: {
            PosTypeDelete: '0',
        },
        orderby: "",
        limit: ""
    };


    console.log(DataSet);

    $.ajax({
        url: hrt.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';


                    if (value1['PosAbbrName'] == null)
                        value1['PosAbbrName'] = "-";


                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:hrt.SetFrom(\'EDIT\',\'' + value1.PosTypeID + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['PosTypeID'] + '"></i></a>\
							<a href="javascript:hrt.callModalDeleteFixField(\'HrtPosType\',\'' + value1['PosTypeID'] + '\'\,\'กลุ่มพนักงาน\',\'PosTypeID\',\'PosTypeDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1['PosTypeID'] + '</td>\
						<td class="text-center">' + value1['PosTypeName'] + '</td>\
						<td class="text-center">' + value1['PosAbbrName'] + '</td>\
						<td class="text-center">' + hrt.SetActive(value1['PosTypeActive']) + '</td>\
					</tr>\
					';


                    $('#PosTypeTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#PosTypeTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadPosType');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.GetPosType = function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'HrtPosType',
        where: {
            PosTypeID: dataid,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: hrt.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);

            //startDate 


            if (result) {
                $("#PosTypeID").val(result.PosTypeID);
                $("#PosTypeName").val(result.PosTypeName);
                $("#PosAbbrName").val(result.PosAbbrName);

                if (result.PosTypeActive == 0) {
                    $("#PosTypeActive").bootstrapToggle('off');
                } else {
                    $("#PosTypeActive").bootstrapToggle('on');
                }
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetPosType');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.EditPosType = function() {
    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();
    var active = 0;

    var checkBox = document.getElementById("PosTypeActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtPosType",
        data: {
            PosTypeID: $("#PosTypeID").val(),
            PosTypeName: $("#PosTypeName").val(),
            PosAbbrName: $("#PosAbbrName").val(),
            PosTypeActive: active,
            PosTypeCreateBy: $("#PosTypeCreateBy").val(),
            PosTypeCreateDate: $("#PosTypeCreateDate").val()
        },
        where: {
            PosTypeID: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};
//ยังไม่สามารถสร้างได้ error
hrt.CreatePosType = function() {
    $('#modal_createcf').modal('hide');


    var active = 0;

    var checkBox = document.getElementById("PosTypeActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtPosType",
        data: {
            PosTypeID: $("#PosTypeID").val(),
            PosTypeName: $("#PosTypeName").val(),
            PosAbbrName: $("#PosAbbrName").val(),
            PosTypeActive: active,
            PosTypeDelete: '0',
            PosTypeCreateBy: $("#PosTypeCreateBy").val(),
            PosTypeCreateDate: $("#PosTypeCreateDate").val() + ".000",
            PosTypeUpdateBy: $("#PosTypeCreateBy").val(),
            PosTypeUpdateDate: $("#PosTypeCreateDate").val() + ".000"
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + data.code;
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

////////////////////////////////////////////////////////////////////////////////////////////
//HrtPosTypeCreateBy   int
//HrtPosTypeCreateDate  datetime
//HrtPosTypeUpdateBy    int
//HrtPosTypeUpdateDate
/////////////////////////////////////////////////////////////////////////////////////////////
//EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
////////////////////////////////////////////////////////////////////////////////////////////
///////////////////    SkillPrefix : อักษรนำเลข ลชทอ.  /////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
//EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
////////////////////////////////////////////////////////////////////////////////////////////

hrt.LoadSkillPrefix = function() {
    var DataSet = {
        table: 'HrtSkillPrefix',
        where: {
            HrtSkillPrefixDelete: 0,
        },
        orderby: "",
        limit: ""
    };
    console.log(DataSet);

    $.ajax({
        //url:hrt.url+'?mode=LoadAllData',
        url: '/Model/Ducklab/hrt.inc.php?mode=LoadSkillPrefix',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';



                    if (value1['HrtSkillPrefixFullName'] == null)
                        value1['HrtSkillPrefixFullName'] = "-";



                    contents += '\
					<tr>\
						<td class="text-center">\
						<a href="edit.php?SkillPrefix=&PersonType"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['HrtSkillPrefixID'] + '"></i></a>\
							<a href="javascript:hrt.callModalDeleteFixField(\'HrtSkillPrefix\',\'' + value1['HrtSkillPrefixID'] + '\'\,\'อักษรนำเลข ลชทอ.\',\'HrtSkillPrefixID\',\'HrtSkillPrefixDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1['HrtPersonTypeName'] + '</td>\
						<td class="text-center">' + value1['HrtSkillPrefixName'] + '</td>\
						<td class="text-center">' + value1['HrtSkillPrefixFullName'] + '</td>\
						<td class="text-mAuto">' + hrt.SetActive(value1['HrtSkillPrefixActive']) + '</td>\
					</tr>\
					';


                    $('#SkillPrefixTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#SkillPrefixTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadSkillPrefixTable');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};
/////ยังไม่ได้แก้ GetSkillPrefix
hrt.GetSkillPrefix = function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'HrtPosEmpGroup',
        where: {
            HrtPosEmpGroupCode: dataid,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: hrt.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);

            //startDate 


            if (result) {
                $("#HrtPosEmpGroupName").val(result.HrtPosEmpGroupName);

                if (result.HrtPosEmpGroupActive == 0) {
                    $("#HrtPosEmpGroupActive").bootstrapToggle('off');
                } else {
                    $("#HrtPosEmpGroupActive").bootstrapToggle('on');
                }
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetPosEmpGroup');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};
/////ยังไม่ได้แก้ EditSkillPrefix
hrt.EditSkillPrefix = function() {
    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();
    var active = 0;

    var checkBox = document.getElementById("HrtPosEmpGroupActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtPosEmpGroup",
        data: {
            HrtPosEmpGroupName: $("#HrtPosEmpGroupName").val(),
            HrtPosEmpGroupActive: active,
            HrtPosEmpGroupUpdateBy: $("#HrtPosEmpGroupUpdateBy").val(),
            HrtPosEmpGroupUpdateDate: $("#HrtPosEmpGroupUpdateDate").val()
        },
        where: {
            HrtPosEmpGroupCode: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};
////ยังไม่ได้แก้ CreateSkillPrefix
hrt.CreateSkillPrefix = function() {
    $('#modal_createcf').modal('hide');


    var active = 0;

    var checkBox = document.getElementById("HrtPosEmpGroupActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtPosEmpGroup",
        data: {
            HrtPosEmpGroupName: $("#HrtPosEmpGroupName").val(),
            HrtPosEmpGroupActive: active,
            HrtPosEmpGroupDelete: '0',
            HrtPosEmpGroupCreateBy: $("#HrtPosEmpGroupCreateBy").val(),
            HrtPosEmpGroupCreateDate: $("#HrtPosEmpGroupCreateDate").val() + ".000",
            HrtPosEmpGroupUpdateBy: $("#HrtPosEmpGroupCreateBy").val(),
            HrtPosEmpGroupUpdateDate: $("#HrtPosEmpGroupCreateDate").val() + ".000"
        }
    };

    console.log(exDat)

    $.ajax({
        url: org.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + data.code;
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

/////////////////////////////////////////////////////////////////////////////////////////////
//EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE

//EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//มี
hrt.LoadAddamt = function() {
    var DataSet = {
        table: 'HrtAddamt',
        where: {
            AddamtDelete: '0',
        },
        orderby: "",
        limit: ""
    };


    console.log(DataSet);

    $.ajax({
        url: hrt.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';

                    if (value1['AddamtName'] == null)
                        value1['AddamtName'] = "-";

                    if (value1['AddamtAbbrName'] == null)
                        value1['AddamtAbbrName'] = "-";



                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:hrt.SetFrom(\'EDIT\',\'' + value1.AddamtCode + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['AddamtCode'] + '"></i></a>\
							<a href="javascript:hrt.callModalDeleteFixField(\'HrtAddamt\',\'' + value1['AddamtCode'] + '\'\,\'เงินเพิ่ม\',\'AddamtCode\',\'AddamtDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1['AddamtCode'] + '</td>\
						<td class="text-center">' + value1['AddamtName'] + '</td>\
						<td class="text-center">' + value1['AddamtAbbrName'] + '</td>\
						<td class="text-center">' + hrt.SetActive(value1['AddamtActive']) + '</td>\
					</tr>\
					';


                    $('#AddamtTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#AddamtTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadAddamt');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.GetAddamt = function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'HrtAddamt',
        where: {
            AddamtCode: dataid,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: hrt.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);

            //startDate 


            if (result) {
                $("#AddamtCode").val(result.AddamtCode);
                $("#AddamtName").val(result.AddamtName);
                $("#AddamtAbbrName").val(result.AddamtAbbrName);

                if (result.AddamtActive == 0) {
                    $("#AddamtActive").bootstrapToggle('off');
                } else {
                    $("#AddamtActive").bootstrapToggle('on');
                }
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetAddamt');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.EditAddamt = function() {
    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();
    var active = 0;

    var checkBox = document.getElementById("AddamtActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtAddamt",
        data: {
            AddamtCode: $("#AddamtCode").val(),
            AddamtName: $("#AddamtName").val(),
            AddamtAbbrName: $("#AddamtAbbrName").val(),
            AddamtActive: active,
            UpdateUserID: $("#UpdateUserID").val(),
            UpdateDate: $("#UpdateDate").val()
        },
        where: {
            AddamtCode: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};
//////ยังไม่ทำ มีปัญหาเพราะมีการแก้ไข filecode
hrt.CreateAddamt = function() {
    $('#modal_createcf').modal('hide');


    var active = 0;

    var checkBox = document.getElementById("AddamtActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtAddamt",
        data: {
            AddamtCode: $("#AddamtCode").val(),
            AddamtName: $("#AddamtName").val(),
            AddamtAbbrName: $("#AddamtAbbrName").val(),
            AddamtActive: active,
            AddamtDelete: '0',
            CreateUserID: $("#CreateUserID").val(),
            CreateDate: $("#CreateDate").val() + ".000",
            UpdateUserID: $("#CreateUserID").val(),
            UpdateDate: $("#CreateDate").val() + ".000"
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=AddDataID',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();

            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + $("#AddamtCode").val();
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};
////////////////////////////////////////////////////////////////////////////////////////////
//FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
////////////////////////////////////////////////////////////////////////////////////////////
//มี
hrt.LoadPosEmpLevel = function() {
    var DataSet = {
        table: 'HrtPosEmpLevel',
        where: {
            HrtPosEmpLevelDelete: '0',
        },
        orderby: "",
        limit: ""
    };


    console.log(DataSet);

    $.ajax({
        url: hrt.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';



                    contents += '\
					<tr>\
					<td class="text-center">\
						<a href="javascript:hrt.SetFrom(\'EDIT\',\'' + value1.HrtPosEmpLevelCode + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['HrtPosEmpLevelCode'] + '"></i></a>\
						<a href="javascript:hrt.callModalDeleteFixField(\'HrtPosEmpLevel\',\'' + value1['HrtPosEmpLevelCode'] + '\'\,\'ระดับตำแหน่งของลูกจ้างประจำ\',\'HrtPosEmpLevelCode\',\'HrtPosEmpLevelDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
							</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1['HrtPosEmpLevelName'] + '</td>\
						<td class="text-center">' + hrt.SetActive(value1['HrtPosEmpLevelActive']) + '</td>\
					</tr>\
					';


                    $('#PosEmpLevelTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#PosEmpLevelTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadPosEmpLevel');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.GetPosEmpLevel = function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'HrtPosEmpLevel',
        where: {
            HrtPosEmpLevelCode: dataid,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: hrt.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);

            //startDate 


            if (result) {
                $("#HrtPosEmpLevelName").val(result.HrtPosEmpLevelName);

                if (result.HrtPosEmpLevelActive == 0) {
                    $("#HrtPosEmpLevelActive").bootstrapToggle('off');
                } else {
                    $("#HrtPosEmpLevelActive").bootstrapToggle('on');
                }
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetPosEmpLevel');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.EditPosEmpLevel = function() {
    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();
    var active = 0;

    var checkBox = document.getElementById("HrtPosEmpLevelActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtPosEmpLevel",
        data: {
            HrtPosEmpLevelName: $("#HrtPosEmpLevelName").val(),
            HrtPosEmpLevelActive: active,
            HrtPosEmpLevelUpdateBy: $("#HrtPosEmpLevelUpdateBy").val(),
            HrtPosEmpLevelUpdateDate: $("#HrtPosEmpLevelUpdateDate").val()
        },
        where: {
            HrtPosEmpLevelCode: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

hrt.CreatePosEmpLevel = function() {
    $('#modal_createcf').modal('hide');


    var active = 0;

    var checkBox = document.getElementById("HrtPosEmpLevelActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtPosEmpLevel",
        data: {
            HrtPosEmpLevelName: $("#HrtPosEmpLevelName").val(),
            HrtPosEmpLevelActive: active,
            HrtPosEmpLevelDelete: '0',
            HrtPosEmpLevelCreateBy: $("#HrtPosEmpLevelCreateBy").val(),
            HrtPosEmpLevelCreateDate: $("#HrtPosEmpLevelCreateDate").val() + ".000",
            HrtPosEmpLevelUpdateBy: $("#HrtPosEmpLevelCreateBy").val(),
            HrtPosEmpLevelUpdateDate: $("#HrtPosEmpLevelCreateDate").val() + ".000"
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + data.code;
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};
////////////////////////////////////////////////////////////////////////////////////////////
//GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG
////////////////////////////////////////////////////////////////////////////////////////////

hrt.LoadPosEmpGroup = function() {
    var DataSet = {
        table: 'HrtPosEmpGroup',
        where: {
            HrtPosEmpGroupDelete: '0',
        },
        orderby: "",
        limit: ""
    };


    console.log(DataSet);

    $.ajax({
        url: hrt.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';



                    contents += '\
					<tr>\
					<td class="text-center">\
						<a href="javascript:hrt.SetFrom(\'EDIT\',\'' + value1.HrtPosEmpGroupCode + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['HrtPosEmpGroupCode'] + '"></i></a>\
						<a href="javascript:hrt.callModalDeleteFixField(\'HrtPosEmpGroup\',\'' + value1['HrtPosEmpGroupCode'] + '\'\,\'ตำแหน่งของลูกจ้างประจำ\',\'HrtPosEmpGroupCode\',\'HrtPosEmpGroupDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
							</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1['HrtPosEmpGroupName'] + '</td>\
						<td class="text-center">' + hrt.SetActive(value1['HrtPosEmpGroupActive']) + '</td>\
					</tr>\
					';


                    $('#PosEmpGroupTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#PosEmpGroupTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadPosEmpGroup');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.GetPosEmpGroup = function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'HrtPosEmpGroup',
        where: {
            HrtPosEmpGroupCode: dataid,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: hrt.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);

            //startDate 


            if (result) {
                $("#HrtPosEmpGroupName").val(result.HrtPosEmpGroupName);

                if (result.HrtPosEmpGroupActive == 0) {
                    $("#HrtPosEmpGroupActive").bootstrapToggle('off');
                } else {
                    $("#HrtPosEmpGroupActive").bootstrapToggle('on');
                }
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetPosEmpGroup');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.EditPosEmpGroup = function() {
    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();
    var active = 0;

    var checkBox = document.getElementById("HrtPosEmpGroupActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtPosEmpGroup",
        data: {
            HrtPosEmpGroupName: $("#HrtPosEmpGroupName").val(),
            HrtPosEmpGroupActive: active,
            HrtPosEmpGroupUpdateBy: $("#HrtPosEmpGroupUpdateBy").val(),
            HrtPosEmpGroupUpdateDate: $("#HrtPosEmpGroupUpdateDate").val()
        },
        where: {
            HrtPosEmpGroupCode: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

hrt.CreatePosEmpGroup = function() {
    $('#modal_createcf').modal('hide');


    var active = 0;

    var checkBox = document.getElementById("HrtPosEmpGroupActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtPosEmpGroup",
        data: {
            HrtPosEmpGroupName: $("#HrtPosEmpGroupName").val(),

            HrtPosEmpGroupActive: active,
            HrtPosEmpGroupDelete: '0',
            HrtPosEmpGroupCreateBy: $("#HrtPosEmpGroupCreateBy").val(),
            HrtPosEmpGroupCreateDate: $("#HrtPosEmpGroupCreateDate").val() + ".000",
            HrtPosEmpGroupUpdateBy: $("#HrtPosEmpGroupCreateBy").val(),
            HrtPosEmpGroupUpdateDate: $("#HrtPosEmpGroupCreateDate").val() + ".000"
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + data.code;
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};
////////////////////////////////////////////////////////////////////////////////////////////
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////

hrt.LoadPersonType = function() {
    var DataSet = {
        table: 'HrtPersonType',
        where: {
            HrtPersonTypeDelete: '0',
        },
        orderby: "",
        limit: ""
    };


    console.log(DataSet);

    $.ajax({
        url: hrt.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);
            if (result) {

                $.each(result, function(key1, value1) {
                    var contents = '';

                    contents += '\
					<tr>\
						<td class="text-center">\
							<a href="javascript:hrt.SetFrom(\'EDIT\',\'' + value1.HrtPersonTypeID + '\',\'edit.php\');"><i class="la la-pencil-square-o icon_action  iconedit" id="edit' + value1['HrtPersonTypeID'] + '"></i></a>\
							<a href="javascript:hrt.callModalDeleteFixField(\'HrtPersonType\',\'' + value1['HrtPersonTypeID'] + '\'\,\'สัญชาติ\',\'HrtPersonTypeID\',\'HrtPersonTypeDelete\',\'index.php\');"><i class="la la-trash-o icon_action" id=""></i></a>\
						</td>\
						<td class="text-center">' + (key1 + 1) + '</td>\
						<td class="text-center">' + value1['HrtPersonTypeID'] + '</td>\
						<td class="text-center">' + value1['HrtPersonTypeName'] + '</td>\
						<td class="text-center">' + hrt.SetActive(value1['HrtPersonTypeActive']) + '</td>\
					</tr>\
					';


                    $('#PersonTypeTable> tbody').append(contents);
                });
            }
        },
        complete: function() {
            $('#PersonTypeTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true
            });
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadPersonType');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.GetPersonType = function() {
    var dataid = $("#dataid").val();
    var DataSet = {
        table: 'HrtPersonType',
        where: {
            HrtPersonTypeID: dataid,
        }
    };
    //console.log(DataSet);
    $.ajax({
        url: hrt.url + '?mode=LoadOneRow',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(result, status, xhr) {

            console.log(result);


            if (result) {
                $("#HrtPersonTypeID").val(result.HrtPersonTypeID);
                $("#HrtPersonTypeName").val(result.HrtPersonTypeName);

                if (result.HrtPersonTypeActive == 0) {
                    $("#HrtPersonTypeActive").bootstrapToggle('off');
                } else {
                    $("#HrtPersonTypeActive").bootstrapToggle('on');
                }
            }
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts GetPersonType');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

hrt.EditPersonType = function() {
    $('#modal_editcf').modal('hide');


    var dataid = $("#dataid").val();
    var active = 0;

    var checkBox = document.getElementById("HrtPersonTypeActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtPersonType",
        data: {
            HrtPersonTypeID: $("#HrtPersonTypeID").val(),
            HrtPersonTypeName: $("#HrtPersonTypeName").val(),
            HrtPersonTypeActive: active,
            HrtPersonTypeUpdateBy: $("#HrtPersonTypeUpdateBy").val(),
            HrtPersonTypeUpdateDate: $("#HrtPersonTypeUpdateDate").val()
        },
        where: {
            HrtPersonTypeID: dataid
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=EditDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_editcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "index.php";
                duck.SwalSuccess('แก้ไขข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },


        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

hrt.CreatePersonType = function() {
    $('#modal_createcf').modal('hide');


    var active = 0;

    var checkBox = document.getElementById("HrtPersonTypeActive");
    if (checkBox.checked == true) {
        var active = "1";
    } else {
        var active = "0";
    }


    var exDat = {
        table: "HrtPersonType",
        data: {
            // HrtPersonTypeID: $("#HrtPersonTypeID").val(), // Auto
            HrtPersonTypeName: $("#HrtPersonTypeName").val(),
            HrtPersonTypeActive: active,
            HrtPersonTypeDelete: '0',
            HrtPersonTypeCreateBy: $("#HrtPersonTypeCreateBy").val(),
            HrtPersonTypeCreateDate: $("#HrtPersonTypeCreateDate").val() + ".000",
            HrtPersonTypeUpdateBy: $("#HrtPersonTypeCreateBy").val(),
            HrtPersonTypeUpdateDate: $("#HrtPersonTypeCreateDate").val() + ".000"
        }
    };

    console.log(exDat)

    $.ajax({
        url: hrt.url + '?mode=AddDataSet',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        beforeSend: function() {
            $('#modal_createcf').hide();
            $("#modalLoading").show();
        },
        success: function(data) {

            console.log(data);
            $("#modalLoading").hide();
            if (data.success == "COMPLETE") {
                var link = "edit.php?dataid=" + data.code;
                // var link = "index.php";
                duck.SwalSuccess('เพิ่มข้อมูลสำเร็จ');
                setTimeout(duck.OpenPage, 2000, link, '_self');
            } else {
                duck.NotiWarning();
            }

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(data) {
            console.log(data);
            duck.NotiDanger();
        }
    });
};

/////////////////////////////////////////////////////////////////////////////////////////////
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
////////////////////////////////////////////////////////////////////////////////////////////
hrt.LoadPersonTypeSel = function(v_val, element_id) {
    if (v_val) {
        //		alert(v_val);
    }
    var exDat = {
        table: "HrtPersonType",
        where: { HrtPersonTypeDelete: "0" },
        orderby: "",
        limit: "",
    };

    $.ajax({
        url: hrt.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        success: function(data) {
            //console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกประเภทกำลังพล").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.HrtPersonTypeID == v_val) {
                    $(".select2-chosen").text(result.HrtPersonTypeName);
                    $('<option>').attr('value', result.HrtPersonTypeID).attr('selected', 'selected').text(result.HrtPersonTypeName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.HrtPersonTypeID).text(result.HrtPersonTypeName).appendTo('#' + element_id);
                }
            });

        },
        error: function(data) {
            console.log(data);
            console.log('check hrt.LoadPersonTypeSel');
            duck.NotiDanger();
        }
    });

};
////////////////////////////////////////////////////////////////////////////////////////////
hrt.LoadChiefSpcSel = function(v_val, element_id) {
    if (v_val) {
        //		alert(v_val);
    }
    var exDat = {
        table: "HrtChiefSpc",
        where: { ChiefSpcDelete: "0" },
        orderby: "",
        limit: "",
    };

    $.ajax({
        url: hrt.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        success: function(data) {
            //console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกสายวิทยาการ").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.ChiefSpcID == v_val) {
                    $(".select2-chosen").text(result.ChiefSpcName);
                    $('<option>').attr('value', result.ChiefSpcID).attr('selected', 'selected').text(result.ChiefSpcName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.ChiefSpcID).text(result.ChiefSpcName).appendTo('#' + element_id);
                }
            });

        },
        error: function(data) {
            console.log(data);
            console.log('check hrt.LoadChiefSpcSel');
            duck.NotiDanger();
        }
    });

};
////////////////////////////////////////////////////////////////////////////////////////////
hrt.HrtProvinceSel = function(v_val, element_id) {
    if (v_val) {
        //		alert(v_val);
    }
    var exDat = {
        table: "HrtProvince",
        where: { HrtProvinceDelete: "0" },
        orderby: "",
        limit: "",
    };

    $.ajax({
        url: hrt.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        success: function(data) {
            //console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกจังหวัด").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.HrtProvinceID == v_val) {
                    $(".select2-chosen").text(result.HrtProvinceName);
                    $('<option>').attr('value', result.HrtProvinceID).attr('selected', 'selected').text(result.HrtProvinceName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.HrtProvinceID).text(result.HrtProvinceName).appendTo('#' + element_id);
                }
            });

        },
        error: function(data) {
            console.log(data);
            console.log('check hrt.HrtProvinceSel');
            duck.NotiDanger();
        }
    });

};
////////////////////////////////////////////////////////////////////////////////////////////
hrt.HrtSpcSel = function(v_val, element_id) {
    if (v_val) {
        //		alert(v_val);
    }
    var exDat = {
        table: "HrtSpc",
        where: { SpcDelete: "0" },
        orderby: "",
        limit: "",
    };

    $.ajax({
        url: hrt.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        success: function(data) {
            //console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกจำพวกทหาร").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.SpcID == v_val) {
                    $(".select2-chosen").text(result.SpcName);
                    $('<option>').attr('value', result.SpcID).attr('selected', 'selected').text(result.SpcName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.SpcID).text(result.SpcName).appendTo('#' + element_id);
                }
            });

        },
        error: function(data) {
            console.log(data);
            console.log('check hrt.HrtSpcSel');
            duck.NotiDanger();
        }
    });

};

///////////////////////////////////////////////////////////////////////////////////////////////
hrt.HrtArmSel = function(v_val, element_id) {
    if (v_val) {
        //		alert(v_val);
    }
    var exDat = {
        table: "HrtArm",
        where: { ArmDelete: "0" },
        orderby: "",
        limit: "",
    };

    $.ajax({
        url: hrt.url + '?mode=LoadAllData',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        success: function(data) {
            //console.log(data);
            $('#' + element_id).find('option').remove()
            $('<option>').attr('value', '').text("กรุณาเลือกเหล่าทหาร").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.ArmCode == v_val) {
                    $(".select2-chosen").text(result.ArmName);
                    $('<option>').attr('value', result.ArmCode).attr('selected', 'selected').text(result.ArmName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.ArmCode).text(result.ArmName).appendTo('#' + element_id);
                }
            });

        },
        error: function(data) {
            console.log(data);
            console.log('check hrt.HrtArmSel');
            duck.NotiDanger();
        }
    });

};

/////////////////////////////////////////////////////////////////////////////////////////////////

hrt.checkFieldmore = function(DataSet, element_id) {

    console.log(DataSet);
    $.ajax({
        url: hrt.url + '?mode=LoadCountDistByField',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {},
        success: function(result, status, xhr) {

            console.log(result);

            if (result['cnt'] == 0) {
                $("#" + element_id).removeClass("danger");
                $("#" + element_id).removeClass("border-danger");
                $("#btncreatecf").removeAttr('disabled');
                $("#btneditcf").removeAttr('disabled');

            } else {
                $("#" + element_id).addClass("danger");
                $("#" + element_id).addClass("border-danger");
                $("#btncreatecf").attr('disabled', 'disabled');
                $("#btneditcf").attr('disabled', 'disabled');

            }
        },
        complete: function() {},
        error: function(xhr, status, error) {
            console.log('Please check scripts checkFieldmore');
            console.log(xhr);
            duck.NotiDanger();
        }
    });

};

/////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////            ///////////    //////////            /////////////   ////////////    ////////////
////  /////////////////         ////////  ///////////////////           ////////     ////////////
////           //////////   ///////////             ////////////   /////////////    /////////////
////////////////////////////////////////////////////////////////////////////////////////////////

hrt.SetActive = function(dataactive) {
    if (dataactive == 1) {
        var active = '<i class="la la-toggle-on" style="color: green; font-size:30px; "></i>';
    } else {
        var active = '<i class="la la-toggle-off" style="color: red;font-size:30px;"></i>';
    }

    return active;
};

hrt.CheckCheckbox = function() {

    if (checkBox.checked == true) {

    } else {

    }
    return
};

/////////////////////////////////////////////////////////////////////////////////////
$(document).ready(function() {



});
//EdithrtType
////////////////////          END hrt Script         ///////////////////////////
////////////////////////////////////////////////////////////////////////////////////




//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
$(document).ready(function() {



});
//EdithrtType
////////////////////          END hrt Script         ///////////////////////////
////////////////////////////////////////////////////////////////////////////////////




//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
////////////////////////////////////////////////////////////////////////////////////////////
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
////////////////////////////////////////////////////////////////////////////////////////////