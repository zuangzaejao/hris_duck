////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////    org2 Script For Person    /////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////   For Person registration    /////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

org.LoadDataOrgSel=function(selfDID, OrgTypeID , OrgLevel2DID, element_id){
   
    var v_val = selfDID  ;
    var exDat = { 
        OrgTypeID: OrgTypeID,
        OrgLevel2DID: OrgLevel2DID,
    };

    $.ajax({
        url: org.url + '?mode=LoadDataOrgSel',
        type: 'POST',
        dataType: 'json',
        data: exDat,
        success: function(data) {
           // console.log(data);
             
            $('#' + element_id).find('option').remove();
            $('<option>').attr('value', '').text("กรุณาเลือกชื่อหน่วยงาน").appendTo('#' + element_id);
            $.each(data.orgdetail, function(i, result) {
                if (result.orgdid == v_val) {
                    $(".select2-chosen").text(result.orgname);
                    $('<option>').attr('value', result.orgdid).attr('selected', 'selected').text(result.orgname).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.orgdid).text(result.orgname).appendTo('#' + element_id);
                }
            });  
        },
        error: function(data) {
            console.log(data);
            console.log('check LoadDataOrgSel');
            duck.NotiDanger();
        }
    });
};

org.LoadPositionByOrgLevelDIDSel=function(selfId ,element_id,OrgLevelDID) {
    //console.log(OrgLevelDID);

    var v_val = selfId;
    $("#OrgLevelDIDtxt").val(OrgLevelDID); 

    var DataSet = {
        OrgLevelDID: OrgLevelDID
    };


   // console.log(DataSet);

    $.ajax({
        url: org.url + '?mode=LoadPositionByOrgLevelx',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(data, status, xhr) {
 
            //HrtPositionID: "303224", HrtPosName 
           // console.log(data);
            $('#' + element_id).find('option').remove();
            $('<option>').attr('value', '').text("กรุณาเลือกตำแหน่ง").appendTo('#' + element_id);
            $.each(data, function(i, result) {
                if (result.HrtPositionID == v_val) {
                    $(".select2-chosen").text(result.HrtPosName);
                    $('<option>').attr('value', result.HrtPositionID).attr('selected', 'selected').text(result.HrtPosName).appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.HrtPositionID).text(result.HrtPosName).appendTo('#' + element_id);
                }
            }); 
        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadPositionByOrgLevelDID');
            console.log(xhr);
            duck.NotiDanger();
        }
    });

};

org.LoadPosPersonByOrgLevelDIDSel=function(HrtPositionID ,HrtPosNo ,element_id,OrgLevelDID) {
    //console.log(OrgLevelDID);
  
    var v_val = HrtPositionID;

    var DataSet = {
        OrgLevelDID: OrgLevelDID
    };
 
    console.log(DataSet);

    $.ajax({ 
        url: org.url + '?mode=LoadPosPersonByOrgLevelx',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").modal('show');
        },
        success: function(data, status, xhr) {
  

           // console.log(data);
            $('#' + element_id).find('option').remove();
            $('<option>').attr('value', '').text("กรุณาเลือกตำแหน่ง").appendTo('#' + element_id);
            $.each(data, function(i, result) { 
                 if (result.HrtPositionID == v_val   ) { // && result.HrtPosNum == HrtPosNo
                    $(".select2-chosen").text(result.HrtPosName);
                    $('<option>').attr('value', result.HrtPositionID).attr('data-posno', result.HrtPosNum).attr('data-posname', result.HrtPosName).attr('data-abbrname', result.HrtPosAbbrName).attr('selected', 'selected').text(result.HrtPosName+" ("+result.HrtPosNum+")").appendTo('#' + element_id);
                } else {
                    $('<option>').attr('value', result.HrtPositionID).attr('data-posno', result.HrtPosNum).attr('data-posname', result.HrtPosName).attr('data-abbrname', result.HrtPosAbbrName).text(result.HrtPosName+" ("+result.HrtPosNum+")").appendTo('#' + element_id);
                }
            });  
            
        },
        complete: function() {
            $("#modalLoading").hide(); 
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadPositionByOrgLevelDID');
            console.log(xhr);
            duck.NotiDanger();
        }
    });

};

org.LoadPosPersonDetail=function(HrtPositionID ,HrtPosNo, OrgLevDID){
    var DataSet = {
        HrtPositionID: HrtPositionID ,
        HrtPosNo : HrtPosNo,
        OrgLevDID : OrgLevDID
    };  
    //console.log(DataSet);

    $.ajax({
        url: org.url + '?mode=LoadPosPersonDetail',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            $("#modalLoading").show();
        },
        success: function(data, status, xhr) {
          //  console.log(data); 
           
            var PosOrg =  $('#OrgPosition_t15').find(':selected').data("posname")+  data.orgDetail.listname.orgName ;
            $("#HrtPosOrgName_t15").val(PosOrg); 
            var PosAbbrOrg =  $('#OrgPosition_t15').find(':selected').data("abbrname")+  data.orgDetail.listname.orgNameAbbrSemi ;
            $("#HrtPosOrgAbbrName_t15").val(PosAbbrOrg);
            org.LoadPosPerStatusSel( data[0].PosPerStatus,'PosPerStatus_t15');  
            org.LoadRankFix( data[0].RankID,'RankID_t15','');

            var ArmAbbrName="" ;
            var SkillNo="" ;
            $("#HrtArmAbbrName_t15").val('');
            $("#SkillNo_t15").val('');
            if(data.skill){
                $.each(data.skill, function(i, result) {
                    if(result.ArmAbbrName!=null){
                        ArmAbbrName =   ArmAbbrName+""+result.ArmAbbrName+"/" ;
                    } 
                    SkillNo =  SkillNo+""+result.SkillNo+"/" ;  
                }); 
                ArmAbbrName = ArmAbbrName.substring(0 , ArmAbbrName.length-1);
                SkillNo = SkillNo.substring(0 , SkillNo.length-1 );
            } 
            $("#HrtArmAbbrName_t15").val(ArmAbbrName);
            $("#SkillNo_t15").val(SkillNo);

        
 

        },
        complete: function() {
            $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadPosPersonDetail');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};
  
org.LoadDataPositionById=function(PositionID){
    var DataSet = {
        PositionID: PositionID 
    };
   // console.log(DataSet);
    
    $.ajax({ 
        url: org.url + '?mode=LoadPositionDetail',
        type: 'POST',
        dataType: 'json',
        data: DataSet,
        beforeSend: function() {
            //$("#modalLoading").show();
        },
        success: function(result, status, xhr) {

           // console.log(result);

            //org.LoadDataOrgParent
            org.LoadOrgTypeSel(result.OrgTypeID,'OrgTypeID_t15');
            
            var OrgLevel2 = '';
           
            if(result.OrgData.unitM[2] ){
                OrgLevel2 =  result.OrgData.unitM[2].OrgLevelDID; 
            } 
           
            org.LoadOrg2SelByOrgTypeID(OrgLevel2,$("#OrgTypeID_t15").val(),'OrgLevel2_t15'); 

            org.LoadDataOrgSel(result.OrgLevelDID ,$("#OrgTypeID_t15").val(),OrgLevel2,'OrgLevelAll_t15')


            org.LoadPosPersonByOrgLevelDIDSel( PositionID ,'','OrgPosition_t15',result.OrgLevelDID);

          
        },
        complete: function() {
           // $("#modalLoading").hide();
        },
        error: function(xhr, status, error) {
            console.log('Please check scripts LoadPositionDetail');
            console.log(xhr);
            duck.NotiDanger();
        }
    });
};

// LoadOrg2SelByOrgTypeID
// LoadDataOrgSel
// LoadPosPersonByOrgLevelDIDSel
// LoadPosPersonDetail
 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////  End org2 Script For Person    /////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////